# Prereqisites

1. Install homebrew from https://brew.sh/

2. Install yarn

```
$ brew install yarn
```

3. Install imagemagick

```
$ brew install imagemagick
```

4. Install autocrop

```
$ pip install autocrop
```

5. Install the renamer

```
$ git clone git clone git@bitbucket.org:loudstand/loudstand-image-renamer.git
$ cd loudstand-image-renamer
$ yarn install
```

# Auto renaming

1. Go to correct folder

```
$ cd loudstand-image-renamer
```

2. Set sportradar API key before first run

```
$ export SPORTRADAR_API_KEY=xxxxxxx
```

3. Do the renaming

```
$ yarn execute --id sr:competitor:3006 --path ~/Downloads/drive-download-20190528T232227Z-001/Benfica/
```

Example output:

```
No match for  12.jpg
No match for  5.png
No match for  66.png
{ stats: { renamed: 24, notFound: 3, missingImage: 4 },
  remaining:
   [ { id: 'sr:player:1246960',
       jersey_number: 1,
       name: 'nuno_macedo' },
     { id: 'sr:player:44719',
       jersey_number: 6,
       name: 'fabio_pacheco' },
     { id: 'sr:player:1560740',
       jersey_number: undefined,
       name: 'luis_cartucho' },
     { id: 'sr:player:150282',
       jersey_number: undefined,
       name: 'ivo_vieira' } ] }
✨  Done in 3.22s.
```

Look at output, and look in `failed` folder and fix the rest manually.

Renamed images are now in `renamed` folder.

# Auto crop

```
$ cd ~/Downloads/drive-download-20190528T232227Z-001/Benfica/renamed
$ autocrop --width 200 --height 200 --facePercent 40 --output cropped
```

Cropped images are now in `renamed/cropped` folder.

# Convert to JPEG if needed

Skip this step if all images are already JPEG format.

```
$ cd cropped
$ for FILE in _.png; do convert "$FILE" "${FILE%._}".jpg; rm "$FILE"; done
```

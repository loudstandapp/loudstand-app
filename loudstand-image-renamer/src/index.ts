#!/usr/bin/env node
import program from "commander";
import Axios from "axios";
import fs from "fs";
import Fuse from "fuse.js";

const { SPORTRADAR_API_KEY } = process.env;

interface Player {
  id: string;
  name: string;
  jersey_number?: string;
}

import packageJson from "../package.json";

program
  .version(packageJson.version)
  .option("-p, --path <imagePath> ", "Path for files")
  .option("-i, --id <teamId>", "ID of team")

  .parse(process.argv);

function getUrl(teamId: string) {
  return `https://api.sportradar.us/soccer-t3/global/en/teams/${teamId}/profile.json?api_key=${SPORTRADAR_API_KEY}`;
}

function cleanPlayerName(name: string): string {
  const ordered = name.replace(/^([^,]+), ?(.*)$/, "$2 $1");
  const lowercase = ordered.toLowerCase();
  const noSpaces = lowercase.replace(/\s/g, "_");
  return noSpaces;
}

async function getTeamPlayers(teamId: string): Promise<Player[]> {
  const url = getUrl(teamId);
  const response = await Axios.get(url);
  const players: Player[] = response.data.players;
  const manager: Player | undefined = response.data.manager;
  if (!manager) {
    return players;
  }
  return [...players, manager];
}

function getFileNames(imagePath: string) {
  const files = fs.readdirSync(imagePath);
  return files;
}

interface InterpretedFileName {
  filename: string;
  name?: string;
  jersey_number?: string;
}

function interpretFilename(filename: string): InterpretedFileName | null {
  const match = filename.match(/^(.*)\.[A-z]+$/);
  if (!match) {
    return null;
  }
  const cleanName = match[1]
    .toLowerCase()
    .replace(/-/g, "_")
    .replace(/\s/g, "_");

  const jerseyMatch = cleanName.match(/^[^\d]*?([\d]{1,2})[^\d]*?$/);
  const nameMatch = cleanName.match(/^.*?([a-z]+([-_]+)?[a-z]+).*$/);

  return {
    filename,
    jersey_number: (jerseyMatch && jerseyMatch[1]) || undefined,
    name: (nameMatch && nameMatch[1]) || undefined
  };
}

interface NameMatch {
  score: number;
  player: Player;
}

interface GeneratedMatch {
  name?: NameMatch;
  jersey?: {
    player: Player;
  };
}
type GeneratedMatchList = { [key in string]: GeneratedMatch };

function generateMatches(
  files: InterpretedFileName[],
  players: Player[]
): GeneratedMatchList {
  const filesFuse = new Fuse(files, {
    shouldSort: true,

    includeScore: true,
    threshold: 0.6,
    location: 0,
    distance: 100,
    maxPatternLength: 64,
    minMatchCharLength: 1,
    keys: ["name"]
  });

  const filesForPlayers: { [key in string]: NameMatch } = players.reduce(
    (acc, player) => {
      const fileMatch = filesFuse.search(player.name);
      const firstMatch = fileMatch[0];
      if (!firstMatch) {
        return acc;
      }
      return {
        [firstMatch.item.filename]: { player, score: firstMatch.score },
        ...acc
      };
    },
    {}
  );

  const matched = files.reduce((acc, file) => {
    const nameMatch = filesForPlayers[file.filename];
    const jerseyMatch = players.find(player => {
      if (!player.jersey_number) {
        return false;
      }
      return player.jersey_number.toString() === file.jersey_number;
    });
    return {
      ...acc,
      [file.filename]: {
        name: nameMatch,
        jersey: jerseyMatch ? { player: jerseyMatch } : undefined
      }
    };
  }, {});

  return matched;
}

const NAME_OVER_JERSEY_THRESHOLD = 0.2;

function determineBestMatch(match: GeneratedMatch): Player | null {
  if (!match.jersey && !match.name) {
    return null;
  }
  if (match.name && !match.jersey) {
    return match.name.player;
  }
  if (match.jersey && !match.name) {
    return match.jersey.player;
  }
  if (
    match.jersey &&
    match.name &&
    match.name.score > NAME_OVER_JERSEY_THRESHOLD
  ) {
    return match.jersey.player;
  }
  return match.name ? match.name.player : null;
}

interface BestMatch {
  filename: string;
  player: Player | null;
}
function getFinalMatches(matches: GeneratedMatchList) {
  const bestMatches = Object.keys(matches).reduce(
    (acc: BestMatch[], filename: string) => {
      const player = determineBestMatch(matches[filename]);
      return [...acc, { filename, player }];
    },
    []
  );
  return bestMatches;
}

function filenameFromMatch(bestMatch: BestMatch): string | null {
  if (!bestMatch.player) {
    return null;
  }
  const extensionMatch = bestMatch.filename.match(/^.*\.([A-z]+)$/);
  const extension = extensionMatch ? extensionMatch[1] : "";
  const filename = bestMatch.player.id.replace(/:/g, "-");
  const fullName = `${filename}.${extension}`;
  return fullName;
}

async function run() {
  try {
    // Output dirs
    const renamedPath = `${imagePath}/renamed`;
    const failedPath = `${imagePath}/failed`;
    fs.mkdirSync(renamedPath, { recursive: true });
    fs.mkdirSync(failedPath, { recursive: true });

    // Process players
    const players = await getTeamPlayers(teamId);
    const cleanedPlayers = players.map(({ name, id, jersey_number }) => ({
      id,
      jersey_number,
      name: cleanPlayerName(name)
    }));

    const remainingPlayers: { [key in string]: Player } = cleanedPlayers.reduce(
      (acc, player) => ({
        ...acc,
        [player.id]: player
      }),
      {}
    );

    // Process files
    const files = getFileNames(imagePath);
    const cleanFiles = files
      .map(interpretFilename)
      .filter(file => file !== null);

    // Match players and files
    // @ts-ignore
    const matches = generateMatches(cleanFiles, cleanedPlayers);
    const finalMatches = getFinalMatches(matches);

    // Do the work
    const stats = {
      renamed: 0,
      notFound: 0,
      missingImage: cleanedPlayers.length
    };
    finalMatches.forEach(match => {
      const sourcePath = `${imagePath}/${match.filename}`;
      const destinationFilename = filenameFromMatch(match);
      if (!destinationFilename) {
        console.warn("No match for ", match.filename);
        fs.copyFileSync(sourcePath, `${failedPath}/${match.filename}`);
        stats.notFound++;
        return;
      }
      const destinationPath = `${renamedPath}/${destinationFilename}`;
      fs.copyFileSync(sourcePath, destinationPath);
      match.player && delete remainingPlayers[match.player.id];
      stats.renamed++;
      stats.missingImage--;
    });
    console.log({ stats, remaining: Object.values(remainingPlayers) });
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
}

const { path: imagePath, id: teamId } = program;

if (!SPORTRADAR_API_KEY) {
  console.error(
    "No API key is set. Set a key by running `export SPORTRADAR_API_KEY=xxxxxxx`"
  );
  process.exit(1);
}
if (!imagePath) {
  console.error("Image path is missing");
  process.exit(1);
}
if (!teamId) {
  console.error("Team ID is missing");
  process.exit(1);
}

run();
// console.log(process.env);

const PORT = Number(process.env.PORT) || 8080;
import * as express from "express";
import * as request from "request";

const config = require("./config.json");

const apiKey = process.env.sportradar_api_key;
const api3Prefix = process.env.sportradar_api3_prefix;
if (!apiKey) {
  console.error("sportradar_api_key missing");
  process.exit(666);
  throw new Error("sportradar_api_key not found");
}
if (!api3Prefix) {
  console.error("sportradar_api3_prefix missing");
  process.exit(664);
  throw new Error("sportradar_api3_prefix not found");
}

const tournaments = config.tournaments.join(",");
const eventTypes = config.eventTypes.join(",");

const threads = {
  events: {
    feedUrl: `http://api.sportradar.us/${api3Prefix}/global/stream/events/subscribe?api_key=${apiKey}&format=json&tournament_id=${tournaments}&event_id=${eventTypes}`,
    receiverUrls: config.servers.map(baseUrl => `${baseUrl}/receiveEvent`)
  },
  statistics: {
    feedUrl: `http://api.sportradar.us/${api3Prefix}/global/stream/statistics/subscribe?api_key=${apiKey}&format=json&tournament_id=${tournaments}`,
    receiverUrls: config.servers.map(baseUrl => `${baseUrl}/receiveStatistics`)
  }
};

const MAX_MESSAGES = 10;
const status = {
  serviceStartTime: new Date(),
  threads: {}
};

// If more than 3 heartbeats are missed, we want to reconnect.
const INTERVAL_TIMEOUT = 15000;

const app = express();

app.get("/", (req, res) => {
  res.send(status);
});

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
});

function startReceiving(threadId) {
  const { feedUrl, receiverUrls } = threads[threadId];
  status.threads[threadId] = {
    isRunning: false,
    lastMessages: [],
    feedUrl: "",
    lastErrors: [],
    currentFeedStartTime: null,
    lastMessageTime: null,
    lastErrorTime: null,
    totalErrors: 0,
    totalMessages: 0
  };

  let chunkStr = "";
  const req = request.get(feedUrl, { timeout: INTERVAL_TIMEOUT });

  req.on("socket", () => {
    status.threads[threadId].feedUrl = feedUrl.replace(apiKey, "xxxxxxxxx");
    status.threads[threadId].isRunning = true;
    status.threads[threadId].currentFeedStartTime = new Date();
  });
  req.on("data", data => {
    try {
      chunkStr += data.toString();
      // Wait until next step if the chunk is incomplete.
      if (chunkStr.slice(-2) !== "\r\n") {
        return;
      }
      const message = JSON.parse(chunkStr);
      if (status.threads[threadId].lastMessages.length >= MAX_MESSAGES) {
        status.threads[threadId].lastMessages.shift();
      }
      status.threads[threadId].lastMessages.push({ time: new Date(), message });
      chunkStr = "";
      status.threads[threadId].totalMessages++;
      status.threads[threadId].lastMessageTime = new Date();

      if (message.heartbeat) {
        return;
      }

      receiverUrls.forEach(receiverUrl => {
      try {
        // Push to receiver service
          request.post(receiverUrl, {
            body: message,
            json: true
          });
      } catch (error) {
        if (status.threads[threadId].lastErrors.length >= MAX_MESSAGES) {
          status.threads[threadId].lastErrors.shift();
        }
        status.threads[threadId].lastErrors.push({ error });
        status.threads[threadId].totalErrors++;
        status.threads[threadId].lastErrorTime = new Date();
        console.error("POST ERROR", { error });
      }
      });
    } catch (error) {
      const str = data.toString();
      if (status.threads[threadId].lastErrors.length >= MAX_MESSAGES) {
        status.threads[threadId].lastErrors.shift();
      }
      status.threads[threadId].lastErrors.push({ error, str });
      status.threads[threadId].totalErrors++;
      status.threads[threadId].lastErrorTime = new Date();
      console.error("PARSE ERROR", { error, str });
    }
  });
  req.on("error", (...args) => {
    console.error("CONNECTION ERROR - restarting", args);
    status.threads[threadId].isRunning = false;
    setTimeout(() => startReceiving(threadId), 1000);
  });
  req.on("complete", (...args) => {
    console.log("COMPLETED - restarting", args);
    status.threads[threadId].isRunning = false;
    setTimeout(() => startReceiving(threadId), 1000);
  });
}

function startThreads(threadIds) {
  console.log({ threadIds });

  const threadId = threadIds.shift();
  if (!threadId) {
    console.log("No more threads");
    return;
  }
  console.log("Starting", { threadId });
  startReceiving(threadId);
  // Delay start to avoid hitting request limit.
  setTimeout(() => startThreads(threadIds), 5000);
}

startThreads(Object.keys(threads));

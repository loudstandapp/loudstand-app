SCHEDULED_QUEUE_NAME="ScheduledResults"
CUSTOM_QUEUE_NAME="Custom"
echo "Running predeploy tasks"
gcloud tasks queues update $SCHEDULED_QUEUE_NAME --max-attempts=3 --max-retry-duration=300s
gcloud tasks queues update $CUSTOM_QUEUE_NAME --max-attempts=3 --max-retry-duration=300s

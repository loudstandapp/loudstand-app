export const lineups = {
  generated_at: "2019-04-04T21:52:53+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/match_lineups.json",
  sport_event: {
    id: "sr:match:14946111",
    scheduled: "2019-02-01T20:30:00+00:00",
    start_time_tbd: false,
    tournament_round: {
      type: "group",
      number: 20
    },
    season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19",
      tournament_id: "sr:tournament:238"
    },
    tournament: {
      id: "sr:tournament:238",
      name: "Primeira Liga",
      sport: {
        id: "sr:sport:1",
        name: "Soccer"
      },
      category: {
        id: "sr:category:44",
        name: "Portugal",
        country_code: "PRT"
      }
    },
    competitors: [
      {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "RAV",
        qualifier: "home"
      },
      {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "TON",
        qualifier: "away"
      }
    ],
    venue: {
      id: "sr:venue:2013",
      name: "Estadio Dos Arcos",
      capacity: 9065,
      city_name: "Vila do Conde",
      country_name: "Portugal",
      map_coordinates: "41.362755,-8.740186",
      country_code: "PRT"
    }
  },
  lineups: [
    {
      team: "home",
      formation: "4-3-3",
      manager: {
        id: "sr:player:108007",
        name: "Ramos, Daniel",
        nationality: "Portugal",
        country_code: "PRT"
      },
      jersey: {
        type: "third",
        base: "ffffff",
        sleeve: "66ff00",
        number: "000000",
        squares: false,
        stripes: false,
        horizontal_stripes: false,
        split: false,
        shirt_type: "short_sleeves"
      },
      starting_lineup: [
        {
          id: "sr:player:595598",
          name: "Jardim, Leo",
          type: "goalkeeper",
          jersey_number: 20,
          position: "Goalkeeper",
          order: 1
        },
        {
          id: "sr:player:891520",
          name: "Nadjack",
          type: "defender",
          jersey_number: 22,
          position: "Right back",
          order: 2
        },
        {
          id: "sr:player:110767",
          name: "Buatu, Jonathan",
          type: "defender",
          jersey_number: 5,
          position: "Central defender",
          order: 3
        },
        {
          id: "sr:player:364600",
          name: "Monte, Nelson",
          type: "defender",
          jersey_number: 4,
          position: "Central defender",
          order: 4
        },
        {
          id: "sr:player:853560",
          name: "Reis, Matheus",
          type: "defender",
          jersey_number: 2,
          position: "Left back",
          order: 5
        },
        {
          id: "sr:player:44751",
          name: "Tarantini",
          type: "midfielder",
          jersey_number: 8,
          position: "Central midfielder",
          order: 6
        },
        {
          id: "sr:player:244591",
          name: "Schmidt, Joao",
          type: "midfielder",
          jersey_number: 15,
          position: "Central midfielder",
          order: 7
        },
        {
          id: "sr:player:780366",
          name: "Jambor, Nikola",
          type: "midfielder",
          jersey_number: 23,
          position: "Central midfielder",
          order: 8
        },
        {
          id: "sr:player:59784",
          name: "Moreira, Bruno",
          type: "forward",
          jersey_number: 9,
          position: "Striker",
          order: 10
        },
        {
          id: "sr:player:1017831",
          name: "Freitas, Murilo",
          type: "forward",
          jersey_number: 7,
          position: "Striker",
          order: 11
        },
        {
          id: "sr:player:1013263",
          name: "Galeno, Wenderson",
          type: "forward",
          jersey_number: 90,
          position: "Striker",
          order: 9
        }
      ],
      substitutes: [
        {
          id: "sr:player:966647",
          name: "Vitor, Paulo",
          type: "goalkeeper",
          jersey_number: 29
        },
        {
          id: "sr:player:344199",
          name: "Semedo, Ruben",
          type: "defender",
          jersey_number: 3
        },
        {
          id: "sr:player:252661",
          name: "Afonso Figueiredo",
          type: "defender",
          jersey_number: 25
        },
        {
          id: "sr:player:556766",
          name: "Vito",
          type: "midfielder",
          jersey_number: 81
        },
        {
          id: "sr:player:244065",
          name: "Leandrinho",
          type: "midfielder",
          jersey_number: 21
        },
        {
          id: "sr:player:553678",
          name: "Jeronimo, Ronan",
          type: "forward",
          jersey_number: 19
        },
        {
          id: "sr:player:1696861",
          name: "Silva, Andre",
          type: "forward",
          jersey_number: 99
        }
      ]
    },
    {
      team: "away",
      formation: "4-4-2",
      manager: {
        id: "sr:player:867046",
        name: "Pepa",
        nationality: "Portugal",
        country_code: "PRT"
      },
      jersey: {
        type: "away",
        base: "000000",
        sleeve: "ffff00",
        number: "ffff00",
        squares: false,
        stripes: false,
        horizontal_stripes: false,
        split: false,
        shirt_type: "short_sleeves",
        sleeve_detail: "000000"
      },
      starting_lineup: [
        {
          id: "sr:player:116372",
          name: "Claudio Ramos",
          type: "goalkeeper",
          jersey_number: 1,
          position: "Goalkeeper",
          order: 1
        },
        {
          id: "sr:player:862680",
          name: "Moufi, Fahd",
          type: "defender",
          jersey_number: 18,
          position: "Right back",
          order: 2
        },
        {
          id: "sr:player:252717",
          name: "Alves, Ricardo",
          type: "defender",
          jersey_number: 34,
          position: "Central defender",
          order: 3
        },
        {
          id: "sr:player:5883",
          name: "Costa, Ricardo",
          type: "defender",
          jersey_number: 20,
          position: "Central defender",
          order: 4
        },
        {
          id: "sr:player:156319",
          name: "Joaozinho",
          type: "defender",
          jersey_number: 23,
          position: "Left back",
          order: 5
        },
        {
          id: "sr:player:339695",
          name: "Delgado, Juan",
          type: "forward",
          jersey_number: 15,
          position: "Right winger",
          order: 6
        },
        {
          id: "sr:player:61180",
          name: "Monteiro, Bruno",
          type: "midfielder",
          jersey_number: 27,
          position: "Central midfielder",
          order: 7
        },
        {
          id: "sr:player:856980",
          name: "Jaquite, Joao",
          type: "midfielder",
          jersey_number: 5,
          position: "Central midfielder",
          order: 8
        },
        {
          id: "sr:player:252707",
          name: "Xavier",
          type: "forward",
          jersey_number: 7,
          position: "Left winger",
          order: 9
        },
        {
          id: "sr:player:795906",
          name: "Pena, Sergio",
          type: "midfielder",
          jersey_number: 16,
          position: "Striker",
          order: 11
        },
        {
          id: "sr:player:126076",
          name: "Tomane",
          type: "forward",
          jersey_number: 19,
          position: "Striker",
          order: 10
        }
      ],
      substitutes: [
        {
          id: "sr:player:357724",
          name: "Silva, Pedro",
          type: "goalkeeper",
          jersey_number: 28
        },
        {
          id: "sr:player:343957",
          name: "Pite",
          type: "defender",
          jersey_number: 10
        },
        {
          id: "sr:player:343487",
          name: "Reis, Joao",
          type: "defender",
          jersey_number: 17
        },
        {
          id: "sr:player:1142544",
          name: "Tembeng, Nego",
          type: "midfielder",
          jersey_number: 6
        },
        {
          id: "sr:player:1067538",
          name: "Mendes, Joao",
          type: "midfielder",
          jersey_number: 25
        },
        {
          id: "sr:player:247903",
          name: "Arango, Chicho",
          type: "forward",
          jersey_number: 70
        },
        {
          id: "sr:player:845913",
          name: "Murillo, Jhon",
          type: "forward",
          jersey_number: 14
        }
      ]
    }
  ]
};

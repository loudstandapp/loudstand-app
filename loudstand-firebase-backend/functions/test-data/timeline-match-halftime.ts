export const matchHalfTime = {
  generated_at: "2019-03-31T19:49:46+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/match_summary.json",
  sport_event: {
    id: "sr:match:14946247",
    scheduled: "2019-03-31T19:00:00+00:00",
    start_time_tbd: false,
    tournament_round: {
      type: "group",
      number: 27,
      phase: "main_round_1"
    },
    season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19",
      tournament_id: "sr:tournament:238"
    },
    tournament: {
      id: "sr:tournament:238",
      name: "Primeira Liga",
      sport: {
        id: "sr:sport:1",
        name: "Soccer"
      },
      category: {
        id: "sr:category:44",
        name: "Portugal",
        country_code: "PRT"
      }
    },
    competitors: [
      {
        id: "sr:competitor:2995",
        name: "Boavista FC",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "BOA",
        qualifier: "home"
      },
      {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "BEL",
        qualifier: "away"
      }
    ],
    venue: {
      id: "sr:venue:14556",
      name: "Estadio Do Bessa Xxi",
      capacity: 28263,
      city_name: "Porto",
      country_name: "Portugal",
      map_coordinates: "41.162278,-8.642753",
      country_code: "PRT"
    }
  },
  sport_event_conditions: {
    referee: {
      id: "sr:referee:81446",
      name: "Almeida, Nuno",
      nationality: "Portugal",
      country_code: "PRT"
    },
    venue: {
      id: "sr:venue:14556",
      name: "Estadio Do Bessa Xxi",
      capacity: 28263,
      city_name: "Porto",
      country_name: "Portugal",
      map_coordinates: "41.162278,-8.642753",
      country_code: "PRT"
    },
    weather_info: {
      pitch: "good",
      weather_conditions: "good"
    }
  },
  sport_event_status: {
    status: "live",
    match_status: "halftime",
    home_score: 0,
    away_score: 0,
    clock: {
      match_time: "45:00"
    },
    period_scores: [
      {
        home_score: 0,
        away_score: 0,
        type: "regular_period",
        number: 1
      },
      {
        home_score: 0,
        away_score: 0,
        type: "regular_period",
        number: 2
      }
    ]
  },
  statistics: {
    teams: [
      {
        id: "sr:competitor:2995",
        name: "Boavista FC",
        abbreviation: "BOA",
        qualifier: "home",
        statistics: {
          ball_possession: 48,
          corner_kicks: 5,
          shots_off_target: 5,
          shots_saved: 1,
          fouls: 15,
          free_kicks: 6,
          throw_ins: 11,
          goal_kicks: 5,
          shots_blocked: 2,
          shots_on_target: 2,
          injuries: 2
        },
        players: [
          {
            id: "sr:player:44676",
            name: "Bracalli, Rafael",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:100844",
            name: "Machado, Edu",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:122076",
            name: "Obiora, Nwankwo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:992231",
            name: "Neris",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:778216",
            name: "Talocha, Joao",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1236518",
            name: "Njie, Yusupha",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:759238",
            name: "Sauer, Gustavo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:80500",
            name: "Espinho, Fabio",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:329195",
            name: "Jubal",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:128183",
            name: "Costa, Rafael",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:15540",
            name: "Mateus",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          }
        ]
      },
      {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "BEL",
        qualifier: "away",
        statistics: {
          ball_possession: 52,
          throw_ins: 16,
          goal_kicks: 5,
          shots_on_target: 1,
          free_kicks: 15,
          fouls: 4,
          offsides: 2,
          shots_blocked: 1,
          corner_kicks: 1,
          shots_saved: 2,
          injuries: 2
        },
        players: [
          {
            id: "sr:player:108486",
            name: "Muriel",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:70919",
            name: "Viana, Diogo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:169729",
            name: "Silva, Goncalo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:74071",
            name: "Sasso, Vincent",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:140088",
            name: "Bergdich, Zakarya",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:41649",
            name: "Coelho, Nuno",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:552532",
            name: "Henrique, Eduardo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:59594",
            name: "Santos, Andre",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:268407",
            name: "Lucca, Jonatan",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:799022",
            name: "Velez, Nicolas",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:43735",
            name: "Lica",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          }
        ]
      }
    ]
  }
};

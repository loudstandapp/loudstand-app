export const matchNotStarted = {
  generated_at: "2019-03-31T18:47:05+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/match_summary.json",
  sport_event: {
    id: "sr:match:14946247",
    scheduled: "2019-03-31T19:00:00+00:00",
    start_time_tbd: false,
    tournament_round: {
      type: "group",
      number: 27,
      phase: "main_round_1"
    },
    season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19",
      tournament_id: "sr:tournament:238"
    },
    tournament: {
      id: "sr:tournament:238",
      name: "Primeira Liga",
      sport: {
        id: "sr:sport:1",
        name: "Soccer"
      },
      category: {
        id: "sr:category:44",
        name: "Portugal",
        country_code: "PRT"
      }
    },
    competitors: [
      {
        id: "sr:competitor:2995",
        name: "Boavista FC",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "BOA",
        qualifier: "home"
      },
      {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "BEL",
        qualifier: "away"
      }
    ],
    venue: {
      id: "sr:venue:14556",
      name: "Estadio Do Bessa Xxi",
      capacity: 28263,
      city_name: "Porto",
      country_name: "Portugal",
      map_coordinates: "41.162278,-8.642753",
      country_code: "PRT"
    }
  },
  sport_event_conditions: {
    referee: {
      id: "sr:referee:81446",
      name: "Almeida, Nuno",
      nationality: "Portugal",
      country_code: "PRT"
    },
    venue: {
      id: "sr:venue:14556",
      name: "Estadio Do Bessa Xxi",
      capacity: 28263,
      city_name: "Porto",
      country_name: "Portugal",
      map_coordinates: "41.162278,-8.642753",
      country_code: "PRT"
    }
  },
  sport_event_status: {
    status: "not_started"
  }
};

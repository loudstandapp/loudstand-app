export const standingsForSeason = {
  generated_at: "2019-03-29T16:03:30+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/tournament_standings.json",
  tournament: {
    id: "sr:tournament:238",
    name: "Primeira Liga",
    sport: {
      id: "sr:sport:1",
      name: "Soccer"
    },
    category: {
      id: "sr:category:44",
      name: "Portugal",
      country_code: "PRT"
    },
    current_season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19"
    }
  },
  season: {
    id: "sr:season:55243",
    name: "Primeira Liga 18/19",
    start_date: "2018-08-10",
    end_date: "2019-05-19",
    year: "18/19",
    tournament_id: "sr:tournament:238"
  },
  standings: [
    {
      tie_break_rule:
        "In the event that two (or more) teams have an equal number of points, the following rules break the tie during the season:\r\n1. Goal difference\r\n2. Goals scored\r\nAt the end of the season, the following tie-breaking procedures are used:\r\n1. Head-to-head\r\n2. Goal difference\r\n3. Goals scored",
      type: "total",
      groups: [
        {
          id: "sr:group:34011",
          team_standings: [
            {
              team: {
                id: "sr:competitor:3006",
                name: "Benfica Lisbon"
              },
              rank: 1,
              current_outcome: "Champions League",
              played: 26,
              win: 20,
              draw: 3,
              loss: 3,
              goals_for: 72,
              goals_against: 23,
              goal_diff: 49,
              points: 63,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3002",
                name: "FC Porto"
              },
              rank: 2,
              current_outcome: "Champions League Qualification",
              played: 26,
              win: 20,
              draw: 3,
              loss: 3,
              goals_for: 53,
              goals_against: 15,
              goal_diff: 38,
              points: 63,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:2999",
                name: "Sporting Braga"
              },
              rank: 3,
              current_outcome: "Europa League Qualification",
              played: 26,
              win: 18,
              draw: 4,
              loss: 4,
              goals_for: 44,
              goals_against: 24,
              goal_diff: 20,
              points: 58,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3001",
                name: "Sporting CP"
              },
              rank: 4,
              current_outcome: "Europa League Qualification",
              played: 26,
              win: 17,
              draw: 4,
              loss: 5,
              goals_for: 50,
              goals_against: 27,
              goal_diff: 23,
              points: 55,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3009",
                name: "Vitoria Guimaraes"
              },
              rank: 5,
              played: 26,
              win: 12,
              draw: 6,
              loss: 8,
              goals_for: 31,
              goals_against: 23,
              goal_diff: 8,
              points: 42,
              change: 1
            },
            {
              team: {
                id: "sr:competitor:3014",
                name: "Moreirense FC"
              },
              rank: 6,
              played: 26,
              win: 13,
              draw: 3,
              loss: 10,
              goals_for: 31,
              goals_against: 34,
              goal_diff: -3,
              points: 42,
              change: -1
            },
            {
              team: {
                id: "sr:competitor:3004",
                name: "CF Os Belenenses"
              },
              rank: 7,
              played: 26,
              win: 9,
              draw: 11,
              loss: 6,
              goals_for: 33,
              goals_against: 27,
              goal_diff: 6,
              points: 38,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3011",
                name: "CD Santa Clara"
              },
              rank: 8,
              played: 26,
              win: 9,
              draw: 5,
              loss: 12,
              goals_for: 32,
              goals_against: 33,
              goal_diff: -1,
              points: 32,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3036",
                name: "Rio Ave FC"
              },
              rank: 9,
              played: 26,
              win: 8,
              draw: 8,
              loss: 10,
              goals_for: 36,
              goals_against: 38,
              goal_diff: -2,
              points: 32,
              change: 1
            },
            {
              team: {
                id: "sr:competitor:3026",
                name: "Portimonense SC"
              },
              rank: 10,
              played: 26,
              win: 9,
              draw: 5,
              loss: 12,
              goals_for: 36,
              goals_against: 41,
              goal_diff: -5,
              points: 32,
              change: -1
            },
            {
              team: {
                id: "sr:competitor:3007",
                name: "CS Maritimo Madeira"
              },
              rank: 11,
              played: 26,
              win: 8,
              draw: 3,
              loss: 15,
              goals_for: 16,
              goals_against: 31,
              goal_diff: -15,
              points: 27,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3032",
                name: "CD das Aves"
              },
              rank: 12,
              played: 26,
              win: 7,
              draw: 5,
              loss: 14,
              goals_for: 26,
              goals_against: 39,
              goal_diff: -13,
              points: 26,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:2995",
                name: "BOAVISTA FC"
              },
              rank: 13,
              played: 26,
              win: 7,
              draw: 5,
              loss: 14,
              goals_for: 20,
              goals_against: 34,
              goal_diff: -14,
              points: 26,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3013",
                name: "CD Nacional"
              },
              rank: 14,
              played: 26,
              win: 7,
              draw: 5,
              loss: 14,
              goals_for: 28,
              goals_against: 55,
              goal_diff: -27,
              points: 26,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3008",
                name: "Vitoria Setubal"
              },
              rank: 15,
              played: 26,
              win: 5,
              draw: 10,
              loss: 11,
              goals_for: 20,
              goals_against: 27,
              goal_diff: -7,
              points: 25,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:38396",
                name: "CD Tondela"
              },
              rank: 16,
              current_outcome: "Relegation",
              played: 26,
              win: 6,
              draw: 7,
              loss: 13,
              goals_for: 29,
              goals_against: 40,
              goal_diff: -11,
              points: 25,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3025",
                name: "GD Chaves"
              },
              rank: 17,
              current_outcome: "Relegation",
              played: 26,
              win: 6,
              draw: 6,
              loss: 14,
              goals_for: 19,
              goals_against: 36,
              goal_diff: -17,
              points: 24,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:4501",
                name: "CD Feirense"
              },
              rank: 18,
              current_outcome: "Relegation",
              played: 26,
              win: 2,
              draw: 9,
              loss: 15,
              goals_for: 16,
              goals_against: 45,
              goal_diff: -29,
              points: 15,
              change: 0
            }
          ]
        }
      ]
    },
    {
      tie_break_rule:
        "In the event that two (or more) teams have an equal number of points, the following rules break the tie during the season:\r\n1. Goal difference\r\n2. Goals scored\r\nAt the end of the season, the following tie-breaking procedures are used:\r\n1. Head-to-head\r\n2. Goal difference\r\n3. Goals scored",
      type: "home",
      groups: [
        {
          id: "sr:group:34011",
          team_standings: [
            {
              team: {
                id: "sr:competitor:3001",
                name: "Sporting CP"
              },
              rank: 1,
              played: 14,
              win: 12,
              draw: 1,
              loss: 1,
              goals_for: 32,
              goals_against: 12,
              goal_diff: 20,
              points: 37,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:2999",
                name: "Sporting Braga"
              },
              rank: 2,
              played: 13,
              win: 11,
              draw: 1,
              loss: 1,
              goals_for: 24,
              goals_against: 8,
              goal_diff: 16,
              points: 34,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3002",
                name: "FC Porto"
              },
              rank: 3,
              played: 13,
              win: 11,
              draw: 0,
              loss: 2,
              goals_for: 32,
              goals_against: 8,
              goal_diff: 24,
              points: 33,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3006",
                name: "Benfica Lisbon"
              },
              rank: 4,
              played: 12,
              win: 9,
              draw: 2,
              loss: 1,
              goals_for: 43,
              goals_against: 13,
              goal_diff: 30,
              points: 29,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3009",
                name: "Vitoria Guimaraes"
              },
              rank: 5,
              played: 13,
              win: 8,
              draw: 3,
              loss: 2,
              goals_for: 16,
              goals_against: 7,
              goal_diff: 9,
              points: 27,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3014",
                name: "Moreirense FC"
              },
              rank: 6,
              played: 13,
              win: 7,
              draw: 3,
              loss: 3,
              goals_for: 15,
              goals_against: 13,
              goal_diff: 2,
              points: 24,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3026",
                name: "Portimonense SC"
              },
              rank: 7,
              played: 13,
              win: 6,
              draw: 4,
              loss: 3,
              goals_for: 25,
              goals_against: 17,
              goal_diff: 8,
              points: 22,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3004",
                name: "CF Os Belenenses"
              },
              rank: 8,
              played: 13,
              win: 5,
              draw: 4,
              loss: 4,
              goals_for: 19,
              goals_against: 14,
              goal_diff: 5,
              points: 19,
              change: 1
            },
            {
              team: {
                id: "sr:competitor:2995",
                name: "BOAVISTA FC"
              },
              rank: 9,
              played: 13,
              win: 6,
              draw: 1,
              loss: 6,
              goals_for: 10,
              goals_against: 10,
              goal_diff: 0,
              points: 19,
              change: -1
            },
            {
              team: {
                id: "sr:competitor:3036",
                name: "Rio Ave FC"
              },
              rank: 10,
              played: 13,
              win: 4,
              draw: 5,
              loss: 4,
              goals_for: 20,
              goals_against: 20,
              goal_diff: 0,
              points: 17,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3013",
                name: "CD Nacional"
              },
              rank: 11,
              played: 14,
              win: 5,
              draw: 2,
              loss: 7,
              goals_for: 12,
              goals_against: 17,
              goal_diff: -5,
              points: 17,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3032",
                name: "CD das Aves"
              },
              rank: 12,
              played: 14,
              win: 4,
              draw: 3,
              loss: 7,
              goals_for: 16,
              goals_against: 19,
              goal_diff: -3,
              points: 15,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:38396",
                name: "CD Tondela"
              },
              rank: 13,
              played: 13,
              win: 4,
              draw: 3,
              loss: 6,
              goals_for: 12,
              goals_against: 17,
              goal_diff: -5,
              points: 15,
              change: 3
            },
            {
              team: {
                id: "sr:competitor:3008",
                name: "Vitoria Setubal"
              },
              rank: 14,
              played: 13,
              win: 3,
              draw: 5,
              loss: 5,
              goals_for: 10,
              goals_against: 11,
              goal_diff: -1,
              points: 14,
              change: -1
            },
            {
              team: {
                id: "sr:competitor:3011",
                name: "CD Santa Clara"
              },
              rank: 15,
              played: 13,
              win: 4,
              draw: 2,
              loss: 7,
              goals_for: 18,
              goals_against: 21,
              goal_diff: -3,
              points: 14,
              change: -1
            },
            {
              team: {
                id: "sr:competitor:3025",
                name: "GD Chaves"
              },
              rank: 16,
              played: 13,
              win: 3,
              draw: 5,
              loss: 5,
              goals_for: 12,
              goals_against: 15,
              goal_diff: -3,
              points: 14,
              change: -1
            },
            {
              team: {
                id: "sr:competitor:3007",
                name: "CS Maritimo Madeira"
              },
              rank: 17,
              played: 12,
              win: 4,
              draw: 2,
              loss: 6,
              goals_for: 9,
              goals_against: 14,
              goal_diff: -5,
              points: 14,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:4501",
                name: "CD Feirense"
              },
              rank: 18,
              played: 12,
              win: 1,
              draw: 5,
              loss: 6,
              goals_for: 12,
              goals_against: 19,
              goal_diff: -7,
              points: 8,
              change: 0
            }
          ]
        }
      ]
    },
    {
      tie_break_rule:
        "In the event that two (or more) teams have an equal number of points, the following rules break the tie during the season:\r\n1. Goal difference\r\n2. Goals scored\r\nAt the end of the season, the following tie-breaking procedures are used:\r\n1. Head-to-head\r\n2. Goal difference\r\n3. Goals scored",
      type: "away",
      groups: [
        {
          id: "sr:group:34011",
          team_standings: [
            {
              team: {
                id: "sr:competitor:3006",
                name: "Benfica Lisbon"
              },
              rank: 1,
              played: 14,
              win: 11,
              draw: 1,
              loss: 2,
              goals_for: 29,
              goals_against: 10,
              goal_diff: 19,
              points: 34,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3002",
                name: "FC Porto"
              },
              rank: 2,
              played: 13,
              win: 9,
              draw: 3,
              loss: 1,
              goals_for: 21,
              goals_against: 7,
              goal_diff: 14,
              points: 30,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:2999",
                name: "Sporting Braga"
              },
              rank: 3,
              played: 13,
              win: 7,
              draw: 3,
              loss: 3,
              goals_for: 20,
              goals_against: 16,
              goal_diff: 4,
              points: 24,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3004",
                name: "CF Os Belenenses"
              },
              rank: 4,
              played: 13,
              win: 4,
              draw: 7,
              loss: 2,
              goals_for: 14,
              goals_against: 13,
              goal_diff: 1,
              points: 19,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3001",
                name: "Sporting CP"
              },
              rank: 5,
              played: 12,
              win: 5,
              draw: 3,
              loss: 4,
              goals_for: 18,
              goals_against: 15,
              goal_diff: 3,
              points: 18,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3011",
                name: "CD Santa Clara"
              },
              rank: 6,
              played: 13,
              win: 5,
              draw: 3,
              loss: 5,
              goals_for: 14,
              goals_against: 12,
              goal_diff: 2,
              points: 18,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3014",
                name: "Moreirense FC"
              },
              rank: 7,
              played: 13,
              win: 6,
              draw: 0,
              loss: 7,
              goals_for: 16,
              goals_against: 21,
              goal_diff: -5,
              points: 18,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3009",
                name: "Vitoria Guimaraes"
              },
              rank: 8,
              played: 13,
              win: 4,
              draw: 3,
              loss: 6,
              goals_for: 15,
              goals_against: 16,
              goal_diff: -1,
              points: 15,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3036",
                name: "Rio Ave FC"
              },
              rank: 9,
              played: 13,
              win: 4,
              draw: 3,
              loss: 6,
              goals_for: 16,
              goals_against: 18,
              goal_diff: -2,
              points: 15,
              change: 1
            },
            {
              team: {
                id: "sr:competitor:3007",
                name: "CS Maritimo Madeira"
              },
              rank: 10,
              played: 14,
              win: 4,
              draw: 1,
              loss: 9,
              goals_for: 7,
              goals_against: 17,
              goal_diff: -10,
              points: 13,
              change: -1
            },
            {
              team: {
                id: "sr:competitor:3008",
                name: "Vitoria Setubal"
              },
              rank: 11,
              played: 13,
              win: 2,
              draw: 5,
              loss: 6,
              goals_for: 10,
              goals_against: 16,
              goal_diff: -6,
              points: 11,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3032",
                name: "CD das Aves"
              },
              rank: 12,
              played: 12,
              win: 3,
              draw: 2,
              loss: 7,
              goals_for: 10,
              goals_against: 20,
              goal_diff: -10,
              points: 11,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:38396",
                name: "CD Tondela"
              },
              rank: 13,
              played: 13,
              win: 2,
              draw: 4,
              loss: 7,
              goals_for: 17,
              goals_against: 23,
              goal_diff: -6,
              points: 10,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3026",
                name: "Portimonense SC"
              },
              rank: 14,
              played: 13,
              win: 3,
              draw: 1,
              loss: 9,
              goals_for: 11,
              goals_against: 24,
              goal_diff: -13,
              points: 10,
              change: 0
            },
            {
              team: {
                id: "sr:competitor:3025",
                name: "GD Chaves"
              },
              rank: 15,
              played: 13,
              win: 3,
              draw: 1,
              loss: 9,
              goals_for: 7,
              goals_against: 21,
              goal_diff: -14,
              points: 10,
              change: 2
            },
            {
              team: {
                id: "sr:competitor:3013",
                name: "CD Nacional"
              },
              rank: 16,
              played: 12,
              win: 2,
              draw: 3,
              loss: 7,
              goals_for: 16,
              goals_against: 38,
              goal_diff: -22,
              points: 9,
              change: -1
            },
            {
              team: {
                id: "sr:competitor:2995",
                name: "BOAVISTA FC"
              },
              rank: 17,
              played: 13,
              win: 1,
              draw: 4,
              loss: 8,
              goals_for: 10,
              goals_against: 24,
              goal_diff: -14,
              points: 7,
              change: -1
            },
            {
              team: {
                id: "sr:competitor:4501",
                name: "CD Feirense"
              },
              rank: 18,
              played: 14,
              win: 1,
              draw: 4,
              loss: 9,
              goals_for: 4,
              goals_against: 26,
              goal_diff: -22,
              points: 7,
              change: 0
            }
          ]
        }
      ]
    }
  ]
};

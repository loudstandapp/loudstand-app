export const summary = {
  generated_at: "2019-03-29T16:12:36+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/match_summary.json",
  sport_event: {
    id: "sr:match:14946187",
    scheduled: "2019-03-02T15:30:00+00:00",
    start_time_tbd: false,
    tournament_round: {
      type: "group",
      number: 24,
      phase: "main_round_1"
    },
    season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19",
      tournament_id: "sr:tournament:238"
    },
    tournament: {
      id: "sr:tournament:238",
      name: "Primeira Liga",
      sport: {
        id: "sr:sport:1",
        name: "Soccer"
      },
      category: {
        id: "sr:category:44",
        name: "Portugal",
        country_code: "PRT"
      }
    },
    competitors: [
      {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "CDN",
        qualifier: "home"
      },
      {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "TON",
        qualifier: "away"
      }
    ],
    venue: {
      id: "sr:venue:1335",
      name: "Estadio da Madeira",
      capacity: 5132,
      city_name: "Funchal",
      country_name: "Portugal",
      map_coordinates: "32.670834,-16.883333",
      country_code: "PRT"
    }
  },
  sport_event_conditions: {
    referee: {
      id: "sr:referee:89559",
      name: "Malheiro, Helder",
      nationality: "Portugal",
      country_code: "PRT"
    },
    venue: {
      id: "sr:venue:1335",
      name: "Estadio da Madeira",
      capacity: 5132,
      city_name: "Funchal",
      country_name: "Portugal",
      map_coordinates: "32.670834,-16.883333",
      country_code: "PRT"
    },
    weather_info: {
      pitch: "medium",
      weather_conditions: "medium"
    }
  },
  sport_event_status: {
    status: "closed",
    match_status: "ended",
    home_score: 3,
    away_score: 2,
    winner_id: "sr:competitor:3013",
    period_scores: [
      {
        home_score: 1,
        away_score: 1,
        type: "regular_period",
        number: 1
      },
      {
        home_score: 2,
        away_score: 1,
        type: "regular_period",
        number: 2
      }
    ]
  },
  statistics: {
    teams: [
      {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN",
        qualifier: "home",
        statistics: {
          ball_possession: 51,
          throw_ins: 23,
          offsides: 3,
          free_kicks: 26,
          goal_kicks: 10,
          corner_kicks: 8,
          shots_off_target: 8,
          shots_saved: 2,
          fouls: 10,
          shots_on_target: 5,
          shots_blocked: 2,
          injuries: 4,
          yellow_cards: 2
        },
        players: [
          {
            id: "sr:player:329103",
            name: "Guimaraes, Daniel",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:868640",
            name: "Kalindi",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 1,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1142164",
            name: "Julio Cesar",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:177175",
            name: "Rosic, Lazar",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:871486",
            name: "Cerqueira, Mauro",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:170483",
            name: "Goncalves, Vitor",
            nickname: "Vitinha",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:267461",
            name: "Palocevic, Aleksandar",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:12676",
            name: "Tissone, Fernando",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:791645",
            name: "Witi",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 1,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:328335",
            name: "Rashidov, Sardor",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:537040",
            name: "Riascos, Brayan",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1003011",
            name: "Hamzaoui, Okacha",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:546558",
            name: "Rochez, Bryan",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:202084",
            name: "Kaka",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          }
        ]
      },
      {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "TON",
        qualifier: "away",
        statistics: {
          ball_possession: 49,
          free_kicks: 13,
          fouls: 22,
          throw_ins: 23,
          goal_kicks: 12,
          offsides: 4,
          corner_kicks: 7,
          shots_off_target: 6,
          shots_on_target: 4,
          shots_saved: 2,
          shots_blocked: 3,
          injuries: 3,
          yellow_cards: 4,
          red_cards: 17
        },
        players: [
          {
            id: "sr:player:116372",
            name: "Claudio Ramos",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:862680",
            name: "Moufi, Fahd",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:252717",
            name: "Alves, Ricardo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:5883",
            name: "Costa, Ricardo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:156319",
            name: "Joaozinho",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 1,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:845913",
            name: "Murillo, Jhon",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:61180",
            name: "Monteiro, Bruno",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:856980",
            name: "Jaquite, Joao",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:343957",
            name: "Pite",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:251217",
            name: "Pedro, Joao",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:126076",
            name: "Tomane",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 1,
            penalty_goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:339695",
            name: "Delgado, Juan",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:343487",
            name: "Reis, Joao",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:247903",
            name: "Arango, Chicho",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          }
        ]
      }
    ]
  }
};

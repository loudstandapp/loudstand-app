export const summary = {
  generated_at: "2019-04-05T19:12:55+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/match_summary.json",
  sport_event: {
    id: "sr:match:14946279",
    scheduled: "2019-04-12T19:30:00+00:00",
    start_time_tbd: false,
    tournament_round: {
      type: "group",
      number: 29,
      phase: "main_round_1"
    },
    season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19",
      tournament_id: "sr:tournament:238"
    },
    tournament: {
      id: "sr:tournament:238",
      name: "Primeira Liga",
      sport: {
        id: "sr:sport:1",
        name: "Soccer"
      },
      category: {
        id: "sr:category:44",
        name: "Portugal",
        country_code: "PRT"
      }
    },
    competitors: [
      {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "CHA",
        qualifier: "home"
      },
      {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "BEL",
        qualifier: "away"
      }
    ],
    venue: {
      id: "sr:venue:12071",
      name: "Estadio Municipal Eng. Manuel Branco Teixeira",
      capacity: 9000,
      city_name: "Chaves",
      country_name: "Portugal",
      map_coordinates: "41.750552,-7.464952",
      country_code: "PRT"
    }
  },
  sport_event_conditions: {
    venue: {
      id: "sr:venue:12071",
      name: "Estadio Municipal Eng. Manuel Branco Teixeira",
      capacity: 9000,
      city_name: "Chaves",
      country_name: "Portugal",
      map_coordinates: "41.750552,-7.464952",
      country_code: "PRT"
    }
  },
  sport_event_status: {
    status: "not_started"
  }
};

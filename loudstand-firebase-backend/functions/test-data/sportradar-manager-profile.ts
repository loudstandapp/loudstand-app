export const managerProfile = {
  generated_at: "2019-04-12T13:39:26+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/player_profile.json",
  player: {
    id: "sr:player:1512453",
    name: "Lage, Bruno",
    first_name: "Bruno",
    last_name: "Lage",
    date_of_birth: "1976-05-12",
    nationality: "Portugal",
    country_code: "PRT",
    gender: "male"
  },
  teams: [
    {
      id: "sr:competitor:3006",
      name: "Benfica Lisbon",
      country: "Portugal",
      country_code: "PRT",
      abbreviation: "BEN"
    }
  ],
  roles: [
    {
      type: "manager",
      active: "true",
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "BEN"
      },
      start_date: "2019-01-04"
    },
    {
      type: "manager",
      active: "false",
      team: {
        id: "sr:competitor:74465",
        name: "Benfica Lisbon B",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "BEB"
      },
      start_date: "2018-07-09",
      end_date: "2019-01-03"
    }
  ]
};

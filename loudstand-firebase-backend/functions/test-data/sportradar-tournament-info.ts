export const tournamentInfo = {
  generated_at: "2019-03-30T17:21:16+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/tournament_info.json",
  tournament: {
    id: "sr:tournament:238",
    name: "Primeira Liga",
    sport: {
      id: "sr:sport:1",
      name: "Soccer"
    },
    category: {
      id: "sr:category:44",
      name: "Portugal",
      country_code: "PRT"
    },
    current_season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19"
    }
  },
  season: {
    id: "sr:season:55243",
    name: "Primeira Liga 18/19",
    start_date: "2018-08-10",
    end_date: "2019-05-19",
    year: "18/19",
    tournament_id: "sr:tournament:238"
  },
  round: {
    type: "group",
    number: 27
  },
  season_coverage_info: {
    season_id: "sr:season:55243",
    scheduled: 306,
    played: 235,
    max_coverage_level: "gold",
    max_covered: 235,
    min_coverage_level: "gold"
  },
  coverage_info: {
    live_coverage: true
  },
  groups: [
    {
      teams: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA"
        }
      ]
    }
  ]
};

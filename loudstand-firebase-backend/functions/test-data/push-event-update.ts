export const pushEventUpdate = {
  payload: {
    sport_event_status: {
      status: "live",
      match_status: "1st_half",
      home_score: 1,
      away_score: 0,
      period: 1
    },
    event: {
      id: 538846896,
      type: "score_change",
      time: "2019-03-29T11:54:45+00:00",
      match_time: 20,
      match_clock: "19:16",
      team: "home",
      x: 86,
      y: 71,
      period: 1,
      period_type: "regular_period",
      sport_event_id: "sr:match:17369511",
      home_score: 1,
      away_score: 0,
      goal_scorer: { id: "sr:player:560860", name: "Muzepper, Mirahmetjan" }
    }
  },
  metadata: {
    format: "json",
    tournament_id: "sr:tournament:649",
    sport_event_id: "sr:match:17369511",
    event_id: "score_change",
    channel: "soccer-global-dfc",
    sport_id: "sr:sport:1",
    season_id: "sr:season:63691"
  }
};

export const tournamentSchedule = {
  generated_at: "2019-03-29T15:09:14+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/tournament_schedule.json",
  tournament: {
    id: "sr:tournament:238",
    name: "Primeira Liga",
    sport: {
      id: "sr:sport:1",
      name: "Soccer"
    },
    category: {
      id: "sr:category:44",
      name: "Portugal",
      country_code: "PRT"
    }
  },
  sport_events: [
    {
      id: "sr:match:14944793",
      scheduled: "2018-08-10T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 1,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14944811",
      scheduled: "2018-08-11T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 1,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14944813",
      scheduled: "2018-08-11T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 1,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14944819",
      scheduled: "2018-08-11T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 1,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14944807",
      scheduled: "2018-08-12T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 1,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14944821",
      scheduled: "2018-08-12T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 1,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14944809",
      scheduled: "2018-08-12T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 1,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14944805",
      scheduled: "2018-08-12T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 1,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14944815",
      scheduled: "2018-08-13T19:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 1,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945793",
      scheduled: "2018-08-17T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 2,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945797",
      scheduled: "2018-08-18T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 2,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945799",
      scheduled: "2018-08-18T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 2,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945787",
      scheduled: "2018-08-18T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 2,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945783",
      scheduled: "2018-08-19T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 2,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945785",
      scheduled: "2018-08-19T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 2,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945791",
      scheduled: "2018-08-19T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 2,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945795",
      scheduled: "2018-08-19T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 2,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945789",
      scheduled: "2018-08-20T19:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 2,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945811",
      scheduled: "2018-08-24T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 3,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945805",
      scheduled: "2018-08-25T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 3,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945815",
      scheduled: "2018-08-25T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 3,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945801",
      scheduled: "2018-08-25T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 3,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945803",
      scheduled: "2018-08-26T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 3,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945817",
      scheduled: "2018-08-26T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 3,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945807",
      scheduled: "2018-08-26T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 3,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945809",
      scheduled: "2018-08-26T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 3,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945813",
      scheduled: "2018-08-27T19:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 3,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945819",
      scheduled: "2018-08-31T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 4,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945835",
      scheduled: "2018-08-31T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 4,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945831",
      scheduled: "2018-09-01T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 4,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945827",
      scheduled: "2018-09-01T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 4,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945821",
      scheduled: "2018-09-01T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 4,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945825",
      scheduled: "2018-09-02T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 4,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945833",
      scheduled: "2018-09-02T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 4,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945829",
      scheduled: "2018-09-02T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 4,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945823",
      scheduled: "2018-09-02T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 4,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945851",
      scheduled: "2018-09-21T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 5,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945849",
      scheduled: "2018-09-22T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 5,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945843",
      scheduled: "2018-09-22T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 5,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945841",
      scheduled: "2018-09-22T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 5,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945839",
      scheduled: "2018-09-23T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 5,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945853",
      scheduled: "2018-09-23T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 5,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945847",
      scheduled: "2018-09-23T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 5,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945837",
      scheduled: "2018-09-23T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 5,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945845",
      scheduled: "2018-09-24T19:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 5,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945863",
      scheduled: "2018-09-27T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 6,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945865",
      scheduled: "2018-09-28T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 6,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945859",
      scheduled: "2018-09-29T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 6,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945855",
      scheduled: "2018-09-29T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 6,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945861",
      scheduled: "2018-09-29T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 6,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945857",
      scheduled: "2018-09-30T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 6,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945871",
      scheduled: "2018-09-30T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 6,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945867",
      scheduled: "2018-09-30T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 6,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945869",
      scheduled: "2018-10-01T19:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 6,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945879",
      scheduled: "2018-10-05T14:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 7,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945873",
      scheduled: "2018-10-05T17:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 7,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945881",
      scheduled: "2018-10-05T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 7,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945877",
      scheduled: "2018-10-06T14:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 7,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945875",
      scheduled: "2018-10-06T17:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 7,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945885",
      scheduled: "2018-10-06T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 7,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945883",
      scheduled: "2018-10-07T14:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 7,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945887",
      scheduled: "2018-10-07T16:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 7,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945889",
      scheduled: "2018-10-07T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 7,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945907",
      scheduled: "2018-10-26T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 8,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945899",
      scheduled: "2018-10-26T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 8,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945905",
      scheduled: "2018-10-27T14:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 8,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945891",
      scheduled: "2018-10-27T17:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 8,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945903",
      scheduled: "2018-10-27T19:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 8,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945895",
      scheduled: "2018-10-28T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 8,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945897",
      scheduled: "2018-10-28T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 8,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945893",
      scheduled: "2018-10-28T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 8,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945901",
      scheduled: "2018-10-28T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 8,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945923",
      scheduled: "2018-11-02T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 9,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945911",
      scheduled: "2018-11-03T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 9,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945915",
      scheduled: "2018-11-03T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 9,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945917",
      scheduled: "2018-11-03T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 9,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945919",
      scheduled: "2018-11-04T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 9,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945925",
      scheduled: "2018-11-04T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 9,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945921",
      scheduled: "2018-11-04T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 9,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945913",
      scheduled: "2018-11-05T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 9,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945909",
      scheduled: "2018-11-05T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 9,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945943",
      scheduled: "2018-11-09T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 10,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945931",
      scheduled: "2018-11-09T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 10,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945939",
      scheduled: "2018-11-10T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 10,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945941",
      scheduled: "2018-11-10T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 10,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945933",
      scheduled: "2018-11-10T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 10,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945935",
      scheduled: "2018-11-10T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 10,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945929",
      scheduled: "2018-11-11T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 10,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945937",
      scheduled: "2018-11-11T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 10,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945927",
      scheduled: "2018-11-11T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 10,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945951",
      scheduled: "2018-11-30T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 11,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945945",
      scheduled: "2018-12-01T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 11,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945957",
      scheduled: "2018-12-01T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 11,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945955",
      scheduled: "2018-12-01T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 11,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945947",
      scheduled: "2018-12-02T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 11,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945959",
      scheduled: "2018-12-02T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 11,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945953",
      scheduled: "2018-12-02T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 11,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945961",
      scheduled: "2018-12-02T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 11,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945949",
      scheduled: "2018-12-03T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 11,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945973",
      scheduled: "2018-12-07T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 12,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945977",
      scheduled: "2018-12-08T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 12,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945967",
      scheduled: "2018-12-08T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 12,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945963",
      scheduled: "2018-12-08T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 12,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945965",
      scheduled: "2018-12-09T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 12,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945969",
      scheduled: "2018-12-09T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 12,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945979",
      scheduled: "2018-12-09T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 12,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945971",
      scheduled: "2018-12-09T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 12,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945975",
      scheduled: "2018-12-10T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 12,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945981",
      scheduled: "2018-12-14T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 13,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945997",
      scheduled: "2018-12-14T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 13,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945991",
      scheduled: "2018-12-15T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 13,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945993",
      scheduled: "2018-12-15T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 13,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945995",
      scheduled: "2018-12-15T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 13,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945989",
      scheduled: "2018-12-15T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 13,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945983",
      scheduled: "2018-12-16T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 13,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945985",
      scheduled: "2018-12-16T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 13,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945987",
      scheduled: "2018-12-16T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 13,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946003",
      scheduled: "2018-12-22T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 14,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946005",
      scheduled: "2018-12-22T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 14,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946011",
      scheduled: "2018-12-22T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 14,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946007",
      scheduled: "2018-12-22T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 14,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14945999",
      scheduled: "2018-12-22T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 14,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946001",
      scheduled: "2018-12-23T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 14,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946009",
      scheduled: "2018-12-23T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 14,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946013",
      scheduled: "2018-12-23T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 14,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946015",
      scheduled: "2018-12-23T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 14,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946025",
      scheduled: "2019-01-02T16:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 15,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946021",
      scheduled: "2019-01-02T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 15,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946029",
      scheduled: "2019-01-02T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 15,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946033",
      scheduled: "2019-01-02T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 15,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946019",
      scheduled: "2019-01-02T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 15,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946023",
      scheduled: "2019-01-02T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 15,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946027",
      scheduled: "2019-01-03T16:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 15,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946017",
      scheduled: "2019-01-03T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 15,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946031",
      scheduled: "2019-01-03T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 15,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946051",
      scheduled: "2019-01-05T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 16,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946035",
      scheduled: "2019-01-06T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 16,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946037",
      scheduled: "2019-01-06T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 16,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946049",
      scheduled: "2019-01-06T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 16,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946047",
      scheduled: "2019-01-06T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 16,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946043",
      scheduled: "2019-01-06T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 16,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946045",
      scheduled: "2019-01-07T17:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 16,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946041",
      scheduled: "2019-01-07T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 16,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946039",
      scheduled: "2019-01-07T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 16,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946057",
      scheduled: "2019-01-10T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 17,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946055",
      scheduled: "2019-01-11T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 17,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946067",
      scheduled: "2019-01-11T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 17,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946063",
      scheduled: "2019-01-11T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 17,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946061",
      scheduled: "2019-01-12T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 17,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946065",
      scheduled: "2019-01-12T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 17,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946053",
      scheduled: "2019-01-13T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 17,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946069",
      scheduled: "2019-01-13T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 17,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946059",
      scheduled: "2019-01-13T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 17,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946085",
      scheduled: "2019-01-18T18:45:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 18,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946073",
      scheduled: "2019-01-18T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 18,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946081",
      scheduled: "2019-01-18T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 18,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946077",
      scheduled: "2019-01-19T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 18,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946087",
      scheduled: "2019-01-19T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 18,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946075",
      scheduled: "2019-01-19T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 18,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946079",
      scheduled: "2019-01-20T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 18,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946071",
      scheduled: "2019-01-20T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 18,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946083",
      scheduled: "2019-01-20T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 18,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946089",
      scheduled: "2019-01-28T17:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 19,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946091",
      scheduled: "2019-01-28T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 19,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946099",
      scheduled: "2019-01-28T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 19,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946095",
      scheduled: "2019-01-28T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 19,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946101",
      scheduled: "2019-01-29T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 19,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946105",
      scheduled: "2019-01-29T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 19,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946097",
      scheduled: "2019-01-29T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 19,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946093",
      scheduled: "2019-01-30T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 19,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946103",
      scheduled: "2019-01-30T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 19,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946111",
      scheduled: "2019-02-01T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 20,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946115",
      scheduled: "2019-02-02T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 20,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946123",
      scheduled: "2019-02-02T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 20,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946121",
      scheduled: "2019-02-02T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 20,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946109",
      scheduled: "2019-02-03T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 20,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946113",
      scheduled: "2019-02-03T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 20,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946117",
      scheduled: "2019-02-03T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 20,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946107",
      scheduled: "2019-02-03T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 20,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946119",
      scheduled: "2019-02-04T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 20,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946129",
      scheduled: "2019-02-08T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 21,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946139",
      scheduled: "2019-02-09T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 21,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946133",
      scheduled: "2019-02-09T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 21,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946141",
      scheduled: "2019-02-09T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 21,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946125",
      scheduled: "2019-02-10T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 21,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946131",
      scheduled: "2019-02-10T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 21,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946135",
      scheduled: "2019-02-10T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 21,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946127",
      scheduled: "2019-02-10T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 21,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946137",
      scheduled: "2019-02-11T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 21,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946155",
      scheduled: "2019-02-15T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 22,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946159",
      scheduled: "2019-02-16T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 22,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946143",
      scheduled: "2019-02-16T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 22,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946147",
      scheduled: "2019-02-16T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 22,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946145",
      scheduled: "2019-02-17T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 22,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946149",
      scheduled: "2019-02-17T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 22,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946153",
      scheduled: "2019-02-17T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 22,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946157",
      scheduled: "2019-02-17T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 22,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946151",
      scheduled: "2019-02-18T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 22,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946173",
      scheduled: "2019-02-22T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 23,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946171",
      scheduled: "2019-02-22T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 23,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946165",
      scheduled: "2019-02-23T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 23,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946175",
      scheduled: "2019-02-23T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 23,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946177",
      scheduled: "2019-02-23T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 23,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946163",
      scheduled: "2019-02-24T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 23,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946161",
      scheduled: "2019-02-24T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 23,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946169",
      scheduled: "2019-02-25T19:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 23,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946167",
      scheduled: "2019-02-25T21:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 23,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946189",
      scheduled: "2019-03-01T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 24,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946187",
      scheduled: "2019-03-02T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 24,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946183",
      scheduled: "2019-03-02T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 24,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946193",
      scheduled: "2019-03-02T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 24,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946179",
      scheduled: "2019-03-03T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 24,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946185",
      scheduled: "2019-03-03T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 24,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946191",
      scheduled: "2019-03-03T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 24,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946195",
      scheduled: "2019-03-03T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 24,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946181",
      scheduled: "2019-03-04T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 24,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946197",
      scheduled: "2019-03-08T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 25,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946201",
      scheduled: "2019-03-09T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 25,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946205",
      scheduled: "2019-03-09T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 25,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946207",
      scheduled: "2019-03-09T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 25,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946211",
      scheduled: "2019-03-10T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 25,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946213",
      scheduled: "2019-03-10T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 25,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946203",
      scheduled: "2019-03-10T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 25,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946199",
      scheduled: "2019-03-10T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 25,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946209",
      scheduled: "2019-03-11T20:15:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 25,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946231",
      scheduled: "2019-03-15T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 26,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946219",
      scheduled: "2019-03-16T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 26,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946223",
      scheduled: "2019-03-16T15:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 26,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946227",
      scheduled: "2019-03-16T18:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 26,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946217",
      scheduled: "2019-03-16T20:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 26,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946215",
      scheduled: "2019-03-17T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 26,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946221",
      scheduled: "2019-03-17T15:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 26,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946229",
      scheduled: "2019-03-17T17:30:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 26,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946225",
      scheduled: "2019-03-17T20:00:00+00:00",
      start_time_tbd: false,
      status: "closed",
      tournament_round: {
        type: "group",
        number: 26,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946245",
      scheduled: "2019-03-29T20:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 27,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946241",
      scheduled: "2019-03-30T15:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 27,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946239",
      scheduled: "2019-03-30T18:00:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 27,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946233",
      scheduled: "2019-03-30T20:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 27,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946249",
      scheduled: "2019-03-30T20:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 27,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946237",
      scheduled: "2019-03-31T14:00:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 27,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946243",
      scheduled: "2019-03-31T16:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 27,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946247",
      scheduled: "2019-03-31T19:00:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 27,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946235",
      scheduled: "2019-04-01T19:15:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 27,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946263",
      scheduled: "2019-04-05T19:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 28,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946255",
      scheduled: "2019-04-06T14:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 28,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946261",
      scheduled: "2019-04-06T14:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 28,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946259",
      scheduled: "2019-04-06T17:00:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 28,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946267",
      scheduled: "2019-04-06T19:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 28,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946257",
      scheduled: "2019-04-07T14:00:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 28,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946265",
      scheduled: "2019-04-07T16:30:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 28,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946253",
      scheduled: "2019-04-07T19:00:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 28,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946251",
      scheduled: "2019-04-08T19:15:00+00:00",
      start_time_tbd: false,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 28,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946269",
      scheduled: "2019-04-14T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 29,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946271",
      scheduled: "2019-04-14T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 29,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946273",
      scheduled: "2019-04-14T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 29,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946275",
      scheduled: "2019-04-14T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 29,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946277",
      scheduled: "2019-04-14T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 29,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946279",
      scheduled: "2019-04-14T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 29,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946281",
      scheduled: "2019-04-14T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 29,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946283",
      scheduled: "2019-04-14T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 29,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946285",
      scheduled: "2019-04-14T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 29,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946287",
      scheduled: "2019-04-20T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 30,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946289",
      scheduled: "2019-04-20T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 30,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946291",
      scheduled: "2019-04-20T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 30,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946293",
      scheduled: "2019-04-20T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 30,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946295",
      scheduled: "2019-04-20T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 30,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946297",
      scheduled: "2019-04-20T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 30,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946299",
      scheduled: "2019-04-20T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 30,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946301",
      scheduled: "2019-04-20T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 30,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946303",
      scheduled: "2019-04-20T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 30,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946305",
      scheduled: "2019-04-28T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 31,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946307",
      scheduled: "2019-04-28T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 31,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946309",
      scheduled: "2019-04-28T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 31,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946311",
      scheduled: "2019-04-28T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 31,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946313",
      scheduled: "2019-04-28T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 31,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946315",
      scheduled: "2019-04-28T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 31,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946317",
      scheduled: "2019-04-28T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 31,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946319",
      scheduled: "2019-04-28T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 31,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946321",
      scheduled: "2019-04-28T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 31,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946323",
      scheduled: "2019-05-05T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 32,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946325",
      scheduled: "2019-05-05T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 32,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946327",
      scheduled: "2019-05-05T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 32,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946329",
      scheduled: "2019-05-05T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 32,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946331",
      scheduled: "2019-05-05T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 32,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946333",
      scheduled: "2019-05-05T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 32,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946335",
      scheduled: "2019-05-05T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 32,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946337",
      scheduled: "2019-05-05T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 32,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946339",
      scheduled: "2019-05-05T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 32,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946341",
      scheduled: "2019-05-12T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 33,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:12071",
        name: "Estadio Municipal Eng. Manuel Branco Teixeira",
        capacity: 9000,
        city_name: "Chaves",
        country_name: "Portugal",
        map_coordinates: "41.750552,-7.464952",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946343",
      scheduled: "2019-05-12T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 33,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1335",
        name: "Estadio da Madeira",
        capacity: 5132,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.670834,-16.883333",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946345",
      scheduled: "2019-05-12T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 33,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "home"
        },
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:910",
        name: "Jose Alvalade",
        capacity: 50044,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.761246,-9.160833",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946347",
      scheduled: "2019-05-12T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 33,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:14556",
        name: "Estadio Do Bessa Xxi",
        capacity: 28263,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.162278,-8.642753",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946349",
      scheduled: "2019-05-12T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 33,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5318",
        name: "Estadio de Sao Miguel",
        capacity: 13277,
        city_name: "Ponta Delgada",
        country_name: "Portugal",
        map_coordinates: "37.763708,-25.622589",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946351",
      scheduled: "2019-05-12T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 33,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8249",
        name: "Estadio Do CD Das Aves",
        capacity: 8560,
        city_name: "Vila das Aves",
        country_name: "Portugal",
        map_coordinates: "41.366881,-8.411667",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946353",
      scheduled: "2019-05-12T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 33,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2013",
        name: "Estadio Dos Arcos",
        capacity: 9065,
        city_name: "Vila do Conde",
        country_name: "Portugal",
        map_coordinates: "41.362755,-8.740186",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946355",
      scheduled: "2019-05-12T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 33,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1137",
        name: "Dom Afonso Henriques",
        capacity: 30008,
        city_name: "Guimaraes",
        country_name: "Portugal",
        map_coordinates: "41.445885,-8.301164",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946357",
      scheduled: "2019-05-12T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 33,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2090",
        name: "Estadio Municipal de Portimao",
        capacity: 9544,
        city_name: "Portimao",
        country_name: "Portugal",
        map_coordinates: "37.135692,-8.539892",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946359",
      scheduled: "2019-05-19T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 34,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3011",
          name: "CD Santa Clara",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SCL",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:902",
        name: "Estadio do Sport Lisboa e Benfica",
        capacity: 64642,
        city_name: "Lisbon",
        country_name: "Portugal",
        map_coordinates: "38.752670,-9.184697",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946361",
      scheduled: "2019-05-19T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 34,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:2999",
          name: "Sporting Braga",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BRA",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3026",
          name: "Portimonense SC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:951",
        name: "Braga Municipal",
        capacity: 30286,
        city_name: "Braga",
        country_name: "Portugal",
        map_coordinates: "41.562565,-8.430133",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946363",
      scheduled: "2019-05-19T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 34,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3002",
          name: "FC Porto",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "POR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3001",
          name: "Sporting CP",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SPO",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:907",
        name: "Estadio do Dragao",
        capacity: 50033,
        city_name: "Porto",
        country_name: "Portugal",
        map_coordinates: "41.161758,-8.583933",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946365",
      scheduled: "2019-05-19T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 34,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3004",
          name: "CF Os Belenenses",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEL",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3013",
          name: "CD Nacional",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CDN",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2375",
        name: "Estadio Nacional Do Jamor",
        capacity: 37593,
        city_name: "Oeiras",
        country_name: "Portugal",
        map_coordinates: "38.708817,-9.260842",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946367",
      scheduled: "2019-05-19T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 34,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:38396",
          name: "CD Tondela",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "TON",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3025",
          name: "GD Chaves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "CHA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:8243",
        name: "Estadio Joao Cardoso",
        capacity: 5000,
        city_name: "Tondela",
        country_name: "Portugal",
        map_coordinates: "40.512127,-8.082650",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946369",
      scheduled: "2019-05-19T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 34,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:4501",
          name: "CD Feirense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "FEI",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3032",
          name: "CD das Aves",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "AVE",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2555",
        name: "Estadio Marcolino de Castro",
        capacity: 5600,
        city_name: "Santa Maria da Feira",
        country_name: "Portugal",
        map_coordinates: "40.926689,-8.545814",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946371",
      scheduled: "2019-05-19T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 34,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3007",
          name: "CS Maritimo Madeira",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MAR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:2995",
          name: "Boavista FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BOA",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:1178",
        name: "Estadio do Maritimo",
        capacity: 10932,
        city_name: "Funchal",
        country_name: "Portugal",
        map_coordinates: "32.645527,-16.928366",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946373",
      scheduled: "2019-05-19T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 34,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3014",
          name: "Moreirense FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "MOR",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3009",
          name: "Vitoria Guimaraes",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "VIT",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:5312",
        name: "Parque de Jogos Comendador Joaquim de Almeida Freitas",
        capacity: 6153,
        city_name: "Moreira de Conegos",
        country_name: "Portugal",
        map_coordinates: "41.378107,-8.354548",
        country_code: "PRT"
      }
    },
    {
      id: "sr:match:14946375",
      scheduled: "2019-05-19T15:00:00+00:00",
      start_time_tbd: true,
      status: "not_started",
      tournament_round: {
        type: "group",
        number: 34,
        phase: "main_round_1"
      },
      season: {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        start_date: "2018-08-10",
        end_date: "2019-05-19",
        year: "18/19",
        tournament_id: "sr:tournament:238"
      },
      tournament: {
        id: "sr:tournament:238",
        name: "Primeira Liga",
        sport: {
          id: "sr:sport:1",
          name: "Soccer"
        },
        category: {
          id: "sr:category:44",
          name: "Portugal",
          country_code: "PRT"
        }
      },
      competitors: [
        {
          id: "sr:competitor:3008",
          name: "Vitoria Setubal",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "SET",
          qualifier: "home"
        },
        {
          id: "sr:competitor:3036",
          name: "Rio Ave FC",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "RAV",
          qualifier: "away"
        }
      ],
      venue: {
        id: "sr:venue:2091",
        name: "Estadio Do Bonfim",
        capacity: 18694,
        city_name: "Setubal",
        country_name: "Portugal",
        map_coordinates: "38.531387,-8.891111",
        country_code: "PRT"
      }
    }
  ]
};

export const leadingPlayers = {
  generated_at: "2019-04-12T12:22:03+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/tournament_leaders.json",
  tournament: {
    id: "sr:tournament:238",
    name: "Primeira Liga",
    sport: {
      id: "sr:sport:1",
      name: "Soccer"
    },
    category: {
      id: "sr:category:44",
      name: "Portugal",
      country_code: "PRT"
    },
    current_season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19"
    }
  },
  season_coverage_info: {
    season_id: "sr:season:55243",
    scheduled: 306,
    played: 252,
    max_coverage_level: "gold",
    max_covered: 252,
    min_coverage_level: "gold"
  },
  top_points: [
    {
      goals: 15,
      assists: 10,
      rank: 1,
      player: {
        id: "sr:player:288205",
        name: "Fernandes, Bruno"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 18,
      assists: 6,
      rank: 2,
      player: {
        id: "sr:player:50899",
        name: "Seferovic, Haris"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 9,
      assists: 15,
      rank: 2,
      player: {
        id: "sr:player:77725",
        name: "Pizzi"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 14,
      assists: 4,
      rank: 4,
      player: {
        id: "sr:player:128360",
        name: "Sousa, Dyego"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 14,
      assists: 1,
      rank: 5,
      player: {
        id: "sr:player:32260",
        name: "Dost, Bas"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 13,
      assists: 2,
      rank: 5,
      player: {
        id: "sr:player:800432",
        name: "Soares"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 10,
      assists: 5,
      rank: 5,
      player: {
        id: "sr:player:1050565",
        name: "Joao Felix"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 7,
      assists: 6,
      rank: 8,
      player: {
        id: "sr:player:584124",
        name: "Chiquinho"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 10,
      assists: 2,
      rank: 9,
      player: {
        id: "sr:player:126076",
        name: "Tomane"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 4,
      assists: 8,
      rank: 9,
      player: {
        id: "sr:player:177177",
        name: "Grimaldo, Alex"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 7,
      assists: 4,
      rank: 11,
      player: {
        id: "sr:player:17740",
        name: "Nani"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 10,
      assists: 1,
      rank: 11,
      player: {
        id: "sr:player:47906",
        name: "Jonas"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 9,
      assists: 2,
      rank: 11,
      player: {
        id: "sr:player:105404",
        name: "Eduardo, Wilson"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 10,
      assists: 1,
      rank: 11,
      player: {
        id: "sr:player:252577",
        name: "Rafa Silva"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 7,
      assists: 4,
      rank: 11,
      player: {
        id: "sr:player:298300",
        name: "Marega, Moussa"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 10,
      assists: 1,
      rank: 11,
      player: {
        id: "sr:player:546558",
        name: "Rochez, Bryan"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 8,
      assists: 2,
      rank: 17,
      player: {
        id: "sr:player:39066",
        name: "Brahimi, Yacine"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 8,
      assists: 2,
      rank: 17,
      player: {
        id: "sr:player:43735",
        name: "Lica"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 2,
      assists: 8,
      rank: 17,
      player: {
        id: "sr:player:44636",
        name: "Almeida, Andre"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 8,
      assists: 2,
      rank: 17,
      player: {
        id: "sr:player:46819",
        name: "Martinez, Jackson"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 6,
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:78611",
        name: "Fredy"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 5,
      assists: 5,
      rank: 17,
      player: {
        id: "sr:player:1013263",
        name: "Galeno, Wenderson"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 3,
      assists: 6,
      rank: 23,
      player: {
        id: "sr:player:244801",
        name: "Otavio"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 6,
      assists: 3,
      rank: 23,
      player: {
        id: "sr:player:299914",
        name: "Guedes, Alexandre"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 3,
      assists: 6,
      rank: 23,
      player: {
        id: "sr:player:312110",
        name: "Telles, Alex"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 7,
      assists: 2,
      rank: 23,
      player: {
        id: "sr:player:314380",
        name: "Horta, Ricardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 7,
      assists: 2,
      rank: 23,
      player: {
        id: "sr:player:557166",
        name: "Balde, Mama"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 6,
      assists: 2,
      rank: 28,
      player: {
        id: "sr:player:110954",
        name: "Toze"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 7,
      rank: 28,
      player: {
        id: "sr:player:123906",
        name: "Corona, Jesus"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 8,
      assists: 0,
      rank: 28,
      player: {
        id: "sr:player:141021",
        name: "Ze Manuel"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 5,
      assists: 3,
      rank: 28,
      player: {
        id: "sr:player:155825",
        name: "Nakajima, Shoya"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 6,
      assists: 2,
      rank: 28,
      player: {
        id: "sr:player:243479",
        name: "Paulinho"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 4,
      assists: 4,
      rank: 28,
      player: {
        id: "sr:player:252087",
        name: "Arsenio"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 3,
      assists: 5,
      rank: 28,
      player: {
        id: "sr:player:252707",
        name: "Xavier"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 8,
      assists: 0,
      rank: 28,
      player: {
        id: "sr:player:332257",
        name: "Cadiz, Jhonder"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 4,
      assists: 4,
      rank: 28,
      player: {
        id: "sr:player:902400",
        name: "Davidson"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 6,
      assists: 2,
      rank: 28,
      player: {
        id: "sr:player:1013433",
        name: "Tavares, Heriberto"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 8,
      assists: 0,
      rank: 28,
      player: {
        id: "sr:player:1233356",
        name: "Vinicius, Carlos"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 6,
      assists: 1,
      rank: 39,
      player: {
        id: "sr:player:59777",
        name: "Andre Andre"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 5,
      assists: 2,
      rank: 39,
      player: {
        id: "sr:player:156709",
        name: "Rashid, Osama"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 6,
      assists: 1,
      rank: 39,
      player: {
        id: "sr:player:317677",
        name: "Joao Camacho"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 5,
      assists: 2,
      rank: 39,
      player: {
        id: "sr:player:339695",
        name: "Delgado, Juan"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 2,
      assists: 5,
      rank: 39,
      player: {
        id: "sr:player:791645",
        name: "Witi"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 2,
      assists: 5,
      rank: 39,
      player: {
        id: "sr:player:796044",
        name: "Bessa, Eber"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 3,
      assists: 4,
      rank: 39,
      player: {
        id: "sr:player:869378",
        name: "Soares, Rodrigo"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 4,
      assists: 3,
      rank: 39,
      player: {
        id: "sr:player:930466",
        name: "Dala, Gelson"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 2,
      assists: 4,
      rank: 47,
      player: {
        id: "sr:player:34257",
        name: "Claudemir"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 5,
      assists: 1,
      rank: 47,
      player: {
        id: "sr:player:59784",
        name: "Moreira, Bruno"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 2,
      assists: 4,
      rank: 47,
      player: {
        id: "sr:player:89566",
        name: "Costa, Edgar"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 0,
      assists: 6,
      rank: 47,
      player: {
        id: "sr:player:170117",
        name: "Sequeira, Nuno"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 3,
      assists: 3,
      rank: 47,
      player: {
        id: "sr:player:243575",
        name: "Mattheus"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 3,
      assists: 3,
      rank: 47,
      player: {
        id: "sr:player:309358",
        name: "Mendy, Frederic"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 5,
      rank: 47,
      player: {
        id: "sr:player:578560",
        name: "Acuna, Marcos"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 5,
      assists: 1,
      rank: 47,
      player: {
        id: "sr:player:599698",
        name: "Tagueu, Joel"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 4,
      assists: 1,
      rank: 55,
      player: {
        id: "sr:player:15540",
        name: "Mateus"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 5,
      assists: 0,
      rank: 55,
      player: {
        id: "sr:player:39402",
        name: "Henrique Almeida"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 1,
      assists: 4,
      rank: 55,
      player: {
        id: "sr:player:70919",
        name: "Viana, Diogo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 2,
      assists: 3,
      rank: 55,
      player: {
        id: "sr:player:108398",
        name: "Samaris, Andreas"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 2,
      assists: 3,
      rank: 55,
      player: {
        id: "sr:player:115718",
        name: "Diaby, Abdoulay"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 4,
      assists: 1,
      rank: 55,
      player: {
        id: "sr:player:116869",
        name: "Aboubakar, Vincent"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 2,
      assists: 3,
      rank: 55,
      player: {
        id: "sr:player:170005",
        name: "Sturgeon, Fabio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 4,
      assists: 1,
      rank: 55,
      player: {
        id: "sr:player:204790",
        name: "Herrera, Hector"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 4,
      assists: 1,
      rank: 55,
      player: {
        id: "sr:player:205790",
        name: "Paulinho"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 4,
      rank: 55,
      player: {
        id: "sr:player:233370",
        name: "Novais, Joao"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      assists: 3,
      rank: 55,
      player: {
        id: "sr:player:252095",
        name: "Silva, Tiago"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 5,
      assists: 0,
      rank: 55,
      player: {
        id: "sr:player:252815",
        name: "Cardoso, Fabio"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 4,
      assists: 1,
      rank: 55,
      player: {
        id: "sr:player:268821",
        name: "Andrade, Fernando"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 3,
      assists: 2,
      rank: 55,
      player: {
        id: "sr:player:343685",
        name: "Derley"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 3,
      assists: 2,
      rank: 55,
      player: {
        id: "sr:player:584434",
        name: "Nuno, Pedro"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 2,
      assists: 3,
      rank: 55,
      player: {
        id: "sr:player:802944",
        name: "Lamas, Bruno"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 4,
      assists: 1,
      rank: 55,
      player: {
        id: "sr:player:1397359",
        name: "Kikas"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 0,
      assists: 4,
      rank: 72,
      player: {
        id: "sr:player:25994",
        name: "Coentrao, Fabio"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 4,
      assists: 0,
      rank: 72,
      player: {
        id: "sr:player:33520",
        name: "Edinho"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 1,
      assists: 3,
      rank: 72,
      player: {
        id: "sr:player:38152",
        name: "Renan Bressan"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      assists: 2,
      rank: 72,
      player: {
        id: "sr:player:44684",
        name: "Aurelio, Joao"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 3,
      assists: 1,
      rank: 72,
      player: {
        id: "sr:player:44739",
        name: "Gomes, Vitor"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 1,
      assists: 3,
      rank: 72,
      player: {
        id: "sr:player:80500",
        name: "Espinho, Fabio"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 2,
      assists: 2,
      rank: 72,
      player: {
        id: "sr:player:129615",
        name: "Nildo Petrolina"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 3,
      assists: 1,
      rank: 72,
      player: {
        id: "sr:player:133828",
        name: "Texeira, David"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 0,
      assists: 4,
      rank: 72,
      player: {
        id: "sr:player:190137",
        name: "Soares, Rafa"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 0,
      assists: 4,
      rank: 72,
      player: {
        id: "sr:player:231764",
        name: "Jota"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 3,
      assists: 1,
      rank: 72,
      player: {
        id: "sr:player:268407",
        name: "Lucca, Jonatan"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 2,
      assists: 2,
      rank: 72,
      player: {
        id: "sr:player:344205",
        name: "Manafa, Wilson"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 4,
      assists: 0,
      rank: 72,
      player: {
        id: "sr:player:352880",
        name: "Pereira, Hildeberto"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 3,
      rank: 72,
      player: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 2,
      assists: 2,
      rank: 72,
      player: {
        id: "sr:player:557008",
        name: "Cervi, Franco"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 3,
      assists: 1,
      rank: 72,
      player: {
        id: "sr:player:611880",
        name: "Murilo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      assists: 2,
      rank: 72,
      player: {
        id: "sr:player:776747",
        name: "Boa Morte, Aylton"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 1,
      assists: 3,
      rank: 72,
      player: {
        id: "sr:player:795906",
        name: "Pena, Sergio"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 3,
      assists: 1,
      rank: 72,
      player: {
        id: "sr:player:846327",
        name: "Pinho, Rodrigo"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 0,
      assists: 4,
      rank: 72,
      player: {
        id: "sr:player:963143",
        name: "Vieira, Patrick"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 2,
      assists: 2,
      rank: 72,
      player: {
        id: "sr:player:1013609",
        name: "Cabral, Jovane"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 3,
      assists: 1,
      rank: 72,
      player: {
        id: "sr:player:1239936",
        name: "Gabrielzinho"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:27879",
        name: "Ghazaryan, Gevorg"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:39640",
        name: "Gallo, Bruno"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:39981",
        name: "Montero, Fredy"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:44685",
        name: "Nene"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 1,
      assists: 2,
      rank: 94,
      player: {
        id: "sr:player:47334",
        name: "Eduardo Salvio"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 0,
      assists: 3,
      rank: 94,
      player: {
        id: "sr:player:73146",
        name: "Ristovski, Stefan"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:106116",
        name: "Falcone, Federico"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:128183",
        name: "Costa, Rafael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:133736",
        name: "Lopes, Rafael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:173833",
        name: "Keita, Alhassane"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 0,
      assists: 3,
      rank: 94,
      player: {
        id: "sr:player:188133",
        name: "Zivkovic, Andrija"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:243167",
        name: "Dener"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:244591",
        name: "Schmidt, Joao"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:254569",
        name: "Diego Lopes"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:291707",
        name: "Luiz Phellype"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:318941",
        name: "Dias, Ruben"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:328335",
        name: "Rashidov, Sardor"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      assists: 2,
      rank: 94,
      player: {
        id: "sr:player:552532",
        name: "Henrique, Eduardo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:584116",
        name: "Ponck, Carlos"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 1,
      assists: 2,
      rank: 94,
      player: {
        id: "sr:player:589546",
        name: "Pineda, Denis"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:777111",
        name: "Silva, Amilton"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:816572",
        name: "Loum, Mamadou"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:844297",
        name: "Niltinho"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 0,
      assists: 3,
      rank: 94,
      player: {
        id: "sr:player:853560",
        name: "Reis, Matheus"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 1,
      assists: 2,
      rank: 94,
      player: {
        id: "sr:player:868640",
        name: "Kalindi"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:918726",
        name: "Militao, Eder"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:960304",
        name: "Carvalho, Wellington"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 1,
      assists: 2,
      rank: 94,
      player: {
        id: "sr:player:1011951",
        name: "Tabata, Bruno"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 3,
      assists: 0,
      rank: 94,
      player: {
        id: "sr:player:1402615",
        name: "Andre Luis"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      assists: 1,
      rank: 94,
      player: {
        id: "sr:player:1548707",
        name: "Santana, Thiago"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:928",
        name: "Defendi, Rodrigo"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:3179",
        name: "Mathieu, Jeremy"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:5883",
        name: "Costa, Ricardo"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:5884",
        name: "Pepe"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:44683",
        name: "Micael, Ruben"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:44696",
        name: "Pinto, Nuno"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:47614",
        name: "Valente, Nuno"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:59843",
        name: "Zequinha"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:61180",
        name: "Monteiro, Bruno"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:78378",
        name: "Jardel"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:84847",
        name: "Fransergio"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:96840",
        name: "Oliveira, Sergio"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:99545",
        name: "Pereira, Danilo"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:100844",
        name: "Machado, Edu"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:105457",
        name: "Junior, Zainadine"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:125017",
        name: "Wakaso, Alhassan"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:125886",
        name: "Valencia, Jose"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:128207",
        name: "Barrera, Leandro"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:135904",
        name: "John, Ola"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:155629",
        name: "Briseno, Antonio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:155855",
        name: "Stephens, Alfredo"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:156319",
        name: "Joaozinho"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:165703",
        name: "Oliveira, William"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:170483",
        name: "Goncalves, Vitor"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:175097",
        name: "Correa, Jorge"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:226428",
        name: "Mikel Agu"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:231776",
        name: "Torres, Oliver"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:241466",
        name: "Welthon"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:242211",
        name: "Felipe"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:270291",
        name: "Marcelo Goiano"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:295331",
        name: "Chrien, Martin"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:318943",
        name: "Macedo, Ruben"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:344199",
        name: "Semedo, Ruben"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:357734",
        name: "Ferro"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:364612",
        name: "Palhinha, Joao"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:364778",
        name: "Pedrao"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:553678",
        name: "Jeronimo, Ronan"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:583826",
        name: "Aouacheria, Bilel"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:589020",
        name: "Carraca"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:592588",
        name: "Ewerton"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:605442",
        name: "Fernandes, Ivanildo"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:605956",
        name: "Fernandes, Gedson"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:777641",
        name: "Pereira, Andre"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:778216",
        name: "Talocha, Joao"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:868620",
        name: "Jadson"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:956430",
        name: "Raphinha"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:981333",
        name: "Fernandes, Lucas"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:995487",
        name: "Marcao"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:1003011",
        name: "Hamzaoui, Okacha"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:1004865",
        name: "Costa, Vitor"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:1008537",
        name: "Getterson"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 0,
      assists: 2,
      rank: 125,
      player: {
        id: "sr:player:1017831",
        name: "Freitas, Murilo"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:1059983",
        name: "Schettine, Guilherme"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:1145114",
        name: "Pablo Santos"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 1,
      rank: 125,
      player: {
        id: "sr:player:1248458",
        name: "Tormena, Vitor"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:352874",
        name: "Rochinha"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 2,
      assists: 0,
      rank: 125,
      player: {
        id: "sr:player:871372",
        name: "Costinha"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:12676",
        name: "Tissone, Fernando"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:18159",
        name: "Semedo, Jose"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:18984",
        name: "Adrian Lopez"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:26179",
        name: "Pereira, Maxi"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:41649",
        name: "Coelho, Nuno"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:43736",
        name: "Santos, Cris"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:43779",
        name: "Bueno, Alberto"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:44640",
        name: "Mano"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:47443",
        name: "Coates, Sebastian"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:54672",
        name: "Simao, David"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:59653",
        name: "Nelson Lenho"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:59772",
        name: "Lima, Ruben"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:74071",
        name: "Sasso, Vincent"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:79064",
        name: "Silva, Joao"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:96846",
        name: "Bruno, David"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:96850",
        name: "Claro, Andre"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:96888",
        name: "Galo, Diego"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:97000",
        name: "Esgaio, Ricardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:101156",
        name: "Maras, Nikola"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:129724",
        name: "Ferreyra, Facundo"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:134761",
        name: "Babanco"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:137979",
        name: "Martins, Luis"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:138899",
        name: "Teixeira, Joao"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:143587",
        name: "Hernani"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:144421",
        name: "Bruno, Vitor"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:150506",
        name: "Drame, Ousmane"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:169729",
        name: "Silva, Goncalo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:170129",
        name: "Hanin, Florent"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:178407",
        name: "Appelt, Gabriel"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:206770",
        name: "Carvalho, Anderson"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:247903",
        name: "Arango, Chicho"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:249411",
        name: "Farina, Luis"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:252665",
        name: "Ricardo Valente"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:252819",
        name: "Gaspar, Bruno"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:261669",
        name: "Farias, Edson"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:267461",
        name: "Palocevic, Aleksandar"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:295157",
        name: "Eduardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:304264",
        name: "Perdigao"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:329107",
        name: "Cesar"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:329583",
        name: "Celis, Guillermo"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:343965",
        name: "Nanu"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:357728",
        name: "Pepe"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:358552",
        name: "Indio, Matheus"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:551812",
        name: "Raul Silva"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:555624",
        name: "Carvalho, Carlos"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:584158",
        name: "Afonso, Joao"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:588996",
        name: "Sampaio, Philipe"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:777105",
        name: "Sa, Pedro"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:794848",
        name: "Lucas, Joao"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:803656",
        name: "Osorio, Yordan"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:841307",
        name: "Gorre, Kenji"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:843509",
        name: "Sacko, Falaye"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:862680",
        name: "Moufi, Fahd"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:869302",
        name: "Viana, Bruno"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:871358",
        name: "Elhouni, Hamdou"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:885100",
        name: "Singh, Luther"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:891520",
        name: "Nadjack"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:925770",
        name: "Silva, Bruno"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:936362",
        name: "Leite, Diogo"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:936366",
        name: "Morris Luis, Florentino Ibrain"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:936368",
        name: "Luis, Miguel"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:978073",
        name: "Xadas, Bruno"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:986201",
        name: "D'Alberto, Anthony"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:992231",
        name: "Neris"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:1005267",
        name: "Jean Cleber"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1011533",
        name: "Petkov, Stivan"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:1067538",
        name: "Mendes, Joao"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1107923",
        name: "Sabbag, Pablo"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1121457",
        name: "Wendel"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1236518",
        name: "Njie, Yusupha"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1242060",
        name: "Patrick"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1360800",
        name: "Furtado, Damien"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1408953",
        name: "Fellipe, Jorge"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1433867",
        name: "Carlos, Joao"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1514065",
        name: "Africo, Lucas"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 1,
      assists: 0,
      rank: 183,
      player: {
        id: "sr:player:1537087",
        name: "Mouandilmadji, Marius"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:1549507",
        name: "Ruster"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 0,
      assists: 1,
      rank: 183,
      player: {
        id: "sr:player:1658049",
        name: "Pelagio, Pedro"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    }
  ],
  top_goals: [
    {
      goals: 18,
      rank: 1,
      player: {
        id: "sr:player:50899",
        name: "Seferovic, Haris"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 15,
      rank: 2,
      player: {
        id: "sr:player:288205",
        name: "Fernandes, Bruno"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 14,
      rank: 3,
      player: {
        id: "sr:player:128360",
        name: "Sousa, Dyego"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 14,
      rank: 3,
      player: {
        id: "sr:player:32260",
        name: "Dost, Bas"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 13,
      rank: 5,
      player: {
        id: "sr:player:800432",
        name: "Soares"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 10,
      rank: 6,
      player: {
        id: "sr:player:1050565",
        name: "Joao Felix"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 10,
      rank: 6,
      player: {
        id: "sr:player:126076",
        name: "Tomane"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 10,
      rank: 6,
      player: {
        id: "sr:player:47906",
        name: "Jonas"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 10,
      rank: 6,
      player: {
        id: "sr:player:252577",
        name: "Rafa Silva"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 10,
      rank: 6,
      player: {
        id: "sr:player:546558",
        name: "Rochez, Bryan"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 9,
      rank: 11,
      player: {
        id: "sr:player:77725",
        name: "Pizzi"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 9,
      rank: 11,
      player: {
        id: "sr:player:105404",
        name: "Eduardo, Wilson"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 8,
      rank: 13,
      player: {
        id: "sr:player:39066",
        name: "Brahimi, Yacine"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 8,
      rank: 13,
      player: {
        id: "sr:player:43735",
        name: "Lica"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 8,
      rank: 13,
      player: {
        id: "sr:player:46819",
        name: "Martinez, Jackson"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 8,
      rank: 13,
      player: {
        id: "sr:player:141021",
        name: "Ze Manuel"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 8,
      rank: 13,
      player: {
        id: "sr:player:332257",
        name: "Cadiz, Jhonder"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 8,
      rank: 13,
      player: {
        id: "sr:player:1233356",
        name: "Vinicius, Carlos"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 7,
      rank: 19,
      player: {
        id: "sr:player:584124",
        name: "Chiquinho"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 7,
      rank: 19,
      player: {
        id: "sr:player:17740",
        name: "Nani"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 7,
      rank: 19,
      player: {
        id: "sr:player:298300",
        name: "Marega, Moussa"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 7,
      rank: 19,
      player: {
        id: "sr:player:314380",
        name: "Horta, Ricardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 7,
      rank: 19,
      player: {
        id: "sr:player:557166",
        name: "Balde, Mama"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 6,
      rank: 24,
      player: {
        id: "sr:player:78611",
        name: "Fredy"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 6,
      rank: 24,
      player: {
        id: "sr:player:299914",
        name: "Guedes, Alexandre"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 6,
      rank: 24,
      player: {
        id: "sr:player:110954",
        name: "Toze"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 6,
      rank: 24,
      player: {
        id: "sr:player:243479",
        name: "Paulinho"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 6,
      rank: 24,
      player: {
        id: "sr:player:1013433",
        name: "Tavares, Heriberto"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 6,
      rank: 24,
      player: {
        id: "sr:player:59777",
        name: "Andre Andre"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 6,
      rank: 24,
      player: {
        id: "sr:player:317677",
        name: "Joao Camacho"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 5,
      rank: 31,
      player: {
        id: "sr:player:1013263",
        name: "Galeno, Wenderson"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 5,
      rank: 31,
      player: {
        id: "sr:player:155825",
        name: "Nakajima, Shoya"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 5,
      rank: 31,
      player: {
        id: "sr:player:156709",
        name: "Rashid, Osama"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 5,
      rank: 31,
      player: {
        id: "sr:player:339695",
        name: "Delgado, Juan"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 5,
      rank: 31,
      player: {
        id: "sr:player:59784",
        name: "Moreira, Bruno"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 5,
      rank: 31,
      player: {
        id: "sr:player:599698",
        name: "Tagueu, Joel"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 5,
      rank: 31,
      player: {
        id: "sr:player:39402",
        name: "Henrique Almeida"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 5,
      rank: 31,
      player: {
        id: "sr:player:252815",
        name: "Cardoso, Fabio"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:177177",
        name: "Grimaldo, Alex"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:252087",
        name: "Arsenio"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:902400",
        name: "Davidson"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:930466",
        name: "Dala, Gelson"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:15540",
        name: "Mateus"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:116869",
        name: "Aboubakar, Vincent"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:204790",
        name: "Herrera, Hector"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:205790",
        name: "Paulinho"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:268821",
        name: "Andrade, Fernando"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:1397359",
        name: "Kikas"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:33520",
        name: "Edinho"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 4,
      rank: 39,
      player: {
        id: "sr:player:352880",
        name: "Pereira, Hildeberto"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:244801",
        name: "Otavio"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:312110",
        name: "Telles, Alex"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:252707",
        name: "Xavier"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:869378",
        name: "Soares, Rodrigo"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:243575",
        name: "Mattheus"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:309358",
        name: "Mendy, Frederic"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:343685",
        name: "Derley"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:584434",
        name: "Nuno, Pedro"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:44739",
        name: "Gomes, Vitor"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:133828",
        name: "Texeira, David"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:268407",
        name: "Lucca, Jonatan"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:611880",
        name: "Murilo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:846327",
        name: "Pinho, Rodrigo"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:1239936",
        name: "Gabrielzinho"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:44685",
        name: "Nene"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:106116",
        name: "Falcone, Federico"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:173833",
        name: "Keita, Alhassane"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:244591",
        name: "Schmidt, Joao"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:291707",
        name: "Luiz Phellype"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:328335",
        name: "Rashidov, Sardor"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:816572",
        name: "Loum, Mamadou"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:844297",
        name: "Niltinho"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:918726",
        name: "Militao, Eder"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:960304",
        name: "Carvalho, Wellington"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 3,
      rank: 51,
      player: {
        id: "sr:player:1402615",
        name: "Andre Luis"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:44636",
        name: "Almeida, Andre"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:791645",
        name: "Witi"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:796044",
        name: "Bessa, Eber"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:34257",
        name: "Claudemir"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:89566",
        name: "Costa, Edgar"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:108398",
        name: "Samaris, Andreas"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:115718",
        name: "Diaby, Abdoulay"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:170005",
        name: "Sturgeon, Fabio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:252095",
        name: "Silva, Tiago"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:802944",
        name: "Lamas, Bruno"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:44684",
        name: "Aurelio, Joao"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:129615",
        name: "Nildo Petrolina"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:344205",
        name: "Manafa, Wilson"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:557008",
        name: "Cervi, Franco"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:776747",
        name: "Boa Morte, Aylton"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:1013609",
        name: "Cabral, Jovane"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:27879",
        name: "Ghazaryan, Gevorg"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:39640",
        name: "Gallo, Bruno"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:39981",
        name: "Montero, Fredy"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:128183",
        name: "Costa, Rafael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:133736",
        name: "Lopes, Rafael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:243167",
        name: "Dener"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:254569",
        name: "Diego Lopes"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:318941",
        name: "Dias, Ruben"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:584116",
        name: "Ponck, Carlos"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:777111",
        name: "Silva, Amilton"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:1548707",
        name: "Santana, Thiago"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:928",
        name: "Defendi, Rodrigo"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:3179",
        name: "Mathieu, Jeremy"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:5884",
        name: "Pepe"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:61180",
        name: "Monteiro, Bruno"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:78378",
        name: "Jardel"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:125017",
        name: "Wakaso, Alhassan"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:125886",
        name: "Valencia, Jose"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:128207",
        name: "Barrera, Leandro"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:155629",
        name: "Briseno, Antonio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:170483",
        name: "Goncalves, Vitor"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:175097",
        name: "Correa, Jorge"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:344199",
        name: "Semedo, Ruben"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:357734",
        name: "Ferro"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:364778",
        name: "Pedrao"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:778216",
        name: "Talocha, Joao"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:868620",
        name: "Jadson"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:956430",
        name: "Raphinha"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:995487",
        name: "Marcao"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:1003011",
        name: "Hamzaoui, Okacha"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:1008537",
        name: "Getterson"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:1059983",
        name: "Schettine, Guilherme"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:1145114",
        name: "Pablo Santos"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:352874",
        name: "Rochinha"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 2,
      rank: 77,
      player: {
        id: "sr:player:871372",
        name: "Costinha"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:123906",
        name: "Corona, Jesus"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:578560",
        name: "Acuna, Marcos"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:70919",
        name: "Viana, Diogo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:233370",
        name: "Novais, Joao"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:38152",
        name: "Renan Bressan"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:80500",
        name: "Espinho, Fabio"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:795906",
        name: "Pena, Sergio"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:47334",
        name: "Eduardo Salvio"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:552532",
        name: "Henrique, Eduardo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:589546",
        name: "Pineda, Denis"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:868640",
        name: "Kalindi"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1011951",
        name: "Tabata, Bruno"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:5883",
        name: "Costa, Ricardo"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:47614",
        name: "Valente, Nuno"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:59843",
        name: "Zequinha"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:96840",
        name: "Oliveira, Sergio"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:99545",
        name: "Pereira, Danilo"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:105457",
        name: "Junior, Zainadine"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:135904",
        name: "John, Ola"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:165703",
        name: "Oliveira, William"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:226428",
        name: "Mikel Agu"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:231776",
        name: "Torres, Oliver"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:241466",
        name: "Welthon"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:242211",
        name: "Felipe"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:270291",
        name: "Marcelo Goiano"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:295331",
        name: "Chrien, Martin"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:364612",
        name: "Palhinha, Joao"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:553678",
        name: "Jeronimo, Ronan"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:583826",
        name: "Aouacheria, Bilel"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:592588",
        name: "Ewerton"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:605442",
        name: "Fernandes, Ivanildo"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:777641",
        name: "Pereira, Andre"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:981333",
        name: "Fernandes, Lucas"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1004865",
        name: "Costa, Vitor"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1248458",
        name: "Tormena, Vitor"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:12676",
        name: "Tissone, Fernando"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:18159",
        name: "Semedo, Jose"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:18984",
        name: "Adrian Lopez"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:41649",
        name: "Coelho, Nuno"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:43736",
        name: "Santos, Cris"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:43779",
        name: "Bueno, Alberto"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:47443",
        name: "Coates, Sebastian"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:54672",
        name: "Simao, David"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:74071",
        name: "Sasso, Vincent"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:79064",
        name: "Silva, Joao"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:96846",
        name: "Bruno, David"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:96850",
        name: "Claro, Andre"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:96888",
        name: "Galo, Diego"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:101156",
        name: "Maras, Nikola"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:129724",
        name: "Ferreyra, Facundo"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:137979",
        name: "Martins, Luis"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:143587",
        name: "Hernani"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:247903",
        name: "Arango, Chicho"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:249411",
        name: "Farina, Luis"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:252665",
        name: "Ricardo Valente"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:252819",
        name: "Gaspar, Bruno"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:267461",
        name: "Palocevic, Aleksandar"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:304264",
        name: "Perdigao"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:329107",
        name: "Cesar"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:329583",
        name: "Celis, Guillermo"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:358552",
        name: "Indio, Matheus"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:551812",
        name: "Raul Silva"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:555624",
        name: "Carvalho, Carlos"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:588996",
        name: "Sampaio, Philipe"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:803656",
        name: "Osorio, Yordan"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:841307",
        name: "Gorre, Kenji"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:862680",
        name: "Moufi, Fahd"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:885100",
        name: "Singh, Luther"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:936362",
        name: "Leite, Diogo"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:936366",
        name: "Morris Luis, Florentino Ibrain"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:936368",
        name: "Luis, Miguel"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:992231",
        name: "Neris"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1011533",
        name: "Petkov, Stivan"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1107923",
        name: "Sabbag, Pablo"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1121457",
        name: "Wendel"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1236518",
        name: "Njie, Yusupha"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1242060",
        name: "Patrick"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1360800",
        name: "Furtado, Damien"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1408953",
        name: "Fellipe, Jorge"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1433867",
        name: "Carlos, Joao"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1514065",
        name: "Africo, Lucas"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      goals: 1,
      rank: 128,
      player: {
        id: "sr:player:1537087",
        name: "Mouandilmadji, Marius"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    }
  ],
  top_assists: [
    {
      assists: 15,
      rank: 1,
      player: {
        id: "sr:player:77725",
        name: "Pizzi"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 10,
      rank: 2,
      player: {
        id: "sr:player:288205",
        name: "Fernandes, Bruno"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      assists: 8,
      rank: 3,
      player: {
        id: "sr:player:177177",
        name: "Grimaldo, Alex"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 8,
      rank: 3,
      player: {
        id: "sr:player:44636",
        name: "Almeida, Andre"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 7,
      rank: 5,
      player: {
        id: "sr:player:123906",
        name: "Corona, Jesus"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 6,
      rank: 6,
      player: {
        id: "sr:player:50899",
        name: "Seferovic, Haris"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 6,
      rank: 6,
      player: {
        id: "sr:player:584124",
        name: "Chiquinho"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 6,
      rank: 6,
      player: {
        id: "sr:player:244801",
        name: "Otavio"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 6,
      rank: 6,
      player: {
        id: "sr:player:312110",
        name: "Telles, Alex"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 6,
      rank: 6,
      player: {
        id: "sr:player:170117",
        name: "Sequeira, Nuno"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 5,
      rank: 11,
      player: {
        id: "sr:player:1050565",
        name: "Joao Felix"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 5,
      rank: 11,
      player: {
        id: "sr:player:1013263",
        name: "Galeno, Wenderson"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 5,
      rank: 11,
      player: {
        id: "sr:player:252707",
        name: "Xavier"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      assists: 5,
      rank: 11,
      player: {
        id: "sr:player:791645",
        name: "Witi"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      assists: 5,
      rank: 11,
      player: {
        id: "sr:player:796044",
        name: "Bessa, Eber"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      assists: 5,
      rank: 11,
      player: {
        id: "sr:player:578560",
        name: "Acuna, Marcos"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:128360",
        name: "Sousa, Dyego"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:17740",
        name: "Nani"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:298300",
        name: "Marega, Moussa"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:78611",
        name: "Fredy"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:252087",
        name: "Arsenio"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:902400",
        name: "Davidson"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:869378",
        name: "Soares, Rodrigo"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:34257",
        name: "Claudemir"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:89566",
        name: "Costa, Edgar"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:70919",
        name: "Viana, Diogo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:233370",
        name: "Novais, Joao"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:25994",
        name: "Coentrao, Fabio"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:190137",
        name: "Soares, Rafa"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:231764",
        name: "Jota"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      assists: 4,
      rank: 17,
      player: {
        id: "sr:player:963143",
        name: "Vieira, Patrick"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:299914",
        name: "Guedes, Alexandre"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:155825",
        name: "Nakajima, Shoya"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:930466",
        name: "Dala, Gelson"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:243575",
        name: "Mattheus"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:309358",
        name: "Mendy, Frederic"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:108398",
        name: "Samaris, Andreas"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:115718",
        name: "Diaby, Abdoulay"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:170005",
        name: "Sturgeon, Fabio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:252095",
        name: "Silva, Tiago"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:802944",
        name: "Lamas, Bruno"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:38152",
        name: "Renan Bressan"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:80500",
        name: "Espinho, Fabio"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:795906",
        name: "Pena, Sergio"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:73146",
        name: "Ristovski, Stefan"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:188133",
        name: "Zivkovic, Andrija"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 3,
      rank: 32,
      player: {
        id: "sr:player:853560",
        name: "Reis, Matheus"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:800432",
        name: "Soares"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:126076",
        name: "Tomane"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:105404",
        name: "Eduardo, Wilson"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:39066",
        name: "Brahimi, Yacine"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:43735",
        name: "Lica"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:46819",
        name: "Martinez, Jackson"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:314380",
        name: "Horta, Ricardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:557166",
        name: "Balde, Mama"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:110954",
        name: "Toze"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:243479",
        name: "Paulinho"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:1013433",
        name: "Tavares, Heriberto"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:156709",
        name: "Rashid, Osama"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:339695",
        name: "Delgado, Juan"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:343685",
        name: "Derley"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:584434",
        name: "Nuno, Pedro"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:44684",
        name: "Aurelio, Joao"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:129615",
        name: "Nildo Petrolina"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:344205",
        name: "Manafa, Wilson"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:557008",
        name: "Cervi, Franco"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:776747",
        name: "Boa Morte, Aylton"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:1013609",
        name: "Cabral, Jovane"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:47334",
        name: "Eduardo Salvio"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:552532",
        name: "Henrique, Eduardo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:589546",
        name: "Pineda, Denis"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:868640",
        name: "Kalindi"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:1011951",
        name: "Tabata, Bruno"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:44683",
        name: "Micael, Ruben"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:44696",
        name: "Pinto, Nuno"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:84847",
        name: "Fransergio"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:100844",
        name: "Machado, Edu"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:155855",
        name: "Stephens, Alfredo"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:156319",
        name: "Joaozinho"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:318943",
        name: "Macedo, Ruben"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:589020",
        name: "Carraca"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:605956",
        name: "Fernandes, Gedson"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 2,
      rank: 49,
      player: {
        id: "sr:player:1017831",
        name: "Freitas, Murilo"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:32260",
        name: "Dost, Bas"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:47906",
        name: "Jonas"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:252577",
        name: "Rafa Silva"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:546558",
        name: "Rochez, Bryan"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:59777",
        name: "Andre Andre"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:317677",
        name: "Joao Camacho"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:59784",
        name: "Moreira, Bruno"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:599698",
        name: "Tagueu, Joel"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:15540",
        name: "Mateus"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:116869",
        name: "Aboubakar, Vincent"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:204790",
        name: "Herrera, Hector"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:205790",
        name: "Paulinho"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:268821",
        name: "Andrade, Fernando"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:1397359",
        name: "Kikas"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:44739",
        name: "Gomes, Vitor"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:133828",
        name: "Texeira, David"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:268407",
        name: "Lucca, Jonatan"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:611880",
        name: "Murilo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:846327",
        name: "Pinho, Rodrigo"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:1239936",
        name: "Gabrielzinho"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:27879",
        name: "Ghazaryan, Gevorg"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:39640",
        name: "Gallo, Bruno"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:39981",
        name: "Montero, Fredy"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:128183",
        name: "Costa, Rafael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:133736",
        name: "Lopes, Rafael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:243167",
        name: "Dener"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:254569",
        name: "Diego Lopes"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:318941",
        name: "Dias, Ruben"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:584116",
        name: "Ponck, Carlos"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:777111",
        name: "Silva, Amilton"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:1548707",
        name: "Santana, Thiago"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:5883",
        name: "Costa, Ricardo"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:47614",
        name: "Valente, Nuno"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:59843",
        name: "Zequinha"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:96840",
        name: "Oliveira, Sergio"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:99545",
        name: "Pereira, Danilo"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:105457",
        name: "Junior, Zainadine"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:135904",
        name: "John, Ola"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:165703",
        name: "Oliveira, William"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:226428",
        name: "Mikel Agu"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:231776",
        name: "Torres, Oliver"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:241466",
        name: "Welthon"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:242211",
        name: "Felipe"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:270291",
        name: "Marcelo Goiano"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:295331",
        name: "Chrien, Martin"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:364612",
        name: "Palhinha, Joao"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:553678",
        name: "Jeronimo, Ronan"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:583826",
        name: "Aouacheria, Bilel"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:592588",
        name: "Ewerton"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:605442",
        name: "Fernandes, Ivanildo"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:777641",
        name: "Pereira, Andre"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:981333",
        name: "Fernandes, Lucas"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:1004865",
        name: "Costa, Vitor"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:1248458",
        name: "Tormena, Vitor"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:26179",
        name: "Pereira, Maxi"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:44640",
        name: "Mano"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:59653",
        name: "Nelson Lenho"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:59772",
        name: "Lima, Ruben"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:97000",
        name: "Esgaio, Ricardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:134761",
        name: "Babanco"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:138899",
        name: "Teixeira, Joao"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:144421",
        name: "Bruno, Vitor"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:150506",
        name: "Drame, Ousmane"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:169729",
        name: "Silva, Goncalo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:170129",
        name: "Hanin, Florent"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:178407",
        name: "Appelt, Gabriel"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:206770",
        name: "Carvalho, Anderson"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:261669",
        name: "Farias, Edson"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:295157",
        name: "Eduardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:343965",
        name: "Nanu"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:357728",
        name: "Pepe"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:584158",
        name: "Afonso, Joao"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:777105",
        name: "Sa, Pedro"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:794848",
        name: "Lucas, Joao"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:843509",
        name: "Sacko, Falaye"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:869302",
        name: "Viana, Bruno"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:871358",
        name: "Elhouni, Hamdou"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:891520",
        name: "Nadjack"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:925770",
        name: "Silva, Bruno"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:978073",
        name: "Xadas, Bruno"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:986201",
        name: "D'Alberto, Anthony"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:1005267",
        name: "Jean Cleber"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:1067538",
        name: "Mendes, Joao"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:1549507",
        name: "Ruster"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      assists: 1,
      rank: 85,
      player: {
        id: "sr:player:1658049",
        name: "Pelagio, Pedro"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    }
  ],
  top_cards: [
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 13,
      rank: 1,
      player: {
        id: "sr:player:144421",
        name: "Bruno, Vitor"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 10,
      rank: 2,
      player: {
        id: "sr:player:125017",
        name: "Wakaso, Alhassan"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 9,
      rank: 2,
      player: {
        id: "sr:player:47443",
        name: "Coates, Sebastian"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 10,
      rank: 2,
      player: {
        id: "sr:player:1142164",
        name: "Julio Cesar"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 10,
      rank: 2,
      player: {
        id: "sr:player:68332",
        name: "Gudelj, Nemanja"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 9,
      rank: 6,
      player: {
        id: "sr:player:252095",
        name: "Silva, Tiago"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 9,
      rank: 6,
      player: {
        id: "sr:player:343685",
        name: "Derley"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 8,
      rank: 6,
      player: {
        id: "sr:player:25994",
        name: "Coentrao, Fabio"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 8,
      rank: 6,
      player: {
        id: "sr:player:963143",
        name: "Vieira, Patrick"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 9,
      rank: 6,
      player: {
        id: "sr:player:242211",
        name: "Felipe"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 9,
      rank: 6,
      player: {
        id: "sr:player:18159",
        name: "Semedo, Jose"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 8,
      rank: 12,
      player: {
        id: "sr:player:288205",
        name: "Fernandes, Bruno"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 1,
      yellow_cards: 6,
      rank: 12,
      player: {
        id: "sr:player:126076",
        name: "Tomane"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 7,
      rank: 12,
      player: {
        id: "sr:player:796044",
        name: "Bessa, Eber"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 2,
      yellow_red_cards: 1,
      yellow_cards: 5,
      rank: 12,
      player: {
        id: "sr:player:5883",
        name: "Costa, Ricardo"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 8,
      rank: 12,
      player: {
        id: "sr:player:105457",
        name: "Junior, Zainadine"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 8,
      rank: 12,
      player: {
        id: "sr:player:270291",
        name: "Marcelo Goiano"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 8,
      rank: 12,
      player: {
        id: "sr:player:44640",
        name: "Mano"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 7,
      rank: 12,
      player: {
        id: "sr:player:992231",
        name: "Neris"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 7,
      rank: 12,
      player: {
        id: "sr:player:1514065",
        name: "Africo, Lucas"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 7,
      rank: 12,
      player: {
        id: "sr:player:382578",
        name: "Santos, Jefferson"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 7,
      rank: 22,
      player: {
        id: "sr:player:318941",
        name: "Dias, Ruben"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 7,
      rank: 22,
      player: {
        id: "sr:player:61180",
        name: "Monteiro, Bruno"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 7,
      rank: 22,
      player: {
        id: "sr:player:352874",
        name: "Rochinha"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 7,
      rank: 22,
      player: {
        id: "sr:player:777105",
        name: "Sa, Pedro"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 2,
      yellow_cards: 5,
      rank: 22,
      player: {
        id: "sr:player:856980",
        name: "Jaquite, Joao"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 6,
      rank: 22,
      player: {
        id: "sr:player:152163",
        name: "Marakis, Sergio"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:47906",
        name: "Jonas"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:1013263",
        name: "Galeno, Wenderson"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:243479",
        name: "Paulinho"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:252707",
        name: "Xavier"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:578560",
        name: "Acuna, Marcos"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:15540",
        name: "Mateus"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 1,
      yellow_cards: 4,
      rank: 28,
      player: {
        id: "sr:player:252815",
        name: "Cardoso, Fabio"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:268821",
        name: "Andrade, Fernando"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:44684",
        name: "Aurelio, Joao"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 2,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 28,
      player: {
        id: "sr:player:73146",
        name: "Ristovski, Stefan"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:254569",
        name: "Diego Lopes"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:816572",
        name: "Loum, Mamadou"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:589020",
        name: "Carraca"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:605442",
        name: "Fernandes, Ivanildo"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:54672",
        name: "Simao, David"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:206770",
        name: "Carvalho, Anderson"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:247903",
        name: "Arango, Chicho"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:551812",
        name: "Raul Silva"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:891520",
        name: "Nadjack"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:137635",
        name: "Vukovic, Josip"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 28,
      player: {
        id: "sr:player:170515",
        name: "Idris"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:343909",
        name: "Helder Tavares"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:1067262",
        name: "Antunes, Stephen"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 6,
      rank: 28,
      player: {
        id: "sr:player:1244110",
        name: "Bebeto"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:800432",
        name: "Soares"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:546558",
        name: "Rochez, Bryan"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:44636",
        name: "Almeida, Andre"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:46819",
        name: "Martinez, Jackson"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:59777",
        name: "Andre Andre"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:339695",
        name: "Delgado, Juan"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:70919",
        name: "Viana, Diogo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:231764",
        name: "Jota"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 1,
      yellow_cards: 3,
      rank: 52,
      player: {
        id: "sr:player:344205",
        name: "Manafa, Wilson"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:39640",
        name: "Gallo, Bruno"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:244591",
        name: "Schmidt, Joao"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:853560",
        name: "Reis, Matheus"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:868640",
        name: "Kalindi"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:44683",
        name: "Micael, Ruben"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:78378",
        name: "Jardel"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:84847",
        name: "Fransergio"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:170483",
        name: "Goncalves, Vitor"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:995487",
        name: "Marcao"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:41649",
        name: "Coelho, Nuno"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:97000",
        name: "Esgaio, Ricardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:134761",
        name: "Babanco"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:169729",
        name: "Silva, Goncalo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:249411",
        name: "Farina, Luis"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:261669",
        name: "Farias, Edson"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:1408953",
        name: "Fellipe, Jorge"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:59594",
        name: "Santos, Andre"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:24597",
        name: "Danny"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:385400",
        name: "Lucas"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:1007047",
        name: "Neto, Angelo"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:1012931",
        name: "Falcao, Claudio"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 4,
      rank: 52,
      player: {
        id: "sr:player:351810",
        name: "Silva, Raphael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:44663",
        name: "Braga, Bruno"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:44751",
        name: "Tarantini"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:97006",
        name: "Teixeira, Joao Carlos"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:128326",
        name: "Paulinho"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 5,
      rank: 52,
      player: {
        id: "sr:player:343585",
        name: "Djavan"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:17740",
        name: "Nani"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:252577",
        name: "Rafa Silva"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:312110",
        name: "Telles, Alex"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:557166",
        name: "Balde, Mama"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 88,
      player: {
        id: "sr:player:110954",
        name: "Toze"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:252087",
        name: "Arsenio"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:89566",
        name: "Costa, Edgar"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:170117",
        name: "Sequeira, Nuno"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:309358",
        name: "Mendy, Frederic"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:599698",
        name: "Tagueu, Joel"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:39402",
        name: "Henrique Almeida"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:204790",
        name: "Herrera, Hector"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:205790",
        name: "Paulinho"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:802944",
        name: "Lamas, Bruno"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:80500",
        name: "Espinho, Fabio"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:190137",
        name: "Soares, Rafa"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:352880",
        name: "Pereira, Hildeberto"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:557008",
        name: "Cervi, Franco"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:846327",
        name: "Pinho, Rodrigo"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:243167",
        name: "Dener"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:777111",
        name: "Silva, Amilton"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:844297",
        name: "Niltinho"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:47614",
        name: "Valente, Nuno"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 3,
      rank: 88,
      player: {
        id: "sr:player:100844",
        name: "Machado, Edu"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:155629",
        name: "Briseno, Antonio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:156319",
        name: "Joaozinho"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:175097",
        name: "Correa, Jorge"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:778216",
        name: "Talocha, Joao"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:956430",
        name: "Raphinha"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:981333",
        name: "Fernandes, Lucas"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:1003011",
        name: "Hamzaoui, Okacha"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 3,
      rank: 88,
      player: {
        id: "sr:player:1004865",
        name: "Costa, Vitor"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:1145114",
        name: "Pablo Santos"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:79064",
        name: "Silva, Joao"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 3,
      rank: 88,
      player: {
        id: "sr:player:178407",
        name: "Appelt, Gabriel"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:252819",
        name: "Gaspar, Bruno"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:304264",
        name: "Perdigao"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:357728",
        name: "Pepe"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:358552",
        name: "Indio, Matheus"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:862680",
        name: "Moufi, Fahd"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:1005267",
        name: "Jean Cleber"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:329081",
        name: "Leite, Helton"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 1,
      yellow_cards: 2,
      rank: 88,
      player: {
        id: "sr:player:44690",
        name: "Halliche, Rafik"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 88,
      player: {
        id: "sr:player:335813",
        name: "Ramos, Flavio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 88,
      player: {
        id: "sr:player:775079",
        name: "Fernandes, Jorge"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 2,
      yellow_cards: 2,
      rank: 88,
      player: {
        id: "sr:player:252717",
        name: "Alves, Ricardo"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 3,
      rank: 88,
      player: {
        id: "sr:player:1559296",
        name: "Cardoso, Goncalo"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:4694",
        name: "Casillas, Iker"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:31814",
        name: "Fejsa, Ljubomir"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:41650",
        name: "Fernandes, Vasco"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:44630",
        name: "Pereira, Marco"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:44719",
        name: "Pacheco, Fabio"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:122076",
        name: "Obiora, Nwankwo"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:170539",
        name: "Machado, Luis"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:190145",
        name: "Ramos, Chico"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:244005",
        name: "Aberhoun, Mohamed"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:349536",
        name: "Dankler"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:364600",
        name: "Monte, Nelson"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:1040337",
        name: "Pantaleao, Kaio"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 4,
      rank: 88,
      player: {
        id: "sr:player:1099100",
        name: "Sousa, Andre"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 148,
      player: {
        id: "sr:player:105404",
        name: "Eduardo, Wilson"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:298300",
        name: "Marega, Moussa"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:43735",
        name: "Lica"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:244801",
        name: "Otavio"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:141021",
        name: "Ze Manuel"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:332257",
        name: "Cadiz, Jhonder"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:902400",
        name: "Davidson"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:1013433",
        name: "Tavares, Heriberto"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 2,
      rank: 148,
      player: {
        id: "sr:player:317677",
        name: "Joao Camacho"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:930466",
        name: "Dala, Gelson"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:34257",
        name: "Claudemir"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:108398",
        name: "Samaris, Andreas"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:115718",
        name: "Diaby, Abdoulay"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:584434",
        name: "Nuno, Pedro"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:268407",
        name: "Lucca, Jonatan"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:106116",
        name: "Falcone, Federico"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:128183",
        name: "Costa, Rafael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:291707",
        name: "Luiz Phellype"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:584116",
        name: "Ponck, Carlos"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:1011951",
        name: "Tabata, Bruno"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:5884",
        name: "Pepe"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 148,
      player: {
        id: "sr:player:44696",
        name: "Pinto, Nuno"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:128207",
        name: "Barrera, Leandro"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:155855",
        name: "Stephens, Alfredo"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 2,
      rank: 148,
      player: {
        id: "sr:player:364778",
        name: "Pedrao"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:1248458",
        name: "Tormena, Vitor"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:12676",
        name: "Tissone, Fernando"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:26179",
        name: "Pereira, Maxi"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:43736",
        name: "Santos, Cris"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 2,
      rank: 148,
      player: {
        id: "sr:player:74071",
        name: "Sasso, Vincent"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 2,
      rank: 148,
      player: {
        id: "sr:player:96846",
        name: "Bruno, David"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 2,
      rank: 148,
      player: {
        id: "sr:player:329107",
        name: "Cesar"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:588996",
        name: "Sampaio, Philipe"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:843509",
        name: "Sacko, Falaye"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:869302",
        name: "Viana, Bruno"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:885100",
        name: "Singh, Luther"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:1121457",
        name: "Wendel"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:44745",
        name: "Silvio"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:128453",
        name: "Nascimento, Bruno"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 2,
      yellow_cards: 1,
      rank: 148,
      player: {
        id: "sr:player:254561",
        name: "Filipe Augusto"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 2,
      rank: 148,
      player: {
        id: "sr:player:94197",
        name: "Reis, Nuno"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 2,
      rank: 148,
      player: {
        id: "sr:player:1073468",
        name: "Cascardo, Gustavo"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:39586",
        name: "Petrovic, Radosav"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:39693",
        name: "Jefferson"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:47604",
        name: "Figueiredo, Cristiano"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:54891",
        name: "Ferreira, Ricardo"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:61246",
        name: "Filipe, Antonio"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:108486",
        name: "Muriel"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:110767",
        name: "Buatu, Jonathan"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:135130",
        name: "Sagna, Pierre"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:140088",
        name: "Bergdich, Zakarya"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:146381",
        name: "Crivellaro, Rafael"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:158257",
        name: "Battaglia, Rodrigo"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:251217",
        name: "Pedro, Joao"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:310800",
        name: "Artur Jorge"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:329469",
        name: "Baiano, Fabricio"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:351824",
        name: "Campi, Gaston"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:352858",
        name: "Ferreira, Andre"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:377776",
        name: "Abedzadeh, Amir"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:929870",
        name: "Mandava, Reinildo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 3,
      rank: 148,
      player: {
        id: "sr:player:952614",
        name: "Diga"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:77725",
        name: "Pizzi"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:128360",
        name: "Sousa, Dyego"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:177177",
        name: "Grimaldo, Alex"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:39066",
        name: "Brahimi, Yacine"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:78611",
        name: "Fredy"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:299914",
        name: "Guedes, Alexandre"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:123906",
        name: "Corona, Jesus"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:155825",
        name: "Nakajima, Shoya"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:1233356",
        name: "Vinicius, Carlos"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:156709",
        name: "Rashid, Osama"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:869378",
        name: "Soares, Rodrigo"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:59784",
        name: "Moreira, Bruno"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:170005",
        name: "Sturgeon, Fabio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:33520",
        name: "Edinho"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:38152",
        name: "Renan Bressan"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:44739",
        name: "Gomes, Vitor"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:129615",
        name: "Nildo Petrolina"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:795906",
        name: "Pena, Sergio"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:44685",
        name: "Nene"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:133736",
        name: "Lopes, Rafael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 1,
      rank: 210,
      player: {
        id: "sr:player:552532",
        name: "Henrique, Eduardo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:960304",
        name: "Carvalho, Wellington"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:1548707",
        name: "Santana, Thiago"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:928",
        name: "Defendi, Rodrigo"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:3179",
        name: "Mathieu, Jeremy"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:96840",
        name: "Oliveira, Sergio"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:135904",
        name: "John, Ola"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:165703",
        name: "Oliveira, William"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:226428",
        name: "Mikel Agu"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:231776",
        name: "Torres, Oliver"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:318943",
        name: "Macedo, Ruben"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 210,
      player: {
        id: "sr:player:357734",
        name: "Ferro"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:364612",
        name: "Palhinha, Joao"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:553678",
        name: "Jeronimo, Ronan"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:583826",
        name: "Aouacheria, Bilel"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:592588",
        name: "Ewerton"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:605956",
        name: "Fernandes, Gedson"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:1008537",
        name: "Getterson"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:59653",
        name: "Nelson Lenho"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:59772",
        name: "Lima, Ruben"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:101156",
        name: "Maras, Nikola"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:137979",
        name: "Martins, Luis"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:267461",
        name: "Palocevic, Aleksandar"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:329583",
        name: "Celis, Guillermo"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 210,
      player: {
        id: "sr:player:794848",
        name: "Lucas, Joao"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:803656",
        name: "Osorio, Yordan"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:871372",
        name: "Costinha"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:936362",
        name: "Leite, Diogo"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:1433867",
        name: "Carlos, Joao"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 210,
      player: {
        id: "sr:player:320917",
        name: "Conti, German"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 210,
      player: {
        id: "sr:player:138149",
        name: "Vlachodimos, Odysseas"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 210,
      player: {
        id: "sr:player:342939",
        name: "Icaro"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 1,
      rank: 210,
      player: {
        id: "sr:player:177175",
        name: "Rosic, Lazar"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 1,
      rank: 210,
      player: {
        id: "sr:player:780366",
        name: "Jambor, Nikola"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 1,
      rank: 210,
      player: {
        id: "sr:player:1497425",
        name: "Rocha, Junio"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:13971",
        name: "Gomes, Tiago"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:30166",
        name: "Soares, Marco"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:31325",
        name: "Makaridze, Giorgi"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:44677",
        name: "Lopes, Felipe"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:56050",
        name: "Accioly"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:59586",
        name: "Ukra"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:59751",
        name: "Fernandes, Ruben"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:73048",
        name: "Adoua, Issam El"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:80479",
        name: "Lionn"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:82598",
        name: "Ribeiro, Renan"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:101971",
        name: "Melo, Filipe"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:137915",
        name: "Cande, Mamadu"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:139222",
        name: "Beunardeau, Quentin"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:155985",
        name: "Silva, Charles"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:167065",
        name: "Ferreira, Ruben"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:218804",
        name: "Rene"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:243543",
        name: "Lazaroni, Guilherme"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:277437",
        name: "Boyd, Tyler"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:304926",
        name: "Alphonse"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:329103",
        name: "Guimaraes, Daniel"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:329195",
        name: "Jubal"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:345843",
        name: "Secco, Caio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:595598",
        name: "Jardim, Leo"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:793820",
        name: "Gomes, Dalcio"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:802950",
        name: "Amoah, Joseph"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:853436",
        name: "Schons, Alan"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:857936",
        name: "Jhonatan"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:871486",
        name: "Cerqueira, Mauro"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:935124",
        name: "Borevkovic, Toni"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:973535",
        name: "Gelain, Henrique"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:1004005",
        name: "Doumbia, Idrissa"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:1017849",
        name: "China, Fabio"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 2,
      rank: 210,
      player: {
        id: "sr:player:1272740",
        name: "Alhassan, Abdullahi Ibrahim"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:32260",
        name: "Dost, Bas"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1050565",
        name: "Joao Felix"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:584124",
        name: "Chiquinho"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:314380",
        name: "Horta, Ricardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:791645",
        name: "Witi"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:243575",
        name: "Mattheus"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:116869",
        name: "Aboubakar, Vincent"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:233370",
        name: "Novais, Joao"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1397359",
        name: "Kikas"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:776747",
        name: "Boa Morte, Aylton"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1013609",
        name: "Cabral, Jovane"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1239936",
        name: "Gabrielzinho"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:27879",
        name: "Ghazaryan, Gevorg"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:39981",
        name: "Montero, Fredy"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:173833",
        name: "Keita, Alhassane"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:188133",
        name: "Zivkovic, Andrija"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1402615",
        name: "Andre Luis"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:99545",
        name: "Pereira, Danilo"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:125886",
        name: "Valencia, Jose"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:241466",
        name: "Welthon"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 0,
      rank: 300,
      player: {
        id: "sr:player:868620",
        name: "Jadson"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1017831",
        name: "Freitas, Murilo"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1059983",
        name: "Schettine, Guilherme"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:43779",
        name: "Bueno, Alberto"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:96850",
        name: "Claro, Andre"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:96888",
        name: "Galo, Diego"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:129724",
        name: "Ferreyra, Facundo"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 0,
      rank: 300,
      player: {
        id: "sr:player:138899",
        name: "Teixeira, Joao"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:143587",
        name: "Hernani"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:150506",
        name: "Drame, Ousmane"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:170129",
        name: "Hanin, Florent"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:555624",
        name: "Carvalho, Carlos"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:986201",
        name: "D'Alberto, Anthony"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1011533",
        name: "Petkov, Stivan"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1242060",
        name: "Patrick"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1549507",
        name: "Ruster"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1658049",
        name: "Pelagio, Pedro"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 1,
      yellow_red_cards: 0,
      yellow_cards: 0,
      rank: 300,
      player: {
        id: "sr:player:352794",
        name: "Sparagna, Stephane"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 1,
      yellow_cards: 0,
      rank: 300,
      player: {
        id: "sr:player:106384",
        name: "Lema, Cristian"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:18126",
        name: "Taarabt, Adel"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:27069",
        name: "Salin, Romain"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:37952",
        name: "Rodriguez, Patito"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:39594",
        name: "Serginho"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:44676",
        name: "Bracalli, Rafael"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:59535",
        name: "Nunes, Ricardo"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:59616",
        name: "Pinto, Andre"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:59625",
        name: "Pacheco, Pedro"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:59816",
        name: "Santos, Diogo"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:96849",
        name: "Alex"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:143584",
        name: "Ferreira, Filipe"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:143609",
        name: "Diogo Figueiras"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:147897",
        name: "Sen, Erdem"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:148190",
        name: "Milos, Mato"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:153611",
        name: "Ljujic, Matija"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:167655",
        name: "Ilori, Tiago"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:205760",
        name: "Avto"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:243165",
        name: "Navacchio, Leonardo"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:243725",
        name: "Grolli, Douglas"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:253815",
        name: "Nuno Campos"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:253817",
        name: "Diogo Coelho"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:280945",
        name: "Pereira, Joel"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:287023",
        name: "Said, Ahmed"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:314376",
        name: "Geraldes, Chico"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:322007",
        name: "Oliveira, Ruben"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:344159",
        name: "Sa, Tiago"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:353950",
        name: "Evouna, Malick"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:357724",
        name: "Silva, Pedro"
      },
      team: {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "CDT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:359618",
        name: "Alampasu, Dele"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:584430",
        name: "Iago Santos"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:611874",
        name: "Erik"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:759238",
        name: "Sauer, Gustavo"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:799022",
        name: "Velez, Nicolas"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:806742",
        name: "Semedo, Alfa"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:826683",
        name: "Gamboa, Joao"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:841059",
        name: "Marques, Lucas"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:868636",
        name: "Pereira, Marcel"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:918720",
        name: "Dodo"
      },
      team: {
        id: "sr:competitor:3009",
        name: "Vitoria Guimaraes",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:924670",
        name: "Luquinhas"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:928158",
        name: "Hackman"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:957322",
        name: "Borja, Cristian"
      },
      team: {
        id: "sr:competitor:3001",
        name: "Sporting CP",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:962155",
        name: "Pablo Diogo"
      },
      team: {
        id: "sr:competitor:3011",
        name: "CD Santa Clara",
        abbreviation: "CDS"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:964611",
        name: "Franca, Lucas"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1141402",
        name: "Cleylton"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1238282",
        name: "Teles, Ruan"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1246942",
        name: "Rodrigues, Allef"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1357806",
        name: "Boia, Paulo"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1385259",
        name: "Ryller, Ricardo"
      },
      team: {
        id: "sr:competitor:2999",
        name: "Sporting Braga",
        abbreviation: "SPO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1504687",
        name: "Marcao"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1561944",
        name: "Camara, Ibrahima"
      },
      team: {
        id: "sr:competitor:3014",
        name: "Moreirense FC",
        abbreviation: "MOR"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1696861",
        name: "Silva, Andre"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      red_cards: 0,
      yellow_red_cards: 0,
      yellow_cards: 1,
      rank: 300,
      player: {
        id: "sr:player:1707505",
        name: "Reis, Bruno"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    }
  ],
  top_own_goals: [
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:1233356",
        name: "Vinicius, Carlos"
      },
      team: {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RIO"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:44739",
        name: "Gomes, Vitor"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:344205",
        name: "Manafa, Wilson"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:318941",
        name: "Dias, Ruben"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:78378",
        name: "Jardel"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:100844",
        name: "Machado, Edu"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:155629",
        name: "Briseno, Antonio"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:242211",
        name: "Felipe"
      },
      team: {
        id: "sr:competitor:3002",
        name: "FC Porto",
        abbreviation: "FCP"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:868620",
        name: "Jadson"
      },
      team: {
        id: "sr:competitor:3026",
        name: "Portimonense SC",
        abbreviation: "POR"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:169729",
        name: "Silva, Goncalo"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:43751",
        name: "Coelho, Nuno Andre"
      },
      team: {
        id: "sr:competitor:3025",
        name: "GD Chaves",
        abbreviation: "GDC"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:44745",
        name: "Silvio"
      },
      team: {
        id: "sr:competitor:3008",
        name: "Vitoria Setubal",
        abbreviation: "VIT"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:59594",
        name: "Santos, Andre"
      },
      team: {
        id: "sr:competitor:3004",
        name: "CF Os Belenenses",
        abbreviation: "CFO"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:128453",
        name: "Nascimento, Bruno"
      },
      team: {
        id: "sr:competitor:4501",
        name: "CD Feirense",
        abbreviation: "CDF"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:320917",
        name: "Conti, German"
      },
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        abbreviation: "BEN"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:329081",
        name: "Leite, Helton"
      },
      team: {
        id: "sr:competitor:2995",
        name: "BOAVISTA FC",
        abbreviation: "BOA"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:609750",
        name: "Gomes, Bruno"
      },
      team: {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "CDD"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:1142164",
        name: "Julio Cesar"
      },
      team: {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN"
      }
    },
    {
      own_goals: 1,
      rank: 1,
      player: {
        id: "sr:player:1559096",
        name: "Soares, Aloisio"
      },
      team: {
        id: "sr:competitor:3007",
        name: "CS Maritimo Madeira",
        abbreviation: "CSM"
      }
    }
  ]
};

export const matchEnded = {
  generated_at: "2019-03-31T19:48:42+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/match_summary.json",
  sport_event: {
    id: "sr:match:14946243",
    scheduled: "2019-03-31T16:30:00+00:00",
    start_time_tbd: false,
    tournament_round: {
      type: "group",
      number: 27,
      phase: "main_round_1"
    },
    season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19",
      tournament_id: "sr:tournament:238"
    },
    tournament: {
      id: "sr:tournament:238",
      name: "Primeira Liga",
      sport: {
        id: "sr:sport:1",
        name: "Soccer"
      },
      category: {
        id: "sr:category:44",
        name: "Portugal",
        country_code: "PRT"
      }
    },
    competitors: [
      {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "RAV",
        qualifier: "home"
      },
      {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "AVE",
        qualifier: "away"
      }
    ],
    venue: {
      id: "sr:venue:2013",
      name: "Estadio Dos Arcos",
      capacity: 9065,
      city_name: "Vila do Conde",
      country_name: "Portugal",
      map_coordinates: "41.362755,-8.740186",
      country_code: "PRT"
    }
  },
  sport_event_conditions: {
    referee: {
      id: "sr:referee:950626",
      name: "Fernandes Ferreira, Vitor Jorge",
      nationality: "Portugal",
      country_code: "PRT"
    },
    venue: {
      id: "sr:venue:2013",
      name: "Estadio Dos Arcos",
      capacity: 9065,
      city_name: "Vila do Conde",
      country_name: "Portugal",
      map_coordinates: "41.362755,-8.740186",
      country_code: "PRT"
    },
    weather_info: {
      pitch: "good",
      weather_conditions: "medium"
    }
  },
  sport_event_status: {
    status: "closed",
    match_status: "ended",
    home_score: 0,
    away_score: 2,
    winner_id: "sr:competitor:3032",
    period_scores: [
      {
        home_score: 0,
        away_score: 1,
        type: "regular_period",
        number: 1
      },
      {
        home_score: 0,
        away_score: 1,
        type: "regular_period",
        number: 2
      }
    ]
  },
  statistics: {
    teams: [
      {
        id: "sr:competitor:3036",
        name: "Rio Ave FC",
        abbreviation: "RAV",
        qualifier: "home",
        statistics: {
          ball_possession: 69,
          free_kicks: 17,
          shots_saved: 3,
          throw_ins: 19,
          corner_kicks: 12,
          goal_kicks: 8,
          fouls: 10,
          shots_blocked: 6,
          shots_on_target: 3,
          shots_off_target: 7,
          offsides: 1,
          injuries: 2,
          yellow_cards: 1
        },
        players: [
          {
            id: "sr:player:595598",
            name: "Jardim, Leo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:891520",
            name: "Nadjack",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:344199",
            name: "Semedo, Ruben",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:990563",
            name: "Messias",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:252661",
            name: "Afonso Figueiredo",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:254569",
            name: "Diego Lopes",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:780366",
            name: "Jambor, Nikola",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:254561",
            name: "Filipe Augusto",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1013263",
            name: "Galeno, Wenderson",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:287023",
            name: "Said, Ahmed",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:59784",
            name: "Moreira, Bruno",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:352882",
            name: "Santos, Nuno",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:930466",
            name: "Dala, Gelson",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1239936",
            name: "Gabrielzinho",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          }
        ]
      },
      {
        id: "sr:competitor:3032",
        name: "CD das Aves",
        abbreviation: "AVE",
        qualifier: "away",
        statistics: {
          ball_possession: 31,
          fouls: 16,
          throw_ins: 17,
          corner_kicks: 7,
          shots_on_target: 5,
          goal_kicks: 12,
          shots_off_target: 7,
          free_kicks: 11,
          shots_saved: 3,
          offsides: 1,
          shots_blocked: 3,
          injuries: 2,
          yellow_cards: 4
        },
        players: [
          {
            id: "sr:player:139222",
            name: "Beunardeau, Quentin",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:869378",
            name: "Soares, Rodrigo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 1,
            penalty_goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1408953",
            name: "Fellipe, Jorge",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:96888",
            name: "Galo, Diego",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1004865",
            name: "Costa, Vitor",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1012931",
            name: "Falcao, Claudio",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:44739",
            name: "Gomes, Vitor",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 1,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:584116",
            name: "Ponck, Carlos",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:557166",
            name: "Balde, Mama",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:924670",
            name: "Luquinhas",
            nickname: "Luquinhas",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:343685",
            name: "Derley",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:305482",
            name: "Faye, Ablaye Yare",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1012945",
            name: "Varela, Jose",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:322007",
            name: "Oliveira, Ruben",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          }
        ]
      }
    ]
  }
};

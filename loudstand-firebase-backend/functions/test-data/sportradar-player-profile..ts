export const playerProfile = {
  generated_at: "2019-04-12T13:39:34+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/player_profile.json",
  player: {
    id: "sr:player:78378",
    name: "Jardel",
    last_name: "Jardel",
    type: "defender",
    date_of_birth: "1986-03-29",
    nationality: "Brazil",
    country_code: "BRA",
    height: 192,
    weight: 87,
    preferred_foot: "right",
    gender: "male"
  },
  teams: [
    {
      id: "sr:competitor:3006",
      name: "Benfica Lisbon",
      country: "Portugal",
      country_code: "PRT",
      abbreviation: "BEN"
    }
  ],
  roles: [
    {
      type: "player",
      active: "true",
      team: {
        id: "sr:competitor:3006",
        name: "Benfica Lisbon",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "BEN"
      },
      start_date: "2011-01-16",
      jersey_number: 33
    },
    {
      type: "on_loan",
      active: "false",
      team: {
        id: "sr:competitor:4500",
        name: "GD Estoril Praia",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "EST"
      },
      start_date: "2009-07-01",
      end_date: "2010-05-31",
      jersey_number: 3
    },
    {
      type: "player",
      active: "false",
      team: {
        id: "sr:competitor:4956",
        name: "SC Olhanense",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "OLH"
      },
      start_date: "2010-07-01",
      end_date: "2011-01-15",
      jersey_number: 3
    },
    {
      type: "player",
      active: "false",
      team: {
        id: "sr:competitor:74465",
        name: "Benfica Lisbon B",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "BEB"
      },
      start_date: "2011-01-16",
      end_date: "2014-06-30",
      jersey_number: 33
    }
  ],
  statistics: {
    seasons: [
      {
        id: "sr:season:2765",
        name: "Europa League 10/11",
        statistics: {
          matches_played: 5,
          substituted_in: 1,
          substituted_out: 0,
          goals_scored: 1,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2011-05-05T18:06:32+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:679",
          name: "UEFA Europa League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:2765",
          scheduled: 488,
          played: 483,
          max_coverage_level: "silver",
          max_covered: 481,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:4790",
        name: "UEFA Europa League 12/13",
        statistics: {
          matches_played: 5,
          substituted_in: 3,
          substituted_out: 0,
          goals_scored: 1,
          assists: 0,
          own_goals: 1,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2013-05-15T20:19:55+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:679",
          name: "UEFA Europa League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:4790",
          scheduled: 480,
          played: 480,
          max_coverage_level: "silver",
          max_covered: 480,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:10666",
        name: "League Cup 15/16",
        statistics: {
          matches_played: 2,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 1,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2016-05-20T20:36:50+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:327",
          name: "League Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:10666",
          scheduled: 49,
          played: 44,
          max_coverage_level: "gold",
          max_covered: 14,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:54533",
        name: "UEFA Champions League 18/19",
        statistics: {
          matches_played: 8,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 1,
          assists: 0,
          own_goals: 0,
          yellow_cards: 4,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2018-12-12T20:43:05+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:7",
          name: "UEFA Champions League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:54533",
          scheduled: 212,
          played: 208,
          max_coverage_level: "platinum",
          max_covered: 116,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:3075",
        name: "Taca de Portugal 10/11",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 1,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2011-04-20T21:09:56+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:336",
          name: "Taca de Portugal",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:3075",
          scheduled: 82,
          played: 35,
          max_coverage_level: "silver",
          max_covered: 35,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:7236",
        name: "Taca de Portugal 13/14",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 1,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2014-04-16T21:23:13+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:336",
          name: "Taca de Portugal",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:7236",
          scheduled: 92,
          played: 41,
          max_coverage_level: "silver",
          max_covered: 41,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:11550",
        name: "Taca de Portugal 15/16",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 1,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2015-11-21T20:38:51+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:336",
          name: "Taca de Portugal",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:11550",
          scheduled: 109,
          played: 109,
          max_coverage_level: "gold",
          max_covered: 18,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:10484",
        name: "UEFA Champions League 15/16",
        statistics: {
          matches_played: 9,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 3,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2016-04-13T17:58:28+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:7",
          name: "UEFA Champions League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:10484",
          scheduled: 222,
          played: 219,
          max_coverage_level: "gold",
          max_covered: 172,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:57850",
        name: "Taca de Portugal 18/19",
        statistics: {
          matches_played: 3,
          substituted_in: 0,
          substituted_out: 1,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 2,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2019-04-03T18:57:59+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:336",
          name: "Taca de Portugal",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:57850",
          scheduled: 112,
          played: 110,
          max_coverage_level: "gold",
          max_covered: 6,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:55601",
        name: "League Cup 18/19",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 1,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2019-01-22T21:09:18+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:327",
          name: "League Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:55601",
          scheduled: 43,
          played: 43,
          max_coverage_level: "gold",
          max_covered: 10,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:3402",
        name: "Champions League 11/12",
        statistics: {
          matches_played: 3,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2012-03-27T17:58:01+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:7",
          name: "UEFA Champions League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:3402",
          scheduled: 213,
          played: 213,
          max_coverage_level: "silver",
          max_covered: 213,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:5411",
        name: "Taca de Portugal 12/13",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2013-01-17T20:28:40+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:336",
          name: "Taca de Portugal",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:5411",
          scheduled: 83,
          played: 41,
          max_coverage_level: "silver",
          max_covered: 40,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:6361",
        name: "UEFA Europa League 13/14",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2014-02-20T16:58:58+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:679",
          name: "UEFA Europa League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:6361",
          scheduled: 482,
          played: 481,
          max_coverage_level: "silver",
          max_covered: 481,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:6489",
        name: "League Cup 13/14",
        statistics: {
          matches_played: 4,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 1,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2014-04-27T16:57:20+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:327",
          name: "League Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:6489",
          scheduled: 72,
          played: 72,
          max_coverage_level: "silver",
          max_covered: 58,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:9210",
        name: "Taca de Portugal 14/15",
        statistics: {
          matches_played: 2,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2014-12-18T20:45:15+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:336",
          name: "Taca de Portugal",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:9210",
          scheduled: 86,
          played: 63,
          max_coverage_level: "gold",
          max_covered: 23,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:8474",
        name: "League Cup 14/15",
        statistics: {
          matches_played: 5,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 1,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2015-05-29T19:23:28+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:327",
          name: "League Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:8474",
          scheduled: 83,
          played: 83,
          max_coverage_level: "gold",
          max_covered: 32,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:10648",
        name: "Super Cup 2015",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2015-08-09T19:05:13+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:345",
          name: "Super Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:10648",
          scheduled: 1,
          played: 1,
          max_coverage_level: "gold",
          max_covered: 1,
          min_coverage_level: "gold"
        }
      },
      {
        id: "sr:season:35247",
        name: "Taca de Portugal 16/17",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2017-02-28T19:46:31+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:336",
          name: "Taca de Portugal",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:35247",
          scheduled: 110,
          played: 110,
          max_coverage_level: "gold",
          max_covered: 4,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:33699",
        name: "League Cup 16/17",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2017-01-26T20:10:10+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:327",
          name: "League Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:33699",
          scheduled: 46,
          played: 44,
          max_coverage_level: "gold",
          max_covered: 7,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:40816",
        name: "Super Cup 2017",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2017-08-05T20:30:37+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:345",
          name: "Super Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:40816",
          scheduled: 1,
          played: 1,
          max_coverage_level: "gold",
          max_covered: 1,
          min_coverage_level: "gold"
        }
      },
      {
        id: "sr:season:45542",
        name: "Taca de Portugal 17/18",
        statistics: {
          matches_played: 2,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 1,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2017-12-13T23:31:25+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:336",
          name: "Taca de Portugal",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:45542",
          scheduled: 112,
          played: 112,
          max_coverage_level: "gold",
          max_covered: 11,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:54531",
        name: "UEFA Europa League 18/19",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2019-04-11T17:41:37+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:679",
          name: "UEFA Europa League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:54531",
          scheduled: 516,
          played: 512,
          max_coverage_level: "gold",
          max_covered: 281,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:2832",
        name: "Primeira Liga 10/11",
        statistics: {
          matches_played: 16,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 1,
          assists: 0,
          own_goals: 0,
          yellow_cards: 4,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2011-01-15T18:58:06+00:00"
        },
        team: {
          id: "sr:competitor:4956",
          name: "SC Olhanense",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "OLH"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:2832",
          scheduled: 243,
          played: 241,
          max_coverage_level: "silver",
          max_covered: 241,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:2832",
        name: "Primeira Liga 10/11",
        statistics: {
          matches_played: 8,
          substituted_in: 3,
          substituted_out: 1,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 1,
          yellow_red_cards: 1,
          red_cards: 0,
          last_event_time: "2011-05-14T20:47:18+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:2832",
          scheduled: 243,
          played: 241,
          max_coverage_level: "silver",
          max_covered: 241,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:3462",
        name: "Primeira Liga 11/12",
        statistics: {
          matches_played: 10,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 2,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2012-03-23T22:09:03+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:3462",
          scheduled: 241,
          played: 240,
          max_coverage_level: "silver",
          max_covered: 240,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:4907",
        name: "Primeira Liga 12/13",
        statistics: {
          matches_played: 16,
          substituted_in: 1,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2013-05-19T17:20:38+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:4907",
          scheduled: 242,
          played: 241,
          max_coverage_level: "silver",
          max_covered: 241,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:4911",
        name: "Segunda Liga 12/13",
        statistics: {
          matches_played: 3,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 1,
          assists: 0,
          own_goals: 0,
          yellow_cards: 1,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2012-09-02T18:57:21+00:00"
        },
        team: {
          id: "sr:competitor:74465",
          name: "Benfica Lisbon B",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEB"
        },
        tournament: {
          id: "sr:tournament:239",
          name: "Segunda Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:4911",
          scheduled: 466,
          played: 466,
          max_coverage_level: "silver",
          max_covered: 466,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:4788",
        name: "UEFA Champions League 12/13",
        statistics: {
          matches_played: 5,
          substituted_in: 1,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 1,
          yellow_cards: 1,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2012-11-20T21:35:06+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:7",
          name: "UEFA Champions League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:4788",
          scheduled: 214,
          played: 214,
          max_coverage_level: "silver",
          max_covered: 213,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:4915",
        name: "League Cup 12/13",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2012-12-30T15:47:23+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:327",
          name: "League Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:4915",
          scheduled: 68,
          played: 68,
          max_coverage_level: "silver",
          max_covered: 52,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:6483",
        name: "Primeira Liga 13/14",
        statistics: {
          matches_played: 6,
          substituted_in: 2,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 2,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2014-05-10T17:39:30+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:6483",
          scheduled: 243,
          played: 243,
          max_coverage_level: "silver",
          max_covered: 243,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:6485",
        name: "Segunda Liga 13/14",
        statistics: {
          matches_played: 4,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 1,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2013-10-29T09:16:48+00:00"
        },
        team: {
          id: "sr:competitor:74465",
          name: "Benfica Lisbon B",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEB"
        },
        tournament: {
          id: "sr:tournament:239",
          name: "Segunda Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:6485",
          scheduled: 463,
          played: 462,
          max_coverage_level: "silver",
          max_covered: 462,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:8382",
        name: "Primeira Liga 14/15",
        statistics: {
          matches_played: 31,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 4,
          assists: 0,
          own_goals: 0,
          yellow_cards: 3,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2015-05-23T18:29:15+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:8382",
          scheduled: 307,
          played: 306,
          max_coverage_level: "gold",
          max_covered: 295,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:8226",
        name: "UEFA Champions League 14/15",
        statistics: {
          matches_played: 4,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 1,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2014-12-09T19:05:46+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:7",
          name: "UEFA Champions League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:8226",
          scheduled: 218,
          played: 216,
          max_coverage_level: "gold",
          max_covered: 159,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:10231",
        name: "Int. Champions Cup 2015",
        statistics: {
          matches_played: 3,
          substituted_in: 0,
          substituted_out: 1,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2015-07-29T01:59:54+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:1337",
          name: "Int. Champions Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:10231",
          scheduled: 23,
          played: 23,
          max_coverage_level: "gold",
          max_covered: 21,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:10608",
        name: "Primeira Liga 15/16",
        statistics: {
          matches_played: 30,
          substituted_in: 0,
          substituted_out: 1,
          goals_scored: 3,
          assists: 4,
          own_goals: 0,
          yellow_cards: 8,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2016-05-15T17:16:02+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:10608",
          scheduled: 310,
          played: 309,
          max_coverage_level: "gold",
          max_covered: 297,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:33561",
        name: "Primeira Liga 16/17",
        statistics: {
          matches_played: 1,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 1,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2017-03-18T19:42:27+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:33561",
          scheduled: 309,
          played: 309,
          max_coverage_level: "gold",
          max_covered: 306,
          min_coverage_level: "silver"
        }
      },
      {
        id: "sr:season:41988",
        name: "Primeira Liga 17/18",
        statistics: {
          matches_played: 26,
          substituted_in: 0,
          substituted_out: 1,
          goals_scored: 2,
          assists: 3,
          own_goals: 0,
          yellow_cards: 5,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2018-05-05T21:05:44+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:41988",
          scheduled: 307,
          played: 307,
          max_coverage_level: "gold",
          max_covered: 303,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:41198",
        name: "UEFA Champions League 17/18",
        statistics: {
          matches_played: 4,
          substituted_in: 0,
          substituted_out: 0,
          goals_scored: 0,
          assists: 0,
          own_goals: 1,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2017-12-05T21:16:47+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:7",
          name: "UEFA Champions League",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:41198",
          scheduled: 291,
          played: 221,
          max_coverage_level: "platinum",
          max_covered: 158,
          min_coverage_level: "bronze"
        }
      },
      {
        id: "sr:season:53642",
        name: "Int. Champions Cup 2018",
        statistics: {
          matches_played: 3,
          substituted_in: 0,
          substituted_out: 3,
          goals_scored: 0,
          assists: 0,
          own_goals: 0,
          yellow_cards: 0,
          yellow_red_cards: 0,
          red_cards: 0,
          last_event_time: "2018-08-01T21:50:35+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:1337",
          name: "Int. Champions Cup",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:393",
            name: "International Clubs"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:53642",
          scheduled: 27,
          played: 27,
          max_coverage_level: "platinum",
          max_covered: 1,
          min_coverage_level: "gold"
        }
      },
      {
        id: "sr:season:55243",
        name: "Primeira Liga 18/19",
        statistics: {
          matches_played: 18,
          substituted_in: 0,
          substituted_out: 2,
          goals_scored: 2,
          assists: 0,
          own_goals: 1,
          yellow_cards: 4,
          yellow_red_cards: 0,
          red_cards: 1,
          last_event_time: "2019-02-03T18:50:47+00:00"
        },
        team: {
          id: "sr:competitor:3006",
          name: "Benfica Lisbon",
          country: "Portugal",
          country_code: "PRT",
          abbreviation: "BEN"
        },
        tournament: {
          id: "sr:tournament:238",
          name: "Primeira Liga",
          sport: {
            id: "sr:sport:1",
            name: "Soccer"
          },
          category: {
            id: "sr:category:44",
            name: "Portugal",
            country_code: "PRT"
          }
        },
        season_coverage_info: {
          season_id: "sr:season:55243",
          scheduled: 306,
          played: 252,
          max_coverage_level: "gold",
          max_covered: 252,
          min_coverage_level: "gold"
        }
      }
    ],
    totals: {
      matches_played: 248,
      substituted_in: 11,
      substituted_out: 10,
      goals_scored: 17,
      assists: 10,
      own_goals: 4,
      yellow_cards: 47,
      yellow_red_cards: 1,
      red_cards: 1,
      last_event_time: "2019-04-11T17:41:37+00:00"
    }
  }
};

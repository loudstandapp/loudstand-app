export const timeline = {
  generated_at: "2019-03-29T15:58:34+00:00",
  schema:
    "http://schemas.sportradar.com/bsa/soccer/v1/json/endpoints/soccer/match_timeline.json",
  sport_event: {
    id: "sr:match:14946187",
    scheduled: "2019-03-02T15:30:00+00:00",
    start_time_tbd: false,
    tournament_round: {
      type: "group",
      number: 24,
      phase: "main_round_1"
    },
    season: {
      id: "sr:season:55243",
      name: "Primeira Liga 18/19",
      start_date: "2018-08-10",
      end_date: "2019-05-19",
      year: "18/19",
      tournament_id: "sr:tournament:238"
    },
    tournament: {
      id: "sr:tournament:238",
      name: "Primeira Liga",
      sport: {
        id: "sr:sport:1",
        name: "Soccer"
      },
      category: {
        id: "sr:category:44",
        name: "Portugal",
        country_code: "PRT"
      }
    },
    competitors: [
      {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "CDN",
        qualifier: "home"
      },
      {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        country: "Portugal",
        country_code: "PRT",
        abbreviation: "TON",
        qualifier: "away"
      }
    ],
    venue: {
      id: "sr:venue:1335",
      name: "Estadio da Madeira",
      capacity: 5132,
      city_name: "Funchal",
      country_name: "Portugal",
      map_coordinates: "32.670834,-16.883333",
      country_code: "PRT"
    }
  },
  sport_event_conditions: {
    referee: {
      id: "sr:referee:89559",
      name: "Malheiro, Helder",
      nationality: "Portugal",
      country_code: "PRT"
    },
    venue: {
      id: "sr:venue:1335",
      name: "Estadio da Madeira",
      capacity: 5132,
      city_name: "Funchal",
      country_name: "Portugal",
      map_coordinates: "32.670834,-16.883333",
      country_code: "PRT"
    },
    weather_info: {
      pitch: "medium",
      weather_conditions: "medium"
    }
  },
  sport_event_status: {
    status: "closed",
    match_status: "ended",
    home_score: 3,
    away_score: 2,
    winner_id: "sr:competitor:3013",
    period_scores: [
      {
        home_score: 1,
        away_score: 1,
        type: "regular_period",
        number: 1
      },
      {
        home_score: 2,
        away_score: 1,
        type: "regular_period",
        number: 2
      }
    ],
    ball_locations: [
      {
        x: "79",
        y: "59",
        team: "away"
      },
      {
        x: "91",
        y: "54",
        team: "away"
      },
      {
        x: "91",
        y: "54",
        team: "home"
      },
      {
        x: "84",
        y: "51",
        team: "home"
      }
    ]
  },
  coverage_info: {
    level: "gold",
    live_coverage: true,
    coverage: {
      basic_score: true,
      key_events: true,
      detailed_events: true,
      lineups: true,
      commentary: true
    }
  },
  timeline: [
    {
      id: 527353975,
      type: "match_started",
      time: "2019-03-02T15:30:46+00:00"
    },
    {
      id: 527353977,
      type: "period_start",
      time: "2019-03-02T15:30:46+00:00",
      period: 1,
      period_type: "regular_period",
      period_name: "1st half"
    },
    {
      id: 527354159,
      type: "throw_in",
      time: "2019-03-02T15:30:57+00:00",
      match_time: 1,
      match_clock: "0:11",
      team: "home",
      x: 22,
      y: 97,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527354741,
      type: "throw_in",
      time: "2019-03-02T15:31:33+00:00",
      match_time: 1,
      match_clock: "0:47",
      team: "home",
      x: 52,
      y: 98,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527355047,
      type: "offside",
      time: "2019-03-02T15:31:50+00:00",
      match_time: 2,
      match_clock: "1:03",
      player: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      team: "home",
      x: 72,
      y: 76,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527355465,
      type: "throw_in",
      time: "2019-03-02T15:32:16+00:00",
      match_time: 2,
      match_clock: "1:30",
      team: "home",
      x: 23,
      y: 96,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527355675,
      type: "free_kick",
      time: "2019-03-02T15:32:35+00:00",
      match_time: 2,
      match_clock: "1:49",
      team: "home",
      x: 36,
      y: 60,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527356161,
      type: "throw_in",
      time: "2019-03-02T15:33:17+00:00",
      match_time: 3,
      match_clock: "2:31",
      team: "away",
      x: 38,
      y: 98,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527356733,
      type: "throw_in",
      time: "2019-03-02T15:33:59+00:00",
      match_time: 4,
      match_clock: "3:13",
      team: "away",
      x: 26,
      y: 97,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527356987,
      type: "throw_in",
      time: "2019-03-02T15:34:18+00:00",
      match_time: 4,
      match_clock: "3:32",
      team: "away",
      x: 28,
      y: 96,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527357733,
      type: "free_kick",
      time: "2019-03-02T15:35:20+00:00",
      match_time: 5,
      match_clock: "4:34",
      team: "home",
      x: 28,
      y: 81,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527358391,
      type: "throw_in",
      time: "2019-03-02T15:36:04+00:00",
      match_time: 6,
      match_clock: "5:18",
      team: "away",
      x: 69,
      y: 96,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527358889,
      type: "throw_in",
      time: "2019-03-02T15:36:45+00:00",
      match_time: 6,
      match_clock: "5:59",
      team: "away",
      x: 76,
      y: 97,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527359901,
      type: "goal_kick",
      time: "2019-03-02T15:38:09+00:00",
      match_time: 8,
      match_clock: "7:22",
      team: "home",
      x: 5,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527360627,
      type: "corner_kick",
      time: "2019-03-02T15:39:12+00:00",
      match_time: 9,
      match_clock: "8:26",
      player: {
        id: "sr:player:170483",
        name: "Goncalves, Vitor"
      },
      team: "home",
      x: 95,
      y: 90,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527361459,
      type: "corner_kick",
      time: "2019-03-02T15:40:21+00:00",
      match_time: 10,
      match_clock: "9:34",
      player: {
        id: "sr:player:170483",
        name: "Goncalves, Vitor"
      },
      team: "home",
      x: 95,
      y: 90,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527361833,
      type: "throw_in",
      time: "2019-03-02T15:40:54+00:00",
      match_time: 11,
      match_clock: "10:08",
      team: "away",
      x: 90,
      y: 97,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527362335,
      type: "free_kick",
      time: "2019-03-02T15:41:38+00:00",
      match_time: 11,
      match_clock: "10:51",
      team: "home",
      x: 80,
      y: 79,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527363267,
      type: "goal_kick",
      time: "2019-03-02T15:42:49+00:00",
      match_time: 13,
      match_clock: "12:03",
      team: "away",
      x: 95,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527363261,
      type: "shot_off_target",
      time: "2019-03-02T15:42:49+00:00",
      match_time: 13,
      match_clock: "12:03",
      player: {
        id: "sr:player:267461",
        name: "Palocevic, Aleksandar"
      },
      team: "home",
      x: 85,
      y: 53,
      period: 1,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527363747,
      type: "offside",
      time: "2019-03-02T15:43:31+00:00",
      match_time: 13,
      match_clock: "12:45",
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: "away",
      x: 11,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527364143,
      type: "injury",
      time: "2019-03-02T15:44:02+00:00",
      match_time: 14,
      match_clock: "13:16",
      player: {
        id: "sr:player:177175",
        name: "Rosic, Lazar"
      },
      team: "home",
      x: 11,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527365031,
      type: "goal_kick",
      time: "2019-03-02T15:45:01+00:00",
      match_time: 15,
      match_clock: "14:15",
      team: "away",
      x: 95,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527365177,
      type: "injury_return",
      time: "2019-03-02T15:45:07+00:00",
      match_time: 15,
      match_clock: "14:20",
      player: {
        id: "sr:player:177175",
        name: "Rosic, Lazar"
      },
      team: "home",
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527365725,
      type: "throw_in",
      time: "2019-03-02T15:45:39+00:00",
      match_time: 15,
      match_clock: "14:53",
      team: "away",
      x: 62,
      y: 3,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527366061,
      type: "throw_in",
      time: "2019-03-02T15:46:07+00:00",
      match_time: 16,
      match_clock: "15:21",
      team: "away",
      x: 17,
      y: 97,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527366707,
      type: "corner_kick",
      time: "2019-03-02T15:46:50+00:00",
      match_time: 17,
      match_clock: "16:04",
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: "away",
      x: 5,
      y: 90,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527366787,
      type: "shot_off_target",
      time: "2019-03-02T15:46:56+00:00",
      match_time: 17,
      match_clock: "16:10",
      player: {
        id: "sr:player:252717",
        name: "Alves, Ricardo"
      },
      team: "away",
      x: 13,
      y: 51,
      period: 1,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527366791,
      type: "goal_kick",
      time: "2019-03-02T15:46:56+00:00",
      match_time: 17,
      match_clock: "16:10",
      team: "home",
      x: 5,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527367671,
      type: "throw_in",
      time: "2019-03-02T15:47:59+00:00",
      match_time: 18,
      match_clock: "17:13",
      team: "home",
      x: 66,
      y: 97,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527367865,
      type: "free_kick",
      time: "2019-03-02T15:48:17+00:00",
      match_time: 18,
      match_clock: "17:31",
      team: "home",
      x: 14,
      y: 49,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527368147,
      type: "injury",
      time: "2019-03-02T15:48:45+00:00",
      match_time: 18,
      match_clock: "17:59",
      player: {
        id: "sr:player:329103",
        name: "Guimaraes, Daniel"
      },
      team: "home",
      x: 14,
      y: 49,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527368199,
      type: "injury",
      time: "2019-03-02T15:48:48+00:00",
      match_time: 19,
      match_clock: "18:02",
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: "away",
      x: 14,
      y: 49,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527368635,
      type: "injury_return",
      time: "2019-03-02T15:49:41+00:00",
      match_time: 19,
      match_clock: "18:54",
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: "away",
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527368663,
      type: "injury_return",
      time: "2019-03-02T15:49:45+00:00",
      match_time: 19,
      match_clock: "18:59",
      player: {
        id: "sr:player:329103",
        name: "Guimaraes, Daniel"
      },
      team: "home",
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527368819,
      type: "shot_off_target",
      time: "2019-03-02T15:50:03+00:00",
      match_time: 20,
      match_clock: "19:17",
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: "away",
      x: 8,
      y: 32,
      period: 1,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527368827,
      type: "goal_kick",
      time: "2019-03-02T15:50:03+00:00",
      match_time: 20,
      match_clock: "19:17",
      team: "home",
      x: 5,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527369347,
      type: "penalty_awarded",
      time: "2019-03-02T15:51:05+00:00",
      match_time: 21,
      match_clock: "20:17",
      player: {
        id: "sr:player:329103",
        name: "Guimaraes, Daniel"
      },
      team: "away",
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527369425,
      type: "yellow_card",
      time: "2019-03-02T15:51:16+00:00",
      match_time: 21,
      match_clock: "20:29",
      player: {
        id: "sr:player:329103",
        name: "Guimaraes, Daniel"
      },
      team: "home",
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527369859,
      type: "score_change",
      time: "2019-03-02T15:52:15+00:00",
      match_time: 22,
      match_clock: "21:28",
      team: "away",
      x: 10,
      y: 48,
      period: 1,
      period_type: "regular_period",
      home_score: 0,
      away_score: 1,
      goal_scorer: {
        id: "sr:player:126076",
        name: "Tomane",
        method: "penalty"
      }
    },
    {
      id: 527370483,
      type: "shot_off_target",
      time: "2019-03-02T15:53:43+00:00",
      match_time: 23,
      match_clock: "22:57",
      player: {
        id: "sr:player:126076",
        name: "Tomane"
      },
      team: "away",
      x: 7,
      y: 34,
      period: 1,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527370485,
      type: "goal_kick",
      time: "2019-03-02T15:53:43+00:00",
      match_time: 23,
      match_clock: "22:57",
      team: "home",
      x: 5,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527371007,
      type: "throw_in",
      time: "2019-03-02T15:54:44+00:00",
      match_time: 24,
      match_clock: "23:58",
      team: "away",
      x: 79,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527371179,
      type: "throw_in",
      time: "2019-03-02T15:55:12+00:00",
      match_time: 25,
      match_clock: "24:26",
      team: "home",
      x: 32,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527371443,
      type: "throw_in",
      time: "2019-03-02T15:55:49+00:00",
      match_time: 26,
      match_clock: "25:03",
      team: "away",
      x: 31,
      y: 1,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527371521,
      type: "goal_kick",
      time: "2019-03-02T15:56:06+00:00",
      match_time: 26,
      match_clock: "25:20",
      team: "home",
      x: 5,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527371945,
      type: "corner_kick",
      time: "2019-03-02T15:57:03+00:00",
      match_time: 27,
      match_clock: "26:17",
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: "away",
      x: 5,
      y: 5,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527371997,
      type: "shot_saved",
      time: "2019-03-02T15:57:09+00:00",
      match_time: 27,
      match_clock: "26:23",
      team: "home",
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527371993,
      type: "shot_on_target",
      time: "2019-03-02T15:57:09+00:00",
      match_time: 27,
      match_clock: "26:22",
      player: {
        id: "sr:player:5883",
        name: "Costa, Ricardo"
      },
      team: "away",
      x: 13,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527372609,
      type: "free_kick",
      time: "2019-03-02T15:58:34+00:00",
      match_time: 28,
      match_clock: "27:48",
      team: "away",
      x: 60,
      y: 62,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527373263,
      type: "corner_kick",
      time: "2019-03-02T15:59:47+00:00",
      match_time: 30,
      match_clock: "29:01",
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: "away",
      x: 5,
      y: 90,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527373393,
      type: "throw_in",
      time: "2019-03-02T15:59:55+00:00",
      match_time: 30,
      match_clock: "29:09",
      team: "home",
      x: 18,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527373685,
      type: "throw_in",
      time: "2019-03-02T16:00:17+00:00",
      match_time: 30,
      match_clock: "29:30",
      team: "away",
      x: 59,
      y: 97,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527374313,
      type: "throw_in",
      time: "2019-03-02T16:01:03+00:00",
      match_time: 31,
      match_clock: "30:17",
      team: "home",
      x: 71,
      y: 99,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527374415,
      type: "goal_kick",
      time: "2019-03-02T16:01:16+00:00",
      match_time: 31,
      match_clock: "30:30",
      team: "away",
      x: 95,
      y: 50,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527374715,
      type: "throw_in",
      time: "2019-03-02T16:01:43+00:00",
      match_time: 31,
      match_clock: "30:56",
      team: "home",
      x: 27,
      y: 3,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527374973,
      type: "free_kick",
      time: "2019-03-02T16:02:04+00:00",
      match_time: 32,
      match_clock: "31:17",
      team: "home",
      x: 57,
      y: 25,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527375739,
      type: "throw_in",
      time: "2019-03-02T16:02:59+00:00",
      match_time: 33,
      match_clock: "32:13",
      team: "home",
      x: 93,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527375893,
      type: "shot_saved",
      time: "2019-03-02T16:03:12+00:00",
      match_time: 33,
      match_clock: "32:26",
      team: "away",
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527375887,
      type: "shot_on_target",
      time: "2019-03-02T16:03:12+00:00",
      match_time: 33,
      match_clock: "32:26",
      player: {
        id: "sr:player:791645",
        name: "Witi"
      },
      team: "home",
      x: 84,
      y: 32,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527376957,
      type: "throw_in",
      time: "2019-03-02T16:04:18+00:00",
      match_time: 34,
      match_clock: "33:32",
      team: "home",
      x: 24,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527377345,
      type: "throw_in",
      time: "2019-03-02T16:04:45+00:00",
      match_time: 34,
      match_clock: "33:59",
      team: "home",
      x: 71,
      y: 97,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527377815,
      type: "throw_in",
      time: "2019-03-02T16:05:08+00:00",
      match_time: 35,
      match_clock: "34:22",
      team: "away",
      x: 55,
      y: 2,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527378045,
      type: "free_kick",
      time: "2019-03-02T16:05:25+00:00",
      match_time: 35,
      match_clock: "34:39",
      team: "home",
      x: 56,
      y: 26,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527378215,
      type: "yellow_card",
      time: "2019-03-02T16:05:41+00:00",
      match_time: 35,
      match_clock: "34:54",
      player: {
        id: "sr:player:61180",
        name: "Monteiro, Bruno"
      },
      team: "away",
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527378831,
      type: "score_change",
      time: "2019-03-02T16:06:25+00:00",
      match_time: 36,
      match_clock: "35:38",
      team: "home",
      x: 92,
      y: 48,
      period: 1,
      period_type: "regular_period",
      home_score: 1,
      away_score: 1,
      goal_scorer: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      assist: {
        id: "sr:player:791645",
        type: "primary",
        name: "Witi"
      }
    },
    {
      id: 527380693,
      type: "free_kick",
      time: "2019-03-02T16:08:30+00:00",
      match_time: 38,
      match_clock: "37:44",
      team: "away",
      x: 20,
      y: 72,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527382179,
      type: "corner_kick",
      time: "2019-03-02T16:10:09+00:00",
      match_time: 40,
      match_clock: "39:23",
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: "away",
      x: 5,
      y: 5,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527382769,
      type: "corner_kick",
      time: "2019-03-02T16:10:51+00:00",
      match_time: 41,
      match_clock: "40:04",
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: "away",
      x: 5,
      y: 90,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527383239,
      type: "throw_in",
      time: "2019-03-02T16:11:20+00:00",
      match_time: 41,
      match_clock: "40:35",
      team: "home",
      x: 78,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527383699,
      type: "throw_in",
      time: "2019-03-02T16:11:51+00:00",
      match_time: 42,
      match_clock: "41:04",
      team: "home",
      x: 64,
      y: 3,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527383881,
      type: "injury",
      time: "2019-03-02T16:12:03+00:00",
      match_time: 42,
      match_clock: "41:17",
      player: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      team: "home",
      x: 64,
      y: 3,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527385159,
      type: "throw_in",
      time: "2019-03-02T16:13:28+00:00",
      match_time: 43,
      match_clock: "42:41",
      team: "away",
      x: 14,
      y: 97,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527385165,
      type: "injury_return",
      time: "2019-03-02T16:13:29+00:00",
      match_time: 43,
      match_clock: "42:42",
      player: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      team: "home",
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527385625,
      type: "throw_in",
      time: "2019-03-02T16:14:00+00:00",
      match_time: 44,
      match_clock: "43:13",
      team: "away",
      x: 29,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527385715,
      type: "injury",
      time: "2019-03-02T16:14:07+00:00",
      match_time: 44,
      match_clock: "43:20",
      player: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      team: "home",
      x: 29,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527386429,
      type: "substitution",
      time: "2019-03-02T16:14:53+00:00",
      match_time: 44,
      team: "home",
      period: 1,
      period_type: "regular_period",
      player_out: {
        id: "sr:player:537040",
        name: "Riascos, Brayan"
      },
      player_in: {
        id: "sr:player:1003011",
        name: "Hamzaoui, Okacha"
      }
    },
    {
      id: 527387849,
      type: "throw_in",
      time: "2019-03-02T16:16:11+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "1",
      team: "home",
      x: 25,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527388043,
      type: "throw_in",
      time: "2019-03-02T16:16:26+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "1",
      team: "home",
      x: 63,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527388141,
      type: "throw_in",
      time: "2019-03-02T16:16:33+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "1",
      team: "home",
      x: 80,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527387563,
      type: "injury_time_shown",
      time: "2019-03-02T16:15:54+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "1",
      period: 1,
      period_type: "regular_period",
      injury_time_announced: 4
    },
    {
      id: 527387509,
      type: "throw_in",
      time: "2019-03-02T16:15:50+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "1",
      team: "away",
      x: 54,
      y: 18,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527388395,
      type: "throw_in",
      time: "2019-03-02T16:16:49+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "2",
      team: "away",
      x: 94,
      y: 4,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527389551,
      type: "free_kick",
      time: "2019-03-02T16:18:13+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "3",
      team: "home",
      x: 75,
      y: 11,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527390233,
      type: "throw_in",
      time: "2019-03-02T16:18:54+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "4",
      team: "home",
      x: 92,
      y: 2,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527390487,
      type: "corner_kick",
      time: "2019-03-02T16:19:12+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "4",
      player: {
        id: "sr:player:170483",
        name: "Goncalves, Vitor"
      },
      team: "home",
      x: 95,
      y: 90,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527390739,
      type: "corner_kick",
      time: "2019-03-02T16:19:33+00:00",
      match_time: 45,
      match_clock: "45:00",
      stoppage_time: "4",
      player: {
        id: "sr:player:791645",
        name: "Witi"
      },
      team: "home",
      x: 95,
      y: 5,
      period: 1,
      period_type: "regular_period"
    },
    {
      id: 527390973,
      type: "break_start",
      time: "2019-03-02T16:19:50+00:00",
      period_name: "Pause"
    },
    {
      id: 527404529,
      type: "period_start",
      time: "2019-03-02T16:36:53+00:00",
      period: 2,
      period_type: "regular_period",
      period_name: "2nd half"
    },
    {
      id: 527404887,
      type: "throw_in",
      time: "2019-03-02T16:37:20+00:00",
      match_time: 46,
      match_clock: "45:27",
      team: "away",
      x: 42,
      y: 95,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527405033,
      type: "free_kick",
      time: "2019-03-02T16:37:31+00:00",
      match_time: 46,
      match_clock: "45:38",
      team: "home",
      x: 30,
      y: 62,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527405447,
      type: "goal_kick",
      time: "2019-03-02T16:38:04+00:00",
      match_time: 47,
      match_clock: "46:10",
      team: "away",
      x: 95,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527406025,
      type: "throw_in",
      time: "2019-03-02T16:38:43+00:00",
      match_time: 47,
      match_clock: "46:50",
      team: "away",
      x: 44,
      y: 96,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527406249,
      type: "free_kick",
      time: "2019-03-02T16:39:02+00:00",
      match_time: 48,
      match_clock: "47:09",
      team: "home",
      x: 36,
      y: 74,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527406699,
      type: "free_kick",
      time: "2019-03-02T16:39:34+00:00",
      match_time: 48,
      match_clock: "47:40",
      team: "home",
      x: 94,
      y: 84,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527407339,
      type: "corner_kick",
      time: "2019-03-02T16:40:26+00:00",
      match_time: 49,
      match_clock: "48:33",
      player: {
        id: "sr:player:267461",
        name: "Palocevic, Aleksandar"
      },
      team: "home",
      x: 95,
      y: 5,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527407397,
      type: "shot_off_target",
      time: "2019-03-02T16:40:31+00:00",
      match_time: 49,
      match_clock: "48:38",
      player: {
        id: "sr:player:177175",
        name: "Rosic, Lazar"
      },
      team: "home",
      x: 90,
      y: 54,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527407399,
      type: "goal_kick",
      time: "2019-03-02T16:40:31+00:00",
      match_time: 49,
      match_clock: "48:38",
      team: "away",
      x: 95,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527408501,
      type: "offside",
      time: "2019-03-02T16:41:48+00:00",
      match_time: 50,
      match_clock: "49:55",
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: "away",
      x: 22,
      y: 74,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527409173,
      type: "throw_in",
      time: "2019-03-02T16:42:42+00:00",
      match_time: 51,
      match_clock: "50:49",
      team: "home",
      x: 37,
      y: 97,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527409329,
      type: "throw_in",
      time: "2019-03-02T16:42:48+00:00",
      match_time: 51,
      match_clock: "50:55",
      team: "away",
      x: 65,
      y: 96,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527409947,
      type: "shot_off_target",
      time: "2019-03-02T16:43:39+00:00",
      match_time: 52,
      match_clock: "51:46",
      player: {
        id: "sr:player:1003011",
        name: "Hamzaoui, Okacha"
      },
      team: "home",
      x: 93,
      y: 71,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527409953,
      type: "goal_kick",
      time: "2019-03-02T16:43:39+00:00",
      match_time: 52,
      match_clock: "51:46",
      team: "away",
      x: 95,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527410827,
      type: "free_kick",
      time: "2019-03-02T16:44:38+00:00",
      match_time: 53,
      match_clock: "52:44",
      team: "home",
      x: 61,
      y: 78,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527411981,
      type: "shot_off_target",
      time: "2019-03-02T16:45:54+00:00",
      match_time: 55,
      match_clock: "54:01",
      player: {
        id: "sr:player:126076",
        name: "Tomane"
      },
      team: "away",
      x: 13,
      y: 65,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527411987,
      type: "goal_kick",
      time: "2019-03-02T16:45:54+00:00",
      match_time: 55,
      match_clock: "54:01",
      team: "home",
      x: 5,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527412505,
      type: "throw_in",
      time: "2019-03-02T16:46:33+00:00",
      match_time: 55,
      match_clock: "54:40",
      team: "away",
      x: 75,
      y: 97,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527412919,
      type: "offside",
      time: "2019-03-02T16:47:03+00:00",
      match_time: 56,
      match_clock: "55:09",
      player: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      team: "away",
      x: 29,
      y: 78,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527413399,
      type: "free_kick",
      time: "2019-03-02T16:47:46+00:00",
      match_time: 56,
      match_clock: "55:52",
      team: "home",
      x: 68,
      y: 83,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527414891,
      type: "corner_kick",
      time: "2019-03-02T16:50:00+00:00",
      match_time: 59,
      match_clock: "58:07",
      player: {
        id: "sr:player:170483",
        name: "Goncalves, Vitor"
      },
      team: "home",
      x: 95,
      y: 5,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527415053,
      type: "shot_off_target",
      time: "2019-03-02T16:50:15+00:00",
      match_time: 59,
      match_clock: "58:21",
      player: {
        id: "sr:player:170483",
        name: "Goncalves, Vitor"
      },
      team: "home",
      x: 85,
      y: 30,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527415059,
      type: "goal_kick",
      time: "2019-03-02T16:50:15+00:00",
      match_time: 59,
      match_clock: "58:21",
      team: "away",
      x: 95,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527415453,
      type: "substitution",
      time: "2019-03-02T16:50:57+00:00",
      match_time: 59,
      team: "away",
      period: 2,
      period_type: "regular_period",
      player_out: {
        id: "sr:player:845913",
        name: "Murillo, Jhon"
      },
      player_in: {
        id: "sr:player:339695",
        name: "Delgado, Juan"
      }
    },
    {
      id: 527415807,
      type: "shot_on_target",
      time: "2019-03-02T16:51:33+00:00",
      match_time: 60,
      match_clock: "59:40",
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: "away",
      x: 28,
      y: 34,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527415827,
      type: "shot_saved",
      time: "2019-03-02T16:51:34+00:00",
      match_time: 60,
      match_clock: "59:40",
      team: "home",
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527416155,
      type: "corner_kick",
      time: "2019-03-02T16:52:04+00:00",
      match_time: 61,
      match_clock: "60:11",
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: "away",
      x: 5,
      y: 5,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527416199,
      type: "shot_off_target",
      time: "2019-03-02T16:52:09+00:00",
      match_time: 61,
      match_clock: "60:16",
      player: {
        id: "sr:player:252717",
        name: "Alves, Ricardo"
      },
      team: "away",
      x: 14,
      y: 62,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527416207,
      type: "goal_kick",
      time: "2019-03-02T16:52:09+00:00",
      match_time: 61,
      match_clock: "60:16",
      team: "home",
      x: 5,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527416507,
      type: "free_kick",
      time: "2019-03-02T16:52:39+00:00",
      match_time: 61,
      match_clock: "60:46",
      team: "home",
      x: 70,
      y: 47,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527416653,
      type: "yellow_card",
      time: "2019-03-02T16:52:43+00:00",
      match_time: 62,
      player: {
        id: "sr:player:856980",
        name: "Jaquite, Joao"
      },
      team: "away",
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527417157,
      type: "score_change",
      time: "2019-03-02T16:53:36+00:00",
      match_time: 62,
      match_clock: "61:40",
      team: "home",
      x: 93,
      y: 66,
      period: 2,
      period_type: "regular_period",
      home_score: 2,
      away_score: 1,
      goal_scorer: {
        id: "sr:player:12676",
        name: "Tissone, Fernando"
      }
    },
    {
      id: 527417991,
      type: "goal_kick",
      time: "2019-03-02T16:54:54+00:00",
      match_time: 64,
      match_clock: "63:01",
      team: "home",
      x: 5,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527418809,
      type: "free_kick",
      time: "2019-03-02T16:56:25+00:00",
      match_time: 65,
      match_clock: "64:32",
      team: "away",
      x: 90,
      y: 45,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527419171,
      type: "score_change",
      time: "2019-03-02T16:57:12+00:00",
      match_time: 66,
      match_clock: "65:18",
      team: "away",
      x: 9,
      y: 33,
      period: 2,
      period_type: "regular_period",
      home_score: 2,
      away_score: 2,
      goal_scorer: {
        id: "sr:player:862680",
        name: "Moufi, Fahd",
        method: "header"
      },
      assist: {
        id: "sr:player:156319",
        type: "primary",
        name: "Joaozinho"
      }
    },
    {
      id: 527420079,
      type: "throw_in",
      time: "2019-03-02T16:58:39+00:00",
      match_time: 67,
      match_clock: "66:46",
      team: "home",
      x: 48,
      y: 3,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527420327,
      type: "free_kick",
      time: "2019-03-02T16:59:09+00:00",
      match_time: 68,
      match_clock: "67:16",
      team: "away",
      x: 86,
      y: 62,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527420585,
      type: "injury",
      time: "2019-03-02T16:59:46+00:00",
      match_time: 68,
      match_clock: "67:53",
      player: {
        id: "sr:player:856980",
        name: "Jaquite, Joao"
      },
      team: "away",
      x: 86,
      y: 62,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527421441,
      type: "free_kick",
      time: "2019-03-02T17:01:00+00:00",
      match_time: 70,
      match_clock: "69:06",
      team: "away",
      x: 59,
      y: 77,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527421609,
      type: "injury_return",
      time: "2019-03-02T17:01:16+00:00",
      match_time: 70,
      match_clock: "69:22",
      player: {
        id: "sr:player:856980",
        name: "Jaquite, Joao"
      },
      team: "away",
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527422149,
      type: "goal_kick",
      time: "2019-03-02T17:01:46+00:00",
      match_time: 70,
      match_clock: "69:52",
      team: "home",
      x: 5,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527422487,
      type: "free_kick",
      time: "2019-03-02T17:02:16+00:00",
      match_time: 71,
      match_clock: "70:22",
      team: "home",
      x: 67,
      y: 74,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527423249,
      type: "free_kick",
      time: "2019-03-02T17:02:57+00:00",
      match_time: 72,
      match_clock: "71:03",
      team: "away",
      x: 86,
      y: 35,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527424681,
      type: "corner_kick",
      time: "2019-03-02T17:04:11+00:00",
      match_time: 73,
      match_clock: "72:18",
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: "away",
      x: 5,
      y: 90,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527425057,
      type: "shot_off_target",
      time: "2019-03-02T17:04:26+00:00",
      match_time: 73,
      match_clock: "72:33",
      player: {
        id: "sr:player:251217",
        name: "Pedro, Joao"
      },
      team: "away",
      x: 17,
      y: 67,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527425081,
      type: "goal_kick",
      time: "2019-03-02T17:04:27+00:00",
      match_time: 73,
      match_clock: "72:33",
      team: "home",
      x: 5,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527425459,
      type: "free_kick",
      time: "2019-03-02T17:04:54+00:00",
      match_time: 74,
      match_clock: "73:00",
      team: "home",
      x: 71,
      y: 74,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527427439,
      type: "corner_kick",
      time: "2019-03-02T17:06:30+00:00",
      match_time: 75,
      match_clock: "74:37",
      player: {
        id: "sr:player:791645",
        name: "Witi"
      },
      team: "home",
      x: 95,
      y: 5,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527427565,
      type: "free_kick",
      time: "2019-03-02T17:06:40+00:00",
      match_time: 75,
      match_clock: "74:46",
      team: "away",
      x: 92,
      y: 39,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527428377,
      type: "throw_in",
      time: "2019-03-02T17:07:31+00:00",
      match_time: 76,
      match_clock: "75:37",
      team: "away",
      x: 77,
      y: 4,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527428627,
      type: "substitution",
      time: "2019-03-02T17:07:50+00:00",
      match_time: 76,
      match_clock: "75:57",
      team: "home",
      period: 2,
      period_type: "regular_period",
      player_out: {
        id: "sr:player:328335",
        name: "Rashidov, Sardor"
      },
      player_in: {
        id: "sr:player:546558",
        name: "Rochez, Bryan"
      }
    },
    {
      id: 527428689,
      type: "injury",
      time: "2019-03-02T17:07:55+00:00",
      match_time: 77,
      match_clock: "76:01",
      player: {
        id: "sr:player:862680",
        name: "Moufi, Fahd"
      },
      team: "away",
      x: 77,
      y: 4,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527431449,
      type: "free_kick",
      time: "2019-03-02T17:10:20+00:00",
      match_time: 79,
      match_clock: "78:26",
      team: "home",
      x: 62,
      y: 20,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527431511,
      type: "yellow_card",
      time: "2019-03-02T17:10:25+00:00",
      match_time: 79,
      match_clock: "78:30",
      player: {
        id: "sr:player:251217",
        name: "Pedro, Joao"
      },
      team: "away",
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527431897,
      type: "substitution",
      time: "2019-03-02T17:10:55+00:00",
      match_time: 79,
      team: "away",
      period: 2,
      period_type: "regular_period",
      player_out: {
        id: "sr:player:862680",
        name: "Moufi, Fahd"
      },
      player_in: {
        id: "sr:player:343487",
        name: "Reis, Joao"
      }
    },
    {
      id: 527431971,
      type: "shot_off_target",
      time: "2019-03-02T17:11:02+00:00",
      match_time: 80,
      match_clock: "79:08",
      player: {
        id: "sr:player:1003011",
        name: "Hamzaoui, Okacha"
      },
      team: "home",
      x: 87,
      y: 62,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527431981,
      type: "goal_kick",
      time: "2019-03-02T17:11:02+00:00",
      match_time: 80,
      match_clock: "79:09",
      team: "away",
      x: 95,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527432499,
      type: "offside",
      time: "2019-03-02T17:11:35+00:00",
      match_time: 80,
      match_clock: "79:41",
      player: {
        id: "sr:player:126076",
        name: "Tomane"
      },
      team: "away",
      x: 32,
      y: 70,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527432957,
      type: "throw_in",
      time: "2019-03-02T17:11:58+00:00",
      match_time: 81,
      match_clock: "80:04",
      team: "home",
      x: 90,
      y: 4,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527433607,
      type: "throw_in",
      time: "2019-03-02T17:12:40+00:00",
      match_time: 81,
      match_clock: "80:46",
      team: "home",
      x: 72,
      y: 4,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527435353,
      type: "throw_in",
      time: "2019-03-02T17:14:02+00:00",
      match_time: 83,
      match_clock: "82:09",
      team: "home",
      x: 41,
      y: 3,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527436211,
      type: "score_change",
      time: "2019-03-02T17:14:32+00:00",
      match_time: 83,
      match_clock: "82:37",
      team: "home",
      x: 95,
      y: 41,
      period: 2,
      period_type: "regular_period",
      home_score: 3,
      away_score: 2,
      goal_scorer: {
        id: "sr:player:170483",
        name: "Goncalves, Vitor"
      },
      assist: {
        id: "sr:player:868640",
        type: "primary",
        name: "Kalindi"
      }
    },
    {
      id: 527437629,
      type: "throw_in",
      time: "2019-03-02T17:15:51+00:00",
      match_time: 84,
      match_clock: "83:58",
      team: "home",
      x: 28,
      y: 96,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527438127,
      type: "free_kick",
      time: "2019-03-02T17:16:18+00:00",
      match_time: 85,
      match_clock: "84:25",
      team: "away",
      x: 82,
      y: 74,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527438553,
      type: "free_kick",
      time: "2019-03-02T17:16:47+00:00",
      match_time: 85,
      match_clock: "84:53",
      team: "home",
      x: 58,
      y: 66,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527438875,
      type: "substitution",
      time: "2019-03-02T17:17:08+00:00",
      match_time: 86,
      match_clock: "85:14",
      team: "away",
      period: 2,
      period_type: "regular_period",
      player_out: {
        id: "sr:player:251217",
        name: "Pedro, Joao"
      },
      player_in: {
        id: "sr:player:247903",
        name: "Arango, Chicho"
      }
    },
    {
      id: 527439219,
      type: "free_kick",
      time: "2019-03-02T17:17:32+00:00",
      match_time: 86,
      match_clock: "85:38",
      team: "away",
      x: 83,
      y: 15,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527439643,
      type: "shot_on_target",
      time: "2019-03-02T17:18:10+00:00",
      match_time: 87,
      match_clock: "86:16",
      player: {
        id: "sr:player:791645",
        name: "Witi"
      },
      team: "home",
      x: 90,
      y: 28,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527439649,
      type: "shot_saved",
      time: "2019-03-02T17:18:10+00:00",
      match_time: 87,
      match_clock: "86:17",
      team: "away",
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527440157,
      type: "corner_kick",
      time: "2019-03-02T17:18:52+00:00",
      match_time: 87,
      match_clock: "86:58",
      player: {
        id: "sr:player:267461",
        name: "Palocevic, Aleksandar"
      },
      team: "home",
      x: 95,
      y: 5,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527440243,
      type: "shot_off_target",
      time: "2019-03-02T17:19:00+00:00",
      match_time: 88,
      match_clock: "87:07",
      player: {
        id: "sr:player:546558",
        name: "Rochez, Bryan"
      },
      team: "home",
      x: 91,
      y: 66,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527440249,
      type: "goal_kick",
      time: "2019-03-02T17:19:00+00:00",
      match_time: 88,
      match_clock: "87:07",
      team: "away",
      x: 95,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527441159,
      type: "throw_in",
      time: "2019-03-02T17:20:19+00:00",
      match_time: 89,
      match_clock: "88:26",
      team: "away",
      x: 69,
      y: 97,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527441381,
      type: "free_kick",
      time: "2019-03-02T17:20:36+00:00",
      match_time: 89,
      match_clock: "88:43",
      team: "away",
      x: 36,
      y: 70,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527441453,
      type: "yellow_card",
      time: "2019-03-02T17:20:40+00:00",
      match_time: 89,
      match_clock: "88:46",
      player: {
        id: "sr:player:868640",
        name: "Kalindi"
      },
      team: "home",
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527442037,
      type: "free_kick",
      time: "2019-03-02T17:21:29+00:00",
      match_time: 90,
      match_clock: "89:36",
      team: "home",
      x: 27,
      y: 27,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527442073,
      type: "yellow_card",
      time: "2019-03-02T17:21:33+00:00",
      match_time: 90,
      match_clock: "89:00",
      player: {
        id: "sr:player:343957",
        name: "Pite"
      },
      team: "away",
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527442391,
      type: "injury_time_shown",
      time: "2019-03-02T17:21:58+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "1",
      period: 2,
      period_type: "regular_period",
      injury_time_announced: 6
    },
    {
      id: 527442607,
      type: "goal_kick",
      time: "2019-03-02T17:22:16+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "1",
      team: "away",
      x: 95,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527442713,
      type: "offside",
      time: "2019-03-02T17:22:24+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "1",
      player: {
        id: "sr:player:1003011",
        name: "Hamzaoui, Okacha"
      },
      team: "home",
      x: 86,
      y: 29,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527443157,
      type: "throw_in",
      time: "2019-03-02T17:23:00+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "2",
      team: "away",
      x: 63,
      y: 3,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527443447,
      type: "free_kick",
      time: "2019-03-02T17:23:23+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "2",
      team: "home",
      x: 27,
      y: 66,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527443915,
      type: "free_kick",
      time: "2019-03-02T17:24:05+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "3",
      team: "home",
      x: 29,
      y: 36,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527444307,
      type: "free_kick",
      time: "2019-03-02T17:24:44+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "3",
      team: "home",
      x: 72,
      y: 64,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527445455,
      type: "substitution",
      time: "2019-03-02T17:26:28+00:00",
      match_time: 90,
      stoppage_time: "3",
      team: "home",
      period: 2,
      period_type: "regular_period",
      player_out: {
        id: "sr:player:267461",
        name: "Palocevic, Aleksandar"
      },
      player_in: {
        id: "sr:player:202084",
        name: "Kaka"
      }
    },
    {
      id: 527444887,
      type: "shot_off_target",
      time: "2019-03-02T17:25:37+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "4",
      player: {
        id: "sr:player:791645",
        name: "Witi"
      },
      team: "home",
      x: 72,
      y: 64,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527444889,
      type: "goal_kick",
      time: "2019-03-02T17:25:37+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "4",
      team: "away",
      x: 95,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527445963,
      type: "shot_off_target",
      time: "2019-03-02T17:27:09+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "6",
      player: {
        id: "sr:player:546558",
        name: "Rochez, Bryan"
      },
      team: "home",
      x: 92,
      y: 58,
      period: 2,
      period_type: "regular_period",
      outcome: "miss"
    },
    {
      id: 527445965,
      type: "goal_kick",
      time: "2019-03-02T17:27:09+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "6",
      team: "away",
      x: 95,
      y: 50,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527446445,
      type: "free_kick",
      time: "2019-03-02T17:27:47+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "6",
      team: "home",
      x: 16,
      y: 74,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527446955,
      type: "offside",
      time: "2019-03-02T17:28:28+00:00",
      match_time: 90,
      match_clock: "90:00",
      stoppage_time: "7",
      player: {
        id: "sr:player:1003011",
        name: "Hamzaoui, Okacha"
      },
      team: "home",
      x: 91,
      y: 54,
      period: 2,
      period_type: "regular_period"
    },
    {
      id: 527447051,
      type: "match_ended",
      time: "2019-03-02T17:28:36+00:00",
      match_clock: "90:00"
    }
  ],
  statistics: {
    teams: [
      {
        id: "sr:competitor:3013",
        name: "CD Nacional",
        abbreviation: "CDN",
        qualifier: "home",
        statistics: {
          ball_possession: 51,
          throw_ins: 23,
          offsides: 3,
          free_kicks: 26,
          goal_kicks: 10,
          corner_kicks: 8,
          shots_off_target: 8,
          shots_saved: 2,
          fouls: 10,
          shots_on_target: 5,
          shots_blocked: 2,
          injuries: 4,
          yellow_cards: 2
        },
        players: [
          {
            id: "sr:player:329103",
            name: "Guimaraes, Daniel",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:868640",
            name: "Kalindi",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 1,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1142164",
            name: "Julio Cesar",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:177175",
            name: "Rosic, Lazar",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:871486",
            name: "Cerqueira, Mauro",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:170483",
            name: "Goncalves, Vitor",
            nickname: "Vitinha",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:267461",
            name: "Palocevic, Aleksandar",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:12676",
            name: "Tissone, Fernando",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:791645",
            name: "Witi",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 1,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:328335",
            name: "Rashidov, Sardor",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:537040",
            name: "Riascos, Brayan",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:1003011",
            name: "Hamzaoui, Okacha",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:546558",
            name: "Rochez, Bryan",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:202084",
            name: "Kaka",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          }
        ]
      },
      {
        id: "sr:competitor:38396",
        name: "CD Tondela",
        abbreviation: "TON",
        qualifier: "away",
        statistics: {
          ball_possession: 49,
          free_kicks: 13,
          fouls: 22,
          throw_ins: 23,
          goal_kicks: 12,
          offsides: 4,
          corner_kicks: 7,
          shots_off_target: 6,
          shots_on_target: 4,
          shots_saved: 2,
          shots_blocked: 3,
          injuries: 3,
          yellow_cards: 4
        },
        players: [
          {
            id: "sr:player:116372",
            name: "Claudio Ramos",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:862680",
            name: "Moufi, Fahd",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:252717",
            name: "Alves, Ricardo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:5883",
            name: "Costa, Ricardo",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:156319",
            name: "Joaozinho",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 1,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:845913",
            name: "Murillo, Jhon",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:61180",
            name: "Monteiro, Bruno",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:856980",
            name: "Jaquite, Joao",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:343957",
            name: "Pite",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:251217",
            name: "Pedro, Joao",
            substituted_in: 0,
            substituted_out: 1,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 1,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:126076",
            name: "Tomane",
            substituted_in: 0,
            substituted_out: 0,
            goals_scored: 1,
            penalty_goals_scored: 1,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:339695",
            name: "Delgado, Juan",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:343487",
            name: "Reis, Joao",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          },
          {
            id: "sr:player:247903",
            name: "Arango, Chicho",
            substituted_in: 1,
            substituted_out: 0,
            goals_scored: 0,
            assists: 0,
            own_goals: 0,
            yellow_cards: 0,
            yellow_red_cards: 0,
            red_cards: 0
          }
        ]
      }
    ]
  }
};

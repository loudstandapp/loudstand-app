import { Change, EventContext } from "firebase-functions";
import { DocumentSnapshot } from "@google-cloud/firestore";
import {
  TeamStatisticsFans,
  ID,
  FavoriteTeams
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { updateNotificationSettingsFavoriteTeams } from "../notifications/settingsHandler";

async function adjustFans(teamIds: ID[], delta: number) {
  const db = firestore();
  for (const teamId of teamIds) {
    const ref = db
      .collection("teams")
      .doc(teamId)
      .collection("statistics")
      .doc("fans");
    await db.runTransaction(async transaction => {
      const snap = await transaction.get(ref);
      const data = snap.data() as TeamStatisticsFans | undefined;
      const currentValue = data ? data.number : 0;
      const number = currentValue + delta;
      await transaction.set(ref, { number }, { merge: true });
    });
  }
}

export function getAllFavoriteTeamIds(favorites: FavoriteTeams) {
  const all = [];
  for (const tournamentId in favorites.tournaments) {
    const [newestSeasonId] = Object.keys(
      favorites.tournaments[tournamentId].seasons
    )
      .sort()
      .slice(-1);
    if (!newestSeasonId) {
      continue;
    }
    const team =
      favorites.tournaments[tournamentId].seasons[newestSeasonId].team;
    team && all.push(team.id);
  }
  return all;
}

function getChanges(current?: FavoriteTeams, prev?: FavoriteTeams) {
  const prevTeamIds = prev ? getAllFavoriteTeamIds(prev) : [];
  const currentTeamIds = current ? getAllFavoriteTeamIds(current) : [];
  const added = currentTeamIds.filter(id => !prevTeamIds.includes(id));
  const removed = prevTeamIds.filter(id => !currentTeamIds.includes(id));
  return { added, removed };
}

export const handleFavoriteTeams = {
  onWrite: async (change: Change<DocumentSnapshot>, context: EventContext) => {
    const prev = change.before.data() as FavoriteTeams | undefined;
    const current = change.after.data() as FavoriteTeams | undefined;
    const { added, removed } = getChanges(current, prev);
    await adjustFans(added, 1);
    await adjustFans(removed, -1);
    if (current) {
      const allTeams = getAllFavoriteTeamIds(current);
      const { userId } = context.params;
      await updateNotificationSettingsFavoriteTeams(userId, allTeams);
    }
  },
  path: "users/{userId}/attributes/favoriteTeams"
};

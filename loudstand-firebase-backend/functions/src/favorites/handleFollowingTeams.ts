import { Change } from "firebase-functions";
import { DocumentSnapshot } from "@google-cloud/firestore";
import {
  TeamList,
  TeamStatisticsFollowers,
  ID
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";

async function adjustFollowers(teamIds: ID[], delta: number) {
  const db = firestore();
  for (const teamId of teamIds) {
    const ref = db
      .collection("teams")
      .doc(teamId)
      .collection("statistics")
      .doc("followers");
    await db.runTransaction(async transaction => {
      const snap = await transaction.get(ref);
      const data = snap.data() as TeamStatisticsFollowers | undefined;
      const currentValue = data ? data.number : 0;
      const number = currentValue + delta;
      await transaction.set(ref, { number }, { merge: true });
    });
  }
}

function getChanges(current?: TeamList, prev?: TeamList) {
  const currentTeamIds = current ? Object.keys(current) : [];
  const prevTeamIds = prev ? Object.keys(prev) : [];
  const added = currentTeamIds.filter(id => !prevTeamIds.includes(id));
  const removed = prevTeamIds.filter(id => !currentTeamIds.includes(id));
  return { added, removed };
}

export const handleFollowingTeams = {
  onWrite: async (change: Change<DocumentSnapshot>) => {
    const prev: TeamList | undefined = change.before.data();
    const current: TeamList | undefined = change.after.data();
    const { added, removed } = getChanges(current, prev);
    await adjustFollowers(added, 1);
    await adjustFollowers(removed, -1);
  },
  path: "users/{userId}/attributes/followingTeams"
};

import {
  ID,
  CalendarItemByDay,
  MatchListItem,
  ISO8601Date,
  TeamList,
  PlayerBasic,
  TeamRoot,
  ManagerPerformanceResultDocument,
  PopularityDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import {
  getDocumentBody,
  getDocument,
  getComparatorForKey,
  average,
  splitIntoChunks,
  futureTimestamp
} from "../misc/utils";
import { CustomJobExecutor } from "../misc/customJob";
import { CustomJobType } from "../misc/models";

const RUN_INTERVAL = 60 * 60;

export async function generateSeasonManagerPopularity(seasonId: ID) {
  const db = firestore();

  const teamList = await getDocumentBody<TeamList>(
    db.doc(`seasons/${seasonId}/attributes/teams`)
  );
  if (!teamList) {
    throw new Error(`Season teams not found for: ${seasonId}`);
  }

  const managersByTeam: { [TeamId: string]: PlayerBasic } = {};
  for (const teamId of Object.keys(teamList)) {
    const teamRoot = await getDocumentBody<TeamRoot>(db.doc(`teams/${teamId}`));
    if (!teamRoot || !teamRoot.manager) {
      continue;
    }
    managersByTeam[teamId] = teamRoot.manager;
  }

  const matchCalendar = await getDocumentBody<CalendarItemByDay<MatchListItem>>(
    db.doc(`seasons/${seasonId}/attributes/matchCalendar`)
  );

  if (!matchCalendar) {
    throw new Error(`Season match calendar not found for: ${seasonId}`);
  }

  const minimumStartTime = new Date().toISOString();

  const matchesObj: { [MatchId: string]: MatchListItem } = Object.keys(
    matchCalendar
  ).reduce((acc, date: ISO8601Date) => {
    return { ...acc, ...matchCalendar[date] };
  }, {});

  const matches = Object.values(matchesObj)
    .filter(match => match.startTime < minimumStartTime)
    .sort(getComparatorForKey("startTime"));

  const managerMatchResults: { [PlayerId: string]: number[] } = {};

  for (const match of matches) {
    for (const qualifier of ["home", "away"]) {
      if (qualifier !== "home" && qualifier !== "away") {
        continue;
      }
      const team = match.teams[qualifier];
      const manager = managersByTeam[team.id];
      if (!manager) {
        continue;
      }
      const managerPerformanceDoc = await getDocument<
        ManagerPerformanceResultDocument
      >(db.doc(`matches/${match.id}/managerPerformance/${team.id}`));
      if (!managerPerformanceDoc) {
        continue;
      }
      const { rating, voteCount } = managerPerformanceDoc;
      if (voteCount === 0) {
        continue;
      }
      managerMatchResults[manager.id] = [
        ...(managerMatchResults[manager.id] || []),
        rating
      ];
    }
  }

  const chunks = splitIntoChunks(Object.keys(managerMatchResults), 500);

  for (const chunk of chunks) {
    const batch = db.batch();
    for (const managerId of chunk) {
      const managerResult = managerMatchResults[managerId];
      const allAverage = average(managerResult);
      const latestAverage = average(managerResult.slice(-4));
      const value = 0.2 * allAverage + 0.8 * latestAverage;
      const doc: PopularityDocument = {
        allAverage,
        latestAverage,
        value
      };
      batch.set(
        db.doc(`players/${managerId}/managerPopularity/${seasonId}`),
        doc
      );
    }
    await batch.commit();
  }

  return { processed: Object.keys(managerMatchResults).length };
}

export const seasonManagerPopularity: CustomJobExecutor<ID, any> = {
  execute: generateSeasonManagerPopularity,
  nextScheduledTask: async () => futureTimestamp(RUN_INTERVAL),
  argFromVote: vote => {
    if (vote.type !== VoteType.ManagerPerformance) {
      throw new Error(
        `Invalid type: ${vote.type} received in ${CustomJobType.SeasonManagerPopularity}`
      );
    }
    const { seasonId } = vote;
    return seasonId;
  },
  type: CustomJobType.SeasonManagerPopularity
};

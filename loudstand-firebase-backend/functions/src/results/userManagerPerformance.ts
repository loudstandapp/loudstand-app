import { InstantResultGenerator, ResultGenerationType } from "../misc/models";
import {
  ID,
  ManagerPerformanceResultDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import { averageValue } from "../misc/registerInstantResult";

interface Arg {
  userId: ID;
  teamId: ID;
}

const key = "userManagerPerformance";

export const userManagerPerformance: InstantResultGenerator<
  Arg,
  ManagerPerformanceResultDocument,
  null,
  null
> = {
  key,
  type: ResultGenerationType.Instant,
  initialValue: { rating: 0, voteCount: 0 },
  fakeResult() {
    return userManagerPerformance.initialValue;
  },
  getDestination({ userId, teamId }, db) {
    return db
      .collection("users")
      .doc(userId)
      .collection("managerPerformance")
      .doc(teamId);
  },
  argFromVote(vote) {
    if (vote.type !== VoteType.ManagerPerformance) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const { teamId, userId } = vote;
    return { teamId, userId };
  },
  countVote(vote, current) {
    if (vote.type !== VoteType.ManagerPerformance) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    return {
      voteCount: current.voteCount + 1,
      rating: averageValue(current.rating, current.voteCount, vote.rating)
    };
  }
};

import {
  TeamPerformancelResultDocument,
  ID,
  VoteType
} from "../../submodules/loudstand-shared";
import {
  ResultGenerationType,
  ScheduledResultGenerator,
  StatisticsResultType
} from "../misc/models";
import { getRandomNumber } from "../misc/utils";
import { alreadyExistsError } from "../misc/fakeResultAction";
import { averageValue } from "../misc/registerInstantResult";
import { firestore } from "firebase-admin";

interface Arg {
  matchId: ID;
  teamId: ID;
}
const key = "matchTeamPerformance";
export const matchTeamPerformance: ScheduledResultGenerator<
  Arg,
  TeamPerformancelResultDocument,
  null,
  null
> = {
  resultType: StatisticsResultType.MatchTeamPerformance,
  type: ResultGenerationType.Scheduled,
  nextScheduledTask: () =>
    firestore.Timestamp.fromMillis(Date.now() + 1000 * 20),
  argFromVote(vote) {
    if (vote.type !== VoteType.TeamPerformance) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const { matchId, teamId } = vote;
    return { matchId, teamId };
  },
  initialValue: { rating: 0, voteCount: 0 },
  countVote(vote, current) {
    if (vote.type !== VoteType.TeamPerformance) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    return {
      voteCount: current.voteCount + 1,
      rating: averageValue(current.rating, current.voteCount, vote.rating)
    };
  },
  fakeResult(arg, input, current) {
    if (current) {
      throw alreadyExistsError();
    }
    const rating = getRandomNumber(1.2, 3.8);
    return {
      rating,
      voteCount: 0
    };
  },
  getDestination({ matchId, teamId }, db) {
    console.log({ matchId, teamId });
    return db
      .collection("matches")
      .doc(matchId)
      .collection("teamPerformance")
      .doc(teamId);
  },
  key
};

import {
  ID,
  MostValuablePlayerResultDocument,
  VoteType,
  PlayerList
} from "../../submodules/loudstand-shared";
import {
  ResultGenerationType,
  ScheduledResultGenerator,
  StatisticsResultType
} from "../misc/models";
import { getRandomNumber } from "../misc/utils";
import { firestore } from "firebase-admin";
import { alreadyExistsError } from "../misc/fakeResultAction";
import { adjustPickedAverages } from "../misc/registerInstantResult";
import { teamPlayers } from "../import/teamPlayers";

interface Context {
  teamQualifier?: "home" | "away";
}

const key = "teamMostValuablePlayers";
export const teamMostValuablePlayers: ScheduledResultGenerator<
  ID,
  MostValuablePlayerResultDocument,
  PlayerList,
  Context
> = {
  resultType: StatisticsResultType.TeamMostValuablePlayers,
  type: ResultGenerationType.Scheduled,
  nextScheduledTask: () =>
    firestore.Timestamp.fromMillis(Date.now() + 1000 * 20),
  argFromVote({ teamId }) {
    return teamId;
  },
  initialValue: { players: {}, voteCount: 0 },
  countVote(vote, current) {
    if (vote.type !== VoteType.MostValuablePlayer) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const players = adjustPickedAverages(current.voteCount, current.players, [
      vote.playerId
    ]);
    return {
      players,
      voteCount: current.voteCount + 1
    };
  },
  getDestination(teamId, db) {
    return db
      .collection("teams")
      .doc(teamId)
      .collection("statistics")
      .doc("mostValuablePlayers");
  },
  fakeResult(arg, input, current) {
    if (current) {
      throw alreadyExistsError();
    }
    if (!input) {
      throw new Error("Cannot get current team player list");
    }
    const players = Object.keys(input).reduce(
      (acc, playerId) => ({
        ...acc,
        [playerId]: getRandomNumber(0.4, 0.6)
      }),
      {}
    );
    return {
      players,
      voteCount: 0
    };
  },
  getFakeDataSource(teamId, db) {
    return teamPlayers.getDestination(teamId, db);
  },
  key
};

import { ResultGenerationType, InstantResultGenerator } from "../misc/models";
import {
  UserTotalReactionsResultDocument,
  ID,
  FinalSubmittedVote
} from "../../submodules/loudstand-shared";

type Arg = ID;
const key = "userTotalReactions";
export const userTotalReactions: InstantResultGenerator<
  Arg,
  UserTotalReactionsResultDocument,
  null,
  null
> = {
  fakeResult() {
    return userTotalReactions.initialValue;
  },
  type: ResultGenerationType.Instant,
  initialValue: { reactions: 0 },
  key,
  getDestination(userId, db) {
    return db
      .collection("users")
      .doc(userId)
      .collection("statistics")
      .doc("reactions");
  },
  argFromVote({ userId }: FinalSubmittedVote) {
    return userId;
  },
  countVote(vote, current) {
    return { reactions: current.reactions + 1 };
  }
};

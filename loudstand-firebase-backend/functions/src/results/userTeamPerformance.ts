import { InstantResultGenerator, ResultGenerationType } from "../misc/models";
import {
  ID,
  TeamPerformancelResultDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import { averageValue } from "../misc/registerInstantResult";

interface Arg {
  userId: ID;
  teamId: ID;
}

const key = "userTeamPerformance";

export const userTeamPerformance: InstantResultGenerator<
  Arg,
  TeamPerformancelResultDocument,
  null,
  null
> = {
  key,
  type: ResultGenerationType.Instant,
  initialValue: { rating: 0, voteCount: 0 },
  fakeResult() {
    return userTeamPerformance.initialValue;
  },
  getDestination({ userId, teamId }, db) {
    return db
      .collection("users")
      .doc(userId)
      .collection("teamPerformance")
      .doc(teamId);
  },
  argFromVote(vote) {
    if (vote.type !== VoteType.TeamPerformance) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const { teamId, userId } = vote;

    return { teamId, userId };
  },
  countVote(vote, current) {
    if (vote.type !== VoteType.TeamPerformance) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    return {
      voteCount: current.voteCount + 1,
      rating: averageValue(current.rating, current.voteCount, vote.rating)
    };
  }
};

import {
  ID,
  PlayerList,
  LineupSuggestionResultDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import {
  ResultGenerationType,
  ScheduledResultGenerator,
  StatisticsResultType
} from "../misc/models";
import { getRandomNumber } from "../misc/utils";
import { alreadyExistsError } from "../misc/fakeResultAction";
import { adjustPickedAverages } from "../misc/registerInstantResult";
import { firestore } from "firebase-admin";
import { teamPlayers } from "../import/teamPlayers";

const key = "teamLineupSuggestion";
export const teamLineupSuggestion: ScheduledResultGenerator<
  ID,
  LineupSuggestionResultDocument,
  PlayerList,
  null
> = {
  type: ResultGenerationType.Scheduled,
  resultType: StatisticsResultType.TeamLineupSuggestion,
  initialValue: { players: {}, voteCount: 0 },
  nextScheduledTask: () =>
    firestore.Timestamp.fromMillis(Date.now() + 1000 * 20),
  argFromVote({ teamId }) {
    return teamId;
  },
  countVote(vote, current) {
    if (vote.type !== VoteType.Lineup) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const players = adjustPickedAverages(
      current.voteCount,
      current.players,
      vote.lineup
    );
    return {
      players,
      voteCount: current.voteCount + 1
    };
  },
  fakeResult(arg, input, current) {
    if (current) {
      throw alreadyExistsError();
    }
    if (!input) {
      throw new Error("Cannot get current lineup");
    }
    const players = Object.keys(input).reduce(
      (acc, playerId) => ({
        ...acc,
        [playerId]: getRandomNumber(0.4, 0.9)
      }),
      {}
    );
    return {
      players,
      voteCount: 0
    };
  },
  getDestination(teamId, db) {
    return db
      .collection("teams")
      .doc(teamId)
      .collection("statistics")
      .doc("suggestedLineup");
  },
  getFakeDataSource(teamId, db) {
    return teamPlayers.getDestination(teamId, db);
  },
  key
};

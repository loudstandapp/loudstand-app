import {
  ScheduledResultGenerator,
  ResultGenerationType,
  StatisticsResultType
} from "../misc/models";
import {
  SubstitutionApprovalResultDocument,
  ID,
  VoteType,
  QuestionResponseType
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { getRandomResult } from "./matchEventResults";
import { averageValue } from "../misc/registerInstantResult";

interface Arg {
  seasonId: ID;
  managerId: ID;
}

const key = "managerSubstitutionApproval";
export const managerSubstitutionApproval: ScheduledResultGenerator<
  Arg,
  SubstitutionApprovalResultDocument,
  null
> = {
  key,
  type: ResultGenerationType.Scheduled,
  resultType: StatisticsResultType.ManagerSubstitutionApproval,
  initialValue: {
    result: {
      [QuestionResponseType.Yes]: 0,
      [QuestionResponseType.No]: 0
    },
    voteCount: 0
  },
  nextScheduledTask: () =>
    firestore.Timestamp.fromMillis(Date.now() + 1000 * 20),
  argFromVote(vote) {
    if (vote.type !== VoteType.Substitution) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    if (!vote.managerId) {
      throw new Error(
        `Manager missing in: ${vote.created} by ${
          vote.userId
        } received in ${key}`
      );
    }
    const { seasonId, managerId } = vote;
    return { seasonId, managerId };
  },
  countVote(vote, current) {
    if (vote.type !== VoteType.Substitution) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const yes = averageValue(
      current.result.YES,
      current.voteCount,
      vote.answer === QuestionResponseType.Yes ? 1 : 0
    );
    const no = 1 - yes;
    const result = {
      result: {
        [QuestionResponseType.Yes]: yes,
        [QuestionResponseType.No]: no
      },
      voteCount: (current.voteCount || 0) + 1
    };
    return result;
  },
  fakeResult() {
    return {
      voteCount: 0,
      result: getRandomResult()
    };
  },
  getDestination({ managerId, seasonId }, db) {
    return db
      .collection("players")
      .doc(managerId)
      .collection("substitutionApproval")
      .doc(seasonId);
  }
};

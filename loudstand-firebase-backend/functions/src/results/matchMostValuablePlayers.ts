import {
  ID,
  MatchRoot,
  MatchActivePlayers,
  MostValuablePlayerResultDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import {
  ResultGenerationType,
  ScheduledResultGenerator,
  StatisticsResultType
} from "../misc/models";
import { getRandomNumber } from "../misc/utils";
import { firestore } from "firebase-admin";
import { alreadyExistsError } from "../misc/fakeResultAction";
import { adjustPickedAverages } from "../misc/registerInstantResult";

interface Context {
  teamQualifier?: "home" | "away";
}

interface Arg {
  matchId: ID;
  teamId: ID;
}
const key = "matchMostValuablePlayers";
export const matchMostValuablePlayers: ScheduledResultGenerator<
  Arg,
  MostValuablePlayerResultDocument,
  MatchActivePlayers,
  Context
> = {
  resultType: StatisticsResultType.MatchMostValuablePlayers,
  type: ResultGenerationType.Scheduled,
  nextScheduledTask: () =>
    firestore.Timestamp.fromMillis(Date.now() + 1000 * 20),
  argFromVote(vote) {
    if (vote.type !== VoteType.MostValuablePlayer) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const { matchId, teamId } = vote;
    return { matchId, teamId };
  },
  initialValue: { players: {}, voteCount: 0 },
  countVote(vote, current) {
    if (vote.type !== VoteType.MostValuablePlayer) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const players = adjustPickedAverages(current.voteCount, current.players, [
      vote.playerId
    ]);
    return {
      players,
      voteCount: current.voteCount + 1
    };
  },
  async preFakeResult({ matchId, teamId }, context) {
    const db = firestore();
    const docRef = db.collection("matches").doc(matchId);
    const snap = await docRef.get();
    const data = snap.data();
    if (!data || !data.body) {
      throw new Error(
        `matchMostValuablePlayers: matchRoot not found for ${matchId}`
      );
    }
    const matchRoot: MatchRoot = data.body;
    if (!matchRoot.teams) {
      throw new Error(
        `matchMostValuablePlayers: matchRoot teams not found for ${matchId}`
      );
    }
    context.teamQualifier =
      matchRoot.teams.home.id === teamId ? "home" : "away";
    return true;
  },
  fakeResult(arg, input, current, context) {
    if (current) {
      throw alreadyExistsError();
    }
    if (!input) {
      throw new Error("Cannot get current active player list");
    }
    if (!context.teamQualifier) {
      throw new Error("No team qualifier set");
    }
    const activePlayers = input[context.teamQualifier];
    const players = activePlayers.reduce(
      (acc, player) => ({
        ...acc,
        [player.id]: getRandomNumber(0.4, 0.6)
      }),
      {}
    );
    return {
      players,
      voteCount: 0
    };
  },
  getDestination({ matchId, teamId }, db) {
    return db
      .collection("matches")
      .doc(matchId)
      .collection("mostValuablePlayers")
      .doc(teamId);
  },
  getFakeDataSource({ matchId }, db) {
    return db
      .collection("matches")
      .doc(matchId)
      .collection("attributes")
      .doc("activePlayers");
  },
  key
};

import {
  ScheduledResultGenerator,
  ResultGenerationType,
  StatisticsResultType
} from "../misc/models";
import {
  QuestionResultDocument,
  ID,
  VoteType,
  QuestionResponseType
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { averageValue } from "../misc/registerInstantResult";
import { alreadyExistsError } from "../misc/fakeResultAction";
import { getRandomNumber } from "../misc/utils";

const key = "questionResult";
export const questionResult: ScheduledResultGenerator<
  ID,
  QuestionResultDocument,
  null
> = {
  key,
  type: ResultGenerationType.Scheduled,
  resultType: StatisticsResultType.Question,
  nextScheduledTask: () =>
    firestore.Timestamp.fromMillis(Date.now() + 1000 * 20),
  argFromVote(vote) {
    if (vote.type !== VoteType.Question) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const { questionId } = vote;
    return questionId;
  },
  initialValue: {
    result: { [QuestionResponseType.Yes]: 0, [QuestionResponseType.No]: 0 },
    voteCount: 0
  },
  countVote(vote, current) {
    if (vote.type !== VoteType.Question) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const yes = averageValue(
      current.result.YES,
      current.voteCount,
      vote.answer === QuestionResponseType.Yes ? 1 : 0
    );
    const no = 1 - yes;
    return {
      voteCount: current.voteCount + 1,
      result: {
        [QuestionResponseType.Yes]: yes,
        [QuestionResponseType.No]: no
      }
    };
  },
  fakeResult(arg, input, current) {
    if (current) {
      throw alreadyExistsError();
    }
    const yes = getRandomNumber(0.2, 0.8);
    const no = 1 - yes;
    return {
      result: {
        [QuestionResponseType.Yes]: yes,
        [QuestionResponseType.No]: no
      },
      voteCount: 0
    };
  },
  getDestination(id, db) {
    return db
      .collection("questions")
      .doc(id)
      .collection("statistics")
      .doc("result");
  }
};

import {
  ID,
  CalendarItemByDay,
  MatchListItem,
  ISO8601Date,
  MostValuablePlayerResultDocument,
  MatchActivePlayers,
  PlayerMostValuablePlayerShareDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import {
  splitIntoChunks,
  getDocumentBody,
  getDocument,
  futureTimestamp
} from "../misc/utils";
import { CustomJobExecutor } from "../misc/customJob";
import { CustomJobType } from "../misc/models";

const RUN_INTERVAL = 60 * 60;

export async function generateSeasonMostValuablePlayerShare(seasonId: ID) {
  const db = firestore();

  const matchCalendar = await getDocumentBody<CalendarItemByDay<MatchListItem>>(
    db.doc(`seasons/${seasonId}/attributes/matchCalendar`)
  );

  if (!matchCalendar) {
    throw new Error(`Season match calendar not found for: ${seasonId}`);
  }

  const minimumStartTime = new Date().toISOString();

  const matchesObj: { [MatchId: string]: MatchListItem } = Object.keys(
    matchCalendar
  ).reduce((acc, date: ISO8601Date) => {
    return { ...acc, ...matchCalendar[date] };
  }, {});

  const matches = Object.values(matchesObj).filter(
    match => match.startTime < minimumStartTime
  );

  const mvpVoteCountByPlayer: { [PlayerId: string]: number } = {};
  const mvpVoteCountForMatchesWithPlayer: { [PlayerId: string]: number } = {};

  // Collect all data
  for (const matchListItem of matches) {
    const { id, teams } = matchListItem;
    const activePlayers = await getDocumentBody<MatchActivePlayers>(
      db.doc(`matches/${id}/attributes/activePlayers`)
    );
    if (!activePlayers) {
      continue;
    }
    for (const qualifier of Object.keys(teams)) {
      // Make TS happy
      if (qualifier !== "home" && qualifier !== "away") {
        continue;
      }
      const team = teams[qualifier];
      const results = await getDocument<MostValuablePlayerResultDocument>(
        db.doc(`matches/${id}/mostValuablePlayers/${team.id}`)
      );
      if (!results) {
        continue;
      }
      const { voteCount, players } = results;
      if (voteCount === 0) {
        continue;
      }

      for (const player of activePlayers[qualifier]) {
        const currentPlayerVoteCount = mvpVoteCountByPlayer[player.id] || 0;
        const playerValue = players[player.id] || 0;
        mvpVoteCountByPlayer[player.id] =
          currentPlayerVoteCount + voteCount * playerValue;
        const currentTotalVoteCount =
          mvpVoteCountForMatchesWithPlayer[player.id] || 0;
        mvpVoteCountForMatchesWithPlayer[player.id] =
          currentTotalVoteCount + voteCount;
      }
    }
  }

  const chunks = splitIntoChunks(Object.keys(mvpVoteCountByPlayer), 10);

  for (const chunk of chunks) {
    const batch = db.batch();
    for (const playerId of chunk) {
      const playerVotes = mvpVoteCountByPlayer[playerId];
      const totalVotes = mvpVoteCountForMatchesWithPlayer[playerId];
      const value = totalVotes > 0 ? playerVotes / totalVotes : 0;
      const doc: PlayerMostValuablePlayerShareDocument = {
        playerVotes,
        totalVotes,
        value
      };
      const docRef = db
        .collection("players")
        .doc(playerId)
        .collection("mostValuableShare")
        .doc(seasonId);
      batch.set(docRef, doc);
    }
    await batch.commit();
  }
  return { processed: Object.keys(mvpVoteCountByPlayer).length };
}

export const seasonMostValuablePlayerShare: CustomJobExecutor<ID, any> = {
  execute: generateSeasonMostValuablePlayerShare,
  nextScheduledTask: async () => futureTimestamp(RUN_INTERVAL),
  argFromVote: vote => {
    if (vote.type !== VoteType.MostValuablePlayer) {
      throw new Error(
        `Invalid type: ${vote.type} received in ${CustomJobType.SeasonMostValuablePlayerShare}`
      );
    }
    const { seasonId } = vote;
    return seasonId;
  },
  type: CustomJobType.SeasonMostValuablePlayerShare
};

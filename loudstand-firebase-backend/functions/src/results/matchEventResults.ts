import {
  ID,
  MatchRoot,
  MatchEventResultsSummaryDocument,
  MatchTimeline,
  VoteType,
  QuestionResponseType
} from "../../submodules/loudstand-shared";
import {
  ResultGenerationType,
  ScheduledResultGenerator,
  StatisticsResultType
} from "../misc/models";
import { getRandomNumber } from "../misc/utils";
import { firestore } from "firebase-admin";
import { averageValue } from "../misc/registerInstantResult";

interface Context {
  teamQualifier?: "home" | "away";
}

interface Arg {
  matchId: ID;
  teamId: ID;
}

export function getRandomResult() {
  const yes = getRandomNumber(0.2, 0.8);
  const no = 1 - yes;
  return { [QuestionResponseType.Yes]: yes, [QuestionResponseType.No]: no };
}
const key = "matchMatchEventResults";
export const matchMatchEventResults: ScheduledResultGenerator<
  Arg,
  MatchEventResultsSummaryDocument,
  MatchTimeline,
  Context
> = {
  type: ResultGenerationType.Scheduled,
  resultType: StatisticsResultType.MatchEventResults,
  nextScheduledTask: () =>
    firestore.Timestamp.fromMillis(Date.now() + 1000 * 20),
  argFromVote(vote) {
    if (
      !(
        vote.type === VoteType.Penalty ||
        vote.type === VoteType.Substitution ||
        vote.type === VoteType.RedCard ||
        vote.type === VoteType.YellowCard ||
        vote.type === VoteType.YellowRedCard
      )
    ) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const { matchId, teamId } = vote;
    return { matchId, teamId };
  },
  initialValue: {},
  countVote(vote, current) {
    if (
      !(
        vote.type === VoteType.Penalty ||
        vote.type === VoteType.Substitution ||
        vote.type === VoteType.RedCard ||
        vote.type === VoteType.YellowCard ||
        vote.type === VoteType.YellowRedCard
      )
    ) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const currentEventResult = current[vote.eventId];

    if (!currentEventResult) {
      const yes = vote.answer === QuestionResponseType.Yes ? 1 : 0;
      const no = 1 - yes;
      const result = {
        ...current,
        [vote.eventId]: {
          result: {
            [QuestionResponseType.Yes]: yes,
            [QuestionResponseType.No]: no
          },
          voteCount: 1
        }
      };
      return result;
    } else {
      const yes = averageValue(
        currentEventResult.result.YES,
        currentEventResult.voteCount,
        vote.answer === QuestionResponseType.Yes ? 1 : 0
      );
      const no = 1 - yes;
      const result = {
        ...current,
        [vote.eventId]: {
          result: {
            [QuestionResponseType.Yes]: yes,
            [QuestionResponseType.No]: no
          },
          voteCount: (currentEventResult.voteCount || 0) + 1
        }
      };
      return result;
    }
  },
  async preFakeResult({ matchId, teamId }, context) {
    const db = firestore();
    const docRef = db.collection("matches").doc(matchId);
    const snap = await docRef.get();
    const data = snap.data();
    if (!data || !data.body) {
      throw new Error(
        `matchMatchEventResults: matchRoot not found for ${matchId}`
      );
    }
    const matchRoot: MatchRoot = data.body;
    if (!matchRoot.teams) {
      throw new Error(
        `matchMatchEventResults: matchRoot teams not found for ${matchId}`
      );
    }
    context.teamQualifier =
      matchRoot.teams.home.id === teamId ? "home" : "away";
    return true;
  },
  fakeResult(arg, input, current, context) {
    if (!input) {
      throw new Error("Cannot get current timeline ");
    }
    if (!context.teamQualifier) {
      throw new Error("No team qualifier set");
    }
    const fakeResults = Object.values(input.events)
      .filter(event => event.team === context.teamQualifier)
      .reduce(
        (acc, event) => ({
          ...acc,
          [event.id]: {
            voteCount: 0,
            result: getRandomResult()
          }
        }),
        {}
      );
    if (!current) {
      return fakeResults;
    }
    const realResults: MatchEventResultsSummaryDocument = Object.keys(current)
      .filter(id => current[id].voteCount > 0)
      .reduce(
        (acc, id) => ({
          ...acc,
          [id]: current[id]
        }),
        {}
      );
    const results = { ...fakeResults, ...realResults };
    return results;
  },
  getDestination({ matchId, teamId }, db) {
    return db
      .collection("matches")
      .doc(matchId)
      .collection("matchEventResults")
      .doc(teamId);
  },
  getFakeDataSource({ matchId }, db) {
    return db
      .collection("matches")
      .doc(matchId)
      .collection("attributes")
      .doc("timeline");
  },
  key
};

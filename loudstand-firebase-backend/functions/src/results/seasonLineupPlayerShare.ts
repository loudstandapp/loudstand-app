import {
  ID,
  MatchListItem,
  ISO8601Date,
  PlayerMostValuablePlayerShareDocument,
  LineupSuggestionResultDocument,
  TeamList,
  PlayerList,
  VoteType
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import {
  splitIntoChunks,
  getDocument,
  getDocumentBody,
  futureTimestamp
} from "../misc/utils";
import { CustomJobExecutor } from "../misc/customJob";
import { CustomJobType } from "../misc/models";

const RUN_INTERVAL = 60 * 60;

export async function generateSeasonLineupPlayerShare(seasonId: ID) {
  const db = firestore();
  const teamList = await getDocumentBody<TeamList>(
    db.doc(`seasons/${seasonId}/attributes/teams`)
  );

  if (!teamList) {
    throw new Error(`Team list not found for: ${seasonId}`);
  }

  // Get all players for all teams.
  const playersForTeam: { [TeamId: string]: PlayerList } = {};
  for (const teamId of Object.keys(teamList)) {
    const players = await getDocumentBody<PlayerList>(
      db.doc(`teams/${teamId}/attributes/players`)
    );

    if (!players) {
      continue;
    }
    playersForTeam[teamId] = players;
  }

  const matchCalendar = await getDocumentBody<TeamList>(
    db.doc(`seasons/${seasonId}/attributes/matchCalendar`)
  );

  if (!matchCalendar) {
    throw new Error(`Season match calendar not found for: ${seasonId}`);
  }

  const minimumStartTime = new Date().toISOString();

  const matchesObj: { [MatchId: string]: MatchListItem } = Object.keys(
    matchCalendar
  ).reduce((acc, date: ISO8601Date) => {
    return { ...acc, ...matchCalendar[date] };
  }, {});

  const matches = Object.values(matchesObj).filter(
    match => match.startTime < minimumStartTime
  );

  const lineupVoteCountByPlayer: { [PlayerId: string]: number } = {};
  const lineupVoteCountForMatchesWithPlayer: {
    [PlayerId: string]: number;
  } = {};

  // Collect all data
  for (const matchListItem of matches) {
    const { id, teams } = matchListItem;
    for (const qualifier of Object.keys(teams)) {
      // Make TS happy
      if (qualifier !== "home" && qualifier !== "away") {
        continue;
      }
      const team = teams[qualifier];
      const results = await getDocument<LineupSuggestionResultDocument>(
        db.doc(`matches/${id}/suggestedLineup/${team.id}`)
      );
      if (!results) {
        continue;
      }
      const { voteCount, players } = results;
      if (voteCount === 0) {
        continue;
      }
      const teamPlayers = playersForTeam[team.id];
      if (!teamPlayers) {
        continue;
      }

      for (const player of Object.values(teamPlayers)) {
        const currentPlayerVoteCount = lineupVoteCountByPlayer[player.id] || 0;
        const playerValue = players[player.id] || 0;
        lineupVoteCountByPlayer[player.id] =
          currentPlayerVoteCount + voteCount * playerValue;
        const currentTotalVoteCount =
          lineupVoteCountForMatchesWithPlayer[player.id] || 0;
        lineupVoteCountForMatchesWithPlayer[player.id] =
          currentTotalVoteCount + voteCount;
      }
    }
  }

  const chunks = splitIntoChunks(Object.keys(lineupVoteCountByPlayer), 500);

  for (const chunk of chunks) {
    const batch = db.batch();
    for (const playerId of chunk) {
      const playerVotes = lineupVoteCountByPlayer[playerId];
      const totalVotes = lineupVoteCountForMatchesWithPlayer[playerId];
      const value = totalVotes > 0 ? playerVotes / totalVotes : 0;
      const doc: PlayerMostValuablePlayerShareDocument = {
        playerVotes,
        totalVotes,
        value
      };
      const docRef = db
        .collection("players")
        .doc(playerId)
        .collection("lineupShare")
        .doc(seasonId);
      batch.set(docRef, doc);
    }
    await batch.commit();
  }
  return { processed: Object.keys(lineupVoteCountByPlayer).length };
}

export const seasonLineupPlayerShare: CustomJobExecutor<ID, any> = {
  execute: generateSeasonLineupPlayerShare,
  nextScheduledTask: async () => futureTimestamp(RUN_INTERVAL),
  argFromVote: vote => {
    if (vote.type !== VoteType.Lineup) {
      throw new Error(
        `Invalid type: ${vote.type} received in ${CustomJobType.SeasonLineupPlayerShare}`
      );
    }
    const { seasonId } = vote;
    return seasonId;
  },
  type: CustomJobType.SeasonLineupPlayerShare
};

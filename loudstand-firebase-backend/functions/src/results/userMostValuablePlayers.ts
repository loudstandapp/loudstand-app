import {
  ID,
  MostValuablePlayerResultDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import { ResultGenerationType, InstantResultGenerator } from "../misc/models";
import { adjustPickedAverages } from "../misc/registerInstantResult";

interface Arg {
  userId: ID;
  teamId: ID;
}
const key = "userMostValuablePlayers";
export const userMostValuablePlayers: InstantResultGenerator<
  Arg,
  MostValuablePlayerResultDocument,
  null
> = {
  type: ResultGenerationType.Instant,
  argFromVote(vote) {
    if (vote.type !== VoteType.MostValuablePlayer) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const { userId, teamId } = vote;
    return { userId, teamId };
  },
  initialValue: { players: {}, voteCount: 0 },
  countVote(vote, current) {
    if (vote.type !== VoteType.MostValuablePlayer) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const players = adjustPickedAverages(current.voteCount, current.players, [
      vote.playerId
    ]);
    return {
      players,
      voteCount: current.voteCount + 1
    };
  },
  fakeResult() {
    return userMostValuablePlayers.initialValue;
  },
  getDestination({ userId, teamId }, db) {
    return db
      .collection("users")
      .doc(userId)
      .collection("mostValuablePlayers")
      .doc(teamId);
  },
  key
};

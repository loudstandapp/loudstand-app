import {
  ID,
  CalendarItemByDay,
  MatchListItem,
  ISO8601Date,
  TeamPerformancelResultDocument,
  SeasonTeamPopularityDocument,
  PopularityDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import {
  getDocumentBody,
  getDocument,
  getComparatorForKey,
  average,
  futureTimestamp
} from "../misc/utils";
import { CustomJobExecutor } from "../misc/customJob";
import { CustomJobType } from "../misc/models";

const RUN_INTERVAL = 60 * 60;

export async function generateSeasonTeamPopularity(seasonId: ID) {
  const db = firestore();

  const matchCalendar = await getDocumentBody<CalendarItemByDay<MatchListItem>>(
    db.doc(`seasons/${seasonId}/attributes/matchCalendar`)
  );

  if (!matchCalendar) {
    throw new Error(`Season match calendar not found for: ${seasonId}`);
  }

  const minimumStartTime = new Date().toISOString();

  const matchesObj: { [MatchId: string]: MatchListItem } = Object.keys(
    matchCalendar
  ).reduce((acc, date: ISO8601Date) => {
    return { ...acc, ...matchCalendar[date] };
  }, {});

  const matches = Object.values(matchesObj)
    .filter(match => match.startTime < minimumStartTime)
    .sort(getComparatorForKey("startTime"));

  const teamMatchResults: { [TeamId: string]: number[] } = {};

  for (const match of matches) {
    for (const qualifier of ["home", "away"]) {
      if (qualifier !== "home" && qualifier !== "away") {
        continue;
      }
      const team = match.teams[qualifier];
      const teamPerformanceDoc = await getDocument<
        TeamPerformancelResultDocument
      >(db.doc(`matches/${match.id}/teamPerformance/${team.id}`));
      if (!teamPerformanceDoc) {
        continue;
      }
      const { rating, voteCount } = teamPerformanceDoc;
      if (voteCount === 0) {
        continue;
      }
      teamMatchResults[team.id] = [
        ...(teamMatchResults[team.id] || []),
        rating
      ];
    }
  }

  const teamPerformanceList: SeasonTeamPopularityDocument = Object.keys(
    teamMatchResults
  ).reduce((acc, teamId) => {
    const teamResult = teamMatchResults[teamId];
    const allAverage = average(teamResult);
    const latestAverage = average(teamResult.slice(-4));
    const value = 0.2 * allAverage + 0.8 * latestAverage;
    const doc: PopularityDocument = { allAverage, value, latestAverage };
    return {
      ...acc,
      [teamId]: doc
    };
  }, {});
  const docRef = db.doc(`seasons/${seasonId}/statistics/teamPopularity`);
  await docRef.set(teamPerformanceList);
  return { processed: Object.keys(teamPerformanceList).length };
}

export const seasonTeamPopularity: CustomJobExecutor<ID, any> = {
  execute: generateSeasonTeamPopularity,
  nextScheduledTask: async () => futureTimestamp(RUN_INTERVAL),
  argFromVote: vote => {
    if (vote.type !== VoteType.TeamPerformance) {
      throw new Error(
        `Invalid type: ${vote.type} received in ${CustomJobType.SeasonTeamPopularity}`
      );
    }
    const { seasonId } = vote;
    return seasonId;
  },
  type: CustomJobType.SeasonTeamPopularity
};

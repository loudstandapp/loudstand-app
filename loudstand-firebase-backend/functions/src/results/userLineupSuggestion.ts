import {
  ID,
  LineupSuggestionResultDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import { ResultGenerationType, InstantResultGenerator } from "../misc/models";
import { adjustPickedAverages } from "../misc/registerInstantResult";

interface Arg {
  userId: ID;
  teamId: ID;
}
const key = "userLineupSuggestion";
export const userLineupSuggestion: InstantResultGenerator<
  Arg,
  LineupSuggestionResultDocument,
  null
> = {
  type: ResultGenerationType.Instant,
  initialValue: { players: {}, voteCount: 0 },
  argFromVote(vote) {
    if (vote.type !== VoteType.Lineup) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const { userId, teamId } = vote;
    return { userId, teamId };
  },
  countVote(vote, current) {
    if (vote.type !== VoteType.Lineup) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const players = adjustPickedAverages(
      current.voteCount,
      current.players,
      vote.lineup
    );
    return {
      players,
      voteCount: current.voteCount + 1
    };
  },
  fakeResult() {
    return userLineupSuggestion.initialValue;
  },
  getDestination({ userId, teamId }, db) {
    return db
      .collection("users")
      .doc(userId)
      .collection("suggestedLineup")
      .doc(teamId);
  },
  key
};

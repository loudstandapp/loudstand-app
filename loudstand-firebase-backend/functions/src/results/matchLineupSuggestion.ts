import {
  ID,
  PlayerList,
  LineupSuggestionResultDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import {
  ResultGenerationType,
  ScheduledResultGenerator,
  StatisticsResultType
} from "../misc/models";
import { getRandomNumber } from "../misc/utils";
import { alreadyExistsError } from "../misc/fakeResultAction";
import { adjustPickedAverages } from "../misc/registerInstantResult";
import { firestore } from "firebase-admin";

interface Arg {
  matchId: ID;
  teamId: ID;
}
const key = "matchLineupSuggestion";
export const matchLineupSuggestion: ScheduledResultGenerator<
  Arg,
  LineupSuggestionResultDocument,
  PlayerList,
  null
> = {
  type: ResultGenerationType.Scheduled,
  resultType: StatisticsResultType.MatchLineupSuggestion,
  initialValue: { players: {}, voteCount: 0 },
  nextScheduledTask: () =>
    firestore.Timestamp.fromMillis(Date.now() + 1000 * 20),
  argFromVote(vote) {
    if (vote.type !== VoteType.Lineup) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const { matchId, teamId } = vote;
    return { matchId, teamId };
  },
  countVote(vote, current) {
    if (vote.type !== VoteType.Lineup) {
      throw new Error(`Invalid type: ${vote.type} received in ${key}`);
    }
    const players = adjustPickedAverages(
      current.voteCount,
      current.players,
      vote.lineup
    );
    return {
      players,
      voteCount: current.voteCount + 1
    };
  },
  fakeResult(arg, input, current) {
    if (current) {
      throw alreadyExistsError();
    }
    if (!input) {
      throw new Error("Cannot get current lineup");
    }
    const players = Object.keys(input).reduce(
      (acc, playerId) => ({
        ...acc,
        [playerId]: getRandomNumber(0.4, 0.9)
      }),
      {}
    );
    return {
      players,
      voteCount: 0
    };
  },
  getDestination({ matchId, teamId }, db) {
    return db
      .collection("matches")
      .doc(matchId)
      .collection("suggestedLineup")
      .doc(teamId);
  },
  getFakeDataSource({ teamId }, db) {
    return db
      .collection("teams")
      .doc(teamId)
      .collection("attributes")
      .doc("players");
  },
  key
};

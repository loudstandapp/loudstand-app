import {
  average,
  getDocumentBody,
  getDocument,
  splitIntoChunks,
  getComparatorForKey,
  futureTimestamp
} from "../misc/utils";
import {
  ID,
  TeamList,
  TeamPlayer,
  CalendarItemByDay,
  MatchListItem,
  ISO8601Date,
  MatchActivePlayers,
  MostValuablePlayerResultDocument,
  LineupSuggestionResultDocument,
  PopularityDocument,
  VoteType
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { CustomJobExecutor } from "../misc/customJob";
import { CustomJobType } from "../misc/models";

const RUN_INTERVAL = 60 * 60;

function getPopularity(mvpRate: number[], lineupRate: number[]) {
  const averageMvp = average(mvpRate);
  const averageLineup = average(lineupRate);
  return averageMvp * 0.15 + averageLineup * 0.85;
}

export async function generateSeasonPlayerPopularity(seasonId: ID) {
  const mostValuablePlayerRate: { [PlayerId: string]: number[] } = {};
  const lineupPlayerRate: { [PlayerId: string]: number[] } = {};
  const teamPlayerIds: { [TeamId: string]: ID[] } = {};

  const db = firestore();

  // Get all teams of season
  const teamList = await getDocumentBody<TeamList>(
    db.doc(`seasons/${seasonId}/attributes/teams`)
  );

  if (!teamList) {
    throw new Error(`Team list not found for: ${seasonId}`);
  }

  for (const team of Object.values(teamList)) {
    //   Get all players of teams
    const players = await getDocumentBody<TeamPlayer>(
      db.doc(`teams/${team.id}/attributes/players`)
    );
    if (!players) {
      continue;
    }
    teamPlayerIds[team.id] = Object.keys(players);
  }

  const allPlayerIds: ID[] = Object.keys(teamPlayerIds).reduce(
    (acc: ID[], teamId: ID) => [...acc, ...teamPlayerIds[teamId]],
    []
  );
  // Get all matches of season, sort by date

  const matchCalendar = await getDocumentBody<CalendarItemByDay<MatchListItem>>(
    db.doc(`seasons/${seasonId}/attributes/matchCalendar`)
  );

  if (!matchCalendar) {
    throw new Error(`Season match calendar not found for: ${seasonId}`);
  }

  const minimumStartTime = new Date().toISOString();

  const matchesObj: { [MatchId: string]: MatchListItem } = Object.keys(
    matchCalendar
  ).reduce((acc, date: ISO8601Date) => {
    return { ...acc, ...matchCalendar[date] };
  }, {});

  const matches = Object.values(matchesObj)
    .filter(match => match.startTime < minimumStartTime)
    .sort(getComparatorForKey("startTime"));

  // Collect all data
  for (const matchListItem of matches) {
    const { id, teams } = matchListItem;
    const activePlayers = await getDocumentBody<MatchActivePlayers>(
      db.doc(`matches/${id}/attributes/activePlayers`)
    );
    if (!activePlayers) {
      continue;
    }
    for (const qualifier of Object.keys(teams)) {
      // Make TS happy
      if (qualifier !== "home" && qualifier !== "away") {
        continue;
      }
      const team = teams[qualifier];
      const mvpResults = await getDocument<MostValuablePlayerResultDocument>(
        db.doc(`matches/${id}/mostValuablePlayers/${team.id}`)
      );
      if (!mvpResults) {
        continue;
      }
      const { players: mvpPlayers, voteCount: mvpVoteCount } = mvpResults;
      if (mvpVoteCount === 0) {
        continue;
      }

      for (const player of activePlayers[qualifier]) {
        const currentValues = mostValuablePlayerRate[player.id] || [];
        const playerValue = mvpPlayers[player.id] || 0;
        mostValuablePlayerRate[player.id] = [...currentValues, playerValue];
      }
      const lineupResults = await getDocument<LineupSuggestionResultDocument>(
        db.doc(`matches/${id}/suggestedLineup/${team.id}`)
      );
      if (!lineupResults) {
        continue;
      }
      const {
        players: lineupPlayers,
        voteCount: lineupVoteCount
      } = lineupResults;
      if (lineupVoteCount === 0) {
        continue;
      }
      if (!teamPlayerIds[team.id]) {
        continue;
      }
      for (const playerId of teamPlayerIds[team.id]) {
        const currentValues = lineupPlayerRate[playerId] || [];
        const playerValue = lineupPlayers[playerId] || 0;
        lineupPlayerRate[playerId] = [...currentValues, playerValue];
      }
    }
  }

  const chunks = splitIntoChunks(allPlayerIds, 500);
  for (const chunk of chunks) {
    const batch = db.batch();
    for (const playerId of chunk) {
      const mvpRate = mostValuablePlayerRate[playerId] || [];
      const lineupRate = lineupPlayerRate[playerId] || [];
      const allAverage = getPopularity(mvpRate, lineupRate);
      const latestMvpRate = mvpRate.slice(-4);
      const latestLineupRate = lineupRate.slice(-4);
      const latestAverage = getPopularity(latestMvpRate, latestLineupRate);
      const value = latestAverage * 0.8 + allAverage * 0.2;
      const doc: PopularityDocument = {
        value,
        latestAverage,
        allAverage
      };
      const docRef = db.doc(`players/${playerId}/popularity/${seasonId}`);
      batch.set(docRef, doc);
    }
    await batch.commit();
  }

  return {
    mostValuable: Object.keys(mostValuablePlayerRate).length,
    lineup: Object.keys(lineupPlayerRate).length,
    all: allPlayerIds.length
  };
}

export const seasonPlayerPopularity: CustomJobExecutor<ID, any> = {
  execute: generateSeasonPlayerPopularity,
  nextScheduledTask: async () => futureTimestamp(RUN_INTERVAL),
  argFromVote: vote => {
    if (
      vote.type !== VoteType.MostValuablePlayer &&
      vote.type !== VoteType.Lineup
    ) {
      throw new Error(
        `Invalid type: ${vote.type} received in ${CustomJobType.SeasonPlayerPopularity}`
      );
    }
    const { seasonId } = vote;
    return seasonId;
  },
  type: CustomJobType.SeasonPlayerPopularity
};

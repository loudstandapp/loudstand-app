import { DataImporterWithUpdate } from "../misc/models";
import { MatchTeamStatistics } from "../../submodules/loudstand-shared";
import { sportRadarApiKey, sportRadarApi4Prefix } from "../misc/apiKeys";

type MatchId = string;

const LAST_REFRESH_AFTER_START = 5 * 60 * 60;
const REFRESH_INTERVAL_AFTER_START = 5 * 60;
const REFRESH_INTERVAL_NO_DATA = 60 * 60;

function convertStatistics(statisticsCompetitors: any): MatchTeamStatistics {
  const defaultValues = {
    home: 0,
    away: 0
  };
  return statisticsCompetitors.reduce(
    (acc: MatchTeamStatistics, team: any) =>
      team.statistics
        ? Object.keys(team.statistics).reduce(
            (teamAcc, key) => ({
              ...teamAcc,
              [key]: {
                ...defaultValues,
                ...teamAcc[key],
                [team.qualifier]: team.statistics[key]
              }
            }),
            acc
          )
        : {},
    {}
  );
}

export const matchTeamStatistics: DataImporterWithUpdate<
  MatchId,
  any,
  MatchTeamStatistics | null,
  any
> = {
  getUrl: (id: MatchId) =>
    `https://api.sportradar.us/soccer/${sportRadarApi4Prefix}/en/sport_events/${id}/summary.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    const nextRefresh = Date.now() + REFRESH_INTERVAL_AFTER_START * 1000;
    if (!input.sport_event) {
      return new Date(
        Date.now() + REFRESH_INTERVAL_NO_DATA * 1000
      ).toISOString();
    }

    const startTimestamp = new Date(input.sport_event.start_time).getTime();
    const lastRefresh = startTimestamp + LAST_REFRESH_AFTER_START;

    if (Date.now() < startTimestamp) {
      return new Date(startTimestamp).toISOString();
    }
    if (Date.now() > lastRefresh) {
      return null;
    }

    const expiration = Math.max(lastRefresh, nextRefresh);

    return new Date(expiration).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("matches")
      .doc(id)
      .collection("attributes")
      .doc("teamStatistics");
  },
  processData(input) {
    if (!input.statistics) {
      return null;
    }
    return convertStatistics(input.statistics.totals.competitors);
  },
  getImportResult(output) {
    return output && Object.keys(output).length;
  },
  mergeUpdate(current, update) {
    if (!update || !update.payload || !update.payload.statistics) {
      return null;
    }
    const body = convertStatistics(
      update.payload.statistics.competitors || update.payload.statistics.teams
    );
    return body;
  },
  getIdFromUpdate: async update => {
    return update.metadata.sport_event_id;
  },
  key: "teamStatistics"
};

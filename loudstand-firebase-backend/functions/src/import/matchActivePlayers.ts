import { DataImporter } from "../misc/models";
import {
  MatchActivePlayers,
  PlayerList,
  TeamPlayer
} from "../../submodules/loudstand-shared";
import { sportRadarApiKey, sportRadarApi4Prefix } from "../misc/apiKeys";
import { teamPlayers } from "./teamPlayers";
import Axios from "axios";
import { validateStatus } from "../misc/importAction";

type MatchId = string;

const LAST_REFRESH_AFTER_START = 5 * 60 * 60;
const REFRESH_INTERVAL_AFTER_START = 5 * 60;
const REFRESH_INTERVAL_BEFORE_START = 30 * 60;
const REFRESH_INTERVAL_NO_DATA = 1 * 60;

function playerListFromStats(
  teamStats: any,
  teamPlayerList: PlayerList
): TeamPlayer[] {
  return teamStats.players
    .filter(
      (player: any) =>
        player.starter ||
        (player.statistics && player.statistics.substituted_in > 0)
    )
    .map((player: any) => teamPlayerList[player.id])
    .filter((teamPlayer: TeamPlayer | undefined) => !!teamPlayer);
}

interface Context {
  teamPlayers?: {
    home: PlayerList | null;
    away: PlayerList | null;
  };
}

async function getPlayersForTeam(
  input: any,
  qualifier: "home" | "away"
): Promise<PlayerList | null> {
  if (!input.sport_event) {
    console.error("getPlayersForTeam: No sport event in input: ", input);
    return null;
  }
  const team = input.sport_event.competitors.find(
    (item: any) => item.qualifier === qualifier
  );
  const teamPlayersUrl = teamPlayers.getUrl(team.id) as string;
  const teamPlayersResponse = await Axios.get(teamPlayersUrl, {
    validateStatus
  });
  const teamPlayersData = teamPlayers.processData(
    teamPlayersResponse.data,
    team.id,
    null
  );

  return teamPlayersData;
}

export const matchActivePlayers: DataImporter<
  MatchId,
  any,
  MatchActivePlayers | null,
  any
> = {
  preProcessData: async (arg: MatchId, input: any, context: Context) => {
    // get teams and stuff
    try {
      const home = await getPlayersForTeam(input, "home");
      const away = await getPlayersForTeam(input, "away");
      context.teamPlayers = {
        home,
        away
      };
      return true;
    } catch (e) {
      console.error("matchActivePlayers: error loading teams ", e);
      return false;
    }
  },
  getUrl: (id: MatchId) =>
    `https://api.sportradar.us/soccer/${sportRadarApi4Prefix}/en/sport_events/${id}/summary.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    const nextRefresh = Date.now() + REFRESH_INTERVAL_AFTER_START * 1000;
    if (!input.sport_event) {
      return new Date(
        Date.now() + REFRESH_INTERVAL_NO_DATA * 1000
      ).toISOString();
    }
    // Data is currently not stable at the end of the match
    // if (
    //   input.sport_event_status &&
    //   input.sport_event_status.status === "closed"
    // ) {
    //   return null;
    // }

    const startTimestamp = new Date(input.sport_event.start_time).getTime();
    const finalRefresh = startTimestamp + LAST_REFRESH_AFTER_START * 1000;

    if (Date.now() < startTimestamp) {
      return new Date(
        Date.now() + REFRESH_INTERVAL_BEFORE_START * 1000
      ).toISOString();
    }
    if (Date.now() > finalRefresh) {
      return null;
    }

    const expiration = Math.max(finalRefresh, nextRefresh);

    return new Date(expiration).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("matches")
      .doc(id)
      .collection("attributes")
      .doc("activePlayers");
  },
  processData(input, arg, context) {
    if (
      !input.statistics ||
      !input.statistics.totals ||
      !input.statistics.totals.competitors
    ) {
      console.error("matchActivePlayers: No statistics in input: ", arg, input);
      return null;
    }
    if (
      !input.statistics.totals.competitors[0] ||
      !input.statistics.totals.competitors[1] ||
      !input.statistics.totals.competitors[0].players ||
      !input.statistics.totals.competitors[1].players
    ) {
      console.error("matchActivePlayers: No players in input: ", arg, input);
      return null;
    }

    if (
      !context.teamPlayers ||
      !context.teamPlayers.home ||
      !context.teamPlayers.away
    ) {
      console.error(
        "matchActivePlayers: No teamplayers from API: ",
        arg,
        context
      );
      return null;
    }

    const awayTeamStats = input.statistics.totals.competitors.find(
      (team: any) => team.qualifier === "away"
    );
    const homeTeamStats = input.statistics.totals.competitors.find(
      (team: any) => team.qualifier === "home"
    );
    return {
      away: playerListFromStats(awayTeamStats, context.teamPlayers.away),
      home: playerListFromStats(homeTeamStats, context.teamPlayers.home)
    };
  },
  getImportResult(output) {
    return output && { home: output.home.length, away: output.away.length };
  },
  key: "matchActivePlayers"
};

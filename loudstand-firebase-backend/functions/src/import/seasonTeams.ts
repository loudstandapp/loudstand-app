import { DataImporter } from "../misc/models";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import { TeamList } from "../../submodules/loudstand-shared";
import { countryAlpha3ToAlpha2 } from "../misc/utils";

type SeasonId = string;

const REFRESH_INTERVAL_NO_DATA = 24 * 60 * 60;

export const seasonTeams: DataImporter<SeasonId, any, TeamList | null> = {
  getUrl: (id: SeasonId) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/tournaments/${id}/info.json?api_key=${sportRadarApiKey}`,
  getExpiration(input) {
    if (!input.groups) {
      return new Date(
        Date.now() + REFRESH_INTERVAL_NO_DATA * 1000
      ).toISOString();
    }
    return null;
  },
  getDestination(id, db) {
    return db
      .collection("seasons")
      .doc(id)
      .collection("attributes")
      .doc("teams");
  },
  processData(input) {
    if (!input.groups) {
      return null;
    }

    const output = input.groups.reduce((groupAcc: TeamList, group: any) => {
      const teams = group.teams.reduce(
        (teamAcc: TeamList, team: any) => ({
          ...teamAcc,
          [team.id]: {
            id: team.id,
            name: team.name,
            country: countryAlpha3ToAlpha2(team.country_code)
          }
        }),
        {}
      );
      return { ...groupAcc, ...teams };
    }, {});
    return output;
  },
  getImportResult(output) {
    return !!output;
  },
  key: "seasonTeams"
};

import { DataImporter } from "../misc/models";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import {
  SeasonPlayerStandingsByType,
  SeasonPlayerStandingType,
  ID,
  SeasonPlayerStanding
} from "../../submodules/loudstand-shared";
import {
  getComparatorForKey,
  cleanPlayerName,
  countryAlpha3ToAlpha2
} from "../misc/utils";

type PlayerId = string;

const EXPIRATION_TIME = 3 * 60 * 60;
const TOP_POSITION_LIMIT = 20;

function convertOrdinaryItem(input: any, getValue: (item: any) => number) {
  const topResults = input.filter(
    (item: any) => item.rank <= TOP_POSITION_LIMIT
  );
  // const topResults = input.slice(0, TOP_POSITION_LIMIT);

  const playersForType = topResults.reduce(
    (itemResult: { [id in ID]: SeasonPlayerStanding }, item: any) => ({
      ...itemResult,
      [item.player.id]: {
        value: getValue(item),
        position: item.rank,
        player: {
          id: item.player.id,
          name: cleanPlayerName(item.player.name)
        },
        team: {
          id: item.team.id,
          name: item.team.name,
          country: countryAlpha3ToAlpha2(item.team.country_code)
        }
      }
    }),
    {}
  );
  return playersForType;
}

function convertFieldItem(input: any, field: string) {
  const filtered = input.filter((item: any) => item[field] !== 0);
  const sorted: any[] = filtered.sort(getComparatorForKey(field, true));
  const rankedResults = sorted.reduce(
    (result, item) => {
      const { lastValue, lastPosition } = result;
      const output = {
        value: item[field],
        position:
          item[field] === lastValue
            ? lastPosition
            : Object.keys(result.ranked).length + 1,
        player: {
          id: item.player.id,
          name: cleanPlayerName(item.player.name)
        },
        team: {
          id: item.team.id,
          name: item.team.name
        }
      };
      return {
        ranked: [...result.ranked, output],
        lastValue: output.value,
        lastPosition: output.position
      };
    },
    {
      ranked: [],
      lastValue: -1000000,
      lastPosition: 0
    }
  );
  const topResults = rankedResults.ranked.filter(
    (item: any) => item.position <= TOP_POSITION_LIMIT
  );
  // const topResults = rankedResults.ranked.slice(0, TOP_POSITION_LIMIT);
  const resultByKey = topResults.reduce(
    (result: any, item: any) => ({
      ...result,
      [item.player.id]: item
    }),
    {}
  );
  return resultByKey;
}

function getEntriesForType(
  type: string,
  input: any
): SeasonPlayerStandingsByType | null {
  switch (type) {
    case "top_points": {
      return {
        [SeasonPlayerStandingType.TopPoints]: convertOrdinaryItem(
          input,
          ({ goals, assists }) => goals + assists
        )
      };
    }
    case "top_goals": {
      return {
        [SeasonPlayerStandingType.TopGoals]: convertOrdinaryItem(
          input,
          ({ goals }) => goals
        )
      };
    }
    case "top_assists": {
      return {
        [SeasonPlayerStandingType.TopAssists]: convertOrdinaryItem(
          input,
          ({ assists }) => assists
        )
      };
    }
    case "top_cards": {
      return {
        [SeasonPlayerStandingType.TopRedCards]: convertFieldItem(
          input,
          "red_cards"
        ),
        [SeasonPlayerStandingType.TopYellowCards]: convertFieldItem(
          input,
          "yellow_cards"
        ),
        [SeasonPlayerStandingType.TopYellowRedCards]: convertFieldItem(
          input,
          "yellow_red_cards"
        )
      };
    }
    case "top_own_goals": {
      return {
        [SeasonPlayerStandingType.TopOwnGoals]: convertOrdinaryItem(
          input,
          ({ own_goals }) => own_goals
        )
      };
    }
    default:
      return null;
  }
}

export const seasonPlayerStandings: DataImporter<
  PlayerId,
  any,
  SeasonPlayerStandingsByType | null
> = {
  getUrl: (id: PlayerId) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/tournaments/${id}/leaders.json?api_key=${sportRadarApiKey}`,
  getExpiration(input) {
    if (!input || !input.tournament) {
      return new Date(Date.now() + EXPIRATION_TIME * 1000).toISOString();
    }
    const now = new Date().toISOString();
    if (now > input.tournament.current_season.end_date) {
      return null;
    }
    return new Date(Date.now() + EXPIRATION_TIME * 1000).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("seasons")
      .doc(id)
      .collection("attributes")
      .doc("playerStandings");
  },
  getImportResult(output) {
    return output ? Object.keys(output).length : null;
  },
  processData(input: any) {
    const output = Object.keys(input).reduce(
      (result: SeasonPlayerStandingsByType, key: string) => {
        const items = input[key];
        const entries = getEntriesForType(key, items);

        return {
          ...result,
          ...entries
        };
      },
      {}
    );

    return output;
  },
  key: "seasonPlayerStandings"
};

import { DataImporter } from "../misc/models";
import { ID, TeamRoot } from "../../submodules/loudstand-shared";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import { cleanPlayerName, countryAlpha3ToAlpha2 } from "../misc/utils";

const REFRESH_INTERVAL = 24 * 60 * 60;

interface Arg {
  id: ID;
  defaultTournamentId: ID | null;
  defaultCurrentSeasonId: ID | null;
}

export const teamRoot: DataImporter<Arg, any, TeamRoot | null> = {
  getUrl: ({ id }) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/teams/${id}/profile.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    const next = new Date(Date.now() + REFRESH_INTERVAL * 1000).getTime();
    return new Date(next).toISOString();
  },
  getDestination({ id }, db) {
    return db.collection("teams").doc(id);
  },
  processData(input, { defaultCurrentSeasonId, defaultTournamentId }) {
    if (!input.team) {
      return null;
    }

    const { team, manager, jerseys } = input;

    const jersey = jerseys && jerseys.find((item: any) => item.type === "home");

    const output: TeamRoot = {
      id: team.id,
      name: team.name,
      defaultCurrentSeasonId: defaultCurrentSeasonId || null,
      defaultTournamentId: defaultTournamentId || null,
      country: countryAlpha3ToAlpha2(team.country_code),
      manager: manager
        ? {
            id: manager.id,
            name: cleanPlayerName(manager.name)
          }
        : null,
      jerseyColors: jersey
        ? {
            base: `#${jersey.base}`,
            sleeve: `#${jersey.sleeve}`,
            number: `#${jersey.number}`
          }
        : null
    };
    return output;
  },
  getImportResult(output) {
    return true;
  },
  key: "teamRoot"
};

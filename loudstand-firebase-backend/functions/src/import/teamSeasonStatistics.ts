import { DataImporter } from "../misc/models";
import { sportRadarApiKey, sportRadarApi4Prefix } from "../misc/apiKeys";
import {
  ID,
  TournamentRoot,
  TeamSeasonStatistics,
  TeamStatisticsSeason,
  TeamStatisticsPlayer
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { cleanPlayerName } from "../misc/utils";

type TeamId = string;

const REFRESH_INTERVAL = 24 * 60 * 60;

export const teamSeasonStatistics: DataImporter<
  TeamId,
  any[],
  TeamSeasonStatistics | null
> = {
  getUrl: async (teamId: TeamId) => {
    // TODO: Generalize this code
    const db = firestore();
    const tournamentsRef = db.collection("tournaments");
    const collectionQuerySnap = await tournamentsRef.get();
    const supportedSeasonIds: ID[] = collectionQuerySnap.docs.reduce(
      (acc: ID[], snap) => {
        const doc = snap.data();
        if (!doc || !doc.body) {
          return acc;
        }
        const tournament = doc.body as TournamentRoot;
        return [...acc, tournament.currentSeason.id];
      },
      []
    );
    const urls = supportedSeasonIds.map(
      seasonId =>
        `https://api.sportradar.us/soccer/${sportRadarApi4Prefix}/en/seasons/${seasonId}/competitors/${teamId}/statistics.json?api_key=${sportRadarApiKey}`
    );
    return urls;
  },
  getExpiration() {
    return new Date(Date.now() + REFRESH_INTERVAL * 1000).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("teams")
      .doc(id)
      .collection("attributes")
      .doc("seasonStatistics");
  },
  processData(input, arg) {
    const allStatisticsForAllSeasons: TeamSeasonStatistics = input.reduce(
      (acc: TeamSeasonStatistics, statistics: any) => {
        if (!statistics.season) {
          return acc;
        }
        if (!statistics.competitor) {
          return acc;
        }
        if (!statistics.competitor.statistics) {
          return acc;
        }
        if (Object.keys(statistics.competitor.statistics).length === 0) {
          return acc;
        }
        const season: TeamStatisticsSeason = {
          id: statistics.season.id,
          name: statistics.season.name,
          team: statistics.competitor.statistics,
          players: statistics.competitor.players
            ? statistics.competitor.players.reduce(
                (
                  allPlayers: { [key in ID]: TeamStatisticsPlayer },
                  item: any
                ) => {
                  if (!item.statistics) {
                    return allPlayers;
                  }
                  const player: TeamStatisticsPlayer = {
                    id: item.id,
                    name: cleanPlayerName(item.name),
                    statistics: item.statistics
                  };
                  return { ...allPlayers, [player.id]: player };
                },
                {}
              )
            : {}
        };

        return { ...acc, [statistics.season.id]: season };
      },
      {}
    );
    return allStatisticsForAllSeasons;
  },
  getImportResult(output) {
    return output ? { seasons: Object.keys(output).length } : false;
  },
  key: "teamSeasonStatistics"
};

import { DataImporter } from "../misc/models";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import { PlayerList, PlayerPosition } from "../../submodules/loudstand-shared";
import { cleanPlayerName, countryAlpha3ToAlpha2 } from "../misc/utils";

type TeamId = string;

const REFRESH_INTERVAL = 24 * 60 * 60;

export const playerTypeMap: { [key in string]: PlayerPosition } = {
  midfielder: PlayerPosition.Midfielder,
  goalkeeper: PlayerPosition.Goalkeeper,
  forward: PlayerPosition.Forward,
  defender: PlayerPosition.Defender
};

export const teamPlayers: DataImporter<TeamId, any, PlayerList | null> = {
  getUrl: (id: TeamId) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/teams/${id}/profile.json?api_key=${sportRadarApiKey}`,
  getExpiration() {
    return new Date(Date.now() + REFRESH_INTERVAL * 1000).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("teams")
      .doc(id)
      .collection("attributes")
      .doc("players");
  },
  processData(input) {
    if (!input.players) {
      return null;
    }
    const players = input.players.reduce(
      (acc: PlayerList, player: any) => ({
        ...acc,
        [player.id]: {
          id: player.id,
          name: cleanPlayerName(player.name),
          position: playerTypeMap[player.type] || null,
          number: player.jersey_number || null,
          country: countryAlpha3ToAlpha2(player.country_code),
          birthDate: player.date_of_birth || null
        }
      }),
      {}
    );
    return players;
  },
  getImportResult(output) {
    return output ? output.length : false;
  },
  key: "teamPlayers",
  disableMerge: true
};

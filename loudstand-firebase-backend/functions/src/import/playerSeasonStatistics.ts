import { DataImporter } from "../misc/models";
import { ID, PlayerSeasonStatistics } from "../../submodules/loudstand-shared";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";

const REFRESH_INTERVAL = 24 * 60 * 60;

type Arg = {
  playerId: ID;
  seasonId: ID;
};
// TODO translate player names some day in the future

export const playerSeasonStatistics: DataImporter<
  Arg,
  any,
  PlayerSeasonStatistics | null
> = {
  getUrl: ({ playerId }) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/players/${playerId}/profile.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    const next = new Date(Date.now() + REFRESH_INTERVAL * 1000).getTime();
    return new Date(next).toISOString();
    // TBD: Use last_event_time in response to determine if season is active.
  },
  getDestination({ playerId, seasonId }, db) {
    return db
      .collection("players")
      .doc(playerId)
      .collection("seasonStatistics")
      .doc(seasonId);
  },
  processData(input, { seasonId }) {
    if (!input.statistics || !input.statistics.seasons) {
      return null;
    }
    const season = input.statistics.seasons.find(
      ({ id }: any) => id === seasonId
    );
    if (!season) {
      return null;
    }
    const { last_event_time, ...rest } = season.statistics;
    return rest;
  },
  getImportResult(output) {
    return {
      imported: !!output,
      count: output && Object.keys(output).length
    };
  },
  key: "playerSeasonStatistics"
};

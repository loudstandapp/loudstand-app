import { tournamentSchedule } from "../../test-data/sportradar-tournament-schedule";
import { seasonTeamMatches } from "./seasonTeamMatches";

const TEST_SEASON_ID = "123456";
const existingData: any = {
  "2018-03-03": {
    "sr:match:123456": {
      seasonId: "blabla"
    }
    // day outside range
  },
  "2019-10-03": {
    "sr:match:123456": {
      seasonId: "blabla"
    }
    // day outside range
  },
  // 2019-03-16 does not exist  X
  "2019-03-03": {
    // currently empty - one is to be added  X
  },
  "2019-05-12": {
    // one match from another tourmanemt/season - one is to be added X
    "sr:match:123456": {
      seasonId: "blabla"
    }
  },
  "2019-05-29": {
    // one match from another tourmanemt/season - no changes X
    "sr:match:98765": {
      seasonId: "blabla"
    }
  },
  "2018-12-22": {
    // one match from this tourmanemt - update X
    "sr:match:14946011": {
      seasonId: TEST_SEASON_ID
    }
  },
  "2019-01-02": {
    // two matches, from this + another tourmanemt - update + no change X
    "sr:match:45353": {
      seasonId: "blabla"
    },
    "sr:match:14946019": {
      seasonId: TEST_SEASON_ID
    }
  },
  // a match from current tournament, which is moved to another date X
  "2019-03-01": {
    "sr:match:14946195": {
      seasonId: TEST_SEASON_ID
    }
  },
  // a day where the match is moved to
  // "2019-03-03": {}
  // a match which is simply removed X
  "2018-12-24": {
    "sr:match:13245": {
      seasonId: TEST_SEASON_ID
    }
  }
};

describe("seasonTeamMatches", () => {
  const updates = seasonTeamMatches.getUpdates(tournamentSchedule);
  const matchesForTeam = updates["sr:competitor:3026"];
  const merged: any = seasonTeamMatches.mergeUpdate(
    TEST_SEASON_ID,
    existingData,
    matchesForTeam,
    tournamentSchedule
  );

  it("creates day that does not exist", () => {
    const DATE = "2019-03-10";
    expect(merged[DATE]).toEqual({
      "sr:match:14946213": expect.any(Object)
    });
  });
  it("creates match on a day that exists and is empty", () => {
    const DATE = "2019-03-03";
    expect(merged[DATE]).toEqual({
      "sr:match:14946195": expect.any(Object)
    });
  });
  it("creates match on a day that has a match from another season", () => {
    const DATE = "2019-05-12";
    expect(merged[DATE]).toEqual({
      "sr:match:14946357": expect.any(Object),
      "sr:match:123456": expect.any(Object)
    });
  });
  it("leaves a match intact on a day with no matches from this season", () => {
    const DATE = "2019-05-29";
    expect(merged[DATE]).toEqual({
      "sr:match:98765": expect.any(Object)
    });
    expect(merged[DATE]["sr:match:98765"]).toBe(
      existingData[DATE]["sr:match:98765"]
    );
  });
  it("updates match from this season on a day one match", () => {
    const DATE = "2018-12-22";
    expect(merged[DATE]).toEqual({
      "sr:match:14946011": expect.any(Object)
    });
    expect(merged[DATE]["sr:match:14946011"]).not.toBe(
      existingData[DATE]["sr:match:14946011"]
    );
  });
  it("leaves a foreign match intact, updates match from this season, on a day with both", () => {
    const DATE = "2019-01-02";
    expect(merged[DATE]).toEqual({
      "sr:match:14946019": expect.any(Object),
      "sr:match:45353": expect.any(Object)
    });
    expect(merged[DATE]["sr:match:14946019"]).not.toBe(
      existingData[DATE]["sr:match:14946019"]
    );
    expect(merged[DATE]["sr:match:45353"]).toBe(
      existingData[DATE]["sr:match:45353"]
    );
  });
  it("moves a match to another date", () => {
    const DATE_ORIG = "2019-03-01";
    const DATE_NEW = "2019-03-03";
    expect(merged[DATE_NEW]).toEqual({
      "sr:match:14946195": expect.any(Object)
    });
    expect(merged[DATE_ORIG]).toEqual({});
  });
  it("removes a match that no longer exist", () => {
    const DATE = "2018-12-24";
    expect(merged[DATE]).toEqual({});
  });

  it("does not touch dates outside season period", () => {
    const DATE1 = "2018-03-03";
    const DATE2 = "2019-10-03";
    expect(merged[DATE1]).toBe(existingData[DATE1]);
    expect(merged[DATE2]).toBe(existingData[DATE2]);
  });
});

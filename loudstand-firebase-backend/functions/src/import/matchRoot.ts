import { DataImporterWithUpdate } from "../misc/models";
import {
  MatchRoot,
  MatchState,
  MatchStatus,
  MatchStatusType,
  MatchScore,
  ISO8601Timestamp
} from "../../submodules/loudstand-shared";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import {
  countryAlpha3ToAlpha2,
  cleanPlayerName,
  cleanDate
} from "../misc/utils";

type MatchId = string;

const REFRESH_INTERVAL_BEFORE_START = 30 * 60;
const REACTION_TIME_BEFORE_MATCH = 2 * 24 * 60 * 60;
const REACTION_TIME_AFTER_MATCH = 2 * 24 * 60 * 60;

export function matchReactionStartTime(startTime: ISO8601Timestamp) {
  return new Date(
    new Date(startTime).getTime() - REACTION_TIME_BEFORE_MATCH * 1000
  ).toISOString();
}

export function matchReactionEndTime(startTime: ISO8601Timestamp) {
  return new Date(
    new Date(startTime).getTime() + REACTION_TIME_AFTER_MATCH * 1000
  ).toISOString();
}

export const matchStateMap: { [key: string]: MatchState } = {
  not_started: MatchState.PreMatch,
  live: MatchState.Live,
  postponed: MatchState.PostMatch,
  delayed: MatchState.PreMatch,
  start_delayed: MatchState.PreMatch,
  cancelled: MatchState.PreMatch,
  ended: MatchState.PostMatch,
  closed: MatchState.PostMatch,
  abandoned: MatchState.PostMatch
};

export const matchStatusMap: { [key: string]: MatchStatusType } = {
  not_started: MatchStatusType.NotStarted,
  live: MatchStatusType.Live,
  "1st_half": MatchStatusType.FirstHalf,
  "2nd_half": MatchStatusType.SecondHalf,
  "1st_extra": MatchStatusType.FirstExtra,
  "2nd_extra": MatchStatusType.SecondExtra,
  overtime: MatchStatusType.Overtime,
  awaiting_penalties: MatchStatusType.AwaitingPenalties,
  penalties: MatchStatusType.Penalties,
  pause: MatchStatusType.Pause,
  awaiting_extra_time: MatchStatusType.AwaitingExtraTime,
  interrupted: MatchStatusType.Interrupted,
  halftime: MatchStatusType.HalfTime,
  "full-time": MatchStatusType.FullTime,
  extra_time: MatchStatusType.ExtraTime,
  aet: MatchStatusType.EndedAfterExtraTime,
  ap: MatchStatusType.EndedAfterPenalties,
  postponed: MatchStatusType.Postponed,
  delayed: MatchStatusType.Delayed,
  cancelled: MatchStatusType.Cancelled,
  ended: MatchStatusType.Ended,
  closed: MatchStatusType.Closed,
  abandoned: MatchStatusType.Abandoned
};

function getPenaltyScore(sport_event_status: any): MatchScore | null {
  if (!sport_event_status.period_scores) {
    return null;
  }
  const periods = sport_event_status.period_scores;
  const penaltyPeriod = periods.find(
    (period: any) => period.type === "penalties"
  );
  if (!penaltyPeriod) {
    return null;
  }
  return {
    home: penaltyPeriod.home_score,
    away: penaltyPeriod.away_score
  };
}

export const matchRoot: DataImporterWithUpdate<MatchId, any, MatchRoot, any> = {
  getUrl: (id: MatchId) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/matches/${id}/summary.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    const nextRefresh = Date.now() + REFRESH_INTERVAL_BEFORE_START * 1000;
    if (!input.sport_event) {
      return new Date(nextRefresh).toISOString();
    }

    const startTimestamp = new Date(input.sport_event.scheduled).getTime();
    if (Date.now() > startTimestamp) {
      return null;
    }

    // Never expire after start of match
    const expiration = Math.min(nextRefresh, startTimestamp);

    return new Date(expiration).toISOString();
  },
  getDestination(id, db) {
    return db.collection("matches").doc(id);
    // TODO: make this from entityID and attribute IDs.
  },
  processData(input) {
    const { sport_event, sport_event_status, sport_event_conditions } = input;
    if (!sport_event || !sport_event.competitors) {
      throw new Error("not found");
    }

    const homeTeam = sport_event.competitors.find(
      ({ qualifier }: any) => qualifier === "home"
    );
    const awayTeam = sport_event.competitors.find(
      ({ qualifier }: any) => qualifier === "away"
    );

    const homeScore = sport_event_status.home_score || 0;
    const awayScore = sport_event_status.away_score || 0;

    const startTime = sport_event.scheduled;
    const venue = sport_event.venue
      ? { name: sport_event.venue.name, id: sport_event.venue.id }
      : null;
    const state = matchStateMap[sport_event_status.status];

    const matchStatus: MatchStatus | null =
      sport_event_status.match_status &&
      matchStatusMap[sport_event_status.match_status]
        ? {
            current: matchStatusMap[sport_event_status.match_status]
          }
        : null;

    return {
      id: sport_event.id,
      reactionStartTime: startTime ? matchReactionStartTime(startTime) : null,
      reactionEndTime: startTime ? matchReactionEndTime(startTime) : null,
      state: state || null,
      matchStatus,
      startTime: startTime || null,
      venue,
      tournament: {
        id: sport_event.tournament.id,
        name: sport_event.tournament.name
      },
      date: cleanDate(sport_event.scheduled),
      score: {
        home: homeScore,
        away: awayScore
      },
      penaltyScore: getPenaltyScore(sport_event_status),
      season: sport_event.season
        ? {
            id: sport_event.season.id,
            name: sport_event.season.name
          }
        : null,
      referee:
        sport_event_conditions && sport_event_conditions.referee
          ? {
              id: sport_event_conditions.referee.id,
              name: cleanPlayerName(sport_event_conditions.referee.name),
              country: countryAlpha3ToAlpha2(
                sport_event_conditions.referee.country_code
              )
            }
          : null,
      teams:
        homeTeam && awayTeam
          ? {
              home: {
                id: homeTeam.id,
                name: homeTeam.name,
                country: countryAlpha3ToAlpha2(homeTeam.country_code)
              },
              away: {
                id: awayTeam.id,
                name: awayTeam.name,
                country: countryAlpha3ToAlpha2(awayTeam.country_code)
              }
            }
          : null
    };
  },
  getImportResult(output) {
    return !!output;
  },
  getIdFromUpdate: async update => {
    return update.metadata.sport_event_id;
  },
  mergeUpdate(current, update) {
    const matchStatusEvents = ["period_start", "break_start"];
    const stateEvents = ["match_started", "match_ended"];

    if (
      !update.payload ||
      !update.payload.sport_event_status ||
      !update.payload.event
    ) {
      return null;
    }
    const score = {
      home: update.payload.sport_event_status.home_score,
      away: update.payload.sport_event_status.away_score
    };
    const penaltyScore = getPenaltyScore(update.payload.sport_event_status);

    // Update the overall state of the match
    if (stateEvents.includes(update.payload.event.type)) {
      const state = matchStateMap[update.payload.sport_event_status.status];
      const matchStatus = {
        current: matchStatusMap[update.payload.sport_event_status.match_status],
        time: update.payload.event.time
      };
      const updated: Partial<MatchRoot> = {
        ...current,
        matchStatus,
        score,
        penaltyScore,
        state
      };
      return updated as MatchRoot;

      // Update the detailed status and time of the match
    } else if (matchStatusEvents.includes(update.payload.event.type)) {
      const matchStatus = {
        current: matchStatusMap[update.payload.sport_event_status.match_status],
        time: update.payload.event.time
      };
      const updated: Partial<MatchRoot> = {
        ...current,
        score,
        penaltyScore,
        matchStatus
      };
      return updated as MatchRoot;
      // Just to be very safe, we update score on every received event.
    } else {
      return {
        ...current,
        penaltyScore,
        score
      } as MatchRoot;
    }
  },
  key: "matchRoot"
  // TODO: add list of all attributes
};

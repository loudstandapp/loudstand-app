import { DataImporter } from "../misc/models";
import {
  ID,
  TournamentRoot,
  TeamList
} from "../../submodules/loudstand-shared";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import { firestore } from "firebase-admin";
import { startImport } from "../misc/importAction";
import { teamRoot } from "./teamRoot";
import { sleep, countryAlpha3ToAlpha2 } from "../misc/utils";

const EXPIRATION_BEFORE_SEASON_END = 2 * 24 * 60 * 60;
const REFRESH_INTERVAL = 60 * 60;

type TournamentImportArg = {
  id: ID;
  isDefaultForTeams: boolean;
};

interface Context {
  previousValue: TournamentRoot | null;
}

const REFRESH_INTERVAL_NO_DATA = 24 * 60 * 60;

export const tournamentRoot: DataImporter<
  TournamentImportArg,
  any,
  TournamentRoot | null,
  Context
> = {
  preImport: async (arg: TournamentImportArg, context: Context) => {
    console.log("preImport");
    try {
      const db = firestore();
      const currentRef = tournamentRoot.getDestination(arg, db);
      const currentSnap = await currentRef.get();
      const currentData = currentSnap.data();
      context.previousValue =
        (currentData && (currentData.body as TournamentRoot)) || null;
      return true;
    } catch (e) {
      console.error(e);
      return false;
    }
  },
  postImport: async (
    arg: TournamentImportArg,
    input: any,
    output: TournamentRoot | null,
    context: Context
  ) => {
    console.log("post import");
    if (!output) {
      return false;
    }
    // Tournament is not default, do nothing
    if (!output.isDefaultForTeams) {
      return true;
    }
    // No teams defined in data from API
    if (!input.groups) {
      console.log("No teams defined in data from API");
      return false;
    }
    try {
      // Handle no change for season/teams
      if (
        context &&
        context.previousValue &&
        context.previousValue.currentSeason.id === output.currentSeason.id
      ) {
        console.log("currentSeason.id === output.currentSeason.id");
        return true;
      }
      const db = firestore();
      // Handle season change - remove defaults for teams of previous season
      if (
        context &&
        context.previousValue &&
        context.previousValue.currentSeason.id !== output.currentSeason.id
      ) {
        console.log("currentSeason.id !== output.currentSeason.id");
        const oldTeamsRef = db
          .collection("seasons")
          .doc(context.previousValue.currentSeason.id)
          .collection("attributes")
          .doc("teams");
        const oldTeamsSnap = await oldTeamsRef.get();
        const oldTeamsData = oldTeamsSnap.data();
        if (oldTeamsData) {
          const teams: TeamList = oldTeamsData.body;
          const batch = db.batch();
          Object.keys(teams).forEach(teamId => {
            const ref = db.collection("teams").doc(teamId);
            batch.update(ref, {
              "body.defaultCurrentSeasonId": null,
              "body.defaultTournamentId": null
            });
          });
          await batch.commit();
        }
      }
      // Handle create tournament or new season - set as default for all teams.
      const teamIds = input.groups
        .reduce((acc: any[], group: any) => [...acc, ...group.teams], [])
        .map((team: any) => team.id);
      const {
        id,
        currentSeason: { id: currentSeasonId }
      } = output;
      console.log("await teamIds.forEach");
      for (const teamId of teamIds) {
        await startImport(
          {
            id: teamId,
            defaultTournamentId: id,
            defaultCurrentSeasonId: currentSeasonId
          },
          teamRoot
        );
        await sleep(1100);
      }
      return true;
    } catch (e) {
      console.error(e);
      return false;
    }
  },
  getUrl: ({ id }) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/tournaments/${id}/info.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    console.log("getExpiration");
    if (!input || !input.tournament) {
      return new Date(
        Date.now() + REFRESH_INTERVAL_NO_DATA * 1000
      ).toISOString();
    }
    const endDate =
      new Date(input.tournament.current_season.end_date).getTime() -
      EXPIRATION_BEFORE_SEASON_END;
    const next = new Date(Date.now() + REFRESH_INTERVAL * 1000).getTime();
    const first = Math.max(endDate, next);
    return new Date(first).toISOString();
  },
  getDestination({ id }, db) {
    console.log("getDestination");
    return db.collection("tournaments").doc(id);
  },
  processData(input, { isDefaultForTeams }) {
    console.log("processData");
    if (!input.tournament) {
      return null;
    }
    const { tournament } = input;
    const output: TournamentRoot = {
      id: tournament.id,
      name: tournament.name,
      country: countryAlpha3ToAlpha2(tournament.category.country_code),
      isDefaultForTeams: isDefaultForTeams || false,
      currentSeason: {
        id: tournament.current_season.id,
        name: tournament.current_season.name
      }
    };
    return output;
  },
  getImportResult(output) {
    return !!output;
  },
  key: "tournament"
  // TODO: add list of all attributes
};

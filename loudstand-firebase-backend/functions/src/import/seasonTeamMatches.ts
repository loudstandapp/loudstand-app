import { MassUpdater } from "../misc/models";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import { CalendarItemByDay, ID } from "../../submodules/loudstand-shared";
import { convertMatch } from "./seasonMatches";
import { MatchListItem } from "../../submodules/loudstand-shared/models/general";

type SeasonId = string;

type TeamCalendarMap = { [key in ID]: CalendarItemByDay<MatchListItem> };

export const seasonTeamMatches: MassUpdater<
  SeasonId,
  any,
  CalendarItemByDay<MatchListItem> | null,
  CalendarItemByDay<MatchListItem>
> = {
  getUrl: (id: SeasonId) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/tournaments/${id}/schedule.json?api_key=${sportRadarApiKey}`,
  getDestination(id, update, db) {
    return db
      .collection("teams")
      .doc(id)
      .collection("attributes")
      .doc("matchCalendar");
  },
  getUpdates(input) {
    if (!input.sport_events) {
      return null;
    }
    const matches = input.sport_events.map(convertMatch);
    const calendarMap = matches.reduce(
      (result: TeamCalendarMap, match: MatchListItem) => {
        const {
          teams: {
            away: { id: awayTeamId },
            home: { id: homeTeamId }
          },
          date
        } = match;
        const awayMatches =
          (result[awayTeamId] && result[awayTeamId][date]) || {};
        const homeMatches =
          (result[homeTeamId] && result[homeTeamId][date]) || {};

        return {
          ...result,
          [awayTeamId]: {
            ...result[awayTeamId],
            [date]: awayMatches[date]
              ? { ...awayMatches[date], [match.id]: match }
              : { [match.id]: match }
          },
          [homeTeamId]: {
            ...result[homeTeamId],
            [date]: homeMatches[date]
              ? { ...homeMatches[date], [match.id]: match }
              : { [match.id]: match }
          }
        };
      },
      {}
    );
    return calendarMap;
  },
  mergeUpdate(seasonId, current, update, input) {
    if (!input.sport_events) {
      return null;
    }
    if (!current) {
      return update;
    }

    const seasonStart = input.sport_events[0].season.start_date;
    const seasonEnd = input.sport_events[0].season.end_date;
    const existingDates = Object.keys(current).reduce((accDates, date) => {
      if (date < seasonStart || date > seasonEnd) {
        return { ...accDates, [date]: current[date] };
      }
      const otherMatches = Object.keys(current[date]).reduce(
        (accMatches, matchId) => {
          // remove existing matches from same season
          if (current[date][matchId].season.id === seasonId) {
            return accMatches;
          }
          return {
            ...accMatches,
            [matchId]: current[date][matchId]
          };
        },
        {}
      );
      return {
        ...accDates,
        [date]: { ...otherMatches, ...update[date] }
      };
    }, {});
    const output = { ...update, ...existingDates };
    return output;
  }
};

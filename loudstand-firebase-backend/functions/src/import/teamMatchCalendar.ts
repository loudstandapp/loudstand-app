import { DataImporter } from "../misc/models";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import {
  CalendarItemByDay,
  MatchListItem,
  ID,
  TournamentRoot
} from "../../submodules/loudstand-shared";
import { convertMatch } from "./seasonMatches";
import { firestore } from "firebase-admin";

type TeamId = string;

const REFRESH_INTERVAL = 2 * 60 * 60;

export const teamMatchCalendar: DataImporter<
  TeamId,
  any[],
  CalendarItemByDay<MatchListItem> | null
> = {
  disableMerge: true,
  getUrl: async () => {
    const db = firestore();
    const tournamentsRef = db.collection("tournaments");
    const collectionQuerySnap = await tournamentsRef.get();
    const supportedSeasonIds: ID[] = collectionQuerySnap.docs.reduce(
      (acc: ID[], snap) => {
        const doc = snap.data();
        if (!doc || !doc.body) {
          return acc;
        }
        const tournament = doc.body as TournamentRoot;
        return [...acc, tournament.currentSeason.id];
      },
      []
    );

    const urls = supportedSeasonIds.map(
      seasonId =>
        `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/tournaments/${seasonId}/schedule.json?api_key=${sportRadarApiKey}`
    );
    return urls;
  },
  getExpiration() {
    return new Date(Date.now() + REFRESH_INTERVAL * 1000).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("teams")
      .doc(id)
      .collection("attributes")
      .doc("matchCalendar");
  },
  processData(input, arg) {
    const allMatchesForAllSeasons = input.reduce((acc, season) => {
      if (!season.sport_events) {
        return acc;
      }
      return [...acc, ...season.sport_events];
    }, []);

    const allMatchesForTeam = allMatchesForAllSeasons.filter((match: any) =>
      match.competitors.find((team: any) => team.id === arg)
    );

    const convertedMatches: MatchListItem[] = allMatchesForTeam.map(
      convertMatch
    );

    const calendarMap = convertedMatches.reduce(
      (result: CalendarItemByDay<MatchListItem>, match: MatchListItem) => {
        const { date } = match;
        return {
          ...result,
          [date]: { ...result[date], [match.id]: match }
        };
      },
      {}
    );
    return calendarMap;
  },
  getImportResult(output) {
    return output ? { dates: Object.keys(output).length } : false;
  },
  key: "teamMatchCalendar"
};

import { DataImporterWithUpdate } from "../misc/models";
import {
  MatchTimeline,
  MatchEventType,
  MatchEvent
} from "../../submodules/loudstand-shared";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import {
  PlayerBasic,
  MatchPeriodType
} from "../../submodules/loudstand-shared/models/general";
import { cleanPlayerName } from "../misc/utils";

type MatchId = string;

const LAST_REFRESH_AFTER_START = 5 * 60 * 60;
const REFRESH_INTERVAL_AFTER_START = 5 * 60;
const REFRESH_INTERVAL_NO_DATA = 60 * 60;

export const mapMatchPeriodType: { [type: string]: MatchPeriodType } = {
  regular_period: MatchPeriodType.Regular,
  overtime: MatchPeriodType.Overtime,
  penalties: MatchPeriodType.Penalties
};

function getEventTime({ match_time, match_clock }: any): number | null {
  if (!match_time && !match_clock) {
    return null;
  }
  if (!match_clock) {
    return match_time * 60;
  }
  const [min, sec] = match_clock.split(":");
  return parseInt(min) * 60 + parseInt(sec);
}

function getPlayer(player?: any): PlayerBasic | null {
  if (!player) {
    return null;
  }
  return { id: player.id, name: cleanPlayerName(player.name) };
}

function convertEvent(rawEvent: any, status?: any): MatchEvent | undefined {
  const {
    id,
    type,
    team,
    player,
    player_in,
    player_out,
    goal_scorer,
    home_score,
    away_score,
    shootout_home_score,
    shootout_away_score,
    period_type,
    period,
    time
  } = rawEvent;

  const seconds = getEventTime(rawEvent);

  const baseEvent = {
    id: id.toString(),
    seconds,
    team: team || null,
    isReactable: true,
    time,
    periodType: period_type ? mapMatchPeriodType[period_type] : null,
    periodRound: period || null
  };

  switch (type) {
    case "penalty_awarded":
      return {
        ...baseEvent,
        data: {
          type: MatchEventType.Penalty,
          player: getPlayer(player)
        }
      };
    case "yellow_card":
      return {
        ...baseEvent,
        data: {
          type: MatchEventType.YellowCard,
          player: getPlayer(player)
        }
      };
    case "red_card":
      return {
        ...baseEvent,
        data: {
          type: MatchEventType.RedCard,
          player: getPlayer(player)
        }
      };
    case "yellow_red_card":
      return {
        ...baseEvent,
        data: {
          type: MatchEventType.SecondYellowCard,
          player: getPlayer(player)
        }
      };
    case "score_change":
      return {
        ...baseEvent,
        isReactable: false,
        data: {
          type: MatchEventType.Goal,
          player: getPlayer(goal_scorer),
          homeScore: home_score,
          awayScore: away_score,
          homePenaltyScore:
            shootout_home_score !== undefined ? shootout_home_score : null,
          awayPenaltyScore:
            shootout_away_score !== undefined ? shootout_away_score : null
        }
      };
    case "substitution":
      return {
        ...baseEvent,
        data: {
          type: MatchEventType.Substitution,
          playerIn: getPlayer(player_in),
          playerOut: getPlayer(player_out)
        }
      };
    case "match_started":
      return {
        ...baseEvent,
        isReactable: false,
        data: {
          type: MatchEventType.MatchStarted
        }
      };
    case "match_ended":
      return {
        ...baseEvent,
        data: {
          type: MatchEventType.MatchEnded
        }
      };
    case "period_start":
      return {
        ...baseEvent,
        isReactable: false,
        data: {
          type: MatchEventType.PeriodStart
        }
      };
  }
  return;
}
export const matchTimeline: DataImporterWithUpdate<
  MatchId,
  any,
  MatchTimeline | null,
  any // FIXME
> = {
  getUrl: (id: MatchId) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/matches/${id}/timeline.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    const nextRefresh = Date.now() + REFRESH_INTERVAL_AFTER_START * 1000;
    if (!input.sport_event) {
      return new Date(
        Date.now() + REFRESH_INTERVAL_NO_DATA * 1000
      ).toISOString();
    }

    const startTimestamp = new Date(input.sport_event.scheduled).getTime();
    const lastRefresh = startTimestamp + LAST_REFRESH_AFTER_START;

    if (Date.now() < startTimestamp) {
      return new Date(startTimestamp).toISOString();
    }
    if (Date.now() > lastRefresh) {
      return null;
    }

    const expiration = Math.max(lastRefresh, nextRefresh);

    return new Date(expiration).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("matches")
      .doc(id)
      .collection("attributes")
      .doc("timeline");
  },
  processData(input) {
    if (!input.timeline) {
      return null;
    }

    const events = input.timeline.reduce(
      (acc: { [key: string]: MatchEvent }, rawEvent: any) => {
        const event = convertEvent(rawEvent);
        if (!event) {
          return acc;
        }
        const id = event.id.toString();
        return {
          ...acc,
          [id]: event
        };
      },
      {}
    );
    return { events };
  },
  getImportResult(output) {
    return output && Object.keys(output.events).length;
  },
  mergeUpdate(current: MatchTimeline | null, update) {
    const event = convertEvent(
      update.payload.event,
      update.payload.sport_event_status
    );
    if (!event) {
      return null;
    }

    if (!current || !current.events) {
      return {
        events: { [event.id]: event }
      };
    }
    const id = event.id.toString();
    const body = { events: { ...current.events, [id]: event } };
    return body;
  },
  getIdFromUpdate: async update => {
    return update.metadata.sport_event_id;
  },
  key: "timeline"
};

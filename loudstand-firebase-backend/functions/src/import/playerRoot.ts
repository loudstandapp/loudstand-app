import { DataImporter } from "../misc/models";
import {
  ID,
  PlayerRoot,
  PlayerTeamRole,
  PlayerTeamRoleType
} from "../../submodules/loudstand-shared";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import { cleanPlayerName, countryAlpha3ToAlpha2 } from "../misc/utils";

const REFRESH_INTERVAL = 24 * 60 * 60;

type PlaeyrId = ID;

// TODO translate player names some day in the future

function convertRole(role: any): PlayerTeamRole | null {
  const roleTypeMap: { [key in string]: PlayerTeamRoleType } = {
    player: PlayerTeamRoleType.Player,
    on_loan: PlayerTeamRoleType.OnLoan,
    manager: PlayerTeamRoleType.Manager,
    team_captain: PlayerTeamRoleType.TeamCaptain,
    assistant_coach: PlayerTeamRoleType.AssistantCoach
  };
  const { active, start_date, end_date, jersey_number, type, team } = role;
  if (role.type === "career_ended") {
    return null;
  }
  return {
    active: active === "true",
    startDate: start_date || null,
    endDate: end_date || null,
    number: jersey_number || null,
    type: roleTypeMap[type] || type,
    team: {
      id: team.id,
      name: team.name,
      country: countryAlpha3ToAlpha2(team.country_code)
    }
  };
}

export const playerRoot: DataImporter<PlaeyrId, any, PlayerRoot | null> = {
  getUrl: id =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/players/${id}/profile.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    const next = new Date(Date.now() + REFRESH_INTERVAL * 1000).getTime();
    return new Date(next).toISOString();
  },
  getDestination(id, db) {
    return db.collection("players").doc(id);
  },
  processData(input) {
    if (!input.player || !input.roles) {
      return null;
    }

    const { player, roles } = input;
    const {
      id,
      name,
      type,
      date_of_birth,
      country_code,
      height,
      weight
    } = player;

    const output: PlayerRoot = {
      id,
      name: cleanPlayerName(name),
      position: type || null,
      birthDay: date_of_birth || null,
      country: countryAlpha3ToAlpha2(country_code) || null,
      height: height || null,
      weight: weight || null,
      roles: roles
        .map(convertRole)
        .filter((role: PlayerTeamRole | null) => !!role)
    };

    return output;
  },
  getImportResult(output) {
    return {
      player: !!output,
      roles: output && output.roles.length
    };
  },
  key: "player"
};

import { DataImporter } from "../misc/models";
import {
  MatchLineup,
  TeamFormation,
  TeamPlayer,
  LineupPosition,
  LineupPlayer
} from "../../submodules/loudstand-shared";
import { sportRadarApiKey, sportRadarApi4Prefix } from "../misc/apiKeys";
import { cleanPlayerName } from "../misc/utils";
import { playerTypeMap } from "./teamPlayers";

type MatchId = string;

const FIRST_REFRESH_BEFORE_START = -2 * 24 * 60 * 60;
const REFRESH_INTERVAL_BEFORE_START = 30 * 60;
const REFRESH_INTERVAL_AFTER_START = 5 * 60;
const LAST_REFRESH_AFTER_START = 5 * 60 * 60;

export const matchLineup: DataImporter<MatchId, any, MatchLineup | null> = {
  getUrl: (id: MatchId) =>
    `https://api.sportradar.us/soccer/${sportRadarApi4Prefix}/en/sport_events/${id}/lineups.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    const nextRefresh = Date.now() + REFRESH_INTERVAL_BEFORE_START * 1000;
    if (!input.sport_event) {
      return new Date(nextRefresh).toISOString();
    }

    const startTimestamp = new Date(input.sport_event.start_time).getTime();

    // Match has started
    if (Date.now() > startTimestamp) {
      const finalRefresh = startTimestamp + LAST_REFRESH_AFTER_START * 1000;
      if (Date.now() > finalRefresh) {
        return null;
      }
      const nextRefreshAfterStart =
        Date.now() + REFRESH_INTERVAL_AFTER_START * 1000;
      const expirationAfterStart = Math.min(
        finalRefresh,
        nextRefreshAfterStart
      );
      return new Date(expirationAfterStart).toISOString();
    }

    const firstRefresh = startTimestamp + FIRST_REFRESH_BEFORE_START;
    const expiration = Math.max(firstRefresh, nextRefresh);

    return new Date(expiration).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("matches")
      .doc(id)
      .collection("attributes")
      .doc("lineups");
  },
  processData(input) {
    if (!input.lineups.competitors) {
      return null;
    }
    const homeTeam = input.lineups.competitors.find(
      ({ qualifier }: any) => qualifier === "home"
    );
    const awayTeam = input.lineups.competitors.find(
      ({ qualifier }: any) => qualifier === "away"
    );

    return {
      home: createLineup(homeTeam),
      away: createLineup(awayTeam)
    };
  },
  getImportResult(output) {
    return !!output;
  },
  key: "lineups"
};

const mapPlayingPosition: { [key in string]: LineupPosition } = {
  central_defender: LineupPosition.CentralDefender,
  left_back: LineupPosition.LeftBack,
  striker: LineupPosition.Striker,
  central_midfielder: LineupPosition.CentralMidfielder,
  goalkeeper: LineupPosition.GoalKeeper,
  left_winger: LineupPosition.LeftWinger,
  right_back: LineupPosition.RightBack,
  right_winger: LineupPosition.RightWinger
};

const mapLineupPlayer = ({
  id,
  jersey_number: number,
  name,
  type,
  position,
  order
}: any): LineupPlayer => ({
  id,
  name: cleanPlayerName(name),
  number: number || null,
  position: playerTypeMap[type] || null,
  playingPosition: mapPlayingPosition[position] || null,
  country: null,
  birthDate: null,
  order: order || null
});

const mapTeamPlayer = ({
  id,
  jersey_number: number,
  name,
  type
}: any): TeamPlayer => ({
  id,
  name: cleanPlayerName(name),
  number: number || null,
  position: type || null,
  birthDate: null,
  country: null
});

function createLineup(teamLineup: any): TeamFormation {
  const manager =
    teamLineup.manager && teamLineup.manager.id && teamLineup.manager.name
      ? {
          id: teamLineup.manager.id,
          name: cleanPlayerName(teamLineup.manager.name)
        }
      : null;

  const { jersey, players } = teamLineup;
  const jerseyColors =
    jersey && jersey.base && jersey.sleeve && jersey.number
      ? {
          base: `#${jersey.base}`,
          sleeve: `#${jersey.sleeve}`,
          number: `#${jersey.number}`
        }
      : null;

  const starting: LineupPlayer[] = players
    .filter((player: any) => player.starter)
    .map(mapLineupPlayer);
  const substitutes: TeamPlayer[] = players
    .filter((player: any) => !player.starter)
    .map(mapTeamPlayer);

  return { starting, manager, substitutes, jerseyColors };
}

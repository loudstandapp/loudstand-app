import { DataImporter } from "../misc/models";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import { firestore } from "firebase-admin";
import {
  ISO639a2Language,
  TournamentRoot,
  LocalizedTeams
} from "../../submodules/loudstand-shared";

const EMPTY_EXPIRATION_TIME = 30 * 60;

export const localizedTeams: DataImporter<
  ISO639a2Language,
  any[],
  LocalizedTeams | null
> = {
  getUrl: async (languageCode: ISO639a2Language) => {
    const db = firestore();
    const ref = db.collection("tournaments");
    const snaps = await ref.get();
    const urls: string[] = [];
    snaps.forEach(snap => {
      const tournament: TournamentRoot = snap.data().body;
      const { id } = tournament;
      urls.push(
        `https://api.sportradar.us/${sportRadarApi3Prefix}/global/${languageCode}/tournaments/${id}/info.json?api_key=${sportRadarApiKey}`
      );
    });
    return urls;
  },
  getExpiration(input) {
    const now = new Date().toISOString();
    const endDates = input
      .map(tournament => tournament.season.end_date)
      .filter(date => date > now)
      .sort();
    if (!endDates.length) {
      return new Date(Date.now() + EMPTY_EXPIRATION_TIME * 1000).toISOString();
    }
    return endDates[0];
  },
  getDestination(id, db) {
    return db
      .collection("localizations")
      .doc("teams")
      .collection("languages")
      .doc(id);
  },
  processData(input) {
    const output = input.reduce((tournamentAcc, item) => {
      const teamsInTournament = item.groups.reduce(
        (groupAcc: LocalizedTeams, group: any) => {
          const teamsInGroup = group.teams.reduce(
            (teamAcc: LocalizedTeams, team: any) => ({
              ...teamAcc,
              [team.id]: team.name
            }),
            {}
          );
          return { ...groupAcc, ...teamsInGroup };
        },
        {}
      );
      return { ...tournamentAcc, ...teamsInTournament };
    }, {});

    return output;
  },
  getImportResult(output) {
    return output ? Object.keys(output).length : false;
  },
  key: "localizedTeams"
};

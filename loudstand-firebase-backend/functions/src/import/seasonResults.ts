import { DataImporterWithUpdate } from "../misc/models";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import {
  MatchResultList,
  MatchResultItem,
  TournamentRoot
} from "../../submodules/loudstand-shared";
import { matchStateMap } from "./matchRoot";
import { getDocumentBody } from "../misc/utils";
import { firestore } from "firebase-admin";
type SeasonId = string;

const REFRESH_INTERVAL = 30 * 60;

function convertResult({
  status,
  home_score,
  away_score
}: any): MatchResultItem {
  return {
    state: matchStateMap[status] || null,
    score:
      home_score !== undefined && away_score !== undefined
        ? {
            home: home_score,
            away: away_score
          }
        : null
  };
}

export const seasonResults: DataImporterWithUpdate<
  SeasonId,
  any,
  MatchResultList,
  any
> = {
  getUrl: (id: SeasonId) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/tournaments/${id}/results.json?api_key=${sportRadarApiKey}`,
  getExpiration(input: any) {
    const nextRefresh = Date.now() + REFRESH_INTERVAL * 1000;
    return new Date(nextRefresh).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("seasons")
      .doc(id)
      .collection("attributes")
      .doc("results");
  },
  processData(input) {
    const { results } = input;
    if (!results) {
      return null;
    }
    const output = results.reduce(
      (acc: MatchResultList, item: any) => ({
        ...acc,
        [item.sport_event.id]: convertResult(item.sport_event_status)
      }),
      {}
    );
    return output;
  },
  getImportResult(output) {
    return Object.keys(output).length;
  },
  getIdFromUpdate: async update => {
    if (update.metadata.season_id) {
      return update.metadata.season_id;
    }
    if (!update.metadata.tournament_id) {
      throw new Error("No tournament ID in received event");
    }
    const ref = firestore()
      .collection("tournaments")
      .doc(update.metadata.tournament_id);
    const tournament = await getDocumentBody<TournamentRoot>(ref);
    if (!tournament) {
      throw new Error(
        `Cannot load tournament with id ${update.metadata.tournament_id}`
      );
    }

    return tournament.currentSeason.id;
  },
  mergeUpdate(current, update) {
    const stateEvents = ["match_started", "match_ended"];
    if (
      !update.payload ||
      !update.payload.sport_event_status ||
      !update.payload.event ||
      !update.metadata.sport_event_id
    ) {
      return null;
    }
    const matchId = update.metadata.sport_event_id;

    const score = {
      home: update.payload.sport_event_status.home_score,
      away: update.payload.sport_event_status.away_score
    };

    // Update the overall state of the match
    if (stateEvents.includes(update.payload.event.type)) {
      const state = matchStateMap[update.payload.sport_event_status.status];
      return {
        ...current,
        [matchId]: {
          score,
          state
        }
      };
    }
    if (update.payload.event.type === "score_change") {
      const result: MatchResultList = {
        ...(current || {}),
        [matchId]: {
          ...((current && current[matchId]) || {}),
          score
        }
      };
      return result;
    }
    return null;
  },
  key: "seasonResults"
};

import { DataImporter } from "../misc/models";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import { CalendarItemByDay } from "../../submodules/loudstand-shared";
import { cleanDate, countryAlpha3ToAlpha2 } from "../misc/utils";
import { MatchListItem } from "../../submodules/loudstand-shared/models/general";
import { matchReactionStartTime, matchReactionEndTime } from "./matchRoot";

type SeasonId = string;

export function convertMatch(rawEvent: any): MatchListItem {
  const { id, scheduled, season, tournament, competitors, venue } = rawEvent;
  const awayTeam = competitors.find((c: any) => c.qualifier === "away");
  const homeTeam = competitors.find((c: any) => c.qualifier === "home");
  return {
    id,
    startTime: scheduled,
    season: { id: season.id, name: season.name },
    tournament: { id: tournament.id, name: tournament.name },
    teams: {
      home: {
        id: homeTeam.id,
        name: homeTeam.name,
        country: countryAlpha3ToAlpha2(homeTeam.country_code)
      },
      away: {
        id: awayTeam.id,
        name: awayTeam.name,
        country: countryAlpha3ToAlpha2(awayTeam.country_code)
      }
    },
    venue: venue
      ? {
          id: venue.id,
          name: venue.name
        }
      : null,
    date: cleanDate(scheduled),
    reactionStartTime: scheduled ? matchReactionStartTime(scheduled) : null,
    reactionEndTime: scheduled ? matchReactionEndTime(scheduled) : null
  };
}

const EXPIRATION_TIME = 30 * 60;

export const seasonMatches: DataImporter<
  SeasonId,
  any,
  CalendarItemByDay<MatchListItem> | null
> = {
  disableMerge: true,
  getUrl: (id: SeasonId) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/tournaments/${id}/schedule.json?api_key=${sportRadarApiKey}`,
  getExpiration(input) {
    if (!input || !input.sport_events) {
      return new Date(Date.now() + EXPIRATION_TIME * 1000).toISOString();
    }
    const now = new Date().toISOString();
    if (input.sport_events[0] && now > input.sport_events[0].season.end_date) {
      return null;
    }
    return new Date(Date.now() + EXPIRATION_TIME * 1000).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("seasons")
      .doc(id)
      .collection("attributes")
      .doc("matchCalendar");
  },
  getImportResult(output) {
    return output ? Object.keys(output).length : null;
  },
  processData(input: any) {
    if (!input.sport_events) {
      return null;
    }
    const matches = input.sport_events.map(convertMatch);
    const calendarMap = matches.reduce(
      (result: CalendarItemByDay<MatchListItem>, match: MatchListItem) => {
        const { date } = match;
        return {
          ...result,
          [date]: { ...result[date], [match.id]: match }
        };
      },
      {}
    );
    return calendarMap;
  },
  key: "seasonMatches"
};

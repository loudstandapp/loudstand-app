import { DataImporter } from "../misc/models";
import { sportRadarApiKey, sportRadarApi3Prefix } from "../misc/apiKeys";
import {
  SeasonStandings,
  StandingItem,
  ID
} from "../../submodules/loudstand-shared";

type SeasonId = string;

const EXPIRATION_TIME = 30 * 60;

export const seasonStandings: DataImporter<
  SeasonId,
  any,
  SeasonStandings | null
> = {
  getUrl: (id: SeasonId) =>
    `https://api.sportradar.us/${sportRadarApi3Prefix}/global/en/tournaments/${id}/standings.json?api_key=${sportRadarApiKey}`,
  getExpiration(input) {
    const now = new Date().toISOString();
    if (input.season && now > input.season.end_date) {
      return null;
    }
    return new Date(Date.now() + EXPIRATION_TIME * 1000).toISOString();
  },
  getDestination(id, db) {
    return db
      .collection("seasons")
      .doc(id)
      .collection("attributes")
      .doc("standings");
  },
  processData(input) {
    if (!input.standings) {
      return null;
    }

    const totalStandings = input.standings.find(
      (standing: any) => standing.type === "total"
    );

    const standingGroups = totalStandings.groups.reduce(
      (groupsAcc: SeasonStandings, group: any) => {
        const id = group.id;
        const name = group.name || null;
        const standings = group.team_standings.reduce(
          (acc: { [key in ID]: StandingItem }, item: any) => {
            const teamStanding = {
              position: item.rank,
              team: item.team,
              playedMatches: item.played,
              points: item.points,
              change: item.change,
              goalDifference: {
                for: item.goals_for,
                against: item.goals_against
              }
            };
            return {
              ...acc,
              [item.team.id]: teamStanding
            };
          },
          {}
        );
        return {
          ...groupsAcc,
          [id]: { id, name, standings }
        };
      },
      {}
    );
    return standingGroups;
  },
  getImportResult(output) {
    return output ? Object.keys(output).length : null;
  },
  key: "seasonStandings"
};

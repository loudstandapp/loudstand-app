import { firestore } from "firebase-admin";
import * as express from "express";
import * as cors from "cors";
import {
  ISO8601Date,
  Question,
  ID,
  RawQuestion
} from "../../submodules/loudstand-shared";
import {
  getDocument,
  splitIntoChunks,
  validateContentAuth
} from "../misc/utils";
import { isObject } from "util";

async function getTeamQuestions(id: ID, res: express.Response) {
  try {
    const db = firestore();
    const questionsRef = db.doc(`teams/${id}/attributes/questions`);
    const questions:
      | { [key in ISO8601Date]: Question }
      | null = await getDocument(questionsRef);
    if (!questions) {
      res.status(404).send("Team questions not in database.");
    }
    res.send(questions);
  } catch (e) {
    console.error(e.message);
    res.status(500).send(e);
  }
}

async function getPlayerQuestions(id: ID, res: express.Response) {
  try {
    const db = firestore();
    const questionsRef = db.doc(`players/${id}/attributes/questions`);
    const questions:
      | { [key in ISO8601Date]: Question }
      | null = await getDocument(questionsRef);
    if (!questions) {
      res.status(404).send("Player questions not in database.");
    }
    res.send(questions);
  } catch (e) {
    console.error(e.message);
    res.status(500).send(e);
  }
}

function validateQuestion(rawQuestion: RawQuestion) {
  if (!isObject(rawQuestion)) {
    throw new Error("Invalid object");
  }
  const { question, teamId, playerId, date, imageUrl } = rawQuestion;
  if (!teamId) {
    throw new Error("Team ID missing");
  }
  if (typeof teamId === "string" && !teamId.startsWith("sr:competitor:")) {
    throw new Error("Invalid team ID");
  }
  if (
    playerId &&
    (typeof playerId !== "string" || !playerId.startsWith("sr:player:"))
  ) {
    throw new Error("Invalid player ID");
  }
  if (imageUrl && typeof imageUrl !== "string") {
    throw new Error("Wrong type for imageUrl");
  }
  if (!date) {
    throw new Error("Date is missing");
  }
  if (!date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
    throw new Error("Wrong format for date. use YYYY-MM-DD");
  }
  if (!isObject(question) || Object.values(question).length === 0) {
    throw new Error("Wrong question format");
  }
  for (const languageCode in question) {
    if (typeof question[languageCode] !== "string") {
      throw new Error(
        `Wrong data type for question with language ${languageCode}`
      );
    }
  }
}

const questionRef = (db: firestore.Firestore, item: RawQuestion) =>
  db.collection("questions").doc(`${item.date}---${item.teamId}`);

async function saveQuestions(req: express.Request, res: express.Response) {
  try {
    const db = firestore();

    if (!Array.isArray(req.body)) {
      const rawQuestion = req.body;
      try {
        validateQuestion(rawQuestion);
      } catch (e) {
        res.status(400).send({ message: e.message, item: rawQuestion });
        return;
      }
      const ref = questionRef(db, rawQuestion);
      await ref.set(rawQuestion);
      res.status(200).send();
      return;
    }

    const rawQuestions: RawQuestion[] = req.body;
    for (const item of rawQuestions) {
      try {
        validateQuestion(item);
      } catch (e) {
        res.status(400).send({ message: e.message, item });
        return;
      }
    }
    const chunks = splitIntoChunks(rawQuestions, 500);
    for (const chunk of chunks) {
      const batch = db.batch();
      for (const item of chunk) {
        const ref = questionRef(db, item);
        batch.set(ref, item);
      }
      await batch.commit();
    }
    res.status(200).send();
    return;
  } catch (e) {
    console.error(e.message);
    res.status(500).send(e.message);
  }
}

const app = express();
app.use(cors({ origin: true }));
app.use(validateContentAuth);

app.get("/teams/:id", (req: express.Request, res: express.Response) =>
  getTeamQuestions(req.params.id, res)
);

app.get("/players/:id", (req: express.Request, res: express.Response) =>
  getPlayerQuestions(req.params.id, res)
);

app.post("/", (req: express.Request, res: express.Response) =>
  saveQuestions(req, res)
);

export const questionsService = app;

import { firestore } from "firebase-admin";
import * as express from "express";
import * as cors from "cors";
import {
  ID,
  CalendarItemType,
  RawCalendarItem
} from "../../submodules/loudstand-shared";
import { splitIntoChunks, validateContentAuth } from "../misc/utils";
import { isObject } from "util";

const COLLECTION_NAME = "calendarItems";

async function getTeamCalendarItems(id: ID, res: express.Response) {
  try {
    const db = firestore();
    const itemsQueryResult = await db
      .collection(COLLECTION_NAME)
      .where("teamId", "==", id)
      .get();
    const docsData = itemsQueryResult.docs.map(doc => ({
      id: doc.id,
      ...doc.data()
    }));
    res.send(docsData);
  } catch (e) {
    console.error(e.message);
    res.status(500).send(e);
  }
}

function validateItem(item: RawCalendarItem) {
  const { type, location, timestamp, date, subject, teamId } = item;
  if (!Object.values(CalendarItemType).includes(type)) {
    throw new Error("Invalid type");
  }
  if (!teamId) {
    throw new Error("Team ID missing");
  }
  if (typeof teamId === "string" && !teamId.startsWith("sr:competitor:")) {
    throw new Error("Invalid team ID");
  }

  if (location && typeof location !== "string") {
    throw new Error("Wrong type for location");
  }
  if (!date) {
    throw new Error("Date is missing");
  }
  if (!date.match(/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/)) {
    throw new Error("Wrong format for date. use YYYY-MM-DD");
  }
  if (timestamp && !Date.parse(timestamp)) {
    throw new Error(
      "Wrong format for timestamp. use YYYY-MM-DDThh:mm:ssZZZZ (ISO8601)"
    );
  }
  if (subject && (!isObject(subject) || Object.values(subject).length === 0)) {
    throw new Error("Wrong subject format");
  }
  for (const languageCode in subject) {
    if (typeof subject[languageCode] !== "string") {
      throw new Error(`Wrong data type for item with language ${languageCode}`);
    }
  }
}

async function saveItems(req: express.Request, res: express.Response) {
  try {
    const db = firestore();
    const itemsRef = db.collection(COLLECTION_NAME);

    if (!Array.isArray(req.body)) {
      const rawItem = req.body;
      try {
        validateItem(rawItem);
      } catch (e) {
        res.status(400).send({ message: e.message, item: rawItem });
        return;
      }
      await itemsRef.add(rawItem);
      res.status(200).send();
      return;
    }

    const rawItems: RawCalendarItem[] = req.body;
    for (const item of rawItems) {
      try {
        validateItem(item);
      } catch (e) {
        res.status(400).send({ message: e.message, item });
        return;
      }
    }
    const chunks = splitIntoChunks(rawItems, 500);
    for (const chunk of chunks) {
      const batch = db.batch();
      for (const item of chunk) {
        const ref = itemsRef.doc();
        batch.set(ref, item);
      }
      await batch.commit();
    }
    res.status(200).send();
    return;
  } catch (e) {
    console.error(e.message);
    res.status(500).send(e.message);
  }
}

async function deleteItems(ids: string, res: express.Response) {
  const idList = ids.split(",");
  try {
    const db = firestore();
    const itemsRef = db.collection(COLLECTION_NAME);

    const chunks = splitIntoChunks(idList, 500);
    for (const chunk of chunks) {
      const batch = db.batch();
      for (const id of chunk) {
        const ref = itemsRef.doc(id);
        batch.delete(ref);
      }
      await batch.commit();
    }
    res.status(200).send();
    return;
  } catch (e) {
    console.error(e.message);
    res.status(500).send(e.message);
  }
}

const app = express();
app.use(cors({ origin: true }));
app.use(validateContentAuth);

app.get("/teams/:id", (req: express.Request, res: express.Response) =>
  getTeamCalendarItems(req.params.id, res)
);

app.delete("/:ids", (req: express.Request, res: express.Response) =>
  deleteItems(req.params.ids, res)
);
app.post("/", (req: express.Request, res: express.Response) =>
  saveItems(req, res)
);

export const calenderItemsService = app;

import { Change } from "firebase-functions";
import { DocumentSnapshot } from "@google-cloud/firestore";
import {
  ImportedDocument,
  MatchRoot,
  MatchState,
  ISO639a2Language,
  TeamListItem,
  NotificationType
  // MatchEventType,
  // MatchStatusType
} from "../../submodules/loudstand-shared";
import {
  translate,
  translateAllLanguages,
  availableLanguages
} from "../misc/translation";
import { serializeTopicName, sendNotificationToTopics } from "./notifications";
import { localizeTeams } from "../misc/utils";
import { scheduleCustomJob } from "../misc/customJob";
import { matchOpenForVotingScheduledTask } from "./matchScheduledTask";

const SCHEDULE_OPEN_VOTING_THRESHOLD = 4 * 24 * 60 * 60;

async function getDefaultMatchTitle(
  language: ISO639a2Language,
  teams: { home: TeamListItem; away: TeamListItem }
) {
  // Generate generic title if not specified
  const [homeTeamName, awayTeamName] = await localizeTeams(language, [
    teams.home,
    teams.away
  ]);
  return translate(language, "notifications.matchTitle", {
    homeTeamName,
    awayTeamName
  });
}

async function getMatchTitlesWithScores(
  teams: { home: TeamListItem; away: TeamListItem },
  scores: { home: number; away: number }
) {
  const titles: { [key in ISO639a2Language]: string } = {};
  for (const language of availableLanguages) {
    const [homeTeamName, awayTeamName] = await localizeTeams(language, [
      teams.home,
      teams.away
    ]);
    titles[language] = translate(
      language,
      "notifications.matchTitleWithScore",
      {
        homeTeamName,
        awayTeamName,
        homeScore: scores.home,
        awayScore: scores.away
      }
    );
  }
  return titles;
}

export async function sendMatchNotification({
  type,
  match,
  bodies,
  titles,
  exclusiveQualifier
}: {
  type: NotificationType;
  match: MatchRoot;
  bodies: { [key in ISO639a2Language]: string };
  titles?: { [key in ISO639a2Language]: string };
  exclusiveQualifier?: "home" | "away";
}) {
  const { teams, id } = match;
  if (!teams) {
    console.warn("sendMatchNotification: Missing teams in matchRoot", id);
    return;
  }
  const results: { [key in ISO639a2Language]: any } = {};
  for (const language of Object.keys(bodies)) {
    const topics = exclusiveQualifier
      ? [
          serializeTopicName({
            language,
            teamId: teams[exclusiveQualifier].id,
            type
          })
        ]
      : [
          serializeTopicName({ language, teamId: teams.home.id, type }),
          serializeTopicName({ language, teamId: teams.away.id, type })
        ];

    const actualTitle = titles
      ? titles[language]
      : await getDefaultMatchTitle(language, teams);

    const notification = {
      body: bodies[language],
      title: actualTitle,
      tag: id
    };
    console.log("notification", notification);
    const result = await sendNotificationToTopics(topics, notification);
    console.log("result", result);
    results[language] = result;
  }
  return results;
}

export const handleMatchChange = {
  onWrite: async (change: Change<DocumentSnapshot>) => {
    const prev = change.before.data() as
      | ImportedDocument<MatchRoot>
      | undefined;
    const current = change.after.data() as
      | ImportedDocument<MatchRoot>
      | undefined;

    if (!current || !current.body || !current.body.teams) {
      return;
    }
    const matchRoot = current.body;
    const prevState = prev && prev.body && prev.body.state;
    const currentState = matchRoot.state;

    if (currentState === MatchState.PreMatch) {
      if (
        // Do not schedule for empty matches
        !matchRoot.reactionStartTime ||
        // Do not schedule for past matches
        new Date(matchRoot.reactionStartTime).getTime() < Date.now() ||
        // Do not schedule for matches too far in future
        new Date(matchRoot.reactionStartTime).getTime() >
          Date.now() + 1000 * SCHEDULE_OPEN_VOTING_THRESHOLD
      ) {
        console.log({ matchRoot });
        return;
      }
      await scheduleCustomJob(matchRoot.id, matchOpenForVotingScheduledTask);
      return;
    }

    const teams = matchRoot.teams;
    if (!teams) {
      console.warn(matchRoot.id, "No teams defined for match yet");
      return;
    }

    if (prevState !== currentState) {
      switch (currentState) {
        case MatchState.Live: {
          const bodies = translateAllLanguages("notifications.matchStarted");
          await sendMatchNotification({
            type: NotificationType.MatchStart,
            match: matchRoot,
            bodies
          });
          break;
        }
        case MatchState.PostMatch: {
          const bodies = translateAllLanguages("notifications.matchEnded");
          const titles =
            matchRoot.teams && matchRoot.score
              ? await getMatchTitlesWithScores(matchRoot.teams, matchRoot.score)
              : undefined;
          await sendMatchNotification({
            titles,
            type: NotificationType.MatchEnd,
            match: matchRoot,
            bodies
          });
          break;
        }
      }
    }
    // const prevStatus =
    //   prev &&
    //   prev.body &&
    //   prev.body.matchStatus &&
    //   prev.body.matchStatus.current;
    // const currentStatus =
    //   current &&
    //   current.body &&
    //   current.body.matchStatus &&
    //   current.body.matchStatus.current;
    // if (prevStatus !== currentStatus) {
    //   switch (currentStatus) {
    //     case MatchStatusType: {
    //     }
    //     case MatchState.PostMatch: {
    //     }
    //     case MatchState.PreMatch: {
    //     }
    //   }
    // }
  },
  path: "matches/{matchId}"
};

import { CustomJobExecutor } from "../misc/customJob";
import {
  MatchRoot,
  NotificationType,
  ID
} from "../../submodules/loudstand-shared";
import { CustomJobType } from "../misc/models";
import { firestore } from "firebase-admin";
import { translateAllLanguages } from "../misc/translation";
import { sendMatchNotification } from "./matchChangeHandler";
import { getDocumentBody } from "../misc/utils";

type MatchOpenForVotingNotificationResult = any;

export const matchOpenForVotingScheduledTask: CustomJobExecutor<
  ID,
  MatchOpenForVotingNotificationResult
> = {
  type: CustomJobType.MatchOpenForVotingNotification,
  execute: async matchId => {
    const db = firestore();
    const matchRoot = await getDocumentBody<MatchRoot>(
      db.doc(`matches/${matchId}`)
    );
    if (!matchRoot) {
      throw new Error(
        `${CustomJobType.MatchOpenForVotingNotification}: execute: matches/${matchId} not found`
      );
    }
    const bodies = translateAllLanguages("notifications.matchOpenForVoting");
    return await sendMatchNotification({
      type: NotificationType.MatchOpenForVoting,
      match: matchRoot,
      bodies
    });
  },
  nextScheduledTask: async matchId => {
    const db = firestore();
    const matchRoot = await getDocumentBody<MatchRoot>(
      db.doc(`matches/${matchId}`)
    );
    if (!matchRoot || !matchRoot.reactionStartTime) {
      throw new Error(
        `${CustomJobType.MatchOpenForVotingNotification}: nextScheduledTask: matches/${matchId} or reactionStartTime not found`
      );
    }

    return firestore.Timestamp.fromMillis(
      new Date(matchRoot.reactionStartTime).getTime()
    );
  }
};

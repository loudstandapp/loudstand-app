import {
  ID,
  FavoriteTeams,
  UserNotificationSettings,
  UserProfile,
  UserDevice,
  NotificationType
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { getDocument } from "../misc/utils";
import { getAllFavoriteTeamIds } from "../favorites/handleFavoriteTeams";
import {
  serializeTopicName,
  unsubscribeFromTopics,
  subscribeToTopics,
  getSubscribedTopics
} from "./notifications";
import { Change, EventContext } from "firebase-functions";
import { DocumentSnapshot } from "@google-cloud/firestore";

const defaultSettings: { [key in NotificationType]: boolean } = {
  [NotificationType.MatchEnd]: true,
  [NotificationType.MatchOpenForVoting]: true,
  [NotificationType.MatchStart]: true,
  [NotificationType.QuestionAvailable]: true,
  [NotificationType.MatchBreakResult]: false,
  [NotificationType.MatchGoal]: false,
  [NotificationType.MatchSubstitution]: false
};

export async function generateNotificationSettingsDocument(userId: ID) {
  try {
    const db = firestore();
    const favoriteTeams = await getDocument<FavoriteTeams>(
      db
        .collection("users")
        .doc(userId)
        .collection("attributes")
        .doc("favoriteTeams")
    );

    const notificationSettingsDocRef = db
      .collection("users")
      .doc(userId)
      .collection("attributes")
      .doc("notificationSettings");
    const currentSettingsDocument = await getDocument<UserNotificationSettings>(
      notificationSettingsDocRef
    );

    const userProfile = await getDocument<UserProfile>(
      db.collection("users").doc(userId)
    );

    const favoriteTeamIds = favoriteTeams
      ? getAllFavoriteTeamIds(favoriteTeams)
      : [];

    const activeDevices =
      userProfile && userProfile.devices
        ? Object.keys(userProfile.devices).reduce((acc, key) => {
            // Filter away inactive devices
            if (userProfile.devices[key].isActive) {
              return { ...acc, [key]: userProfile.devices[key] };
            }
            return acc;
          }, {})
        : {};

    const newDocument: UserNotificationSettings = {
      favoriteTeamIds,
      updated: new Date().toISOString(),
      types: {
        ...defaultSettings,
        ...(currentSettingsDocument ? currentSettingsDocument.types : {})
      },
      devices: {
        ...activeDevices,
        ...(currentSettingsDocument ? currentSettingsDocument.devices : {})
      }
    };
    await notificationSettingsDocRef.set(newDocument);
  } catch (error) {
    console.error(error);
  }
}

export async function updateNotificationSettingsDevices(
  userId: ID,
  devices: { [key in ID]: UserDevice }
) {
  console.log("updateNotificationSettingsDevices", userId, devices);
  const activeDevices = Object.keys(devices).reduce((acc, key) => {
    // Filter away inactive devices
    if (devices[key].isActive) {
      return { ...acc, [key]: devices[key] };
    }
    return acc;
  }, {});

  try {
    const db = firestore();
    const docRef = db
      .collection("users")
      .doc(userId)
      .collection("attributes")
      .doc("notificationSettings");
    await docRef.update({
      updated: new Date().toISOString(),
      devices: activeDevices
    });
  } catch (error) {
    console.error(error);
    console.log("Attempting to generateNotificationSettingsDocument()", userId);
    await generateNotificationSettingsDocument(userId);
  }
}

export async function updateNotificationSettingsFavoriteTeams(
  userId: ID,
  favoriteTeamIds: ID[]
) {
  try {
    const db = firestore();
    const docRef = db
      .collection("users")
      .doc(userId)
      .collection("attributes")
      .doc("notificationSettings");
    await docRef.update({
      updated: new Date().toISOString(),
      favoriteTeamIds
    });
  } catch (error) {
    console.error(error);
  }
}

export const FALLBACK_LANGUAGE = "en";

type TopicsByDevice = { [DeviceToken in ID]: ID[] };
export function topicsFromNotificationSettings(
  settings: UserNotificationSettings
): TopicsByDevice {
  const { favoriteTeamIds, types, devices } = settings;
  const enabledTypeList = (Object.keys(types) as NotificationType[]).filter(
    type => types[type]
  );

  const topicsForDevices: TopicsByDevice = {};
  for (const device of Object.values(devices)) {
    if (!device.messageToken) {
      continue;
    }
    const topics = [];
    const language = device.activeLanguage || FALLBACK_LANGUAGE;
    for (const teamId of favoriteTeamIds) {
      for (const type of enabledTypeList) {
        topics.push(serializeTopicName({ language, teamId, type }));
      }
    }
    topicsForDevices[device.messageToken] = topics;
  }

  return topicsForDevices;
}

export const handleSettingsChange = {
  onWrite: async (change: Change<DocumentSnapshot>, context: EventContext) => {
    const prev = change.before.data() as UserNotificationSettings | undefined;
    const current = change.after.data() as UserNotificationSettings | undefined;

    const prevDeviceTokens = prev
      ? (Object.values(prev.devices).map(device => device.messageToken) as ID[])
      : [];

    const currentDeviceTokens = current
      ? (Object.values(current.devices).map(
          device => device.messageToken
        ) as ID[])
      : [];

    const removedDeviceTokens = prevDeviceTokens.filter(
      token => !currentDeviceTokens.includes(token)
    );

    for (const deviceToken of removedDeviceTokens) {
      const subscribedTopics = await getSubscribedTopics(deviceToken);
      if (subscribedTopics === null) {
        continue;
      }
      await unsubscribeFromTopics(deviceToken, subscribedTopics);
    }

    if (!current) {
      console.warn(
        "No current notification settings for ",
        context.params.userId
      );
      return;
    }

    const topicsByDeviceToken = topicsFromNotificationSettings(current);
    for (const deviceToken of Object.keys(topicsByDeviceToken)) {
      // get all topics form device token
      const currentTopics = await getSubscribedTopics(deviceToken);
      if (currentTopics === null) {
        continue;
      }
      const newTopics = topicsByDeviceToken[deviceToken];

      const topicsToAdd = newTopics.filter(
        topic => !currentTopics.includes(topic)
      );
      const topicsToRemove = currentTopics.filter(
        topic => !newTopics.includes(topic)
      );
      await unsubscribeFromTopics(deviceToken, topicsToRemove);
      await subscribeToTopics(deviceToken, topicsToAdd);
    }
  },
  path: "users/{userId}/attributes/notificationSettings"
};

export async function updateNotificationSettingsForAllUsers() {
  const db = firestore();
  const snap = await db.collection("users").get();
  const promises = snap.docs.map(doc =>
    generateNotificationSettingsDocument(doc.id)
  );
  await Promise.all(promises);
}

/* cases:
  updating existing user profiles V
    if no document:
      create a basic settings doc, and copy tokens + favorites etc
       generateNotificationSettingsDocument()

  creating a new user V:
    trigger onCreate,
      create a basic settings doc, copy tokens if available
      generateNotificationSettingsDocument()

  mass update V 
    select all users
    for each user
      get favorites, get devices, get default settings, merge into settings document
      generateNotificationSettingsDocument()
*/

// IMPORTANT!!! detect whether a token is expired when querying for topics 404 means expired

import { messaging } from "firebase-admin";
import { ID } from "../../submodules/loudstand-shared";
import { cloudMessagingServerKey } from "../misc/apiKeys";
import axios from "axios";
import { validateStatus } from "../misc/importAction";

export function serializeTopicName(params: { [key: string]: string }): string {
  const separator = ".";
  const equals = "~";
  const replacement = "-";
  const invalidRegex = /[^a-zA-Z0-9_.~%]/g;

  return Object.keys(params)
    .sort()
    .map(key => {
      const cleanKey = key.replace(invalidRegex, replacement);
      const cleanValue = params[key].replace(invalidRegex, replacement);
      return `${cleanKey}${equals}${cleanValue}`;
    })
    .join(separator);
}

interface NotificationMessage {
  body: string;
  title: string;
  tag?: string;
  data?: { [key: string]: string };
}

export async function sendNotificationToTopics(
  topics: string[],
  { body, title, tag }: NotificationMessage
) {
  const firebaseMessaging = messaging();
  const condition = topics.map(topic => `'${topic}' in topics`).join(" || ");
  console.log("condition", condition);
  try {
    const result = await firebaseMessaging.sendToCondition(
      condition,
      {
        notification: {
          title,
          body,
          sound: "default",
          tag
        }
      },
      {
        collapseKey: tag,
        priority: "high"
      }
    );
    return result;
  } catch (error) {
    console.error("Cannot send message", error);
  }
  return null;
}

export async function subscribeToTopics(token: ID, topics: ID[]) {
  console.log("subscribeToTopics", token, topics);
  const firebaseMessaging = messaging();
  for (const topic of topics) {
    // TODO: Determine if we want to catch errors here, or handle the entire (queued) operation based on the write to settings
    try {
      await firebaseMessaging.subscribeToTopic(token, topic);
    } catch (error) {
      console.error("subscribeToTopics", token, topics, error);
    }
  }
}

export async function unsubscribeFromTopics(token: ID, topics: ID[]) {
  console.log("unsubscribeFromTopics", token, topics);
  const firebaseMessaging = messaging();
  for (const topic of topics) {
    try {
      await firebaseMessaging.unsubscribeFromTopic(token, topic);
    } catch (error) {
      console.error("subscribeToTopics", token, topics, error);
    }
  }
}

export async function getSubscribedTopics(token: ID): Promise<ID[] | null> {
  const url = `https://iid.googleapis.com/iid/info/${token}?details=true`;
  const headers = {
    Authorization: `key=${cloudMessagingServerKey}`
  };
  const response = await axios.get(url, { headers, validateStatus });
  if (response.status === 404) {
    return null;
  }
  const json = response.data;
  const topicsObject = (json.rel && json.rel.topics) || {};
  return Object.keys(topicsObject);
}

import { serializeTopicName } from "./notifications";

describe("serializeTopicName", () => {
  it("formats as expected", () => {
    expect(
      serializeTopicName({
        team: "sr:competitor:2003",
        language: "en",
        type: "MATCH_END"
      })
    ).toEqual("language~en.team~sr-competitor-2003.type~MATCH_END");
  });
});

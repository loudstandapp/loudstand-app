import {
  Question,
  ISO8601Date,
  ID,
  RawQuestion,
  NotificationType,
  ISO639a2Language
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { cleanDate, getDocument, localizeTeams } from "../misc/utils";
import { scheduleCustomJob, CustomJobExecutor } from "../misc/customJob";
import { CustomJobType } from "../misc/models";
import { availableLanguages, translate } from "../misc/translation";
import { serializeTopicName, sendNotificationToTopics } from "./notifications";
import { FALLBACK_LANGUAGE } from "./settingsHandler";

function cleanMarkup(input: string) {
  var regex = /\{([^\}]*)\}/g;
  return input.replace(regex, "$1");
}

export async function sendQuestionNotification(rawQuestion: RawQuestion) {
  const { date, teamId, question } = rawQuestion;
  console.log("sendQuestionNotification", rawQuestion);
  const results: { [key in ISO639a2Language]: any } = {};
  for (const language of availableLanguages) {
    const [teamName] = await localizeTeams(language, [
      { id: teamId, name: "N/A", country: null }
    ]);
    if (!teamName) {
      throw new Error(
        `sendQuestionNotification: cannot find localized team name ${teamId}`
      );
    }
    const title = translate(language, "notifications.todaysQuestion", {
      teamName
    });
    const body = question[language] || question[FALLBACK_LANGUAGE];
    if (!body) {
      throw new Error(
        `sendQuestionNotification: cannot get body for ${teamId} ${date}`
      );
    }
    const topic = serializeTopicName({
      teamId,
      language,
      type: NotificationType.QuestionAvailable
    });
    results[language] = await sendNotificationToTopics([topic], {
      body: cleanMarkup(body),
      title,
      tag: teamId
    });
  }
  return results;
}

type Arg = { teamId: ID; date: ISO8601Date };

async function getPopulatedQuestion(
  teamId: ID,
  date: ISO8601Date
): Promise<Question> {
  const db = firestore();
  const docRef = db
    .collection("teams")
    .doc(teamId)
    .collection("attributes")
    .doc("questions");
  const questionCalendar = await getDocument<
    { [key in ISO8601Date]: Question }
  >(docRef);

  if (!questionCalendar) {
    throw new Error(
      `getPopulatedQuestion: teams/${teamId}/attributes/questions not found`
    );
  }
  const question = questionCalendar[date];
  if (!question) {
    throw new Error(
      `getPopulatedQuestion: teams/${teamId}/attributes/questions does not have a question for date ${date}`
    );
  }
  return question;
}

export const questionAvailableScheduledTask: CustomJobExecutor<Arg, any> = {
  execute: async ({ teamId, date }) => {
    const question = await getPopulatedQuestion(teamId, date);
    return await sendQuestionNotification(question);
  },
  type: CustomJobType.QuestionAvailableNotification,
  nextScheduledTask: async ({ teamId, date }) => {
    const question = await getPopulatedQuestion(teamId, date);
    return firestore.Timestamp.fromMillis(
      new Date(question.publishTime).getTime()
    );
  }
};

async function getQuestionsOfDay(date: ISO8601Date): Promise<RawQuestion[]> {
  const db = firestore();
  const query = db.collection("questions").where("date", "==", date);
  const result = await query.get();
  const questions = result.docs.map(doc => doc.data()) as RawQuestion[];
  return questions;
}

export async function scheduleTodaysQuestions() {
  const today = cleanDate(new Date().toISOString());
  const questions = await getQuestionsOfDay(today);
  const promises = questions.map(({ teamId, date }) =>
    scheduleCustomJob({ teamId, date }, questionAvailableScheduledTask)
  );
  return await Promise.all(promises);
}

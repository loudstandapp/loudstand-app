import {
  ID,
  MatchScore,
  PlayerBasic,
  NotificationType,
  MatchRoot
} from "../../submodules/loudstand-shared";
import { cleanPlayerName, getDocumentBody } from "../misc/utils";
import { firestore } from "firebase-admin";
import { sendMatchNotification } from "./matchChangeHandler";
import { translateAllLanguages } from "../misc/translation";

interface DecodedEvent {
  notificationType: NotificationType;
  matchId: ID;
  qualifier?: "home" | "away";
  score?: MatchScore;
  playerIn?: PlayerBasic;
  playerOut?: PlayerBasic;
}

const notificationTypeFromEventType: { [key in ID]: NotificationType } = {
  score_change: NotificationType.MatchGoal,
  break_start: NotificationType.MatchBreakResult,
  substitution: NotificationType.MatchSubstitution
};

function decodeEvent(event: any): DecodedEvent | null {
  const {
    metadata: { sport_event_id: matchId, event_id: eventType },
    payload: {
      event: { team: qualifier, player_in: playerIn, player_out: playerOut },
      sport_event_status: { home_score: homeScore, away_score: awayScore }
    }
  } = event;

  const notificationType = notificationTypeFromEventType[eventType];
  if (!notificationType) {
    return null;
  }

  return {
    notificationType,
    matchId,
    qualifier,
    score:
      homeScore !== undefined && awayScore !== undefined
        ? { home: homeScore, away: awayScore }
        : undefined,
    playerIn: playerIn && {
      id: playerIn.id,
      name: cleanPlayerName(playerIn.name)
    },
    playerOut: playerOut && {
      id: playerOut.id,
      name: cleanPlayerName(playerOut.name)
    }
  };
}

export async function triggerEventNotifications(rawEvent: any): Promise<any> {
  const event = decodeEvent(rawEvent);
  if (!event) {
    return;
  }

  const db = firestore();
  const docRef = db.collection("matches").doc(event.matchId);
  const match = await getDocumentBody<MatchRoot>(docRef);
  if (!match) {
    return;
  }

  const type = event.notificationType;
  const homeScore = event.score && event.score.home;
  const awayScore = event.score && event.score.away;
  switch (type) {
    case NotificationType.MatchBreakResult: {
      const bodies = translateAllLanguages("notifications.matchBreakResult", {
        homeScore,
        awayScore
      });
      return sendMatchNotification({
        type,
        match,
        bodies
      });
    }
    case NotificationType.MatchSubstitution: {
      const { playerIn, playerOut } = event;
      const bodies =
        playerIn && playerOut
          ? translateAllLanguages(
              "notifications.matchSubstitutionWithPlayers",
              { playerInName: playerIn.name, playerOutName: playerOut.name }
            )
          : translateAllLanguages("notifications.matchSubstitutionSimple");

      return await sendMatchNotification({
        type,
        match,
        bodies,
        exclusiveQualifier: event.qualifier
      });
    }
    case NotificationType.MatchGoal: {
      const bodies = translateAllLanguages("notifications.matchGoal", {
        homeScore,
        awayScore
      });
      return await sendMatchNotification({
        type,
        match,
        bodies
      });
    }
  }
}

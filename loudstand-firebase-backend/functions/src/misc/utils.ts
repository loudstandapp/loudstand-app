import {
  ISO8601Date,
  ISO3166v1a2Country,
  FinalSubmittedVote,
  VoteType,
  localizeTeam
} from "../../submodules/loudstand-shared";
import { Request, Response, NextFunction } from "express";
import * as countries from "i18n-iso-countries";
import {
  MatchListItem,
  TeamListItem,
  ISO639a2Language,
  LocalizedTeams
} from "../../submodules/loudstand-shared/models/general";
import { firestore } from "firebase-admin";
import * as packageJson from "../../package.json";
import { contentApiKey } from "../misc/apiKeys";

export const codeVersion = packageJson.version;

export function splitIntoChunks<T>(list: T[], maxSize: number): Array<T[]> {
  let pointer = 0;
  const chunks: Array<T[]> = [];
  do {
    const chunk = list.slice(pointer, pointer + maxSize);
    if (chunk.length > 0) {
      chunks.push(chunk);
    }
    pointer += maxSize;
  } while (pointer < list.length);
  return chunks;
}

export function cleanDate(dateWithZ: string): ISO8601Date {
  const match = dateWithZ.match(/^(\d{4}-\d{2}-\d{2}).*$/);
  if (!match || match.length < 2 || match[1] === null) {
    throw new Error(`cleanDate() could not parse timestamp "${dateWithZ}"`);
  }
  return match[1];
}

export function sortMatchesByDate<T>(
  matches: MatchListItem[]
): MatchListItem[] {
  return matches.sort((a, b) =>
    a.date === b.date ? 0 : a.date > b.date ? 1 : -1
  );
}

export async function sleep(interval: number) {
  return new Promise(resolve => setTimeout(resolve, interval));
}
export function getComparatorForKey<T>(key: keyof T, reverse: boolean = false) {
  if (reverse) {
    return (a: T, b: T) => {
      if (a[key] > b[key]) {
        return -1;
      }
      if (a[key] < b[key]) {
        return 1;
      }
      return 0;
    };
  }
  return (a: T, b: T) => {
    if (a[key] > b[key]) {
      return 1;
    }
    if (a[key] < b[key]) {
      return -1;
    }
    return 0;
  };
}

export function cleanPlayerName(name: string): string {
  return name.replace(/^([^,]+), ?(.*)$/, "$2 $1");
}

type ISO3166v1a3Country = string;
const SPORTRADAR_SELF_MADE_COUNTRIES = ["ENG", "WAL", "SCO", "NIR"];
export function countryAlpha3ToAlpha2(
  countryCode?: ISO3166v1a3Country
): ISO3166v1a2Country | null {
  if (!countryCode) {
    return null;
  }
  if (SPORTRADAR_SELF_MADE_COUNTRIES.includes(countryCode)) {
    return countries.alpha3ToAlpha2("GBR");
  }
  return countries.alpha3ToAlpha2(countryCode) || null;
}

export function getRandomNumber(min: number, max: number) {
  const diff = max - min;
  return Math.random() * diff + min;
}

export async function getDocumentBody<T>(
  ref: firestore.DocumentReference
): Promise<T | null> {
  const snap = await ref.get();
  const data = snap.data();
  if (!data || !data.body) {
    return null;
  }
  const body: T = data.body;
  return body;
}

export function getResponseForVote(vote: FinalSubmittedVote) {
  switch (vote.type) {
    case VoteType.Lineup:
      return vote.lineup;
    case VoteType.ManagerPerformance:
    case VoteType.TeamPerformance:
      return vote.rating;
    case VoteType.MostValuablePlayer:
      return vote.playerId;
    case VoteType.Question:
    case VoteType.Penalty:
    case VoteType.RedCard:
    case VoteType.Substitution:
    case VoteType.YellowCard:
    case VoteType.YellowRedCard:
      return vote.answer;
  }
  return;
}

export async function getDocument<T>(
  ref: firestore.DocumentReference
): Promise<T | null> {
  const snap = await ref.get();
  const data = snap.data();
  if (!data) {
    return null;
  }
  return data as T;
}

export function average(numbers: number[]): number {
  if (numbers.length === 0) {
    return 0;
  }
  const sum = numbers.reduce((prev, curr) => prev + curr, 0);
  return sum / numbers.length;
}

export function futureTimestamp(seconds: number) {
  return firestore.Timestamp.fromMillis(Date.now() + 1000 * seconds);
}

export function validateContentAuth(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const { apikey } = req.query;

  if (!apikey) {
    res.status(401).send("No api key specified");
    return;
  }

  if (apikey !== contentApiKey) {
    res.status(401).send("Wrong API key");
    return;
  }
  next();
}

export async function localizeTeams(
  language: ISO639a2Language,
  teams: TeamListItem[]
): Promise<string[]> {
  const db = firestore();

  const docRef = db
    .collection("localizations")
    .doc("teams")
    .collection("languages")
    .doc(language);
  //make TS happy
  const localizedTeams =
    (await getDocumentBody<LocalizedTeams>(docRef)) || undefined;
  return teams.map(team => localizeTeam(team, localizedTeams));
}

import { ResultGenerator } from "./models";
import { firestore } from "firebase-admin";
import { GenerationResult } from "../../submodules/loudstand-shared";

interface FakeResultGenerationResult extends GenerationResult {
  generated?: any;
}

export function alreadyExistsError() {
  const error = new Error("already Exists");
  // @ts-ignore
  error.alreadyExists = true;
  return error;
}

export async function generateFakeResult<A, T, I, C>(
  arg: A,
  generator: ResultGenerator<A, T, I, C>
): Promise<FakeResultGenerationResult> {
  const result: FakeResultGenerationResult = {};
  try {
    const db = firestore();
    const context: any = {};
    const destination = generator.getDestination(arg, db);
    console.log("DESTINATION", destination.path.toString());
    if (generator.preFakeResult) {
      const preGenerateResult = await generator.preFakeResult(arg, context);
      if (!preGenerateResult) {
        throw new Error(`preGenerate failed for ${generator.key}`);
      }
    }
    let input: I | undefined;
    if (generator.getFakeDataSource) {
      const dataSource = generator.getFakeDataSource(arg, db);
      console.log("DATASOURCE", dataSource.path.toString());
      const snap = await dataSource.get();
      const data = snap.data();
      input = data && data.body;
    }

    await db.runTransaction(async transaction => {
      const currentSnap = await transaction.get(destination);
      const currentData: T | undefined = currentSnap.data() as T;
      try {
        const generatedResult = await generator.fakeResult(
          arg,
          input,
          currentData,
          context
        );
        await transaction.set(destination, generatedResult);
        result.generated = generatedResult;
      } catch (e) {
        if (e.alreadyExists) {
          result.generated = null;
          return;
        }
        throw e;
      }
    });
    return result;
  } catch (e) {
    console.error(e);
    result.errors = [e];
    return result;
  }
}

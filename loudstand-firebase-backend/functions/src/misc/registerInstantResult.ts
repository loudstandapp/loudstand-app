import { ID, FinalSubmittedVote } from "../../submodules/loudstand-shared";
import { InstantResultGenerator } from "./models";

import { firestore } from "firebase-admin";

export function averageValue(
  currentAverage: number,
  currentNumberOfVotes: number,
  submittedValue: number
) {
  return (
    (currentAverage * currentNumberOfVotes + submittedValue) /
    (currentNumberOfVotes + 1)
  );
}

type AveragePerId = { [key in ID]: number };

export function adjustPickedAverages(
  currentNumberOfVotes: number,
  currentValues: AveragePerId,
  voted: ID[]
) {
  const votedIdsSet = new Set(voted);
  const allIds = [...new Set([...voted, ...Object.keys(currentValues)])];
  const results: AveragePerId = allIds.reduce(
    (acc, id) => ({
      ...acc,
      [id]: averageValue(
        currentValues[id] || 0,
        currentNumberOfVotes,
        votedIdsSet.has(id) ? 1 : 0
      )
    }),
    {}
  );
  return results;
}

export async function registerInstantResult<A, T, I, C>(
  vote: FinalSubmittedVote,
  generator: InstantResultGenerator<A, T, I, C>
) {
  const db = firestore();
  try {
    console.log("registerVote running: ", generator.key);
    const context = {} as C;
    const arg = generator.argFromVote(vote);
    const destination = generator.getDestination(arg, db);
    await db.runTransaction(async transaction => {
      const currentSnap = await transaction.get(destination);
      const currentData = currentSnap.data();
      const currentValue = currentData
        ? (currentData as T)
        : generator.initialValue;
      const newValue = generator.countVote(vote, currentValue, context);
      await transaction.set(destination, newValue);
    });
  } catch (e) {
    console.error(e);
  }
}

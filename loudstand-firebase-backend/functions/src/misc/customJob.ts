import { CustomJobType, ResultGenerationJobStatus, CustomJob } from "./models";
import { seasonManagerPopularity } from "../results/seasonManagerPopularity";
import { seasonPlayerPopularity } from "../results/seasonPlayerPopularity";
import { seasonTeamPopularity } from "../results/seasonTeamPopularity";
import { seasonMostValuablePlayerShare } from "../results/seasonMostValuablePlayerShare";
import { seasonLineupPlayerShare } from "../results/seasonLineupPlayerShare";
import { FinalSubmittedVote } from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { createTask, JobType } from "./tasks";
import { encodeArg } from "./scheduleResults";
import { matchOpenForVotingScheduledTask } from "../notifications/matchScheduledTask";
import { questionAvailableScheduledTask } from "../notifications/questionHandler";

export interface CustomJobExecutor<A, R> {
  execute: (arg: A) => Promise<R>;
  type: CustomJobType;
  nextScheduledTask: (arg: A) => Promise<FirebaseFirestore.Timestamp>;
  argFromVote?: (vote: FinalSubmittedVote) => A;
}

export function getHandlerForCustomJobType(type: CustomJobType) {
  const handlerFromType: {
    [key in CustomJobType]: CustomJobExecutor<any, any>;
  } = {
    [CustomJobType.SeasonManagerPopularity]: seasonManagerPopularity,
    [CustomJobType.SeasonPlayerPopularity]: seasonPlayerPopularity,
    [CustomJobType.SeasonTeamPopularity]: seasonTeamPopularity,
    [CustomJobType.SeasonMostValuablePlayerShare]: seasonMostValuablePlayerShare,
    [CustomJobType.SeasonLineupPlayerShare]: seasonLineupPlayerShare,
    [CustomJobType.MatchOpenForVotingNotification]: matchOpenForVotingScheduledTask,
    [CustomJobType.QuestionAvailableNotification]: questionAvailableScheduledTask
  };
  return handlerFromType[type];
}

export async function scheduleCustomJobFromVote<A, R>(
  vote: FinalSubmittedVote,
  handler: CustomJobExecutor<A, R>
) {
  if (!handler.argFromVote) {
    console.error(
      "scheduleCustomJobFromVote: not argFromVote in",
      handler.type,
      vote
    );
    return;
  }
  const arg = handler.argFromVote(vote);
  await scheduleCustomJob(arg, handler);
}

export async function scheduleCustomJob<A, R>(
  arg: A,
  handler: CustomJobExecutor<A, R>
) {
  const db = firestore();
  const argString = encodeArg(arg as any);
  const query = db
    .collection("jobs")
    .where("status", "==", ResultGenerationJobStatus.Scheduled)
    .where("jobType", "==", handler.type)
    .where("arg", "==", arg)
    .limit(1);

  const results = await query.get();
  if (results.size === 0) {
    const jobId = `${handler.type}---${argString}---${Date.now()}`;
    const scheduled = await handler.nextScheduledTask(arg);
    const taskResult = await createTask(
      jobId,
      scheduled.seconds,
      JobType.Custom
    );
    const job: CustomJob = {
      id: jobId,
      taskName: taskResult.name || "N/A",
      status: ResultGenerationJobStatus.Scheduled,
      created: firestore.Timestamp.now(),
      scheduled: scheduled,
      errorMessage: null,
      started: null,
      completed: null,
      result: null,
      arg,
      jobType: handler.type
    };
    await db
      .collection("jobs")
      .doc(jobId)
      .set(job);
  }
}

import {
  FinalSubmittedVote,
  UserVoteHistory,
  generateVoteKeyFromVote,
  ID,
  UserVoteHistoryItem
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { getResponseForVote } from "./utils";

const MAX_VOTES = 2000;
const MAX_AGE = 30 * 24 * 60 * 60;

export async function updateUserVoteHistory(vote: FinalSubmittedVote, uid: ID) {
  const db = firestore();
  const docRef = db
    .collection("users")
    .doc(uid)
    .collection("votes")
    .doc("history");

  const voteKey = generateVoteKeyFromVote(vote);

  await db.runTransaction(async t => {
    const currentSnap = await t.get(docRef);
    const data = currentSnap.data() as UserVoteHistory | null;
    const newItem: UserVoteHistoryItem<any> = {
      timestamp: firestore.Timestamp.now(),
      response: getResponseForVote(vote)
    };
    // Remove oldest votes before object gets too big
    if (data && Object.keys(data).length > MAX_VOTES) {
      const oldestTime = firestore.Timestamp.fromMillis(
        Date.now() - MAX_AGE * 1000
      );
      const validHistory = Object.keys(data)
        .filter(key => data[key].timestamp.toMillis() > oldestTime.toMillis())
        .reduce(
          (acc: UserVoteHistory, key) => ({
            ...acc,
            [key]: data[key]
          }),
          {}
        );

      const updatedHistory = {
        ...validHistory,
        [voteKey]: newItem
      };
      await t.set(docRef, updatedHistory);
      return;
    }
    await t.set(docRef, { [voteKey]: newItem }, { merge: true });
  });
}

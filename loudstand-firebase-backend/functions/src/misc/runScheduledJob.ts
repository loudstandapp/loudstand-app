import { ID, FinalSubmittedVote } from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { ResultGenerationJob, ResultGenerationJobStatus } from "./models";
import { getGeneratorForResultType } from "../voting/handleVote";
import { decodeArg } from "./scheduleResults";

export async function runScheduledJob(jobId: ID) {
  const db = firestore();
  const jobRef = db.collection("jobs").doc(jobId);
  let errors: Error[] = [];

  // transnaction that starts the job
  const job = await db.runTransaction(async t => {
    try {
      const jobSnap = await t.get(jobRef);
      const data = jobSnap.data();
      if (!data) {
        throw new Error(
          `runScheduledJob: Job document not found for id ${jobId}`
        );
      }
      const jobData = data as ResultGenerationJob;
      if (jobData.status !== ResultGenerationJobStatus.Scheduled) {
        throw new Error(
          `runScheduledJob: Job id ${jobId} has already been started`
        );
      }

      t.set(
        jobRef,
        {
          status: ResultGenerationJobStatus.InProgress,
          started: firestore.Timestamp.now()
        },
        { merge: true }
      );
      return jobData;
    } catch (e) {
      console.error(e);
      errors.push(e);
      return undefined;
    }
  });
  if (!job) {
    return;
  }

  const generator = getGeneratorForResultType(job.resultType);

  const docRef = db.collection("scheduledVotes").doc(job.resultType);
  const collectionRefs = await docRef.listCollections();

  let processedVotes = 0;
  let processedResults = 0;
  for (const collectionRef of collectionRefs) {
    const arg = decodeArg(collectionRef.id);
    const destination = generator.getDestination(arg, db);

    // transaction for each result
    processedVotes += await db.runTransaction(async t => {
      try {
        const voteSnaps = await t.get(collectionRef); // // TODO limit this?
        const currentSnap = await t.get(destination);
        const currentValue = currentSnap.data() || generator.initialValue;

        let tempValue = currentValue;
        for (const voteSnap of voteSnaps.docs) {
          const vote = voteSnap.data();
          if (!vote) {
            throw new Error(`runScheduledJob: Empty vote id ${voteSnap.id}`);
          }
          tempValue = generator.countVote(
            vote as FinalSubmittedVote,
            tempValue,
            {}
          );
          t.delete(voteSnap.ref);
        }
        t.set(destination, tempValue);
        processedResults++;
        return voteSnaps.size;
      } catch (e) {
        console.error(e);
        console.log(generator);
        errors.push(e);
        return 0;
      }
    });
  }
  await jobRef.set(
    {
      status: ResultGenerationJobStatus.Completed,
      completed: firestore.Timestamp.now(),
      processedVotes,
      processedResults
    },
    { merge: true }
  );
  const result = {
    errors,
    processedResults,
    processedVotes
  };
  console.log(result);
  return result;
}

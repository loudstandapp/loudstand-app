import * as I18n from "i18n-js";

import * as en from "../../translations/en.json";
import * as pt from "../../translations/pt.json";
import { ISO639a2Language } from "../../submodules/loudstand-shared/index.js";

const translations = {
  en,
  pt
};
// @ts-ignore
I18n.translations = translations;

export function translate(
  language: ISO639a2Language,
  key: string,
  config?: object
) {
  // @ts-ignore
  I18n.locale = language;
  return I18n.t(key, config);
}

export const availableLanguages = Object.keys(translations);

export function translateAllLanguages(
  key: string,
  config?: object
): { [language in ISO639a2Language]: string } {
  return availableLanguages.reduce(
    (acc, current) => ({
      ...acc,
      [current]: translate(current, key, config)
    }),
    {}
  );
}

import { ID } from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { ResultGenerationJobStatus, CustomJob } from "./models";
import { getHandlerForCustomJobType } from "./customJob";

export async function runCustomScheduledJob(jobId: ID) {
  const db = firestore();
  const jobRef = db.collection("jobs").doc(jobId);

  // transaction that starts the job
  const job = await db.runTransaction(async t => {
    try {
      const jobSnap = await t.get(jobRef);
      const data = jobSnap.data();
      if (!data) {
        throw new Error(
          `runCustomScheduledJob: Job document not found for id ${jobId}`
        );
      }
      const jobData = data as CustomJob;
      if (jobData.status !== ResultGenerationJobStatus.Scheduled) {
        throw new Error(
          `runCustomScheduledJob: Job id ${jobId} has already been started`
        );
      }

      t.set(
        jobRef,
        {
          status: ResultGenerationJobStatus.InProgress,
          started: firestore.Timestamp.now()
        },
        { merge: true }
      );
      return jobData;
    } catch (e) {
      console.error(e);
      return undefined;
    }
  });
  if (!job) {
    return;
  }

  console.log("Running custom job: ", job);

  const handler = getHandlerForCustomJobType(job.jobType);
  try {
    const result = await handler.execute(job.arg);
    await jobRef.set(
      {
        status: ResultGenerationJobStatus.Completed,
        completed: firestore.Timestamp.now(),
        result: result || null
      },
      { merge: true }
    );
    return result;
  } catch (error) {
    console.error(error);
    await jobRef.set(
      {
        status: ResultGenerationJobStatus.Failed,
        errorMessage: error.message
      },
      { merge: true }
    );
    return null;
  }
}

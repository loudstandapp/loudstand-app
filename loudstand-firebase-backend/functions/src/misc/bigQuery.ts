import { FinalSubmittedVote } from "../../submodules/loudstand-shared";
import { BigQuery } from "@google-cloud/bigquery";
import { convertVoteForExport } from "../voting/convertVote";

export async function exportVote(vote: FinalSubmittedVote) {
  const bq = new BigQuery();
  const exportedVote = convertVoteForExport(vote);
  try {
    const result = await bq
      .dataset("football")
      .table("votes")
      .insert(exportedVote);
    return result;
    // TODO optimize this to bundle insertions in jobs
  } catch (e) {
    console.error(e);
    console.log(e.response.insertErrors);
    console.log(JSON.stringify(e.errors));
    console.log("ORIGINAL", JSON.stringify(vote, null, 2));
    console.log("EXPORTED", JSON.stringify(exportedVote, null, 2));
    return e;
  }
}

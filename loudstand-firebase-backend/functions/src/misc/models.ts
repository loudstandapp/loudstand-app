import { DocumentReference, Firestore } from "@google-cloud/firestore";
import {
  ISO8601Timestamp,
  ID,
  FinalSubmittedVote
} from "../../submodules/loudstand-shared";

type UrlString = string;

export interface DataImporter<A, I, O, C = null> {
  preImport?: (arg: A, context: C) => Promise<boolean>;
  postImport?: (arg: A, input: I, output: O, context: C) => Promise<boolean>;
  preProcessData?: (arg: A, input: I, context: C) => Promise<boolean>;
  getUrl: (
    arg: A
  ) => Promise<UrlString> | Promise<UrlString[]> | UrlString | UrlString[];
  getDestination: (arg: A, db: Firestore) => DocumentReference;
  getExpiration: (input: I) => ISO8601Timestamp | null;
  processData: (input: I, arg: A, context: C) => O;
  getImportResult: (output: O) => any;
  key: string;
  disableMerge?: boolean;
}

export interface DataImporterWithUpdate<A, I, O, U>
  extends DataImporter<A, I, O> {
  mergeUpdate: (current: O | null, update: U) => O | null;
  getIdFromUpdate: (update: U) => Promise<A | null>;
}

export interface MassUpdater<A, I, O, U> {
  getUrl: (arg: A) => UrlString;
  getUpdates: (input: I) => { [key in ID]: U };
  getDestination: (key: ID, update: U, db: Firestore) => DocumentReference;
  mergeUpdate: (arg: A, current: O, update: U, input: I) => O | null;
}

interface AbstractResultGenerator<A, T, I, C = null> {
  // Faking results
  preFakeResult?: (arg: A, context: C) => Promise<boolean>;
  fakeResult: (
    arg: A,
    input: I | undefined,
    current: T | undefined,
    context: C
  ) => T;
  getFakeDataSource?: (arg: A, db: Firestore) => DocumentReference;
  // General
  type: ResultGenerationType;
  argFromVote: (vote: FinalSubmittedVote) => A;
  initialValue: T;
  getDestination: (arg: A, db: Firestore) => DocumentReference;
  countVote: (vote: FinalSubmittedVote, current: T, context: C) => T;
  // Receive queryResult...
  key: string;
}

export enum ResultGenerationType {
  // Data is processed immediately and destination document is updated
  Instant = "INSTANT",
  // A job is scheduled for each document affected by the vote, votes are read from firestore and added to result document
  Scheduled = "SCHEDULED",
  // Data is queried from external SQL database as scheduled tasks.
  External = "EXTERNAL",
  // A custom task is scheduled after a vote is thrown.
  CustomScheduled = "CUSTOM_SCHEDULED"
}

export interface InstantResultGenerator<A, T, I, C = null>
  extends AbstractResultGenerator<A, T, I, C> {
  type: typeof ResultGenerationType.Instant;
}

export interface ScheduledResultGenerator<A, T, I, C = null>
  extends AbstractResultGenerator<A, T, I, C> {
  nextScheduledTask: () => FirebaseFirestore.Timestamp;
  resultType: StatisticsResultType;
  type: typeof ResultGenerationType.Scheduled;
}

export interface ExternalResultGenerator<A, T, I, C = null>
  extends AbstractResultGenerator<A, T, I, C> {
  type: typeof ResultGenerationType.External;
}

export interface CustomScheduledResultGenerator<A, T, I, C = null>
  extends AbstractResultGenerator<A, T, I, C> {
  type: typeof ResultGenerationType.CustomScheduled;
}

export type ResultGenerator<A, T, I, C = null> =
  | InstantResultGenerator<A, T, I, C>
  | ScheduledResultGenerator<A, T, I, C>
  | ExternalResultGenerator<A, T, I, C>;

export enum ResultGenerationJobStatus {
  Scheduled = "SCHEDULED",
  InProgress = "IN_PROGRESS",
  Completed = "COMPLETED",
  Failed = "FAILED"
}

export enum StatisticsResultType {
  ManagerSubstitutionApproval = "MANAGER_SUBSTITUTION_APPROVAL",
  MatchTeamPerformance = "MATCH_TEAM_PERFORMANCE",
  MatchLineupSuggestion = "MATCH_LINEUP_SUGGESTION",
  MatchManagerPerformance = "MATCH_MANAGER_PERFORMANCE",
  MatchMostValuablePlayers = "MATCH_MOST_VALUABLE_PLAYERS",
  MatchEventResults = "MATCH_EVENT_RESULTS",
  TeamMostValuablePlayers = "TEAM_MOST_VALUABLE_PLAYERS",
  TeamLineupSuggestion = "TEAM_LINEUP_SUGGESTION",
  UserTotalReactions = "USER_TOTAL_REACTIONS",
  UserLineupSuggestion = "USER_LINEUP_SUGGESTION",
  UserMostValuablePlayers = "USER_MOST_VALUABLE_PLAYERS",
  UserTeamPerformance = "USER_TEAM_PERFORMANCE",
  UserManagerPerformance = "USER_MANAGER_PERFORMANCE",
  Question = "QUESTION"
}

export enum CustomJobType {
  SeasonManagerPopularity = "SEASON_MANAGER_POPULARITY",
  SeasonTeamPopularity = "SEASON_TEAM_POPULARITY",
  SeasonPlayerPopularity = "SEASON_PLAYER_POPULARITY",
  SeasonMostValuablePlayerShare = "SEASON_MOST_VALUABLE_PLAYERS",
  SeasonLineupPlayerShare = "SEASON_LINEUP_PLAYER_SHARE",
  MatchOpenForVotingNotification = "MATCH_OPEN_FOR_VOTING_NOTIFICATION",
  QuestionAvailableNotification = "QUESTION_AVAILABLE_NOTIFICATION"
}

export interface ResultGenerationJob {
  id: ID;
  taskName: string;
  resultType: StatisticsResultType;
  status: ResultGenerationJobStatus;
  errorMessage: string | null;
  created: FirebaseFirestore.Timestamp;
  scheduled: FirebaseFirestore.Timestamp;
  started: FirebaseFirestore.Timestamp | null;
  completed: FirebaseFirestore.Timestamp | null;
  processedResults: number | null;
  processedVotes: number | null;
}

export interface CustomJob {
  id: ID;
  taskName: string;
  status: ResultGenerationJobStatus;
  errorMessage: string | null;
  created: FirebaseFirestore.Timestamp;
  scheduled: FirebaseFirestore.Timestamp;
  started: FirebaseFirestore.Timestamp | null;
  completed: FirebaseFirestore.Timestamp | null;
  jobType: CustomJobType;
  arg: any;
  result: any;
}

import { FinalSubmittedVote } from "../../submodules/loudstand-shared";
import {
  ResultGenerationJobStatus,
  ResultGenerationJob,
  ScheduledResultGenerator
} from "./models";
import { firestore } from "firebase-admin";
import { createTask, JobType } from "./tasks";

const STRING_KEY = "_string";

export function encodeArg(arg: string | { [key in string]: string }): string {
  const argObj = typeof arg === "object" ? arg : { [STRING_KEY]: arg };
  const keys = Object.keys(argObj).sort();
  const parts = keys.map(key => `${key}=${encodeURIComponent(argObj[key])}`);
  return parts.join("&");
}

export function decodeArg(
  encoded: string
): string | { [key in string]: string } {
  const parts = encoded.split("&");
  const obj: { [key in string]: string } = parts.reduce((acc, item) => {
    const [key, value] = item.split("=");
    return {
      ...acc,
      [key]: decodeURIComponent(value)
    };
  }, {});
  if (obj[STRING_KEY]) {
    return obj[STRING_KEY];
  }
  return obj;
}

const RESCHEDULE_MARGIN = 60 * 5;

export async function scheduleVoteResults<A, T, I, C>(
  vote: FinalSubmittedVote,
  generator: ScheduledResultGenerator<A, T, I, C>
) {
  const db = firestore();
  const query = db
    .collection("jobs")
    .where("status", "==", ResultGenerationJobStatus.Scheduled)
    .where("resultType", "==", generator.resultType)
    .where("scheduled", ">", new Date(Date.now() - RESCHEDULE_MARGIN * 1000))
    .limit(1);

  const results = await query.get();
  if (results.size === 0) {
    const jobId = `${generator.resultType}---${Date.now()}`;
    const scheduled = generator.nextScheduledTask();
    const taskResult = await createTask(
      jobId,
      scheduled.seconds,
      JobType.ScheduledResults
    );
    const job: ResultGenerationJob = {
      id: jobId,
      taskName: taskResult.name || "N/A",
      resultType: generator.resultType,
      status: ResultGenerationJobStatus.Scheduled,
      created: firestore.Timestamp.now(),
      scheduled: scheduled,
      started: null,
      errorMessage: null,
      completed: null,
      processedVotes: null,
      processedResults: null
    };
    await db
      .collection("jobs")
      .doc(jobId)
      .set(job);
  }

  const arg = generator.argFromVote(vote);
  const encodedArg = encodeArg(arg as any);
  const collectionRef = db
    .collection("scheduledVotes")
    .doc(generator.resultType)
    .collection(encodedArg);
  await collectionRef.add(vote);
}

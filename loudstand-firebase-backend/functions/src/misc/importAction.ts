import { DataImporter } from "./models";
import Axios, { AxiosResponse } from "axios";
import { firestore } from "firebase-admin";
import { GenerationResult } from "../../submodules/loudstand-shared";
import { codeVersion } from "./utils";

interface ImportResult extends GenerationResult {
  imported?: any;
}

export function validateStatus(status: number) {
  if (status === 404) {
    return true;
  }
  if (status < 200) {
    return false;
  }
  if (status > 299) {
    return false;
  }
  return true;
}

export async function startImport<A, I, O, C = null>(
  arg: A,
  importer: DataImporter<A, I, O, C>
): Promise<ImportResult> {
  const result: ImportResult = {};
  try {
    const context: any = {};
    if (importer.preImport) {
      console.log(`Running preimport for ${importer.key}`);
      const preImportResult = await importer.preImport(arg, context);
      if (!preImportResult) {
        throw new Error("preImport failed");
      }
    }

    async function getInput() {
      const urlOrUrls = await importer.getUrl(arg);
      if (Array.isArray(urlOrUrls)) {
        const responsePromises = urlOrUrls.map(url =>
          Axios.get(url, { validateStatus })
        );
        const responses = await Promise.all(responsePromises);
        return responses.map(({ data }) => data);
      }
      try {
        const response = await Axios.get(urlOrUrls, { validateStatus });
        return response.data;
      } catch (error) {
        const response = error.response as AxiosResponse;
        console.error("importAction: getInput: Error getting data: ", {
          responseBody: response.data,
          url: urlOrUrls,
          code: response.status,
          responseHeaders: response.headers
        });
        throw error;
      }
    }

    const input = await getInput();

    if (importer.preProcessData) {
      console.log(`Running preProcessData for ${importer.key}`);
      const preProcessDataResult = await importer.preProcessData(
        arg,
        input,
        context
      );
      if (!preProcessDataResult) {
        throw new Error("preProcessData failed");
      }
    }
    const db = firestore();
    const destination = importer.getDestination(arg, db);

    const output = importer.processData(input, arg, context);
    // Check if output is null, load current document and skip if current has content.
    if (output === null) {
      const snap = await destination.get();
      const data = snap.data();
      if (data && data.body) {
        throw new Error(
          `existing data, but null output received from ${
            importer.key
          }, arg: ${JSON.stringify(arg)}, input: ${JSON.stringify(input)}`
        );
      }
    }
    result.imported = importer.getImportResult(output);
    const expiration = importer.getExpiration(input);
    await destination.set(
      {
        body: output,
        expiration,
        imported: new Date().toISOString(),
        codeVersion
      },
      { merge: !importer.disableMerge }
    );
    if (importer.postImport) {
      console.log(`Running postImport for ${importer.key}`);
      const postImportResult = await importer.postImport(
        arg,
        input,
        output,
        context
      );
      if (!postImportResult) {
        throw new Error(`postImport failed for: ${importer.key}`);
      }
    }
    console.log(`Ran importer for ${importer.key}: ${JSON.stringify(arg)}`);
    return result;
  } catch (e) {
    console.error(e);
    result.errors = [e];
    return result;
  }
}

export const makeImporter = <A, I, O, C>(
  importer: DataImporter<A, I, O, C>
) => (arg: A) => startImport(arg, importer);

export async function importIfNecessary<A, I, O>(
  arg: A,
  importer: DataImporter<A, I, O>
) {
  function shouldImport(snap: firestore.DocumentSnapshot) {
    const data = snap.data();
    if (!data) {
      console.log("Document is not found, importing.");
      return true;
    }
    if (data.expiration && data.expiration < Date.now()) {
      console.log("Document is expired, importing.");
      return true;
    }
    console.log("Document exists, not importing.");
    return false;
  }

  const db = firestore();
  const docRef = importer.getDestination(arg, db);
  const snapshot = await docRef.get();
  console.log(`Checking if ${importer.key}: ${arg} should be imported`);
  if (shouldImport(snapshot)) {
    await startImport(arg, importer);
  }
}

import { scheduleTodaysQuestions } from "../notifications/questionHandler";
import { triggerUpdateVotableTeams } from "./triggerUpdateVotableTeams";

export async function midnightCronJobExecutor() {
  try {
    await triggerUpdateVotableTeams();
    console.log("ran triggerUpdateVotableTeams");
  } catch (e) {
    console.error("Error executing triggerUpdateVotableTeams ");
    console.error(e);
  }
  try {
    await scheduleTodaysQuestions();
    console.log("ran scheduleTodaysQuestions");
  } catch (e) {
    console.error("Error executing scheduleTodaysQuestions ");
    console.error(e);
  }
}

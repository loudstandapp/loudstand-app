import Axios from "axios";
import { firestore } from "firebase-admin";
import { sleep, codeVersion } from "./utils";
import { MassUpdater } from "./models";

interface MassUpdateResult {
  updated: number;
  skipped: number;
  errors: Error[];
}

export async function massUpdate<A, I, U, O>(
  arg: A,
  massUpdater: MassUpdater<A, I, U, O>
): Promise<MassUpdateResult> {
  const result: MassUpdateResult = { updated: 0, skipped: 0, errors: [] };
  try {
    const url = massUpdater.getUrl(arg);
    await sleep(1100); // FIXME
    const response = await Axios.get(url);
    const input = response.data;
    const updates = massUpdater.getUpdates(input);
    const db = firestore();
    for (const key in updates) {
      const update = updates[key];
      const destination = massUpdater.getDestination(key, update, db);
      await db.runTransaction(async transaction => {
        const currentSnap = await transaction.get(destination);

        const currentData = currentSnap.data();
        const currentBody = currentData ? currentData.body : null;
        const newBody = massUpdater.mergeUpdate(
          arg,
          currentBody,
          update,
          input
        );
        if (newBody) {
          await transaction.set(
            destination,
            {
              body: newBody,
              updated: new Date().toISOString(),
              codeVersion
            },
            { merge: true }
          );
          result.updated++;
        } else {
          result.skipped++;
        }
      });
    }
  } catch (e) {
    console.error(e);
    result.errors.push(e);
  }
  return result;
}

export const makeUpdater = <A, I, O, U>(
  massUpdater: MassUpdater<A, I, O, U>
) => (arg: A) => massUpdate(arg, massUpdater);

import tasks, { Task, CreateTaskRequest } from "@google-cloud/tasks";
import { REGION } from "../..";
import { ID } from "../../submodules/loudstand-shared";

export enum JobType {
  ScheduledResults,
  Custom
}

// Remember to change predeploy.sh if changing this.
const queueNames = {
  [JobType.ScheduledResults]: "ScheduledResults",
  [JobType.Custom]: "Custom"
};

// Remember to change index.ts if changing this.
const functionNames = {
  [JobType.ScheduledResults]: "runScheduledJob",
  [JobType.Custom]: "runCustomScheduledJob"
};

export async function createTask(
  jobId: ID,
  scheduledTime: number,
  type: JobType
) {
  const projectId = process.env.GCLOUD_PROJECT;
  if (!projectId) {
    throw new Error("createTask: process.env.GCLOUD_PROJECT has not been set");
  }

  const functionName = functionNames[type];
  const url = `https://${REGION}-${projectId}.cloudfunctions.net/${functionName}`;
  const scheduleTime = { seconds: scheduledTime };

  const queueName = queueNames[type];

  //@ts-ignore wrong typings
  const client = new tasks.v2beta3.CloudTasksClient({});
  const parent = client.queuePath(projectId, REGION, queueName);

  const task: Partial<Task> = {
    // @ts-ignore wrong typings
    httpRequest: {
      url,
      httpMethod: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      // TODO: Add a shared secret or signature
      body: Buffer.from(JSON.stringify({ data: jobId }))
    },
    // @ts-ignore wrong typings
    scheduleTime
  };

  const request: CreateTaskRequest = {
    task,
    parent
  };
  const [result] = await client.createTask(request);
  return result;
}

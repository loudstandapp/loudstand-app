import { firestore } from "firebase-admin";
import { PubSub } from "@google-cloud/pubsub";
import {
  autoImportMatchCalendarHandler,
  AutoImportMatchCalendarArgs
} from "../autoimport/autoImportMatchCalendar";

const MATCH_PRELOAD_DAYS = 7;

export async function triggerUpdateVotableTeams() {
  const db = firestore();
  const pubsub = new PubSub();
  const query = db
    .collection("teams")
    .where("body.defaultCurrentSeasonId", ">", "");
  const result = await query.get();
  console.log("team Ids", result.docs.map(doc => doc.id));
  const promises = result.docs.map(doc => {
    const settings: AutoImportMatchCalendarArgs = {
      teamId: doc.id,
      preloadMatchesForDays: MATCH_PRELOAD_DAYS
    };
    return pubsub
      .topic(autoImportMatchCalendarHandler.topic)
      .publish(Buffer.from(JSON.stringify(settings)));
  });
  return await Promise.all(promises);
}

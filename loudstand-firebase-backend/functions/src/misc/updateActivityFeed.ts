import {
  FinalSubmittedVote,
  VoteType,
  UserActivity,
  UserActivityType,
  MatchRoot,
  PlayerList,
  TeamRoot,
  Question,
  MatchTimeline,
  UserActivityItem,
  ID,
  UserActivityFeed
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";
import { getDocumentBody, getComparatorForKey } from "./utils";

const MAX_ENTRIES = 100;
const LIMITED_ENTRIES = 90;

async function getActivityFromVote(
  vote: FinalSubmittedVote
): Promise<UserActivity> {
  const db = firestore();
  switch (vote.type) {
    case VoteType.Lineup: {
      const match = await getDocumentBody<MatchRoot>(
        db.collection("matches").doc(vote.matchId)
      );
      if (!match) {
        throw new Error("No match data." + JSON.stringify(vote));
      }
      const team = await getDocumentBody<TeamRoot>(
        db.collection("teams").doc(vote.teamId)
      );
      if (!team) {
        throw new Error("No team data." + JSON.stringify(vote));
      }
      const teamPlayers = await getDocumentBody<PlayerList>(
        db
          .collection("teams")
          .doc(vote.teamId)
          .collection("attributes")
          .doc("players")
      );
      if (!teamPlayers) {
        throw new Error("No teamPlayers data" + JSON.stringify(vote));
      }
      const players: PlayerList = vote.lineup.reduce(
        (acc, playerId) => ({ ...acc, [playerId]: teamPlayers[playerId] }),
        {}
      );
      return {
        type: UserActivityType.Lineup,
        response: vote.lineup,
        match,
        players,
        team
      };
    }
    case VoteType.Question: {
      const ref = db.collection("questions").doc(vote.questionId);
      const snap = await ref.get();
      const question = snap.data() as Question | undefined;
      if (!question) {
        throw new Error("No question data" + JSON.stringify(vote));
      }
      return {
        type: UserActivityType.Question,
        response: vote.answer,
        question
      };
    }
    case VoteType.ManagerPerformance: {
      const match = await getDocumentBody<MatchRoot>(
        db.collection("matches").doc(vote.matchId)
      );
      const team = await getDocumentBody<TeamRoot>(
        db.collection("teams").doc(vote.teamId)
      );
      if (!match) {
        throw new Error("No match data" + JSON.stringify(vote));
      }
      if (!team) {
        throw new Error("No team data" + JSON.stringify(vote));
      }
      return {
        type: UserActivityType.ManagerPerformance,
        response: vote.rating,
        match,
        team
      };
    }
    case VoteType.TeamPerformance: {
      const match = await getDocumentBody<MatchRoot>(
        db.collection("matches").doc(vote.matchId)
      );
      const team = await getDocumentBody<TeamRoot>(
        db.collection("teams").doc(vote.teamId)
      );
      if (!match) {
        throw new Error("No match data" + JSON.stringify(vote));
      }
      if (!team) {
        throw new Error("No team data" + JSON.stringify(vote));
      }
      return {
        type: UserActivityType.TeamPerformance,
        response: vote.rating,
        match,
        team
      };
    }
    case VoteType.MostValuablePlayer: {
      const match = await getDocumentBody<MatchRoot>(
        db.collection("matches").doc(vote.matchId)
      );
      if (!match) {
        throw new Error("No match data." + JSON.stringify(vote));
      }
      const team = await getDocumentBody<TeamRoot>(
        db.collection("teams").doc(vote.teamId)
      );
      if (!team) {
        throw new Error("No team data." + JSON.stringify(vote));
      }
      const teamPlayers = await getDocumentBody<PlayerList>(
        db
          .collection("teams")
          .doc(vote.teamId)
          .collection("attributes")
          .doc("players")
      );
      if (!teamPlayers) {
        throw new Error("No teamPlayers data" + JSON.stringify(vote));
      }
      const player = teamPlayers[vote.playerId];
      return {
        type: UserActivityType.MostValuablePlayer,
        response: player,
        match,
        team
      };
    }
    case VoteType.Penalty:
    case VoteType.RedCard:
    case VoteType.YellowCard:
    case VoteType.YellowRedCard:
    case VoteType.Substitution: {
      const match = await getDocumentBody<MatchRoot>(
        db.collection("matches").doc(vote.matchId)
      );
      if (!match) {
        throw new Error("No match data" + JSON.stringify(vote));
      }
      const timeline = await getDocumentBody<MatchTimeline>(
        db
          .collection("matches")
          .doc(vote.matchId)
          .collection("attributes")
          .doc("timeline")
      );
      if (!timeline) {
        throw new Error("No timeline data" + JSON.stringify(vote));
      }
      const matchEvent = timeline.events[vote.eventId];
      return {
        type: UserActivityType.MatchEvent,
        response: vote.answer,
        match,
        matchEvent
      };
    }
    default: {
      throw new Error("Invalid vote type" + JSON.stringify(vote));
    }
  }
}

export async function addVoteToActivityFeed(vote: FinalSubmittedVote, id: ID) {
  const db = firestore();
  const activity = await getActivityFromVote(vote);
  const activityItem: UserActivityItem = {
    timestamp: new Date().toISOString(),
    activity,
    voteId: id
  };
  const userActivityFeedRef = db
    .collection("users")
    .doc(vote.userId)
    .collection("votes")
    .doc("activityFeed");
  await db.runTransaction(async transaction => {
    const currentSnap = await transaction.get(userActivityFeedRef);
    const currentData: UserActivityFeed | undefined = currentSnap.data();
    if (currentData && Object.keys(currentData).length >= MAX_ENTRIES) {
      console.log("Current activity feed is too long, cutting old items away");
      const sortedByDate = Object.values(currentData).sort(
        getComparatorForKey("timestamp", true)
      );
      const limitedList = sortedByDate.slice(0, LIMITED_ENTRIES);
      const limitedFeed = limitedList.reduce(
        (acc, item) => ({ ...acc, [item.voteId]: item }),
        {}
      );
      const updatedFeed = { ...limitedFeed, [id]: activityItem };
      await transaction.set(userActivityFeedRef, updatedFeed);
      console.log("New limited activity feed saved");
      return;
    }
    console.log("Updating activity feed");
    await transaction.set(
      userActivityFeedRef,
      { [id]: activityItem },
      { merge: true }
    );
    console.log("Updated activity feed saved");
  });
}

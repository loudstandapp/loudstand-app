import { DataImporterWithUpdate } from "./models";
import { firestore } from "firebase-admin";
import { GenerationResult } from "../../submodules/loudstand-shared";
import { codeVersion } from "./utils";

interface UpdateResult extends GenerationResult {
  updated?: any;
}

export async function receiveUpdate<A, I, O, U>(
  update: U,
  importer: DataImporterWithUpdate<A, I, O, U>
): Promise<UpdateResult> {
  const result: UpdateResult = {};
  try {
    const id = await importer.getIdFromUpdate(update);
    if (!id) {
      throw new Error(
        `Unable to parse id from update in ${importer.key}, ${JSON.stringify(
          update
        )}`
      );
    }
    const db = firestore();
    const destination = importer.getDestination(id, db);
    await db.runTransaction(async transaction => {
      const currentSnap = await transaction.get(destination);

      const currentData = currentSnap.data();
      const currentBody = currentData ? currentData.body : null;
      const newBody = importer.mergeUpdate(currentBody, update);
      if (!newBody) {
        result.updated = false;
        return;
      }
      try {
        // Set to expire now if no current data is present.
        const expiration =
          (currentData && currentData.expiration) || new Date().toISOString();
        await transaction.set(
          destination,
          {
            body: newBody,
            expiration,
            updated: new Date().toISOString(),
            codeVersion
          },
          { merge: true }
        );
        result.updated = true;
      } catch (e) {
        result.updated = false;
      }
    });
  } catch (e) {
    console.error(e);
    result.errors = result.errors ? [...result.errors, e] : [e];
  }

  return result;
}

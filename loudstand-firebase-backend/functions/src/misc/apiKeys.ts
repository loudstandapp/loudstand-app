import { config } from "firebase-functions";

function getSportRadarConfig() {
  const { apikey, api3_prefix, api4_prefix } = config().sportradar;
  if (!apikey) {
    throw new Error("No API key sportradar.apikey");
  }
  if (!api3_prefix) {
    throw new Error("No sportradar.api3_prefix");
  }
  if (!api4_prefix) {
    throw new Error("No sportradar.api4_prefix");
  }
  return { apikey, api3_prefix, api4_prefix };
}

function getContentApiKey() {
  const { apikey } = config().content;
  if (!apikey) {
    throw new Error("No content.apikey");
  }
  return apikey;
}

function getCloudMessagingServerKey() {
  const { serverkey } = config().cloudmessaging;
  if (!serverkey) {
    throw new Error("No cloudmessaging.serverkey");
  }
  return serverkey;
}

const sportRadarConfig = getSportRadarConfig();

export const sportRadarApiKey = sportRadarConfig.apikey;
export const sportRadarApi3Prefix = sportRadarConfig.api3_prefix;
export const sportRadarApi4Prefix = sportRadarConfig.api4_prefix;
export const contentApiKey = getContentApiKey();
export const cloudMessagingServerKey = getCloudMessagingServerKey();

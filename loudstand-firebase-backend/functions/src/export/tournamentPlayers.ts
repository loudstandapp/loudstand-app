import { Request, Response } from "express";
import { firestore } from "firebase-admin";
import { getDocumentBody } from "../misc/utils";
import {
  TournamentRoot,
  TeamList,
  PlayerList,
  TeamRoot,
  PlayerBasic
} from "../../submodules/loudstand-shared";
import { startImport } from "../misc/importAction";
import { seasonTeams } from "../import/seasonTeams";
import { teamPlayers } from "../import/teamPlayers";

export async function exportTournamentPlayers(req: Request, res: Response) {
  const matches = req.url.match(/^\/(.+)$/);
  if (!matches) {
    return res.status(400).send("No tournament ID given");
  }
  const id = matches[1];
  const db = firestore();
  const tournamentRef = db.doc(`tournaments/${id}`);
  const tournament: TournamentRoot | null = await getDocumentBody(
    tournamentRef
  );
  if (!tournament) {
    return res.status(400).send("Tournament not in database.");
  }
  if (!tournament.isDefaultForTeams) {
    return res.status(405).send("Tournament is not default");
  }
  const teamsImportResult = await startImport(
    tournament.currentSeason.id,
    seasonTeams
  );
  if (!teamsImportResult) {
    return res.status(500).send("Cannot import teams for season.");
  }
  const seasonTeamsRef = db.doc(
    `seasons/${tournament.currentSeason.id}/attributes/teams`
  );
  const teamsForSeason: TeamList | null = await getDocumentBody(seasonTeamsRef);
  if (!teamsForSeason) {
    return res.status(500).send("No teams in season.");
  }

  const playersByTeam: { [TeamId: string]: PlayerBasic[] } = {};
  for (const teamId in teamsForSeason) {
    const teamRootRef = db.doc(`teams/${teamId}`);
    const teamRoot: TeamRoot | null = await getDocumentBody(teamRootRef);
    if (!teamRoot) {
      return res.status(500).send(`No root document for ${teamId}.`);
    }

    const playersImportResult = await startImport(teamId, teamPlayers);
    if (playersImportResult === false) {
      return res.status(500).send(`Cannot import players for ${teamId}.`);
    }
    const teamPlayersRef = db.doc(`teams/${teamId}/attributes/players`);
    const players: PlayerList | null = await getDocumentBody(teamPlayersRef);
    if (!players) {
      return res.status(500).send(`No players in ${teamId}.`);
    }
    playersByTeam[teamId] = [
      ...Object.values(players),
      ...(teamRoot.manager ? [teamRoot.manager] : [])
    ];
  }

  const contentLines: string[] = Object.keys(playersByTeam).reduce(
    (acc: string[], teamId: string) => {
      const teamName = teamsForSeason[teamId].name;
      const players = playersByTeam[teamId];
      const lines = players.map(player =>
        [player.id, player.name, teamId, teamName].join("\t")
      );
      return [...acc, ...lines];
    },
    []
  );

  const header = `# Exported ${new Date().toISOString()} Tournament: ${id} Season: ${
    tournament.currentSeason.id
  }`;
  const fields = ["playerId", "playerName", "teamId", "teamName"].join("\t");
  const content = [header, fields, ...contentLines].join("\n");
  const cleanId = id.replace(":", "-");
  res.set({
    "Content-Type": "application/octet-stream",
    "Content-Disposition": `attachment; filename="${cleanId}_players.tsv"`
  });
  return res.send(new Buffer(content));
}

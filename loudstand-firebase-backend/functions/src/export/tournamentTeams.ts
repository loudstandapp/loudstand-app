import { Request, Response } from "express";
import { firestore } from "firebase-admin";
import { getDocumentBody } from "../misc/utils";
import { TournamentRoot, TeamList } from "../../submodules/loudstand-shared";
import { startImport } from "../misc/importAction";
import { seasonTeams } from "../import/seasonTeams";

export async function exportTournamentTeams(req: Request, res: Response) {
  const matches = req.url.match(/^\/(.+)$/);
  if (!matches) {
    return res.status(400).send("No tournament ID given");
  }
  const id = matches[1];
  const db = firestore();
  const tournamentRef = db.doc(`tournaments/${id}`);
  const tournament: TournamentRoot | null = await getDocumentBody(
    tournamentRef
  );
  if (!tournament) {
    return res.status(400).send("Tournament not in database.");
  }
  if (!tournament.isDefaultForTeams) {
    return res.status(405).send("Tournament is not default");
  }
  const teamsImportResult = await startImport(
    tournament.currentSeason.id,
    seasonTeams
  );
  if (!teamsImportResult) {
    return res.status(500).send("Cannot import teams for season.");
  }
  const seasonTeamsRef = db.doc(
    `seasons/${tournament.currentSeason.id}/attributes/teams`
  );
  const teamsForSeason: TeamList | null = await getDocumentBody(seasonTeamsRef);
  if (!teamsForSeason) {
    return res.status(500).send("No teams in season.");
  }
  const contentLines = Object.keys(teamsForSeason).map(teamId =>
    [teamId, teamsForSeason[teamId].name].join("\t")
  );
  const header = `# Exported ${new Date().toISOString()} Tournament: ${id} Season: ${
    tournament.currentSeason.id
  }`;
  const fields = ["teamId", "teamName"].join("\t");
  const content = [header, fields, ...contentLines].join("\n");
  const cleanId = id.replace(":", "-");
  res.set({
    "Content-Type": "application/octet-stream",
    "Content-Disposition": `attachment; filename="${cleanId}_teams.tsv"`
  });
  return res.send(new Buffer(content));
}

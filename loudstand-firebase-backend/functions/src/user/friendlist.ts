import {
  ID,
  UpdateFriendsListRequest,
  UserFriendList,
  UserItem,
  getFavoriteTeams,
  FavoriteTeams,
  getChangedFacebookFriends
} from "../../submodules/loudstand-shared";
import { https } from "firebase-functions";
import { firestore } from "firebase-admin";

async function getFriendsToAdd(friendIds: ID[], db: firestore.Firestore) {
  const usersRef = db.collection("users");
  const newFriendsArray: (UserItem | null)[] = await Promise.all(
    friendIds.map(async facebookUid => {
      const userQuery = usersRef.where("facebookUid", "==", facebookUid);
      const querySnap = await userQuery.get();
      if (querySnap.empty) {
        return null;
      }
      const docSnap = querySnap.docs[0];
      const firebaseUid = docSnap.id;
      const userProfileData = docSnap.data();
      const { displayName, avatarUrl } = userProfileData;
      const favoritesRef = db
        .collection("users")
        .doc(firebaseUid)
        .collection("attributes")
        .doc("favoriteTeams");
      const favoritesSnap = await favoritesRef.get();
      const favoritesData = favoritesSnap.data() as FavoriteTeams | undefined;
      const favoriteTeams = favoritesData
        ? getFavoriteTeams(favoritesData)
        : {};
      return {
        displayName,
        facebookUid,
        uid: firebaseUid,
        photoUrl: avatarUrl,
        favoriteTeams
      };
    })
  );
  const newFriends = newFriendsArray.reduce((acc, current) => {
    if (!current) {
      return acc;
    }
    return {
      ...acc,
      [current.uid]: current
    };
  }, {});
  return newFriends;
}

export async function updateFriendList(
  request: UpdateFriendsListRequest,
  context: https.CallableContext
) {
  if (!request.friendIds.facebook || !context.auth) {
    return;
  }
  console.log({ request, context }); // FIXME REMOVE
  const db = firestore();
  const { uid } = context.auth;
  const { friendIds, fullRefresh } = request;
  const friendListRef = db
    .collection("users")
    .doc(uid)
    .collection("attributes")
    .doc("friends");
  const friendListSnap = await friendListRef.get();
  const friendList: UserFriendList = (friendListSnap.data() as UserFriendList) || {
    friends: {}
  };
  const facebookFriendIds = friendIds.facebook;

  if (fullRefresh) {
    const friends = await getFriendsToAdd(facebookFriendIds, db);
    await friendListRef.set({ friends });
    return { friends };
  }

  const { removeUids, addFacebookUids } = getChangedFacebookFriends(
    facebookFriendIds,
    friendList
  );

  const newFriends = await getFriendsToAdd(addFacebookUids, db);
  const filteredExistingFriends = Object.keys(friendList.friends).reduce(
    (acc, current) => {
      if (removeUids.includes(current)) {
        return acc;
      }
      return {
        ...acc,
        [current]: friendList.friends[current]
      };
    },
    {}
  );
  const updatedFriends = {
    ...filteredExistingFriends,
    ...newFriends
  };

  await friendListRef.set({ friends: updatedFriends });
  return { removeUids, addFacebookUids, newFriends };
}

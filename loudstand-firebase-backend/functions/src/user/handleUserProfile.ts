import { Change, EventContext } from "firebase-functions";
import { DocumentSnapshot } from "@google-cloud/firestore";
import {
  UserProfile,
  UserProfileProvider,
  ID
} from "../../submodules/loudstand-shared";
import { updateNotificationSettingsDevices } from "../notifications/settingsHandler";

function getFacebookUid(providerData?: UserProfileProvider[]): ID | null {
  if (!providerData) {
    return null;
  }
  const faceBookProvider = providerData.find(
    provider => provider.providerId === "facebook.com"
  );
  if (!faceBookProvider) {
    return null;
  }
  const { uid } = faceBookProvider;
  return uid || null;
}

export const handleUserProfile = {
  path: "users/{userId}",
  onWrite: async (change: Change<DocumentSnapshot>, context: EventContext) => {
    const prev = change.before.data() as UserProfile | undefined;
    const current = change.after.data() as UserProfile | undefined;
    const prevFacebookUid = prev && getFacebookUid(prev.providerData);
    const currentFacebookUid = current && getFacebookUid(current.providerData);

    const currentDevices = current && current.devices;
    const prevDevices = prev && prev.devices;
    // Deep compare list of user devices
    if (JSON.stringify(currentDevices) !== JSON.stringify(prevDevices)) {
      const { userId } = context.params;
      console.log("user profile devices updated", { userId, currentDevices });
      await updateNotificationSettingsDevices(userId, currentDevices || {});
    }
    if (prevFacebookUid !== currentFacebookUid) {
      const facebookUid = currentFacebookUid || null;
      await change.after.ref.update({ facebookUid });
    }
  }
};

import { firestore } from "firebase-admin";
import { Message } from "firebase-functions/lib/providers/pubsub";
import {
  ImportTriggerReason,
  CalendarItemByDay,
  MatchListItem,
  ISO8601Date,
  ID
} from "../../submodules/loudstand-shared";
import { triggerImportAction } from "../trigger/triggerImport";
import { getDocumentBody, cleanDate } from "../misc/utils";

export interface AutoImportMatchCalendarArgs {
  teamId: ID;
  preloadMatchesForDays: number;
}

export const autoImportMatchCalendarHandler = {
  topic: "triggerImportMatchCalendarForTeam",
  onPublish: async (message: Message) => {
    if (!message.json || !message.json.teamId) {
      throw new Error("autoImportMatchCalendarHandler: No teamId");
    }
    const {
      teamId,
      preloadMatchesForDays
    } = message.json as AutoImportMatchCalendarArgs;
    console.log({ teamId });
    const path = `teams/${teamId}/attributes/matchCalendar`;
    const calendarResult = await triggerImportAction({
      reason: ImportTriggerReason.Automatic,
      path
    });
    if (preloadMatchesForDays && preloadMatchesForDays > 0) {
      const db = firestore();
      const docRef = db.doc(path);
      const calendar = await getDocumentBody<CalendarItemByDay<MatchListItem>>(
        docRef
      );
      if (!calendar) {
        return { calendarResult };
      }
      const today = cleanDate(new Date().toISOString());
      const endDate = cleanDate(
        new Date(
          Date.now() + 1000 * 60 * 60 * 24 * preloadMatchesForDays
        ).toISOString()
      );
      const validDates = Object.keys(calendar).filter(
        date => date >= today && date <= endDate
      );
      const matchIds = validDates.reduce((acc: ID[], date: ISO8601Date) => {
        return [...acc, ...Object.keys(calendar[date])];
      }, []);
      console.log({ matchIds });

      const promises = matchIds.map(id =>
        triggerImportAction({
          path: `matches/${id}`,
          reason: ImportTriggerReason.Automatic
        })
      );
      const matchesResults = await Promise.all(promises);
      console.log({ matchesResults, calendarResult });
      return { calendarResult, matchesResults };
    } else {
      return { calendarResult };
    }
  }
};

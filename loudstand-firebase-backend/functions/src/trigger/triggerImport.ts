import {
  ImportTrigger,
  ImportTriggerRequest,
  TournamentRoot,
  TeamRoot,
  ImportTriggerResponse
} from "../../submodules/loudstand-shared";
import { matchRoot } from "../import/matchRoot";
import { startImport } from "../misc/importAction";
import { DataImporter, MassUpdater, ResultGenerator } from "../misc/models";
import { matchTimeline } from "../import/matchTimeline";
import { seasonStandings } from "../import/seasonStandings";
import { matchLineup } from "../import/matchLineup";
import { matchTeamStatistics } from "../import/matchTeamStatistics";
import { seasonPlayerStandings } from "../import/seasonPlayerStandings";
import { seasonMatches } from "../import/seasonMatches";
import { seasonTeams } from "../import/seasonTeams";
import { firestore } from "firebase-admin";
import { massUpdate } from "../misc/massUpdateAction";
import { teamRoot } from "../import/teamRoot";
import { playerRoot } from "../import/playerRoot";
import { teamPlayers } from "../import/teamPlayers";
import { localizedTeams } from "../import/localizedTeams";
import { matchActivePlayers } from "../import/matchActivePlayers";
import { matchTeamPerformance } from "../results/matchTeamPerformance";
import { generateFakeResult } from "../misc/fakeResultAction";
import { matchManagerPerformance } from "../results/matchManagerPerformance";
import { matchLineupSuggestion } from "../results/matchLineupSuggestion";
import { matchMostValuablePlayers } from "../results/matchMostValuablePlayers";
import { matchMatchEventResults } from "../results/matchEventResults";
import { userTotalReactions } from "../results/userTotalReactions";
import { userMostValuablePlayers } from "../results/userMostValuablePlayers";
import { userLineupSuggestion } from "../results/userLineupSuggestion";
import { userTeamPerformance } from "../results/userTeamPerformance";
import { userManagerPerformance } from "../results/userManagerPerformance";
import { questionResult } from "../results/questionResult";
import { tournamentRoot } from "../import/tournamentRoot";
import { teamMatchCalendar } from "../import/teamMatchCalendar";
import { teamLineupSuggestion } from "../results/teamLineupSuggestion";
import { teamMostValuablePlayers } from "../results/teamMostValuablePlayers";
import { seasonResults } from "../import/seasonResults";
import { teamSeasonStatistics } from "../import/teamSeasonStatistics";
// import { seasonTeamMatches } from "../import/seasonTeamMatches";
// import { massUpdate } from "../misc/massUpdateAction";

interface MatchImporterDefinition<A> {
  pattern: RegExp;
  importer?: DataImporter<A, any, any, any>;
  massUpdater?: MassUpdater<A, any, any, any>;
  fakeResultGenerator?: ResultGenerator<A, any, any, any>;
  makeArg?: (matches: string[]) => Promise<A | undefined>;
}

const matchImporters: MatchImporterDefinition<any>[] = [
  {
    pattern: /^matches\/([^\/]+)\/teamPerformance\/([^\/]+)$/,
    fakeResultGenerator: matchTeamPerformance,
    makeArg: async matches => ({
      matchId: matches[1],
      teamId: matches[2]
    })
  },
  {
    pattern: /^users\/([^\/]+)\/teamPerformance\/([^\/]+)$/,
    fakeResultGenerator: userTeamPerformance,
    makeArg: async matches => ({
      userId: matches[1],
      teamId: matches[2]
    })
  },
  {
    pattern: /^matches\/([^\/]+)\/managerPerformance\/([^\/]+)$/,
    fakeResultGenerator: matchManagerPerformance,
    makeArg: async matches => ({
      matchId: matches[1],
      teamId: matches[2]
    })
  },
  {
    pattern: /^users\/([^\/]+)\/managerPerformance\/([^\/]+)$/,
    fakeResultGenerator: userManagerPerformance,
    makeArg: async matches => ({
      userId: matches[1],
      teamId: matches[2]
    })
  },
  {
    pattern: /^matches\/([^\/]+)\/suggestedLineup\/([^\/]+)$/,
    fakeResultGenerator: matchLineupSuggestion,
    makeArg: async matches => ({
      matchId: matches[1],
      teamId: matches[2]
    })
  },
  {
    pattern: /^users\/([^\/]+)\/suggestedLineup\/([^\/]+)$/,
    fakeResultGenerator: userLineupSuggestion,
    makeArg: async matches => ({
      userId: matches[1],
      teamId: matches[2]
    })
  },
  {
    pattern: /^teams\/([^\/]+)\/statistics\/suggestedLineup$/,
    fakeResultGenerator: teamLineupSuggestion
  },

  {
    pattern: /^matches\/([^\/]+)\/mostValuablePlayers\/([^\/]+)$/,
    fakeResultGenerator: matchMostValuablePlayers,
    makeArg: async matches => ({
      matchId: matches[1],
      teamId: matches[2]
    })
  },
  {
    pattern: /^users\/([^\/]+)\/mostValuablePlayers\/([^\/]+)$/,
    fakeResultGenerator: userMostValuablePlayers,
    makeArg: async matches => ({
      userId: matches[1],
      teamId: matches[2]
    })
  },
  {
    pattern: /^teams\/([^\/]+)\/statistics\/mostValuablePlayers$/,
    fakeResultGenerator: teamMostValuablePlayers
  },
  {
    pattern: /^matches\/([^\/]+)\/matchEventResults\/([^\/]+)$/,
    fakeResultGenerator: matchMatchEventResults,
    makeArg: async matches => ({
      matchId: matches[1],
      teamId: matches[2]
    })
  },
  {
    pattern: /^users\/([^\/]+)\/statistics\/reactions$/,
    fakeResultGenerator: userTotalReactions
  },
  {
    pattern: /^questions\/([^\/]+)\/statistics\/result$/,
    fakeResultGenerator: questionResult
  },
  { pattern: /^matches\/([^\/]+)$/, importer: matchRoot },
  {
    pattern: /^matches\/([^\/]+)\/attributes\/timeline$/,
    importer: matchTimeline
  },
  {
    pattern: /^matches\/([^\/]+)\/attributes\/lineups$/,
    importer: matchLineup
  },
  {
    pattern: /^matches\/([^\/]+)\/attributes\/activePlayers$/,
    importer: matchActivePlayers
  },
  {
    pattern: /^matches\/([^\/]+)\/attributes\/teamStatistics$/,
    importer: matchTeamStatistics
  },
  {
    pattern: /^seasons\/([^\/]+)\/attributes\/standings$/,
    importer: seasonStandings
  },
  {
    pattern: /^seasons\/([^\/]+)\/attributes\/matchCalendar$/,
    importer: seasonMatches
  },
  {
    pattern: /^seasons\/([^\/]+)\/attributes\/results$/,
    importer: seasonResults
  },
  {
    pattern: /^seasons\/([^\/]+)\/attributes\/playerStandings$/,
    importer: seasonPlayerStandings
  },
  {
    pattern: /^seasons\/([^\/]+)\/attributes\/teams$/,
    importer: seasonTeams
  },
  {
    pattern: /^teams\/([^\/]+)\/attributes\/seasonStatistics$/,
    importer: teamSeasonStatistics
  },
  {
    pattern: /^teams\/([^\/]+)$/,
    importer: teamRoot,
    makeArg: async matches => {
      // LOAD current to get the right args for reimporting
      const db = firestore();
      const id = matches[1];
      if (!id) {
        return undefined;
      }
      const teamRef = db.collection("teams").doc(id);
      const teamSnap = await teamRef.get();
      const teamData = teamSnap.data();
      if (!teamData || !teamData.body) {
        return {
          id,
          defaultTournamentId: null,
          defaultCurrentSeasonId: null
        };
      }
      const { defaultTournamentId } = teamData.body as TeamRoot;
      if (!defaultTournamentId) {
        return {
          id,
          defaultTournamentId: null,
          defaultCurrentSeasonId: null
        };
      }
      const tournamentRef = db
        .collection("tournaments")
        .doc(defaultTournamentId);
      const tournamentSnap = await tournamentRef.get();
      const tournamentData = tournamentSnap.data();
      if (!tournamentData || !tournamentData.body) {
        return undefined;
      }
      const {
        currentSeason: { id: defaultCurrentSeasonId }
      } = tournamentData.body as TournamentRoot;

      return {
        id,
        defaultTournamentId,
        defaultCurrentSeasonId
      };
    }
  },
  {
    pattern: /^tournaments\/([^\/]+)$/,
    importer: tournamentRoot,
    makeArg: async matches => {
      // LOAD current to get the right args for reimporting
      const db = firestore();
      const id = matches[1];
      if (!id) {
        return undefined;
      }
      const ref = db.collection("tournaments").doc(id);
      const snap = await ref.get();
      const data = snap.data();
      if (!data || !data.body) {
        return undefined;
      }
      const { isDefaultForTeams } = data.body as TournamentRoot;
      return {
        id,
        isDefaultForTeams
      };
    }
  },
  {
    pattern: /^teams\/([^\/]+)\/attributes\/players$/,
    importer: teamPlayers
  },
  {
    pattern: /^players\/([^\/]+)$/,
    importer: playerRoot
  },
  {
    pattern: /^localizations\/teams\/languages\/([^\/]+)$/,
    importer: localizedTeams
  },
  {
    pattern: /^teams\/([^\/]+)\/attributes\/matchCalendar$/,
    importer: teamMatchCalendar
    // massUpdater: seasonTeamMatches,
    // makeArg: async matches => {
    //   const db = firestore();
    //   // @ts-ignore
    //   const [path, teamId] = matches;
    //   const ref = db.collection("teams").doc(teamId);
    //   const snap = await ref.get();
    //   const data = snap.data();
    //   if (!data || !data.body) {
    //     return undefined;
    //   }
    //   const { defaultCurrentSeasonId } = data.body;
    //   return defaultCurrentSeasonId;
    // }
  }
  // TODO: Handle lists if items
];

interface MatchImporterResult<A> {
  arg: A;
  importer?: DataImporter<A, any, any>;
  massUpdater?: MassUpdater<A, any, any, any>;
  fakeResultGenerator?: ResultGenerator<A, any, any, any>;
}

async function findMatch<A>(
  path: string
): Promise<MatchImporterResult<A> | undefined> {
  for (const {
    pattern,
    importer,
    makeArg,
    massUpdater,
    fakeResultGenerator
  } of matchImporters) {
    const match = path.match(pattern);
    if (!match) {
      continue;
    }
    if (makeArg) {
      const arg = await makeArg(match);
      if (!arg) {
        throw new Error(
          `makeArg returned undefined for ${JSON.stringify({
            importer: importer && importer.key,
            massUpdater: !!massUpdater,
            fakeResultGenerator: fakeResultGenerator && fakeResultGenerator.key,
            match
          })}`
        );
      }
      return { arg, importer, massUpdater, fakeResultGenerator };
    } else {
      const arg = (match[1] as unknown) as A;
      return { arg, importer, massUpdater, fakeResultGenerator };
    }
  }

  return undefined;
}

export const triggerImportAction: ImportTrigger = async (
  request: ImportTriggerRequest
): Promise<ImportTriggerResponse> => {
  const { path, reason } = request;
  let match: MatchImporterResult<any> | undefined;
  try {
    match = await findMatch(path);
  } catch (error) {
    return { errors: [error.message] };
  }
  if (!match) {
    return { errors: ["No importer"] };
  }
  const { importer, massUpdater, fakeResultGenerator, arg } = match;
  if (importer) {
    const result = await startImport(arg, importer);
    console.log("trigger importer", {
      importer: importer.key,
      arg,
      reason,
      result
    });
    return {
      ...result,
      errors: result.errors && result.errors.map(e => e.message)
    };
  }
  if (massUpdater) {
    const result = await massUpdate(arg, massUpdater);
    console.log("trigger mass updater", {
      massUpdater,
      arg,
      reason,
      result
    });
    return {
      ...result,
      errors: result.errors && result.errors.map(e => e.message)
    };
  }
  if (fakeResultGenerator) {
    const result = await generateFakeResult(arg, fakeResultGenerator);
    console.log("trigger fake result generator", {
      massUpdater,
      arg,
      reason,
      result
    });
    return {
      ...result,
      errors: result.errors && result.errors.map(e => e.message)
    };
  }
  return { errors: ["No valid importer defined"] };
};

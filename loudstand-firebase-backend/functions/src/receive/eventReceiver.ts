import { firestore } from "firebase-admin";
import { Request, Response } from "express";
import { receiveUpdate } from "../misc/updateAction";
import { matchTimeline } from "../import/matchTimeline";
import { matchRoot } from "../import/matchRoot";
import { seasonResults } from "../import/seasonResults";
import { triggerEventNotifications } from "../notifications/matchEventHandler";
import { DocumentSnapshot } from "firebase-functions/lib/providers/firestore";
import { EventContext } from "firebase-functions";

export async function eventReceiver(req: Request, res: Response) {
  const event = req.body;
  console.log("EVENT", JSON.stringify(event));
  const results: any = {};
  const db = firestore();
  try {
    await db
      .collection("receivedEvents")
      .doc(event.payload.event.id.toString())
      .set(event);

    const matchTimelineResult = await receiveUpdate(event, matchTimeline);
    results.matchTimelineResult = matchTimelineResult;
    const matchRootResult = await receiveUpdate(event, matchRoot);
    results.matchRootResult = matchRootResult;
    const seasonResultsResult = await receiveUpdate(event, seasonResults);
    results.seasonResultsResult = seasonResultsResult;
    res.send({ status: "ok", results });
  } catch (error) {
    console.error(error);
    res.status(500);
    res.send(error.message);
  }
}

export const handleEvent = {
  path: "receivedEvents/{eventId}",
  onCreate: async (snapshot: DocumentSnapshot, context: EventContext) => {
    const rawEvent = snapshot.data();
    if (!rawEvent) {
      throw new Error("empty data");
    }
    const notificationsResult = await triggerEventNotifications(rawEvent);
    return { notificationsResult };
  }
};

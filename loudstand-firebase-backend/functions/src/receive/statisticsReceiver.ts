import { firestore } from "firebase-admin";
import { Request, Response } from "express";
import { receiveUpdate } from "../misc/updateAction";
import { matchTeamStatistics } from "../import/matchTeamStatistics";

export async function statisticsReceiver(req: Request, res: Response) {
  const statistics = req.body;
  const results: any = {};

  const db = firestore();
  try {
    await db
      .collection("receivedStatistics")
      .doc(new Date().toISOString())
      .set(statistics);

    const matchTimelineResult = await receiveUpdate(
      statistics,
      matchTeamStatistics
    );
    results.matchTimelineResult = matchTimelineResult;
    res.send({ status: "ok", results });
  } catch (error) {
    console.error(error);
    res.status(500);
    res.send(error);
  }
}

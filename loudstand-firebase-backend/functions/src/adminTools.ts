import { firestore } from "firebase-admin";
import { splitIntoChunks } from "./misc/utils";

export async function cleanAllQuestionResults() {
  try {
    const db = firestore();
    const questionsRef = db.collection("questions");
    const allQuestions = await questionsRef.get();
    await db.runTransaction(async t => {
      allQuestions.docs.forEach(doc => {
        const ref = questionsRef
          .doc(doc.id)
          .collection("statistics")
          .doc("result");
        t.delete(ref);
      });
    });
    const teamsRef = db.collection("teams");
    const allTeams = await teamsRef.get();
    await db.runTransaction(async t => {
      allTeams.docs.forEach(doc => {
        const ref = teamsRef
          .doc(doc.id)
          .collection("statistics")
          .doc("questionResults");
        t.delete(ref);
      });
    });
    return { questions: allQuestions.docs.length, teams: allTeams.docs.length };
  } catch (e) {
    console.error(e);
    return false;
  }
}

export async function cleanAllImportedMatchAttributes() {
  try {
    const db = firestore();
    const matchesRef = db.collection("matches");
    const allMatches = await matchesRef.get();
    const chunks = splitIntoChunks(allMatches.docs, 100);
    for (const chunk of chunks) {
      await db.runTransaction(async t => {
        chunk.forEach(doc => {
          const attributesCollection = matchesRef
            .doc(doc.id)
            .collection("attributes");
          const lineupRef = attributesCollection.doc("lineups");
          t.delete(lineupRef);
          const teamStatisticsRef = attributesCollection.doc("teamStatistics");
          t.delete(teamStatisticsRef);
          const activePlayersRef = attributesCollection.doc("activePlayers");
          t.delete(activePlayersRef);
          const timelineRef = attributesCollection.doc("timeline");
          t.delete(timelineRef);
        });
      });
    }
    return { questions: allMatches.docs.length };
  } catch (e) {
    console.error(e);
    return false;
  }
}

import { EventContext } from "firebase-functions";
import { DocumentSnapshot } from "@google-cloud/firestore";
import {
  FinalSubmittedVote,
  VoteType
} from "../../submodules/loudstand-shared";
import { registerInstantResult } from "../misc/registerInstantResult";
import { scheduleVoteResults } from "../misc/scheduleResults";
import {
  ResultGenerationType,
  StatisticsResultType,
  ResultGenerator,
  CustomJobType
} from "../misc/models";

import { matchMatchEventResults } from "../results/matchEventResults";
import { matchTeamPerformance } from "../results/matchTeamPerformance";
import { matchManagerPerformance } from "../results/matchManagerPerformance";
import { matchMostValuablePlayers } from "../results/matchMostValuablePlayers";
import { matchLineupSuggestion } from "../results/matchLineupSuggestion";
import { userTotalReactions } from "../results/userTotalReactions";
import { userLineupSuggestion } from "../results/userLineupSuggestion";
import { userTeamPerformance } from "../results/userTeamPerformance";
import { userManagerPerformance } from "../results/userManagerPerformance";
import { userMostValuablePlayers } from "../results/userMostValuablePlayers";
import { updateUserVoteHistory } from "../misc/userVoteHistory";
import { exportVote } from "../misc/bigQuery";
import { teamMostValuablePlayers } from "../results/teamMostValuablePlayers";
import { managerSubstitutionApproval } from "../results/managerSubstitutionApproval";
import { teamLineupSuggestion } from "../results/teamLineupSuggestion";
import { questionResult } from "../results/questionResult";
import { addVoteToActivityFeed } from "../misc/updateActivityFeed";
import {
  getHandlerForCustomJobType,
  scheduleCustomJobFromVote
} from "../misc/customJob";

export const resultTypesFromVoteType: {
  [key in VoteType]: StatisticsResultType[];
} = {
  [VoteType.Lineup]: [
    StatisticsResultType.MatchLineupSuggestion,
    StatisticsResultType.TeamLineupSuggestion,
    StatisticsResultType.UserLineupSuggestion,
    StatisticsResultType.UserTotalReactions
  ],
  [VoteType.ManagerPerformance]: [
    StatisticsResultType.MatchManagerPerformance,
    StatisticsResultType.UserManagerPerformance,
    StatisticsResultType.UserTotalReactions
  ],
  [VoteType.TeamPerformance]: [
    StatisticsResultType.MatchTeamPerformance,
    StatisticsResultType.UserTeamPerformance,
    StatisticsResultType.UserTotalReactions
  ],
  [VoteType.MostValuablePlayer]: [
    StatisticsResultType.MatchMostValuablePlayers,
    StatisticsResultType.TeamMostValuablePlayers,
    StatisticsResultType.UserMostValuablePlayers,
    StatisticsResultType.UserTotalReactions
  ],
  [VoteType.Substitution]: [
    StatisticsResultType.ManagerSubstitutionApproval,
    StatisticsResultType.MatchEventResults,
    StatisticsResultType.UserTotalReactions
  ],
  [VoteType.RedCard]: [
    StatisticsResultType.MatchEventResults,
    StatisticsResultType.UserTotalReactions
  ],
  [VoteType.YellowCard]: [
    StatisticsResultType.MatchEventResults,
    StatisticsResultType.UserTotalReactions
  ],
  [VoteType.YellowRedCard]: [
    StatisticsResultType.MatchEventResults,
    StatisticsResultType.UserTotalReactions
  ],
  [VoteType.Penalty]: [
    StatisticsResultType.MatchEventResults,
    StatisticsResultType.UserTotalReactions
  ],
  [VoteType.Question]: [
    StatisticsResultType.UserTotalReactions,
    StatisticsResultType.Question
  ]
};

export const customJobsFromVoteType: { [key in VoteType]: CustomJobType[] } = {
  [VoteType.Lineup]: [
    CustomJobType.SeasonLineupPlayerShare,
    CustomJobType.SeasonPlayerPopularity
  ],
  [VoteType.ManagerPerformance]: [CustomJobType.SeasonManagerPopularity],
  [VoteType.TeamPerformance]: [CustomJobType.SeasonTeamPopularity],
  [VoteType.MostValuablePlayer]: [
    CustomJobType.SeasonMostValuablePlayerShare,
    CustomJobType.SeasonPlayerPopularity
  ],
  [VoteType.Substitution]: [],
  [VoteType.RedCard]: [],
  [VoteType.YellowCard]: [],
  [VoteType.YellowRedCard]: [],
  [VoteType.Penalty]: [],
  [VoteType.Question]: []
};

export function getGeneratorForResultType(resultType: StatisticsResultType) {
  const resultGeneratorFromResultType: {
    [key in StatisticsResultType]: ResultGenerator<any, any, any, any>;
  } = {
    [StatisticsResultType.ManagerSubstitutionApproval]: managerSubstitutionApproval,
    [StatisticsResultType.MatchEventResults]: matchMatchEventResults,
    [StatisticsResultType.MatchLineupSuggestion]: matchLineupSuggestion,
    [StatisticsResultType.MatchTeamPerformance]: matchTeamPerformance,
    [StatisticsResultType.MatchManagerPerformance]: matchManagerPerformance,
    [StatisticsResultType.MatchMostValuablePlayers]: matchMostValuablePlayers,
    [StatisticsResultType.Question]: questionResult,
    [StatisticsResultType.TeamLineupSuggestion]: teamLineupSuggestion,
    [StatisticsResultType.TeamMostValuablePlayers]: teamMostValuablePlayers,
    [StatisticsResultType.UserLineupSuggestion]: userLineupSuggestion,
    [StatisticsResultType.UserTotalReactions]: userTotalReactions,
    [StatisticsResultType.UserTeamPerformance]: userTeamPerformance,
    [StatisticsResultType.UserManagerPerformance]: userManagerPerformance,
    [StatisticsResultType.UserMostValuablePlayers]: userMostValuablePlayers
  };
  return resultGeneratorFromResultType[resultType];
}

export async function dispatchVote(vote: FinalSubmittedVote) {
  const resultTypes = resultTypesFromVoteType[vote.type];
  for (const resultType of resultTypes) {
    const resultGenerator = getGeneratorForResultType(resultType);
    switch (resultGenerator.type) {
      case ResultGenerationType.Instant: {
        await registerInstantResult(vote, resultGenerator);
        break;
      }
      case ResultGenerationType.Scheduled: {
        await scheduleVoteResults(vote, resultGenerator);
        break;
      }
      case ResultGenerationType.External: {
        // do some external stuff
        // await registerVote(vote);
        break;
      }
    }
  }

  // Set up custom jobs
  const customJobTypes = customJobsFromVoteType[vote.type];
  for (const jobType of customJobTypes) {
    const handler = getHandlerForCustomJobType(jobType);
    await scheduleCustomJobFromVote(vote, handler);
  }
}

export const handleVote = {
  async onCreate(snapshot: DocumentSnapshot, context: EventContext) {
    console.log("Vote was saved", snapshot.id);
    const vote = snapshot.data() as FinalSubmittedVote | undefined;
    if (!vote) {
      throw new Error(
        `A vote was saved but cannot be found by handleVote. ID: ${snapshot.id}`
      );
    }
    // Update user vote history
    await updateUserVoteHistory(vote, vote.userId);

    // Update user activity feed
    await addVoteToActivityFeed(vote, snapshot.id);

    // Feed to bigquery
    await exportVote(vote);

    // Dispatch to result handlers
    await dispatchVote(vote);

    console.log("Vote was handled");
  },
  path: "votes/{voteId}"
};

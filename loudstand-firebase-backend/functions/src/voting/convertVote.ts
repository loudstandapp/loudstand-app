import {
  ExportedVote,
  VoteType,
  FinalSubmittedVote,
  UserMetadata
} from "../../submodules/loudstand-shared";

function convertMetaData(metadata: UserMetadata) {
  const userGeoLocation = metadata.geoLocation
    ? `POINT(${metadata.geoLocation.latitude} ${
        metadata.geoLocation.longitude
      })`
    : null;
  const { gender, country, birthDate } = metadata;
  return {
    userGeoLocation,
    userGender: gender,
    userCountry: country,
    userBirthDate: birthDate ? new Date(birthDate).toISOString() : null
  };
}

export function convertVoteForExport(vote: FinalSubmittedVote): ExportedVote {
  switch (vote.type) {
    case VoteType.Lineup: {
      const {
        teamId,
        userId,
        lineup,
        matchDate,
        matchId,
        seasonId,
        created,
        metadata,
        type
      } = vote;
      const exportedVote: ExportedVote = {
        teamId,
        userId,
        matchDate,
        matchId,
        seasonId,
        created,
        voteType: type,
        managerId: null,
        playerId: lineup,
        refereeId: null,
        questionId: null,
        answer: null,
        rating: null,
        eventId: null,
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
    case VoteType.MostValuablePlayer: {
      const {
        userId,
        created,
        metadata,
        type,
        teamId,
        matchDate,
        matchId,
        seasonId,
        playerId
      } = vote;
      const exportedVote = {
        teamId,
        userId,
        matchDate,
        matchId,
        seasonId,
        created,
        voteType: type,
        playerId: [playerId],
        managerId: null,
        refereeId: null,
        questionId: null,
        answer: null,
        rating: null,
        eventId: null,
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
    case VoteType.TeamPerformance: {
      const {
        userId,
        created,
        metadata,
        teamId,
        matchDate,
        matchId,
        seasonId,
        rating,
        type
      } = vote;
      const exportedVote = {
        teamId,
        userId,
        matchDate,
        matchId,
        seasonId,
        created,
        voteType: type,
        rating: rating,
        managerId: null,
        refereeId: null,
        questionId: null,
        playerId: [],
        answer: null,
        eventId: null,
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
    case VoteType.ManagerPerformance: {
      const {
        userId,
        created,
        metadata,
        teamId,
        matchDate,
        matchId,
        seasonId,
        rating,
        managerId,
        type
      } = vote;
      const exportedVote = {
        teamId,
        userId,
        matchDate,
        matchId,
        seasonId,
        created,
        voteType: type,
        managerId: managerId,
        rating: rating,
        refereeId: null,
        questionId: null,
        playerId: [],
        answer: null,
        eventId: null,
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
    case VoteType.Question: {
      const {
        userId,
        created,
        metadata,
        teamId,
        questionId,
        answer,
        playerId,
        type
      } = vote;
      const exportedVote = {
        teamId,
        userId,
        questionId,
        created,
        answer,
        playerId: playerId ? [playerId] : [],
        voteType: type,
        eventId: null,
        matchDate: null,
        matchId: null,
        rating: null,
        seasonId: null,
        managerId: null,
        refereeId: null,
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
    case VoteType.Substitution: {
      const {
        userId,
        created,
        metadata,
        teamId,
        managerId,
        answer,
        matchDate,
        matchId,
        seasonId,
        type,
        eventId
      } = vote;
      const exportedVote = {
        teamId,
        userId,
        created,
        answer,
        managerId,
        matchDate,
        matchId,
        seasonId,
        eventId,
        voteType: type,
        questionId: null,
        rating: null,
        refereeId: null,
        playerId: [],
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
    case VoteType.RedCard: {
      const {
        userId,
        created,
        metadata,
        teamId,
        refereeId,
        answer,
        matchDate,
        matchId,
        seasonId,
        type,
        eventId
      } = vote;
      const exportedVote = {
        teamId,
        userId,
        created,
        refereeId,
        answer,
        matchDate,
        matchId,
        seasonId,
        eventId,
        voteType: type,
        questionId: null,
        rating: null,
        playerId: [],
        managerId: null,
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
    case VoteType.YellowCard: {
      const {
        userId,
        created,
        metadata,
        teamId,
        refereeId,
        answer,
        matchDate,
        matchId,
        seasonId,
        type,
        eventId
      } = vote;
      const exportedVote = {
        teamId,
        userId,
        created,
        refereeId,
        answer,
        matchDate,
        matchId,
        seasonId,
        eventId,
        voteType: type,
        questionId: null,
        rating: null,
        playerId: [],
        managerId: null,
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
    case VoteType.YellowRedCard: {
      const {
        userId,
        created,
        metadata,
        teamId,
        refereeId,
        answer,
        matchDate,
        matchId,
        seasonId,
        type,
        eventId
      } = vote;
      const exportedVote = {
        teamId,
        userId,
        created,
        refereeId,
        answer,
        matchDate,
        matchId,
        seasonId,
        eventId,
        voteType: type,
        questionId: null,
        rating: null,
        playerId: [],
        managerId: null,
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
    case VoteType.Penalty: {
      const {
        userId,
        created,
        metadata,
        teamId,
        refereeId,
        answer,
        matchDate,
        matchId,
        seasonId,
        type,
        eventId
      } = vote;
      const exportedVote = {
        teamId,
        userId,
        created,
        refereeId,
        answer,
        matchDate,
        matchId,
        seasonId,
        eventId,
        voteType: type,
        questionId: null,
        rating: null,
        playerId: [],
        managerId: null,
        ...convertMetaData(metadata)
      };
      return exportedVote;
    }
  }
}

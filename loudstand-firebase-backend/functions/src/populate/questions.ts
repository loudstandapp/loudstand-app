import {
  RawQuestion,
  Question,
  QuestionResultDocument
} from "../../submodules/loudstand-shared/models/loudstand";
import { firestore } from "firebase-admin";
import { Change, EventContext } from "firebase-functions";
import { DocumentSnapshot } from "firebase-functions/lib/providers/firestore";

export const PUBLISH_TIME_OF_DAY_UTC = 8 * 60 * 60;
const REACTION_PERIOD = 7 * 24 * 60 * 60;

export const populateQuestion = {
  async onWrite(change: Change<DocumentSnapshot>, context: EventContext) {
    // TODO: If player id is deleted or changed, remove question from original player id
    const { questionId } = context.params;
    try {
      const oldData = change.before.data() as RawQuestion;
      const newData = change.after.data() as RawQuestion;

      const publishDateObj = new Date(newData.date);
      publishDateObj.setUTCSeconds(PUBLISH_TIME_OF_DAY_UTC);
      const publishTime = publishDateObj.toISOString();
      const reactionEndTime = new Date(
        publishDateObj.getTime() + REACTION_PERIOD * 1000
      ).toISOString();

      const question: Question = {
        ...newData,
        imageUrl: newData.imageUrl || null,
        id: questionId,
        publishTime,
        reactionEndTime
      };

      if (!question.date) {
        console.warn("Question without date has been written", change.after.id);
        return;
      }
      const db = firestore();
      // Player was changed, remove the question from old player
      if (
        oldData &&
        oldData.playerId &&
        oldData.playerId !== newData.playerId
      ) {
        await db
          .collection("players")
          .doc(oldData.playerId)
          .collection("attributes")
          .doc("questions")
          .set(
            { [question.date]: firestore.FieldValue.delete() },
            { merge: true }
          );
      }
      // Player was added or unchanged, copy the question to the player profile
      if (newData.playerId) {
        await db
          .collection("players")
          .doc(newData.playerId)
          .collection("attributes")
          .doc("questions")
          .set({ [question.date]: question }, { merge: true });
      }
      if (question.teamId) {
        await db
          .collection("teams")
          .doc(question.teamId)
          .collection("attributes")
          .doc("questions")
          .set({ [question.date]: question }, { merge: true });
      }
    } catch (e) {
      console.error("ERROR on write question", e);
    }
  },
  path: "questions/{questionId}"
};

export const populateQuestionResult = {
  async onWrite(change: Change<DocumentSnapshot>, context: EventContext) {
    try {
      const resultDoc = change.after.data() as QuestionResultDocument;
      const { questionId } = context.params;
      const db = firestore();

      const questionRef = db.collection("questions").doc(questionId);
      const questionSnap = await questionRef.get();
      const question = questionSnap.data() as RawQuestion;

      if (!question) {
        throw new Error("Question not found for generated result");
      }

      const playerRef =
        question.playerId &&
        db
          .collection("players")
          .doc(question.playerId)
          .collection("statistics")
          .doc("questionResults");

      const teamRef =
        question.teamId &&
        db
          .collection("teams")
          .doc(question.teamId)
          .collection("statistics")
          .doc("questionResults");

      if (playerRef) {
        await playerRef.set(
          { [questionId]: resultDoc.result },
          { merge: true }
        );
      }
      if (teamRef) {
        await teamRef.set({ [questionId]: resultDoc.result }, { merge: true });
      }
    } catch (e) {
      console.error("ERROR on write question", e);
    }
  },
  path: "questions/{questionId}/statistics/result"
};

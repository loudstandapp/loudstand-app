import { Change, EventContext } from "firebase-functions";
import { DocumentSnapshot, FieldValue } from "@google-cloud/firestore";
import {
  RawCalendarItem,
  CalendarItem,
  ID,
  CalendarItemType,
  ISO8601Date
} from "../../submodules/loudstand-shared";
import { firestore } from "firebase-admin";

async function writeItemToDestination(
  item: CalendarItem | FieldValue,
  teamId: ID,
  itemId: ID,
  date: ISO8601Date,
  type: CalendarItemType | null | undefined
) {
  const db = firestore();

  switch (type) {
    case CalendarItemType.Memory: {
      const ref = db
        .collection("teams")
        .doc(teamId)
        .collection("attributes")
        .doc("homeScreenMemoryCalendar");
      await ref.set({ [itemId]: item }, { merge: true });
      return;
    }
    default: {
      const ref = db
        .collection("teams")
        .doc(teamId)
        .collection("attributes")
        .doc("homeScreenItemCalendar");
      await ref.set({ [date]: { [itemId]: item } }, { merge: true });
      return;
    }
  }
  return;
}

export const populateCalendarItem = {
  async onWrite(change: Change<DocumentSnapshot>, context: EventContext) {
    const { itemId } = context.params;
    try {
      const oldData = change.before.data();
      const newData = change.after.data();

      // HANDLING A DELETED ITEM
      if (oldData && !newData) {
        const oldId = change.before.id;
        const { teamId, date, type } = oldData;
        if (!date) {
          console.error("Item without date has been deleted", oldData);
          return;
        }
        if (!teamId) {
          console.error("Item without teamId has been deleted", oldData);
          return;
        }
        await writeItemToDestination(
          firestore.FieldValue.delete(),
          teamId,
          oldId,
          date,
          type
        );
        return;
      }

      // HANDLING A CREATED ITEM
      const rawItem = newData as RawCalendarItem;

      const item: CalendarItem = {
        ...rawItem,
        id: itemId
      };

      if (!item.date) {
        console.error("Item without date has been written", change.after.id);
        return;
      }
      if (!item.teamId) {
        console.error("Item without teamId has been written", change.after.id);
        return;
      }
      await writeItemToDestination(
        item,
        item.teamId,
        itemId,
        item.date,
        item.type
      );
    } catch (e) {
      console.error("ERROR on write question", e);
    }
  },
  path: "calendarItems/{itemId}"
};

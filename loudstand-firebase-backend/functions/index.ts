import * as functions from "firebase-functions";
import { initializeApp } from "firebase-admin";
import { eventReceiver, handleEvent } from "./src/receive/eventReceiver";
import { statisticsReceiver } from "./src/receive/statisticsReceiver";
import { PubSub } from "@google-cloud/pubsub";
import {
  makeImporter,
  startImport
  // importIfNecessary
} from "./src/misc/importAction";
import { matchLineup } from "./src/import/matchLineup";
import { matchRoot } from "./src/import/matchRoot";
import { matchTimeline } from "./src/import/matchTimeline";
import { matchTeamStatistics } from "./src/import/matchTeamStatistics";
import { seasonTeams } from "./src/import/seasonTeams";
import { seasonResults } from "./src/import/seasonResults";
import { teamPlayers } from "./src/import/teamPlayers";
import { seasonStandings } from "./src/import/seasonStandings";
import { tournamentRoot } from "./src/import/tournamentRoot";
import { seasonMatches } from "./src/import/seasonMatches";
import { playerRoot } from "./src/import/playerRoot";
import { seasonPlayerStandings } from "./src/import/seasonPlayerStandings";
import { triggerImportAction } from "./src/trigger/triggerImport";
import { playerSeasonStatistics } from "./src/import/playerSeasonStatistics";
import { teamRoot } from "./src/import/teamRoot";
import { localizedTeams } from "./src/import/localizedTeams";
import {
  populateQuestion,
  populateQuestionResult
} from "./src/populate/questions";
import { matchActivePlayers } from "./src/import/matchActivePlayers";
import { handleVote, dispatchVote } from "./src/voting/handleVote";
import { runScheduledJob as runScheduledJobAction } from "./src/misc/runScheduledJob";
import { runCustomScheduledJob as runCustomScheduledJobAction } from "./src/misc/runCustomScheduledJob";

import { exportVote } from "./src/misc/bigQuery";
import { handleFavoriteTeams } from "./src/favorites/handleFavoriteTeams";
import { handleFollowingTeams } from "./src/favorites/handleFollowingTeams";
import { handleUserProfile } from "./src/user/handleUserProfile";
import { updateFriendList } from "./src/user/friendlist";
import { generateSeasonMostValuablePlayerShare } from "./src/results/seasonMostValuablePlayerShare";
import { generateSeasonLineupPlayerShare } from "./src/results/seasonLineupPlayerShare";
import { generateSeasonTeamPopularity } from "./src/results/seasonTeamPopularity";
import { generateSeasonManagerPopularity } from "./src/results/seasonManagerPopularity";
import { generateSeasonPlayerPopularity } from "./src/results/seasonPlayerPopularity";
import { teamMatchCalendar } from "./src/import/teamMatchCalendar";
import { exportTournamentTeams as exportTournamentTeamsService } from "./src/export/tournamentTeams";
import { exportTournamentPlayers as exportTournamentPlayersService } from "./src/export/tournamentPlayers";
import { questionsService } from "./src/rest-api/questionsService";
import { calenderItemsService } from "./src/rest-api/calendarItemsService";
import { populateCalendarItem } from "./src/populate/calendarItems";
import { handleMatchChange } from "./src/notifications/matchChangeHandler";
import { autoImportMatchCalendarHandler } from "./src/autoimport/autoImportMatchCalendar";
import { midnightCronJobExecutor } from "./src/misc/cronJobs";
import { teamSeasonStatistics } from "./src/import/teamSeasonStatistics";

export const REGION = "europe-west1";

initializeApp();

// Callable from firebase client

export const triggerImport = functions
  .region(REGION)
  .https.onCall(triggerImportAction);

export const updateFriendsList = functions
  .region(REGION)
  .https.onCall(updateFriendList);

// Callable by external HTTP client

export const receiveEvent = functions
  .region(REGION)
  .https.onRequest(eventReceiver);

// Handles a created event which was received
export const onCreateEvent = functions
  .region(REGION)
  .firestore.document(handleEvent.path)
  .onCreate(handleEvent.onCreate);

export const receiveStatistics = functions
  .region(REGION)
  .https.onRequest(statisticsReceiver);

// Callable internally

// Takes match ID
export const importMatchLineup = functions
  .region(REGION)
  .https.onCall(makeImporter(matchLineup));

// Takes match ID
export const importMatch = functions
  .region(REGION)
  .https.onCall(makeImporter(matchRoot));

// Takes match ID
export const importMatchTimeline = functions
  .region(REGION)
  .https.onCall(makeImporter(matchTimeline));

// Takes match ID
export const importMatchTeamStatistics = functions
  .region(REGION)
  .https.onCall(makeImporter(matchTeamStatistics));

// Takes match ID
export const importMatchActivePlayers = functions
  .region(REGION)
  .https.onCall(makeImporter(matchActivePlayers));

// Takes season ID
export const importSeasonTeams = functions
  .region(REGION)
  .https.onCall(makeImporter(seasonTeams));

// Takes season ID
export const importSeasonResults = functions
  .region(REGION)
  .https.onCall(makeImporter(seasonResults));

// Takes team ID
export const importTeamMatchCalendar = functions
  .region(REGION)
  .https.onCall(makeImporter(teamMatchCalendar));

// Takes season ID
export const importSeasonPlayerStandings = functions
  .region(REGION)
  .https.onCall(makeImporter(seasonPlayerStandings));

// Takes season ID
export const importSeasonMatches = functions
  .region(REGION)
  .https.onCall(makeImporter(seasonMatches));

// Takes player/manager ID
export const importPlayer = functions
  .region(REGION)
  .https.onCall(makeImporter(playerRoot));

// Takes player ID
export const importPlayerSeasonStatistics = functions
  .region(REGION)
  .https.onCall(makeImporter(playerSeasonStatistics));

// Takes team ID
export const importTeamPlayers = functions
  .region(REGION)
  .https.onCall(makeImporter(teamPlayers));

// Takes team ID
export const importSeasonStandings = functions
  .region(REGION)
  .https.onCall(makeImporter(seasonStandings));

// Takes season Id
export const importSeason = functions
  .region(REGION)
  .https.onCall(async seasonId => {
    const importResult = await startImport(seasonId, seasonTeams);
    // const updateResult = await massUpdate(seasonId, seasonTeamMatches);
    return importResult;
  });

// Takes team Id
export const importTeamSeasonStatistics = functions
  .region(REGION)
  .https.onCall(makeImporter(teamSeasonStatistics));

// Takes language code
export const importLocalizedTeams = functions
  .region(REGION)
  .https.onCall(makeImporter(localizedTeams));

// Takes {id: "sr:tournament:39", isDefaultForTeams: true}
export const importTournament = functions
  .region(REGION)
  .https.onCall(makeImporter(tournamentRoot));

// Takes importTeam( {id: "sr:competitor:1284", defaultCurrentSeasonId: "sr:season:55243", defaultTournamentId: "sr:tournament:238"})
export const importTeam = functions
  .region(REGION)
  .https.onCall(makeImporter(teamRoot));

// export const onCreateTeam = functions
//   .region(REGION)
//   .firestore.document("seasons/{seasonId}/attributes/teams")
//   .onCreate(async (snap, context) => {
//     const { body } = snap.data() as any;
//     if (!body) {
//       return;
//     }
//     await body.forEach(async (team: any) => {
//       await importIfNecessary(team.id, teamPlayers);
//     });
//   });

// Populates written questions to relevant locations.
export const onWriteQuestion = functions
  .region(REGION)
  .firestore.document(populateQuestion.path)
  .onWrite(populateQuestion.onWrite);

// Populates written questions results to relevant locations.
export const onWriteResult = functions
  .region(REGION)
  .firestore.document(populateQuestionResult.path)
  .onWrite(populateQuestionResult.onWrite);

// Handles a saved vote
export const onSaveVote = functions
  .region(REGION)
  .firestore.document(handleVote.path)
  .onCreate(handleVote.onCreate);

export const onUpdateFavoriteTeams = functions
  .region(REGION)
  .firestore.document(handleFavoriteTeams.path)
  .onWrite(handleFavoriteTeams.onWrite);

export const onUpdateFollowingTeams = functions
  .region(REGION)
  .firestore.document(handleFollowingTeams.path)
  .onWrite(handleFollowingTeams.onWrite);

export const onWriteUserProfile = functions
  .region(REGION)
  .firestore.document(handleUserProfile.path)
  .onWrite(handleUserProfile.onWrite);

// Runs an internal schedule result generation job
export const runScheduledJob = functions
  .region(REGION)
  .https.onCall(runScheduledJobAction);

// export const testRegisterVote = functions
//   .region(REGION)
//   .https.onCall(async (vote: FinalSubmittedVote) => {
//     await registerVote(vote);
//   });
// export const testScheduleVote = functions
//   .region(REGION)
//   .https.onCall(scheduleVoteResults);
// export const testReceiveVote = functions
//   .region(REGION)
//   .https.onCall(scheduleVoteResults);

export const testDispatchVote = functions
  .region(REGION)
  .https.onCall(dispatchVote);

export const testExportVote = functions.region(REGION).https.onCall(exportVote);

// Takes season ID
export const seasonMostValuablePlayerShare = functions
  .region(REGION)
  .https.onCall(generateSeasonMostValuablePlayerShare);

// Takes season ID
export const seasonLineupPlayerShare = functions
  .region(REGION)
  .https.onCall(generateSeasonLineupPlayerShare);

// Takes season ID
export const seasonTeamPopularity = functions
  .region(REGION)
  .https.onCall(generateSeasonTeamPopularity);

// Takes season ID
export const seasonManagerPopularity = functions
  .region(REGION)
  .https.onCall(generateSeasonManagerPopularity);

// Takes season ID
export const seasonPlayerPopularity = functions
  .region(REGION)
  .https.onCall(generateSeasonPlayerPopularity);

// Runs a custom scheduled result generation job
export const runCustomScheduledJob = functions
  .region(REGION)
  .https.onCall(runCustomScheduledJobAction);

export const exportTournamentTeams = functions
  .region(REGION)
  .https.onRequest(exportTournamentTeamsService);

export const exportTournamentPlayers = functions
  .region(REGION)
  .https.onRequest(exportTournamentPlayersService);

export const questions = functions
  .region(REGION)
  .https.onRequest(questionsService);

export const calendarItems = functions
  .region(REGION)
  .https.onRequest(calenderItemsService);

export const onWriteCalendarItem = functions
  .region(REGION)
  .firestore.document(populateCalendarItem.path)
  .onWrite(populateCalendarItem.onWrite);

export const onWriteMatchRoot = functions
  .region(REGION)
  .firestore.document(handleMatchChange.path)
  .onWrite(handleMatchChange.onWrite);

export const onWriteNotificationSettings = functions
  .region(REGION)
  .runWith({
    memory: "2GB",
    timeoutSeconds: 540
  })
  .firestore.document(handleSettingsChange.path)
  .onWrite(handleSettingsChange.onWrite);

// takes {teamId: ....,preloadMatchesForDays: 12 }
export const autoImportMatchCalendar = functions
  .region(REGION)
  .runWith({ memory: "2GB", timeoutSeconds: 540 })
  .pubsub.topic(autoImportMatchCalendarHandler.topic)
  .onPublish(autoImportMatchCalendarHandler.onPublish);

export const midnightCronJob = functions
  .region(REGION)
  .runWith({ memory: "2GB", timeoutSeconds: 540 })
  .pubsub.schedule("0 0 * * *")
  .timeZone("UTC")
  .onRun(midnightCronJobExecutor);


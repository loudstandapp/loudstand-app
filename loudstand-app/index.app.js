/* global __DEV__ */
import { Sentry } from 'react-native-sentry'
import { startApp } from './src/startApp'

!__DEV__ &&
  Sentry.config(
    'https://f9511fb440c34a96826c1a1da4472170@sentry.io/1539278'
  ).install()
startApp()

# /bin/sh
VERSION=`node -pe "require('./package.json').version"`
ENVIRONMENT=$1
IOS_APP_ID="loudstand/Loudstand-Football-Ios"
ANDROID_APP_ID="loudstand/Loudstand-Football-Android"

execute(){
  echo $VERSION
  echo $ENVIRONMENT
  # mkdir -p build
  # Push code

  # Upload sourcemaps to Sentry
  
  appcenter codepush release-react --app $ANDROID_APP_ID -m -d "$ENVIRONMENT" --description $VERSION --output-dir ./build || exit 1
  export SENTRY_PROPERTIES=./android/sentry.properties
  sentry-cli react-native appcenter $ANDROID_APP_ID android ./build/codePush || exit 1

  appcenter codepush release-react --app $IOS_APP_ID -m -d "$ENVIRONMENT" --description $VERSION --output-dir ./build || exit 1
  export SENTRY_PROPERTIES=./ios/sentry.properties
  sentry-cli react-native appcenter $IOS_APP_ID ios ./build/codePush || exit 1

  exit 0
}


if [ -z $ENVIRONMENT ]
then
  echo "ERROR. Please specify environment as first argument."
  exit 1
fi

echo "The following will be pushed:"
echo
echo "Description:  $VERSION"
echo "Environment:  $ENVIRONMENT"
echo
echo "Android:      $ANDROID_APP_ID"
echo "iOS:          $IOS_APP_ID"
echo
echo


while true; do
    read -p "Do you wish to continue? " yn
    case $yn in
        [Yy]* ) execute;;
        [Nn]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done


// Auto-generated file created by react-native-storybook-loader
// Do not edit.
//
// https://github.com/elderfo/react-native-storybook-loader.git

function loadStories() {
  require('../src/components/ActivityIndicatorLine.stories');
  require('../src/components/CountryPickerItem.stories');
  require('../src/components/CurrentUserIndicator.stories');
  require('../src/components/Flag.stories');
  require('../src/components/GenericNavigationHeader.stories');
  require('../src/components/GuideOverlay.stories');
  require('../src/components/LineupResult.stories');
  require('../src/components/LineupResultRow.stories');
  require('../src/components/PercentageCircle.stories');
  require('../src/components/PercentageCircleWithLines.stories');
  require('../src/components/PlayerListItem.stories');
  require('../src/components/PlayerPickerItem.stories');
  require('../src/components/ProfileNavigationHeader.stories');
  require('../src/components/RemoteImage.stories');
  require('../src/components/RemoteImageBackground.stories');
  require('../src/components/ScreenWrapper.stories');
  require('../src/components/SimpleLineupResult.stories');
  require('../src/components/SwiperContainer.stories');
  require('../src/components/TabSelector.stories');
  require('../src/components/Top3Players.stories');
  require('../src/components/TournamentSelectorButton.stories');
  require('../src/features/main/components/DaySelector.stories');
  require('../src/features/main/components/GenericItem.stories');
  require('../src/features/main/components/QuestionItem.stories');
  require('../src/features/main/screens/HomeScreen.stories');
  require('../src/features/match/components/FiveStarRating.stories');
  require('../src/features/match/components/LineupPickerRow.stories');
  require('../src/features/match/components/LiveEventItem.stories');
  require('../src/features/match/components/LiveEvents.stories');
  require('../src/features/match/components/MatchTime.stories');
  require('../src/features/match/components/MostValuablePlayerPicker.stories');
  require('../src/features/match/components/PerformanceRating.stories');
  require('../src/features/match/components/RatingPicker.stories');
  require('../src/features/match/components/SummaryRow.stories');
  require('../src/features/match/components/Timeline.stories');
  require('../src/features/match/screens/MatchScreen.stories');
  require('../src/features/stats/components/CurrentCompetitions.stories');
  require('../src/features/stats/components/PlayerStandings.stories');
  require('../src/features/stats/components/PopularityBar.stories');
  require('../src/features/stats/components/ProfileTopBar.stories');
  require('../src/features/stats/components/SeasonMatches.stories');
  require('../src/features/stats/components/Standings.stories');
  require('../src/features/stats/components/TileButton.stories');
  require('../src/features/stats/screens/CoachScreen.stories');
  require('../src/features/stats/screens/LeaguesScreen.stories');
  require('../src/features/stats/screens/PlayerScreen.stories');
  require('../src/features/stats/screens/StatsScreen.stories');
  require('../src/features/stats/screens/TeamScreen.stories');
  require('../src/features/user/components/FanList.stories');
  require('../src/features/user/components/PhoneNumberInput.stories');
  require('../src/features/user/components/TeamSelector.stories');
  require('../src/features/user/screens/AwaitAuthScreen.stories');
  require('../src/features/user/screens/NotificationSettingsScreen.stories');
  require('../src/features/user/screens/RequestLocationScreen.stories');
  require('../src/features/user/screens/RequestNotificationPermissionScreen.stories');
  require('../src/features/user/screens/SettingsScreen.stories');
  require('../src/features/user/screens/VerifyPhoneNumberScreen.stories');
  
}

const stories = [
  '../src/components/ActivityIndicatorLine.stories',
  '../src/components/CountryPickerItem.stories',
  '../src/components/CurrentUserIndicator.stories',
  '../src/components/Flag.stories',
  '../src/components/GenericNavigationHeader.stories',
  '../src/components/GuideOverlay.stories',
  '../src/components/LineupResult.stories',
  '../src/components/LineupResultRow.stories',
  '../src/components/PercentageCircle.stories',
  '../src/components/PercentageCircleWithLines.stories',
  '../src/components/PlayerListItem.stories',
  '../src/components/PlayerPickerItem.stories',
  '../src/components/ProfileNavigationHeader.stories',
  '../src/components/RemoteImage.stories',
  '../src/components/RemoteImageBackground.stories',
  '../src/components/ScreenWrapper.stories',
  '../src/components/SimpleLineupResult.stories',
  '../src/components/SwiperContainer.stories',
  '../src/components/TabSelector.stories',
  '../src/components/Top3Players.stories',
  '../src/components/TournamentSelectorButton.stories',
  '../src/features/main/components/DaySelector.stories',
  '../src/features/main/components/GenericItem.stories',
  '../src/features/main/components/QuestionItem.stories',
  '../src/features/main/screens/HomeScreen.stories',
  '../src/features/match/components/FiveStarRating.stories',
  '../src/features/match/components/LineupPickerRow.stories',
  '../src/features/match/components/LiveEventItem.stories',
  '../src/features/match/components/LiveEvents.stories',
  '../src/features/match/components/MatchTime.stories',
  '../src/features/match/components/MostValuablePlayerPicker.stories',
  '../src/features/match/components/PerformanceRating.stories',
  '../src/features/match/components/RatingPicker.stories',
  '../src/features/match/components/SummaryRow.stories',
  '../src/features/match/components/Timeline.stories',
  '../src/features/match/screens/MatchScreen.stories',
  '../src/features/stats/components/CurrentCompetitions.stories',
  '../src/features/stats/components/PlayerStandings.stories',
  '../src/features/stats/components/PopularityBar.stories',
  '../src/features/stats/components/ProfileTopBar.stories',
  '../src/features/stats/components/SeasonMatches.stories',
  '../src/features/stats/components/Standings.stories',
  '../src/features/stats/components/TileButton.stories',
  '../src/features/stats/screens/CoachScreen.stories',
  '../src/features/stats/screens/LeaguesScreen.stories',
  '../src/features/stats/screens/PlayerScreen.stories',
  '../src/features/stats/screens/StatsScreen.stories',
  '../src/features/stats/screens/TeamScreen.stories',
  '../src/features/user/components/FanList.stories',
  '../src/features/user/components/PhoneNumberInput.stories',
  '../src/features/user/components/TeamSelector.stories',
  '../src/features/user/screens/AwaitAuthScreen.stories',
  '../src/features/user/screens/NotificationSettingsScreen.stories',
  '../src/features/user/screens/RequestLocationScreen.stories',
  '../src/features/user/screens/RequestNotificationPermissionScreen.stories',
  '../src/features/user/screens/SettingsScreen.stories',
  '../src/features/user/screens/VerifyPhoneNumberScreen.stories',
  
];

module.exports = {
  loadStories,
  stories,
};

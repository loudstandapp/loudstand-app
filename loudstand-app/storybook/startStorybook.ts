import { Navigation } from 'react-native-navigation'
import CodePush from 'react-native-code-push'
import StorybookUIRoot from '.'

function appLaunchListener() {
  Navigation.setDefaultOptions({
    layout: { orientation: ['portrait'] },
  })
  Navigation.setRoot({
    root: {
      component: {
        id: 'Storybook',
        name: 'Storybook',
      },
    },
  })
}

export function startStorybook() {
  CodePush.notifyAppReady()
  Navigation.registerComponent('Storybook', () => StorybookUIRoot)
  Navigation.events().registerAppLaunchedListener(appLaunchListener)
}

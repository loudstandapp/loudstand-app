import countries from 'i18n-iso-countries'
import * as RNLocalize from 'react-native-localize'
import i18n from 'i18n-js'
import memoize from 'lodash/memoize'
import { I18nManager } from 'react-native'
import 'intl'
import 'intl/locale-data/jsonp/en'
import 'intl/locale-data/jsonp/pt'

import 'moment/locale/en-gb'
import 'moment/locale/pt'

import moment from 'moment'

// FIXME type this file
const translationGetters = {
  en: () => require('../translations/en.json'),
  pt: () => require('../translations/pt.json'),
}

export const translate = memoize(
  (key, config?) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key)
)

export const fallback = { languageTag: 'en', isRTL: false }

const setI18nConfig = () => {
  // fallback if no available language fits

  const { languageTag, isRTL } =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback

  // clear translation cache
  translate.cache.clear && translate.cache.clear()
  // update layout direction
  I18nManager.forceRTL(isRTL)

  // set i18n-js config
  i18n.translations = {
    // @ts-ignore
    [languageTag]: translationGetters[languageTag](),
  }
  i18n.locale = languageTag
  console.log({
    fallback,
    languageTag,
    isRTL,
    translate,
    it: i18n.translations,
    il: i18n.locale,
  })
}

function getCurrentLocale(): RNLocalize.Locale {
  const supportedLanguageCodes = Object.keys(translationGetters)
  const chosenLocale = RNLocalize.getLocales().find(locale =>
    supportedLanguageCodes.includes(locale.languageCode)
  )

  if (chosenLocale) {
    return chosenLocale
  }
  return {
    isRTL: false,
    languageCode: 'en',
    countryCode: 'US',
    languageTag: 'en-US',
  }
}

export const currentLocale = getCurrentLocale()

export function setupTranslation() {
  countries.registerLocale(require('i18n-iso-countries/langs/en.json'))
  countries.registerLocale(require('i18n-iso-countries/langs/pt.json'))
  moment.locale(currentLocale.languageCode)
  setI18nConfig()
}

export const profileReadPermissions = [
  'email',
  'user_friends',
  'user_birthday',
  'user_gender',
  'user_hometown',
  'user_location',
  'user_link',
]

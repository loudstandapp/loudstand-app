/* global __DEV__ */

import { Navigation } from 'react-native-navigation'
import { Store } from 'redux'
import firebase from 'react-native-firebase'
import { persistStore } from 'redux-persist'
import CodePush, { CodePushOptions } from 'react-native-code-push'
import { UserProfileDetails } from '@loudstand/shared'
import Sentry from 'react-native-sentry'
import { showAwaitAuthScreen } from './features/user/navigation'
import registerScreens from './registerScreens'
import { getStore } from './store/getStore'
import { setupTranslation } from './translation'
import { userFlowThunk } from './features/user/userFlow'
import {
  favoriteTeams,
  allTournaments,
  followingTeams,
} from './store/firestoreQueries'
import { ReduxState } from './store/model'
import { launchCleanup } from './store/actions'
import { updateUserDevice } from './services/device'

let favoriteTeamsQuery: object | undefined
let followingTeamsQuery: object | undefined

function handleUserListener(store: Store<ReduxState>) {
  const user = firebase.auth().currentUser
  if (user) {
    favoriteTeamsQuery = favoriteTeams.query(user.uid)
    // @ts-ignore missing typings for react-redux-firebase
    store.firestore.setListener(favoriteTeamsQuery)
    followingTeamsQuery = followingTeams.query(user.uid)
    // @ts-ignore missing typings for react-redux-firebase
    store.firestore.setListener(followingTeamsQuery)
    // @ts-ignore missing typings for react-redux-firebase
    store.firestore.setListener(allTournaments.query())
  } else {
    // @ts-ignore missing typings for react-redux-firebase
    favoriteTeamsQuery && store.firestore.unsetListener(favoriteTeamsQuery)
    // @ts-ignore missing typings for react-redux-firebase
    followingTeamsQuery && store.firestore.unsetListener(followingTeamsQuery)
    // @ts-ignore missing typings for react-redux-firebase
    store.firestore.unsetListener(allTournaments.query())
    favoriteTeamsQuery = undefined
  }
}

function observeStore(store: Store<ReduxState>) {
  let currentActiveTeamId: string | undefined
  let currentUserProfileDetails: UserProfileDetails | undefined

  store.subscribe(() => {
    const state = store.getState()

    const { facebookProfileDetails } = state.firebase.profile
    if (currentUserProfileDetails !== facebookProfileDetails) {
      currentUserProfileDetails = facebookProfileDetails
      if (facebookProfileDetails) {
        const {
          birthyear,
          gender,
          country,
          location,
        } = facebookProfileDetails as UserProfileDetails
        firebase.analytics().setUserProperties({
          birthyear: birthyear ? birthyear.toString() : null,
          gender,
          country,
          location: location && location.name,
        })
      }
    }

    const { activeTeam } = state.general
    if (currentActiveTeamId && !activeTeam) {
      currentActiveTeamId = undefined
      firebase.analytics().setUserProperty('activeTeamId', null)
    } else if (activeTeam && currentActiveTeamId !== activeTeam.id) {
      currentActiveTeamId = activeTeam.id
      firebase.analytics().setUserProperty('activeTeamId', activeTeam.id)
    }
  })
}

function setupNavigation(store: Store<ReduxState>) {
  Navigation.setDefaultOptions({
    layout: { orientation: ['portrait'] },
    statusBar: { style: 'light' },
    topBar: {
      visible: false,
      drawBehind: true,
    },
  })
  Navigation.events().registerComponentDidAppearListener(
    ({ componentName }) => {
      firebase.analytics().setCurrentScreen(componentName, componentName)
    }
  )
  registerScreens(store)
}

function setupMessageTokenHandler(store: Store<ReduxState>) {
  firebase.messaging().onTokenRefresh(async fcmToken => {
    const state = store.getState()
    const { uid } = state.firebase.auth
    if (uid) {
      await updateUserDevice(uid, fcmToken)
    }
  })
}

const codePushOptions: CodePushOptions = {
  installMode: CodePush.InstallMode.ON_NEXT_RESUME,
  checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
  updateDialog: { appendReleaseDescription: true },
}

async function codePushSync() {
  try {
    const pushResult = await CodePush.sync(codePushOptions)
    console.log('codepush result', pushResult)
  } catch (e) {
    console.error('codepush error', e)
  }
}

async function codePushSentryReport() {
  try {
    const update = await CodePush.getUpdateMetadata()
    if (update) {
      Sentry.setVersion(`${update.appVersion}-codepush:${update.label}`)
    }
  } catch (e) {
    console.error('codepush update error', e)
  }
}

export async function startApp() {
  console.log('startApp')
  setupTranslation()
  const store = getStore()
  observeStore(store)
  setupMessageTokenHandler(store)
  !__DEV__ && codePushSync()
  !__DEV__ && codePushSentryReport()

  let isNavigationReady = false
  let isFirebaseAuthReady = false
  let hasAppBeenStarted = false

  async function startAppIfReady() {
    handleUserListener(store)
    console.log('startAppIfReady')
    if (hasAppBeenStarted) {
      console.log('hasAppBeenStarted')
      return
    }
    if (isFirebaseAuthReady && isNavigationReady) {
      console.log('isFirebaseAuthReady && isNavigationReady')

      store.dispatch(launchCleanup())
      hasAppBeenStarted = true
      // @ts-ignore typings for userFlowThunk
      store.dispatch(userFlowThunk())
    }
    if (!isFirebaseAuthReady) {
      console.log('!isFirebaseAuthReady')
      await showAwaitAuthScreen()
    }
  }

  firebase.auth().onAuthStateChanged(async user => {
    console.log('AUTH STATE CHANGED: ', { user })
    isFirebaseAuthReady = true
    await startAppIfReady()
  })

  Navigation.events().registerAppLaunchedListener(async () => {
    console.log('registerAppLaunchedListener')
    persistStore(store, {}, async () => {
      setupNavigation(store)
      isNavigationReady = true
      // Android can call this after the javascript context has been initialized, so we need to make sure app is started properly in that case.
      hasAppBeenStarted = false
      await startAppIfReady()
    })
  })
}

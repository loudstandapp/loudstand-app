import {
  ISO8601Date,
  TeamListItem,
  LocalizedTeams,
  ID,
} from '@loudstand/shared'
import firebase from 'react-native-firebase'
import DeviceInfo from 'react-native-device-info'
import { Alert, Linking, Platform } from 'react-native'
import { translate } from 'i18n-js'
import AndroidOpenSettings from 'react-native-android-open-settings'

// eslint-disable-next-line @typescript-eslint/no-var-requires
export const config = require('./config/config.json')

export function getComparatorForKey<T>(key: keyof T, reverse: boolean = false) {
  if (reverse) {
    return (a: T, b: T) => {
      if (a[key] > b[key] || b[key] === undefined || b[key] === null) {
        return -1
      }
      if (a[key] < b[key] || a[key] === undefined || a[key] === null) {
        return 1
      }
      return 0
    }
  }
  return (a: T, b: T) => {
    if (a[key] > b[key] || b[key] === undefined || b[key] === null) {
      return 1
    }
    if (a[key] < b[key] || a[key] === undefined || a[key] === null) {
      return -1
    }
    return 0
  }
}

export function cleanDate(dateWithZ: string): ISO8601Date {
  const match = dateWithZ.match(/^(\d{4}-\d{2}-\d{2}).*$/)
  if (!match || match.length < 2 || match[1] === null) {
    throw new Error(`cleanDate() could not parse timestamp "${dateWithZ}"`)
  }
  return match[1]
}

export function localizeTeam(
  team: TeamListItem,
  localizations?: LocalizedTeams
) {
  if (!localizations) {
    return team.name
  }
  return localizations[team.id] || team.name
}

export enum ItemImageType {
  Headshot = 'HEADSHOT',
  Actionshot = 'ACTIONSHOT',
  Venue = 'VENUE',
  TeamLogo = 'TEAM_LOGO',
  SeasonLogo = 'SEASON_LOGO',
  TeamPhoto = 'TEAM_PHOTO',
}

interface ImageType {
  folder: string
  extension: 'jpg' | 'png'
}

const imageTypes: { [key in ItemImageType]: ImageType } = {
  [ItemImageType.Headshot]: {
    folder: 'headshots',
    extension: 'jpg',
  },
  [ItemImageType.Actionshot]: {
    folder: 'actionshots',
    extension: 'jpg',
  },
  [ItemImageType.TeamPhoto]: {
    folder: 'teamphotos',
    extension: 'jpg',
  },
  [ItemImageType.Venue]: {
    folder: 'venues',
    extension: 'jpg',
  },
  [ItemImageType.TeamLogo]: {
    folder: 'teamlogos',
    extension: 'png',
  },
  [ItemImageType.SeasonLogo]: {
    folder: 'seasonlogos',
    extension: 'png',
  },
}

export function getImageUrl(id: ID, type: ItemImageType) {
  const filename = id.replace(/:/g, '-')
  const typeConfig = imageTypes[type]
  const url = `${config.imageBaseUrl}/${typeConfig.folder}/${filename}.${
    typeConfig.extension
  }`
  return url
}

export const fallbackImages = {
  teamLogo: require('../assets/images/fallback/team-logo.png'),
  venue: require('../assets/images/fallback/stadium.jpg'),
  managerActionShot: require('../assets/images/fallback/coach.jpg'),
  playerActionShot: require('../assets/images/fallback/player.jpg'),
  question: require('../assets/images/fallback/question.jpg'),
  homescreen: require('../assets/images/fallback/stadium.jpg'),
  penalty: require('../assets/images/fallback/penalty.jpg'),
  redCard: require('../assets/images/fallback/red.jpg'),
  yellowCard: require('../assets/images/fallback/yellow.jpg'),
  yellowRedCard: require('../assets/images/fallback/red.jpg'),
  refereeActionShot: require('../assets/images/fallback/referee.jpg'),
  substitution: require('../assets/images/fallback/substitution.jpg'),
  teamPhoto: require('../assets/images/fallback/team.jpg'),
  userProfile: require('../assets/images/fallback/user.jpg'),
  seasonLogo: require('../assets/icons/indicators/season-logo.png'),
}
// @ts-ignore FIXME
export const functions = firebase.app().functions(config.region)

export const sleep = (duration: number) =>
  new Promise(resolve => setTimeout(resolve, duration))

interface WithId {
  id: ID
}
export function objectFromArrayWithIds<T extends WithId>(
  array: T[]
): { [key in ID]: T } {
  return array.reduce(
    (acc: { [key in ID]: T }, team) => ({
      ...acc,
      [team.id]: team,
    }),
    {}
  )
}

export function formatPercentage(decimal: number): string {
  return `${Math.round(decimal * 100)}%`
}

export function formatEventTime(seconds: number | null) {
  if (seconds === null) {
    return ''
  }
  return `${Math.round(seconds / 60)}'`
}

export function numberToPercentage(number: number) {
  return Math.round(number * 100)
}

export function ratingToPercentage(rating: number) {
  return Math.round((rating * 100) / 5)
}

export const deviceInfo = {
  buildNumber: DeviceInfo.getBuildNumber(),
  readableVersion: DeviceInfo.getReadableVersion(),
  version: DeviceInfo.getVersion(),
  applicationName: DeviceInfo.getApplicationName(),
  deviceId: DeviceInfo.getDeviceId(),
  systemVersion: DeviceInfo.getSystemVersion(),
  manufacturer: DeviceInfo.getManufacturer(),
  brand: DeviceInfo.getBrand(),
  bundleId: DeviceInfo.getBundleId(),
  buildId: DeviceInfo.getBuildId(),
}

export const cheats = {
  alwaysAllowVoting: false,
}

let cheatClickCounter = 0

export function cheatClick() {
  cheatClickCounter += 1
  cheats.alwaysAllowVoting = cheatClickCounter >= 10
  if (cheatClickCounter === 10) {
    Alert.alert('🤘God mode enabled🤘')
  }
}
export function randomComponentId(name: string) {
  return `${name}-${Math.random()}`
}

export async function openUrl(url: string) {
  const canOpen = await Linking.canOpenURL(url)

  if (!canOpen) {
    if (url.startsWith('mailto:')) {
      Alert.alert(translate('url.error'), translate('url.cannotOpenEmailUrl'))
    } else {
      Alert.alert(translate('url.error'), translate('url.cannotOpenUrl'))
    }
    return
  }
  await Linking.openURL(url)
}

export const openAppSettings = Platform.select({
  ios: () => openUrl('app-settings:'),
  android: async () => {
    AndroidOpenSettings.appDetailsSettings()
  },
})

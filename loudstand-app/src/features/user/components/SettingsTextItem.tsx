import React from 'react'
import { Text, StyleSheet, View } from 'react-native'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { spacingUnit } from '../../../theme/layout'
import { cheatClick } from '../../../utils'

interface Props {
  title: string
  content?: string
  presentationStyle: 'even' | 'odd'
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    paddingVertical: spacingUnit,
    paddingHorizontal: 2 * spacingUnit,
  },
  titleText: {
    ...(fonts.HeaderSmall as object),
  },
  even: {
    backgroundColor: colors.tertiaryLight,
  },
  odd: {
    backgroundColor: 'transparent',
  },
  contentText: {
    ...(fonts.Body as object),
    marginTop: spacingUnit,
  },
})

export default class SettingsTextItem extends React.PureComponent<Props> {
  handlePress = () => {
    const { title } = this.props
    if (title === 'JS bundle version') {
      cheatClick()
    }
  }

  render = () => {
    const { title, content, presentationStyle } = this.props
    return (
      <View
        style={[
          styles.container,
          presentationStyle === 'even' && styles.even,
          presentationStyle === 'odd' && styles.odd,
        ]}
      >
        <Text style={styles.titleText}>{title}</Text>
        {content && (
          <Text onPress={this.handlePress} style={styles.contentText}>
            {content}
          </Text>
        )}
      </View>
    )
  }
}

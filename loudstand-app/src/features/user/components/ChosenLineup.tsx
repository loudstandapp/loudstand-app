import React from 'react'
import { PlayerList, PlayerPosition, JerseyColors, ID } from '@loudstand/shared'
import { ImageBackground, StyleSheet } from 'react-native'
import { spacingUnit, marginDefault } from '../../../theme/layout'
import colors from '../../../theme/colors'
import ChosenLineupRow from './ChosenLineupRow'
import InfoButton from '../../../components/InfoButton'
import { InfoType } from '../../../components/Info'

interface Props {
  chosen: ID[]
  teamPlayers: PlayerList
  jerseyColors?: JerseyColors
  onSelectPlayer: (id: ID) => void
}
const styles = StyleSheet.create({
  fieldContainer: {
    justifyContent: 'space-between',
    marginHorizontal: marginDefault,
    marginVertical: spacingUnit,
    flex: 1,
  },
  fieldContainerImage: {
    tintColor: colors.tertiaryLight,
    borderWidth: 3,
    borderColor: colors.tertiaryLight,
  },
})

export default class ChosenLineup extends React.PureComponent<Props> {
  render = () => {
    const { chosen, teamPlayers, jerseyColors, onSelectPlayer } = this.props

    const goalKeepers = Object.values(teamPlayers).filter(
      player =>
        chosen.includes(player.id) &&
        player.position === PlayerPosition.Goalkeeper
    )
    const defenders = Object.values(teamPlayers).filter(
      player =>
        chosen.includes(player.id) &&
        player.position === PlayerPosition.Defender
    )
    const midfielders = Object.values(teamPlayers).filter(
      player =>
        chosen.includes(player.id) &&
        player.position === PlayerPosition.Midfielder
    )
    const forwards = Object.values(teamPlayers).filter(
      player =>
        chosen.includes(player.id) && player.position === PlayerPosition.Forward
    )

    return (
      <ImageBackground
        style={styles.fieldContainer}
        imageStyle={styles.fieldContainerImage}
        source={require('../../../../assets/images/borderless-pitch.png')}
      >
        <ChosenLineupRow
          onSelectPlayer={onSelectPlayer}
          jerseyColors={jerseyColors}
          players={goalKeepers}
        />
        <ChosenLineupRow
          onSelectPlayer={onSelectPlayer}
          jerseyColors={jerseyColors}
          players={defenders}
        />
        <ChosenLineupRow
          onSelectPlayer={onSelectPlayer}
          jerseyColors={jerseyColors}
          players={midfielders}
        />
        <ChosenLineupRow
          onSelectPlayer={onSelectPlayer}
          jerseyColors={jerseyColors}
          players={forwards}
        />
        <InfoButton type={InfoType.Lineup} />
      </ImageBackground>
    )
  }
}

import React from 'react'
import {
  UserItem,
  UserActivityItem,
  ID,
  UserActivityType,
  LocalizedTeams,
  MatchListItem,
} from '@loudstand/shared'
import { StyleSheet, View, Text } from 'react-native'
import moment from 'moment'
import FanListItem from './FanListItem'
import LiveEventItem from '../../match/components/LiveEventItem'
import QuestionItem from '../../main/components/QuestionItem'
import MostValuablePlayerPicker from '../../match/components/MostValuablePlayerPicker'
import PerformanceRating from '../../match/components/PerformanceRating'
import ChosenLineup from './ChosenLineup'
import MatchListItemView from '../../../components/MatchListItemView'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { currentLocale, translate } from '../../../translation'
import SimpleLineupResult from '../../../components/SimpleLineupResult'

const matchDateFormatter = new Intl.DateTimeFormat(currentLocale.languageTag, {
  month: 'numeric',
  day: 'numeric',
  year: 'numeric',
})

interface Props {
  userItem: UserItem | null
  activityItem: UserActivityItem
  onSelectUser?: (userId: ID) => void
  onSelectPlayer: (id: ID) => void
  onSelectTeam: (id: ID) => void
  onSelectManager: (id: ID) => void
  onSelectMatch: (match: MatchListItem) => void
  localizedTeams?: LocalizedTeams
}

const styles = StyleSheet.create({
  cancelMarginBottom: {
    marginBottom: -marginDefault,
  },
  timestampText: {
    ...(fonts.BodySmall as object),
    marginHorizontal: 2 * spacingUnit,
    marginVertical: spacingUnit,
    color: colors.tertiary,
    textAlign: 'right',
  },
})

export default class UserActivityView extends React.PureComponent<Props> {
  renderActivity = () => {
    const {
      activityItem: { activity },
      localizedTeams,
      onSelectPlayer,
      onSelectTeam,
      onSelectManager,
    } = this.props
    switch (activity.type) {
      case UserActivityType.MatchEvent: {
        return (
          <View style={styles.cancelMarginBottom}>
            <LiveEventItem
              event={activity.matchEvent}
              allowsVoting={false}
              chosen={activity.response}
            />
          </View>
        )
      }
      case UserActivityType.Question: {
        return (
          <QuestionItem
            question={activity.question}
            chosen={activity.response}
            availableTeams={{}}
          />
        )
      }
      case UserActivityType.MostValuablePlayer: {
        return (
          <View style={styles.cancelMarginBottom}>
            <MostValuablePlayerPicker
              players={[activity.response]}
              onSelect={player => onSelectPlayer(player.id)}
            />
          </View>
        )
      }
      case UserActivityType.TeamPerformance: {
        return (
          <View style={styles.cancelMarginBottom}>
            <PerformanceRating
              team={activity.team}
              result={{ rating: activity.response, voteCount: 0 }}
              allowsVoting={false}
              localizedTeams={localizedTeams}
              onSelectTeam={onSelectTeam}
            />
          </View>
        )
      }
      case UserActivityType.ManagerPerformance: {
        return (
          <View style={styles.cancelMarginBottom}>
            <PerformanceRating
              manager={activity.team.manager}
              result={{ rating: activity.response, voteCount: 0 }}
              allowsVoting={false}
              localizedTeams={localizedTeams}
              onSelectManager={onSelectManager}
            />
          </View>
        )
      }
      case UserActivityType.Lineup: {
        return (
          <SimpleLineupResult
            teamPlayers={activity.players}
            jerseyColors={activity.team.jerseyColors || undefined}
            onSelectPlayer={onSelectPlayer}
            chosen={activity.response}
            title={translate('community.starting11')}
          />
        )
      }
      default:
        return null
    }
  }

  renderMatch = () => {
    const {
      activityItem: { activity },
      localizedTeams,
      onSelectMatch,
    } = this.props
    switch (activity.type) {
      case UserActivityType.Lineup:
      case UserActivityType.ManagerPerformance:
      case UserActivityType.MatchEvent:
      case UserActivityType.MostValuablePlayer:
      case UserActivityType.TeamPerformance: {
        const { match } = activity
        return (
          <MatchListItemView
            match={match}
            localizedTeams={localizedTeams}
            onSelectMatch={onSelectMatch}
            dateFormatter={matchDateFormatter}
          />
        )
      }
      default:
        return null
    }
  }

  render = () => {
    const {
      userItem,
      onSelectUser,
      activityItem: { timestamp },
    } = this.props

    const formattedTimestamp = moment(timestamp).fromNow()
    return (
      <View>
        {userItem ? (
          <FanListItem
            fan={userItem}
            onPress={onSelectUser}
            subHeader={formattedTimestamp}
          />
        ) : (
          <Text style={styles.timestampText}>{formattedTimestamp}</Text>
        )}
        {this.renderActivity()}
        {this.renderMatch()}
      </View>
    )
  }
}

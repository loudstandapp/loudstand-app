import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import PhoneNumberInput from './PhoneNumberInput'

const onChangeCountry = (callback: (countryCode: string) => void) =>
  callback('US')

storiesOf('PhoneNumberInput', module).add('default', () => (
  <PhoneNumberInput
    initialCountryCode="DK"
    numberValue="22422444"
    onSelectCountry={onChangeCountry}
    onChange={action('onChange')}
    onSubmit={action('onSubmit')}
  />
))

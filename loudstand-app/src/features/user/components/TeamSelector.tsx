import React from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import { TeamListItem, ID, TeamList, LocalizedTeams } from '@loudstand/shared'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import TeamSelectorItem from './TeamSelectorItem'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import colors from '../../../theme/colors'

interface Props {
  teams: TeamList
  selectedTeams: TeamList
  disabledTeams?: TeamList
  allowsMultiple: boolean
  localizedTeams?: LocalizedTeams
  onSelect?: (selected: TeamListItem) => void
  onDeselect?: (deselected: TeamListItem) => void
  onChange?: (selected: TeamList) => void
}

const styles = StyleSheet.create({
  container: {
    margin: marginDefault,
    backgroundColor: colors.secondary10pct,
    padding: spacingUnit,
    borderRadius: spacingUnit,
    flex: 1,
  },
  contentContainer: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    // This is a hack to fix hidden content in the bottom on both iOS and Android
    paddingBottom: 4 * spacingUnit,
  },
})

export class TeamSelector extends React.PureComponent<Props> {
  static defaultProps = { allowsMultiple: false, teams: {} }

  handleSelect = (team: TeamListItem) => {
    const { allowsMultiple, onChange, selectedTeams, onSelect } = this.props
    const selected = allowsMultiple
      ? { ...selectedTeams, [team.id]: team }
      : { [team.id]: team }
    onSelect && onSelect(team)
    onChange && onChange(selected)
    ReactNativeHapticFeedback.trigger('impactHeavy')
  }

  handleDeselect = (team: TeamListItem) => {
    const { onChange, selectedTeams, onDeselect } = this.props
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { [team.id]: _, ...rest } = selectedTeams
    onDeselect && onDeselect(team)
    onChange && onChange(rest)
    ReactNativeHapticFeedback.trigger('impactLight')
  }

  render() {
    const { teams, selectedTeams, localizedTeams, disabledTeams } = this.props
    return (
      <ScrollView
        // contentInset={{ bottom: 4 * spacingUnit }}
        contentContainerStyle={styles.contentContainer}
        style={styles.container}
      >
        {Object.values(teams).map((team: TeamListItem) => (
          <TeamSelectorItem
            key={team.id}
            team={team}
            localizedTeams={localizedTeams}
            isSelected={!!selectedTeams[team.id]}
            disabled={disabledTeams && !!disabledTeams[team.id]}
            onSelect={this.handleSelect}
            onDeselect={this.handleDeselect}
          />
        ))}
      </ScrollView>
    )
  }
}

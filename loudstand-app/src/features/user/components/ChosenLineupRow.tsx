import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import { JerseyColors, TeamPlayer, ID } from '@loudstand/shared'
import Color from 'color'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { spacingUnit } from '../../../theme/layout'
import PlayerHeadshot from '../../../components/PlayerHeadshot'

const BORDER_WIDTH = 3

const styles = StyleSheet.create({
  lineupRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    flex: 1,
    marginVertical: spacingUnit,
  },
  player: {
    alignItems: 'center',
    flex: 1,
  },
  playerImage: {
    width: PlayerHeadshot.mediumSize + 2 * BORDER_WIDTH,
    height: PlayerHeadshot.mediumSize + 2 * BORDER_WIDTH,
    borderRadius: PlayerHeadshot.mediumSize + BORDER_WIDTH,
    borderWidth: BORDER_WIDTH,
  },
  name: {
    marginTop: spacingUnit / 2,
    ...(fonts.BodySmallBold as object),
    textAlign: 'center',
  },
  homeCircle: {
    // FIXME
    borderColor: colors.primaryLight,
  },
  awayCircle: {
    borderColor: colors.secondaryLight,
  },
})

interface Props {
  players: TeamPlayer[]
  jerseyColors?: JerseyColors
  onSelectPlayer: (id: ID) => void
}

const homeDefaultJerseyColors: JerseyColors = {
  base: colors.primary,
  number: colors.secondaryText,
  sleeve: colors.tertiary,
}

export default class ChosenLineupRow extends React.PureComponent<Props> {
  render = () => {
    const {
      jerseyColors: jerseyColorsOrUndefined,
      players,
      onSelectPlayer,
    } = this.props
    const jerseyColors = jerseyColorsOrUndefined || homeDefaultJerseyColors
    return (
      <View style={styles.lineupRow}>
        {players.map(player => (
          <TouchableOpacity
            onPress={() => onSelectPlayer(player.id)}
            style={styles.player}
            key={player.id}
          >
            <PlayerHeadshot
              style={[
                styles.playerImage,
                {
                  borderColor: Color(jerseyColors.number)
                    .alpha(0.4)
                    .toString(),
                },
              ]}
              player={player}
              size="medium"
              jerseyColors={jerseyColors}
            />
            <Text ellipsizeMode="tail" style={styles.name}>
              {player.name}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    )
  }
}

import React from 'react'
import { Text, StyleSheet } from 'react-native'
import fonts from '../../../theme/fonts'
import { spacingUnit } from '../../../theme/layout'

interface Props {
  title: string
}

const styles = StyleSheet.create({
  titleText: {
    textAlign: 'center',
    flex: 1,
    ...(fonts.HeaderSmall as object),
    padding: 2 * spacingUnit,
  },
})

export default class SettingsSectionHeader extends React.PureComponent<Props> {
  render = () => {
    const { title } = this.props
    return <Text style={styles.titleText}>{title}</Text>
  }
}

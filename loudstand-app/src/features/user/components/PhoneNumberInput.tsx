import React from 'react'
import {
  TouchableOpacity,
  TextInput,
  StyleSheet,
  View,
  Text,
  StyleProp,
  ViewStyle,
  TextInputProps,
  Image,
} from 'react-native'
import PhoneNumber from 'awesome-phonenumber'
import { ISO3166v1a2Country } from '@loudstand/shared'
import Flag from '../../../components/Flag'
import { spacingUnit } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: spacingUnit,
  },
  countrySelectorButton: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: spacingUnit,
    marginVertical: spacingUnit,
    marginRight: spacingUnit,
    borderRadius: spacingUnit / 2,
    backgroundColor: colors.secondary,
  },
  input: {
    flex: 1,
    ...(fonts.CustomPhoneNumberInput as object),
  },
  inputContainer: {},
  indicator: {},
  validIndicator: {
    tintColor: colors.valid,
  },
  invalidIndicator: {
    tintColor: colors.invalid,
  },
  countryCodeText: {
    minWidth: 30,
    textAlign: 'right',
    margin: spacingUnit,
    ...(fonts.CustomPhoneNumberInput as object),
    color: colors.secondaryText,
  },
})

interface Props {
  numberValue: string
  initialCountryCode: ISO3166v1a2Country
  onSelectCountry: (callback: (countryCode: ISO3166v1a2Country) => void) => void
  onChange: (phoneNumber: string, isValid: boolean) => void
  onSubmit: () => void
  style: StyleProp<ViewStyle>
  disabled: boolean
  textInputProps: TextInputProps
}

interface State {
  input: string
  countryCode: ISO3166v1a2Country
  regionCode: number
  isValid: boolean
  phoneNumber: PhoneNumber
}

export default class PhoneNumberInput extends React.PureComponent<
  Props,
  State
> {
  static defaultProps = {
    numberValue: '',
    style: {},
    disabled: false,
    textInputProps: {},
  }

  textInputRef = React.createRef<TextInput>()

  constructor(props: Props) {
    super(props)
    this.state = {
      phoneNumber: new PhoneNumber(props.numberValue, props.initialCountryCode),
      input: props.numberValue,
      countryCode: props.initialCountryCode,
      regionCode: PhoneNumber.getCountryCodeForRegionCode(
        props.initialCountryCode
      ),
      isValid: false,
    }
  }

  clear = () => this.textInputRef.current && this.textInputRef.current.clear()

  blur = () => this.textInputRef.current && this.textInputRef.current.blur()

  focus = () => this.textInputRef.current && this.textInputRef.current.focus()

  componentDidUpdate = (prevProps: Props) => {
    const { countryCode } = this.state
    const { numberValue } = this.props
    if (prevProps.numberValue !== numberValue && numberValue) {
      const phoneNumber = new PhoneNumber(numberValue, countryCode)
      this.setState(
        {
          input: numberValue,
          phoneNumber,
          isValid: phoneNumber.isValid(),
        },
        this.handleChange
      )
    }
  }

  handleInputChange = (input: string) => {
    this.setState(prevState => {
      const phoneNumber = new PhoneNumber(input, prevState.countryCode)
      return { input, phoneNumber }
    }, this.handleChange)
  }

  handleChange = () => {
    const { phoneNumber } = this.state
    const isValid = phoneNumber.isValid()
    this.setState({
      isValid,
    })
    const { onChange } = this.props
    onChange(phoneNumber.getNumber(), isValid)
  }

  handleSelectCountry = () => {
    const { onSelectCountry } = this.props
    onSelectCountry((countryCode: ISO3166v1a2Country) => {
      const regionCode = PhoneNumber.getCountryCodeForRegionCode(countryCode)

      this.setState(prevState => {
        const phoneNumber = new PhoneNumber(prevState.input, countryCode)
        return { countryCode, regionCode, phoneNumber }
      }, this.handleChange)
    })
  }

  render = () => {
    const { style, disabled, textInputProps, onSubmit } = this.props
    const { input, countryCode, isValid, regionCode } = this.state
    return (
      <View style={[styles.container, style]}>
        {!input.startsWith('+') && (
          <TouchableOpacity
            style={styles.countrySelectorButton}
            onPress={this.handleSelectCountry}
          >
            <Flag countryCode={countryCode} />
            <Text style={styles.countryCodeText}>{`+${regionCode} `}</Text>
          </TouchableOpacity>
        )}
        <TextInput
          ref={this.textInputRef}
          textContentType="telephoneNumber"
          keyboardType="phone-pad"
          style={styles.input}
          value={input}
          onChangeText={this.handleInputChange}
          onSubmitEditing={onSubmit}
          editable={!disabled}
          {...textInputProps}
        />
        {isValid ? (
          <Image
            source={require('../../../../assets/icons/indicators/checked.png')}
            style={[styles.indicator, styles.validIndicator]}
          />
        ) : (
          <Image
            source={require('../../../../assets/icons/indicators/error.png')}
            style={[styles.indicator, styles.invalidIndicator]}
          />
        )}
      </View>
    )
  }
}

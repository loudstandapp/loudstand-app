import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { TeamSelector } from './TeamSelector'

const TEAMS = {
  'sr:competitor:2995': {
    id: 'sr:competitor:2995',
    name: 'Boavista FC',
    country: 'DE',
  },
  'sr:competitor:2999': {
    id: 'sr:competitor:2999',
    name: 'Sporting Braga',
    country: 'DE',
  },
  'sr:competitor:3001': {
    id: 'sr:competitor:3001',
    name: 'Sporting CP',
    country: 'DE',
  },
  'sr:competitor:3002': {
    id: 'sr:competitor:3002',
    name: 'FC Porto',
    country: 'DE',
  },
  'sr:competitor:3004': {
    id: 'sr:competitor:3004',
    name: 'CF Os Belenenses',
    country: 'DE',
  },
  'sr:competitor:3006': {
    id: 'sr:competitor:3006',
    name: 'Benfica Lisbon',
    country: 'DE',
  },
  'sr:competitor:3007': {
    id: 'sr:competitor:3007',
    name: 'CS Maritimo Madeira',
    country: 'DE',
  },
  'sr:competitor:3008': {
    id: 'sr:competitor:3008',
    name: 'Vitoria Setubal',
    country: 'DE',
  },
  'sr:competitor:3009': {
    id: 'sr:competitor:3009',
    name: 'Vitoria Guimaraes',
    country: 'DE',
  },
  'sr:competitor:3011': {
    id: 'sr:competitor:3011',
    name: 'CD Santa Clara',
    country: 'DE',
  },
  'sr:competitor:3013': {
    id: 'sr:competitor:3013',
    name: 'CD Nacional',
    country: 'DE',
  },
  'sr:competitor:3014': {
    id: 'sr:competitor:3014',
    name: 'Moreirense FC',
    country: 'DE',
  },
  'sr:competitor:3025': {
    id: 'sr:competitor:3025',
    name: 'GD Chaves',
    country: 'DE',
  },
  'sr:competitor:3026': {
    id: 'sr:competitor:3026',
    name: 'Portimonense SC',
    country: 'DE',
  },
  'sr:competitor:3032': {
    id: 'sr:competitor:3032',
    name: 'CD das Aves',
    country: 'DE',
  },
  'sr:competitor:3036': {
    id: 'sr:competitor:3036',
    name: 'Rio Ave FC',
    country: 'DE',
  },
  'sr:competitor:38396': {
    id: 'sr:competitor:38396',
    name: 'CD Tondela',
    country: 'DE',
  },
  'sr:competitor:4501': {
    id: 'sr:competitor:4501',
    name: 'CD Feirense',
    country: 'DE',
  },
}

storiesOf('TeamSelector', module)
  .add('none selected, multiple allowed', () => (
    <TeamSelector
      teams={TEAMS}
      selectedTeams={{}}
      allowsMultiple
      onChange={action('onChange')}
    />
  ))
  .add('3 selected, multiple allowed', () => (
    <TeamSelector
      teams={TEAMS}
      selectedTeams={{
        'sr:competitor:3014': {
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
          country: 'DE',
        },
        'sr:competitor:3032': {
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
          country: 'DE',
        },

        'sr:competitor:38396': {
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
          country: 'DE',
        },
      }}
      allowsMultiple
      onChange={action('onChange')}
    />
  ))
  .add('2 selected, 1 disabled', () => (
    <TeamSelector
      teams={TEAMS}
      selectedTeams={{
        'sr:competitor:3014': {
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
          country: 'DE',
        },

        'sr:competitor:38396': {
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
          country: 'DE',
        },
      }}
      allowsMultiple
      onChange={action('onChange')}
      disabledTeams={{
        'sr:competitor:3032': {
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
          country: 'DE',
        },
      }}
    />
  ))
  .add('none selected, multiple not allowed', () => (
    <TeamSelector
      teams={TEAMS}
      selectedTeams={{}}
      onChange={action('onChange')}
    />
  ))
  .add('1 selected, multiple not allowed', () => (
    <TeamSelector
      teams={TEAMS}
      selectedTeams={{
        'sr:competitor:3032': {
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
          country: 'DE',
        },

        'sr:competitor:38396': {
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
          country: 'DE',
        },
      }}
      onChange={action('onChange')}
    />
  ))

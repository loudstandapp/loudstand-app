import React from 'react'
import { TeamListItem, LocalizedTeams } from '@loudstand/shared'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'
import {
  localizeTeam,
  getImageUrl,
  ItemImageType,
  fallbackImages,
} from '../../../utils'
import fonts from '../../../theme/fonts'
import {
  spacingUnit,
  isSmallDevice,
  marginDefault,
} from '../../../theme/layout'
import RemoteImage from '../../../components/RemoteImage'

interface Props {
  isSelected: boolean
  team: TeamListItem
  disabled: boolean
  localizedTeams?: LocalizedTeams
  onSelect: (team: TeamListItem) => void
  onDeselect: (team: TeamListItem) => void
}

const LOGO_SIZE = 50
const WIDTH = isSmallDevice ? '33%' : '25%'

const styles = StyleSheet.create({
  teamLogo: {
    width: LOGO_SIZE,
    height: LOGO_SIZE,
    opacity: 0.4,
  },
  teamName: {
    marginTop: marginDefault,
    ...(fonts.BodySmall as object),
    flex: 1,
    textAlign: 'center',
  },
  teamNameSelected: {
    ...(fonts.BodySmallBold as object),
  },
  container: {
    width: WIDTH,
    paddingVertical: spacingUnit,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  selected: {
    opacity: 1,
  },
  disabled: {
    opacity: 0.3,
  },
})

export default class TeamSelectorItem extends React.PureComponent<Props> {
  static defaultProps = {
    disabled: false,
  }

  handlePress = () => {
    const { team, onSelect, onDeselect, isSelected } = this.props
    isSelected ? onDeselect(team) : onSelect(team)
  }

  render = () => {
    const { team, isSelected, localizedTeams, disabled } = this.props
    return (
      <TouchableOpacity
        onPress={this.handlePress}
        style={[styles.container]}
        disabled={disabled}
      >
        <RemoteImage
          style={[
            styles.teamLogo,
            isSelected && styles.selected,
            disabled && styles.disabled,
          ]}
          source={{ uri: getImageUrl(team.id, ItemImageType.TeamLogo) }}
          defaultSource={fallbackImages.teamLogo}
        />
        <Text
          style={[
            styles.teamName,
            disabled && styles.disabled,
            isSelected && styles.teamNameSelected,
          ]}
          numberOfLines={2}
        >
          {localizeTeam(team, localizedTeams)}
        </Text>
      </TouchableOpacity>
    )
  }
}

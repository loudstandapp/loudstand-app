import React from 'react'
import { TouchableOpacity, StyleSheet, View, Image, Text } from 'react-native'
import { UserItem, ID } from '@loudstand/shared'
import colors from '../../../theme/colors'
import { spacingUnit } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import { getImageUrl, ItemImageType, fallbackImages } from '../../../utils'
import RemoteImage from '../../../components/RemoteImage'

interface Props {
  fan: UserItem
  subHeader?: string
  onPress?: (userId: ID) => void
}

const PROFILE_IMAGE_SIZE = 60
const TEAM_LOGO_SIZE = 20
const TEAM_LOGO_WRAPPER_SIZE = 24

const styles = StyleSheet.create({
  rowContainer: {
    flexDirection: 'row',
    paddingHorizontal: 2 * spacingUnit,
    paddingVertical: spacingUnit,
    alignItems: 'center',
  },
  nameText: {
    ...(fonts.Header as object),
  },
  profileImage: {
    width: PROFILE_IMAGE_SIZE,
    height: PROFILE_IMAGE_SIZE,
    borderRadius: PROFILE_IMAGE_SIZE / 2,
    shadowColor: colors.secondary,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  profileImageBorder: {
    shadowColor: colors.secondary,
    shadowOffset: { width: 0.5, height: 0.5 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
  },
  teamLogoImageBorder: {
    width: TEAM_LOGO_WRAPPER_SIZE,
    height: TEAM_LOGO_WRAPPER_SIZE,
    padding: 2,
    shadowColor: colors.secondary,
    shadowOffset: { width: 0.5, height: 0.5 },
    shadowOpacity: 0.2,
    shadowRadius: 1,
    marginLeft: -spacingUnit,
    borderRadius: TEAM_LOGO_WRAPPER_SIZE / 2,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.background,
  },
  teamLogoImage: {
    width: TEAM_LOGO_SIZE,
    height: TEAM_LOGO_SIZE,
  },
  teamlogosContainer: {
    width: PROFILE_IMAGE_SIZE,
    overflow: 'hidden',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    left: 0,
    justifyContent: 'flex-end',
  },
  profileImageContainer: {
    marginRight: 2 * spacingUnit,
  },
  textContainer: {},
  subHeaderText: {
    ...(fonts.BodySmall as object),
    marginTop: spacingUnit,
    color: colors.tertiary,
  },
})

export default class FanListItem extends React.PureComponent<Props> {
  handleSelect = () => {
    const { fan, onPress } = this.props
    onPress && onPress(fan.uid)
  }

  render = () => {
    const { fan, subHeader } = this.props
    return (
      <TouchableOpacity style={styles.rowContainer} onPress={this.handleSelect}>
        <View style={styles.profileImageContainer}>
          {fan.photoUrl && (
            <View style={styles.profileImageBorder}>
              <RemoteImage
                style={styles.profileImage}
                source={{
                  uri: `${fan.photoUrl}?width=${PROFILE_IMAGE_SIZE * 2}`,
                }}
                defaultSource={fallbackImages.userProfile}
              />
            </View>
          )}
          <View style={styles.teamlogosContainer}>
            {Object.values(fan.favoriteTeams).map(team => (
              <View style={styles.teamLogoImageBorder} key={team.id}>
                <RemoteImage
                  source={{ uri: getImageUrl(team.id, ItemImageType.TeamLogo) }}
                  defaultSource={fallbackImages.teamLogo}
                  style={styles.teamLogoImage}
                />
              </View>
            ))}
          </View>
        </View>
        <View style={[styles.textContainer]}>
          <Text style={styles.nameText}>{fan.displayName}</Text>
          {subHeader && <Text style={styles.subHeaderText}>{subHeader}</Text>}
        </View>
      </TouchableOpacity>
    )
  }
}

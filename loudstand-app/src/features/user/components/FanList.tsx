import React from 'react'
import { FlatList, ListRenderItem } from 'react-native'
import { UserItem, ID } from '@loudstand/shared'
import FanListItem from './FanListItem'
import { ListSeparator } from '../../../components/ListSeparator'
import { getComparatorForKey } from '../../../utils'

interface Props {
  fans: { [FirebaseUID: string]: UserItem }
  onSelectFan: (userId: ID) => void
  footer?: React.ComponentType<any> | React.ReactElement | null
}

export default class FanList extends React.PureComponent<Props> {
  renderFan: ListRenderItem<ID> = ({ item }) => {
    const { fans, onSelectFan } = this.props
    return <FanListItem fan={fans[item]} onPress={onSelectFan} />
  }

  render = () => {
    const { fans, footer } = this.props
    const sortedFans = Object.values(fans).sort(
      getComparatorForKey('displayName')
    )
    const ids = sortedFans.map(fan => fan.uid)
    return (
      <FlatList
        data={ids}
        keyExtractor={item => item}
        ItemSeparatorComponent={ListSeparator}
        renderItem={this.renderFan}
        ListFooterComponent={footer}
      />
    )
  }
}

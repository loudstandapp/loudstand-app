import React from 'react'
import { Text, StyleSheet, View, TouchableOpacity, Switch } from 'react-native'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { spacingUnit } from '../../../theme/layout'

interface Props<T> {
  title: string
  content?: string
  value?: boolean
  onChange: (id: T, value: boolean) => void
  onPress: (id: T) => void
  presentationStyle: 'even' | 'odd'
  id: T
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: spacingUnit,
    paddingHorizontal: 2 * spacingUnit,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  titleText: {
    ...(fonts.HeaderSmall as object),
  },
  even: {
    backgroundColor: colors.tertiaryLight,
  },
  odd: {
    backgroundColor: 'transparent',
  },
  contentText: {
    ...(fonts.Body as object),
    marginTop: spacingUnit,
  },
  textWrapper: {
    flex: 1,
    // alignItems: 'center',
    justifyContent: 'space-between',
  },
})

export default class SettingsSwitchItem<T> extends React.PureComponent<
  Props<T>
> {
  handlePress = () => {
    const { id, onPress } = this.props
    onPress(id)
  }

  handleChange = (value: boolean) => {
    const { id, onChange } = this.props
    onChange(id, value)
  }

  render = () => {
    const { title, content, presentationStyle, value } = this.props
    return (
      <TouchableOpacity
        style={[
          styles.container,
          presentationStyle === 'even' && styles.even,
          presentationStyle === 'odd' && styles.odd,
        ]}
        onPress={this.handlePress}
      >
        <View style={styles.textWrapper}>
          <Text style={styles.titleText}>{title}</Text>
          {content && <Text style={styles.contentText}>{content}</Text>}
        </View>
        <Switch onValueChange={this.handleChange} value={value} />
      </TouchableOpacity>
    )
  }
}

import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import FanList from './FanList'

const fanList = {
  BLA2YlZMqENnBsSNhcS6a9NAUdG2: {
    favoriteTeams: {
      'sr:competitor:3006': {
        country: 'PT',
        id: 'sr:competitor:3006',
        name: 'Benfica Lisbon',
      },
    },
    facebookUid: '10214857986750898',
    photoUrl: 'https://graph.facebook.com/10214857986750898/picture',
    displayName: 'Foo Bar',
    uid: 'BLA2YlZMqENnBsSNhcS6a9NAUdG2',
  },
  uvBIyMTs0Tb3GrB6Pl1pstf5YfG2: {
    favoriteTeams: {
      'sr:competitor:7': {
        country: null,
        id: 'sr:competitor:7',
        name: 'Crystal Palace',
      },
      'sr:competitor:2699': {
        country: 'IT',
        id: 'sr:competitor:2699',
        name: 'Lazio Roma',
      },
      'sr:competitor:3006': {
        country: 'PT',
        id: 'sr:competitor:3006',
        name: 'Benfica Lisbon',
      },
      'sr:competitor:1281': {
        country: 'DK',
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      'sr:competitor:2673': {
        country: 'DE',
        id: 'sr:competitor:2673',
        name: 'Borussia Dortmund',
      },
      'sr:competitor:2076': {
        country: 'AT',
        id: 'sr:competitor:2076',
        name: 'Wolfsberger AC',
      },
    },
    facebookUid: '2227569560632186',
    photoUrl: 'https://graph.facebook.com/2227569560632186/picture',
    displayName: 'Diogo Agostinho',
    uid: 'uvBIyMTs0Tb3GrB6Pl1pstf5YfG2',
  },
  hJXgIAdP2OUPLQdHXfWwCgpZ3fB2: {
    favoriteTeams: {
      'sr:competitor:2055': {
        country: 'AT',
        id: 'sr:competitor:2055',
        name: 'SK Rapid Wien',
      },
      'sr:competitor:1291': {
        country: 'DK',
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      'sr:competitor:44': {
        country: null,
        id: 'sr:competitor:44',
        name: 'Liverpool FC',
      },
    },
    facebookUid: '2720432564637044',
    photoUrl: 'https://graph.facebook.com/2720432564637044/picture',
    displayName: 'Alberto Conceição',
    uid: 'hJXgIAdP2OUPLQdHXfWwCgpZ3fB2',
  },
}

storiesOf('FanList', module).add('default', () => (
  <FanList fans={fanList} onSelectFan={action('onSelectFan')} />
))

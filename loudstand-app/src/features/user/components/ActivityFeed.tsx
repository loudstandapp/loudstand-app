import React from 'react'
import {
  UserItem,
  UserActivityItem,
  ID,
  UserActivityFeed,
  LocalizedTeams,
  MatchListItem,
} from '@loudstand/shared'
import { FlatList, ListRenderItem, StyleSheet, View } from 'react-native'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import { connect } from 'react-redux'
import UserActivityView from './UserActivityView'
import * as fq from '../../../store/firestoreQueries'
import { ReduxState } from '../../../store/model'
import { spacingUnit } from '../../../theme/layout'
import colors from '../../../theme/colors'

export interface ActivityEntry {
  user: UserItem | null
  activity: UserActivityItem
}

interface ReduxProps {
  activities: ActivityEntry[]
}

interface Props extends ReduxProps {
  users: { [FirebaseUID: string]: UserItem }
  onSelectUser?: (userId: ID) => void
  onSelectMatch: (match: MatchListItem) => void
  onSelectPlayer: (id: ID) => void
  onSelectTeam: (id: ID) => void
  onSelectManager: (id: ID) => void
  localizedTeams?: LocalizedTeams
}

const styles = StyleSheet.create({
  separator: {
    marginVertical: spacingUnit,
    height: StyleSheet.hairlineWidth,
    flex: 1,
    backgroundColor: colors.tertiary,
  },
})
const Separator = () => <View style={styles.separator} />

export class ActivityFeed extends React.PureComponent<Props> {
  renderItem: ListRenderItem<ActivityEntry> = ({ item }) => {
    const {
      onSelectUser,
      onSelectMatch,
      localizedTeams,
      onSelectManager,
      onSelectPlayer,
      onSelectTeam,
    } = this.props
    return (
      <UserActivityView
        userItem={item.user}
        activityItem={item.activity}
        onSelectUser={onSelectUser}
        onSelectMatch={onSelectMatch}
        localizedTeams={localizedTeams}
        onSelectManager={onSelectManager}
        onSelectPlayer={onSelectPlayer}
        onSelectTeam={onSelectTeam}
      />
    )
  }

  render = () => {
    const { activities } = this.props
    return (
      <FlatList
        data={activities}
        renderItem={this.renderItem}
        ItemSeparatorComponent={Separator}
        keyExtractor={(item, index) =>
          item.activity ? item.activity.voteId : index.toString()
        }
      />
    )
  }
}

export function compareByTimestamp(a: ActivityEntry, b: ActivityEntry) {
  if (a.activity.timestamp < b.activity.timestamp) {
    return 1
  }
  if (a.activity.timestamp > b.activity.timestamp) {
    return -1
  }
  return 0
}

const mapStateToProps = (state: ReduxState, props: Props): ReduxProps => {
  const friendIds = Object.keys(props.users)
  const activities: ActivityEntry[] = friendIds
    .reduce((acc: ActivityEntry[], friendId: ID) => {
      const feedContent: UserActivityFeed | null =
        state.firestore.data[fq.userActivities.key(friendId)]
      if (!feedContent) {
        return acc
      }
      return [
        ...acc,
        ...Object.values(feedContent).map(feedItem => {
          return {
            user: props.users[friendId],
            activity: feedItem,
          }
        }),
      ]
    }, [])
    .sort(compareByTimestamp)
  return { activities }
}

export default compose(
  firestoreConnect((props: Props) =>
    Object.keys(props.users).map(id => fq.userActivities.query(id))
  ),
  connect(mapStateToProps)
)(ActivityFeed)

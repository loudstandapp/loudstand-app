import React from 'react'
import { TouchableOpacity, Text, Image, StyleSheet } from 'react-native'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { spacingUnit } from '../../../theme/layout'

interface Props {
  title: string
  onPress: () => void
  presentationStyle: 'even' | 'odd'
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: spacingUnit,
    paddingHorizontal: 2 * spacingUnit,
  },
  titleText: {
    ...(fonts.HeaderSmall as object),
    flex: 1,
  },
  even: {
    backgroundColor: colors.tertiaryLight,
  },
  odd: {
    backgroundColor: 'transparent',
  },
  arrowIndicator: {
    flex: 0,
    marginLeft: spacingUnit,
    tintColor: colors.primaryText,
  },
})

export default class SettingsLinkItem extends React.PureComponent<Props> {
  render = () => {
    const { title, onPress, presentationStyle } = this.props
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[
          styles.container,
          presentationStyle === 'even' && styles.even,
          presentationStyle === 'odd' && styles.odd,
        ]}
      >
        <Text style={styles.titleText}>{title}</Text>
        <Image
          style={styles.arrowIndicator}
          source={require('../../../../assets/icons/buttons/front-arrow.png')}
        />
      </TouchableOpacity>
    )
  }
}

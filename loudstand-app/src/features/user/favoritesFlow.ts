import { FavoriteTeams, TournamentRoot, TeamListItem } from '@loudstand/shared'
import {
  FlowChallenge,
  showChallengeScreen,
  FlowValidator,
} from '../../navigation/conditionalFlowHandler'
import { ScreenNames } from '../../registerScreens'

import { signupFavoriteNavigationProps } from './navigation'
import * as selectors from '../../store/selectors'
import { sleep } from '../../utils'
import { Mode } from './screens/SignupFavoriteScreen'
import { setTournament, setAvailableTeams } from '../../store/actions'
import * as fq from '../../store/firestoreQueries'

export function getTournamentsWithMissingFavoriteTeams(
  favoriteTeams: FavoriteTeams,
  defaultTournaments: TournamentRoot[],
  requirePickedTeam?: boolean
): TournamentRoot[] {
  const missing = defaultTournaments.filter(tournament => {
    const {
      id,
      currentSeason: { id: currentSeasonId },
    } = tournament
    if (
      !favoriteTeams.tournaments ||
      !favoriteTeams.tournaments[id] ||
      !favoriteTeams.tournaments[id].seasons ||
      !favoriteTeams.tournaments[id].seasons[currentSeasonId] ||
      (requirePickedTeam &&
        !favoriteTeams.tournaments[id].seasons[currentSeasonId].team)
    ) {
      return true
    }
  })
  return missing
}

enum PreviousSeasonFavoriteStatus {
  Skipped = 'SKIPPED',
  Set = 'SET',
  NotFound = 'NOT_FOUND',
}

// 1. A favorite was set in previous season. return {status: Set, team: {...}}
// 2. A favorite was skipped in previous season. return {status: Skipped}
// 3. No data for previous season. Return {status: NotFound}
function getFavoriteInPreviousSeason(
  tournament: TournamentRoot,
  favoriteTeams: FavoriteTeams
): { status: PreviousSeasonFavoriteStatus; team?: TeamListItem } {
  const {
    id,
    currentSeason: { id: currentSeasonId },
  } = tournament
  if (
    !favoriteTeams.tournaments ||
    !favoriteTeams.tournaments[id] ||
    !favoriteTeams.tournaments[id].seasons
  ) {
    return { status: PreviousSeasonFavoriteStatus.NotFound }
  }
  const oldSeasonIds = Object.keys(favoriteTeams.tournaments[id].seasons)
    .filter(seasonId => seasonId !== currentSeasonId)
    .sort((a, b) => (a > b ? 1 : a < b ? -1 : 0))
  const [lastSeasonId] = oldSeasonIds
  if (!lastSeasonId) {
    return { status: PreviousSeasonFavoriteStatus.NotFound }
  }
  if (!favoriteTeams.tournaments[id].seasons[lastSeasonId]) {
    return { status: PreviousSeasonFavoriteStatus.NotFound }
  }

  const { team } = favoriteTeams.tournaments[id].seasons[lastSeasonId]
  if (!team) {
    return { status: PreviousSeasonFavoriteStatus.Skipped }
  }
  return {
    status: PreviousSeasonFavoriteStatus.Set,
    team,
  }
}

async function saveFavorites(
  firestore: any,
  selectedTeams: FavoriteTeams,
  uid: string
) {
  firestore
    .collection('users')
    .doc(uid)
    .collection('attributes')
    .doc('favoriteTeams')
    .set(selectedTeams)
}

export const favoriteTeamsChallenge: FlowChallenge = (
  latestComponentId,
  dispatch,
  getState,
  { getFirestore, getFirebase }
) => {
  console.log('favoriteTeamsChallenge')
  const screenName = ScreenNames.SignupFavoriteScreen
  const state = getState()
  const favoriteTeams = selectors.ownFavoriteTeams(state)
  const defaultTournaments = selectors.defaultTournamentsList(state)
  const firestore = getFirestore()
  const firebase = getFirebase()
  const { uid } = firebase.auth().currentUser

  // User has not picked any favorite teams, let him choose among all of them.
  console.log({ favoriteTeams, defaultTournaments })

  if (!favoriteTeams) {
    const promise = new Promise(resolve => {
      const onNext = async (selectedTeams: FavoriteTeams) => {
        console.log({ selectedTeams })
        await saveFavorites(firestore, selectedTeams, uid)
        const chosenTeams = selectors.getChosenTeams(
          defaultTournaments,
          selectedTeams,
          {}
        )
        dispatch(setAvailableTeams(chosenTeams))
        resolve()
      }
      showChallengeScreen({
        screenName,
        latestComponentId,
        passProps: signupFavoriteNavigationProps({
          mode: Mode.Initial,
          allowSkip: false,
          onNext,
          previousFavorite: null,
        }),
        canGoBack: false,
      })
    })

    return { promise, componentId: screenName }
  }

  const promise = new Promise(async resolveAll => {
    // This should never happen, but TS is unhappy without.
    if (!favoriteTeams) {
      return
    }

    // Tournaments where a user has not made an active choice
    const tournamentsWithoutFavorites = getTournamentsWithMissingFavoriteTeams(
      favoriteTeams,
      defaultTournaments
    )

    let updatedFavoriteTeams = favoriteTeams

    /* 
      eslint-disable no-restricted-syntax, default-case, no-fallthrough, no-await-in-loop, no-loop-func, no-continue
      The code below works, but when changing it, please address the eslint rules that's currently disabled.
    */
    for (const tournament of tournamentsWithoutFavorites) {
      const { team, status } = getFavoriteInPreviousSeason(
        tournament,
        favoriteTeams
      )
      const {
        id,
        currentSeason: { id: currentSeasonId },
      } = tournament

      // Tournaments where a user has not picked a team
      const tournamentsWithoutPickedFavorites = getTournamentsWithMissingFavoriteTeams(
        updatedFavoriteTeams,
        defaultTournaments,
        true
      )

      switch (status) {
        // The user has never decided favorites for this tournament. Show all options.
        case PreviousSeasonFavoriteStatus.NotFound:
        // The user had a favorite in the previous season, that will be default choice again.
        case PreviousSeasonFavoriteStatus.Set: {
          await new Promise(resolveTournament => {
            const onNext = async (selectedTeams: FavoriteTeams) => {
              // This should never happen, but TS is unhappy without.
              if (!favoriteTeams) {
                return
              }
              console.log({ selectedTeams })
              updatedFavoriteTeams = {
                tournaments: {
                  ...updatedFavoriteTeams.tournaments,
                  [id]: {
                    seasons: {
                      ...(updatedFavoriteTeams.tournaments &&
                        updatedFavoriteTeams.tournaments[id] &&
                        updatedFavoriteTeams.tournaments[id].seasons),
                      [currentSeasonId]:
                        selectedTeams.tournaments[id].seasons[currentSeasonId],
                    },
                  },
                },
              }

              await saveFavorites(firestore, updatedFavoriteTeams, uid)
              resolveTournament()
            }
            dispatch(setTournament(tournament))
            showChallengeScreen({
              // FIXME new component id
              screenName,
              latestComponentId,
              passProps: signupFavoriteNavigationProps({
                mode: Mode.NewSeason, // TODO maybe more fine grained modes based on the returned status?
                previousFavorite: team || null,
                // As soon as one tournament has a favorite, the rest does not require the user to pick a favorite
                allowSkip:
                  tournamentsWithoutPickedFavorites.length <
                  defaultTournaments.length,
                onNext,
              }),
              canGoBack: false,
            })
          })
        }
        // The user skipped favorites in the previous season, so we won't prompt him again.
        case PreviousSeasonFavoriteStatus.Skipped: {
          continue
        }
      }
    }
    const chosenTeams = selectors.getChosenTeams(
      defaultTournaments,
      updatedFavoriteTeams,
      {}
    )
    dispatch(setAvailableTeams(chosenTeams))
    resolveAll()
  })
  return { promise, componentId: screenName }
}

export const hasValidFavoriteTeams: FlowValidator = async (
  dispatch,
  getState
) => {
  let state = getState()
  const { uid } = state.firebase.auth
  while (state.firestore.status.requesting[fq.favoriteTeams.key(uid)]) {
    console.log('Waiting 100')
    await sleep(100)
    state = getState()
  }
  const defaultTournaments = selectors.defaultTournamentsList(state)
  const favoriteTeams = selectors.ownFavoriteTeams(state)

  console.log({ defaultTournaments, favoriteTeams })
  if (!favoriteTeams) {
    return false
  }

  const tournamentsWithoutFavorites = getTournamentsWithMissingFavoriteTeams(
    favoriteTeams,
    defaultTournaments
  )
  return tournamentsWithoutFavorites.length === 0
}

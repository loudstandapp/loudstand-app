import { Navigation } from 'react-native-navigation'
import {
  TeamListItem,
  FavoriteTeams,
  TeamList,
  ID,
  MatchListItem,
} from '@loudstand/shared'
import { NavigationProps as FansListNavigationProps } from './screens/FansListScreen'
import { NavigationProps as UserProfileNavigationProps } from './screens/UserProfileScreen'
import { NavigationProps as LoginScreenNavigationProps } from './screens/LoginScreen'
import { NavigationProps as SettingsNavigationProps } from './screens/SettingsScreen'
import { NavigationProps as NotificationSettingsNavigationProps } from './screens/NotificationSettingsScreen'
import { NavigationProps as UserActivityHistoryProps } from './screens/UserActivityHistoryScreen'
import {
  NavigationProps as SignupFaviroteNavigationProps,
  Mode,
} from './screens/SignupFavoriteScreen'
import { NavigationProps as SignupFollowNavigationProps } from './screens/SignupFollowScreen'
import { NavigationProps as RequestLocationNavigationProps } from './screens/RequestLocationScreen'
import { NavigationProps as RequestNotificationPermissionNavigationProps } from './screens/RequestNotificationPermissionScreen'
import { NavigationProps as VerifyPhoneNumberNavigationProps } from './screens/VerifyPhoneNumberScreen'

import { ScreenNames } from '../../registerScreens'
import { matchScreenNavigationProps } from '../match/navigation'
import { randomComponentId } from '../../utils'
import {
  playerScreenNavigationProps,
  teamScreenNavigationProps,
  coachScreenNavigationProps,
} from '../stats/navigation'
import { NotificationTypeSettings } from './screens/NotificationSettingsScreen'

export function signupFollowNavigationProps(
  mode: 'signup' | 'change',
  onNext: (selectedTeams: TeamList) => void
): SignupFollowNavigationProps {
  return {
    onNext,
    mode,
  }
}

function notificationSettingsNavigationProps(
  mainComponentId: string,
  settings: NotificationTypeSettings,
  onSave: (settings: NotificationTypeSettings) => Promise<void>
): NotificationSettingsNavigationProps {
  return {
    onBack: () => Navigation.pop(mainComponentId),
    onSave,
    settings,
  }
}

export const settingsNavigationProps = (
  mainComponentId: string
): SettingsNavigationProps => ({
  onSelectNotificationSettings: (
    settings: NotificationTypeSettings,
    onSave: (settings: NotificationTypeSettings) => Promise<void>
  ) => {
    const componentId = randomComponentId(
      ScreenNames.NotificationSettingsScreen
    )
    return Navigation.push(mainComponentId, {
      component: {
        id: componentId,
        name: ScreenNames.NotificationSettingsScreen,
        passProps: notificationSettingsNavigationProps(
          componentId,
          settings,
          onSave
        ),
      },
    })
  },
  onBack: () => Navigation.pop(mainComponentId),
  onChangeFollowingTeams: (onNext: (selectedTeams: TeamList) => void) => {
    const componentId = randomComponentId(ScreenNames.SignupFollowScreen)
    return Navigation.showModal({
      component: {
        name: ScreenNames.SignupFollowScreen,
        id: componentId,
        passProps: signupFollowNavigationProps(
          'change',
          async (selectedTeams: TeamList) => {
            onNext(selectedTeams)
            await Navigation.dismissModal(componentId)
          }
        ),
      },
    })
  },
})

export function userProfileNavigationProps(
  mainComponentId: string,
  userId?: ID
): UserProfileNavigationProps {
  return {
    onSelectPlayer: (id: ID) => {
      const componentId = randomComponentId(ScreenNames.PlayerScreen)
      return Navigation.push(mainComponentId, {
        component: {
          name: ScreenNames.PlayerScreen,
          id: componentId,
          passProps: playerScreenNavigationProps(componentId, id),
        },
      })
    },
    onSelectSettings: () => {
      const componentId = randomComponentId(ScreenNames.SettingsScreen)
      return Navigation.push(mainComponentId, {
        component: {
          name: ScreenNames.SettingsScreen,
          id: componentId,
          passProps: settingsNavigationProps(componentId),
        },
      })
    },
    onBack: () => Navigation.pop(mainComponentId),
    id: userId,
  }
}

export const fansListNavigationProps = (
  mainComponentId: string
): FansListNavigationProps => ({
  onSelectMatch: (match: MatchListItem) => {
    const componentId = randomComponentId(ScreenNames.MatchScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.MatchScreen,
        id: componentId,
        passProps: matchScreenNavigationProps(componentId, match),
      },
    })
  },
  onSelectUser: (userId: ID) => {
    const componentId = randomComponentId(ScreenNames.UserProfileScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.UserProfileScreen,
        id: componentId,
        passProps: userProfileNavigationProps(componentId, userId),
      },
    })
  },
  onSelectPlayer: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.PlayerScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.PlayerScreen,
        id: componentId,
        passProps: playerScreenNavigationProps(componentId, id),
      },
    })
  },
  onSelectTeam: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.TeamScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.TeamScreen,
        id: componentId,
        passProps: teamScreenNavigationProps(componentId, id),
      },
    })
  },
  onSelectManager: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.CoachScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.CoachScreen,
        id: componentId,
        passProps: coachScreenNavigationProps(componentId, id),
      },
    })
  },
})

export function signupFavoriteNavigationProps({
  onNext,
  allowSkip,
  mode,
  previousFavorite,
}: {
  onNext: (favoriteTeams: FavoriteTeams) => void
  allowSkip: boolean
  mode: Mode
  previousFavorite: TeamListItem | null
}): SignupFaviroteNavigationProps {
  return {
    onNext,
    allowSkip,
    mode,
    previousFavorite,
  }
}

export function loginScreenNavigationProps(
  resolver: () => void
): LoginScreenNavigationProps {
  return {
    // onLogin: (email, password) => {
    //   console.log({ email, password })
    //   // Do something
    //   startMainAppFlow()
    // },
    // onSignup: () => startSignupFlow(),
    // onContinueAfterLogin: resolver,
    // onFacebookLogin: () => loginWithFacebook(resolver),
    // onFacebookLoginFinished: getFacebookLoginFinishHandler(resolver),
    resolver,
  }
}

export function userActivityHistoryNavigationProps(
  mainComponentId: string,
  userId: ID
): UserActivityHistoryProps {
  return {
    userId,
    onBack: () => Navigation.pop(mainComponentId),
    onSelectMatch: (match: MatchListItem) => {
      const componentId = randomComponentId(ScreenNames.MatchScreen)
      return Navigation.push(mainComponentId, {
        component: {
          name: ScreenNames.MatchScreen,
          id: componentId,
          passProps: matchScreenNavigationProps(componentId, match),
        },
      })
    },
    onSelectPlayer: (id: ID) => {
      const componentId = randomComponentId(ScreenNames.PlayerScreen)
      return Navigation.push(mainComponentId, {
        component: {
          name: ScreenNames.PlayerScreen,
          id: componentId,
          passProps: playerScreenNavigationProps(componentId, id),
        },
      })
    },
    onSelectTeam: (id: ID) => {
      const componentId = randomComponentId(ScreenNames.TeamScreen)
      return Navigation.push(mainComponentId, {
        component: {
          name: ScreenNames.TeamScreen,
          id: componentId,
          passProps: teamScreenNavigationProps(componentId, id),
        },
      })
    },
    onSelectManager: (id: ID) => {
      const componentId = randomComponentId(ScreenNames.CoachScreen)
      return Navigation.push(mainComponentId, {
        component: {
          name: ScreenNames.CoachScreen,
          id: componentId,
          passProps: coachScreenNavigationProps(componentId, id),
        },
      })
    },
  }
}

export function requestLocationScreenNavigationProps(
  onAccept: () => void,
  onCancel: () => void,
  onReject: () => void
): RequestLocationNavigationProps {
  return {
    onAccept,
    onCancel,
    onReject,
  }
}

export function requestNotificationScreenNavigationProps(
  onAccept: () => void,
  onSkip: () => void
): RequestNotificationPermissionNavigationProps {
  return {
    onAccept,
    onSkip,
  }
}

export function verifyPhoneNumberScreenNavigationProps(
  onRequestVerification: (arg: {
    phoneNumber: string
    onCodeSent: () => void
    onCodeReceived: (code: string) => void
    onError: (code: string) => void
  }) => void,
  onVerify: (verificationCode: string) => Promise<void>,
  onNext: () => void,
  onCancel: () => void
): VerifyPhoneNumberNavigationProps {
  return {
    onNext,
    onRequestVerification,
    onVerify,
    onCancel,
  }
}

export function startLoginFlow() {
  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: ScreenNames.LoginScreen,
              id: ScreenNames.LoginScreen,
              passProps: loginScreenNavigationProps(() => {}),
            },
          },
        ],
      },
    },
  })
}

export async function showAwaitAuthScreen() {
  await Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              id: ScreenNames.AwaitAuthScreen,
              name: ScreenNames.AwaitAuthScreen,
            },
          },
        ],
      },
    },
  })
}

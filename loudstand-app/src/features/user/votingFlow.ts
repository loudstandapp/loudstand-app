import Geolocation, { PositionError } from 'react-native-geolocation-service'
import { Navigation } from 'react-native-navigation'
import { RNFirebase } from 'react-native-firebase'
import { SubmittedVote } from '@loudstand/shared'
import { Platform, Alert, PermissionsAndroid } from 'react-native'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import {
  FlowValidator,
  FlowChallenge,
  conditionalFlowThunk,
} from '../../navigation/conditionalFlowHandler'
import { ScreenNames } from '../../registerScreens'
import {
  verifyPhoneNumberScreenNavigationProps,
  requestLocationScreenNavigationProps,
} from './navigation'
import { showErrorOverlay } from '../../components/ErrorOverlay'
import { translate } from '../../translation'
import {
  setGeolocation,
  clearGeolocation,
  setGeolocationAccess,
  continueWithoutGeolocationPermission,
} from '../../store/actions'
import { submitVoteThunk } from '../../services/voting'
import { randomComponentId, openAppSettings } from '../../utils'

// Soft custom prompt
const MAX_GEOLOCATION_ACCESS_REQUESTS = 5
// Mobile OS geolocation rejected, ask to fix settings
const MAX_GEOLOCATION_PERMISSION_REQUESTS = 5
const MAX_GEOLOCATION_AGE_SECONDS = 600
const GEOLOCATION_TIMEOUT = 30

const isPhoneNumberVerified: FlowValidator = async (dispatch, getState) => {
  const state = getState()
  return !!state.firebase.auth.phoneNumber
}

const phoneNumberVerificationChallenge: FlowChallenge = (
  latestComponentId,
  dispatch,
  getState,
  { getFirebase }
) => {
  const firebase = getFirebase()
  const screenName = ScreenNames.VerifyPhoneNumberScreen
  const componentId = randomComponentId(screenName)
  const promise = new Promise((resolve, reject) => {
    let storedVerificationId: string | undefined

    function requestPhoneNumberVerification({
      phoneNumber,
      onCodeSent,
      onCodeReceived,
      onError,
    }: {
      phoneNumber: string
      onCodeSent: () => void
      onCodeReceived: (code: string) => void
      onError: (code: string) => void
    }) {
      firebase
        .auth()
        .verifyPhoneNumber(phoneNumber)
        .on(
          'state_changed',
          (observerSnap: RNFirebase.PhoneAuthSnapshot) => {
            const { code, state, verificationId } = observerSnap
            switch (state) {
              case firebase.auth.PhoneAuthState.CODE_SENT: {
                storedVerificationId = verificationId
                onCodeSent()
                break
              }
              case firebase.auth.PhoneAuthState.AUTO_VERIFIED: {
                storedVerificationId = verificationId
                code && onCodeReceived(code)
                break
              }
              default:
                break
            }
          },
          (error: RNFirebase.PhoneAuthError) => {
            if (error.code) {
              const localizedError = translate(
                `phoneNumberVerification.errors.${error.code}`
              )
              showErrorOverlay(localizedError)
              onError(error.code)
            }
          }
        )
    }

    async function verifyPhoneNumber(verificationCode: string) {
      const { currentUser } = firebase.auth()
      if (storedVerificationId && currentUser) {
        try {
          const phoneCredential = await firebase.auth.PhoneAuthProvider.credential(
            storedVerificationId,
            verificationCode
          )
          await currentUser.updatePhoneNumber(phoneCredential)
          await firebase.reloadAuth()
        } catch (error) {
          if (error.code) {
            const localizedError = translate(
              `phoneNumberVerification.errors.${error.code}`
            )
            showErrorOverlay(localizedError)
          }
          throw error
        }
      }
    }

    async function next() {
      await Navigation.dismissModal(componentId)
      resolve()
    }

    async function cancel() {
      await Navigation.dismissModal(componentId)
      reject()
    }

    Navigation.showModal({
      component: {
        name: screenName,
        id: componentId,
        passProps: verifyPhoneNumberScreenNavigationProps(
          requestPhoneNumberVerification,
          verifyPhoneNumber,
          next,
          cancel
        ),
      },
    })
  })
  return { promise, componentId }
}

const hasRecentGeolocation: FlowValidator = async (dispatch, getState) => {
  const state = getState()
  const { geolocation } = state.general
  if (!geolocation) {
    return false
  }
  if (geolocation.timestamp + MAX_GEOLOCATION_AGE_SECONDS * 1000 < Date.now()) {
    return false
  }
  return true
}

const updateGeolocationChallenge: FlowChallenge = (
  latestComponentId,
  dispatch,
  getState
) => {
  const screenName = ScreenNames.RequestLocationScreen
  const componentId = randomComponentId(screenName)
  const state = getState()
  const { geolocationAccess, launchCount } = state.general

  const promise = new Promise((resolve, reject) => {
    async function onCancel() {
      await Navigation.dismissModal(componentId)
      reject()
    }

    function handleNoAccess(onContinue: () => void) {
      if (
        geolocationAccess.numberOfContinueWithoutPermission >=
        MAX_GEOLOCATION_PERMISSION_REQUESTS
      ) {
        onContinue()
        return
      }
      Alert.alert(
        translate('requestGeolocation.permissionDeniedDialogTitle'),
        translate('requestGeolocation.permissionDeniedDialogMessage'),
        [
          {
            text: translate('requestGeolocation.openSettings'),
            onPress: () => {
              openAppSettings()
              onCancel()
            },
            style: 'default',
          },
          {
            text: translate('requestGeolocation.continueWithout'),
            onPress: () => {
              dispatch(continueWithoutGeolocationPermission())
              onContinue()
            },
            style: 'destructive',
          },
          {
            text: translate('requestGeolocation.cancel'),
            onPress: onCancel,
            style: 'cancel',
          },
        ]
      )
    }

    async function updateLocationAndProceed(completed: () => void) {
      if (Platform.OS === 'android') {
        const permission = await PermissionsAndroid.request(
          'android.permission.ACCESS_COARSE_LOCATION'
        )
        if (permission !== 'granted') {
          handleNoAccess(() => {
            completed()
          })
          return
        }
      }
      Geolocation.getCurrentPosition(
        position => {
          console.log('LOCATION', position)
          dispatch(
            setGeolocation(
              position.coords.latitude,
              position.coords.longitude,
              position.timestamp
            )
          )
          completed()
        },
        error => {
          console.log({ error })
          if (error.code === PositionError.PERMISSION_DENIED) {
            handleNoAccess(() => {
              completed()
            })
            return
          }
          showErrorOverlay(error.message)
          completed()
        },
        {
          enableHighAccuracy: false,
          maximumAge: MAX_GEOLOCATION_AGE_SECONDS * 1000,
          timeout: GEOLOCATION_TIMEOUT * 1000,
        }
      )
    }

    if (geolocationAccess.isAllowed) {
      updateLocationAndProceed(() => {
        resolve()
      })
      return
    }

    // Skip on first launch after login, or if the user has rejected many times.
    if (
      launchCount === 1 ||
      geolocationAccess.numberOfRejects >= MAX_GEOLOCATION_ACCESS_REQUESTS
    ) {
      dispatch(clearGeolocation())
      resolve()
      return
    }

    async function onAccept() {
      dispatch(setGeolocationAccess(true))
      updateLocationAndProceed(async () => {
        await Navigation.dismissModal(componentId)
        resolve()
      })
    }

    async function onReject() {
      dispatch(setGeolocationAccess(false))
      await Navigation.dismissModal(componentId)
      resolve()
    }

    Navigation.showModal({
      component: {
        name: screenName,
        id: componentId,
        passProps: requestLocationScreenNavigationProps(
          onAccept,
          onCancel,
          onReject
        ),
      },
    })
  })
  return { promise, componentId }
}

const votingRequirements = [
  {
    validator: isPhoneNumberVerified,
    challenge: phoneNumberVerificationChallenge,
  },
  {
    validator: hasRecentGeolocation,
    challenge: updateGeolocationChallenge,
  },
]

export function votingFlowThunk(vote: SubmittedVote) {
  const action = conditionalFlowThunk(votingRequirements, () => {
    console.log('SUBMITTING VOTE')
    ReactNativeHapticFeedback.trigger('notificationSuccess', {
      enableVibrateFallback: true,
      ignoreAndroidSystemSettings: true,
    })
    return submitVoteThunk(vote)
  })
  console.log({ action })
  return action
}

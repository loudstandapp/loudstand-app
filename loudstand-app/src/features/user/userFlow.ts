import FBSDK, { GraphRequest, GraphRequestManager } from 'react-native-fbsdk'
import { Dispatch } from 'redux'
import {
  TeamList,
  UserProfileDetails,
  parseFacebookBirthdate,
  ISO3166v1a2Country,
} from '@loudstand/shared'
import Sentry from 'react-native-sentry'
import {
  conditionalFlowThunk,
  FlowChallenge,
  FlowValidator,
  showChallengeScreen,
} from '../../navigation/conditionalFlowHandler'
import { startMainAppThunk } from '../main/navigation'
import { ScreenNames } from '../../registerScreens'
import {
  loginScreenNavigationProps,
  signupFollowNavigationProps,
} from './navigation'
import { showErrorOverlay } from '../../components/ErrorOverlay'
import { profileReadPermissions } from '../../config/facebook'
import { ReduxState } from '../../store/model'
import { hasValidFavoriteTeams, favoriteTeamsChallenge } from './favoritesFlow'
import * as selectors from '../../store/selectors'
import {
  setAvailableTeams,
  logoutCleanup,
  beginFacebookLogin,
  endFacebookLogin,
} from '../../store/actions'
import { sleep } from '../../utils'
import { disableUserDevice, updateUserDevice } from '../../services/device'
import * as fq from '../../store/firestoreQueries'
import { NotificationTypeSettings } from './screens/NotificationSettingsScreen'

const loginChallenge: FlowChallenge = (
  latestComponentId: string | undefined,
  dispatch,
  getState,
  { getFirebase }
) => {
  const firebase = getFirebase()
  const screenName = ScreenNames.LoginScreen
  const promise = new Promise(async resolve => {
    // Make sure everything is logged out
    try {
      await FBSDK.LoginManager.logOut()
    } catch (e) {
      console.log('FACEBOOK Logout before login error', e)
    }
    try {
      await firebase.logout()
    } catch (e) {
      console.log('FIREBASE Logout before login error', e)
    }
    dispatch(logoutCleanup())
    showChallengeScreen({
      screenName,
      latestComponentId,
      passProps: loginScreenNavigationProps(resolve),
      canGoBack: false,
    })
  })
  return { promise, componentId: screenName }
}

const profileDetailsChallenge: FlowChallenge = () => {
  return { promise: Promise.resolve(), componentId: 'FIXME' }
}

export function saveFollowingTeamsThunk(teams: TeamList, onSaved?: () => void) {
  return async (
    dispatch: Dispatch,
    getState: () => ReduxState,
    { getFirebase, getFirestore }: any
  ) => {
    const firebase = getFirebase()
    const firestore = getFirestore()
    const { currentUser } = firebase.auth()
    const state = getState()
    const defaultTournaments = selectors.defaultTournamentsList(state)
    const favoriteTeams = selectors.ownFavoriteTeams(state)

    if (!favoriteTeams) {
      throw new Error(
        'favoriteTeams is not available in followingTeamsChallenge'
      )
    }
    await firestore
      .collection('users')
      .doc(currentUser.uid)
      .collection('attributes')
      .doc('followingTeams')
      .set(teams)
    const chosenTeams = selectors.getChosenTeams(
      defaultTournaments,
      favoriteTeams,
      teams
    )
    dispatch(setAvailableTeams(chosenTeams))
    onSaved && onSaved()
  }
}

const followingTeamsChallenge: FlowChallenge = (
  latestComponentId,
  dispatch
) => {
  const screenName = ScreenNames.SignupFollowScreen
  const promise = new Promise(resolve => {
    async function onNext(teams: TeamList) {
      // @ts-ignore thunk action type issue
      dispatch(saveFollowingTeamsThunk(teams, resolve))
    }
    showChallengeScreen({
      screenName,
      latestComponentId,
      passProps: signupFollowNavigationProps('signup', onNext),
      canGoBack: false,
    })
  })
  return { promise, componentId: screenName }
}

const isUserSignedIn: FlowValidator = async (dispatch, getState) => {
  const state = getState()
  if (state.firebase.auth.isEmpty) {
    return false
  }
  const accessToken = await FBSDK.AccessToken.getCurrentAccessToken()

  if (!accessToken) {
    return false
  }
  try {
    await FBSDK.AccessToken.refreshCurrentAccessTokenAsync()
  } catch (e) {
    console.error(e)
    return false
  }
  return true
}

const hasProfileDetails: FlowValidator = async (dispatch, getState) => {
  const state = getState()
  // FIXME add more details to check for here
  return !state.firebase.profile.isEmpty
}

const hasFollowingTeams: FlowValidator = async (dispatch, getState) => {
  let state = getState()
  const { uid } = state.firebase.auth
  while (state.firestore.status.requesting[fq.followingTeams.key(uid)]) {
    console.log('Waiting 100')
    await sleep(100)
    state = getState()
  }
  const followingTeams = selectors.followingTeams(state)
  return !!followingTeams
}

const homeScreenRequirements = [
  {
    validator: isUserSignedIn,
    challenge: loginChallenge,
  },
  // {
  //   validator: hasProfileDetails,
  //   challenge: profileDetailsChallenge,
  // },
  {
    validator: hasValidFavoriteTeams,
    challenge: favoriteTeamsChallenge,
  },
  {
    validator: hasFollowingTeams,
    challenge: followingTeamsChallenge,
  },
]

export function userFlowThunk(originComponentId?: string) {
  const action = conditionalFlowThunk(
    homeScreenRequirements,
    startMainAppThunk,
    originComponentId
  )
  return action
}

export function logoutThunk() {
  return async (
    dispatch: Dispatch,
    getState: () => ReduxState,
    { getFirebase }: any
  ) => {
    try {
      const firebase = getFirebase()
      const state = getState()
      const { uid } = state.firebase.auth
      await disableUserDevice(uid) // fixme: also do this after failing userflow?
      await FBSDK.LoginManager.logOut()
      await firebase.logout()
      dispatch(logoutCleanup())
      // @ts-ignore
      dispatch(userFlowThunk())
    } catch (e) {
      showErrorOverlay(e.message)
      Sentry.captureException(e)
    }
  }
}

export function unlinkPhoneNumberThunk() {
  return async (
    dispatch: Dispatch,
    getState: () => ReduxState,
    { getFirebase }: any
  ) => {
    try {
      const firebase = getFirebase()
      const { currentUser } = firebase.auth()
      if (currentUser) {
        await currentUser.unlink(firebase.auth.PhoneAuthProvider.PROVIDER_ID)
        await firebase.reloadAuth()
      }
    } catch (e) {
      showErrorOverlay(e.message)
    }
  }
}

export function saveNotificationSettingsThunk(
  settings: NotificationTypeSettings
) {
  return async (
    dispatch: Dispatch,
    getState: () => ReduxState,
    { getFirestore, getFirebase }: any
  ) => {
    try {
      const firestore = getFirestore()
      const firebase = getFirebase()
      const { currentUser } = firebase.auth()
      if (currentUser) {
        const docRef = firestore
          .collection('users')
          .doc(currentUser.uid)
          .collection('attributes')
          .doc('notificationSettings')
        await docRef.set(
          {
            types: settings,
          },
          { merge: true }
        )
      }
    } catch (e) {
      showErrorOverlay(e.message)
    }
  }
}

function getUserCountry() {
  return new Promise(
    (
      resolve: (countryCode: ISO3166v1a2Country | null) => void,
      reject: (error: object) => void
    ) => {
      const locationRequest = new GraphRequest(
        '/me',
        {
          parameters: {
            fields: { string: 'location{location{country_code}}' },
          },
        },
        (error?: object, result?: object) => {
          if (error) {
            reject(error)
            return
          }
          // @ts-ignore
          if (
            !result ||
            // @ts-ignore
            !result.location ||
            // @ts-ignore
            !result.location.location
          ) {
            resolve(null)
            return
          }

          // @ts-ignore
          const { country_code: countryCode } = result.location.location
          resolve(countryCode || null)
        }
      )
      new GraphRequestManager().addRequest(locationRequest).start()
    }
  )
}

export function loginWithFacebookThunk(resolver: () => void) {
  return async (
    dispatch: Dispatch,
    getState: () => ReduxState,
    { getFirebase, getFirestore }: any
  ) => {
    try {
      dispatch(beginFacebookLogin())
      const firebase = getFirebase()
      const result = await FBSDK.LoginManager.logInWithReadPermissions(
        profileReadPermissions
      )

      if (result.error) {
        showErrorOverlay(result.error)
        dispatch(endFacebookLogin())
        return
      }
      if (result.isCancelled) {
        dispatch(endFacebookLogin())
        return
      }
      const accessTokenResult = await FBSDK.AccessToken.getCurrentAccessToken()
      if (!accessTokenResult) {
        dispatch(endFacebookLogin())
        throw new Error('EMPTY accessTokenResult')
      }
      console.log({ accessTokenResult })
      const credential = firebase.auth.FacebookAuthProvider.credential(
        accessTokenResult.accessToken
      )
      // const response = await firebase
      //   .auth()
      //   .signInAndRetrieveDataWithCredential(credential)
      const response = await firebase.login({ credential })
      firebase.analytics().setUserId(response.user.uid)
      firebase.analytics().logEvent('login', { method: 'facebook' })
      if (
        response &&
        response.additionalUserInfo &&
        response.additionalUserInfo.profile
      ) {
        const { profile } = response.additionalUserInfo

        // Get user country from graph api....
        const country = await getUserCountry()
        const facebookProfileDetails: UserProfileDetails = {
          ...parseFacebookBirthdate(profile.birthday),
          country,
          gender: profile.gender || null,
          facebookProfileLink: profile.link || null,
          firstName: profile.first_name || null,
          lastName: profile.last_name || null,
          middleName: profile.middle_name || null,
          location: profile.location || null,
        }

        // Update profile with facebook data
        const firestore = getFirestore()
        await firestore
          .collection('users')
          .doc(response.user.uid)
          .set({ facebookProfileDetails }, { merge: true })
        await updateUserDevice(response.user.uid)
      }
      dispatch(endFacebookLogin())
      resolver()
    } catch (e) {
      dispatch(endFacebookLogin())
      showErrorOverlay(e.message)
      Sentry.captureException(e)
    }
  }
}

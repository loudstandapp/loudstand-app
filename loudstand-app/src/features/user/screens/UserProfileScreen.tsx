import React, { Component, Key } from 'react'
import { StyleSheet, Text, View, ScrollView, Dimensions } from 'react-native'
import { Dispatch, bindActionCreators, Store, compose } from 'redux'
import { connect } from 'react-redux'
import { RNFirebase } from 'react-native-firebase'
import countries from 'i18n-iso-countries'
import moment from 'moment'
import {
  LineupSuggestionResultDocument,
  MostValuablePlayerResultDocument,
  TeamPerformancelResultDocument,
  ManagerPerformanceResultDocument,
  UserTotalReactionsResultDocument,
  LocalizedTeams,
  TeamListItem,
  ID,
  TeamList,
  PlayerList,
  UserProfileDetails,
  ISO8601Date,
  LabelValuePair,
  TeamRoot,
} from '@loudstand/shared'
import { firestoreConnect } from 'react-redux-firebase'
import { ReduxState } from '../../../store/model'
import colors from '../../../theme/colors'
import ProfileNavigationHeader from '../../../components/ProfileNavigationHeader'
import * as fq from '../../../store/firestoreQueries'
import { translate, currentLocale } from '../../../translation'
import * as actions from '../../../store/actions'
import { showPickerOverlay } from '../../../components/PickerOverlay'
import * as selectors from '../../../store/selectors'
import TeamPickerItem from '../../../components/TeamPickerItem'
import { fallbackImages, ratingToPercentage } from '../../../utils'
import ProfileTopBar from '../../stats/components/ProfileTopBar'
import SwiperContainer, {
  smallInset,
} from '../../../components/SwiperContainer'
import fonts from '../../../theme/fonts'
import { spacingUnit } from '../../../theme/layout'
import PercentageCircleWithLines from '../../../components/PercentageCircleWithLines'
import Top3Players from '../../../components/Top3Players'
import ScreenWrapper from '../../../components/ScreenWrapper'
import SimpleLineupResult from '../../../components/SimpleLineupResult'

const STATS_KEY = 'stats'

export interface NavigationProps {
  id?: string
  onSelectSettings: () => void
  onBack: () => void
  onSelectPlayer: (id: ID) => void
}
const currentLanguageCode = currentLocale.languageCode

interface ReduxProps {
  currentUser?: RNFirebase.User
  // ID of the shown user
  userId: string
  facebookProfileDetails?: UserProfileDetails
  favoriteTeamList?: TeamList
  activeTeam?: TeamListItem
  suggestedLineupForTeam?: LineupSuggestionResultDocument
  topPlayersforTeam?: MostValuablePlayerResultDocument
  teamApproval?: TeamPerformancelResultDocument
  managerApproval?: ManagerPerformanceResultDocument
  reactions?: UserTotalReactionsResultDocument
  localizedTeams?: LocalizedTeams
  teamPlayers?: PlayerList
  isLoading: boolean
  isGenerating: boolean
  teamRoot?: TeamRoot
}

interface ReduxActionProps {
  setActiveTeamForUser: (team: TeamListItem, userId: ID) => void
}

const styles = StyleSheet.create({
  header: {
    textAlign: 'center',
    ...(fonts.HeaderSmall as object),
    marginVertical: spacingUnit,
  },
  slideContainer: {
    flex: 1,
  },
  statsContainer: {},
  statsRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
})

function calculateAge(birthdate: ISO8601Date | null, birthyear: number | null) {
  if (!birthdate && !birthyear) {
    return null
  }
  if (birthdate) {
    return moment().diff(birthdate, 'years')
  }
  const fakeDate = `${birthyear}-07-03`
  return moment().diff(fakeDate, 'years')
}

interface Props extends NavigationProps, ReduxProps, ReduxActionProps {}

const { width } = Dimensions.get('screen')
const topBarHeight = width / 2.5

interface State {
  isStatsSlideVisible: boolean
}
export class UserProfileScreen extends Component<Props, State> {
  state = { isStatsSlideVisible: false }

  swiperContainerRef = React.createRef<SwiperContainer>()

  handleSnapToSlide = (key: Key) => {
    if (key === STATS_KEY) {
      console.log(STATS_KEY)
      this.setState({ isStatsSlideVisible: true })
    }
  }

  componentDidMount = () => {
    this.setTeamIfNeeded()
  }

  componentDidUpdate = (prevProps: Props) => {
    const { favoriteTeamList, activeTeam } = this.props
    if (favoriteTeamList && !prevProps.favoriteTeamList) {
      this.setTeamIfNeeded()
    }
    if (activeTeam !== prevProps.activeTeam) {
      this.setState({ isStatsSlideVisible: false })
      this.swiperContainerRef.current && this.swiperContainerRef.current.reset()
    }
  }

  showTeamSelector = () => {
    const { favoriteTeamList, localizedTeams } = this.props
    if (!favoriteTeamList) {
      return
    }
    showPickerOverlay({
      data: Object.values(favoriteTeamList),
      backgroundColor: colors.navigationBackground,
      renderItem: ({ item }) => (
        <TeamPickerItem item={item} localizedTeams={localizedTeams} />
      ),
      onSelect: this.handleSelectTeam,
    })
  }

  handleSelectTeam = (team: TeamListItem) => {
    const { setActiveTeamForUser, userId } = this.props
    setActiveTeamForUser(team, userId)
    return true
  }

  setTeamIfNeeded = () => {
    const {
      activeTeam,
      favoriteTeamList,
      setActiveTeamForUser,
      userId,
    } = this.props

    if (activeTeam) {
      return
    }
    if (!favoriteTeamList || Object.keys(favoriteTeamList).length === 0) {
      return
    }
    const [team] = Object.values(favoriteTeamList)
    setActiveTeamForUser(team, userId)
  }

  render() {
    const {
      localizedTeams,
      onBack,
      id,
      onSelectSettings,
      activeTeam,
      currentUser,
      reactions,
      suggestedLineupForTeam,
      teamPlayers,
      topPlayersforTeam,
      teamApproval,
      managerApproval,
      facebookProfileDetails,
      isLoading,
      isGenerating,
      onSelectPlayer,
      teamRoot,
    } = this.props
    const { isStatsSlideVisible } = this.state
    const topBarData: LabelValuePair[] = []
    if (facebookProfileDetails) {
      const age = calculateAge(
        facebookProfileDetails.birthdate,
        facebookProfileDetails.birthyear
      )
      age &&
        topBarData.push({
          label: translate('userProfile.age'),
          value: age.toString(),
        })

      facebookProfileDetails.location &&
        topBarData.push({
          label: translate('userProfile.location'),
          value: facebookProfileDetails.location.name,
        })

      facebookProfileDetails.country &&
        topBarData.push({
          label: translate('userProfile.country'),
          value: countries.getName(
            facebookProfileDetails.country,
            currentLanguageCode
          ),
        })
    }
    reactions &&
      topBarData.push({
        label: translate('userProfile.numberOfReactions'),
        value: reactions.reactions.toString(),
      })

    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        {currentUser && (
          <ProfileNavigationHeader
            backgroundHeight={topBarHeight}
            defaultBackgroundSource={fallbackImages.userProfile}
            backgroundSource={
              currentUser.avatarUrl // FIXME USER PROFILE STRUCTURE
                ? {
                    uri: `${currentUser.avatarUrl}?width=500`,
                  }
                : fallbackImages.userProfile
            }
            onBack={onBack}
            title={(currentUser && currentUser.displayName) || 'N/A'}
            onSelectTeam={this.showTeamSelector}
            team={activeTeam}
            onSelectSettings={!id ? onSelectSettings : undefined}
            localizedTeams={localizedTeams}
          />
        )}
        {topBarData && <ProfileTopBar data={topBarData} />}
        <SwiperContainer
          ref={this.swiperContainerRef}
          onSnapToSlide={this.handleSnapToSlide}
        >
          {suggestedLineupForTeam && (
            <View style={styles.slideContainer}>
              <ScrollView contentContainerStyle={{ paddingTop: smallInset }}>
                {suggestedLineupForTeam && teamPlayers && (
                  <SimpleLineupResult
                    onSelectPlayer={onSelectPlayer}
                    teamPlayers={teamPlayers}
                    lineupSuggestionResult={suggestedLineupForTeam}
                    jerseyColors={
                      teamRoot && teamRoot.jerseyColors
                        ? teamRoot.jerseyColors
                        : undefined
                    }
                    title={translate('userProfile.suggestedLineup')}
                  />
                )}
              </ScrollView>
            </View>
          )}

          <View style={styles.slideContainer} key={STATS_KEY}>
            <ScrollView contentContainerStyle={{ paddingTop: smallInset }}>
              <View style={styles.statsRow}>
                {teamApproval && (
                  <PercentageCircleWithLines
                    title={translate('userProfile.teamApproval')}
                    percentage={ratingToPercentage(teamApproval.rating)}
                    shouldAnimate
                    isVisible={isStatsSlideVisible || !suggestedLineupForTeam}
                  />
                )}
                {managerApproval && (
                  <PercentageCircleWithLines
                    title={translate('userProfile.managerApproval')}
                    percentage={ratingToPercentage(managerApproval.rating)}
                    shouldAnimate
                    isVisible={isStatsSlideVisible || !suggestedLineupForTeam}
                  />
                )}
              </View>
              {topPlayersforTeam && teamPlayers && (
                <>
                  <Text style={styles.header}>
                    {translate('userProfile.mostValuablePlayers')}
                  </Text>
                  <Top3Players
                    onSelectPlayer={onSelectPlayer}
                    teamPlayers={teamPlayers}
                    results={topPlayersforTeam}
                  />
                </>
              )}
            </ScrollView>
          </View>
        </SwiperContainer>
      </ScreenWrapper>
    )
  }
}

const mapStateToProps = (state: ReduxState, ownProps: Props): ReduxProps => {
  const userId = ownProps.id || state.firebase.auth.uid
  const activeTeam = state.general.activeTeamForUser[userId]
  const {
    [fq.favoriteTeams.key(userId)]: favoriteTeams,
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
    [fq.reactionsByUser.key(userId)]: reactions,
    [fq.userProfile.key(userId)]: currentUser,
  } = state.firestore.data
  const defaultTournaments = selectors.defaultTournamentsList(state)

  const basic = {
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
    userId,
    currentUser,
    facebookProfileDetails: currentUser && currentUser.facebookProfileDetails,
    reactions,
    localizedTeams: localizedTeams && localizedTeams.body,
    favoriteTeamList:
      favoriteTeams &&
      selectors.getFavoriteTeams(defaultTournaments, favoriteTeams),
  }
  if (!activeTeam) {
    return basic
  }
  const teamId = activeTeam.id
  const {
    [fq.teamMostValuablePlayersByUser.key({
      userId,
      teamId,
    })]: topPlayersforTeam,
    [fq.teamPerformanceByUser.key({ userId, teamId })]: teamApproval,
    [fq.managerPerformanceByUser.key({
      userId,
      teamId,
    })]: managerApproval,
    [fq.teamSuggestedLineupByUser.key({
      userId,
      teamId,
    })]: suggestedLineupForTeam,
    [fq.teamPlayers.key(teamId)]: teamPlayers,
    [fq.teamRoot.key(teamId)]: teamRoot,
  } = state.firestore.data
  return {
    ...basic,
    userId,
    topPlayersforTeam,
    teamApproval,
    managerApproval,
    suggestedLineupForTeam,
    activeTeam,
    teamRoot: teamRoot && teamRoot.body,
    teamPlayers: teamPlayers && teamPlayers.body,
  }
}

const mapDispatchToProps = (dispatch: Dispatch): ReduxActionProps =>
  bindActionCreators(
    {
      setActiveTeamForUser: actions.setActiveTeamForUser,
    },
    dispatch
  )

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const userId = props.id || state.firebase.auth.uid
    const activeTeam = state.general.activeTeamForUser[userId]
    if (!userId) {
      return []
    }
    const basic = [
      fq.localizedTeams.query(currentLanguageCode),
      fq.reactionsByUser.query(userId),
      fq.favoriteTeams.query(userId),
      fq.userProfile.query(userId),
      fq.allTournaments.query(),
    ]

    if (!activeTeam) {
      return basic
    }
    return [
      ...basic,
      fq.teamSuggestedLineupByUser.query({
        userId,
        teamId: activeTeam.id,
      }),
      fq.managerPerformanceByUser.query({
        userId,
        teamId: activeTeam.id,
      }),
      fq.teamPerformanceByUser.query({
        userId,
        teamId: activeTeam.id,
      }),
      fq.teamMostValuablePlayersByUser.query({
        userId,
        teamId: activeTeam.id,
      }),
      fq.teamRoot.query(activeTeam.id),
      fq.teamPlayers.query(activeTeam.id),
    ]
  })
  // @ts-ignore https://github.com/prescottprue/react-redux-firebase/issues/682
)(UserProfileScreen)

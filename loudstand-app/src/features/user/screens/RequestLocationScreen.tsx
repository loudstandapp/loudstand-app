import React from 'react'
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  StyleSheet,
  SafeAreaView,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Color from 'color'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import colors from '../../../theme/colors'
import fonts from '../../../theme/fonts'
import { spacingUnit, marginDefault } from '../../../theme/layout'
import { translate } from '../../../translation'

export interface NavigationProps {
  onAccept: () => void
  onCancel: () => void
  onReject: () => void
}

interface Props extends NavigationProps {}

const styles = StyleSheet.create({
  backgroundSolid: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: colors.primary,
  },
  backgroundGradient: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  globeImage: {
    marginVertical: 20,
  },
  header: {
    ...(fonts.TopBarTitle as object),
    color: colors.secondaryText,
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  message: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
    margin: marginDefault,
    textAlign: 'center',
    flex: 1,
    alignContent: 'flex-start',
  },
  acceptButton: {
    backgroundColor: colors.background,
    paddingVertical: spacingUnit,
    borderRadius: spacingUnit / 2,
  },
  acceptButtonText: {
    ...(fonts.Header as object),
    color: colors.primaryText,
    textAlign: 'center',
  },
  rejectButtonText: {
    ...(fonts.BodyLarge as object),
    color: colors.secondaryText,
    paddingVertical: spacingUnit,
    textAlign: 'center',
  },
  cancelButton: {
    alignItems: 'flex-end',
    padding: spacingUnit,
    width: '100%',
  },
  imageWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  textWrapper: {
    flex: 1,
    alignItems: 'center',
  },
  buttonWrapper: {
    margin: marginDefault,
  },
  topButtonWrapper: {
    alignItems: 'flex-end',
  },
  closeButton: {
    margin: marginDefault,
    tintColor: colors.secondaryText,
  },
})

export default class RequestLocationScreen extends React.PureComponent<Props> {
  componentDidMount = () => {
    ReactNativeHapticFeedback.trigger('notificationWarning', {
      enableVibrateFallback: true,
      ignoreAndroidSystemSettings: true,
    })
  }

  render = () => {
    const { onAccept, onCancel, onReject } = this.props
    return (
      <View style={styles.backgroundSolid}>
        <LinearGradient
          colors={[
            Color(colors.secondary)
              .alpha(0.5)
              .toString(),
            colors.secondary,
          ]}
          style={styles.backgroundGradient}
        >
          <SafeAreaView style={styles.container}>
            <View style={styles.topButtonWrapper}>
              <TouchableOpacity onPress={onCancel} style={styles.cancelButton}>
                <Image
                  source={require('../../../../assets/icons/buttons/close.png')}
                  style={styles.closeButton}
                />
              </TouchableOpacity>
            </View>

            <View style={styles.imageWrapper}>
              <Image
                style={[styles.globeImage, { width: 150, height: 150 }]}
                source={require('../../../../assets/icons/indicators/phone-confirmation.png')}
              />
            </View>

            <View style={styles.textWrapper}>
              <Text style={styles.header}>
                {translate('requestGeolocation.title')}
              </Text>
              <Text style={styles.message}>
                {translate('requestGeolocation.message')}
              </Text>
            </View>

            <View style={styles.buttonWrapper}>
              <TouchableOpacity onPress={onAccept} style={styles.acceptButton}>
                <Text style={styles.acceptButtonText}>
                  {translate('requestGeolocation.acceptButtonText')}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={onReject}>
                <Text style={styles.rejectButtonText}>
                  {translate('requestGeolocation.rejectButtonText')}
                </Text>
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </LinearGradient>
      </View>
    )
  }
}

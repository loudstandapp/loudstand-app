import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  SafeAreaView,
  ListRenderItemInfo,
  Alert,
  View,
} from 'react-native'
import { compose, bindActionCreators, Dispatch, Store } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import {
  TeamListItem,
  TeamList,
  TournamentRoot,
  LocalizedTeams,
  ID,
  FavoriteTeams,
} from '@loudstand/shared'
import { Options } from 'react-native-navigation'
import ScreenWrapper from '../../../components/ScreenWrapper'
import { TeamSelector } from '../components/TeamSelector'
import { setTournament } from '../../../store/actions'
import { ReduxState } from '../../../store/model'
import { currentLocale, translate } from '../../../translation'
import fonts from '../../../theme/fonts'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import TournamentSelectorButton from '../../../components/TournamentSelectorButton'
import { showPickerOverlay } from '../../../components/PickerOverlay'
import * as selectors from '../../../store/selectors'
import TournamentPickerItem, {
  tournamentListToArray,
} from '../../../components/TournamentPickerItem'
import CurrentUserIndicator from '../../../components/CurrentUserIndicator'
import * as fq from '../../../store/firestoreQueries'
import SubmitButton from '../../../components/SubmitButton'

const currentLanguageCode = currentLocale.languageCode

function getFavoriteStatus(
  tournaments: TournamentRoot[],
  favoriteTeams: FavoriteTeams
) {
  const tournamentsWithFavoriteTeam = tournaments.filter(tournament => {
    const {
      id,
      currentSeason: { id: currentSeasonId },
    } = tournament
    if (!favoriteTeams.tournaments[id]) {
      return false
    }
    if (!favoriteTeams.tournaments[id].seasons[currentSeasonId]) {
      return false
    }
    if (!favoriteTeams.tournaments[id].seasons[currentSeasonId].team) {
      return false
    }
    return true
  })
  return {
    total: tournaments.length,
    skipped: tournaments.length - tournamentsWithFavoriteTeam.length,
    picked: tournamentsWithFavoriteTeam.length,
  }
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    alignItems: 'stretch',
  },
  topHeader: {
    ...(fonts.SuperLarge as object),
    margin: marginDefault,
  },
  buttonWrapper: {
    alignItems: 'center',
    marginBottom: spacingUnit,
  },
})

export enum Mode {
  Initial = 'INITIAL',
  NewSeason = 'NEW_SEASON',
}

export interface NavigationProps {
  mode: Mode
  onNext: (selectedTeams: FavoriteTeams) => void
  allowSkip: boolean
  previousFavorite: TeamListItem | null
  // TODO: Facebook, google, twitter
}

interface ReduxProps {
  defaultTournaments?: { [key in ID]: TournamentRoot }
  teams?: TeamList
  activeTournament?: TournamentRoot
  localizedTeams?: LocalizedTeams
  isLoading: boolean
  isGenerating: boolean
}

interface ReduxActionProps {
  setTournament: typeof setTournament
}

interface FavoriteStatus {
  total: number
  picked: number
  skipped: number
}

interface State {
  selectedTeams?: FavoriteTeams
  favoriteStatus?: FavoriteStatus
}

interface Props extends NavigationProps, ReduxProps, ReduxActionProps {}
export class SignupFavoriteScreen extends Component<Props, State> {
  static options = (): Options => ({
    statusBar: {
      style: 'dark',
    },
  })

  state: State = { selectedTeams: undefined, favoriteStatus: undefined }

  componentDidMount = () => {
    this.setInitialTournament()
    this.setInitialSelectedTeams()
  }

  componentDidUpdate = () => {
    this.setInitialTournament()
    this.setInitialSelectedTeams()
  }

  setInitialSelectedTeams = () => {
    const { selectedTeams } = this.state
    if (selectedTeams) {
      return
    }
    const {
      mode,
      defaultTournaments,
      activeTournament,
      teams,
      previousFavorite,
    } = this.props
    if (!defaultTournaments || !teams || !activeTournament) {
      return
    }
    if (mode === Mode.Initial) {
      const numberOfTournaments = Object.keys(defaultTournaments).length
      const selectedTeams = Object.values(defaultTournaments).reduce(
        (acc: FavoriteTeams, tournament) => {
          return {
            tournaments: {
              ...acc.tournaments,
              [tournament.id]: {
                seasons: {
                  [tournament.currentSeason.id]: {
                    team: null,
                  },
                },
              },
            },
          }
        },
        { tournaments: {} }
      )
      this.setState({
        selectedTeams,
        favoriteStatus: {
          picked: 0,
          total: numberOfTournaments,
          skipped: numberOfTournaments,
        },
      })
    } else {
      const {
        id,
        currentSeason: { id: currentSeasonId },
      } = activeTournament

      const team = (previousFavorite && teams[previousFavorite.id]) || null

      this.setState({
        selectedTeams: {
          tournaments: {
            [id]: {
              seasons: {
                [currentSeasonId]: {
                  team,
                },
              },
            },
          },
        },
        favoriteStatus: {
          picked: team ? 1 : 0,
          total: 1,
          skipped: team ? 0 : 1,
        },
      })
    }
  }

  setInitialTournament = () => {
    const { activeTournament, defaultTournaments, setTournament } = this.props
    if (
      defaultTournaments &&
      Object.values(defaultTournaments).length > 0 &&
      (!activeTournament || !defaultTournaments[activeTournament.id])
    ) {
      const tournament = Object.values(defaultTournaments)[0]
      setTournament(tournament)
    }
  }

  handleSelectTournament = (selectedTournament: TournamentRoot) => {
    // eslint-disable-next-line react/destructuring-assignment
    this.props.setTournament(selectedTournament)
    return true
  }

  handleTeamSelectionChange = (selected: TeamList) => {
    // FIXME Make support for exclusive tournament
    const { activeTournament, defaultTournaments, mode } = this.props
    const currentSeasonId =
      activeTournament && activeTournament.currentSeason.id
    const tournamentId = activeTournament && activeTournament.id

    if (
      !currentSeasonId ||
      !tournamentId ||
      !activeTournament ||
      !defaultTournaments
    ) {
      return
    }

    const team = Object.values(selected)[0] || null
    this.setState(prevState => {
      if (!prevState.selectedTeams) {
        return {}
      }
      const selectedTeams = {
        tournaments: {
          ...prevState.selectedTeams.tournaments,
          [tournamentId]: {
            seasons: {
              ...(prevState.selectedTeams.tournaments[tournamentId] &&
                prevState.selectedTeams.tournaments[tournamentId].seasons),
              [currentSeasonId]: { team },
            },
          },
        },
      }
      const tournaments =
        mode === Mode.Initial
          ? Object.values(defaultTournaments)
          : [activeTournament]
      const favoriteStatus = getFavoriteStatus(tournaments, selectedTeams)
      return { selectedTeams, favoriteStatus }
    })
  }

  handleNextInitial = () => {
    const { onNext } = this.props
    const { selectedTeams, favoriteStatus } = this.state
    if (!favoriteStatus) {
      return
    }

    function next() {
      if (!selectedTeams) {
        return
      }
      // FIXME obj vs array
      const teams = Object.values(selectedTeams).reduce(
        (acc: TeamList, team: TeamListItem) => ({
          ...acc,
          [team.id]: team,
        }),
        {}
      )
      onNext(selectedTeams)
    }
    if (favoriteStatus.skipped === 0) {
      next()
    }

    if (favoriteStatus.picked === 0) {
      return
    }

    if (favoriteStatus.skipped > 0) {
      Alert.alert(
        translate('signupFavorite.skipTournamentsWarningTitle'),
        translate('signupFavorite.skipTournamentsWarningMessage'),
        [
          {
            text: translate('signupFavorite.skipTournamentsWarningGoBack'),
            style: 'cancel',
          },
          {
            text: translate('signupFavorite.skipTournamentsWarningContinue'),
            style: 'default',
            onPress: next,
          },
        ]
      )
    }
  }

  handleNextNewSeason = () => {
    const { allowSkip, activeTournament, onNext } = this.props
    const { selectedTeams } = this.state
    if (!activeTournament) {
      return
    }

    function next() {
      if (!selectedTeams) {
        return
      }
      onNext(selectedTeams)
    }

    const {
      id,
      currentSeason: { id: currentSeasonId },
    } = activeTournament
    if (!selectedTeams) {
      return
    }
    const { team } = selectedTeams.tournaments[id].seasons[currentSeasonId]

    if (!team && !allowSkip) {
      return
    }
    if (!team) {
      Alert.alert(
        translate('signupFavorite.skipNewSeasonWarningTitle'),
        translate('signupFavorite.skipNewSeasonWarningMessage'),
        [
          {
            text: translate('signupFavorite.skipTournamentsWarningGoBack'),
            style: 'cancel',
          },
          {
            text: translate('signupFavorite.skipTournamentsWarningContinue'),
            style: 'default',
            onPress: next,
          },
        ]
      )
    } else {
      next()
    }
  }

  handleNext = () => {
    const { mode } = this.props
    mode === Mode.Initial
      ? this.handleNextInitial()
      : this.handleNextNewSeason()
  }

  showTournamentSelector = () => {
    const { defaultTournaments } = this.props
    showPickerOverlay({
      data: tournamentListToArray(defaultTournaments),
      renderItem: ({ item }: ListRenderItemInfo<TournamentRoot>) => (
        <TournamentPickerItem item={item} />
      ),
      onSelect: this.handleSelectTournament,
      backgroundColor: TournamentPickerItem.backgroundColor,
    })
  }

  render() {
    const {
      activeTournament,
      teams,
      localizedTeams,
      allowSkip,
      mode,
      isLoading,
      isGenerating,
      defaultTournaments,
    } = this.props
    const { selectedTeams, favoriteStatus } = this.state

    const currentSeasonId =
      activeTournament && activeTournament.currentSeason.id
    const tournamentId = activeTournament && activeTournament.id
    const selectedTeam =
      selectedTeams &&
      currentSeasonId &&
      tournamentId &&
      selectedTeams.tournaments[tournamentId] &&
      selectedTeams.tournaments[tournamentId].seasons &&
      selectedTeams.tournaments[tournamentId].seasons[currentSeasonId].team

    const singleTournament =
      defaultTournaments &&
      Object.values(defaultTournaments).length === 1 &&
      Object.values(defaultTournaments)[0]

    const shouldShowSelectorButton =
      activeTournament &&
      defaultTournaments &&
      (mode === Mode.NewSeason || !singleTournament)

    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <SafeAreaView style={styles.safeArea}>
          <Text style={styles.topHeader}>
            {mode === Mode.Initial &&
              (singleTournament
                ? translate('signupFavorite.initialMessageOneTournamentOnly', {
                    name: singleTournament.currentSeason.name,
                  })
                : translate('signupFavorite.initialMessage'))}
            {mode === Mode.NewSeason &&
              translate('signupFavorite.newSeasonMessage')}
          </Text>
          {shouldShowSelectorButton && (
            <TournamentSelectorButton
              disabled={mode === Mode.NewSeason}
              onPress={this.showTournamentSelector}
            />
          )}
          {teams && (
            <TeamSelector
              localizedTeams={localizedTeams}
              selectedTeams={
                selectedTeam ? { [selectedTeam.id]: selectedTeam } : {}
              }
              teams={teams}
              onChange={this.handleTeamSelectionChange}
            />
          )}

          <View style={styles.buttonWrapper}>
            <SubmitButton
              title={translate('signupFavorite.next')}
              onSubmit={this.handleNext}
              disabled={
                !allowSkip && (!favoriteStatus || favoriteStatus.picked === 0)
              }
            />
          </View>
        </SafeAreaView>
        <CurrentUserIndicator />
      </ScreenWrapper>
    )
  }
}

const mapStateToProps = (state: ReduxState): ReduxProps => {
  const { activeTournament } = state.general
  const defaultTournaments = selectors.defaultTournamentBodies(state)
  const basic = {
    defaultTournaments,
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
  }
  if (!activeTournament) {
    return basic
  }
  console.log({ activeTournament })
  const {
    [fq.seasonTeams.key(activeTournament.currentSeason.id)]: teams,
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
  } = state.firestore.data

  return {
    ...basic,
    teams: teams && teams.body,
    localizedTeams: localizedTeams && localizedTeams.body,
    activeTournament,
  }
}

const mapDispatchToProps = (dispatch: Dispatch): ReduxActionProps =>
  bindActionCreators(
    {
      setTournament,
    },
    dispatch
  )

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const { activeTournament } = state.general
    if (!activeTournament) {
      return [fq.allTournaments.query()]
    }
    return [
      fq.allTournaments.query(),
      fq.seasonTeams.query(activeTournament.currentSeason.id),
      fq.localizedTeams.query(currentLanguageCode),
    ]
  })
  // @ts-ignore https://github.com/prescottprue/react-redux-firebase/issues/682
)(SignupFavoriteScreen)

import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  SafeAreaView,
  ListRenderItemInfo,
  View,
} from 'react-native'
import {
  TournamentRoot,
  TeamList,
  LocalizedTeams,
  TeamListItem,
  ID,
} from '@loudstand/shared'
import { compose, bindActionCreators, Dispatch, Store } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Options } from 'react-native-navigation'
import ScreenWrapper from '../../../components/ScreenWrapper'
import { TeamSelector } from '../components/TeamSelector'
import * as actions from '../../../store/actions'
import { ReduxState, ChosenTeamList } from '../../../store/model'
import { currentLocale, translate } from '../../../translation'
import * as fq from '../../../store/firestoreQueries'
import * as selectors from '../../../store/selectors'
import { objectFromArrayWithIds } from '../../../utils'
import CurrentUserIndicator from '../../../components/CurrentUserIndicator'
import TournamentSelectorButton from '../../../components/TournamentSelectorButton'
import { showPickerOverlay } from '../../../components/PickerOverlay'
import TournamentPickerItem, {
  tournamentListToArray,
} from '../../../components/TournamentPickerItem'
import { defaultTournamentBodies } from '../../../store/selectors'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import SubmitButton from '../../../components/SubmitButton'

const currentLanguageCode = currentLocale.languageCode

export interface NavigationProps {
  onNext: (selectedTeams: TeamList) => void
  mode: 'signup' | 'change'
}

interface ReduxProps {
  defaultTournaments?: { [key in ID]: TournamentRoot }
  teams?: TeamList
  activeTournament?: TournamentRoot
  availableTeams: ChosenTeamList
  localizedTeams?: LocalizedTeams
  isLoading: boolean
  isGenerating: boolean
}

interface ReduxActionProps {
  setTournament: typeof actions.setTournament
}

interface State {
  selectedTeams: TeamList
  disabledTeams: TeamList
}

const styles = StyleSheet.create({
  safeArea: {
    flex: 1,
    alignItems: 'stretch',
  },
  topHeader: {
    margin: marginDefault,
    ...(fonts.SuperLarge as object),
  },
  buttonWrapper: {
    alignItems: 'center',
    marginBottom: spacingUnit,
  },
})

interface Props extends NavigationProps, ReduxProps, ReduxActionProps {}
export class SignupFollowScreen extends Component<Props, State> {
  public static options = (): Options => ({
    statusBar: {
      style: 'dark',
    },
  })

  state: State = { selectedTeams: {}, disabledTeams: {} }

  componentDidMount = () => {
    this.handleUpdate()
    this.setInitialTournament()
  }

  componentDidUpdate = (prevProps: Props) => {
    this.setInitialTournament()

    const { availableTeams } = this.props
    if (prevProps.availableTeams !== availableTeams) {
      this.handleUpdate()
    }
  }

  handleUpdate = () => {
    const { availableTeams } = this.props
    const disabledTeams = objectFromArrayWithIds(
      Object.values(availableTeams).filter(team => team.isFavorite)
    )
    this.setState(prevState => {
      const selectedTeams = objectFromArrayWithIds(
        Object.values(availableTeams).filter(team => !team.isFavorite)
      )
      return {
        disabledTeams,
        selectedTeams: { ...prevState.selectedTeams, ...selectedTeams },
      }
    })
  }

  setInitialTournament = () => {
    const { activeTournament, defaultTournaments, setTournament } = this.props
    if (
      defaultTournaments &&
      Object.values(defaultTournaments).length > 0 &&
      (!activeTournament || !defaultTournaments[activeTournament.id])
    ) {
      const tournament = Object.values(defaultTournaments)[0]
      setTournament(tournament)
    }
  }

  handleSelect = (team: TeamListItem) => {
    this.setState(prevState => ({
      selectedTeams: {
        ...prevState.selectedTeams,
        [team.id]: team,
      },
    }))
  }

  handleDeselect = (team: TeamListItem) => {
    this.setState(prevState => {
      const { [team.id]: removed, ...selectedTeams } = prevState.selectedTeams
      return { selectedTeams }
    })
  }

  handleSelectTournament = (selectedTournament: TournamentRoot) => {
    // eslint-disable-next-line react/destructuring-assignment
    this.props.setTournament(selectedTournament)
    return true
  }

  handleNext = () => {
    const { onNext } = this.props
    const { selectedTeams } = this.state
    onNext(selectedTeams)
  }

  showTournamentSelector = () => {
    const { defaultTournaments } = this.props
    showPickerOverlay({
      data: tournamentListToArray(defaultTournaments),
      renderItem: ({ item }: ListRenderItemInfo<TournamentRoot>) => (
        <TournamentPickerItem item={item} />
      ),
      onSelect: this.handleSelectTournament,
      backgroundColor: TournamentPickerItem.backgroundColor,
    })
  }

  render() {
    const {
      activeTournament,
      teams,
      localizedTeams,
      mode,
      isLoading,
      isGenerating,
      defaultTournaments,
    } = this.props
    const { selectedTeams, disabledTeams } = this.state
    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <SafeAreaView style={styles.safeArea}>
          <Text style={styles.topHeader}>
            {mode === 'signup' && translate('signupFollow.message')}
            {mode === 'change' && translate('signupFollow.changeMessage')}
          </Text>
          {activeTournament && defaultTournaments && (
            <TournamentSelectorButton
              disabled={Object.keys(defaultTournaments).length === 1}
              onPress={this.showTournamentSelector}
            />
          )}
          {teams && (
            <TeamSelector
              allowsMultiple
              teams={teams}
              onSelect={this.handleSelect}
              onDeselect={this.handleDeselect}
              selectedTeams={selectedTeams}
              localizedTeams={localizedTeams}
              disabledTeams={disabledTeams}
            />
          )}
          <View style={styles.buttonWrapper}>
            <SubmitButton
              title={
                mode === 'change'
                  ? translate('signupFollow.done')
                  : translate('signupFollow.next')
              }
              onSubmit={this.handleNext}
            />
          </View>
        </SafeAreaView>
        {mode === 'signup' && <CurrentUserIndicator />}
      </ScreenWrapper>
    )
  }
}

const mapStateToProps = (state: ReduxState): ReduxProps => {
  const defaultTournaments = defaultTournamentBodies(state)

  const { availableTeams, activeTournament } = state.general
  const basic = {
    defaultTournaments,
    availableTeams,
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
  }
  if (!activeTournament) {
    return basic
  }
  const {
    [fq.seasonTeams.key(activeTournament.currentSeason.id)]: teams,
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
  } = state.firestore.data
  return {
    ...basic,
    teams: teams && teams.body,
    localizedTeams: localizedTeams && localizedTeams.body,
    activeTournament,
  }
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const mapDispatchToProps = (dispatch: Dispatch): ReduxActionProps =>
  bindActionCreators(
    {
      setTournament: actions.setTournament,
    },
    dispatch
  )

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const { activeTournament } = state.general
    if (!activeTournament) {
      return [fq.allTournaments.query()]
    }
    return [
      fq.allTournaments.query(),
      fq.seasonTeams.query(activeTournament.currentSeason.id),
      fq.localizedTeams.query(currentLanguageCode),
    ]
  })
  // @ts-ignore https://github.com/prescottprue/react-redux-firebase/issues/682
)(SignupFollowScreen)

import React, { Component } from 'react'
import {
  StyleSheet,
  SectionListData,
  SectionBase,
  SectionList,
  SectionListRenderItemInfo,
  SafeAreaView,
  Alert,
  View,
} from 'react-native'

import { NotificationType } from '@loudstand/shared'
import ScreenWrapper from '../../../components/ScreenWrapper'
import SettingsSectionHeader from '../components/SettingsSectionHeader'
import fonts from '../../../theme/fonts'
import { spacingUnit, marginDefault } from '../../../theme/layout'
import { translate } from '../../../translation'
import GenericNavigationHeader from '../../../components/GenericNavigationHeader'
import SettingsSwitchItem from '../components/SettingsSwitchItem'
import SubmitButton from '../../../components/SubmitButton'

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  sectionlist: {},
  sectionFooterText: {
    ...(fonts.BodySmall as object),
    textAlign: 'center',
    margin: 2 * spacingUnit,
  },
  saveButtonContainer: { margin: marginDefault, alignItems: 'center' },
})

interface Section extends SectionBase<NotificationType> {
  title: string
}

export type NotificationTypeSettings = { [key in NotificationType]?: boolean }

export interface NavigationProps {
  onBack: () => void
  onSave: (settings: NotificationTypeSettings) => Promise<void>
  settings: NotificationTypeSettings
}

interface Props extends NavigationProps {}

interface State {
  settings: NotificationTypeSettings
}

export default class NotificationSettingsScreen extends Component<
  Props,
  State
> {
  constructor(props: Props) {
    super(props)
    this.state = { settings: props.settings }
  }

  getSectionListContent = (): Section[] => {
    return [
      {
        title: translate('notificationSettings.sections.general'),
        data: [
          NotificationType.MatchStart,
          NotificationType.MatchOpenForVoting,
          NotificationType.MatchEnd,
        ],
      },
      {
        title: translate('notificationSettings.sections.questions'),
        data: [NotificationType.QuestionAvailable],
      },
      {
        title: translate('notificationSettings.sections.matchEvents'),
        data: [
          NotificationType.MatchBreakResult,
          NotificationType.MatchSubstitution,
          NotificationType.MatchGoal,
        ],
      },
    ]
  }

  renderSectionHeader = (info: {
    section: SectionListData<NotificationType>
  }) => {
    return <SettingsSectionHeader title={info.section.title} />
  }

  handlePress = (id: NotificationType) =>
    this.setState(prev => ({
      settings: { ...prev.settings, [id]: !prev.settings[id] },
    }))

  handleChange = (id: NotificationType, value: boolean) =>
    this.setState(prev => ({
      settings: { ...prev.settings, [id]: value },
    }))

  renderItem = ({
    item,
    index,
  }: SectionListRenderItemInfo<NotificationType>) => {
    const { settings } = this.state
    return (
      <SettingsSwitchItem
        id={item}
        title={translate(`notificationSettings.types.${item}`)}
        onPress={this.handlePress}
        onChange={this.handleChange}
        value={settings[item]}
        presentationStyle={index % 2 === 0 ? 'even' : 'odd'}
      />
    )
  }

  handleSave = async () => {
    const { onSave, onBack } = this.props
    const { settings } = this.state
    await onSave(settings)
    onBack()
  }

  handleBack = async () => {
    const { settings: originalSettings, onBack, onSave } = this.props
    const { settings } = this.state
    if (JSON.stringify(originalSettings) !== JSON.stringify(settings)) {
      Alert.alert(
        translate('notificationSettings.saveWarningTitle'),
        translate('notificationSettings.saveWarningDescription'),
        [
          {
            text: translate('notificationSettings.yes'),
            style: 'default',
            onPress: async () => {
              await onSave(settings)
              onBack()
            },
          },
          {
            text: translate('notificationSettings.no'),
            style: 'destructive',
            onPress: () => {
              onBack()
            },
          },
          {
            text: translate('notificationSettings.cancel'),
            onPress: () => {},
            style: 'cancel',
          },
        ]
      )
    } else {
      onBack()
    }
  }

  render() {
    return (
      <ScreenWrapper>
        <GenericNavigationHeader
          title={translate('notificationSettings.title')}
          onBack={this.handleBack}
        />

        <SafeAreaView style={styles.container}>
          <SectionList
            // style={styles.sectionList}
            sections={this.getSectionListContent()}
            renderSectionHeader={this.renderSectionHeader}
            renderItem={this.renderItem}
            ListFooterComponent={() => (
              <View style={styles.saveButtonContainer}>
                <SubmitButton
                  title={translate('notificationSettings.save')}
                  onSubmit={this.handleSave}
                />
              </View>
            )}
            stickySectionHeadersEnabled={false}
            keyExtractor={item => item}
          />
        </SafeAreaView>
      </ScreenWrapper>
    )
  }
}

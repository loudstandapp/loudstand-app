import React from 'react'
import {
  ID,
  MatchListItem,
  UserActivityFeed,
  LocalizedTeams,
} from '@loudstand/shared'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import { connect } from 'react-redux'
import {
  ActivityFeed,
  compareByTimestamp,
  ActivityEntry,
} from '../components/ActivityFeed'
import { ReduxState } from '../../../store/model'
import * as fq from '../../../store/firestoreQueries'
import * as selectors from '../../../store/selectors'
import { currentLocale, translate } from '../../../translation'
import GenericNavigationHeader from '../../../components/GenericNavigationHeader'
import ScreenWrapper from '../../../components/ScreenWrapper'

export interface NavigationProps {
  userId: ID
  onSelectMatch: (match: MatchListItem) => void
  onBack: () => void
  onSelectPlayer: (id: ID) => void
  onSelectTeam: (id: ID) => void
  onSelectManager: (id: ID) => void
}

interface ReduxProps {
  activities: ActivityEntry[]
  localizedTeams?: LocalizedTeams
  isLoading: boolean
  isGenerating: boolean
}

interface Props extends NavigationProps, ReduxProps {}

export class UserActivityHistoryScreen extends React.PureComponent<Props> {
  render = () => {
    const {
      onSelectMatch,
      activities,
      localizedTeams,
      onBack,
      isLoading,
      isGenerating,
      onSelectManager,
      onSelectTeam,
      onSelectPlayer,
    } = this.props
    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <GenericNavigationHeader
          title={translate('userActivityHistory.title')}
          onBack={onBack}
        />
        <ActivityFeed
          onSelectManager={onSelectManager}
          onSelectPlayer={onSelectPlayer}
          onSelectTeam={onSelectTeam}
          onSelectMatch={onSelectMatch}
          users={{}}
          localizedTeams={localizedTeams}
          activities={activities}
        />
      </ScreenWrapper>
    )
  }
}
const currentLanguageCode = currentLocale.languageCode

const mapStateToProps = (state: ReduxState, props: Props): ReduxProps => {
  const activityFeed: UserActivityFeed | null =
    state.firestore.data[fq.userActivities.key(props.userId)]
  const activityList: ActivityEntry[] = Object.values(activityFeed || {}).map(
    feedItem => ({
      user: null,
      activity: feedItem,
    })
  )
  const activities: ActivityEntry[] = activityList.sort(compareByTimestamp)
  const localizedTeams =
    state.firestore.data[fq.localizedTeams.key(currentLanguageCode)]
  return {
    activities,
    localizedTeams,
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
  }
}

export default compose(
  firestoreConnect((props: Props) => [
    fq.userActivities.query(props.userId),
    fq.localizedTeams.query(currentLanguageCode),
  ]),
  connect(mapStateToProps)
)(UserActivityHistoryScreen)

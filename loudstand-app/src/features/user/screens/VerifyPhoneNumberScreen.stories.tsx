import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { VerifyPhoneNumberScreen } from './VerifyPhoneNumberScreen'

const onRequestVerificationWithReceiveCode = ({
  phoneNumber,
  onCodeSent,
  onCodeReceived,
}: {
  phoneNumber: string
  onCodeSent: () => void
  onCodeReceived: (code: string) => void
  onError: (code: string) => void
}) => {
  const myAction = action('onRequestVerification')
  myAction(phoneNumber)
  setTimeout(onCodeSent, 1500)
  setTimeout(() => onCodeReceived('13423'), 4000)
}

const onRequestVerificationDefault = ({
  phoneNumber,
  onCodeSent,
}: {
  phoneNumber: string
  onCodeSent: () => void
  onCodeReceived: (code: string) => void
  onError: (code: string) => void
}) => {
  const myAction = action('onRequestVerification')
  myAction(phoneNumber)
  setTimeout(onCodeSent, 1500)
}
const onRequestVerificationWithError = ({
  phoneNumber,
  onError,
}: {
  phoneNumber: string
  onCodeSent: () => void
  onCodeReceived: (code: string) => void
  onError: (code: string) => void
}) => {
  const myAction = action('onRequestVerification')
  myAction(phoneNumber)
  setTimeout(() => onError('error.code'), 1500)
}

const onVerify = (verificationCode: string): Promise<void> => {
  const myAction = action('onVerify')
  myAction(verificationCode)
  return new Promise(resolve => setTimeout(resolve, 1000))
}

storiesOf('VerifyPhoneNumberScreeen', module)
  .add('default', () => (
    <VerifyPhoneNumberScreen
      onRequestVerification={onRequestVerificationDefault}
      onVerify={onVerify}
      onNext={action('onNext')}
      onCancel={action('onCancel')}
    />
  ))
  .add('auto receive code', () => (
    <VerifyPhoneNumberScreen
      onRequestVerification={onRequestVerificationWithReceiveCode}
      onVerify={onVerify}
      onNext={action('onNext')}
      onCancel={action('onCancel')}
    />
  ))
  .add('error', () => (
    <VerifyPhoneNumberScreen
      onRequestVerification={onRequestVerificationWithError}
      onVerify={onVerify}
      onNext={action('onNext')}
      onCancel={action('onCancel')}
    />
  ))

import React from 'react'
import {
  View,
  Text,
  Animated,
  Easing,
  StyleSheet,
  SafeAreaView,
  Image,
} from 'react-native'
import { Options } from 'react-native-navigation'
import fonts from '../../../theme/fonts'
import { translate } from '../../../translation'
import { spacingUnit } from '../../../theme/layout'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  scarfImage: {},
  scarfImageContainer: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoImage: { position: 'absolute', bottom: 80 },
  awaitingText: {
    ...(fonts.SuperLarge as object),
    marginTop: 4 * spacingUnit,
  },
})

export default class AwaitAuthScreen extends React.PureComponent {
  static options = (): Options => ({
    topBar: {
      visible: false,
      drawBehind: true,
    },
  })

  value = new Animated.Value(0)

  componentDidMount = () => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(this.value, {
          toValue: 1,
          duration: 500,
          easing: Easing.linear,
        }),
        Animated.timing(this.value, {
          toValue: 0,
          duration: 500,
          easing: Easing.linear,
        }),
      ])
    ).start()
  }

  render = () => {
    const scale = this.value.interpolate({
      inputRange: [0, 1],
      outputRange: [1, 0.8],
    })
    return (
      <View style={styles.container}>
        <View style={styles.scarfImageContainer}>
          <Animated.Image
            style={[
              styles.scarfImage,
              {
                transform: [{ scale }],
              },
            ]}
            source={require('../../../../assets/images/loudstand-green-icon-scarf.png')}
          />
        </View>
        <Image
          source={require('../../../../assets/images/splash-logo.png')}
          style={styles.logoImage}
        />
        <SafeAreaView>
          <Text style={styles.awaitingText}>
            {translate('launch.connecting')}
          </Text>
        </SafeAreaView>
      </View>
    )
  }
}

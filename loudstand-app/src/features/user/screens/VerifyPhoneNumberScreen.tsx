import React from 'react'
import {
  StyleSheet,
  KeyboardAvoidingView,
  TextInput,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  Image,
  Keyboard,
  EmitterSubscription,
} from 'react-native'
import { ISO3166v1a2Country } from '@loudstand/shared'
// @ts-ignore missing types
import RNCarrierInfo from 'react-native-carrier-info'
import LinearGradient from 'react-native-linear-gradient'
import Color from 'color'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import PhoneNumber from 'awesome-phonenumber'
import PhoneNumberInput from '../components/PhoneNumberInput'
import { showPickerOverlay } from '../../../components/PickerOverlay'
import CountryPickerItem from '../../../components/CountryPickerItem'
import colors from '../../../theme/colors'
import { translate } from '../../../translation'
import fonts from '../../../theme/fonts'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import { flags } from '../../../components/Flag'

const VERIFICATION_CODE_LENGTH = 6

const COUNTRY_CODES = PhoneNumber.getSupportedRegionCodes().filter(
  countryCode => countryCode in flags
)

const styles = StyleSheet.create({
  backgroundSolid: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: colors.primary,
  },
  backgroundGradient: {
    ...StyleSheet.absoluteFillObject,
  },
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  globeImage: {
    marginVertical: 20,
  },
  header: {
    ...(fonts.TopBarTitle as object),
    color: colors.secondaryText,
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  message: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
    margin: marginDefault,
    textAlign: 'center',
    flex: 1,
    alignContent: 'flex-start',
  },
  inputFieldLabel: {
    ...(fonts.HeaderSmall as object),
    color: colors.secondaryText,
    margin: marginDefault,
    textAlign: 'center',
  },
  acceptButton: {
    backgroundColor: colors.background,
    paddingVertical: spacingUnit,
    borderRadius: spacingUnit / 2,
  },
  acceptButtonText: {
    ...(fonts.Header as object),
    color: colors.primaryText,
    textAlign: 'center',
  },
  dismissButtonText: {
    ...(fonts.BodyLarge as object),
    color: colors.secondaryText,
    paddingVertical: spacingUnit,
    textAlign: 'center',
  },
  dismissButtonTop: {
    alignItems: 'flex-end',
    padding: spacingUnit,
    width: '100%',
  },
  disabledBotton: {
    opacity: 0.4,
  },
  imageWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  textWrapper: {
    flex: 1,
    alignItems: 'center',
  },
  buttonWrapper: {
    margin: marginDefault,
  },
  topButtonWrapper: {
    alignItems: 'flex-end',
  },
  closeButton: {
    margin: marginDefault,
    tintColor: colors.secondaryText,
  },
  phoneNumberFieldContainer: {
    backgroundColor: colors.background,
    marginHorizontal: marginDefault,
    borderRadius: spacingUnit / 2,
  },
  confirmationCodeFieldContainer: {
    backgroundColor: colors.background,
    marginHorizontal: marginDefault,
    borderRadius: spacingUnit / 2,
    flexDirection: 'row',
    padding: spacingUnit,
  },
  confirmationCodeField: {
    ...(fonts.CustomPhoneNumberInput as object),
    flex: 1,
  },
  disabledNumberField: {
    opacity: 0.4,
  },
  keyboardAvoidingView: {
    flex: 1,
    justifyContent: 'center',
  },
})

export interface NavigationProps {
  onRequestVerification: (arg: {
    phoneNumber: string
    onCodeSent: () => void
    onCodeReceived: (code: string) => void
    onError: (code: string) => void
  }) => void
  onVerify: (verificationCode: string) => Promise<void>
  onNext: () => void
  onCancel: () => void
}

interface Props extends NavigationProps {}

enum Status {
  EnterPhoneNumber = 'enter_phone_number',
  WaitingForCode = 'waiting_for_code',
  EnterVerificationCode = 'enter_verification_code',
  WaitingForResult = 'waiting_for_result',
  Success = 'success',
}

interface State {
  phoneNumber?: string
  verificationCode?: string
  status: Status
  countryCode?: ISO3166v1a2Country
  isPhoneNumberValid: boolean
  isVerificationCodeValid: boolean
  isKeyboardVisible: boolean
}
export class VerifyPhoneNumberScreen extends React.PureComponent<Props, State> {
  state: State = {
    phoneNumber: undefined,
    verificationCode: undefined,
    status: Status.EnterPhoneNumber,
    countryCode: undefined, // FIXME
    isPhoneNumberValid: false,
    isVerificationCodeValid: false,
    isKeyboardVisible: false,
  }

  verificationCodeRef = React.createRef<TextInput>()

  phoneNumberInputRef = React.createRef<PhoneNumberInput>()

  keyboardDidShowListener?: EmitterSubscription

  keyboardDidHideListener?: EmitterSubscription

  componentDidMount = async () => {
    ReactNativeHapticFeedback.trigger('notificationWarning', {
      enableVibrateFallback: true,
      ignoreAndroidSystemSettings: true,
    })

    try {
      const carrierCountryCode =
        (await RNCarrierInfo.isoCountryCode()) || COUNTRY_CODES[0]
      this.setState({
        countryCode: carrierCountryCode.toUpperCase(),
      })
    } catch (e) {
      this.setState({ countryCode: COUNTRY_CODES[0] })
    }
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this.handleKeyboardShow
    )
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this.handleKeyboardHide
    )
  }

  componentWillUnmount() {
    this.keyboardDidShowListener && this.keyboardDidShowListener.remove()
    this.keyboardDidHideListener && this.keyboardDidHideListener.remove()
  }

  handleSubmit = async () => {
    const { isPhoneNumberValid } = this.state
    if (!isPhoneNumberValid) {
      return false
    }
    await this.handlePhoneAuth()
    return true
  }

  handleKeyboardShow = () => this.setState({ isKeyboardVisible: true })

  handleKeyboardHide = () => this.setState({ isKeyboardVisible: false })

  handlePhoneAuth = async () => {
    const { phoneNumber } = this.state
    if (!phoneNumber) {
      return
    }
    const { onRequestVerification } = this.props
    this.setState({ status: Status.WaitingForCode })
    onRequestVerification({
      phoneNumber,
      onCodeSent: this.handleCodeSent,
      onCodeReceived: this.handleCodeReceived,
      onError: this.handleError,
    })
  }

  handleCodeSent = () => {
    this.setState({ status: Status.EnterVerificationCode }, () => {
      this.verificationCodeRef.current &&
        this.verificationCodeRef.current.focus()
    })
  }

  handleCodeReceived = (verificationCode: string) => {
    this.setState({ verificationCode }, this.handleVerify)
  }

  handlePhoneNumberChange = (phoneNumber: string, isValid: boolean) => {
    this.setState({ phoneNumber, isPhoneNumberValid: isValid })
  }

  handleError = () => {
    this.phoneNumberInputRef.current && this.phoneNumberInputRef.current.clear()
    this.setState({
      phoneNumber: undefined,
      verificationCode: undefined,
      status: Status.EnterPhoneNumber,
      isPhoneNumberValid: false,
      isVerificationCodeValid: false,
    })
  }

  handleVerificationCodeChange = (verificationCode: string) => {
    const isVerificationCodeValid =
      verificationCode.length === VERIFICATION_CODE_LENGTH
    this.setState(
      { verificationCode, isVerificationCodeValid },
      () => isVerificationCodeValid && this.handleVerify()
    )
  }

  handleVerify = async () => {
    const { verificationCode } = this.state
    if (!verificationCode) {
      return
    }
    const { onVerify } = this.props
    this.setState({ status: Status.WaitingForResult })
    try {
      await onVerify(verificationCode)
      this.setState({ status: Status.Success })
      ReactNativeHapticFeedback.trigger('notificationSuccess', {
        enableVibrateFallback: true,
        ignoreAndroidSystemSettings: true,
      })
    } catch (error) {
      this.handleError()
    }
  }

  showCountrySelector = (
    inputCallback: (countryCode: ISO3166v1a2Country) => void
  ) => {
    const callback = (countryCode: ISO3166v1a2Country) => {
      inputCallback(countryCode)
      this.phoneNumberInputRef.current &&
        this.phoneNumberInputRef.current.focus()
      return true
    }
    this.phoneNumberInputRef.current && this.phoneNumberInputRef.current.blur()
    showPickerOverlay({
      data: COUNTRY_CODES,
      renderItem: ({ item }) => <CountryPickerItem countryCode={item} />,
      onSelect: callback,
      backgroundColor: colors.navigationBackground,
    })
  }

  render = () => {
    const {
      status,
      verificationCode,
      countryCode,
      isPhoneNumberValid,
      isKeyboardVisible,
    } = this.state
    const { onNext, onCancel } = this.props
    const isNextButtonVisible = status === Status.Success
    const isPhoneNumberInputVisible = status !== Status.Success
    const isPhoneNumberInputEnabled = status === Status.EnterPhoneNumber
    const isVerificationCodeInputVisible = [
      Status.WaitingForResult,
      Status.EnterVerificationCode,
    ].includes(status)
    const isVerificationCodeInputEnabled =
      Status.EnterVerificationCode === status

    return (
      <View style={styles.backgroundSolid}>
        <LinearGradient
          colors={[
            Color(colors.secondary)
              .alpha(0.5)
              .toString(),
            colors.secondary,
          ]}
          style={styles.backgroundGradient}
        >
          <SafeAreaView style={styles.container}>
            <View style={styles.topButtonWrapper}>
              <TouchableOpacity
                onPress={onCancel}
                style={styles.dismissButtonTop}
              >
                <Image
                  source={require('../../../../assets/icons/buttons/close.png')}
                  style={styles.closeButton}
                />
              </TouchableOpacity>
            </View>
            {!isKeyboardVisible && (
              <View style={styles.imageWrapper}>
                <Image
                  source={require('../../../../assets/icons/indicators/phone-confirmation.png')}
                  style={styles.globeImage}
                />
              </View>
            )}
            <View style={styles.textWrapper}>
              <Text style={styles.header}>
                {translate('phoneNumberVerification.title')}
              </Text>
              {!isKeyboardVisible && (
                <>
                  {!isNextButtonVisible && (
                    <Text style={styles.message}>
                      {translate('phoneNumberVerification.message')}
                    </Text>
                  )}
                  {isNextButtonVisible && (
                    <Text style={styles.message}>
                      {translate('phoneNumberVerification.success')}
                    </Text>
                  )}
                </>
              )}
            </View>

            <KeyboardAvoidingView
              behavior="padding"
              style={styles.keyboardAvoidingView}
            >
              <>
                {isPhoneNumberInputVisible && countryCode && (
                  <>
                    <Text style={styles.inputFieldLabel}>
                      {translate('phoneNumberVerification.phoneNumber')}
                    </Text>
                    <PhoneNumberInput
                      style={[
                        styles.phoneNumberFieldContainer,
                        !isPhoneNumberInputEnabled &&
                          styles.disabledNumberField,
                      ]}
                      initialCountryCode={countryCode}
                      numberValue=""
                      onSubmit={this.handleSubmit}
                      onSelectCountry={this.showCountrySelector}
                      onChange={this.handlePhoneNumberChange}
                      disabled={!isPhoneNumberInputEnabled}
                      // textInputProps={{ autoFocus: true }}
                      ref={this.phoneNumberInputRef}
                    />
                  </>
                )}
                {isVerificationCodeInputVisible && (
                  <>
                    <Text style={styles.inputFieldLabel}>
                      {translate('phoneNumberVerification.verificationCode')}
                    </Text>
                    <View style={styles.confirmationCodeFieldContainer}>
                      <TextInput
                        maxLength={VERIFICATION_CODE_LENGTH}
                        style={[
                          styles.confirmationCodeField,
                          !isVerificationCodeInputEnabled &&
                            styles.disabledNumberField,
                        ]}
                        keyboardType="numeric"
                        textContentType="oneTimeCode"
                        onChangeText={this.handleVerificationCodeChange}
                        value={verificationCode}
                        editable={isVerificationCodeInputEnabled}
                        ref={this.verificationCodeRef}
                      />
                    </View>
                  </>
                )}
              </>
              <View style={styles.buttonWrapper}>
                {isPhoneNumberInputEnabled && (
                  <TouchableOpacity
                    onPress={this.handlePhoneAuth}
                    style={[
                      styles.acceptButton,
                      !isPhoneNumberValid && styles.disabledBotton,
                    ]}
                    disabled={!isPhoneNumberValid}
                  >
                    <Text style={styles.acceptButtonText}>
                      {translate('phoneNumberVerification.getVerificationCode')}
                    </Text>
                  </TouchableOpacity>
                )}
                {isNextButtonVisible && (
                  <TouchableOpacity
                    onPress={onNext}
                    style={styles.acceptButton}
                  >
                    <Text style={styles.acceptButtonText}>
                      {translate('phoneNumberVerification.continue')}
                    </Text>
                  </TouchableOpacity>
                )}
                {!isNextButtonVisible && (
                  <TouchableOpacity onPress={onCancel}>
                    <Text style={styles.dismissButtonText}>
                      {translate('phoneNumberVerification.dismiss')}
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            </KeyboardAvoidingView>
          </SafeAreaView>
        </LinearGradient>
      </View>
    )
  }
}

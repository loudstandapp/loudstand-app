import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Image,
  ImageBackground,
  View,
  Alert,
} from 'react-native'
import { connect } from 'react-redux'
import { Dispatch } from 'redux'
import { Options } from 'react-native-navigation'
import ScreenWrapper from '../../../components/ScreenWrapper'
import * as selectors from '../../../store/selectors'
import { loginWithFacebookThunk } from '../userFlow'
import colors from '../../../theme/colors'
import fonts from '../../../theme/fonts'
import { spacingUnit } from '../../../theme/layout'
import { version } from '../../../../package.json'
import { deviceInfo } from '../../../utils'
import { ReduxState } from '../../../store/model'
import { translate } from '../../../translation'

const blueScarfButton = Image.resolveAssetSource(
  require('../../../../assets/icons/buttons/blue-scarf-signup.png')
)

const styles = StyleSheet.create({
  safeAreaView: { flex: 1 },
  contentWrapper: { flex: 1, justifyContent: 'center', alignItems: 'center' },
  scarfButton: {
    width: blueScarfButton.width,
    height: blueScarfButton.height,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 40,
  },
  scarfButtonImage: {
    overflow: 'visible',
  },
  facebookSignInText: {
    ...(fonts.Body as object),
    color: colors.secondaryText,
    marginLeft: 2 * spacingUnit,
  },
  pleaseWaitText: {
    ...(fonts.BodyStrong as object),
  },
  roundScarf: {
    margin: 8 * spacingUnit,
  },
  versionText: {
    ...(fonts.BodySmall as object),
    opacity: 0.3,
  },
})

export interface NavigationProps {
  resolver: () => void
}
interface ReduxProps {
  isLoading: boolean
  isGenerating: boolean
  isFacebookLoggingIn: boolean
}

interface ReduxActionProps {
  onFacebookLogin: () => void
}
interface Props extends NavigationProps, ReduxActionProps, ReduxProps {}

export class LoginScreen extends Component<Props> {
  static options = (): Options => ({
    statusBar: {
      style: 'dark',
    },
  })

  showDebug = () => {
    const message = Object.keys(deviceInfo)
      .map(
        // @ts-ignore
        key => `${key}: ${deviceInfo[key]}`
      )
      .join('\n')
    Alert.alert('Debug', message)
  }

  render() {
    const {
      onFacebookLogin,
      isLoading,
      isGenerating,
      isFacebookLoggingIn,
    } = this.props
    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <SafeAreaView style={styles.safeAreaView}>
          <View style={styles.contentWrapper}>
            <Image
              style={styles.roundScarf}
              source={require('../../../../assets/images/loudstand-green-icon-scarf.png')}
            />
            {isFacebookLoggingIn ? (
              <Text style={styles.pleaseWaitText}>
                {translate('launch.loggingIn')}
              </Text>
            ) : (
              <TouchableOpacity
                onPress={onFacebookLogin}
                disabled={isFacebookLoggingIn}
              >
                <ImageBackground
                  style={styles.scarfButton}
                  source={blueScarfButton}
                  imageStyle={styles.scarfButtonImage}
                >
                  <Image
                    source={require('../../../../assets/icons/misc/facebook.png')}
                  />
                  <Text style={styles.facebookSignInText}>
                    {translate('launch.facebookLogin')}
                  </Text>
                </ImageBackground>
              </TouchableOpacity>
            )}
          </View>
          <TouchableOpacity onPress={this.showDebug}>
            <Text style={styles.versionText}>{version}</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </ScreenWrapper>
    )
  }
}

const mapStateToProps = (state: ReduxState): ReduxProps => {
  return {
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
    isFacebookLoggingIn: state.general.isFacebookLoggingIn,
  }
}
const mapDispatchToProps = (
  dispatch: Dispatch,
  props: Props
): ReduxActionProps => ({
  // @ts-ignore thunk type
  onFacebookLogin: () => dispatch(loginWithFacebookThunk(props.resolver)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen)

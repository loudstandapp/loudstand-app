import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { SettingsScreen } from './SettingsScreen'

storiesOf('SettingsScreen', module)
  .add('no phone number', () => (
    <SettingsScreen
      onLogout={action('onLogout')}
      onChangeFollowingTeams={action('onChangeFollowingTeams')}
      onUnlinkPhoneNumber={action('onUnlinkPhoneNumber')}
    />
  ))
  .add('phone number', () => (
    <SettingsScreen
      onLogout={action('onLogout')}
      onChangeFollowingTeams={action('onChangeFollowingTeams')}
      onUnlinkPhoneNumber={action('onUnlinkPhoneNumber')}
      userPhoneNumber="+452242244"
    />
  ))

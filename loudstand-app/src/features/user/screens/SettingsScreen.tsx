import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  Alert,
  SectionListData,
  SectionBase,
  SectionList,
  SectionListRenderItemInfo,
  SafeAreaView,
  Platform,
} from 'react-native'
import { Dispatch, compose, Store } from 'redux'
import { connect } from 'react-redux'
import { TeamList, UserNotificationSettings } from '@loudstand/shared'
import { firestoreConnect } from 'react-redux-firebase'
import ScreenWrapper from '../../../components/ScreenWrapper'
import * as selectors from '../../../store/selectors'
import * as fq from '../../../store/firestoreQueries'
import { ReduxState } from '../../../store/model'
import {
  logoutThunk,
  unlinkPhoneNumberThunk,
  saveFollowingTeamsThunk,
  saveNotificationSettingsThunk,
} from '../userFlow'
import SettingsSectionHeader from '../components/SettingsSectionHeader'
import SettingsLinkItem from '../components/SettingsLinkItem'
import SettingsTextItem from '../components/SettingsTextItem'
import fonts from '../../../theme/fonts'
import { spacingUnit } from '../../../theme/layout'
import { translate } from '../../../translation'
import { config, deviceInfo, openUrl } from '../../../utils'
import { version } from '../../../../package.json'
import GenericNavigationHeader from '../../../components/GenericNavigationHeader'
import { NotificationTypeSettings } from './NotificationSettingsScreen'

const FACEBOOK_URL = 'https://www.facebook.com/loudstand'
const INSTAGRAM_URL = 'https://www.instagram.com/loudstandapp/'
const TERMS_AND_CONDITIONS_URL = 'https://www.loudstand.com/privacy-policy/'
const COPYRIGHT_URL = 'https://www.loudstand.com/copyright-information/ '
const WEBSITE_URL = 'https://www.loudstand.com/'
const FEEDBACK_URL = 'mailto:feedback@loudstand.com?subject=User%20feedback'
const RATING_URL_IOS = `https://itunes.apple.com/us/app/${
  config.appStoreAppId
}?action=write-review`
const RATING_URL_ANDROID = `market://details?id=${config.androidPackageName}`

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  sectionlist: {},
  sectionFooterText: {
    ...(fonts.BodySmall as object),
    textAlign: 'center',
    margin: 2 * spacingUnit,
  },
})

enum RowType {
  TextRow,
  LinkRow,
}

interface TextRow {
  type: RowType.TextRow
  title: string
  content?: string
}

interface LinkRow {
  type: RowType.LinkRow
  title: string
  onPress: () => void
}

interface Section extends SectionBase<LinkRow | TextRow> {
  title: string
}

function confirmDialog(
  message: string | undefined,
  onAccept: () => void,
  onReject?: () => void
) {
  Alert.alert(translate('settings.areYouSure'), message, [
    { text: translate('settings.cancel'), style: 'cancel', onPress: onReject },
    { text: translate('settings.ok'), style: 'destructive', onPress: onAccept },
  ])
}

function ListFooter() {
  return (
    <Text style={styles.sectionFooterText}>
      {translate('settings.footerText', { year: new Date().getFullYear() })}
    </Text>
  )
}

export interface NavigationProps {
  onChangeFollowingTeams: (onNext: (selectedTeams: TeamList) => void) => void
  onBack: () => void
  onSelectNotificationSettings: (
    settings: NotificationTypeSettings,
    onSave: (settings: NotificationTypeSettings) => Promise<void>
  ) => void
}

interface ReduxProps {
  userPhoneNumber?: string
  isLoading: boolean
  isGenerating: boolean
  notificationSettings?: UserNotificationSettings
}

interface ReduxActionProps {
  onLogout: () => void
  onUnlinkPhoneNumber: () => void
  saveFollowingTeams: (teams: TeamList) => void
  onSaveNotificationSettings: (
    settings: NotificationTypeSettings
  ) => Promise<void>
}

interface Props extends NavigationProps, ReduxProps, ReduxActionProps {}
export class SettingsScreen extends Component<Props> {
  getSectionListContent = (): Section[] => {
    const {
      onLogout,
      userPhoneNumber,
      onUnlinkPhoneNumber,
      onChangeFollowingTeams,
      saveFollowingTeams,
      onSelectNotificationSettings,
      notificationSettings,
      onSaveNotificationSettings,
    } = this.props

    const notificationSettingsRow: LinkRow = {
      type: RowType.LinkRow,
      onPress: () =>
        notificationSettings &&
        onSelectNotificationSettings(
          notificationSettings.types,
          onSaveNotificationSettings
        ),
      title: translate('notificationSettings.title'),
    }
    const followingTeams: LinkRow = {
      type: RowType.LinkRow,
      onPress: () =>
        onChangeFollowingTeams((selectedTeams: TeamList) =>
          saveFollowingTeams(selectedTeams)
        ),
      title: translate('settings.followingTeams'),
    }

    const favoriteTeams: TextRow = {
      type: RowType.TextRow,
      title: translate('settings.favoriteTeams'),
      content: translate('settings.favoriteTeamsContent'),
    }

    const phoneNumber: TextRow = {
      type: RowType.TextRow,
      title: translate('settings.phoneNumber'),
      content: userPhoneNumber,
    }

    const unlinkPhoneNumber: LinkRow = {
      type: RowType.LinkRow,
      onPress: () =>
        confirmDialog(
          translate('settings.unlinkPhoneNumberWarning'),
          onUnlinkPhoneNumber
        ),
      title: translate('settings.unlinkPhoneNumber'),
    }
    const logout: LinkRow = {
      type: RowType.LinkRow,
      onPress: () => confirmDialog(undefined, onLogout),
      title: translate('settings.logOut'),
    }

    const userSectionItems = userPhoneNumber
      ? [
          notificationSettingsRow,
          followingTeams,
          favoriteTeams,
          phoneNumber,
          unlinkPhoneNumber,
          logout,
        ]
      : [notificationSettingsRow, followingTeams, favoriteTeams, logout]

    const deviceInfoRows: TextRow[] = Object.keys(deviceInfo).map(key => ({
      type: RowType.TextRow,
      title: key,
      // @ts-ignore
      content: deviceInfo[key],
    }))

    return [
      {
        title: translate('settings.user'),
        data: userSectionItems,
      },
      {
        title: translate('settings.loudstand'),
        data: [
          {
            type: RowType.LinkRow,
            onPress: () => openUrl(FEEDBACK_URL),
            title: translate('settings.feedback'),
          },
          {
            type: RowType.LinkRow,
            onPress: () =>
              openUrl(
                Platform.select({
                  ios: RATING_URL_IOS,
                  android: RATING_URL_ANDROID,
                })
              ),
            title: translate('settings.rate'),
          },
          {
            type: RowType.LinkRow,
            onPress: () => openUrl(FACEBOOK_URL),
            title: translate('settings.followOnFacebook'),
          },
          {
            type: RowType.LinkRow,
            onPress: () => openUrl(INSTAGRAM_URL),
            title: translate('settings.followOnInstagram'),
          },
          {
            type: RowType.LinkRow,
            onPress: () => openUrl(WEBSITE_URL),
            title: translate('settings.website'),
          },
        ],
      },
      {
        title: 'Legal',
        data: [
          {
            type: RowType.LinkRow,
            onPress: () => openUrl(COPYRIGHT_URL),
            title: translate('settings.copyright'),
          },
          {
            type: RowType.LinkRow,
            onPress: () => openUrl(TERMS_AND_CONDITIONS_URL),
            title: translate('settings.termsAndConditions'),
          },
        ],
      },
      {
        title: 'Debug',
        data: [
          {
            type: RowType.TextRow,
            title: 'JS bundle version',
            content: version,
          },
          ...deviceInfoRows,
        ],
      },
    ]
  }

  renderSectionHeader = (info: {
    section: SectionListData<LinkRow | TextRow>
  }) => {
    return <SettingsSectionHeader title={info.section.title} />
  }

  renderItem = ({
    item,
    index,
  }: SectionListRenderItemInfo<LinkRow | TextRow>) => {
    switch (item.type) {
      case RowType.LinkRow: {
        return (
          <SettingsLinkItem
            title={item.title}
            onPress={item.onPress}
            presentationStyle={index % 2 === 0 ? 'even' : 'odd'}
          />
        )
      }
      case RowType.TextRow: {
        return (
          <SettingsTextItem
            title={item.title}
            content={item.content}
            presentationStyle={index % 2 === 0 ? 'even' : 'odd'}
          />
        )
      }
      default:
        return null
    }
  }

  render() {
    const { onBack, isLoading, isGenerating } = this.props
    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <GenericNavigationHeader
          title={translate('settings.settings')}
          onBack={onBack}
        />

        <SafeAreaView style={styles.container}>
          <SectionList
            sections={this.getSectionListContent()}
            renderSectionHeader={this.renderSectionHeader}
            renderItem={this.renderItem}
            ListFooterComponent={ListFooter}
            stickySectionHeadersEnabled={false}
            keyExtractor={item => item.title}
          />
        </SafeAreaView>
      </ScreenWrapper>
    )
  }
}

const mapStateToProps = (state: ReduxState): ReduxProps => {
  const { uid } = state.firebase.auth
  return {
    userPhoneNumber: state.firebase.auth.phoneNumber,
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
    notificationSettings:
      state.firestore.data[fq.userNotificationSettings.key(uid)],
  }
}

const mapDispatchToProps = (dispatch: Dispatch): ReduxActionProps => ({
  // @ts-ignore support thunk type
  onLogout: () => dispatch(logoutThunk()),
  // @ts-ignore support thunk type
  onUnlinkPhoneNumber: () => dispatch(unlinkPhoneNumberThunk()),
  onSaveNotificationSettings: (settings: NotificationTypeSettings) =>
    // @ts-ignore support thunk type
    dispatch(saveNotificationSettingsThunk(settings)),

  saveFollowingTeams: (teams: TeamList) =>
    // @ts-ignore support thunk type
    dispatch(saveFollowingTeamsThunk(teams)),
})

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const { uid } = state.firebase.auth
    return [fq.userNotificationSettings.query(uid)]
  })
  // @ts-ignore https://github.com/prescottprue/react-redux-firebase/issues/682
)(SettingsScreen)

import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import RequestNotificationPermissionScreen from './RequestNotificationPermissionScreen'

storiesOf('RequestNotificationPermissionScreen', module).add('Default', () => (
  <RequestNotificationPermissionScreen
    onAccept={action('onAccept')}
    onSkip={action('onSkip')}
  />
))

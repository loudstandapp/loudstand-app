import React from 'react'

// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react-native'
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions'

import { NotificationType } from '@loudstand/shared'
import NotificationSettingsScreen from './NotificationSettingsScreen'

storiesOf('NotificationSettingsScreen', module).add('no phone number', () => (
  <NotificationSettingsScreen
    onBack={action('onBack')}
    onSave={async () => action('onSave')()}
    settings={{
      [NotificationType.MatchEnd]: true,
      [NotificationType.MatchStart]: true,
      [NotificationType.MatchOpenForVoting]: true,
      [NotificationType.QuestionAvailable]: false,
      [NotificationType.MatchBreakResult]: false,
      [NotificationType.MatchGoal]: true,
      [NotificationType.MatchSubstitution]: false,
    }}
  />
))

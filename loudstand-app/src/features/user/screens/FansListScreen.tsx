import React from 'react'
import { StyleSheet, View } from 'react-native'
import { compose, Store, Dispatch, bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import {
  UserFriendList,
  ID,
  LocalizedTeams,
  MatchListItem,
} from '@loudstand/shared'
import ScreenWrapper from '../../../components/ScreenWrapper'
import * as selectors from '../../../store/selectors'
import * as fq from '../../../store/firestoreQueries'
import { ReduxState, DismissedGuides } from '../../../store/model'
import { updateFriendListThunk } from '../../../services/friends'
import FanList from '../components/FanList'
import ActivityFeed from '../components/ActivityFeed'
import TabSelector from '../../../components/TabSelector'
import { translate, currentLocale } from '../../../translation'
import { spacingUnit } from '../../../theme/layout'
import GenericButton from '../../../components/GenericButton'
import GenericNavigationHeader from '../../../components/GenericNavigationHeader'
import Guide, { GuideType } from '../../../components/Guide'
import { dismissGuide } from '../../../store/actions'

const currentLanguageCode = currentLocale.languageCode

const styles = StyleSheet.create({
  contentWrapper: { flex: 1 },
  contentTop: {
    marginHorizontal: 2 * spacingUnit,
    marginVertical: spacingUnit,
  },
  regenerateButtonContainer: {
    margin: 2 * spacingUnit,
    alignItems: 'center',
  },
})

export interface ReduxProps {
  friendList?: UserFriendList
  isLoadingFriendsList: boolean
  localizedTeams?: LocalizedTeams
  isLoading: boolean
  isGenerating: boolean
  dismissedGuides: DismissedGuides
  launchCount: number
}

export interface ReduxActionProps {
  onLoadFriendList: () => void
  onRegenerateFriendList: () => void
  onDismissGuide: (type: GuideType) => void
}

export interface NavigationProps {
  onSelectUser: (userId: ID) => void
  onSelectMatch: (match: MatchListItem) => void
  onSelectPlayer: (id: ID) => void
  onSelectTeam: (id: ID) => void
  onSelectManager: (id: ID) => void
}
interface Props extends NavigationProps, ReduxProps, ReduxActionProps {}

enum Tab {
  FriendList,
  FriendsActivity,
}

interface State {
  activeTab: Tab
}

export class FansListScreen extends React.PureComponent<Props, State> {
  state = { activeTab: Tab.FriendList }

  componentDidUpdate = (prevProps: Props) => {
    const { isLoadingFriendsList } = this.props
    if (!isLoadingFriendsList && prevProps.isLoadingFriendsList) {
      const { onLoadFriendList } = this.props
      onLoadFriendList()
    }
  }

  handleTabSelection = (index: number) => {
    this.setState({ activeTab: index })
  }

  renderRegenerateButton = () => {
    const { onRegenerateFriendList, isLoadingFriendsList } = this.props
    return (
      <View style={styles.regenerateButtonContainer}>
        <GenericButton
          title={translate('community.reloadFriends')}
          onPress={onRegenerateFriendList}
          disabled={isLoadingFriendsList}
        />
      </View>
    )
  }

  render() {
    const {
      onSelectUser,
      friendList,
      localizedTeams,
      onSelectMatch,
      isLoading,
      isGenerating,
      onSelectManager,
      onSelectPlayer,
      onSelectTeam,
      dismissedGuides,
      onDismissGuide,
      launchCount,
    } = this.props
    const { activeTab } = this.state
    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <GenericNavigationHeader title={translate('community.title')} />
        <View style={styles.contentTop}>
          <TabSelector
            options={[
              translate('community.friendList'),
              translate('community.friendsActivity'),
            ]}
            onSelect={this.handleTabSelection}
            selectedIndex={Tab.FriendList}
          />
        </View>
        <View style={styles.contentWrapper}>
          {activeTab === Tab.FriendList && (
            <>
              {friendList && friendList.friends ? (
                <FanList
                  fans={friendList.friends}
                  onSelectFan={onSelectUser}
                  footer={this.renderRegenerateButton}
                />
              ) : (
                this.renderRegenerateButton()
              )}
            </>
          )}
          {friendList && activeTab === Tab.FriendsActivity && (
            <ActivityFeed
              onSelectManager={onSelectManager}
              onSelectPlayer={onSelectPlayer}
              onSelectTeam={onSelectTeam}
              users={friendList.friends}
              onSelectUser={onSelectUser}
              onSelectMatch={onSelectMatch}
              localizedTeams={localizedTeams}
            />
          )}
        </View>
        <Guide
          type={GuideType.Community}
          dismissedGuides={dismissedGuides}
          onDismiss={onDismissGuide}
          launchCount={launchCount}
        />
      </ScreenWrapper>
    )
  }
}

const mapStateToProps = (state: ReduxState): ReduxProps => {
  const { uid } = state.firebase.auth
  const {
    [fq.userFriendList.key(uid)]: friendList,
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
  } = state.firestore.data
  const isLoadingFriendsList =
    state.firestore.status.requesting[fq.userFriendList.key(uid)]
  const { dismissedGuides, launchCount } = state.general
  return {
    friendList,
    isLoadingFriendsList,
    localizedTeams: localizedTeams && localizedTeams.body,
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
    dismissedGuides,
    launchCount,
  }
}

const mapDispatchToProps = (dispatch: Dispatch): ReduxActionProps =>
  bindActionCreators(
    {
      onDismissGuide: dismissGuide,
      onLoadFriendList: () => updateFriendListThunk(false),
      onRegenerateFriendList: () => updateFriendListThunk(true),
    },
    dispatch
  )

export default compose(
  firestoreConnect((props: Props, store: Store) => {
    const state = store.getState()
    const { uid } = state.firebase.auth
    return [
      fq.userFriendList.query(uid),
      fq.localizedTeams.query(currentLanguageCode),
    ]
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(FansListScreen)

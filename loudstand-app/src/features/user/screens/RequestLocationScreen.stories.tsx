import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import RequestLocationScreen from './RequestLocationScreen'

storiesOf('RequestLocationScreen', module).add('Default', () => (
  <RequestLocationScreen
    onAccept={action('onAccept')}
    onDismiss={action('onDismiss')}
  />
))

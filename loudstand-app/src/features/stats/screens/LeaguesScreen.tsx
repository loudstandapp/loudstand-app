import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  ListRenderItemInfo,
  TouchableOpacity,
  LayoutAnimation,
  Image,
  Platform,
} from 'react-native'
import {
  TournamentRoot,
  SeasonPlayerStandingType,
  SeasonPlayerStandingsByType,
  CalendarItemByDay,
  MatchListItem,
  SeasonStandings,
  LocalizedTeams,
  ID,
  SeasonTeamPopularityDocument,
  MatchResultList,
} from '@loudstand/shared'
import { compose, Store, bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import ScreenWrapper from '../../../components/ScreenWrapper'
import * as selectors from '../../../store/selectors'
import SeasonMatches from '../components/SeasonMatches'
import Standings from '../components/Standings'
import PlayerStandings from '../components/PlayerStandings'
import { ReduxState } from '../../../store/model'
import SwiperContainer, {
  largeInset,
} from '../../../components/SwiperContainer'
import { showPickerOverlay } from '../../../components/PickerOverlay'
import fonts from '../../../theme/fonts'
import { marginDefault, spacingUnit, buttonHeight } from '../../../theme/layout'
import { translate, currentLocale } from '../../../translation'
import * as actions from '../../../store/actions'
import TournamentSelectorButton from '../../../components/TournamentSelectorButton'
import TournamentPickerItem, {
  tournamentListToArray,
} from '../../../components/TournamentPickerItem'
import * as fq from '../../../store/firestoreQueries'
import GenericNavigationHeader from '../../../components/GenericNavigationHeader'
import colors from '../../../theme/colors'

const currentLanguageCode = currentLocale.languageCode

const styles = StyleSheet.create({
  pickerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  pickerText: {
    ...(fonts.Header as object),
    color: colors.secondaryText,
  },
  playerStandingsContainer: {
    flex: 1,
  },
  selectButton: {
    height: buttonHeight,
    minWidth: buttonHeight * 2,
    borderRadius: buttonHeight / 2,
    justifyContent: 'space-between',
    backgroundColor: colors.tertiaryLight,
    marginHorizontal: marginDefault,
    marginTop: spacingUnit,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: marginDefault,
  },
  selectButtonText: {
    flex: 1,
    ...(fonts.HeaderSmall as object),
    color: colors.primaryText,
  },
})

export interface NavigationProps {
  onSelectTeam: (id: ID) => void
  onSelectMatch: (match: MatchListItem) => void
  onSelectPlayer: (id: ID) => void
  onBack: () => void
  seasonId?: ID
  today: Date
}

export interface ReduxProps {
  activeTournament?: TournamentRoot
  playerStandings?: SeasonPlayerStandingsByType
  seasonMatches?: CalendarItemByDay<MatchListItem>
  seasonResults?: MatchResultList
  seasonStandings?: SeasonStandings
  localizedTeams?: LocalizedTeams
  tournaments?: { [key in ID]: TournamentRoot }
  teamPopularity?: SeasonTeamPopularityDocument
  isLoading: boolean
  isGenerating: boolean
}

interface ReduxActionProps {
  setTournament: (tournament: TournamentRoot) => void
}

interface Props extends NavigationProps, ReduxProps, ReduxActionProps {}

interface State {
  playerStandingType: SeasonPlayerStandingType
}

export class LeaguesScreen extends Component<Props, State> {
  state = {
    playerStandingType: SeasonPlayerStandingType.TopGoals,
  }

  hasSwitchedSeason = false

  componentDidMount = () => {
    this.switchSeasonIfNeeded()
  }

  componentDidUpdate = () => {
    this.switchSeasonIfNeeded()
  }

  switchSeasonIfNeeded = () => {
    if (this.hasSwitchedSeason) {
      return
    }
    const {
      seasonId,
      activeTournament,
      tournaments,
      setTournament,
    } = this.props
    if (!seasonId || !tournaments) {
      return
    }
    if (activeTournament && activeTournament.currentSeason.id === seasonId) {
      return
    }
    const newTournament = Object.values(tournaments).find(
      tournament => tournament.currentSeason.id === seasonId
    )
    if (newTournament) {
      this.hasSwitchedSeason = true
      setTournament(newTournament)
    }
  }

  handleSelectTournament = (tournament: TournamentRoot) => {
    const { setTournament } = this.props
    setTournament(tournament)
    if (Platform.OS === 'ios') {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
    }
    return true
  }

  showTournamentSelector = () => {
    const { tournaments } = this.props
    showPickerOverlay({
      data: tournamentListToArray(tournaments),
      renderItem: ({ item }: ListRenderItemInfo<TournamentRoot>) => (
        <TournamentPickerItem item={item} />
      ),
      onSelect: this.handleSelectTournament,
      backgroundColor: TournamentPickerItem.backgroundColor,
    })
  }

  handleSelectPlayerStandingType = (
    playerStandingType: SeasonPlayerStandingType
  ) => {
    if (Platform.OS === 'ios') {
      LayoutAnimation.configureNext(LayoutAnimation.Presets.spring)
    }
    this.setState({
      playerStandingType,
    })
    return true
  }

  showPlayerStandingTypeSelector = () => {
    showPickerOverlay({
      data: [
        SeasonPlayerStandingType.TopGoals,
        SeasonPlayerStandingType.TopAssists,
        SeasonPlayerStandingType.TopPoints,
        SeasonPlayerStandingType.TopYellowCards,
        SeasonPlayerStandingType.TopYellowRedCards,
        SeasonPlayerStandingType.TopRedCards,
        SeasonPlayerStandingType.TopOwnGoals,
      ],
      renderItem: ({ item }: ListRenderItemInfo<SeasonPlayerStandingType>) => (
        <View style={styles.pickerRow}>
          <Text style={styles.pickerText}>
            {translate(`tournamentPlayerStandings.types.${item}.long`)}
          </Text>
        </View>
      ),
      onSelect: this.handleSelectPlayerStandingType,
      backgroundColor: colors.navigationBackground,
    })
  }

  render() {
    const {
      onSelectPlayer,
      onSelectTeam,
      onSelectMatch,
      seasonMatches,
      seasonResults,
      seasonStandings,
      playerStandings,
      localizedTeams,
      today,
      teamPopularity,
      onBack,
      isLoading,
      isGenerating,
    } = this.props
    const { playerStandingType } = this.state
    const selectedPlayerStatistics =
      playerStandings && playerStandings[playerStandingType]
    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <GenericNavigationHeader onBack={onBack}>
          <TournamentSelectorButton
            onPress={this.showTournamentSelector}
            tintColor={colors.secondaryText}
          />
        </GenericNavigationHeader>
        <SwiperContainer
          titles={[
            translate('tournaments.matches'),
            translate('tournaments.standings'),
            translate('tournaments.statistics'),
          ]}
        >
          {seasonMatches ? (
            <SeasonMatches
              onSelectMatch={onSelectMatch}
              results={seasonResults}
              matchesByDay={seasonMatches}
              localizedTeams={localizedTeams}
              today={today}
              topInset={largeInset}
            />
          ) : (
            <></>
          )}
          {seasonStandings ? (
            <Standings
              standingGroups={seasonStandings}
              onSelectTeam={onSelectTeam}
              localizedTeams={localizedTeams}
              teamPopularity={teamPopularity}
              topInset={largeInset}
            />
          ) : (
            <></>
          )}
          <View
            style={[styles.playerStandingsContainer, { marginTop: largeInset }]}
          >
            <TouchableOpacity
              onPress={this.showPlayerStandingTypeSelector}
              style={[styles.selectButton]}
            >
              <Text style={styles.selectButtonText}>
                {translate(
                  `tournamentPlayerStandings.types.${playerStandingType}.long`
                )}
              </Text>
              <Image
                source={require('../../../../assets/icons/buttons/down-arrow2.png')}
              />
            </TouchableOpacity>
            {selectedPlayerStatistics && (
              <PlayerStandings
                players={selectedPlayerStatistics}
                onSelectPlayer={onSelectPlayer}
                type={playerStandingType}
                localizedTeams={localizedTeams}
              />
            )}
          </View>
        </SwiperContainer>
      </ScreenWrapper>
    )
  }
}

function mapStateToProps(state: ReduxState): ReduxProps {
  const { activeTournament } = state.general
  const basic = {
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
  }
  if (!activeTournament) {
    return basic
  }
  const { id: seasonId } = activeTournament.currentSeason
  const {
    [fq.seasonMatchCalendar.key(seasonId)]: seasonMatches,
    [fq.seasonResults.key(seasonId)]: seasonResults,
    [fq.seasonPlayerStandings.key(seasonId)]: playerStandings,
    [fq.seasonStandings.key(seasonId)]: seasonStandings,
    [fq.seasonTeamPopularity.key(seasonId)]: teamPopularity,
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
    [fq.allTournaments.key()]: tournaments,
  } = state.firestore.data

  return {
    ...basic,
    activeTournament,
    seasonMatches: seasonMatches && seasonMatches.body,
    seasonResults: seasonResults && seasonResults.body,
    playerStandings: playerStandings && playerStandings.body,
    seasonStandings: seasonStandings && seasonStandings.body,
    localizedTeams: localizedTeams && localizedTeams.body,
    tournaments: selectors.tournamentBodies(tournaments),
    teamPopularity,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      setTournament: actions.setTournament,
    },
    dispatch
  )

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const { activeTournament } = state.general
    if (!activeTournament) {
      return []
    }
    const { id: seasonId } = activeTournament.currentSeason
    return [
      fq.seasonMatchCalendar.query(seasonId),
      fq.seasonResults.query(seasonId),
      fq.seasonPlayerStandings.query(seasonId),
      fq.seasonStandings.query(seasonId),
      fq.localizedTeams.query(currentLanguageCode),
      fq.allTournaments.query(),
      fq.seasonTeamPopularity.query(seasonId),
    ]
  })
  // @ts-ignore https://github.com/prescottprue/react-redux-firebase/issues/682
)(LeaguesScreen)

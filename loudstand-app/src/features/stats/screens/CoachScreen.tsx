import React, { Component } from 'react'
import { StyleSheet, View, ScrollView, Dimensions } from 'react-native'
import { Navigation, EventSubscription } from 'react-native-navigation'
import {
  ID,
  PlayerRoot,
  LabelValuePair,
  ISO8601Date,
  Question,
  LocalizedTeams,
  QuestionResultsSummaryDocument,
  UserVoteHistory,
  SubmittedVote,
  PlayerTeamRole,
  SubstitutionApprovalResultDocument,
  PopularityDocument,
  TeamRoot,
  QuestionResponseType,
} from '@loudstand/shared'
import { compose, Store, bindActionCreators, Dispatch } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import { connect } from 'react-redux'
import moment from 'moment'
import countries from 'i18n-iso-countries'
import ScreenWrapper from '../../../components/ScreenWrapper'
import * as selectors from '../../../store/selectors'
import {
  ReduxState,
  SubmittedVotes,
  ChosenTeamList,
} from '../../../store/model'
import ProfileTopBar from '../components/ProfileTopBar'
import * as fq from '../../../store/firestoreQueries'
import { translate, currentLocale } from '../../../translation'
import ProfileNavigationHeader from '../../../components/ProfileNavigationHeader'
import {
  getImageUrl,
  ItemImageType,
  localizeTeam,
  getComparatorForKey,
  fallbackImages,
  numberToPercentage,
} from '../../../utils'
import { votingFlowThunk } from '../../user/votingFlow'
import SwiperContainer, {
  smallInset,
} from '../../../components/SwiperContainer'
import PercentageCircle from '../../../components/PercentageCircle'
import PercentageCircleWithLines from '../../../components/PercentageCircleWithLines'
import MentionsInQuestions from '../components/MentionsInQuestions'
import { yearFromDate } from './PlayerScreen'
import LabelValueList from '../../../components/LabelValueList'

const currentLanguageCode = currentLocale.languageCode

const { width } = Dimensions.get('screen')
const topBarHeight = width / 3

const roleToLabelValuePair = (
  { team, number, startDate, endDate }: PlayerTeamRole,
  localizedTeams?: LocalizedTeams
): LabelValuePair => ({
  label: number
    ? `${localizeTeam(team, localizedTeams)}`
    : localizeTeam(team, localizedTeams),
  value:
    (startDate &&
      endDate &&
      `${startDate ? yearFromDate(startDate) : ''} - ${
        endDate ? yearFromDate(endDate) : ''
      }`) ||
    '',
})

function makeTopBarData(
  manager: PlayerRoot,
  localizedTeams?: LocalizedTeams
): LabelValuePair[] {
  const result = []
  manager.birthDay &&
    result.push({
      label: translate('playerProfile.age'),
      value: moment()
        .diff(manager.birthDay, 'year')
        .toString(),
    })
  manager.country &&
    result.push({
      label: translate('playerProfile.country'),
      value: countries.getName(manager.country, currentLanguageCode),
    })
  const activeRole = manager.roles.find(role => role.active)
  activeRole &&
    result.push({
      label: translate('managerProfile.currentTeam'),
      value: localizeTeam(activeRole.team, localizedTeams),
    })
  return result
}

const styles = StyleSheet.create({
  slideContainer: {
    flex: 1,
  },
  statsContainer: {},
  statsRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
})

interface ReduxProps {
  manager?: PlayerRoot
  questions?: { [key in ISO8601Date]: Question }
  localizedTeams?: LocalizedTeams
  questionResults?: QuestionResultsSummaryDocument
  voteHistory?: UserVoteHistory
  substitutionApproval?: SubstitutionApprovalResultDocument
  submittedVotes?: SubmittedVotes
  managerPopularity?: PopularityDocument
  currentRole?: PlayerTeamRole
  teamRoot?: TeamRoot
  availableTeams: ChosenTeamList
  isLoading: boolean
  isGenerating: boolean
}
interface ReduxActions {
  onVote: (vote: SubmittedVote) => void
}

export interface NavigationProps {
  id: ID
  onBack: () => void
}

interface Props extends ReduxProps, ReduxActions, NavigationProps {}
interface State {
  isStatsSlideVisible: boolean
}
export class CoachScreen extends Component<Props, State> {
  state = { isStatsSlideVisible: false }

  componentDidMount = () => {
    this.navigationEventListener = Navigation.events().bindComponent(this)
  }

  componentDidAppear = () => {
    this.setState({ isStatsSlideVisible: true })
  }

  navigationEventListener?: EventSubscription

  render() {
    const {
      manager,
      id,
      onBack,
      questions,
      questionResults,
      onVote,
      voteHistory,
      localizedTeams,
      substitutionApproval,
      submittedVotes,
      managerPopularity,
      availableTeams,
      isLoading,
      isGenerating,
    } = this.props
    const { isStatsSlideVisible } = this.state
    const topBarData = manager && makeTopBarData(manager, localizedTeams)
    const previousRoles = manager
      ? manager.roles
          .filter(role => !role.active)
          .sort(getComparatorForKey('endDate', true))
          .map(role => roleToLabelValuePair(role, localizedTeams))
      : []
    const sections = [
      {
        title: translate('playerProfile.previous'),
        data: previousRoles,
      },
    ]
    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <ProfileNavigationHeader
          backgroundHeight={topBarHeight}
          defaultBackgroundSource={fallbackImages.managerActionShot}
          backgroundSource={{
            uri: getImageUrl(id, ItemImageType.Actionshot),
          }}
          onBack={onBack}
          title={manager ? manager.name : ''}
        />
        {topBarData && <ProfileTopBar data={topBarData} />}
        <SwiperContainer>
          <View style={styles.slideContainer}>
            <ScrollView
              style={[styles.statsContainer]}
              contentContainerStyle={{
                paddingTop: smallInset,
              }}
            >
              <View style={styles.statsRow}>
                {managerPopularity && (
                  <PercentageCircle
                    title={translate('managerProfile.popularity')}
                    percentage={numberToPercentage(managerPopularity.value / 5)}
                    shouldAnimate
                    isVisible={isStatsSlideVisible}
                  />
                )}
              </View>
              <View style={styles.statsRow}>
                {substitutionApproval && (
                  <PercentageCircleWithLines
                    title={translate('managerProfile.substitutionsApproval')}
                    percentage={numberToPercentage(
                      substitutionApproval.result[QuestionResponseType.Yes]
                    )}
                    shouldAnimate
                    isVisible={isStatsSlideVisible}
                  />
                )}
              </View>
              <LabelValueList sections={sections} />
            </ScrollView>
          </View>
          {questions && Object.keys(questions).length > 0 && (
            <MentionsInQuestions
              topInset={smallInset}
              availableTeams={availableTeams}
              questions={questions}
              questionResults={questionResults}
              onVote={onVote}
              voteHistory={voteHistory}
              submittedVotes={submittedVotes}
            />
          )}
        </SwiperContainer>
      </ScreenWrapper>
    )
  }
}

const mapStateToProps = (state: ReduxState, props: Props): ReduxProps => {
  const { uid } = state.firebase.auth
  const {
    [fq.playerRoot.key(props.id)]: manager,
    [fq.playerQuestions.key(props.id)]: questions,
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
    [fq.playerQuestionResults.key(props.id)]: questionResults,
    [fq.userVoteHistory.key(uid)]: voteHistory,
  } = state.firestore.data
  const result = {
    manager: manager && manager.body,
    localizedTeams: localizedTeams && localizedTeams.body,
    questions,
    questionResults,
    voteHistory,
    submittedVotes: state.general.submittedVotes,
    availableTeams: state.general.availableTeams,
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
  }
  if (!manager) {
    return result
  }
  const currentRole = (manager.body as PlayerRoot).roles.find(
    role => role.active
  )
  if (!currentRole) {
    return result
  }
  const teamRootDoc = state.firestore.data[fq.teamRoot.key(currentRole.team.id)]
  const teamRoot: TeamRoot | undefined = teamRootDoc && teamRootDoc.body
  if (!teamRoot || !teamRoot.defaultCurrentSeasonId) {
    return {
      ...result,
      currentRole,
      teamRoot,
    }
  }
  const playerId = props.id
  const seasonId = teamRoot.defaultCurrentSeasonId
  const {
    [fq.managerPopularity.key({ playerId, seasonId })]: managerPopularity,
    [fq.managerSubstitutionApproval.key({
      playerId,
      seasonId,
    })]: substitutionApproval,
  } = state.firestore.data

  return {
    ...result,
    substitutionApproval,
    managerPopularity,
    currentRole,
    teamRoot,
  }
}

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      onVote: votingFlowThunk,
    },
    dispatch
  )

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const { uid } = state.firebase.auth
    const list = [
      fq.playerRoot.query(props.id),
      fq.localizedTeams.query(currentLanguageCode),
      fq.playerQuestions.query(props.id),
      fq.playerQuestionResults.query(props.id),
      fq.userVoteHistory.query(uid),
    ]
    if (!props.currentRole) {
      return list
    }
    if (!props.teamRoot || !props.teamRoot.defaultCurrentSeasonId) {
      return [...list, fq.teamRoot.query(props.currentRole.team.id)]
    }
    return [
      ...list,
      fq.teamRoot.query(props.currentRole.team.id),
      fq.managerPopularity.query({
        playerId: props.id,
        seasonId: props.teamRoot.defaultCurrentSeasonId,
      }),
      fq.managerSubstitutionApproval.query({
        playerId: props.id,
        seasonId: props.teamRoot.defaultCurrentSeasonId,
      }),
    ]
  })
  // @ts-ignore https://github.com/prescottprue/react-redux-firebase/issues/682
)(CoachScreen)

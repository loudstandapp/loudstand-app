import React, { Component, Key } from 'react'
import {
  StyleSheet,
  View,
  ScrollView,
  Dimensions,
  SectionListData,
} from 'react-native'
import {
  ID,
  PlayerRoot,
  PlayerTeamRole,
  ISO8601Date,
  LabelValuePair,
  LocalizedTeams,
  Question,
  QuestionResultsSummaryDocument,
  UserVoteHistory,
  SubmittedVote,
  PopularityDocument,
  TeamRoot,
  TeamSeasonStatistics,
  TeamStatisticsSeason,
} from '@loudstand/shared'
import { compose, Store, Dispatch, bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import moment from 'moment'
import countries from 'i18n-iso-countries'
import ScreenWrapper from '../../../components/ScreenWrapper'
import * as selectors from '../../../store/selectors'
import {
  ReduxState,
  SubmittedVotes,
  ChosenTeamList,
} from '../../../store/model'
import ProfileTopBar from '../components/ProfileTopBar'
import {
  getComparatorForKey,
  localizeTeam,
  getImageUrl,
  ItemImageType,
  fallbackImages,
  numberToPercentage,
} from '../../../utils'
import LabelValueList from '../../../components/LabelValueList'
import { currentLocale, translate } from '../../../translation'
import * as fq from '../../../store/firestoreQueries'
import { votingFlowThunk } from '../../user/votingFlow'
import ProfileNavigationHeader from '../../../components/ProfileNavigationHeader'
import SwiperContainer, {
  smallInset,
} from '../../../components/SwiperContainer'
import PercentageCircle from '../../../components/PercentageCircle'
import PercentageCircleWithLines from '../../../components/PercentageCircleWithLines'
import MentionsInQuestions from '../components/MentionsInQuestions'

const currentLanguageCode = currentLocale.languageCode

const { width } = Dimensions.get('screen')
const topBarHeight = width / 3

export const yearFromDate = (date: ISO8601Date) => new Date(date).getFullYear()

function makeTopBarData(player: PlayerRoot): LabelValuePair[] {
  const result = []
  player.birthDay &&
    result.push({
      label: translate('playerProfile.age'),
      value: moment()
        .diff(player.birthDay, 'year')
        .toString(),
    })
  player.country &&
    result.push({
      label: translate('playerProfile.country'),
      value: countries.getName(player.country, currentLanguageCode),
    })
  player.height &&
    result.push({
      label: translate('playerProfile.height'),
      value: player.height.toString(),
    })
  player.weight &&
    result.push({
      label: translate('playerProfile.weight'),
      value: player.weight.toString(),
    })
  return result
}

const roleToLabelValuePair = (
  { team, number, startDate, endDate }: PlayerTeamRole,
  localizedTeams?: LocalizedTeams
): LabelValuePair => ({
  label: number
    ? `${localizeTeam(team, localizedTeams)} (${number})`
    : localizeTeam(team, localizedTeams),
  value:
    (startDate &&
      endDate &&
      `${startDate ? yearFromDate(startDate) : ''} - ${
        endDate ? yearFromDate(endDate) : ''
      }`) ||
    '',
})

const fields = [
  'matches_played',
  'minutes_played',
  'goals_scored',
  'assists',
  'goal_attempts',
  'shots_on_goal',
  'shots_off_goal',
  'shots_blocked',
  'yellow_cards',
  'yellow_red_cards',
  'red_cards',
  'substituted_in',
  'substituted_out',
  'own_goals',
  'offsides',
]

function getStatisticsSections(
  playerId: ID,
  seasonStatistics: TeamSeasonStatistics
): SectionListData<LabelValuePair>[] {
  return Object.values(seasonStatistics)
    .reduce(
      (
        result: SectionListData<LabelValuePair>[],
        season: TeamStatisticsSeason
      ) => {
        if (!season.players[playerId]) {
          return result
        }
        if (!season.players[playerId].statistics) {
          return result
        }
        const data = season.players[playerId].statistics

        const section = {
          title: season.name,
          data: fields.reduce((acc: LabelValuePair[], field: string) => {
            if (!data[field]) {
              return acc
            }
            return [
              ...acc,
              {
                label: translate(`playerStats.${field}`),
                value: data[field].toString(),
              },
            ]
          }, []),
        }
        return [...result, section]
      },
      []
    )
    .filter(season => Object.keys(season).length > 0)
}

const styles = StyleSheet.create({
  statsContainer: {},
  statsRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
})

interface ReduxProps {
  player?: PlayerRoot
  questions?: { [key in ISO8601Date]: Question }
  localizedTeams?: LocalizedTeams
  questionResults?: QuestionResultsSummaryDocument
  voteHistory?: UserVoteHistory
  submittedVotes?: SubmittedVotes
  playerPopularity?: PopularityDocument
  playerMostValuableShare?: PopularityDocument
  playerLineupShare?: PopularityDocument
  currentRole?: PlayerTeamRole
  teamRoot?: TeamRoot
  teamSeasonStatistics?: TeamSeasonStatistics
  availableTeams: ChosenTeamList
  isLoading: boolean
  isGenerating: boolean
}

interface ReduxActions {
  onVote: (vote: SubmittedVote) => void
}

export interface NavigationProps {
  id: ID
  onBack: () => void
}

const STATS_KEY = 'stats'

interface Props extends ReduxProps, ReduxActions, NavigationProps {}

interface State {
  isStatsSlideVisible: boolean
}
export class PlayerScreen extends Component<Props, State> {
  state = { isStatsSlideVisible: false }

  handleSnapToSlide = (key: Key) => {
    if (key === STATS_KEY) {
      console.log(STATS_KEY)
      this.setState({ isStatsSlideVisible: true })
    }
  }

  render() {
    const {
      player,
      localizedTeams,
      questions,
      onBack,
      id,
      questionResults,
      voteHistory,
      onVote,
      submittedVotes,
      playerPopularity,
      playerLineupShare,
      playerMostValuableShare,
      availableTeams,
      teamSeasonStatistics,
      isLoading,
      isGenerating,
    } = this.props
    const { isStatsSlideVisible } = this.state
    const topBarData = player && makeTopBarData(player)
    const currentRoles = player
      ? player.roles
          .filter(role => role.active)
          .sort(getComparatorForKey('startDate', true))
          .map(role => roleToLabelValuePair(role, localizedTeams))
      : []
    const previousRoles = player
      ? player.roles
          .filter(role => !role.active)
          .sort(getComparatorForKey('endDate', true))
          .map(role => roleToLabelValuePair(role, localizedTeams))
      : []

    const sections = [
      {
        title: '',
        data:
          player && player.position
            ? [
                {
                  label: translate('playerProfile.position'),
                  value: translate(`playerPosition.${player.position}`),
                },
              ]
            : [],
      },
      {
        title: translate('playerProfile.current'),
        data: currentRoles,
      },
      {
        title: translate('playerProfile.previous'),
        data: previousRoles,
      },
    ]
    const [primaryRole] = currentRoles
    const statisticsSections =
      teamSeasonStatistics && getStatisticsSections(id, teamSeasonStatistics)

    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <ProfileNavigationHeader
          defaultBackgroundSource={fallbackImages.playerActionShot}
          backgroundSource={{
            uri: getImageUrl(id, ItemImageType.Actionshot),
          }}
          onBack={onBack}
          title={player ? player.name : ''}
          subTitle={primaryRole && primaryRole.label}
          backgroundHeight={topBarHeight}
        />
        {topBarData && <ProfileTopBar data={topBarData} />}
        <SwiperContainer onSnapToSlide={this.handleSnapToSlide}>
          <LabelValueList sections={sections} topInset={smallInset} />
          {// REMEMBER to update this if changing content
          (playerPopularity ||
            playerMostValuableShare ||
            playerLineupShare) && (
            <View key={STATS_KEY}>
              <ScrollView
                style={styles.statsContainer}
                contentContainerStyle={{ paddingTop: smallInset }}
              >
                <View style={styles.statsRow}>
                  {playerPopularity && (
                    <PercentageCircle
                      title={translate('playerProfile.popularity')}
                      percentage={numberToPercentage(playerPopularity.value)}
                      shouldAnimate
                      isVisible={isStatsSlideVisible}
                    />
                  )}
                </View>
                <View style={styles.statsRow}>
                  {playerMostValuableShare && (
                    <PercentageCircleWithLines
                      title={translate('playerProfile.mostValuablePlayer')}
                      percentage={numberToPercentage(
                        playerMostValuableShare.value
                      )}
                      shouldAnimate
                      isVisible={isStatsSlideVisible}
                    />
                  )}
                  {playerLineupShare && (
                    <PercentageCircleWithLines
                      title={translate('playerProfile.lineup')}
                      percentage={numberToPercentage(playerLineupShare.value)}
                      shouldAnimate
                      isVisible={isStatsSlideVisible}
                    />
                  )}
                </View>
              </ScrollView>
            </View>
          )}
          {statisticsSections && statisticsSections.length > 0 && (
            <LabelValueList
              sections={statisticsSections}
              topInset={smallInset}
            />
          )}
          {questions && Object.keys(questions).length > 0 && (
            <MentionsInQuestions
              topInset={smallInset}
              availableTeams={availableTeams}
              questions={questions}
              questionResults={questionResults}
              onVote={onVote}
              voteHistory={voteHistory}
              submittedVotes={submittedVotes}
            />
          )}
        </SwiperContainer>
      </ScreenWrapper>
    )
  }
}

const mapStateToProps = (state: ReduxState, props: Props): ReduxProps => {
  const { uid } = state.firebase.auth
  const {
    [fq.playerRoot.key(props.id)]: player,
    [fq.playerQuestions.key(props.id)]: questions,
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
    [fq.playerQuestionResults.key(props.id)]: questionResults,
    [fq.userVoteHistory.key(uid)]: voteHistory,
  } = state.firestore.data
  const result = {
    localizedTeams: localizedTeams && localizedTeams.body,
    player: player && player.body,
    questions,
    questionResults,
    voteHistory,
    submittedVotes: state.general.submittedVotes,
    availableTeams: state.general.availableTeams,
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
  }
  if (!player) {
    return result
  }
  const currentRole = (player.body as PlayerRoot).roles.find(
    role => role.active
  )
  if (!currentRole) {
    return result
  }
  const teamRootDoc = state.firestore.data[fq.teamRoot.key(currentRole.team.id)]
  const teamRoot: TeamRoot | undefined = teamRootDoc && teamRootDoc.body
  if (!teamRoot || !teamRoot.defaultCurrentSeasonId) {
    return {
      ...result,
      currentRole,
      teamRoot,
    }
  }

  const playerId = props.id
  const seasonId = teamRoot.defaultCurrentSeasonId
  const {
    [fq.teamSeasonStatistics.key(teamRoot.id)]: teamSeasonStatistics,
    [fq.playerPopularity.key({ playerId, seasonId })]: playerPopularity,
    [fq.playerLineupShare.key({ playerId, seasonId })]: playerLineupShare,
    [fq.playerMostValuableShare.key({
      playerId,
      seasonId,
    })]: playerMostValuableShare,
  } = state.firestore.data
  return {
    ...result,
    currentRole,
    teamRoot,
    teamSeasonStatistics: teamSeasonStatistics && teamSeasonStatistics.body,
    playerPopularity,
    playerLineupShare,
    playerMostValuableShare,
  }
}
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      onVote: votingFlowThunk,
    },
    dispatch
  )

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const { uid } = state.firebase.auth
    const list = [
      fq.playerRoot.query(props.id),
      fq.localizedTeams.query(currentLanguageCode),
      fq.playerQuestions.query(props.id),
      fq.playerQuestionResults.query(props.id),
      fq.userVoteHistory.query(uid),
    ]
    if (!props.currentRole) {
      return list
    }
    if (!props.teamRoot || !props.teamRoot.defaultCurrentSeasonId) {
      return [...list, fq.teamRoot.query(props.currentRole.team.id)]
    }
    return [
      ...list,
      fq.teamRoot.query(props.currentRole.team.id),
      fq.teamSeasonStatistics.query(props.currentRole.team.id),
      fq.playerLineupShare.query({
        playerId: props.id,
        seasonId: props.teamRoot.defaultCurrentSeasonId,
      }),
      fq.playerMostValuableShare.query({
        playerId: props.id,
        seasonId: props.teamRoot.defaultCurrentSeasonId,
      }),
      fq.playerPopularity.query({
        playerId: props.id,
        seasonId: props.teamRoot.defaultCurrentSeasonId,
      }),
    ]
  })
  // @ts-ignore https://github.com/prescottprue/react-redux-firebase/issues/682
)(PlayerScreen)

/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { storiesOf } from '@storybook/react-native'
import {
  PlayerPosition,
  PlayerRoot,
  PlayerTeamRoleType,
} from '@loudstand/shared'
import { action } from '@storybook/addon-actions'
import { PlayerScreen } from './PlayerScreen'
import { ChosenTeamList } from '../../../store/model'

const TEST_PLAYER_ID = 'sr:player:47443'

const TEST_AVAILABLE_TEAMS: ChosenTeamList = {
  'sr:competitor:36': {
    country: 'UK',
    id: 'sr:competitor:36',
    name: 'Middlesbrough FC',
    isFavorite: true,
  },
  'sr:competitor:22596': {
    country: 'CZ',
    id: 'sr:competitor:22596',
    name: 'Czech Republic',
  },
  'sr:competitor:38': {
    country: 'UK',
    id: 'sr:competitor:38',
    name: 'Chelsea FC',
    isFavorite: true,
  },
}

const player: PlayerRoot = {
  birthDay: '1993-05-15',
  country: 'CZE',
  height: 184,
  id: TEST_PLAYER_ID,
  name: 'Kalas, Tomas',
  position: PlayerPosition.Defender,
  roles: [
    {
      active: false,
      endDate: '2016-05-31',
      number: 26,
      startDate: '2015-01-09',
      team: {
        country: 'ENG',
        id: 'sr:competitor:36',
        name: 'Middlesbrough FC',
      },
      type: PlayerTeamRoleType.OnLoan,
    },
    {
      active: true,
      endDate: null,
      number: null,
      startDate: '2010-07-01',
      team: {
        country: 'ENG',
        id: 'sr:competitor:38',
        name: 'Chelsea FC',
      },
      type: PlayerTeamRoleType.Player,
    },
    {
      active: false,
      endDate: '2017-05-31',
      number: 26,
      startDate: '2016-07-13',
      team: {
        country: 'ENG',
        id: 'sr:competitor:43',
        name: 'Fulham FC',
      },
      type: PlayerTeamRoleType.OnLoan,
    },
    {
      active: false,
      endDate: '2018-05-31',
      number: 26,
      startDate: '2017-07-27',
      team: {
        country: 'ENG',
        id: 'sr:competitor:43',
        name: 'Fulham FC',
      },
      type: PlayerTeamRoleType.OnLoan,
    },
    {
      active: true,
      endDate: '2019-05-31',
      number: 22,
      startDate: '2018-08-23',
      team: {
        country: 'ENG',
        id: 'sr:competitor:58',
        name: 'Bristol City',
      },
      type: PlayerTeamRoleType.OnLoan,
    },
    {
      active: false,
      endDate: '2011-06-30',
      number: 28,
      startDate: '2010-01-01',
      team: {
        country: 'CZE',
        id: 'sr:competitor:2207',
        name: 'SK Sigma Olomouc',
      },
      type: PlayerTeamRoleType.Player,
    },
    {
      active: false,
      endDate: '2011-06-30',
      number: 2,
      startDate: '2010-08-01',
      team: {
        country: 'CZE',
        id: 'sr:competitor:2207',
        name: 'SK Sigma Olomouc',
      },
      type: PlayerTeamRoleType.OnLoan,
    },
    {
      active: false,
      endDate: '2015-01-05',
      number: null,
      startDate: '2014-07-01',
      team: {
        country: 'DEU',
        id: 'sr:competitor:2525',
        name: '1 FC Cologne II',
      },
      type: PlayerTeamRoleType.OnLoan,
    },
    {
      active: false,
      endDate: '2015-01-05',
      number: 3,
      startDate: '2014-07-01',
      team: {
        country: 'DEU',
        id: 'sr:competitor:2671',
        name: '1 FC Cologne',
      },
      type: PlayerTeamRoleType.OnLoan,
    },
    {
      active: false,
      endDate: '2013-06-30',
      number: 2,
      startDate: '2011-08-01',
      team: {
        country: 'NLD',
        id: 'sr:competitor:2958',
        name: 'Vitesse Arnhem',
      },
      type: PlayerTeamRoleType.OnLoan,
    },
    {
      active: true,
      endDate: null,
      number: 6,
      startDate: null,
      team: {
        country: 'CZE',
        id: 'sr:competitor:4714',
        name: 'Czech Republic',
      },
      type: PlayerTeamRoleType.Player,
    },
    {
      active: false,
      endDate: '2015-11-15',
      number: 22,
      startDate: null,
      team: {
        country: 'CZE',
        id: 'sr:competitor:6022',
        name: 'Czech Republic',
      },
      type: PlayerTeamRoleType.Player,
    },
    {
      active: false,
      endDate: null,
      number: 5,
      startDate: null,
      team: {
        country: 'CZE',
        id: 'sr:competitor:22501',
        name: 'Czech Republic',
      },
      type: PlayerTeamRoleType.Player,
    },
    {
      active: false,
      endDate: null,
      number: 2,
      startDate: null,
      team: {
        country: 'CZE',
        id: 'sr:competitor:22596',
        name: 'Czech Republic',
      },
      type: PlayerTeamRoleType.Player,
    },
  ],
  weight: 71,
}

storiesOf('PlayerScreen', module)
  // .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <PlayerScreen
      availableTeams={TEST_AVAILABLE_TEAMS}
      id={TEST_PLAYER_ID}
      player={player}
      onVote={action('onVote')}
      onBack={action('onBack')}
    />
  ))
  .add('no data', () => (
    <PlayerScreen
      availableTeams={TEST_AVAILABLE_TEAMS}
      id={TEST_PLAYER_ID}
      onVote={action('onVote')}
      onBack={action('onBack')}
    />
  ))

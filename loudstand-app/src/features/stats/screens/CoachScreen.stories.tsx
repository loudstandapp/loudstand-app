/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { PlayerRoot, PlayerTeamRoleType } from '@loudstand/shared'
import { storiesOf } from '@storybook/react-native'
import { CoachScreen } from './CoachScreen'

const manager: PlayerRoot = {
  birthDay: '1975-02-07',
  country: 'DNK',
  height: null,
  id: 'sr:player:123285',
  name: 'Henriksen, Bo',
  position: null,
  roles: [
    {
      active: true,
      endDate: null,
      number: null,
      startDate: '2014-07-01',
      team: {
        country: 'DNK',
        id: 'sr:competitor:1297',
        name: 'AC Horsens',
      },
      type: PlayerTeamRoleType.Manager,
    },
    {
      active: true,
      endDate: '2014-06-30',
      number: null,
      startDate: '2006-12-15',
      team: {
        country: 'DNK',
        id: 'sr:competitor:1299',
        name: 'Bronshoj BK',
      },
      type: PlayerTeamRoleType.Manager,
    },
  ],
  weight: null,
}

storiesOf('CoachScreen', module)
  // .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('default', () => <CoachScreen id="123" manager={manager} />)
  .add('no data', () => <CoachScreen id="123" />)

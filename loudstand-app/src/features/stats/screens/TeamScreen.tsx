import React from 'react'
import {
  StyleSheet,
  Text,
  SectionList,
  SectionListRenderItemInfo,
  ScrollView,
  View,
  Dimensions,
  SectionListData,
} from 'react-native'
import { Navigation, EventSubscription } from 'react-native-navigation'
import {
  TeamRoot,
  CalendarItemByDay,
  MatchListItem,
  PlayerList,
  ID,
  PlayerPosition,
  TeamPlayer,
  MostValuablePlayerResultDocument,
  LineupSuggestionResultDocument,
  ISO8601Date,
  SeasonTeamPopularityDocument,
  SeasonBasic,
  LocalizedTeams,
  SubmittedVote,
  UserVoteHistory,
  QuestionResultsSummaryDocument,
  Question,
  TeamSeasonStatistics,
  LabelValuePair,
  TeamStatisticsSeason,
} from '@loudstand/shared'
import { compose, Store, bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import {
  ReduxState,
  ChosenTeamList,
  SubmittedVotes,
} from '../../../store/model'
import {
  getComparatorForKey,
  ItemImageType,
  getImageUrl,
  fallbackImages,
  numberToPercentage,
  localizeTeam,
} from '../../../utils'
import fonts from '../../../theme/fonts'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import { translate, currentLocale } from '../../../translation'
import PlayerListItem from '../../../components/PlayerListItem'
import * as fq from '../../../store/firestoreQueries'
import ProfileNavigationHeader from '../../../components/ProfileNavigationHeader'
import Top3Players from '../../../components/Top3Players'
import SwiperContainer, {
  smallInset,
} from '../../../components/SwiperContainer'
import PercentageCircle from '../../../components/PercentageCircle'
import TeamNextMatch from '../components/TeamNextMatch'
import CurrentCompetitions from '../components/CurrentCompetitions'
import ScreenWrapper from '../../../components/ScreenWrapper'
import * as selectors from '../../../store/selectors'
import MentionsInQuestions from '../components/MentionsInQuestions'
import { votingFlowThunk } from '../../user/votingFlow'
import SimpleLineupResult from '../../../components/SimpleLineupResult'
import LabelValueList from '../../../components/LabelValueList'

const currentLanguageCode = currentLocale.languageCode

const { width } = Dimensions.get('screen')
const topBarHeight = width / 3

export function getNextMatch(
  calendar: CalendarItemByDay<MatchListItem>,
  today: ISO8601Date
): MatchListItem | null {
  const futureOrCurrentDates = Object.keys(calendar).filter(
    date => date >= today
  )
  const [nextDate] = futureOrCurrentDates.sort()
  if (!nextDate) {
    return null
  }
  const [nextMatch] = Object.values(calendar[nextDate])
  return nextMatch || null
}

export function getCurrentCompetitions(
  calendar: CalendarItemByDay<MatchListItem>,
  today: ISO8601Date
): SeasonBasic[] {
  const seasons = Object.keys(calendar)
    .filter(date => date >= today)
    .reduce((acc: MatchListItem[], date: ISO8601Date) => {
      return [...acc, ...Object.values(calendar[date])]
    }, [])
    .reduce(
      (acc: { [SeasonId: string]: SeasonBasic }, match: MatchListItem) => {
        return {
          ...acc,
          [match.season.id]: match.season,
        }
      },
      {}
    )
  return Object.values(seasons)
}

const fields = [
  'matches_played',
  'matches_won',
  'matches_drawn',
  'matches_lost',
  'goals_scored',
  'goals_conceded',
  'form',
]

function getStatisticsSections(
  seasonStatistics: TeamSeasonStatistics
): SectionListData<LabelValuePair>[] {
  return Object.values(seasonStatistics).map(
    (season: TeamStatisticsSeason) => ({
      title: season.name,
      data: fields.reduce((acc: LabelValuePair[], field: string) => {
        if (!season.team[field]) {
          return acc
        }
        return [
          ...acc,
          {
            label: translate(`teamStats.${field}`),
            value: season.team[field].toString(),
          },
        ]
      }, []),
    })
  )
}

const styles = StyleSheet.create({
  slideContainer: {
    flex: 1,
  },
  playerType: {
    textAlign: 'center',
    ...(fonts.HeaderSmall as object),
    margin: marginDefault,
  },
  squadTitle: {
    textAlign: 'center',
  },
  header: {
    textAlign: 'center',
    ...(fonts.HeaderSmall as object),
    marginVertical: spacingUnit,
  },
})

export interface NavigationProps {
  onSelectCoach: (id: ID) => void
  onSelectPlayer: (id: ID) => void
  onSelectMatch: (match: MatchListItem) => void
  onSelectSeason: (id: ID) => void
  onBack: () => void
  id: ID
  today: ISO8601Date
}

interface ReduxProps {
  teamRoot?: TeamRoot
  matchCalendar?: CalendarItemByDay<MatchListItem>
  players?: PlayerList
  mostValuablePlayers?: MostValuablePlayerResultDocument
  suggestedLineup?: LineupSuggestionResultDocument
  teamPopularity?: SeasonTeamPopularityDocument
  localizedTeams?: LocalizedTeams
  isLoading: boolean
  isGenerating: boolean
  questions?: { [key in ISO8601Date]: Question }
  questionResults?: QuestionResultsSummaryDocument
  voteHistory?: UserVoteHistory
  submittedVotes?: SubmittedVotes
  seasonStatistics?: TeamSeasonStatistics
  availableTeams: ChosenTeamList
}

interface ReduxActionProps {
  onVote: (vote: SubmittedVote) => void
}
interface State {
  isVisible: boolean
}

interface Props extends NavigationProps, ReduxProps, ReduxActionProps {}
export class TeamScreen extends React.PureComponent<Props, State> {
  state = { isVisible: false }

  componentDidMount = () => {
    if ('componentId' in this.props) {
      this.navigationEventListener = Navigation.events().bindComponent(this)
    }
  }

  componentDidAppear = () => {
    this.setState({ isVisible: true })
  }

  navigationEventListener?: EventSubscription

  render() {
    const {
      localizedTeams,
      onBack,
      onSelectPlayer,
      players,
      teamRoot,
      mostValuablePlayers,
      id,
      suggestedLineup,
      matchCalendar,
      onSelectMatch,
      today,
      teamPopularity,
      isLoading,
      isGenerating,
      questionResults,
      questions,
      availableTeams,
      submittedVotes,
      onVote,
      voteHistory,
      onSelectSeason,
      onSelectCoach,
      seasonStatistics,
    } = this.props
    const { isVisible } = this.state

    const sortedPlayersList = players
      ? Object.keys(players)
          .reduce(
            (acc: TeamPlayer[], playerId) => [...acc, players[playerId]],
            []
          )
          .sort(getComparatorForKey('number'))
      : []

    const playerSections = players
      ? [
          {
            title: translate('playerPosition.goalkeepers'),
            data: sortedPlayersList.filter(
              ({ position }) => position === PlayerPosition.Goalkeeper
            ),
          },
          {
            title: translate('playerPosition.defenders'),
            data: sortedPlayersList.filter(
              ({ position }) => position === PlayerPosition.Defender
            ),
          },
          {
            title: translate('playerPosition.midfielders'),
            data: sortedPlayersList.filter(
              ({ position }) => position === PlayerPosition.Midfielder
            ),
          },
          {
            title: translate('playerPosition.forwards'),
            data: sortedPlayersList.filter(
              ({ position }) => position === PlayerPosition.Forward
            ),
          },
        ]
      : []
    const managerSections =
      teamRoot && teamRoot.manager
        ? [
            {
              title: translate('lineup.manager'),
              data: [teamRoot.manager],
            },
          ]
        : []
    const sections = [...playerSections, ...managerSections]

    const nextMatch = matchCalendar && getNextMatch(matchCalendar, today)
    function getPopularity() {
      if (!teamPopularity) {
        return null
      }
      if (teamPopularity[id] === undefined) {
        return 0
      }
      return teamPopularity[id].value / 5
    }
    const popularity = getPopularity()

    const currentCompetitions =
      matchCalendar && getCurrentCompetitions(matchCalendar, today)
    const statisticsSections =
      seasonStatistics && getStatisticsSections(seasonStatistics)

    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <ProfileNavigationHeader
          defaultBackgroundSource={fallbackImages.teamPhoto}
          backgroundSource={{
            uri: getImageUrl(id, ItemImageType.TeamPhoto),
          }}
          onBack={onBack}
          title={teamRoot ? localizeTeam(teamRoot, localizedTeams) : ''}
          backgroundHeight={topBarHeight}
        />
        <SwiperContainer>
          <View style={styles.slideContainer}>
            <ScrollView contentContainerStyle={{ paddingTop: smallInset }}>
              {nextMatch && (
                <>
                  <Text style={styles.header}>
                    {translate('teamProfile.nextMatch')}
                  </Text>
                  <TeamNextMatch match={nextMatch} onPress={onSelectMatch} />
                </>
              )}
              {popularity !== null && (
                <PercentageCircle
                  shouldAnimate
                  percentage={numberToPercentage(popularity)}
                  title={translate('teamProfile.popularity')}
                  isVisible={isVisible}
                />
              )}
              {currentCompetitions && (
                <>
                  <Text style={styles.header}>
                    {translate('teamProfile.currentCompetitions')}
                  </Text>
                  <CurrentCompetitions
                    seasons={currentCompetitions}
                    onSelectSeason={onSelectSeason}
                  />
                </>
              )}
              {mostValuablePlayers && players && teamRoot && (
                <>
                  <Text style={styles.header}>
                    {translate('teamProfile.mostValuablePlayers')}
                  </Text>
                  <Top3Players
                    onSelectPlayer={onSelectPlayer}
                    teamPlayers={players}
                    results={mostValuablePlayers}
                    jerseyColors={teamRoot.jerseyColors || undefined}
                  />
                </>
              )}
            </ScrollView>
          </View>

          {suggestedLineup && players && teamRoot && (
            <ScrollView
              style={styles.slideContainer}
              contentContainerStyle={{ paddingTop: smallInset }}
            >
              <SimpleLineupResult
                onSelectPlayer={onSelectPlayer}
                teamPlayers={players}
                lineupSuggestionResult={suggestedLineup}
                jerseyColors={teamRoot.jerseyColors || undefined}
                title={translate('teamProfile.suggestedLineup')}
              />
            </ScrollView>
          )}
          <SectionList
            stickySectionHeadersEnabled={false}
            sections={sections}
            contentContainerStyle={{ paddingTop: smallInset }}
            renderItem={({ item }: SectionListRenderItemInfo<TeamPlayer>) => (
              <PlayerListItem
                player={item}
                onSelect={
                  teamRoot &&
                  teamRoot.manager &&
                  teamRoot.manager.id === item.id
                    ? onSelectCoach
                    : onSelectPlayer
                }
                jerseyColors={(teamRoot && teamRoot.jerseyColors) || undefined}
              />
            )}
            renderSectionHeader={({ section: { title } }) => (
              <Text style={[styles.playerType]}>{title}</Text>
            )}
            keyExtractor={item => item.id}
          />
          {statisticsSections && statisticsSections.length > 0 && (
            <LabelValueList
              topInset={smallInset}
              sections={statisticsSections}
            />
          )}
          {questions && Object.keys(questions).length > 0 && (
            <MentionsInQuestions
              topInset={smallInset}
              availableTeams={availableTeams}
              questions={questions}
              questionResults={questionResults}
              onVote={onVote}
              voteHistory={voteHistory}
              submittedVotes={submittedVotes}
            />
          )}
        </SwiperContainer>
      </ScreenWrapper>
    )
  }
}
const mapStateToProps = (state: ReduxState, props: Props): ReduxProps => {
  const { uid } = state.firebase.auth
  const {
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
    [fq.teamRoot.key(props.id)]: teamRoot,
    [fq.teamPlayers.key(props.id)]: players,
    [fq.teamMatchCalendar.key(props.id)]: matchCalendar,
    [fq.teamQuestions.key(props.id)]: questions,
    [fq.teamQuestionResults.key(props.id)]: questionResults,
    [fq.userVoteHistory.key(uid)]: voteHistory,
    [fq.teamMostValuablePlayers.key(props.id)]: mostValuablePlayers,
    [fq.teamSuggestedLineup.key(props.id)]: suggestedLineup,
    [fq.teamSeasonStatistics.key(props.id)]: seasonStatistics,
  } = state.firestore.data

  const basic = {
    localizedTeams: localizedTeams && localizedTeams.body,
    players: players && players.body,
    teamRoot: teamRoot && teamRoot.body,
    matchCalendar: matchCalendar && matchCalendar.body,
    mostValuablePlayers,
    suggestedLineup,
    questions,
    questionResults,
    voteHistory,
    seasonStatistics: seasonStatistics && seasonStatistics.body,
    availableTeams: state.general.availableTeams,
    submittedVotes: state.general.submittedVotes,
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
  }

  if (!teamRoot || !teamRoot.body || !teamRoot.body.defaultCurrentSeasonId) {
    return basic
  }

  const {
    [fq.seasonTeamPopularity.key(
      teamRoot.body.defaultCurrentSeasonId
    )]: teamPopularity,
  } = state.firestore.data
  return {
    ...basic,
    teamPopularity,
  }
}
const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      onVote: votingFlowThunk,
    },
    dispatch
  )

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const { uid } = state.firebase.auth
    const basic = [
      fq.localizedTeams.query(currentLanguageCode),
      fq.teamRoot.query(props.id),
      fq.teamPlayers.query(props.id),
      fq.teamMatchCalendar.query(props.id),
      fq.teamQuestionResults.query(props.id),
      fq.teamQuestions.query(props.id),
      fq.teamSeasonStatistics.query(props.id),
      fq.userVoteHistory.query(uid),
    ]
    if (!props.teamRoot || !props.teamRoot.defaultCurrentSeasonId) {
      return basic
    }
    return [
      ...basic,
      fq.teamMostValuablePlayers.query(props.id),
      fq.teamSuggestedLineup.query(props.id),
      fq.seasonTeamPopularity.query(props.teamRoot.defaultCurrentSeasonId),
    ]
  })
  // @ts-ignore https://github.com/prescottprue/react-redux-firebase/issues/682
)(TeamScreen)

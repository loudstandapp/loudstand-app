import React, { Component } from 'react'

import { StyleSheet, View } from 'react-native'
import { TeamRoot, ID, LocalizedTeams } from '@loudstand/shared'
import { compose, Store, Dispatch, bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import { RNFirebase } from 'react-native-firebase'
import { ReduxState, DismissedGuides } from '../../../store/model'
import TileButton from '../components/TileButton'
import colors from '../../../theme/colors'
import { translate, currentLocale } from '../../../translation'
import * as fq from '../../../store/firestoreQueries'
import RemoteImageBackground from '../../../components/RemoteImageBackground'
import { localizeTeam } from '../../../utils'
import Guide, { GuideType } from '../../../components/Guide'
import { dismissGuide } from '../../../store/actions'

const currentLanguageCode = currentLocale.languageCode

const USER_IMAGE_SIZE = 500
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  topImageBackground: {
    flex: 1,
  },
  tileRow: {
    flex: 0,
    aspectRatio: 2,
    flexDirection: 'row',
  },
})

export interface NavigationProps {
  onSelectUserProfile: () => void
  onSelectTeam: (id: ID) => void
  onSelectCoach: (id: ID) => void
  onSelectLeagues: () => void
  onSelectActivityHistory: (id: ID) => void
}

interface ReduxProps {
  teamRoot?: TeamRoot
  currentUser: RNFirebase.auth.OrNull<RNFirebase.User>
  localizedTeams?: LocalizedTeams
  dismissedGuides: DismissedGuides
  launchCount: number
}

interface ReduxActionProps {
  onDismissGuide: (type: GuideType) => void
}

interface Props extends NavigationProps, ReduxProps, ReduxActionProps {}

// eslint-disable-next-line react/prefer-stateless-function
export class StatsScreen extends Component<Props> {
  render() {
    const {
      localizedTeams,
      onSelectCoach,
      onSelectLeagues,
      onSelectTeam,
      onSelectUserProfile,
      teamRoot,
      currentUser,
      onSelectActivityHistory,
      dismissedGuides,
      onDismissGuide,
      launchCount,
    } = this.props
    return (
      <View style={styles.container}>
        <RemoteImageBackground
          source={
            currentUser && currentUser.photoURL
              ? { uri: `${currentUser.photoURL}?width=${USER_IMAGE_SIZE}` }
              : require('../../../../assets/icons/navigation/user-stats.png')
          }
          style={styles.topImageBackground}
        >
          <TileButton
            onPress={onSelectUserProfile}
            icon={require('../../../../assets/icons/navigation/user-stats.png')} // FIXME
            title={(currentUser && currentUser.displayName) || 'N/A'}
            subTitle={translate('stats.userProfile')}
            backgroundColors={['#000000aa', '#000000cc']}
            color={colors.secondaryText}
          />
        </RemoteImageBackground>
        <View style={styles.tileRow}>
          {teamRoot && (
            <TileButton
              onPress={() => onSelectTeam(teamRoot.id)}
              title={localizeTeam(teamRoot, localizedTeams)}
              subTitle={translate('stats.teamProfile')}
              icon={require('../../../../assets/icons/buttons/team-profile.png')}
              backgroundColors={[colors.background, colors.backgroundAlt]}
              color={colors.primaryText}
            />
          )}
          <TileButton
            onPress={() =>
              currentUser && onSelectActivityHistory(currentUser.uid)
            }
            icon={require('../../../../assets/icons/navigation/activity.png')}
            title={translate('stats.history')}
            subTitle={translate('stats.yourActivities')}
            backgroundColors={[colors.primary, colors.primaryAlt]}
            color={colors.secondaryText}
          />
        </View>
        <View style={styles.tileRow}>
          {teamRoot && teamRoot.manager && (
            <TileButton
              onPress={() =>
                teamRoot.manager && onSelectCoach(teamRoot.manager.id)
              }
              title={teamRoot.manager.name}
              subTitle={translate('stats.coachProfile')}
              icon={require('../../../../assets/icons/buttons/coach-profile.png')}
              backgroundColors={[colors.tertiaryLight, colors.tertiaryAlt]}
              color={colors.primaryText}
            />
          )}
          <TileButton
            onPress={onSelectLeagues}
            title={translate('stats.leagues')}
            subTitle={translate('stats.leagueProfile')}
            icon={require('../../../../assets/icons/buttons/league-profile.png')}
            backgroundColors={[colors.background, colors.backgroundAlt]}
            color={colors.primaryText}
          />
        </View>
        <Guide
          type={GuideType.Statistics}
          dismissedGuides={dismissedGuides}
          onDismiss={onDismissGuide}
          launchCount={launchCount}
        />
      </View>
    )
  }
}
const mapStateToProps = (state: ReduxState): ReduxProps => {
  const {
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
  } = state.firestore.data
  const { launchCount, dismissedGuides, activeTeam } = state.general
  const basic = {
    localizedTeams: localizedTeams && localizedTeams.body,
    currentUser: state.firebase.auth,
    launchCount,
    dismissedGuides,
  }

  if (!activeTeam) {
    return basic
  }
  const { [fq.teamRoot.key(activeTeam.id)]: teamRoot } = state.firestore.data
  return {
    ...basic,
    teamRoot: teamRoot && teamRoot.body,
  }
}

const mapDispatchToProps = (dispatch: Dispatch): ReduxActionProps =>
  bindActionCreators(
    {
      onDismissGuide: dismissGuide,
    },
    dispatch
  )

export default compose(
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const basic = [fq.localizedTeams.query(currentLanguageCode)]
    const { activeTeam } = state.general
    if (!activeTeam) {
      return basic
    }
    return [...basic, fq.teamRoot.query(activeTeam.id)]
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(StatsScreen)

/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { CalendarItemByDay, MatchListItem } from '@loudstand/shared'
import { LeaguesScreen } from './LeaguesScreen'
import {
  TEST_SEASON_STANDINGS,
  TEST_TEAM_POPULARITY,
} from '../components/Standings.stories'
import { TEST_PLAYER_STANDINGS } from '../components/PlayerStandings.stories'

export const TEST_SEASON_CALENDAR: CalendarItemByDay<MatchListItem> = {
  '2019-05-12': {
    'sr:match:14946355': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946355',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-05-12',
      startTime: '2019-05-12T14:00:00+00:00',
    },
    'sr:match:14946353': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946353',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-05-12',
      startTime: '2019-05-12T19:00:00+00:00',
    },
    'sr:match:14946341': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946341',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-05-12',
      startTime: '2019-05-12T14:00:00+00:00',
    },
    'sr:match:14946343': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946343',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-05-12',
      startTime: '2019-05-12T16:30:00+00:00',
    },
  },
  '2019-04-13': {
    'sr:match:14946277': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946277',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-04-13',
      startTime: '2019-04-13T19:30:00+00:00',
    },
    'sr:match:14946273': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946273',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-04-13',
      startTime: '2019-04-13T14:30:00+00:00',
    },
    'sr:match:14946285': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946285',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-04-13',
      startTime: '2019-04-13T17:00:00+00:00',
    },
  },
  '2018-08-10': {
    'sr:match:14944793': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14944793',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2018-08-10',
      startTime: '2018-08-10T19:30:00+00:00',
    },
  },
  '2018-08-26': {
    'sr:match:14945817': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14945817',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2018-08-26',
      startTime: '2018-08-26T15:00:00+00:00',
    },
    'sr:match:14945807': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14945807',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2018-08-26',
      startTime: '2018-08-26T17:30:00+00:00',
    },
    'sr:match:14945803': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14945803',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2018-08-26',
      startTime: '2018-08-26T15:00:00+00:00',
    },
    'sr:match:14945809': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14945809',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2018-08-26',
      startTime: '2018-08-26T19:30:00+00:00',
    },
  },
  '2019-02-15': {
    'sr:match:14946155': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946155',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-02-15',
      startTime: '2019-02-15T20:30:00+00:00',
    },
  },
  '2018-11-11': {
    'sr:match:14945927': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14945927',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2018-11-11',
      startTime: '2018-11-11T20:00:00+00:00',
    },
    'sr:match:14945929': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14945929',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2018-11-11',
      startTime: '2018-11-11T15:00:00+00:00',
    },
    'sr:match:14945937': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14945937',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2018-11-11',
      startTime: '2018-11-11T17:30:00+00:00',
    },
  },
  '2018-12-10': {
    'sr:match:14945975': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14945975',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2018-12-10',
      startTime: '2018-12-10T20:15:00+00:00',
    },
  },
  '2019-01-20': {
    'sr:match:14946071': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946071',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-01-20',
      startTime: '2019-01-20T17:30:00+00:00',
    },
    'sr:match:14946079': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946079',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-01-20',
      startTime: '2019-01-20T15:00:00+00:00',
    },
    'sr:match:14946083': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946083',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-01-20',
      startTime: '2019-01-20T20:00:00+00:00',
    },
  },
  '2018-10-28': {
    'sr:match:14945901': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14945901',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2018-10-28',
      startTime: '2018-10-28T20:00:00+00:00',
    },
    'sr:match:14945895': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14945895',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2018-10-28',
      startTime: '2018-10-28T15:00:00+00:00',
    },
    'sr:match:14945897': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14945897',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2018-10-28',
      startTime: '2018-10-28T15:00:00+00:00',
    },
    'sr:match:14945893': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14945893',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2018-10-28',
      startTime: '2018-10-28T17:30:00+00:00',
    },
  },
  '2019-04-14': {
    'sr:match:14946275': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946275',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-04-14',
      startTime: '2019-04-14T19:00:00+00:00',
    },
    'sr:match:14946271': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946271',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-04-14',
      startTime: '2019-04-14T16:30:00+00:00',
    },
    'sr:match:14946269': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946269',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-04-14',
      startTime: '2019-04-14T16:30:00+00:00',
    },
    'sr:match:14946283': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946283',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-04-14',
      startTime: '2019-04-14T14:00:00+00:00',
    },
    'sr:match:14946281': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946281',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-04-14',
      startTime: '2019-04-14T14:00:00+00:00',
    },
  },
  '2018-09-30': {
    'sr:match:14945857': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14945857',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2018-09-30',
      startTime: '2018-09-30T15:00:00+00:00',
    },
    'sr:match:14945867': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14945867',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2018-09-30',
      startTime: '2018-09-30T19:30:00+00:00',
    },
    'sr:match:14945871': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14945871',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2018-09-30',
      startTime: '2018-09-30T17:30:00+00:00',
    },
  },
  '2019-03-15': {
    'sr:match:14946231': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946231',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-03-15',
      startTime: '2019-03-15T20:30:00+00:00',
    },
  },
  '2018-12-07': {
    'sr:match:14945973': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14945973',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2018-12-07',
      startTime: '2018-12-07T20:30:00+00:00',
    },
  },
  '2018-08-11': {
    'sr:match:14944811': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14944811',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2018-08-11',
      startTime: '2018-08-11T15:30:00+00:00',
    },
    'sr:match:14944813': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14944813',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2018-08-11',
      startTime: '2018-08-11T18:00:00+00:00',
    },
    'sr:match:14944819': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14944819',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2018-08-11',
      startTime: '2018-08-11T20:00:00+00:00',
    },
  },
  '2019-02-16': {
    'sr:match:14946143': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946143',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-02-16',
      startTime: '2019-02-16T18:00:00+00:00',
    },
    'sr:match:14946147': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946147',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2019-02-16',
      startTime: '2019-02-16T20:30:00+00:00',
    },
    'sr:match:14946159': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946159',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-02-16',
      startTime: '2019-02-16T15:30:00+00:00',
    },
  },
  '2018-08-31': {
    'sr:match:14945835': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14945835',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2018-08-31',
      startTime: '2018-08-31T20:15:00+00:00',
    },
    'sr:match:14945819': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14945819',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2018-08-31',
      startTime: '2018-08-31T18:00:00+00:00',
    },
  },
  '2018-08-27': {
    'sr:match:14945813': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14945813',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2018-08-27',
      startTime: '2018-08-27T19:15:00+00:00',
    },
  },
  '2018-09-27': {
    'sr:match:14945863': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14945863',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2018-09-27',
      startTime: '2018-09-27T20:15:00+00:00',
    },
  },
  '2019-03-16': {
    'sr:match:14946217': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946217',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2019-03-16',
      startTime: '2019-03-16T20:30:00+00:00',
    },
    'sr:match:14946219': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946219',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-03-16',
      startTime: '2019-03-16T15:30:00+00:00',
    },
    'sr:match:14946227': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946227',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-03-16',
      startTime: '2019-03-16T18:00:00+00:00',
    },
    'sr:match:14946223': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946223',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-03-16',
      startTime: '2019-03-16T15:30:00+00:00',
    },
  },
  '2018-12-08': {
    'sr:match:14945963': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14945963',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2018-12-08',
      startTime: '2018-12-08T20:30:00+00:00',
    },
    'sr:match:14945967': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14945967',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2018-12-08',
      startTime: '2018-12-08T18:00:00+00:00',
    },
    'sr:match:14945977': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14945977',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2018-12-08',
      startTime: '2018-12-08T15:30:00+00:00',
    },
  },
  '2019-02-01': {
    'sr:match:14946111': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946111',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-02-01',
      startTime: '2019-02-01T20:30:00+00:00',
    },
  },
  '2018-08-12': {
    'sr:match:14944821': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14944821',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2018-08-12',
      startTime: '2018-08-12T15:00:00+00:00',
    },
    'sr:match:14944807': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14944807',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2018-08-12',
      startTime: '2018-08-12T15:00:00+00:00',
    },
    'sr:match:14944809': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14944809',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2018-08-12',
      startTime: '2018-08-12T17:30:00+00:00',
    },
    'sr:match:14944805': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14944805',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2018-08-12',
      startTime: '2018-08-12T19:30:00+00:00',
    },
  },
  '2018-11-09': {
    'sr:match:14945943': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14945943',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2018-11-09',
      startTime: '2018-11-09T19:00:00+00:00',
    },
    'sr:match:14945931': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14945931',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2018-11-09',
      startTime: '2018-11-09T21:15:00+00:00',
    },
  },
  '2019-01-02': {
    'sr:match:14946021': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946021',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-01-02',
      startTime: '2019-01-02T18:00:00+00:00',
    },
    'sr:match:14946023': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946023',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-01-02',
      startTime: '2019-01-02T20:15:00+00:00',
    },
    'sr:match:14946029': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946029',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-01-02',
      startTime: '2019-01-02T18:00:00+00:00',
    },
    'sr:match:14946025': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946025',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-01-02',
      startTime: '2019-01-02T16:00:00+00:00',
    },
    'sr:match:14946033': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946033',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-01-02',
      startTime: '2019-01-02T18:00:00+00:00',
    },
    'sr:match:14946019': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946019',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-01-02',
      startTime: '2019-01-02T20:15:00+00:00',
    },
  },
  '2019-01-18': {
    'sr:match:14946073': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946073',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-01-18',
      startTime: '2019-01-18T19:00:00+00:00',
    },
    'sr:match:14946081': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946081',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-01-18',
      startTime: '2019-01-18T21:15:00+00:00',
    },
    'sr:match:14946085': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946085',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-01-18',
      startTime: '2019-01-18T18:45:00+00:00',
    },
  },
  '2019-02-17': {
    'sr:match:14946149': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946149',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-02-17',
      startTime: '2019-02-17T15:00:00+00:00',
    },
    'sr:match:14946145': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946145',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-02-17',
      startTime: '2019-02-17T15:00:00+00:00',
    },
    'sr:match:14946153': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946153',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-02-17',
      startTime: '2019-02-17T17:30:00+00:00',
    },
    'sr:match:14946157': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946157',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-02-17',
      startTime: '2019-02-17T20:00:00+00:00',
    },
  },
  '2019-04-20': {
    'sr:match:14946295': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946295',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-04-20',
      startTime: '2019-04-20T14:30:00+00:00',
    },
    'sr:match:14946303': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946303',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-04-20',
      startTime: '2019-04-20T17:00:00+00:00',
    },
    'sr:match:14946297': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946297',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-04-20',
      startTime: '2019-04-20T14:30:00+00:00',
    },
    'sr:match:14946293': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946293',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2019-04-20',
      startTime: '2019-04-20T19:30:00+00:00',
    },
  },
  '2018-09-28': {
    'sr:match:14945865': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14945865',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2018-09-28',
      startTime: '2018-09-28T19:30:00+00:00',
    },
  },
  '2019-03-17': {
    'sr:match:14946229': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946229',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-03-17',
      startTime: '2019-03-17T17:30:00+00:00',
    },
    'sr:match:14946225': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946225',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2019-03-17',
      startTime: '2019-03-17T20:00:00+00:00',
    },
    'sr:match:14946221': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946221',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-03-17',
      startTime: '2019-03-17T15:00:00+00:00',
    },
    'sr:match:14946215': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946215',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-03-17',
      startTime: '2019-03-17T15:00:00+00:00',
    },
  },
  '2018-08-13': {
    'sr:match:14944815': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14944815',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2018-08-13',
      startTime: '2018-08-13T19:15:00+00:00',
    },
  },
  '2019-03-01': {
    'sr:match:14946189': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946189',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-03-01',
      startTime: '2019-03-01T20:30:00+00:00',
    },
  },
  '2019-02-22': {
    'sr:match:14946173': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946173',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-02-22',
      startTime: '2019-02-22T19:00:00+00:00',
    },
    'sr:match:14946171': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946171',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2019-02-22',
      startTime: '2019-02-22T21:15:00+00:00',
    },
  },
  '2019-02-18': {
    'sr:match:14946151': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946151',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-02-18',
      startTime: '2019-02-18T20:15:00+00:00',
    },
  },
  '2019-01-03': {
    'sr:match:14946017': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946017',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-01-03',
      startTime: '2019-01-03T18:00:00+00:00',
    },
    'sr:match:14946027': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946027',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-01-03',
      startTime: '2019-01-03T16:00:00+00:00',
    },
    'sr:match:14946031': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946031',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-01-03',
      startTime: '2019-01-03T20:15:00+00:00',
    },
  },
  '2019-01-19': {
    'sr:match:14946077': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946077',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-01-19',
      startTime: '2019-01-19T15:30:00+00:00',
    },
    'sr:match:14946075': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946075',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-01-19',
      startTime: '2019-01-19T20:30:00+00:00',
    },
    'sr:match:14946087': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946087',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-01-19',
      startTime: '2019-01-19T18:00:00+00:00',
    },
  },
  '2018-12-09': {
    'sr:match:14945971': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14945971',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2018-12-09',
      startTime: '2018-12-09T20:00:00+00:00',
    },
    'sr:match:14945969': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14945969',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2018-12-09',
      startTime: '2018-12-09T15:00:00+00:00',
    },
    'sr:match:14945965': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14945965',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2018-12-09',
      startTime: '2018-12-09T15:00:00+00:00',
    },
    'sr:match:14945979': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14945979',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2018-12-09',
      startTime: '2018-12-09T17:30:00+00:00',
    },
  },
  '2019-02-02': {
    'sr:match:14946115': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946115',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-02-02',
      startTime: '2019-02-02T15:30:00+00:00',
    },
    'sr:match:14946121': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946121',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-02-02',
      startTime: '2019-02-02T20:30:00+00:00',
    },
    'sr:match:14946123': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946123',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-02-02',
      startTime: '2019-02-02T18:00:00+00:00',
    },
  },
  '2019-05-16': {
    'sr:match:14946365': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946365',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-05-16',
      startTime: '2019-05-16T17:30:00+00:00',
    },
  },
  '2019-04-01': {
    'sr:match:14946235': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946235',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-04-01',
      startTime: '2019-04-01T19:15:00+00:00',
    },
  },
  '2018-09-29': {
    'sr:match:14945855': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14945855',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2018-09-29',
      startTime: '2018-09-29T18:00:00+00:00',
    },
    'sr:match:14945859': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14945859',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2018-09-29',
      startTime: '2018-09-29T15:30:00+00:00',
    },
    'sr:match:14945861': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14945861',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2018-09-29',
      startTime: '2018-09-29T20:00:00+00:00',
    },
  },
  '2019-03-02': {
    'sr:match:14946183': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946183',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-03-02',
      startTime: '2019-03-02T18:00:00+00:00',
    },
    'sr:match:14946187': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946187',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-03-02',
      startTime: '2019-03-02T15:30:00+00:00',
    },
    'sr:match:14946193': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946193',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2019-03-02',
      startTime: '2019-03-02T20:30:00+00:00',
    },
  },
  '2018-12-14': {
    'sr:match:14945997': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14945997',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2018-12-14',
      startTime: '2018-12-14T21:15:00+00:00',
    },
    'sr:match:14945981': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14945981',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2018-12-14',
      startTime: '2018-12-14T19:00:00+00:00',
    },
  },
  '2019-02-03': {
    'sr:match:14946109': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946109',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-02-03',
      startTime: '2019-02-03T15:00:00+00:00',
    },
    'sr:match:14946117': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946117',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-02-03',
      startTime: '2019-02-03T17:30:00+00:00',
    },
    'sr:match:14946113': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946113',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-02-03',
      startTime: '2019-02-03T15:00:00+00:00',
    },
    'sr:match:14946107': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946107',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-02-03',
      startTime: '2019-02-03T20:00:00+00:00',
    },
  },
  '2019-02-23': {
    'sr:match:14946165': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946165',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-02-23',
      startTime: '2019-02-23T15:30:00+00:00',
    },
    'sr:match:14946175': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946175',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-02-23',
      startTime: '2019-02-23T18:00:00+00:00',
    },
    'sr:match:14946177': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946177',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-02-23',
      startTime: '2019-02-23T20:30:00+00:00',
    },
  },
  '2019-05-17': {
    'sr:match:14946361': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946361',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-05-17',
      startTime: '2019-05-17T19:30:00+00:00',
    },
  },
  '2019-04-22': {
    'sr:match:14946289': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946289',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-04-22',
      startTime: '2019-04-22T19:15:00+00:00',
    },
  },
  '2019-03-03': {
    'sr:match:14946179': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946179',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-03-03',
      startTime: '2019-03-03T15:00:00+00:00',
    },
    'sr:match:14946195': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946195',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-03-03',
      startTime: '2019-03-03T20:00:00+00:00',
    },
    'sr:match:14946191': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946191',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-03-03',
      startTime: '2019-03-03T17:30:00+00:00',
    },
    'sr:match:14946185': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946185',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-03-03',
      startTime: '2019-03-03T15:00:00+00:00',
    },
  },
  '2018-12-15': {
    'sr:match:14945995': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14945995',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2018-12-15',
      startTime: '2018-12-15T18:00:00+00:00',
    },
    'sr:match:14945991': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14945991',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2018-12-15',
      startTime: '2018-12-15T15:30:00+00:00',
    },
    'sr:match:14945989': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14945989',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2018-12-15',
      startTime: '2018-12-15T20:30:00+00:00',
    },
    'sr:match:14945993': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14945993',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2018-12-15',
      startTime: '2018-12-15T15:30:00+00:00',
    },
  },
  '2019-02-04': {
    'sr:match:14946119': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946119',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-02-04',
      startTime: '2019-02-04T20:15:00+00:00',
    },
  },
  '2019-02-24': {
    'sr:match:14946163': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946163',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-02-24',
      startTime: '2019-02-24T15:30:00+00:00',
    },
    'sr:match:14946161': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946161',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-02-24',
      startTime: '2019-02-24T17:30:00+00:00',
    },
  },
  '2019-01-05': {
    'sr:match:14946051': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946051',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-01-05',
      startTime: '2019-01-05T20:30:00+00:00',
    },
  },
  '2018-10-01': {
    'sr:match:14945869': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14945869',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2018-10-01',
      startTime: '2018-10-01T19:15:00+00:00',
    },
  },
  '2019-05-18': {
    'sr:match:14946363': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946363',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2019-05-18',
      startTime: '2019-05-18T17:30:00+00:00',
    },
    'sr:match:14946371': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946371',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-05-18',
      startTime: '2019-05-18T14:30:00+00:00',
    },
    'sr:match:14946369': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946369',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-05-18',
      startTime: '2019-05-18T14:30:00+00:00',
    },
    'sr:match:14946375': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946375',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-05-18',
      startTime: '2019-05-18T14:30:00+00:00',
    },
    'sr:match:14946359': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946359',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-05-18',
      startTime: '2019-05-18T17:30:00+00:00',
    },
  },
  '2019-04-19': {
    'sr:match:14946287': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946287',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-04-19',
      startTime: '2019-04-19T14:30:00+00:00',
    },
    'sr:match:14946299': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946299',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2019-04-19',
      startTime: '2019-04-19T14:30:00+00:00',
    },
    'sr:match:14946291': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946291',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-04-19',
      startTime: '2019-04-19T17:00:00+00:00',
    },
    'sr:match:14946301': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946301',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-04-19',
      startTime: '2019-04-19T19:30:00+00:00',
    },
  },
  '2019-03-04': {
    'sr:match:14946181': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946181',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-03-04',
      startTime: '2019-03-04T20:15:00+00:00',
    },
  },
  '2018-08-20': {
    'sr:match:14945789': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14945789',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2018-08-20',
      startTime: '2018-08-20T19:15:00+00:00',
    },
  },
  '2018-12-16': {
    'sr:match:14945985': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14945985',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2018-12-16',
      startTime: '2018-12-16T17:30:00+00:00',
    },
    'sr:match:14945983': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14945983',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2018-12-16',
      startTime: '2018-12-16T15:00:00+00:00',
    },
    'sr:match:14945987': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14945987',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2018-12-16',
      startTime: '2018-12-16T20:00:00+00:00',
    },
  },
  '2019-02-25': {
    'sr:match:14946169': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946169',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-02-25',
      startTime: '2019-02-25T19:00:00+00:00',
    },
    'sr:match:14946167': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946167',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-02-25',
      startTime: '2019-02-25T21:15:00+00:00',
    },
  },
  '2019-01-10': {
    'sr:match:14946057': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946057',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-01-10',
      startTime: '2019-01-10T20:15:00+00:00',
    },
  },
  '2019-01-06': {
    'sr:match:14946047': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946047',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-01-06',
      startTime: '2019-01-06T17:30:00+00:00',
    },
    'sr:match:14946043': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946043',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-01-06',
      startTime: '2019-01-06T20:00:00+00:00',
    },
    'sr:match:14946035': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946035',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-01-06',
      startTime: '2019-01-06T15:00:00+00:00',
    },
    'sr:match:14946037': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946037',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-01-06',
      startTime: '2019-01-06T15:00:00+00:00',
    },
    'sr:match:14946049': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946049',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-01-06',
      startTime: '2019-01-06T15:00:00+00:00',
    },
  },
  '2019-01-30': {
    'sr:match:14946103': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946103',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2019-01-30',
      startTime: '2019-01-30T21:15:00+00:00',
    },
    'sr:match:14946093': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946093',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-01-30',
      startTime: '2019-01-30T19:00:00+00:00',
    },
  },
  '2019-05-03': {
    'sr:match:14946327': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946327',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-05-03',
      startTime: '2019-05-03T19:30:00+00:00',
    },
  },
  '2019-05-19': {
    'sr:match:14946373': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946373',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-05-19',
      startTime: '2019-05-19T19:00:00+00:00',
    },
    'sr:match:14946367': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946367',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2019-05-19',
      startTime: '2019-05-19T16:30:00+00:00',
    },
  },
  '2018-12-01': {
    'sr:match:14945945': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14945945',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2018-12-01',
      startTime: '2018-12-01T15:30:00+00:00',
    },
    'sr:match:14945955': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14945955',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2018-12-01',
      startTime: '2018-12-01T20:30:00+00:00',
    },
    'sr:match:14945957': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14945957',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2018-12-01',
      startTime: '2018-12-01T18:00:00+00:00',
    },
  },
  '2018-08-17': {
    'sr:match:14945793': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14945793',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2018-08-17',
      startTime: '2018-08-17T19:30:00+00:00',
    },
  },
  '2019-02-10': {
    'sr:match:14946135': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946135',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-02-10',
      startTime: '2019-02-10T17:30:00+00:00',
    },
    'sr:match:14946131': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946131',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-02-10',
      startTime: '2019-02-10T15:00:00+00:00',
    },
    'sr:match:14946125': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946125',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-02-10',
      startTime: '2019-02-10T15:00:00+00:00',
    },
    'sr:match:14946127': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946127',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-02-10',
      startTime: '2019-02-10T20:00:00+00:00',
    },
  },
  '2018-11-02': {
    'sr:match:14945923': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14945923',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2018-11-02',
      startTime: '2018-11-02T20:30:00+00:00',
    },
  },
  '2019-01-11': {
    'sr:match:14946055': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946055',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-01-11',
      startTime: '2019-01-11T19:00:00+00:00',
    },
    'sr:match:14946063': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946063',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-01-11',
      startTime: '2019-01-11T21:15:00+00:00',
    },
    'sr:match:14946067': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946067',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-01-11',
      startTime: '2019-01-11T19:00:00+00:00',
    },
  },
  '2019-01-07': {
    'sr:match:14946039': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946039',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2019-01-07',
      startTime: '2019-01-07T21:15:00+00:00',
    },
    'sr:match:14946041': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946041',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2019-01-07',
      startTime: '2019-01-07T19:00:00+00:00',
    },
    'sr:match:14946045': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946045',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-01-07',
      startTime: '2019-01-07T17:00:00+00:00',
    },
  },
  '2019-05-04': {
    'sr:match:14946337': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946337',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2019-05-04',
      startTime: '2019-05-04T19:30:00+00:00',
    },
    'sr:match:14946333': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946333',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-05-04',
      startTime: '2019-05-04T14:30:00+00:00',
    },
    'sr:match:14946339': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946339',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-05-04',
      startTime: '2019-05-04T14:30:00+00:00',
    },
    'sr:match:14946331': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946331',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-05-04',
      startTime: '2019-05-04T17:00:00+00:00',
    },
  },
  '2019-04-05': {
    'sr:match:14946263': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946263',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2019-04-05',
      startTime: '2019-04-05T19:30:00+00:00',
    },
  },
  '2018-09-01': {
    'sr:match:14945821': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14945821',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2018-09-01',
      startTime: '2018-09-01T20:00:00+00:00',
    },
    'sr:match:14945827': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14945827',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2018-09-01',
      startTime: '2018-09-01T18:00:00+00:00',
    },
    'sr:match:14945831': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14945831',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2018-09-01',
      startTime: '2018-09-01T15:30:00+00:00',
    },
  },
  '2018-09-21': {
    'sr:match:14945851': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14945851',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2018-09-21',
      startTime: '2018-09-21T19:30:00+00:00',
    },
  },
  '2019-03-10': {
    'sr:match:14946213': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946213',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-03-10',
      startTime: '2019-03-10T15:00:00+00:00',
    },
    'sr:match:14946199': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946199',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-03-10',
      startTime: '2019-03-10T20:00:00+00:00',
    },
    'sr:match:14946203': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946203',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-03-10',
      startTime: '2019-03-10T17:30:00+00:00',
    },
    'sr:match:14946211': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946211',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-03-10',
      startTime: '2019-03-10T15:00:00+00:00',
    },
  },
  '2018-12-02': {
    'sr:match:14945953': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14945953',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2018-12-02',
      startTime: '2018-12-02T17:30:00+00:00',
    },
    'sr:match:14945961': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14945961',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2018-12-02',
      startTime: '2018-12-02T20:00:00+00:00',
    },
    'sr:match:14945947': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14945947',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2018-12-02',
      startTime: '2018-12-02T15:00:00+00:00',
    },
    'sr:match:14945959': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14945959',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2018-12-02',
      startTime: '2018-12-02T15:00:00+00:00',
    },
  },
  '2018-08-18': {
    'sr:match:14945787': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14945787',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2018-08-18',
      startTime: '2018-08-18T20:00:00+00:00',
    },
    'sr:match:14945797': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14945797',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2018-08-18',
      startTime: '2018-08-18T15:30:00+00:00',
    },
    'sr:match:14945799': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14945799',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2018-08-18',
      startTime: '2018-08-18T18:00:00+00:00',
    },
  },
  '2018-12-22': {
    'sr:match:14946003': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946003',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2018-12-22',
      startTime: '2018-12-22T15:30:00+00:00',
    },
    'sr:match:14946011': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946011',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2018-12-22',
      startTime: '2018-12-22T15:30:00+00:00',
    },
    'sr:match:14945999': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14945999',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2018-12-22',
      startTime: '2018-12-22T20:30:00+00:00',
    },
    'sr:match:14946005': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946005',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2018-12-22',
      startTime: '2018-12-22T15:30:00+00:00',
    },
    'sr:match:14946007': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946007',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2018-12-22',
      startTime: '2018-12-22T18:00:00+00:00',
    },
  },
  '2018-11-03': {
    'sr:match:14945911': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14945911',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2018-11-03',
      startTime: '2018-11-03T15:30:00+00:00',
    },
    'sr:match:14945915': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14945915',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2018-11-03',
      startTime: '2018-11-03T18:00:00+00:00',
    },
    'sr:match:14945917': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14945917',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2018-11-03',
      startTime: '2018-11-03T20:30:00+00:00',
    },
  },
  '2019-02-11': {
    'sr:match:14946137': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946137',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-02-11',
      startTime: '2019-02-11T20:15:00+00:00',
    },
  },
  '2019-01-12': {
    'sr:match:14946065': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946065',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-01-12',
      startTime: '2019-01-12T20:30:00+00:00',
    },
    'sr:match:14946061': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946061',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-01-12',
      startTime: '2019-01-12T15:30:00+00:00',
    },
  },
  '2019-01-28': {
    'sr:match:14946099': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946099',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2019-01-28',
      startTime: '2019-01-28T19:00:00+00:00',
    },
    'sr:match:14946095': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946095',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-01-28',
      startTime: '2019-01-28T21:15:00+00:00',
    },
    'sr:match:14946091': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946091',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-01-28',
      startTime: '2019-01-28T19:00:00+00:00',
    },
    'sr:match:14946089': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946089',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-01-28',
      startTime: '2019-01-28T17:00:00+00:00',
    },
  },
  '2019-03-30': {
    'sr:match:14946249': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946249',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-03-30',
      startTime: '2019-03-30T20:30:00+00:00',
    },
    'sr:match:14946233': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946233',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-03-30',
      startTime: '2019-03-30T20:30:00+00:00',
    },
    'sr:match:14946241': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946241',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-03-30',
      startTime: '2019-03-30T15:30:00+00:00',
    },
    'sr:match:14946239': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946239',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-03-30',
      startTime: '2019-03-30T18:00:00+00:00',
    },
  },
  '2019-05-05': {
    'sr:match:14946323': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946323',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-05-05',
      startTime: '2019-05-05T16:30:00+00:00',
    },
    'sr:match:14946325': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946325',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-05-05',
      startTime: '2019-05-05T19:00:00+00:00',
    },
    'sr:match:14946329': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946329',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2019-05-05',
      startTime: '2019-05-05T14:00:00+00:00',
    },
  },
  '2019-04-06': {
    'sr:match:14946267': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946267',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-04-06',
      startTime: '2019-04-06T19:30:00+00:00',
    },
    'sr:match:14946255': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14946255',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2019-04-06',
      startTime: '2019-04-06T14:30:00+00:00',
    },
    'sr:match:14946261': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946261',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-04-06',
      startTime: '2019-04-06T14:30:00+00:00',
    },
    'sr:match:14946259': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946259',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2019-04-06',
      startTime: '2019-04-06T17:00:00+00:00',
    },
  },
  '2018-09-02': {
    'sr:match:14945829': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14945829',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2018-09-02',
      startTime: '2018-09-02T17:30:00+00:00',
    },
    'sr:match:14945825': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14945825',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2018-09-02',
      startTime: '2018-09-02T15:00:00+00:00',
    },
    'sr:match:14945833': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14945833',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2018-09-02',
      startTime: '2018-09-02T15:00:00+00:00',
    },
    'sr:match:14945823': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14945823',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2018-09-02',
      startTime: '2018-09-02T19:30:00+00:00',
    },
  },
  '2018-09-22': {
    'sr:match:14945841': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14945841',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2018-09-22',
      startTime: '2018-09-22T20:00:00+00:00',
    },
    'sr:match:14945843': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14945843',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2018-09-22',
      startTime: '2018-09-22T18:00:00+00:00',
    },
    'sr:match:14945849': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14945849',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2018-09-22',
      startTime: '2018-09-22T15:30:00+00:00',
    },
  },
  '2019-03-11': {
    'sr:match:14946209': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946209',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-03-11',
      startTime: '2019-03-11T20:15:00+00:00',
    },
  },
  '2019-03-31': {
    'sr:match:14946237': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946237',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-03-31',
      startTime: '2019-03-31T14:00:00+00:00',
    },
    'sr:match:14946243': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946243',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-03-31',
      startTime: '2019-03-31T16:30:00+00:00',
    },
    'sr:match:14946247': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946247',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-03-31',
      startTime: '2019-03-31T19:00:00+00:00',
    },
  },
  '2018-12-23': {
    'sr:match:14946015': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14946015',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2018-12-23',
      startTime: '2018-12-23T20:00:00+00:00',
    },
    'sr:match:14946009': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14946009',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2018-12-23',
      startTime: '2018-12-23T15:00:00+00:00',
    },
    'sr:match:14946013': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946013',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2018-12-23',
      startTime: '2018-12-23T17:30:00+00:00',
    },
    'sr:match:14946001': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946001',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2018-12-23',
      startTime: '2018-12-23T15:00:00+00:00',
    },
  },
  '2019-02-08': {
    'sr:match:14946129': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5312',
        name: 'Parque de Jogos Comendador Joaquim de Almeida Freitas',
      },
      id: 'sr:match:14946129',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
      },
      date: '2019-02-08',
      startTime: '2019-02-08T20:30:00+00:00',
    },
  },
  '2018-08-19': {
    'sr:match:14945795': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14945795',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2018-08-19',
      startTime: '2018-08-19T19:30:00+00:00',
    },
    'sr:match:14945791': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14945791',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2018-08-19',
      startTime: '2018-08-19T17:30:00+00:00',
    },
    'sr:match:14945783': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14945783',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2018-08-19',
      startTime: '2018-08-19T15:00:00+00:00',
    },
    'sr:match:14945785': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14945785',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2018-08-19',
      startTime: '2018-08-19T15:00:00+00:00',
    },
  },
  '2018-12-03': {
    'sr:match:14945949': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14945949',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2018-12-03',
      startTime: '2018-12-03T20:15:00+00:00',
    },
  },
  '2019-01-13': {
    'sr:match:14946053': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14946053',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2019-01-13',
      startTime: '2019-01-13T15:00:00+00:00',
    },
    'sr:match:14946059': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946059',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-01-13',
      startTime: '2019-01-13T20:00:00+00:00',
    },
    'sr:match:14946069': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946069',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-01-13',
      startTime: '2019-01-13T17:30:00+00:00',
    },
  },
  '2018-10-05': {
    'sr:match:14945873': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14945873',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2018-10-05',
      startTime: '2018-10-05T17:00:00+00:00',
    },
    'sr:match:14945879': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14945879',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2018-10-05',
      startTime: '2018-10-05T14:30:00+00:00',
    },
    'sr:match:14945881': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14945881',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2018-10-05',
      startTime: '2018-10-05T19:30:00+00:00',
    },
  },
  '2019-04-26': {
    'sr:match:14946321': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14946321',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2019-04-26',
      startTime: '2019-04-26T19:30:00+00:00',
    },
  },
  '2019-01-29': {
    'sr:match:14946097': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946097',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-01-29',
      startTime: '2019-01-29T21:15:00+00:00',
    },
    'sr:match:14946101': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946101',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-01-29',
      startTime: '2019-01-29T19:00:00+00:00',
    },
    'sr:match:14946105': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14946105',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2019-01-29',
      startTime: '2019-01-29T19:00:00+00:00',
    },
  },
  '2018-11-04': {
    'sr:match:14945919': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14945919',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2018-11-04',
      startTime: '2018-11-04T15:00:00+00:00',
    },
    'sr:match:14945921': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14945921',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2018-11-04',
      startTime: '2018-11-04T20:00:00+00:00',
    },
    'sr:match:14945925': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14945925',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2018-11-04',
      startTime: '2018-11-04T17:30:00+00:00',
    },
  },
  '2019-05-10': {
    'sr:match:14946351': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946351',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-05-10',
      startTime: '2019-05-10T19:30:00+00:00',
    },
  },
  '2019-05-06': {
    'sr:match:14946335': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946335',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-05-06',
      startTime: '2019-05-06T19:15:00+00:00',
    },
  },
  '2019-04-07': {
    'sr:match:14946253': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946253',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-04-07',
      startTime: '2019-04-07T19:00:00+00:00',
    },
    'sr:match:14946257': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14946257',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2019-04-07',
      startTime: '2019-04-07T14:00:00+00:00',
    },
    'sr:match:14946265': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14946265',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2019-04-07',
      startTime: '2019-04-07T16:30:00+00:00',
    },
  },
  '2019-04-27': {
    'sr:match:14946317': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946317',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-04-27',
      startTime: '2019-04-27T17:00:00+00:00',
    },
    'sr:match:14946313': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14946313',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2019-04-27',
      startTime: '2019-04-27T19:30:00+00:00',
    },
    'sr:match:14946307': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946307',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-04-27',
      startTime: '2019-04-27T14:30:00+00:00',
    },
    'sr:match:14946319': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946319',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-04-27',
      startTime: '2019-04-27T14:30:00+00:00',
    },
  },
  '2018-09-23': {
    'sr:match:14945837': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14945837',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2018-09-23',
      startTime: '2018-09-23T19:30:00+00:00',
    },
    'sr:match:14945853': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2555',
        name: 'Estadio Marcolino de Castro',
      },
      id: 'sr:match:14945853',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
      },
      date: '2018-09-23',
      startTime: '2018-09-23T15:00:00+00:00',
    },
    'sr:match:14945839': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14945839',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2018-09-23',
      startTime: '2018-09-23T15:00:00+00:00',
    },
    'sr:match:14945847': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14945847',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2018-09-23',
      startTime: '2018-09-23T17:30:00+00:00',
    },
  },
  '2019-03-08': {
    'sr:match:14946197': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946197',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-03-08',
      startTime: '2019-03-08T20:30:00+00:00',
    },
  },
  '2018-08-24': {
    'sr:match:14945811': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14945811',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2018-08-24',
      startTime: '2018-08-24T19:30:00+00:00',
    },
  },
  '2019-02-09': {
    'sr:match:14946139': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946139',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-02-09',
      startTime: '2019-02-09T15:30:00+00:00',
    },
    'sr:match:14946133': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946133',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-02-09',
      startTime: '2019-02-09T18:00:00+00:00',
    },
    'sr:match:14946141': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946141',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2019-02-09',
      startTime: '2019-02-09T20:30:00+00:00',
    },
  },
  '2018-11-05': {
    'sr:match:14945913': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14945913',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2018-11-05',
      startTime: '2018-11-05T19:00:00+00:00',
    },
    'sr:match:14945909': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14945909',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2018-11-05',
      startTime: '2018-11-05T21:15:00+00:00',
    },
  },
  '2018-10-06': {
    'sr:match:14945875': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14945875',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2018-10-06',
      startTime: '2018-10-06T17:00:00+00:00',
    },
    'sr:match:14945877': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2091',
        name: 'Estadio Do Bonfim',
      },
      id: 'sr:match:14945877',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3008',
          name: 'Vitoria Setubal',
        },
      },
      date: '2018-10-06',
      startTime: '2018-10-06T14:30:00+00:00',
    },
    'sr:match:14945885': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14945885',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2018-10-06',
      startTime: '2018-10-06T19:30:00+00:00',
    },
  },
  '2018-10-26': {
    'sr:match:14945907': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14945907',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2018-10-26',
      startTime: '2018-10-26T18:00:00+00:00',
    },
    'sr:match:14945899': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1137',
        name: 'Dom Afonso Henriques',
      },
      id: 'sr:match:14945899',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
      },
      date: '2018-10-26',
      startTime: '2018-10-26T20:15:00+00:00',
    },
  },
  '2019-05-11': {
    'sr:match:14946349': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14946349',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:4501',
          name: 'CD Feirense',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2019-05-11',
      startTime: '2019-05-11T14:30:00+00:00',
    },
    'sr:match:14946345': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:910',
        name: 'Jose Alvalade',
      },
      id: 'sr:match:14946345',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
      },
      date: '2019-05-11',
      startTime: '2019-05-11T19:30:00+00:00',
    },
    'sr:match:14946357': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946357',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-05-11',
      startTime: '2019-05-11T14:30:00+00:00',
    },
    'sr:match:14946347': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946347',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-05-11',
      startTime: '2019-05-11T17:00:00+00:00',
    },
  },
  '2019-04-12': {
    'sr:match:14946279': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946279',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-04-12',
      startTime: '2019-04-12T19:30:00+00:00',
    },
  },
  '2019-04-08': {
    'sr:match:14946251': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8243',
        name: 'Estadio Joao Cardoso',
      },
      id: 'sr:match:14946251',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
      },
      date: '2019-04-08',
      startTime: '2019-04-08T19:15:00+00:00',
    },
  },
  '2019-04-28': {
    'sr:match:14946305': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:12071',
        name: 'Estadio Municipal Eng. Manuel Branco Teixeira',
      },
      id: 'sr:match:14946305',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
      },
      date: '2019-04-28',
      startTime: '2019-04-28T14:00:00+00:00',
    },
    'sr:match:14946315': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946315',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-04-28',
      startTime: '2019-04-28T16:30:00+00:00',
    },
    'sr:match:14946311': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946311',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-04-28',
      startTime: '2019-04-28T19:00:00+00:00',
    },
    'sr:match:14946309': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946309',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:38396',
          name: 'CD Tondela',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-04-28',
      startTime: '2019-04-28T14:00:00+00:00',
    },
  },
  '2019-03-09': {
    'sr:match:14946201': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1178',
        name: 'Estadio do Maritimo',
      },
      id: 'sr:match:14946201',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
      },
      date: '2019-03-09',
      startTime: '2019-03-09T15:30:00+00:00',
    },
    'sr:match:14946205': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14946205',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2019-03-09',
      startTime: '2019-03-09T18:00:00+00:00',
    },
    'sr:match:14946207': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14946207',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2019-03-09',
      startTime: '2019-03-09T20:30:00+00:00',
    },
  },
  '2018-09-24': {
    'sr:match:14945845': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:951',
        name: 'Braga Municipal',
      },
      id: 'sr:match:14945845',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
      },
      date: '2018-09-24',
      startTime: '2018-09-24T19:15:00+00:00',
    },
  },
  '2018-08-25': {
    'sr:match:14945801': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14945801',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3009',
          name: 'Vitoria Guimaraes',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2018-08-25',
      startTime: '2018-08-25T20:00:00+00:00',
    },
    'sr:match:14945805': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14945805',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2018-08-25',
      startTime: '2018-08-25T15:30:00+00:00',
    },
    'sr:match:14945815': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14945815',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2018-08-25',
      startTime: '2018-08-25T18:00:00+00:00',
    },
  },
  '2019-03-29': {
    'sr:match:14946245': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14946245',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3014',
          name: 'Moreirense FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2019-03-29',
      startTime: '2019-03-29T20:30:00+00:00',
    },
  },
  '2018-11-10': {
    'sr:match:14945933': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:1335',
        name: 'Estadio da Madeira',
      },
      id: 'sr:match:14945933',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3007',
          name: 'CS Maritimo Madeira',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3013',
          name: 'CD Nacional',
        },
      },
      date: '2018-11-10',
      startTime: '2018-11-10T18:00:00+00:00',
    },
    'sr:match:14945941': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14945941',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2018-11-10',
      startTime: '2018-11-10T15:30:00+00:00',
    },
    'sr:match:14945939': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14945939',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2018-11-10',
      startTime: '2018-11-10T15:30:00+00:00',
    },
    'sr:match:14945935': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:907',
        name: 'Estadio do Dragao',
      },
      id: 'sr:match:14945935',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:2999',
          name: 'Sporting Braga',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
      },
      date: '2018-11-10',
      startTime: '2018-11-10T20:30:00+00:00',
    },
  },
  '2018-11-30': {
    'sr:match:14945951': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:5318',
        name: 'Estadio de Sao Miguel',
      },
      id: 'sr:match:14945951',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
      },
      date: '2018-11-30',
      startTime: '2018-11-30T20:30:00+00:00',
    },
  },
  '2018-10-07': {
    'sr:match:14945883': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:14556',
        name: 'Estadio Do Bessa Xxi',
      },
      id: 'sr:match:14945883',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:2995',
          name: 'Boavista FC',
        },
      },
      date: '2018-10-07',
      startTime: '2018-10-07T14:00:00+00:00',
    },
    'sr:match:14945887': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:902',
        name: 'Estadio do Sport Lisboa e Benfica',
      },
      id: 'sr:match:14945887',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3002',
          name: 'FC Porto',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
      },
      date: '2018-10-07',
      startTime: '2018-10-07T16:30:00+00:00',
    },
    'sr:match:14945889': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2090',
        name: 'Estadio Municipal de Portimao',
      },
      id: 'sr:match:14945889',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3001',
          name: 'Sporting CP',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3026',
          name: 'Portimonense SC',
        },
      },
      date: '2018-10-07',
      startTime: '2018-10-07T19:00:00+00:00',
    },
  },
  '2018-10-27': {
    'sr:match:14945891': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2013',
        name: 'Estadio Dos Arcos',
      },
      id: 'sr:match:14945891',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3025',
          name: 'GD Chaves',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3036',
          name: 'Rio Ave FC',
        },
      },
      date: '2018-10-27',
      startTime: '2018-10-27T17:00:00+00:00',
    },
    'sr:match:14945903': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:2375',
        name: 'Estadio Nacional Do Jamor',
      },
      id: 'sr:match:14945903',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3006',
          name: 'Benfica Lisbon',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3004',
          name: 'CF Os Belenenses',
        },
      },
      date: '2018-10-27',
      startTime: '2018-10-27T19:30:00+00:00',
    },
    'sr:match:14945905': {
      season: {
        id: 'sr:season:55243',
        name: 'Primeira Liga 18/19',
      },
      venue: {
        id: 'sr:venue:8249',
        name: 'Estadio Do CD Das Aves',
      },
      id: 'sr:match:14945905',
      tournament: {
        id: 'sr:tournament:238',
        name: 'Primeira Liga',
      },
      teams: {
        home: {
          country: 'PT',
          id: 'sr:competitor:3011',
          name: 'CD Santa Clara',
        },
        away: {
          country: 'PT',
          id: 'sr:competitor:3032',
          name: 'CD das Aves',
        },
      },
      date: '2018-10-27',
      startTime: '2018-10-27T14:30:00+00:00',
    },
  },
}

storiesOf('LeaguesScreen', module)
  // .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <LeaguesScreen
      today={new Date('2019-03-01')}
      onSelectMatch={action('onSelectMatch')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      setTournament={action('setTournament')}
      seasonMatches={TEST_SEASON_CALENDAR}
      playerStandings={TEST_PLAYER_STANDINGS}
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      onBack={action('onBack')}
      isGenerating={false}
      isLoading={false}
    />
  ))
  .add('no data', () => (
    <LeaguesScreen
      today={new Date('2019-03-01')}
      setTournament={action('setTournament')}
      onSelectMatch={action('onSelectMatch')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      teamPopularity={TEST_TEAM_POPULARITY}
      onBack={action('onBack')}
      isGenerating={false}
      isLoading={false}
    />
  ))

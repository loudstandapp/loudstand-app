/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { TeamRoot } from '@loudstand/shared'
import { StatsScreen } from './StatsScreen'

const teamRoot: TeamRoot = {
  country: 'DK',
  defaultCurrentSeasonId: 'sr:season:54369',
  defaultTournamentId: 'sr:tournament:39',
  id: 'sr:competitor:1291',
  languageCode: 'da',
  manager: {
    id: 'sr:player:8458',
    name: 'Nielsen, David',
  },
  name: 'AGF',
}

storiesOf('StatsScreen', module)
  // .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <StatsScreen
      currentUser={null} // FIXME
      onSelectCoach={action('onSelectCoach')}
      onSelectUserProfile={action('onSelectUserProfile')}
      onSelectTeam={action('onSelectTeam')}
      onSelectLeagues={action('onSelectLeagues')}
      teamRoot={teamRoot}
    />
  ))
  .add('no data', () => (
    <StatsScreen
      currentUser={null} // FIXME
      onSelectCoach={action('onSelectCoach')}
      onSelectUserProfile={action('onSelectUserProfile')}
      onSelectTeam={action('onSelectTeam')}
      onSelectLeagues={action('onSelectLeagues')}
    />
  ))

/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import {
  TeamRoot,
  PlayerList,
  PlayerPosition,
  LineupSuggestionResultDocument,
  SeasonTeamPopularityDocument,
} from '@loudstand/shared'
import { MostValuablePlayerResultDocument } from '@loudstand/shared'
import { TeamScreen } from './TeamScreen'
import { matchCalendar } from '../../main/screens/HomeScreen.stories'

const TEST_TEAM_ID = 'sr:competitor:3006'

const teamRoot: TeamRoot = {
  country: 'ES',
  defaultCurrentSeasonId: 'sr:season:54369',
  defaultTournamentId: 'sr:tournament:39',
  id: TEST_TEAM_ID,
  languageCode: 'da',
  manager: {
    id: 'sr:player:8458',
    name: 'Nielsen, David',
  },
  name: 'AGF',
  jerseyColors: {
    base: 'pink',
    number: 'purple',
    sleeve: 'white',
  },
}

export const TEST_TEAM_PLAYERS: PlayerList = {
  'sr:player:253515': {
    id: 'sr:player:253515',
    country: 'DE',
    number: 40,
    name: 'Eric Oelschlagel',
    position: PlayerPosition.Goalkeeper,
  },
  'sr:player:26760': {
    id: 'sr:player:26760',
    country: 'DE',
    number: 29,
    name: 'Marcel Schmelzer',
    position: PlayerPosition.Defender,
  },
  'sr:player:383560': {
    id: 'sr:player:383560',
    country: 'CH',
    number: 16,
    name: 'Manuel Akanji',
    position: PlayerPosition.Defender,
  },
  'sr:player:907834': {
    id: 'sr:player:907834',
    country: 'FR',
    number: 2,
    name: 'Dan Axel Zagadou',
    position: PlayerPosition.Defender,
  },
  'sr:player:39015': {
    id: 'sr:player:39015',
    country: 'TR',
    number: 36,
    name: 'Omer Toprak',
    position: PlayerPosition.Defender,
  },
  'sr:player:1419559': {
    id: 'sr:player:1419559',
    country: 'AR',
    number: 18,
    name: 'Leonardo Balerdi',
    position: PlayerPosition.Defender,
  },
  'sr:player:37378': {
    id: 'sr:player:37378',
    country: 'CH',
    number: 1,
    name: 'Roman Burki',
    position: PlayerPosition.Goalkeeper,
  },
  'sr:player:237068': {
    id: 'sr:player:237068',
    country: 'DE',
    number: 33,
    name: 'Julian Weigl',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:96366': {
    id: 'sr:player:96366',
    country: 'ES',
    number: 9,
    name: 'Paco Alcacer',
    position: PlayerPosition.Forward,
  },
  'sr:player:254119': {
    id: 'sr:player:254119',
    country: 'DE',
    number: 27,
    name: 'Marius Wolf',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:171927': {
    id: 'sr:player:171927',
    country: 'DE',
    number: 20,
    name: 'Maximilian Philipp',
    position: PlayerPosition.Forward,
  },
  'sr:player:66518': {
    id: 'sr:player:66518',
    country: 'DE',
    number: 10,
    name: 'Mario Gotze',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:37813': {
    id: 'sr:player:37813',
    country: 'CH',
    number: 35,
    name: 'Marwin Hitz',
    position: PlayerPosition.Goalkeeper,
  },
  'sr:player:1047147': {
    id: 'sr:player:1047147',
    country: null,
    number: 7,
    name: 'Jadon Sancho',
    position: PlayerPosition.Forward,
  },
  'sr:player:37092': {
    id: 'sr:player:37092',
    country: 'DE',
    number: 11,
    name: 'Marco Reus',
    position: PlayerPosition.Forward,
  },
  'sr:player:246999': {
    id: 'sr:player:246999',
    country: 'PT',
    number: 13,
    name: 'Raphael Guerreiro',
    position: PlayerPosition.Defender,
  },
  'sr:player:756428': {
    id: 'sr:player:756428',
    country: 'DK',
    number: 34,
    name: 'Jacob Bruun Larsen',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:24829': {
    id: 'sr:player:24829',
    country: 'PL',
    number: 26,
    name: 'Lukasz Piszczek',
    position: PlayerPosition.Defender,
  },
  'sr:player:1051087': {
    id: 'sr:player:1051087',
    country: 'ES',
    number: 17,
    name: 'Sergio Gomez',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:66477': {
    id: 'sr:player:66477',
    country: 'DK',
    number: 6,
    name: 'Thomas Delaney',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:1047385': {
    id: 'sr:player:1047385',
    country: 'DE',
    number: 25,
    name: 'Luca Unbehaun',
    position: PlayerPosition.Goalkeeper,
  },
  'sr:player:280435': {
    id: 'sr:player:280435',
    country: 'FR',
    number: 4,
    name: 'Abdou Diallo',
    position: PlayerPosition.Defender,
  },
  'sr:player:809758': {
    id: 'sr:player:809758',
    country: 'US',
    number: 22,
    name: 'Christian Pulisic',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:903858': {
    id: 'sr:player:903858',
    country: 'MA',
    number: 5,
    name: 'Achraf Hakimi',
    position: PlayerPosition.Defender,
  },
  'sr:player:35612': {
    id: 'sr:player:35612',
    country: 'BE',
    number: 28,
    name: 'Axel Witsel',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:341589': {
    id: 'sr:player:341589',
    country: 'DE',
    number: 19,
    name: 'Mahmoud Dahoud',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:178407': {
    id: 'sr:player:178407',
    country: 'MA',
    number: 5,
    name: 'Achraf Hakimi',
    position: PlayerPosition.Defender,
  },
  'sr:player:47906': {
    id: 'sr:player:47906',
    country: 'BE',
    number: 28,
    name: 'Axel Witsel',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:557008': {
    id: 'sr:player:557008',
    country: 'DE',
    number: 19,
    name: 'Mahmoud Dahoud',
    position: PlayerPosition.Midfielder,
  },
}

const TEST_MOST_VALUABLE_PLAYERS: MostValuablePlayerResultDocument = {
  players: {
    'sr:player:178407': 0.39,
    'sr:player:341589': 0.21,
    'sr:player:903858': 0.724,
    'sr:player:383560': 0.31,
    'sr:player:26760': 0.12,
  },
  voteCount: 311,
}
const TEST_TEAM_POPULARITY: SeasonTeamPopularityDocument = {
  [TEST_TEAM_ID]: {
    value: 1.3,
    allAverage: 0,
    latestAverage: 0,
  },
}
const TEST_SUGGESTED_LINEUP: LineupSuggestionResultDocument = {
  players: {
    'sr:player:557008': 0.3,
    'sr:player:47906': 0.3,
    'sr:player:178407': 0.39,
    'sr:player:341589': 0.21,
    'sr:player:903858': 0.724,
    'sr:player:809758': 0.1352,
    'sr:player:96366': 0.321,
    'sr:player:237068': 0.5341,
    'sr:player:37378': 0.7651,
    'sr:player:1419559': 0.5431,
    'sr:player:39015': 0.7421,
    'sr:player:907834': 0.151,
    'sr:player:383560': 0.31,
    'sr:player:26760': 0.12,
    'sr:player:253515': 0.14,
    'sr:player:171927': 0.61,
    'sr:player:66518': 0.14,
    'sr:player:37813': 0.61,
    'sr:player:1047147': 0.11,
    'sr:player:37092': 0.15,
    'sr:player:246999': 0.41,
  },
  voteCount: 322,
}

storiesOf('TeamScreen', module)
  // .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <TeamScreen
      id={TEST_TEAM_ID}
      onSelectCoach={action('onSelectCoach')}
      onSelectPlayer={action('onSelectPlayer')}
      teamRoot={teamRoot}
      matchCalendar={matchCalendar}
      players={TEST_TEAM_PLAYERS}
      mostValuablePlayers={TEST_MOST_VALUABLE_PLAYERS}
      suggestedLineup={TEST_SUGGESTED_LINEUP}
      teamPopularity={TEST_TEAM_POPULARITY}
      onSelectMatch={action('onSelectMatch')}
      onBack={action('onBack')}
      today="2012-03-12"
    />
  ))
  .add('no data', () => (
    <TeamScreen
      id={TEST_TEAM_ID}
      onSelectCoach={action('onSelectCoach')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectMatch={action('onSelectMatch')}
      onBack={action('onBack')}
      today="2012-03-12"
    />
  ))

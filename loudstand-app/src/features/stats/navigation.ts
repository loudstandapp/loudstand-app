import { Navigation } from 'react-native-navigation'
import { MatchListItem, ID } from '@loudstand/shared'
import { NavigationProps as StatsNavigationProps } from './screens/StatsScreen'
import { NavigationProps as TeamNavigationProps } from './screens/TeamScreen'
import { NavigationProps as PlayerNavigationProps } from './screens/PlayerScreen'
import { NavigationProps as CoachNavigationProps } from './screens/CoachScreen'
import { NavigationProps as LeagueNavigationProps } from './screens/LeaguesScreen'
import { ScreenNames } from '../../registerScreens'
import {
  userProfileNavigationProps,
  userActivityHistoryNavigationProps,
} from '../user/navigation'
import { matchScreenNavigationProps } from '../match/navigation'
import { cleanDate, randomComponentId } from '../../utils'

export const coachScreenNavigationProps = (
  mainComponentId: string,
  coachId: ID
): CoachNavigationProps => ({
  id: coachId,
  onBack: () => Navigation.pop(mainComponentId),
})

export const playerScreenNavigationProps = (
  mainComponentId: string,
  playerId: ID
): PlayerNavigationProps => ({
  id: playerId,
  onBack: () => Navigation.pop(mainComponentId),
})

export const teamScreenNavigationProps: (
  mainComponentId: string,
  teamId: ID
) => TeamNavigationProps = (mainComponentId, teamId) => ({
  onSelectPlayer: id => {
    const componentId = randomComponentId(ScreenNames.PlayerScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.PlayerScreen,
        id: componentId,
        passProps: playerScreenNavigationProps(componentId, id),
      },
    })
  },
  onSelectCoach: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.CoachScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.CoachScreen,
        id: componentId,
        passProps: coachScreenNavigationProps(componentId, id),
      },
    })
  },
  onSelectMatch: (match: MatchListItem) => {
    const componentId = randomComponentId(ScreenNames.MatchScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.MatchScreen,
        id: componentId,
        passProps: matchScreenNavigationProps(componentId, match),
      },
    })
  },
  onSelectSeason: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.LeaguesScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.LeaguesScreen,
        id: componentId,
        // eslint-disable-next-line no-use-before-define, @typescript-eslint/no-use-before-define
        passProps: leaguesScreenNavigationProps(componentId, id),
      },
    })
  },
  onBack: () => Navigation.pop(mainComponentId),
  id: teamId,
  today: cleanDate(new Date().toISOString()),
})

export const leaguesScreenNavigationProps = (
  mainComponentId: string,
  seasonId?: ID
): LeagueNavigationProps => ({
  seasonId,
  today: new Date(),
  onBack: () => Navigation.pop(mainComponentId),
  onSelectPlayer: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.PlayerScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.PlayerScreen,
        id: componentId,
        passProps: playerScreenNavigationProps(componentId, id),
      },
    })
  },
  onSelectTeam: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.TeamScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.TeamScreen,
        id: componentId,
        passProps: teamScreenNavigationProps(componentId, id),
      },
    })
  },
  onSelectMatch: (match: MatchListItem) => {
    const componentId = randomComponentId(ScreenNames.MatchScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.MatchScreen,
        id: componentId,
        passProps: matchScreenNavigationProps(componentId, match),
      },
    })
  },
})

export const statsScreenNavigationProps = (
  mainComponentId: string
): StatsNavigationProps => ({
  onSelectActivityHistory: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.UserActivityHistoryScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.UserActivityHistoryScreen,
        id: componentId,
        passProps: userActivityHistoryNavigationProps(componentId, id),
      },
    })
  },
  onSelectCoach: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.CoachScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.CoachScreen,
        id: componentId,
        passProps: coachScreenNavigationProps(componentId, id),
      },
    })
  },
  onSelectLeagues: () => {
    const componentId = randomComponentId(ScreenNames.LeaguesScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.LeaguesScreen,
        id: componentId,
        passProps: leaguesScreenNavigationProps(componentId),
      },
    })
  },
  onSelectUserProfile: () => {
    const componentId = randomComponentId(ScreenNames.UserProfileScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.UserProfileScreen,
        id: componentId,
        passProps: userProfileNavigationProps(componentId),
      },
    })
  },
  onSelectTeam: (id: ID) => {
    const componentId = randomComponentId(ScreenNames.TeamScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.TeamScreen,
        id: componentId,
        passProps: teamScreenNavigationProps(componentId, id),
      },
    })
  },
})

import React from 'react'
import { SectionList, View, Text, StyleSheet } from 'react-native'
import {
  CalendarItemByDay,
  MatchListItem,
  LocalizedTeams,
  MatchResultList,
} from '@loudstand/shared'
import LinearGradient from 'react-native-linear-gradient'
import { cleanDate, getComparatorForKey } from '../../../utils'
import colors from '../../../theme/colors'
import { spacingUnit, marginDefault } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import MatchListItemView from '../../../components/MatchListItemView'
import GenericButton from '../../../components/GenericButton'
import { translate, currentLocale } from '../../../translation'

const dateFormatter = new Intl.DateTimeFormat(currentLocale.languageTag, {
  year: 'numeric',
  month: 'long',
  day: 'numeric',
  weekday: 'long',
})

const timeFormatter = new Intl.DateTimeFormat(currentLocale.languageTag, {
  hour: 'numeric',
  minute: 'numeric',
})

interface Props {
  matchesByDay: CalendarItemByDay<MatchListItem>
  localizedTeams?: LocalizedTeams
  results?: MatchResultList
  onSelectMatch: (match: MatchListItem) => void
  today: Date
  topInset: number
}

const DATE_HEIGHT = 24

const styles = StyleSheet.create({
  dateHeader: {
    height: DATE_HEIGHT,
    justifyContent: 'center',
    paddingHorizontal: spacingUnit,
    marginHorizontal: marginDefault,
    borderRadius: DATE_HEIGHT / 2,
  },
  dateHeaderText: {
    ...(fonts.CustomFullDate as object),
    color: colors.secondaryText,
  },
  dateHeaderWrapper: {
    alignItems: 'flex-end',
  },
  container: {
    flex: 1,
  },
  list: {
    paddingTop: spacingUnit / 2,
  },
  previousButtonContainer: {
    margin: spacingUnit,
    alignItems: 'center',
    opacity: 0.8,
  },
})

function getSectionContent(
  matchesByDay: CalendarItemByDay<MatchListItem>,
  offset: number
) {
  return Object.keys(matchesByDay)
    .sort()
    .slice(offset)
    .map(day => ({
      date: day,
      data: Object.keys(matchesByDay[day])
        .map(matchId => matchesByDay[day][matchId])
        .sort(getComparatorForKey('startTime')),
    }))
}

function getDateIndex(
  matchesByDay: CalendarItemByDay<MatchListItem>,
  date: Date
) {
  const isoDate = cleanDate(date.toISOString())
  const allDays = Object.keys(matchesByDay)
  const dateIndex = allDays.sort().findIndex(day => day >= isoDate)
  return Math.max(0, dateIndex)
}

interface State {
  dateOffset: number
}

const STEP = 10
export default class SeasonMatches extends React.PureComponent<Props, State> {
  static defaultProps = {
    topInset: 0,
  }

  state = { dateOffset: 0 }

  componentDidUpdate = (prevProps: Props) => {
    const { matchesByDay, today } = this.props
    if (prevProps.matchesByDay !== matchesByDay || prevProps.today !== today) {
      this.handleUpdate()
    }
  }

  componentDidMount = () => {
    this.handleUpdate()
  }

  handleUpdate = () => {
    const { today, matchesByDay } = this.props
    const dateOffset = getDateIndex(matchesByDay, today)
    this.setState({ dateOffset })
  }

  handleShowPrevious = () => {
    this.setState(prevState => ({
      dateOffset: Math.max(0, prevState.dateOffset - STEP),
    }))
  }

  handleShowPreviousButton = () => {
    const { dateOffset } = this.state
    if (dateOffset === 0) {
      return null
    }
    return (
      <View style={styles.previousButtonContainer}>
        <GenericButton
          title={translate('seasonMatches.showPrevious')}
          onPress={this.handleShowPrevious}
        />
      </View>
    )
  }

  render = () => {
    const {
      matchesByDay,
      onSelectMatch,
      localizedTeams,
      today,
      results,
      topInset,
    } = this.props
    const { dateOffset } = this.state
    const sections = getSectionContent(matchesByDay, dateOffset)
    const todayIsoDate = cleanDate(today.toISOString())

    return (
      <View style={styles.container}>
        <SectionList
          contentContainerStyle={[
            styles.list,
            {
              paddingTop: topInset,
            },
          ]}
          style={styles.list}
          keyExtractor={item => item.id}
          stickySectionHeadersEnabled={false}
          ListHeaderComponent={this.handleShowPreviousButton}
          renderSectionHeader={({ section: { date } }) => {
            const dateObj = new Date(date)
            return (
              <View style={styles.dateHeaderWrapper}>
                <LinearGradient
                  style={styles.dateHeader}
                  colors={[colors.primary, colors.primaryAlt]}
                  start={{ x: 0, y: 0 }}
                  end={{ x: 1, y: 0 }}
                >
                  <Text style={styles.dateHeaderText}>
                    {dateFormatter.format(dateObj)}
                  </Text>
                </LinearGradient>
              </View>
            )
          }}
          renderItem={({ item }: { item: MatchListItem; index: number }) => (
            <MatchListItemView
              today={todayIsoDate}
              match={item}
              dateFormatter={timeFormatter}
              localizedTeams={localizedTeams}
              onSelectMatch={onSelectMatch}
              result={results && results[item.id]}
              key={item.id}
            />
          )}
          sections={sections}
        />
      </View>
    )
  }
}

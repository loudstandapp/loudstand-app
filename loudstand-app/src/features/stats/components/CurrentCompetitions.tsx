import React from 'react'
import { SeasonBasic, ID } from '@loudstand/shared'
import { StyleSheet, View, TouchableOpacity } from 'react-native'
import RemoteImage from '../../../components/RemoteImage'
import { fallbackImages, getImageUrl, ItemImageType } from '../../../utils'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import colors from '../../../theme/colors'

interface Props {
  seasons: SeasonBasic[]
  onSelectSeason: (id: ID) => void
}

const IMAGE_SIZE = 32
const logoCircleSize = IMAGE_SIZE * 1.2

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: marginDefault,
  },
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  seasonItem: {
    width: logoCircleSize,
    height: logoCircleSize,
    borderRadius: logoCircleSize / 2,
    marginHorizontal: spacingUnit,
    elevation: 2,
    shadowRadius: 2,
    shadowOpacity: 0.3,
    shadowColor: 'black',
    shadowOffset: { width: 1, height: 1 },
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.background,
  },
  logoImage: {
    width: IMAGE_SIZE,
    height: IMAGE_SIZE,
    borderRadius: IMAGE_SIZE / 2,
  },
})

export default class CurrentCompetitions extends React.PureComponent<Props> {
  render = () => {
    const { seasons, onSelectSeason } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          {seasons.map(season => {
            const imageUrl = getImageUrl(season.id, ItemImageType.SeasonLogo)
            return (
              <TouchableOpacity
                style={styles.seasonItem}
                key={season.id}
                onPress={() => onSelectSeason(season.id)}
              >
                <RemoteImage
                  source={{ uri: imageUrl }}
                  defaultSource={fallbackImages.seasonLogo}
                  style={styles.logoImage}
                />
              </TouchableOpacity>
            )
          })}
        </View>
      </View>
    )
  }
}

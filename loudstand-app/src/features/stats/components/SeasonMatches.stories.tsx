import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import SeasonMatches from './SeasonMatches'
import { TEST_SEASON_CALENDAR } from '../screens/LeaguesScreen.stories'

storiesOf('SeasonMatches', module)
  .add('mid season', () => (
    <SeasonMatches
      matchesByDay={TEST_SEASON_CALENDAR}
      onSelectMatch={action('onSelectMatch')}
      today={new Date('2019-03-01')}
    />
  ))
  .add('before first match', () => (
    <SeasonMatches
      matchesByDay={TEST_SEASON_CALENDAR}
      onSelectMatch={action('onSelectMatch')}
      today={new Date('2018-08-01')}
    />
  ))
  .add('after last match', () => (
    <SeasonMatches
      matchesByDay={TEST_SEASON_CALENDAR}
      onSelectMatch={action('onSelectMatch')}
      today={new Date('2019-06-01')}
    />
  ))

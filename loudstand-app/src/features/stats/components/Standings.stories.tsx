/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { storiesOf } from '@storybook/react-native'
import {
  SeasonStandings,
  SeasonTeamPopularityDocument,
} from '@loudstand/shared'
import Standings from './Standings'

export const TEST_TEAM_POPULARITY: SeasonTeamPopularityDocument = {
  'sr:competitor:2046': {
    value: 0.4,
    allAverage: 0.3,
    lastAverage: 0.4,
  },
  'sr:competitor:4856': {
    value: 0.32,
    allAverage: 0.3,
    lastAverage: 0.4,
  },
  'sr:competitor:2203': {
    value: 0.24,
    allAverage: 0.3,
    lastAverage: 0.4,
  },
  'sr:competitor:2061': {
    value: 0.122,
    allAverage: 0.3,
    lastAverage: 0.4,
  },
  'sr:competitor:2058': {
    value: 0.25,
    allAverage: 0.3,
    lastAverage: 0.4,
  },
  'sr:competitor:2055': {
    value: 0.223,
    allAverage: 0.3,
    lastAverage: 0.4,
  },
  'sr:competitor:2051': {
    value: 0.72,
    allAverage: 0.3,
    lastAverage: 0.4,
  },
  'sr:competitor:2050': {
    value: 0.92,
    allAverage: 0.3,
    lastAverage: 0.4,
  },
}

export const TEST_SEASON_STANDINGS: SeasonStandings = {
  'sr:group:33639': {
    id: 'sr:group:33639',
    name: null,
    standings: {
      'sr:competitor:2046': {
        change: 0,
        goalDifference: {
          against: 18,
          for: 51,
        },
        playedMatches: 22,
        points: 55,
        position: 1,
        team: {
          id: 'sr:competitor:2046',
          name: 'FC Salzburg',
          country: 'DE',
        },
      },
      'sr:competitor:2050': {
        change: 0,
        goalDifference: {
          against: 42,
          for: 26,
        },
        playedMatches: 22,
        points: 21,
        position: 10,
        team: {
          id: 'sr:competitor:2050',
          name: 'Admira Wacker',
          country: 'DE',
        },
      },
      'sr:competitor:2051': {
        change: 3,
        goalDifference: {
          against: 23,
          for: 26,
        },
        playedMatches: 22,
        points: 31,
        position: 3,
        team: {
          id: 'sr:competitor:2051',
          name: 'SK Sturm Graz',
          country: 'DE',
        },
      },
      'sr:competitor:2055': {
        change: -1,
        goalDifference: {
          against: 29,
          for: 26,
        },
        playedMatches: 22,
        points: 27,
        position: 8,
        team: {
          id: 'sr:competitor:2055',
          name: 'SK Rapid Vienna',
          country: 'DE',
        },
      },
      'sr:competitor:2058': {
        change: 0,
        goalDifference: {
          against: 19,
          for: 40,
        },
        playedMatches: 22,
        points: 46,
        position: 2,
        team: {
          id: 'sr:competitor:2058',
          name: 'LASK Linz',
          country: 'DE',
        },
      },
      'sr:competitor:2061': {
        change: 1,
        goalDifference: {
          against: 36,
          for: 28,
        },
        playedMatches: 22,
        points: 29,
        position: 7,
        team: {
          id: 'sr:competitor:2061',
          name: 'SV Mattersburg',
          country: 'DE',
        },
      },
      'sr:competitor:2075': {
        change: 0,
        goalDifference: {
          against: 45,
          for: 35,
        },
        playedMatches: 22,
        points: 26,
        position: 9,
        team: {
          id: 'sr:competitor:2075',
          name: 'TSV Hartberg',
          country: 'DE',
        },
      },
      'sr:competitor:2076': {
        change: 1,
        goalDifference: {
          against: 31,
          for: 32,
        },
        playedMatches: 22,
        points: 30,
        position: 4,
        team: {
          id: 'sr:competitor:2076',
          name: 'RZ Pellets WAC',
          country: 'DE',
        },
      },
      'sr:competitor:2088': {
        change: 0,
        goalDifference: {
          against: 34,
          for: 17,
        },
        playedMatches: 22,
        points: 17,
        position: 12,
        team: {
          id: 'sr:competitor:2088',
          name: 'Wacker Innsbruck',
          country: 'DE',
        },
      },
      'sr:competitor:2119': {
        change: -2,
        goalDifference: {
          against: 29,
          for: 26,
        },
        playedMatches: 22,
        points: 30,
        position: 6,
        team: {
          id: 'sr:competitor:2119',
          name: 'SKN St. Polten',
          country: 'DE',
        },
      },
      'sr:competitor:2203': {
        change: -2,
        goalDifference: {
          against: 28,
          for: 29,
        },
        playedMatches: 22,
        points: 30,
        position: 5,
        team: {
          id: 'sr:competitor:2203',
          name: 'FK Austria Vienna',
          country: 'DE',
        },
      },
      'sr:competitor:4856': {
        change: 0,
        goalDifference: {
          against: 32,
          for: 30,
        },
        playedMatches: 22,
        points: 18,
        position: 11,
        team: {
          id: 'sr:competitor:4856',
          name: 'SCR Altach',
          country: 'DE',
        },
      },
    },
  },
  'sr:group:39962': {
    id: 'sr:group:39962',
    name: 'Championship Round',
    standings: {
      'sr:competitor:2046': {
        change: 0,
        goalDifference: {
          against: 26,
          for: 70,
        },
        playedMatches: 30,
        points: 46,
        position: 1,
        team: {
          id: 'sr:competitor:2046',
          name: 'FC Salzburg',
          country: 'DE',
        },
      },
      'sr:competitor:2051': {
        change: 0,
        goalDifference: {
          against: 36,
          for: 35,
        },
        playedMatches: 30,
        points: 24,
        position: 4,
        team: {
          id: 'sr:competitor:2051',
          name: 'SK Sturm Graz',
          country: 'DE',
        },
      },
      'sr:competitor:2058': {
        change: 0,
        goalDifference: {
          against: 29,
          for: 53,
        },
        playedMatches: 30,
        points: 34,
        position: 2,
        team: {
          id: 'sr:competitor:2058',
          name: 'LASK Linz',
          country: 'DE',
        },
      },
      'sr:competitor:2076': {
        change: 0,
        goalDifference: {
          against: 44,
          for: 45,
        },
        playedMatches: 30,
        points: 28,
        position: 3,
        team: {
          id: 'sr:competitor:2076',
          name: 'RZ Pellets WAC',
          country: 'DE',
        },
      },
      'sr:competitor:2119': {
        change: 0,
        goalDifference: {
          against: 42,
          for: 32,
        },
        playedMatches: 30,
        points: 21,
        position: 6,
        team: {
          id: 'sr:competitor:2119',
          name: 'SKN St. Polten',
          country: 'DE',
        },
      },
      'sr:competitor:2203': {
        change: 0,
        goalDifference: {
          against: 43,
          for: 41,
        },
        playedMatches: 30,
        points: 24,
        position: 5,
        team: {
          id: 'sr:competitor:2203',
          name: 'FK Austria Vienna',
          country: 'DE',
        },
      },
    },
  },
  'sr:group:39964': {
    id: 'sr:group:39964',
    name: 'Relegation Round',
    standings: {
      'sr:competitor:2050': {
        change: -1,
        goalDifference: {
          against: 57,
          for: 38,
        },
        playedMatches: 30,
        points: 19,
        position: 5,
        team: {
          id: 'sr:competitor:2050',
          name: 'Admira Wacker',
          country: 'DE',
        },
      },
      'sr:competitor:2055': {
        change: 0,
        goalDifference: {
          against: 41,
          for: 47,
        },
        playedMatches: 30,
        points: 32,
        position: 1,
        team: {
          id: 'sr:competitor:2055',
          name: 'SK Rapid Vienna',
          country: 'DE',
        },
      },
      'sr:competitor:2061': {
        change: 0,
        goalDifference: {
          against: 44,
          for: 40,
        },
        playedMatches: 30,
        points: 25,
        position: 2,
        team: {
          id: 'sr:competitor:2061',
          name: 'SV Mattersburg',
          country: 'DE',
        },
      },
      'sr:competitor:2075': {
        change: 2,
        goalDifference: {
          against: 62,
          for: 44,
        },
        playedMatches: 30,
        points: 19,
        position: 4,
        team: {
          id: 'sr:competitor:2075',
          country: 'DE',
          name: 'TSV Hartberg',
        },
      },
      'sr:competitor:2088': {
        change: -1,
        goalDifference: {
          against: 48,
          for: 26,
        },
        playedMatches: 30,
        points: 17,
        position: 6,
        team: {
          id: 'sr:competitor:2088',
          country: 'DE',
          name: 'Wacker Innsbruck',
        },
      },
      'sr:competitor:4856': {
        change: 0,
        goalDifference: {
          against: 42,
          for: 43,
        },
        playedMatches: 30,
        points: 22,
        position: 3,
        team: {
          id: 'sr:competitor:4856',
          country: 'DE',
          name: 'SCR Altach',
        },
      },
    },
  },
}

storiesOf('Standings', module)
  // .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <Standings
      standingGroups={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
    />
  ))
  .add('ongoing match', () => (
    <Standings
      standingGroups={TEST_SEASON_STANDINGS}
      playingTeams={[
        {
          id: 'sr:competitor:1287',
          name: 'Esbjerg FB',
          country: 'NL',
        },
        {
          id: 'sr:competitor:1314',
          name: 'Vendsyssel FF',
          country: 'NL',
        },
      ]}
      teamPopularity={TEST_TEAM_POPULARITY}
    />
  ))
  .add('no data', () => <Standings standingGroups={{}} teamPopularity={{}} />)

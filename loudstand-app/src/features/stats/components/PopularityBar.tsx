import React from 'react'
import { StyleProp, ViewStyle, View, StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { formatPercentage } from '../../../utils'
import colors from '../../../theme/colors'

const HEIGHT = 6
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.tertiaryLight,
    height: HEIGHT,
    borderRadius: HEIGHT / 2,
  },
  indicator: {
    flex: 1,
    borderRadius: HEIGHT / 2,
  },
})

interface Props {
  value: number
  style?: StyleProp<ViewStyle>
}
export default class PopularityBar extends React.PureComponent<Props> {
  render = () => {
    const { value, style } = this.props
    return (
      <View style={[styles.container, style]}>
        <LinearGradient
          colors={[colors.primary, colors.primaryAlt]}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          style={[styles.indicator, { width: formatPercentage(value) }]}
          removeClippedSubviews
        />
      </View>
    )
  }
}

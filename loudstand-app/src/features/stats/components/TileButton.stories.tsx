/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { View, ScrollView } from 'react-native'
import TileButton from './TileButton'
import colors from '../../../theme/colors'

storiesOf('TileButton', module)
  // .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <ScrollView>
      <View style={{ width: 150, height: 150 }}>
        <TileButton
          icon={require('../../../../assets/icons/buttons/league-profile.png')}
          onPress={action('onPress')}
          title="Some title"
          subTitle="Some long subtitle"
          backgroundColors={['#faba9a', '#c08050']}
          color="white"
        />
      </View>

      <View style={{ width: 150, height: 150 }}>
        <TileButton
          icon={require('../../../../assets/icons/buttons/league-profile.png')}
          onPress={action('onPress')}
          title="Some title"
          subTitle="Some long subtitle"
          backgroundColors={[colors.primary, colors.primaryAlt]}
          color="white"
        />
      </View>

      <View style={{ width: 150, height: 150 }}>
        <TileButton
          icon={require('../../../../assets/icons/buttons/league-profile.png')}
          onPress={action('onPress')}
          title="Some title"
          subTitle="Some long subtitle"
          backgroundColors={[colors.background, colors.backgroundAlt]}
          color={colors.primaryText}
        />
      </View>

      <View style={{ width: 150, height: 150 }}>
        <TileButton
          icon={require('../../../../assets/icons/buttons/league-profile.png')}
          onPress={action('onPress')}
          title="Some title"
          subTitle="Some long subtitle"
          backgroundColors={[colors.primary, colors.secondary]}
          color={colors.secondaryText}
        />
      </View>
    </ScrollView>
  ))

import React from 'react'
import { MatchListItem } from '@loudstand/shared'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { getImageUrl, ItemImageType, fallbackImages } from '../../../utils'
import { translate, currentLocale } from '../../../translation'
import { timeFormatter } from '../../match/components/MatchTime'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { spacingUnit } from '../../../theme/layout'
import RemoteImage from '../../../components/RemoteImage'
import RemoteImageBackground from '../../../components/RemoteImageBackground'

const dateFormatter = new Intl.DateTimeFormat(currentLocale.languageTag, {
  year: 'numeric',
  month: 'numeric',
  day: 'numeric',
  weekday: 'short',
})
interface Props {
  match: MatchListItem
  onPress: (match: MatchListItem) => void
}
const LOGO_SIZE = 60

const styles = StyleSheet.create({
  outerContainer: {
    margin: 2 * spacingUnit,
  },
  containerBackgroundImage: {
    borderRadius: spacingUnit,
  },
  innerContainer: {
    padding: spacingUnit,
    borderRadius: spacingUnit,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  teamLogo: {
    width: LOGO_SIZE,
    height: LOGO_SIZE,
  },
  textContainer: {
    alignItems: 'center',
    flex: 1,
  },
  standingsText: {
    ...(fonts.CustomMatchScore as object),
    color: colors.secondaryText,
  },
  startTimeText: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
    marginTop: spacingUnit / 2,
  },
  stadiumNameText: {
    ...(fonts.BodyStrong as object),
    marginTop: spacingUnit / 2,
    color: colors.secondaryText,
  },
})

export default class TeamNextMatch extends React.PureComponent<Props> {
  handleSelectMatch = () => {
    const { match, onPress } = this.props
    onPress(match)
  }

  render = () => {
    const { match } = this.props

    return (
      <TouchableOpacity
        onPress={this.handleSelectMatch}
        style={styles.outerContainer}
      >
        <RemoteImageBackground
          style={styles.containerBackgroundImage}
          source={
            match.venue
              ? {
                  uri: getImageUrl(match.venue.id, ItemImageType.Venue),
                }
              : fallbackImages.venue
          }
          defaultSource={fallbackImages.venue}
        >
          <LinearGradient
            colors={['rgba(0,0,0,0.4)', 'rgba(0,0,0,0.6)']}
            style={styles.innerContainer}
          >
            {match.teams && (
              <RemoteImage
                style={styles.teamLogo}
                defaultSource={fallbackImages.teamLogo}
                source={{
                  uri: getImageUrl(match.teams.home.id, ItemImageType.TeamLogo),
                }}
              />
            )}
            <View style={styles.textContainer}>
              <Text style={styles.standingsText}>{translate('match.vs')}</Text>
              {match.startTime && (
                <Text
                  style={styles.startTimeText}
                  numberOfLines={1}
                  ellipsizeMode="head"
                >
                  {`${dateFormatter.format(
                    new Date(match.startTime)
                  )} | ${timeFormatter.format(new Date(match.startTime))}`}
                </Text>
              )}
              {match.venue && (
                <Text
                  style={styles.stadiumNameText}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                >
                  {match.venue.name}
                </Text>
              )}
            </View>
            {match.teams && (
              <RemoteImage
                style={styles.teamLogo}
                defaultSource={fallbackImages.teamLogo}
                source={{
                  uri: getImageUrl(match.teams.away.id, ItemImageType.TeamLogo),
                }}
              />
            )}
          </LinearGradient>
        </RemoteImageBackground>
      </TouchableOpacity>
    )
  }
}

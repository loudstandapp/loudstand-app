import {
  SeasonPlayerStandingsByType,
  SeasonPlayerStandingType,
} from '@loudstand/shared'
import { storiesOf } from '@storybook/react-native'
import React from 'react'
import PlayerStandings from './PlayerStandings'

export const TEST_PLAYER_STANDINGS: SeasonPlayerStandingsByType = {
  TOP_YELLOW_RED_CARDS: {
    'sr:player:1237978': {
      value: 1,
      player: {
        id: 'sr:player:1237978',
        name: 'Victor Mpindi Ekani',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 1,
    },
    'sr:player:929734': {
      value: 1,
      player: {
        id: 'sr:player:929734',
        name: 'Mads Juel Andersen',
      },
      team: {
        id: 'sr:competitor:1297',
        name: 'AC Horsens',
      },
      position: 1,
    },
    'sr:player:1046705': {
      value: 1,
      player: {
        id: 'sr:player:1046705',
        name: 'Joel Kabongo',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 1,
    },
    'sr:player:50455': {
      value: 1,
      player: {
        id: 'sr:player:50455',
        name: 'Erik Marxen',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 1,
    },
    'sr:player:22801': {
      value: 1,
      player: {
        id: 'sr:player:22801',
        name: 'Sivert Heltne Nilsen',
      },
      team: {
        id: 'sr:competitor:1297',
        name: 'AC Horsens',
      },
      position: 1,
    },
    'sr:player:34725': {
      value: 1,
      player: {
        id: 'sr:player:34725',
        name: 'Daniel Christensen',
      },
      team: {
        id: 'sr:competitor:1314',
        name: 'Vendsyssel FF',
      },
      position: 1,
    },
    'sr:player:116019': {
      value: 1,
      player: {
        id: 'sr:player:116019',
        name: 'Marc Dal Hende',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 1,
    },
    'sr:player:295123': {
      value: 1,
      player: {
        id: 'sr:player:295123',
        name: 'Paul Onuachu',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 1,
    },
    'sr:player:36211': {
      value: 1,
      player: {
        id: 'sr:player:36211',
        name: 'Kevin Conboy',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 1,
    },
    'sr:player:591466': {
      value: 1,
      player: {
        id: 'sr:player:591466',
        name: 'Mayron George',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 1,
    },
    'sr:player:350562': {
      value: 1,
      player: {
        id: 'sr:player:350562',
        name: 'Oscar Whalley',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 1,
    },
    'sr:player:53808': {
      value: 1,
      player: {
        id: 'sr:player:53808',
        name: 'Agus Garcia',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 1,
    },
    'sr:player:231894': {
      value: 1,
      player: {
        id: 'sr:player:231894',
        name: 'Francisco Junior',
      },
      team: {
        id: 'sr:competitor:1314',
        name: 'Vendsyssel FF',
      },
      position: 1,
    },
    'sr:player:281233': {
      value: 1,
      player: {
        id: 'sr:player:281233',
        name: 'Casper Hojer Nielsen',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 1,
    },
    'sr:player:935554': {
      value: 1,
      player: {
        id: 'sr:player:935554',
        name: 'Jonas Wind',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 1,
    },
    'sr:player:1110735': {
      value: 1,
      player: {
        id: 'sr:player:1110735',
        name: 'Allan Sousa',
      },
      team: {
        id: 'sr:competitor:4872',
        name: 'Vejle BK',
      },
      position: 1,
    },
    'sr:player:570406': {
      value: 1,
      player: {
        id: 'sr:player:570406',
        name: 'Jon Dagur Thorsteinsson',
      },
      team: {
        id: 'sr:competitor:1314',
        name: 'Vendsyssel FF',
      },
      position: 1,
    },
    'sr:player:97026': {
      value: 1,
      player: {
        id: 'sr:player:97026',
        name: 'Lasha Parunashvili',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 1,
    },
    'sr:player:344035': {
      value: 1,
      player: {
        id: 'sr:player:344035',
        name: 'Oti',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 1,
    },
    'sr:player:991169': {
      value: 1,
      player: {
        id: 'sr:player:991169',
        name: 'Benjamin Hvidt',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 1,
    },
    'sr:player:548090': {
      value: 1,
      player: {
        id: 'sr:player:548090',
        name: 'Philipp Ochs',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 1,
    },
    'sr:player:966433': {
      value: 1,
      player: {
        id: 'sr:player:966433',
        name: 'Andreas Skovgaard',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 1,
    },
    'sr:player:139357': {
      value: 1,
      player: {
        id: 'sr:player:139357',
        name: 'Adama Guira',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 1,
    },
    'sr:player:81485': {
      value: 1,
      player: {
        id: 'sr:player:81485',
        name: 'Hallur Hansson',
      },
      team: {
        id: 'sr:competitor:1297',
        name: 'AC Horsens',
      },
      position: 1,
    },
    'sr:player:855438': {
      value: 1,
      player: {
        id: 'sr:player:855438',
        name: 'Rasmus Nicolaisen',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 1,
    },
    'sr:player:90341': {
      value: 1,
      player: {
        id: 'sr:player:90341',
        name: 'Jores Okore',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 1,
    },
  },
  TOP_YELLOW_CARDS: {
    'sr:player:929732': {
      value: 7,
      player: {
        id: 'sr:player:929732',
        name: 'Sammy Skytte',
      },
      team: {
        id: 'sr:competitor:1297',
        name: 'AC Horsens',
      },
      position: 18,
    },
    'sr:player:50455': {
      value: 8,
      player: {
        id: 'sr:player:50455',
        name: 'Erik Marxen',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 6,
    },
    'sr:player:8507': {
      value: 7,
      player: {
        id: 'sr:player:8507',
        name: 'Niki Zimling',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 18,
    },
    'sr:player:21536': {
      value: 8,
      player: {
        id: 'sr:player:21536',
        name: 'Jesper Juelsgaard',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 6,
    },
    'sr:player:295123': {
      value: 7,
      player: {
        id: 'sr:player:295123',
        name: 'Paul Onuachu',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 18,
    },
    'sr:player:359802': {
      value: 8,
      player: {
        id: 'sr:player:359802',
        name: 'Karlo Bartolec',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 6,
    },
    'sr:player:355542': {
      value: 7,
      player: {
        id: 'sr:player:355542',
        name: 'Stefan Gartenmann',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 18,
    },
    'sr:player:28387': {
      value: 7,
      player: {
        id: 'sr:player:28387',
        name: 'Joni Kauko',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 18,
    },
    'sr:player:857882': {
      value: 9,
      player: {
        id: 'sr:player:857882',
        name: 'Oliver Abildgaard',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 3,
    },
    'sr:player:99174': {
      value: 8,
      player: {
        id: 'sr:player:99174',
        name: 'Marcel Romer',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 6,
    },
    'sr:player:37549': {
      value: 8,
      player: {
        id: 'sr:player:37549',
        name: 'Marc Pedersen',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 6,
    },
    'sr:player:356390': {
      value: 7,
      player: {
        id: 'sr:player:356390',
        name: 'Mikkel Kallesoe',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 18,
    },
    'sr:player:15916': {
      value: 8,
      player: {
        id: 'sr:player:15916',
        name: 'Jonas Brix-Damborg',
      },
      team: {
        id: 'sr:competitor:1392',
        name: 'Hobro IK',
      },
      position: 6,
    },
    'sr:player:7459': {
      value: 7,
      player: {
        id: 'sr:player:7459',
        name: 'Markus Halsti',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 18,
    },
    'sr:player:15233': {
      value: 7,
      player: {
        id: 'sr:player:15233',
        name: 'Paulus Arajuuri',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 18,
    },
    'sr:player:280999': {
      value: 7,
      player: {
        id: 'sr:player:280999',
        name: 'Ulrik Yttergard Jenssen',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 18,
    },
    'sr:player:570406': {
      value: 8,
      player: {
        id: 'sr:player:570406',
        name: 'Jon Dagur Thorsteinsson',
      },
      team: {
        id: 'sr:competitor:1314',
        name: 'Vendsyssel FF',
      },
      position: 6,
    },
    'sr:player:574486': {
      value: 8,
      player: {
        id: 'sr:player:574486',
        name: 'Frederik Borsting',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 6,
    },
    'sr:player:1015505': {
      value: 9,
      player: {
        id: 'sr:player:1015505',
        name: 'Victor Nelsson',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 3,
    },
    'sr:player:952868': {
      value: 8,
      player: {
        id: 'sr:player:952868',
        name: 'Jens Stage',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 6,
    },
    'sr:player:135263': {
      value: 7,
      player: {
        id: 'sr:player:135263',
        name: 'Lucas Andersen',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 18,
    },
    'sr:player:139357': {
      value: 8,
      player: {
        id: 'sr:player:139357',
        name: 'Adama Guira',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 6,
    },
    'sr:player:81485': {
      value: 8,
      player: {
        id: 'sr:player:81485',
        name: 'Hallur Hansson',
      },
      team: {
        id: 'sr:competitor:1297',
        name: 'AC Horsens',
      },
      position: 6,
    },
    'sr:player:41594': {
      value: 9,
      player: {
        id: 'sr:player:41594',
        name: 'Rodolph Austin',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 3,
    },
    'sr:player:292873': {
      value: 13,
      player: {
        id: 'sr:player:292873',
        name: 'Nicolai Poulsen',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 1,
    },
    'sr:player:541350': {
      value: 8,
      player: {
        id: 'sr:player:541350',
        name: 'Jonas Bager',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 6,
    },
    'sr:player:19398': {
      value: 10,
      player: {
        id: 'sr:player:19398',
        name: 'Eggert Gunnthor Jonsson',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 2,
    },
    'sr:player:174921': {
      value: 7,
      player: {
        id: 'sr:player:174921',
        name: 'Mads Lauritsen',
      },
      team: {
        id: 'sr:competitor:4872',
        name: 'Vejle BK',
      },
      position: 18,
    },
  },
  TOP_OWN_GOALS: {
    'sr:player:290413': {
      value: 1,
      player: {
        id: 'sr:player:290413',
        name: 'Jeppe Hojbjerg',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 4,
    },
    'sr:player:293127': {
      value: 2,
      player: {
        id: 'sr:player:293127',
        name: 'Denis Vavro',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 1,
    },
    'sr:player:37524': {
      value: 1,
      player: {
        id: 'sr:player:37524',
        name: 'Johnny Thomsen',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 4,
    },
    'sr:player:72903': {
      value: 1,
      player: {
        id: 'sr:player:72903',
        name: 'Jan Gregus',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 4,
    },
    'sr:player:1015505': {
      value: 2,
      player: {
        id: 'sr:player:1015505',
        name: 'Victor Nelsson',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 1,
    },
    'sr:player:15175': {
      value: 1,
      player: {
        id: 'sr:player:15175',
        name: 'Danny Olsen',
      },
      team: {
        id: 'sr:competitor:1392',
        name: 'Hobro IK',
      },
      position: 4,
    },
    'sr:player:50455': {
      value: 1,
      player: {
        id: 'sr:player:50455',
        name: 'Erik Marxen',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 4,
    },
    'sr:player:36150': {
      value: 2,
      player: {
        id: 'sr:player:36150',
        name: 'Pierre Kanstrup',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 1,
    },
    'sr:player:952868': {
      value: 1,
      player: {
        id: 'sr:player:952868',
        name: 'Jens Stage',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 4,
    },
    'sr:player:136706': {
      value: 1,
      player: {
        id: 'sr:player:136706',
        name: 'Josip Radosevic',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 4,
    },
    'sr:player:314292': {
      value: 1,
      player: {
        id: 'sr:player:314292',
        name: 'Kasper Pedersen',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 4,
    },
    'sr:player:966433': {
      value: 1,
      player: {
        id: 'sr:player:966433',
        name: 'Andreas Skovgaard',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 4,
    },
    'sr:player:13993': {
      value: 1,
      player: {
        id: 'sr:player:13993',
        name: 'Patrick Kristensen',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 4,
    },
    'sr:player:193578': {
      value: 1,
      player: {
        id: 'sr:player:193578',
        name: 'Soren Henriksen',
      },
      team: {
        id: 'sr:competitor:1314',
        name: 'Vendsyssel FF',
      },
      position: 4,
    },
    'sr:player:150802': {
      value: 1,
      player: {
        id: 'sr:player:150802',
        name: 'Alexander Ludwig',
      },
      team: {
        id: 'sr:competitor:1286',
        name: 'Odense Boldklub',
      },
      position: 4,
    },
  },
  TOP_RED_CARDS: {
    'sr:player:51178': {
      value: 1,
      player: {
        id: 'sr:player:51178',
        name: 'Matej Delac',
      },
      team: {
        id: 'sr:competitor:1297',
        name: 'AC Horsens',
      },
      position: 2,
    },
    'sr:player:137927': {
      value: 1,
      player: {
        id: 'sr:player:137927',
        name: 'Mohammad Amini',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 2,
    },
    'sr:player:123661': {
      value: 1,
      player: {
        id: 'sr:player:123661',
        name: 'Benjamin Bellot',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 2,
    },
    'sr:player:90341': {
      value: 1,
      player: {
        id: 'sr:player:90341',
        name: 'Jores Okore',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 2,
    },
    'sr:player:125383': {
      value: 1,
      player: {
        id: 'sr:player:125383',
        name: 'Thomas Juel Nielsen',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 2,
    },
    'sr:player:322485': {
      value: 1,
      player: {
        id: 'sr:player:322485',
        name: 'Jeppe Svenningsen',
      },
      team: {
        id: 'sr:competitor:1314',
        name: 'Vendsyssel FF',
      },
      position: 2,
    },
    'sr:player:1110735': {
      value: 1,
      player: {
        id: 'sr:player:1110735',
        name: 'Allan Sousa',
      },
      team: {
        id: 'sr:competitor:4872',
        name: 'Vejle BK',
      },
      position: 2,
    },
    'sr:player:1015509': {
      value: 1,
      player: {
        id: 'sr:player:1015509',
        name: 'Abdul Mumin',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 2,
    },
    'sr:player:134918': {
      value: 1,
      player: {
        id: 'sr:player:134918',
        name: 'Sonni Nattestad',
      },
      team: {
        id: 'sr:competitor:1297',
        name: 'AC Horsens',
      },
      position: 2,
    },
    'sr:player:48050': {
      value: 1,
      player: {
        id: 'sr:player:48050',
        name: 'Erik Sviatchenko',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 2,
    },
    'sr:player:99174': {
      value: 1,
      player: {
        id: 'sr:player:99174',
        name: 'Marcel Romer',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 2,
    },
    'sr:player:87148': {
      value: 2,
      player: {
        id: 'sr:player:87148',
        name: 'Viljormur Davidsen',
      },
      team: {
        id: 'sr:competitor:4872',
        name: 'Vejle BK',
      },
      position: 1,
    },
    'sr:player:97026': {
      value: 1,
      player: {
        id: 'sr:player:97026',
        name: 'Lasha Parunashvili',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 2,
    },
    'sr:player:53808': {
      value: 1,
      player: {
        id: 'sr:player:53808',
        name: 'Agus Garcia',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 2,
    },
    'sr:player:1395885': {
      value: 1,
      player: {
        id: 'sr:player:1395885',
        name: 'Emil Riis Jakobsen',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 2,
    },
  },
  TOP_ASSISTS: {
    'sr:player:116660': {
      value: 4,
      player: {
        id: 'sr:player:116660',
        name: 'Rasmus Falk',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 20,
    },
    'sr:player:36048': {
      value: 7,
      player: {
        id: 'sr:player:36048',
        name: 'Tobias Sana',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 5,
    },
    'sr:player:95443': {
      value: 6,
      player: {
        id: 'sr:player:95443',
        name: 'Nicolai Boilesen',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 10,
    },
    'sr:player:125261': {
      value: 7,
      player: {
        id: 'sr:player:125261',
        name: 'Dominik Kaiser',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 5,
    },
    'sr:player:997779': {
      value: 4,
      player: {
        id: 'sr:player:997779',
        name: 'Rasmus Thellufsen',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 20,
    },
    'sr:player:1336192': {
      value: 6,
      player: {
        id: 'sr:player:1336192',
        name: 'Mikkel Damsgaard',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 10,
    },
    'sr:player:113700': {
      value: 8,
      player: {
        id: 'sr:player:113700',
        name: 'Peter Ankersen',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 4,
    },
    'sr:player:221350': {
      value: 4,
      player: {
        id: 'sr:player:221350',
        name: 'Hany Mukhtar',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 20,
    },
    'sr:player:247471': {
      value: 4,
      player: {
        id: 'sr:player:247471',
        name: 'Casper Nielsen',
      },
      team: {
        id: 'sr:competitor:1286',
        name: 'Odense Boldklub',
      },
      position: 20,
    },
    'sr:player:94287': {
      value: 11,
      player: {
        id: 'sr:player:94287',
        name: 'Viktor Fischer',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 1,
    },
    'sr:player:36211': {
      value: 5,
      player: {
        id: 'sr:player:36211',
        name: 'Kevin Conboy',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 14,
    },
    'sr:player:137927': {
      value: 6,
      player: {
        id: 'sr:player:137927',
        name: 'Mohammad Amini',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 10,
    },
    'sr:player:209742': {
      value: 5,
      player: {
        id: 'sr:player:209742',
        name: 'Mikkel Rygaard',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 14,
    },
    'sr:player:307082': {
      value: 10,
      player: {
        id: 'sr:player:307082',
        name: 'Awer Mabil',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 2,
    },
    'sr:player:121260': {
      value: 7,
      player: {
        id: 'sr:player:121260',
        name: 'Kasper Kusk',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 5,
    },
    'sr:player:147634': {
      value: 4,
      player: {
        id: 'sr:player:147634',
        name: 'Gustav Wikheim',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 20,
    },
    'sr:player:1110735': {
      value: 5,
      player: {
        id: 'sr:player:1110735',
        name: 'Allan Sousa',
      },
      team: {
        id: 'sr:competitor:4872',
        name: 'Vejle BK',
      },
      position: 14,
    },
    'sr:player:935554': {
      value: 5,
      player: {
        id: 'sr:player:935554',
        name: 'Jonas Wind',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 14,
    },
    'sr:player:57971': {
      value: 4,
      player: {
        id: 'sr:player:57971',
        name: 'Jacob Schoop',
      },
      team: {
        id: 'sr:competitor:4872',
        name: 'Vejle BK',
      },
      position: 20,
    },
    'sr:player:570406': {
      value: 4,
      player: {
        id: 'sr:player:570406',
        name: 'Jon Dagur Thorsteinsson',
      },
      team: {
        id: 'sr:competitor:1314',
        name: 'Vendsyssel FF',
      },
      position: 20,
    },
    'sr:player:327863': {
      value: 7,
      player: {
        id: 'sr:player:327863',
        name: 'Robert Skov',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 5,
    },
    'sr:player:121267': {
      value: 9,
      player: {
        id: 'sr:player:121267',
        name: 'Jesper Lauridsen',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 3,
    },
    'sr:player:8364': {
      value: 6,
      player: {
        id: 'sr:player:8364',
        name: 'Jakob Poulsen',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 10,
    },
    'sr:player:1033199': {
      value: 7,
      player: {
        id: 'sr:player:1033199',
        name: 'Mustapha Bundu',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 5,
    },
    'sr:player:41968': {
      value: 4,
      player: {
        id: 'sr:player:41968',
        name: 'Kian Hansen',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 20,
    },
    'sr:player:67761': {
      value: 4,
      player: {
        id: 'sr:player:67761',
        name: 'Johan Larsson',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 20,
    },
    'sr:player:95446': {
      value: 5,
      player: {
        id: 'sr:player:95446',
        name: 'Bashkim Kadrii',
      },
      team: {
        id: 'sr:competitor:1286',
        name: 'Odense Boldklub',
      },
      position: 14,
    },
    'sr:player:775809': {
      value: 5,
      player: {
        id: 'sr:player:775809',
        name: 'Joel Andersson',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 14,
    },
  },
  TOP_POINTS: {
    'sr:player:36048': {
      value: 13,
      player: {
        id: 'sr:player:36048',
        name: 'Tobias Sana',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 10,
    },
    'sr:player:94287': {
      value: 20,
      player: {
        id: 'sr:player:94287',
        name: 'Viktor Fischer',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 5,
    },
    'sr:player:918706': {
      value: 10,
      player: {
        id: 'sr:player:918706',
        name: 'Evander',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 18,
    },
    'sr:player:221350': {
      value: 10,
      player: {
        id: 'sr:player:221350',
        name: 'Hany Mukhtar',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 18,
    },
    'sr:player:116019': {
      value: 10,
      player: {
        id: 'sr:player:116019',
        name: 'Marc Dal Hende',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 18,
    },
    'sr:player:295123': {
      value: 20,
      player: {
        id: 'sr:player:295123',
        name: 'Paul Onuachu',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 5,
    },
    'sr:player:307082': {
      value: 16,
      player: {
        id: 'sr:player:307082',
        name: 'Awer Mabil',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 7,
    },
    'sr:player:121260': {
      value: 12,
      player: {
        id: 'sr:player:121260',
        name: 'Kasper Kusk',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 13,
    },
    'sr:player:316513': {
      value: 13,
      player: {
        id: 'sr:player:316513',
        name: 'Godsway Donyoh',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 10,
    },
    'sr:player:1110735': {
      value: 15,
      player: {
        id: 'sr:player:1110735',
        name: 'Allan Sousa',
      },
      team: {
        id: 'sr:competitor:4872',
        name: 'Vejle BK',
      },
      position: 8,
    },
    'sr:player:935554': {
      value: 11,
      player: {
        id: 'sr:player:935554',
        name: 'Jonas Wind',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 15,
    },
    'sr:player:1239198': {
      value: 24,
      player: {
        id: 'sr:player:1239198',
        name: 'Andreas Olsen',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 2,
    },
    'sr:player:218618': {
      value: 10,
      player: {
        id: 'sr:player:218618',
        name: 'Mart Lieder',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 18,
    },
    'sr:player:327863': {
      value: 35,
      player: {
        id: 'sr:player:327863',
        name: 'Robert Skov',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 1,
    },
    'sr:player:8364': {
      value: 13,
      player: {
        id: 'sr:player:8364',
        name: 'Jakob Poulsen',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 10,
    },
    'sr:player:135263': {
      value: 12,
      player: {
        id: 'sr:player:135263',
        name: 'Lucas Andersen',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 13,
    },
    'sr:player:30178': {
      value: 24,
      player: {
        id: 'sr:player:30178',
        name: "Dame N'Doye",
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 2,
    },
    'sr:player:1033199': {
      value: 11,
      player: {
        id: 'sr:player:1033199',
        name: 'Mustapha Bundu',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 15,
    },
    'sr:player:95446': {
      value: 15,
      player: {
        id: 'sr:player:95446',
        name: 'Bashkim Kadrii',
      },
      team: {
        id: 'sr:competitor:1286',
        name: 'Odense Boldklub',
      },
      position: 8,
    },
    'sr:player:46130': {
      value: 23,
      player: {
        id: 'sr:player:46130',
        name: 'Kamil Wilczek',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 4,
    },
    'sr:player:286923': {
      value: 11,
      player: {
        id: 'sr:player:286923',
        name: 'Saba Lobzhanidze',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 15,
    },
  },
  TOP_GOALS: {
    'sr:player:48461': {
      value: 8,
      player: {
        id: 'sr:player:48461',
        name: 'Tom Van Weert',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 12,
    },
    'sr:player:36210': {
      value: 9,
      player: {
        id: 'sr:player:36210',
        name: 'Patrick Mortensen',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 10,
    },
    'sr:player:94287': {
      value: 9,
      player: {
        id: 'sr:player:94287',
        name: 'Viktor Fischer',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 10,
    },
    'sr:player:918706': {
      value: 8,
      player: {
        id: 'sr:player:918706',
        name: 'Evander',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 12,
    },
    'sr:player:119170': {
      value: 7,
      player: {
        id: 'sr:player:119170',
        name: 'Pal Alexander Kirkevold',
      },
      team: {
        id: 'sr:competitor:1392',
        name: 'Hobro IK',
      },
      position: 18,
    },
    'sr:player:116019': {
      value: 7,
      player: {
        id: 'sr:player:116019',
        name: 'Marc Dal Hende',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 18,
    },
    'sr:player:295123': {
      value: 17,
      player: {
        id: 'sr:player:295123',
        name: 'Paul Onuachu',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 5,
    },
    'sr:player:28387': {
      value: 8,
      player: {
        id: 'sr:player:28387',
        name: 'Joni Kauko',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 12,
    },
    'sr:player:316513': {
      value: 10,
      player: {
        id: 'sr:player:316513',
        name: 'Godsway Donyoh',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 6,
    },
    'sr:player:1110735': {
      value: 10,
      player: {
        id: 'sr:player:1110735',
        name: 'Allan Sousa',
      },
      team: {
        id: 'sr:competitor:4872',
        name: 'Vejle BK',
      },
      position: 6,
    },
    'sr:player:1239198': {
      value: 21,
      player: {
        id: 'sr:player:1239198',
        name: 'Andreas Olsen',
      },
      team: {
        id: 'sr:competitor:1292',
        name: 'FC Nordsjaelland',
      },
      position: 3,
    },
    'sr:player:218618': {
      value: 7,
      player: {
        id: 'sr:player:218618',
        name: 'Mart Lieder',
      },
      team: {
        id: 'sr:competitor:1295',
        name: 'SonderjyskE',
      },
      position: 18,
    },
    'sr:player:327863': {
      value: 28,
      player: {
        id: 'sr:player:327863',
        name: 'Robert Skov',
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 1,
    },
    'sr:player:820400': {
      value: 8,
      player: {
        id: 'sr:player:820400',
        name: 'Adrian Petre',
      },
      team: {
        id: 'sr:competitor:1287',
        name: 'Esbjerg FB',
      },
      position: 12,
    },
    'sr:player:8364': {
      value: 7,
      player: {
        id: 'sr:player:8364',
        name: 'Jakob Poulsen',
      },
      team: {
        id: 'sr:competitor:1289',
        name: 'FC Midtjylland',
      },
      position: 18,
    },
    'sr:player:135263': {
      value: 10,
      player: {
        id: 'sr:player:135263',
        name: 'Lucas Andersen',
      },
      team: {
        id: 'sr:competitor:1290',
        name: 'Aalborg BK',
      },
      position: 6,
    },
    'sr:player:81485': {
      value: 7,
      player: {
        id: 'sr:player:81485',
        name: 'Hallur Hansson',
      },
      team: {
        id: 'sr:competitor:1297',
        name: 'AC Horsens',
      },
      position: 18,
    },
    'sr:player:95446': {
      value: 10,
      player: {
        id: 'sr:player:95446',
        name: 'Bashkim Kadrii',
      },
      team: {
        id: 'sr:competitor:1286',
        name: 'Odense Boldklub',
      },
      position: 6,
    },
    'sr:player:121263': {
      value: 8,
      player: {
        id: 'sr:player:121263',
        name: 'Jakob Ankersen',
      },
      team: {
        id: 'sr:competitor:1291',
        name: 'AGF Aarhus',
      },
      position: 12,
    },
    'sr:player:30178': {
      value: 22,
      player: {
        id: 'sr:player:30178',
        name: "Dame N'Doye",
      },
      team: {
        id: 'sr:competitor:1284',
        name: 'FC Copenhagen',
      },
      position: 2,
    },
    'sr:player:46130': {
      value: 20,
      player: {
        id: 'sr:player:46130',
        name: 'Kamil Wilczek',
      },
      team: {
        id: 'sr:competitor:1281',
        name: 'Brondby IF',
      },
      position: 4,
    },
    'sr:player:116662': {
      value: 7,
      player: {
        id: 'sr:player:116662',
        name: 'Nicklas Helenius',
      },
      team: {
        id: 'sr:competitor:1286',
        name: 'Odense Boldklub',
      },
      position: 18,
    },
    'sr:player:286923': {
      value: 8,
      player: {
        id: 'sr:player:286923',
        name: 'Saba Lobzhanidze',
      },
      team: {
        id: 'sr:competitor:1301',
        name: 'Randers FC',
      },
      position: 12,
    },
  },
}

storiesOf('PlayerStandings', module).add('default', () => (
  <PlayerStandings
    players={TEST_PLAYER_STANDINGS[SeasonPlayerStandingType.TopAssists]}
    type={SeasonPlayerStandingType.TopAssists}
  />
))

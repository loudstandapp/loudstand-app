import React from 'react'
import {
  TouchableOpacity,
  View,
  Image,
  Text,
  ImageSourcePropType,
  StyleSheet,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Color from 'color'
import fonts from '../../../theme/fonts'
import { spacingUnit } from '../../../theme/layout'

interface Props {
  title: string
  subTitle: string
  backgroundColors: string[]
  color: string
  icon: ImageSourcePropType
  onPress(): void
}

const IMAGE_SIZE = 24

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  background: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentWrapper: {
    alignItems: 'center',
  },
  imageWrapper: {
    padding: IMAGE_SIZE / 3,
    borderRadius: IMAGE_SIZE,
    borderWidth: 3,
    margin: spacingUnit / 2,
  },
  title: {},
  subTitle: {},
})

export default function TileButton({
  title,
  subTitle,
  backgroundColors,
  color,
  icon,
  onPress,
}: // FIXME: maybe make smart calculation on colors
Props) {
  const borderColor = Color(backgroundColors[0])
    .mix(Color(color).alpha(0.5), 0.3)
    .toString()

  const backgroundColor = Color(backgroundColors[0])
    .mix(Color(color).alpha(0.5), 0.7)
    .toString()

  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <LinearGradient
        colors={backgroundColors}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        style={styles.background}
      >
        <View style={styles.contentWrapper}>
          <View style={[styles.imageWrapper, { borderColor, backgroundColor }]}>
            <Image
              style={[
                {
                  tintColor: color,
                },
              ]}
              source={icon}
              width={IMAGE_SIZE}
              height={IMAGE_SIZE}
            />
          </View>
          <Text
            numberOfLines={1}
            style={[styles.title, fonts.TopBarTitle, { color }]}
          >
            {title}
          </Text>
          <Text
            numberOfLines={1}
            style={[styles.subTitle, fonts.Body, { color }]}
          >
            {subTitle}
          </Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  )
}

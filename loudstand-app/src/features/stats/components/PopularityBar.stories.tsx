import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { View } from 'react-native'
import PopularityBar from './PopularityBar'

storiesOf('PopularityBar', module)
  .addDecorator(getStory => (
    <View
      style={{
        width: 70,
        margin: 100,
        // , alignItems: 'center', justifyContent: 'center'
      }}
    >
      {getStory()}
    </View>
  ))
  .add('0.0', () => <PopularityBar value={0.0} />)
  .add('0.3', () => <PopularityBar value={0.3} />)
  .add('0.7', () => <PopularityBar value={0.7} />)
  .add('1.0', () => <PopularityBar value={1.0} />)

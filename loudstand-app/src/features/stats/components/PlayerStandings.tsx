import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Image,
} from 'react-native'
import {
  SeasonPlayerStanding,
  ID,
  SeasonPlayerStandingType,
  LocalizedTeams,
} from '@loudstand/shared'
import { getComparatorForKey, localizeTeam } from '../../../utils'
import { ListSeparator } from '../../../components/ListSeparator'
import { spacingUnit, marginDefault, headerHeight } from '../../../theme/layout'
import colors from '../../../theme/colors'
import fonts from '../../../theme/fonts'
import { translate } from '../../../translation'

interface Props {
  players: { [PlayerId in ID]: SeasonPlayerStanding }
  type: SeasonPlayerStandingType
  onSelectPlayer?: (id: ID) => void
  localizedTeams?: LocalizedTeams
}

const styles = StyleSheet.create({
  tableHeader: {
    flexDirection: 'row',
    paddingVertical: spacingUnit / 2, // fixme
    paddingHorizontal: marginDefault,
    backgroundColor: colors.secondary,
    justifyContent: 'space-around',
    marginHorizontal: marginDefault,
    marginTop: spacingUnit,
    height: headerHeight,
    borderRadius: headerHeight / 2,
    alignItems: 'center',
  },
  list: {
    paddingVertical: spacingUnit,
  },
  tableRow: {
    flexDirection: 'row',
    paddingHorizontal: marginDefault, // fixme
    justifyContent: 'space-around',
    marginVertical: spacingUnit / 2,
    marginHorizontal: marginDefault,
  },
  positionCol: {
    width: 30,
    textAlign: 'center',
    marginRight: spacingUnit / 2,
  },
  valueCol: {
    width: 30,
    textAlign: 'center',
  },
  nameCol: {
    flex: 1,
  },
  teamCol: {
    flex: 1,
  },
  headerText: {
    ...(fonts.BodySmall as object),
    color: colors.secondaryText,
  },
  rowText: {
    ...(fonts.Body as object),
    color: colors.primaryText,
  },
  valueText: {
    ...(fonts.BodyStrong as object),
    color: colors.primaryText,
  },
  container: {
    flex: 1,
    // height: '100%',
  },
  yellowRedCardsTypeHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
})

const getTypeHeader = (type: SeasonPlayerStandingType) => {
  const map = {
    [SeasonPlayerStandingType.TopPoints]: (
      <Text style={styles.headerText}>
        {translate(`tournamentPlayerStandings.types.${type}.short`)}
      </Text>
    ),
    [SeasonPlayerStandingType.TopGoals]: (
      <Text style={styles.headerText}>
        {translate(`tournamentPlayerStandings.types.${type}.short`)}
      </Text>
    ),
    [SeasonPlayerStandingType.TopAssists]: (
      <Text style={styles.headerText}>
        {translate(`tournamentPlayerStandings.types.${type}.short`)}
      </Text>
    ),
    [SeasonPlayerStandingType.TopOwnGoals]: (
      <Text style={styles.headerText}>
        {translate(`tournamentPlayerStandings.types.${type}.short`)}
      </Text>
    ),
    [SeasonPlayerStandingType.TopRedCards]: (
      <Image
        source={require('../../../../assets/icons/indicators/red-card.png')}
      />
    ),
    [SeasonPlayerStandingType.TopYellowCards]: (
      <Image
        source={require('../../../../assets/icons/indicators/yellow-card.png')}
      />
    ),
    [SeasonPlayerStandingType.TopYellowRedCards]: (
      <View style={styles.yellowRedCardsTypeHeader}>
        <Image
          source={require('../../../../assets/icons/indicators/red-card.png')}
        />
        <Image
          source={require('../../../../assets/icons/indicators/yellow-card.png')}
        />
      </View>
    ),
  }
  return map[type]
}
export default class PlayerStandings extends React.PureComponent<Props> {
  static defaultProps = {
    topInset: 0,
  }

  render = () => {
    const { players, onSelectPlayer, type, localizedTeams } = this.props
    const data = Object.keys(players)
      .map(playerId => players[playerId])
      .sort(getComparatorForKey('position'))

    return (
      <View style={styles.container}>
        <View style={styles.tableHeader}>
          <Text style={[styles.positionCol, styles.headerText]}>
            {translate('tournamentPlayerStandings.position')}
          </Text>
          <Text style={[styles.nameCol, styles.headerText]}>
            {translate('tournamentPlayerStandings.player')}
          </Text>
          <Text style={[styles.teamCol, styles.headerText]}>
            {translate('tournamentPlayerStandings.team')}
          </Text>
          <View style={styles.valueCol}>{getTypeHeader(type)}</View>
        </View>
        <FlatList
          contentContainerStyle={styles.list}
          keyExtractor={item => item.player.id}
          ItemSeparatorComponent={ListSeparator}
          renderItem={({
            item,
            index,
          }: {
            item: SeasonPlayerStanding
            index: number
          }) => (
            <TouchableOpacity
              key={index}
              onPress={() => onSelectPlayer && onSelectPlayer(item.player.id)}
            >
              <View style={styles.tableRow}>
                <Text style={[styles.rowText, styles.positionCol]}>
                  {item.position}
                </Text>
                <Text style={[styles.rowText, styles.nameCol]}>
                  {item.player.name}
                </Text>
                <Text style={[styles.rowText, styles.teamCol]}>
                  {localizeTeam(item.team, localizedTeams)}
                </Text>
                <Text style={[styles.valueCol, styles.valueText]}>
                  {item.value}
                </Text>
              </View>
            </TouchableOpacity>
          )}
          data={data}
        />
      </View>
    )
  }
}

/* eslint-disable import/no-extraneous-dependencies */
import React from 'react'
import { storiesOf } from '@storybook/react-native'
import ProfileTopBar from './ProfileTopBar'

const data = [
  { label: 'Label1', value: '12312' },
  { label: 'Label 2', value: 'some value' },
  { label: 'sh', value: '2342432' },
  { label: 'Long long label', value: '323' },
]

storiesOf('ProfileTopBar', module)
  // .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('default', () => <ProfileTopBar data={data} />)
  .add('one item', () => <ProfileTopBar data={data.slice(0, 1)} />)
  .add('two items', () => <ProfileTopBar data={data.slice(0, 2)} />)
  .add('three items', () => <ProfileTopBar data={data.slice(0, 3)} />)
  .add('too many items', () => <ProfileTopBar data={[...data, ...data]} />)
  .add('no data', () => <ProfileTopBar data={[]} />)

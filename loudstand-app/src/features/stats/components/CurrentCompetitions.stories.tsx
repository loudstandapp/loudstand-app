import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { SeasonBasic } from '@loudstand/shared'
import CurrentCompetitions from './CurrentCompetitions'

const TEST_SEASONS: SeasonBasic[] = [
  {
    name: 'Premier League 19/20',
    id: 'sr:season:66441',
  },
  {
    name: 'Serie A 19/20',
    id: 'sr:season:67061',
  },
  {
    name: 'Primeira Liga 19/20',
    id: 'sr:season:67257',
  },
  {
    name: 'Segunda Liga 19/20',
    id: 'sr:season:67259',
  },
  {
    name: 'Ligue 1 19/20',
    id: 'sr:season:66881',
  },
  {
    name: 'Bundesliga 19/20',
    id: 'sr:season:66509',
  },
  {
    name: 'Eredivisie 19/20',
    id: 'sr:season:67181',
  },
  {
    name: 'Superliga 19/20',
    id: 'sr:season:66803',
  },
  {
    name: 'UEFA Europa League 19/20',
    id: 'sr:season:68134',
  },
]

storiesOf('CurrentCompetitions', module)
  .add('3', () => <CurrentCompetitions seasons={TEST_SEASONS.slice(0, 3)} />)
  .add('1', () => <CurrentCompetitions seasons={TEST_SEASONS.slice(0, 1)} />)
  .add('9', () => <CurrentCompetitions seasons={TEST_SEASONS} />)

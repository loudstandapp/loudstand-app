/* eslint-disable react/no-array-index-key */

import React, { Fragment } from 'react'
import { View, Text, StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { LabelValuePair } from '@loudstand/shared'
import colors from '../../../theme/colors'
import { spacingUnit } from '../../../theme/layout'
import fonts from '../../../theme/fonts'

interface Props {
  data: LabelValuePair[]
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingHorizontal: 2 * spacingUnit,
  },
  itemContainer: {
    padding: spacingUnit,
    maxWidth: '33%',
    alignItems: 'center',
  },
  text: {
    color: colors.white,
  },
  separator: {
    marginVertical: spacingUnit * 1.5,
    width: StyleSheet.hairlineWidth,
    backgroundColor: colors.white,
  },
})

export default function ProfileTopBar({ data }: Props) {
  return (
    <LinearGradient
      style={styles.container}
      colors={[colors.secondary, colors.primaryAlt]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 0 }}
    >
      {data.map((item, index) => (
        <Fragment key={index}>
          {index > 0 && <View style={styles.separator} />}
          <View style={styles.itemContainer}>
            <Text style={[fonts.HeaderSmall, styles.text]}>{item.label}</Text>
            <Text numberOfLines={1} style={[fonts.Header, styles.text]}>
              {item.value}
            </Text>
          </View>
        </Fragment>
      ))}
    </LinearGradient>
  )
}

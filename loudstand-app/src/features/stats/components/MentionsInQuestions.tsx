import React from 'react'
import {
  ISO8601Date,
  Question,
  QuestionResultsSummaryDocument,
  UserVoteHistory,
  generateVoteKey,
  VoteType,
  QuestionResponseType,
  SubmittedVote,
  ISO8601Timestamp,
} from '@loudstand/shared'
import { StyleSheet, View, Text, FlatList, ListRenderItem } from 'react-native'
import { translate, currentLocale } from '../../../translation'
import fonts from '../../../theme/fonts'
import QuestionItem from '../../main/components/QuestionItem'
import { questionVote } from '../../match/voting'
import { spacingUnit } from '../../../theme/layout'
import colors from '../../../theme/colors'
import { SubmittedVotes, ChosenTeamList } from '../../../store/model'
import { cheats } from '../../../utils'

interface Props {
  questions: { [key in ISO8601Date]: Question }
  questionResults?: QuestionResultsSummaryDocument
  voteHistory?: UserVoteHistory
  onVote: (vote: SubmittedVote) => void
  submittedVotes?: SubmittedVotes
  availableTeams: ChosenTeamList
  topInset: number
}

const formatter = new Intl.DateTimeFormat(currentLocale.languageTag, {
  day: 'numeric',
  month: 'long',
  year: 'numeric',
})

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    ...(fonts.HeaderSmall as object),
    textAlign: 'center',
    marginVertical: spacingUnit,
  },
  questionWrapper: {
    marginVertical: spacingUnit,
  },
  questionDateWrapper: {
    alignItems: 'flex-end',
  },
  questionDate: {
    borderRadius: spacingUnit,
    backgroundColor: colors.secondary,
    margin: spacingUnit,
  },
  questionDateText: {
    color: colors.secondaryText,
    ...(fonts.CustomFullDate as object),
    paddingVertical: spacingUnit / 2,
    paddingHorizontal: 2 * spacingUnit,
  },
})

export default class MentionsInQuestions extends React.PureComponent<Props> {
  static defaultProps = {
    topInset: 0,
  }

  handleQuestionResponse = (
    question: Question,
    response: QuestionResponseType
  ) => {
    const { onVote } = this.props
    const { id, teamId, playerId } = question
    const vote = questionVote({
      questionId: id,
      teamId,
      playerId,
      answer: response,
    })
    onVote(vote)
  }

  renderQuestionForDate: ListRenderItem<ISO8601Date> = ({ item: date }) => {
    const {
      questions,
      questionResults,
      voteHistory,
      submittedVotes,
      availableTeams,
    } = this.props
    const question = questions && questions[date]
    if (!question) {
      return null
    }
    const questionVoteKey =
      question &&
      generateVoteKey(VoteType.Question, [{ question: question.id }])
    const hasBeenAnswered =
      !!voteHistory && !!questionVoteKey && !!voteHistory[questionVoteKey]
    const hasBeenSubmitted =
      !!submittedVotes && !!questionVoteKey && !!submittedVotes[questionVoteKey]
    const chosenOption =
      voteHistory &&
      voteHistory[questionVoteKey] &&
      voteHistory[questionVoteKey].response
    return (
      <View style={styles.questionWrapper}>
        <View style={styles.questionDateWrapper}>
          <View style={styles.questionDate}>
            <Text style={styles.questionDateText}>
              {formatter.format(new Date(date))}
            </Text>
          </View>
        </View>
        <QuestionItem
          availableTeams={availableTeams}
          question={question}
          onResponse={this.handleQuestionResponse}
          hasBeenAnswered={hasBeenAnswered}
          result={questionResults && questionResults[question.id]}
          chosen={chosenOption}
          hasBeenSubmitted={hasBeenSubmitted}
        />
      </View>
    )
  }

  render = () => {
    const { questions, topInset } = this.props
    const now = new Date()
    const visibleQuestionDates: { [key in ISO8601Date]: Question } | undefined =
      questions &&
      Object.keys(questions).reduce((acc, date: ISO8601Timestamp) => {
        const question = questions[date]
        if (!cheats.alwaysAllowVoting && new Date(question.publishTime) > now) {
          return acc
        }
        return {
          ...acc,
          [date]: question,
        }
      }, {})

    return (
      <View style={styles.container}>
        <FlatList
          contentContainerStyle={{ paddingTop: topInset }}
          ListHeaderComponent={() => (
            <Text style={styles.header}>
              {translate('playerProfile.mentionsInQuestions')}
            </Text>
          )}
          data={Object.keys(visibleQuestionDates)
            .sort()
            .reverse()}
          renderItem={this.renderQuestionForDate}
          keyExtractor={item => item}
        />
      </View>
    )
  }
}

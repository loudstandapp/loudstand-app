import React from 'react'
import {
  SectionList,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  SectionListRenderItemInfo,
  Image,
} from 'react-native'
import {
  SeasonStandings,
  TeamListItem,
  StandingItem,
  ID,
  LocalizedTeams,
  SeasonTeamPopularityDocument,
} from '@loudstand/shared'
import { getComparatorForKey, localizeTeam } from '../../../utils'
import { ListSeparator } from '../../../components/ListSeparator'
import { spacingUnit, marginDefault, headerHeight } from '../../../theme/layout'
import colors from '../../../theme/colors'
import fonts from '../../../theme/fonts'
import { translate } from '../../../translation'
import PopularityBar from './PopularityBar'

interface Props {
  standingGroups: SeasonStandings
  playingTeams: TeamListItem[]
  localizedTeams?: LocalizedTeams
  onSelectTeam?: (id: ID) => void
  teamPopularity?: SeasonTeamPopularityDocument
  topInset: number
}

function getSectionContent(standingGroups: SeasonStandings) {
  return Object.values(standingGroups)
    .sort(getComparatorForKey('name'))
    .map(group => ({
      title: group.name,
      data: Object.values(group.standings).sort(
        getComparatorForKey('position')
      ),
    }))
}

const styles = StyleSheet.create({
  playingRow: {
    backgroundColor: colors.secondary,
    marginVertical: 0,
    marginHorizontal: 0,
    paddingHorizontal: 2 * marginDefault,
    paddingVertical: spacingUnit,
    shadowColor: colors.primary,
    shadowRadius: 2,
    shadowOpacity: 1,
    shadowOffset: { width: 0, height: 0 },
  },
  playingText: {
    color: colors.secondaryText,
  },
  playingLeadingSeparator: {
    height: 0,
    margin: 0,
  },
  playingTrailingSeparator: {
    height: 0,
    margin: 0,
  },
  tableHeader: {
    flexDirection: 'row',
    paddingVertical: spacingUnit / 2, // fixme
    paddingHorizontal: marginDefault,
    backgroundColor: colors.secondary,
    justifyContent: 'space-around',
    marginTop: spacingUnit,
    marginHorizontal: marginDefault,
    height: headerHeight,
    borderRadius: headerHeight / 2,
    alignItems: 'center',
  },
  list: {
    paddingVertical: spacingUnit,
  },
  tableRow: {
    flexDirection: 'row',
    paddingHorizontal: marginDefault, // fixme
    justifyContent: 'space-around',
    marginVertical: spacingUnit / 2,
    marginHorizontal: marginDefault,
    alignItems: 'center',
  },
  positionCol: {
    width: 30,
    textAlign: 'center',
    marginRight: spacingUnit / 2,
  },
  valueCol: {
    width: 30,
    textAlign: 'center',
  },
  largeValueCol: {
    width: 50,
    textAlign: 'center',
  },
  loudstandIndicator: {
    alignItems: 'center',
  },
  changeCol: {
    width: 16,
    paddingRight: spacingUnit / 2,
    justifyContent: 'center',
  },
  nameCol: {
    flex: 1,
  },
  teamCol: {
    flex: 1,
  },
  headerText: {
    ...(fonts.BodySmall as object),
    color: colors.secondaryText,
  },
  rowText: {
    ...(fonts.Body as object),
    color: colors.primaryText,
  },
  pointsCol: {
    ...(fonts.BodyStrong as object),
  },
  sectionHeaderText: {
    ...(fonts.HeaderSmall as object),
    textAlign: 'center',
    marginVertical: marginDefault,
  },
  container: {
    flex: 1,
  },
})

function getChangeIndicator(change: number) {
  if (change < 0) {
    return {
      file: require('../../../../assets/icons/indicators/league-down.png'),
      color: colors.invalid,
    }
  }
  if (change > 0) {
    return {
      file: require('../../../../assets/icons/indicators/league-up.png'),
      color: colors.valid,
    }
  }
  return {
    file: require('../../../../assets/icons/indicators/league-stay.png'),
    color: colors.secondary,
  }
}

export default class Standings extends React.PureComponent<Props> {
  static defaultProps = {
    playingTeams: [],
    topInset: 0,
  }

  renderSeparator = ({
    leadingItem,
    trailingItem,
  }: {
    leadingItem?: StandingItem
    trailingItem?: StandingItem
  }) => {
    const { playingTeams } = this.props
    if (playingTeams.length === 0) {
      return <ListSeparator />
    }
    const playingTeamIds = playingTeams.map(team => team.id)

    if (leadingItem && playingTeamIds.includes(leadingItem.team.id)) {
      return null
    }
    if (trailingItem && playingTeamIds.includes(trailingItem.team.id)) {
      return null
    }
    return <ListSeparator />
  }

  render = () => {
    const {
      standingGroups,
      playingTeams,
      onSelectTeam,
      localizedTeams,
      teamPopularity,
      topInset,
    } = this.props
    const playingTeamIds = playingTeams.map(team => team.id)
    const sections = getSectionContent(standingGroups)
    return (
      <View style={[styles.container, { paddingTop: topInset }]}>
        <View style={styles.tableHeader}>
          <Text style={[styles.positionCol, styles.headerText]}>
            {translate('tournamentStandings.position')}
          </Text>
          <Text style={[styles.changeCol, styles.headerText]} />
          <Text style={[styles.teamCol, styles.headerText]}>
            {translate('tournamentStandings.team')}
          </Text>
          <Text style={[styles.valueCol, styles.headerText]}>
            {translate('tournamentStandings.playedMatches')}
          </Text>
          <Text style={[styles.largeValueCol, styles.headerText]}>
            {translate('tournamentStandings.goalDifference')}
          </Text>
          {teamPopularity && (
            <View style={[styles.largeValueCol, styles.loudstandIndicator]}>
              <Image
                source={require('../../../../assets/icons/indicators/very-small-loudstand-green-icon-scarf.png')}
              />
            </View>
          )}
          <Text style={[styles.valueCol, styles.headerText]}>
            {translate('tournamentStandings.points')}
          </Text>
        </View>
        <SectionList
          contentContainerStyle={styles.list}
          ItemSeparatorComponent={this.renderSeparator}
          stickySectionHeadersEnabled={false}
          keyExtractor={item => item.team.id}
          renderSectionHeader={({ section: { title } }) =>
            title ? (
              <Text style={styles.sectionHeaderText}>{title}</Text>
            ) : (
              <></>
            )
          }
          renderItem={({
            item,
            index,
          }: SectionListRenderItemInfo<StandingItem>) => {
            const isPlaying = playingTeamIds.includes(item.team.id)
            const { file, color } = getChangeIndicator(item.change)
            const popularity =
              (teamPopularity &&
                teamPopularity[item.team.id] &&
                teamPopularity[item.team.id].value) ||
              0
            return (
              <TouchableOpacity
                key={index}
                onPress={() => onSelectTeam && onSelectTeam(item.team.id)}
                disabled={!onSelectTeam}
              >
                <View style={[styles.tableRow, isPlaying && styles.playingRow]}>
                  <Text
                    style={[
                      styles.rowText,
                      styles.positionCol,
                      isPlaying && styles.playingText,
                    ]}
                  >
                    {item.position}
                  </Text>
                  <View style={styles.changeCol}>
                    <Image style={{ tintColor: color }} source={file} />
                  </View>
                  <Text
                    style={[
                      styles.rowText,
                      styles.teamCol,
                      isPlaying && styles.playingText,
                    ]}
                  >
                    {localizeTeam(item.team, localizedTeams)}
                  </Text>
                  <Text
                    style={[
                      styles.rowText,
                      styles.valueCol,
                      isPlaying && styles.playingText,
                    ]}
                  >
                    {item.playedMatches}
                  </Text>
                  <Text
                    style={[
                      styles.rowText,
                      styles.largeValueCol,
                      isPlaying && styles.playingText,
                    ]}
                  >
                    {`${item.goalDifference.for}:${
                      item.goalDifference.against
                    }`}
                  </Text>
                  {teamPopularity && (
                    <PopularityBar
                      value={popularity / 5}
                      style={styles.largeValueCol}
                    />
                  )}
                  <Text
                    style={[
                      styles.rowText,
                      styles.valueCol,
                      styles.pointsCol,
                      isPlaying && styles.playingText,
                    ]}
                  >
                    {item.points}
                  </Text>
                </View>
              </TouchableOpacity>
            )
          }}
          sections={sections}
        />
      </View>
    )
  }
}

import { MatchListItem, TeamListItem } from '@loudstand/shared'
import { Navigation } from 'react-native-navigation'
import { Dispatch } from 'redux'
import { Platform } from 'react-native'
import { NavigationProps as HomeScreenNavigationProps } from './screens/HomeScreen'
import { matchScreenNavigationProps } from '../match/navigation'
import { showSearchOverlay } from '../../components/navigation'
import { ScreenNames } from '../../registerScreens'
import { statsScreenNavigationProps } from '../stats/navigation'
import {
  fansListNavigationProps,
  settingsNavigationProps,
} from '../user/navigation'
import { translate } from '../../translation'
import colors from '../../theme/colors'
import { setTeam, countLaunch } from '../../store/actions'
import { notificationPermissionThunk } from '../../services/device'
import { randomComponentId } from '../../utils'

export const homeScreenNavigationProps = (
  mainComponentId: string
): HomeScreenNavigationProps => ({
  onSelectMatch: (match: MatchListItem) => {
    const componentId = randomComponentId(ScreenNames.MatchScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.MatchScreen,
        id: componentId,
        passProps: matchScreenNavigationProps(componentId, match),
      },
    })
  },
  onSelectSearch: () => showSearchOverlay({}),
  onSelectSettings: () => {
    const componentId = randomComponentId(ScreenNames.SettingsScreen)
    return Navigation.push(mainComponentId, {
      component: {
        name: ScreenNames.SettingsScreen,
        id: componentId,
        passProps: settingsNavigationProps(componentId),
      },
    })
  },
})

enum Tabs {
  Stats = 1,
  Home = 0,
  Community = 2,
}

const DEFAULT_TAB = Tabs.Home

const defaultTabOptions = {
  iconColor: 'grey',
  selectedIconColor: 'white',
  textColor: 'grey',
  selectedTextColor: 'white',
}

const setRoot = () => {
  const homeScreenComponentId = randomComponentId(ScreenNames.HomeScreen)
  const statsScreenComponentId = randomComponentId(ScreenNames.StatsScreen)
  const fansListScreenComponentId = randomComponentId(
    ScreenNames.FansListScreen
  )

  Navigation.setRoot({
    root: {
      bottomTabs: {
        children: [
          {
            stack: {
              children: [
                {
                  component: {
                    id: homeScreenComponentId,
                    name: ScreenNames.HomeScreen,
                    passProps: homeScreenNavigationProps(homeScreenComponentId),
                    options: {
                      bottomTab: {
                        ...defaultTabOptions,
                        icon: require('../../../assets/icons/navigation/calendar.png'),
                        text: translate('topNavigation.home'),
                      },
                    },
                  },
                },
              ],
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    id: statsScreenComponentId,
                    name: ScreenNames.StatsScreen,
                    passProps: statsScreenNavigationProps(
                      statsScreenComponentId
                    ),
                  },
                },
              ],
              options: {
                bottomTab: {
                  ...defaultTabOptions,
                  icon: require('../../../assets/icons/navigation/statistics.png'),
                  text: translate('topNavigation.statistics'),
                },
              },
            },
          },
          {
            stack: {
              children: [
                {
                  component: {
                    id: fansListScreenComponentId,
                    name: ScreenNames.FansListScreen,
                    passProps: fansListNavigationProps(
                      fansListScreenComponentId
                    ),
                  },
                },
              ],
              options: {
                bottomTab: {
                  ...defaultTabOptions,
                  icon: require('../../../assets/icons/navigation/fans-list.png'),
                  text: translate('topNavigation.community'),
                },
              },
            },
          },
        ],
        options: {
          bottomTabs: {
            currentTabIndex: DEFAULT_TAB,
            titleDisplayMode: 'alwaysShow',
            backgroundColor: colors.navigationBackground,
          },
        },
      },
    },
  })
}

export function startMainAppThunk() {
  return async (dispatch: Dispatch) => {
    dispatch(countLaunch())
    // @ts-ignore dispatch not understanding thunks
    dispatch(notificationPermissionThunk())
    await setRoot()
    if (Platform.OS === 'android') {
      Navigation.events().registerBottomTabSelectedListener(
        async ({ selectedTabIndex, unselectedTabIndex }) => {
          if (
            selectedTabIndex === Tabs.Stats &&
            unselectedTabIndex === Tabs.Stats
          ) {
            await Navigation.popToRoot(ScreenNames.StatsScreen)
          }
        }
      )
    }
  }
}

export function selectTeamThunk(team: TeamListItem) {
  return async (dispatch: Dispatch) => {
    dispatch(setTeam(team))
    // Reload everything in order to make sure everything is loaded
    await setRoot()
  }
}

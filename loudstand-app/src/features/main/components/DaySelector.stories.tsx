import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { View } from 'react-native'
import { withKnobs, select } from '@storybook/addon-knobs'
import DaySelector from './DaySelector'
import { generateDateList } from '../screens/HomeScreen'

const dateList1 = generateDateList('2018-08-12', '2019-06-10')
const date1 = '2019-05-14'
const dateList2 = generateDateList('2018-08-15', '2019-04-30')
const date2 = '2019-04-30'

const options = {
  Set1: {
    allDates: dateList1,
    selectedIndex: dateList1.indexOf(date1),
    onChange: (index: number) => action('onChange')(dateList1[index]),
  },
  Set2: {
    allDates: dateList2,
    selectedIndex: dateList2.indexOf(date2),
    onChange: (index: number) => action('onChange')(dateList2[index]),
  },
}

const allDates = {
  dateList1,
  dateList2,
}

const dates = {
  [date1]: dateList1.indexOf(date1),
  [date2]: dateList2.indexOf(date2),
}

storiesOf('DaySelector', module)
  .addDecorator(getStory => (
    <View
      style={{ backgroundColor: 'black', flex: 1, justifyContent: 'center' }}
    >
      {getStory()}
    </View>
  ))
  .addDecorator(withKnobs)
  .add('default', () => (
    <DaySelector
      allDates={dateList1}
      selectedIndex={dateList1.indexOf(date1)}
      onChange={action('onChange')}
    />
  ))
  .add('no dates', () => (
    <DaySelector
      allDates={[]}
      selectedIndex={0}
      onChange={action('onChange')}
    />
  ))
  .add('pick set', () => (
    <DaySelector {...select('Set', options, options.Set1, 'GNAA')} />
  ))
  .add('pick individual', () => (
    <DaySelector
      allDates={select('Set', allDates, allDates.dateList1, 'GNAA')}
      selectedIndex={select('DateIndex', dates, dates[date1], 'GNAA')}
      onChange={action('onChange')}
    />
  ))

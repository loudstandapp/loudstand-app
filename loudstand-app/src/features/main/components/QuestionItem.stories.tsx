import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { Question, QuestionResponseType } from '@loudstand/shared'
import { action } from '@storybook/addon-actions'
import QuestionItem from './QuestionItem'
import { ChosenTeamList } from '../../../store/model'

const FUTURE_DATE = new Date(Date.now() + 60 * 60 * 1000).toISOString()
const PAST_DATE = new Date(Date.now() - 60 * 60 * 1000).toISOString()

const AVAILABLE_TEAMS: ChosenTeamList = {
  1231231231: {
    name: 'Brøndby IF',
    id: '1231231231',
    country: 'DK',
    isFavorite: true,
  },
}

const TEST_QUESTION_NO_PLAYER: Question = {
  date: '2013-01-23',
  id: '12345678',
  question: {
    en: 'Do you like {Brøndby IF} 100%?',
  },
  teamId: '1231231231',
  playerId: null,
  publishTime: '1900-01-01',
  reactionEndTime: FUTURE_DATE,
  imageUrl: null,
}

const TEST_QUESTION_WITH_PLAYER: Question = {
  date: '2013-01-23',
  id: '12345678',
  question: {
    en: 'Do you prefer {BRIAN LAUDRUP} to stop his carrer?',
  },
  imageUrl:
    'https://blueswww.blob.core.windows.net/cache/0/e/8/7/b/f/0e87bf95989e4df92b048b818b9d52d6787b1828.jpg',
  teamId: '1231231231',
  playerId: '1231231231',
  publishTime: '1900-01-01',
  reactionEndTime: FUTURE_DATE,
}

const TEST_QUESTION_WITH_LONG_PLAYER: Question = {
  date: '2013-01-23',
  id: '12345678',
  question: {
    en:
      'Do you prefer {Brian Laudrup With A Very long name} to stop his carrer?',
  },
  imageUrl:
    'https://d3nfwcxd527z59.cloudfront.net/content/uploads/2019/05/21081510/Brahim-Diaz-Real-Madrid-1.jpg',
  teamId: '1231231231',
  playerId: '1231231231',
  publishTime: '1900-01-01',
  reactionEndTime: FUTURE_DATE,
}
const TEST_QUESTION_LONG_TEXT: Question = {
  date: '2013-01-23',
  id: '12345678',
  question: {
    en:
      'Do you like {Brøndby IF} 100% or only 10% or maybe 20% or some other number that makes this long?',
  },
  imageUrl:
    'https://blueswww.blob.core.windows.net/cache/0/e/8/7/b/f/0e87bf95989e4df92b048b818b9d52d6787b1828.jpg',
  teamId: '1231231231',
  publishTime: '1900-01-01',
  reactionEndTime: FUTURE_DATE,
  playerId: null,
}

const TEST_RESPONSE_50_50 = {
  [QuestionResponseType.Yes]: 0.5,
  [QuestionResponseType.No]: 0.5,
}

const TEST_RESPONSE_100_0 = {
  [QuestionResponseType.Yes]: 1,
  [QuestionResponseType.No]: 0,
}

const TEST_RESPONSE_0_100 = {
  [QuestionResponseType.Yes]: 0,
  [QuestionResponseType.No]: 1,
}

const TEST_RESPONSE_10_90 = {
  [QuestionResponseType.Yes]: 0.1,
  [QuestionResponseType.No]: 0.9,
}
const TEST_RESPONSE_90_10 = {
  [QuestionResponseType.Yes]: 0.9,
  [QuestionResponseType.No]: 0.1,
}

const TEST_RESPONSE_80_20 = {
  [QuestionResponseType.Yes]: 0.8,
  [QuestionResponseType.No]: 0.2,
}

storiesOf('QuestionItem', module)
  .add('non favorite team', () => (
    <QuestionItem
      question={TEST_QUESTION_NO_PLAYER}
      hasBeenAnswered={false}
      result={TEST_RESPONSE_0_100}
      onResponse={action('onResponse')}
      availableTeams={{}}
    />
  ))
  .add('expired', () => (
    <QuestionItem
      question={{ ...TEST_QUESTION_NO_PLAYER, reactionEndTime: PAST_DATE }}
      hasBeenAnswered={false}
      result={TEST_RESPONSE_0_100}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('highlighted team', () => (
    <QuestionItem
      question={TEST_QUESTION_NO_PLAYER}
      hasBeenAnswered={false}
      result={TEST_RESPONSE_0_100}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('highlighted player', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered={false}
      result={TEST_RESPONSE_0_100}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('highlighted player, no image Url', () => (
    <QuestionItem
      question={{ ...TEST_QUESTION_WITH_PLAYER, imageUrl: null }}
      hasBeenAnswered={false}
      result={TEST_RESPONSE_0_100}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('very long player', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_LONG_PLAYER}
      hasBeenAnswered={false}
      result={TEST_RESPONSE_0_100}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('very long text', () => (
    <QuestionItem
      question={TEST_QUESTION_LONG_TEXT}
      hasBeenAnswered={false}
      result={TEST_RESPONSE_0_100}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('submitted, but not saved', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenSubmitted
      result={TEST_RESPONSE_50_50}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('saved, but no result', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenSubmitted
      hasBeenAnswered
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('answered 50-50', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_50_50}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('answered 100-0', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_100_0}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('answered 0-100', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_0_100}
      availableTeams={AVAILABLE_TEAMS}
      onResponse={action('onResponse')}
    />
  ))
  .add('answered 10_90', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_10_90}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('answered 80_20', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_80_20}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('answered 90_10', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_90_10}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('current yes 100 but chose no', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_100_0}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
      chosen={QuestionResponseType.No}
    />
  ))
  .add('yes 100 none chosen', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_100_0}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))
  .add('current yes 90 but chose no', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_90_10}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
      chosen={QuestionResponseType.No}
    />
  ))
  .add('yes 90 none chosen', () => (
    <QuestionItem
      question={TEST_QUESTION_WITH_PLAYER}
      hasBeenAnswered
      result={TEST_RESPONSE_90_10}
      onResponse={action('onResponse')}
      availableTeams={AVAILABLE_TEAMS}
    />
  ))

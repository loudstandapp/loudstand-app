import React from 'react'
import { MatchListItem, LocalizedTeams } from '@loudstand/shared'
import { TouchableOpacity, View, StyleSheet, Image, Text } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import {
  localizeTeam,
  getImageUrl,
  ItemImageType,
  fallbackImages,
} from '../../../utils'
import { spacingUnit, marginDefault } from '../../../theme/layout'
import colors from '../../../theme/colors'
import { translate } from '../../../translation'
import GenericItem, { DAILY_ITEM_ICON_SIZE } from './GenericItem'
import RemoteImage from '../../../components/RemoteImage'
import { timeFormatter } from './HomeScreenCarouselItem'
import fonts from '../../../theme/fonts'
import { ChosenTeamList } from '../../../store/model'

interface Props {
  match: MatchListItem
  localizedTeams?: LocalizedTeams
  onSelect: (match: MatchListItem) => void
  availableTeams?: ChosenTeamList
}
const logoImageSize = DAILY_ITEM_ICON_SIZE / 1.1

const styles = StyleSheet.create({
  tournamentLogo: {
    width: logoImageSize,
    height: logoImageSize,
    borderRadius: logoImageSize / 2,
  },
  seasonLogoWrapper: {
    marginRight: spacingUnit,
    width: DAILY_ITEM_ICON_SIZE,
    height: DAILY_ITEM_ICON_SIZE,
    backgroundColor: colors.background,
    borderRadius: DAILY_ITEM_ICON_SIZE / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  matchButton: {
    backgroundColor: colors.navigationBackground,
    borderRadius: spacingUnit,
  },
  arrowIndicator: {
    flex: 0,
    marginLeft: spacingUnit,
    tintColor: colors.secondaryLight,
  },
  arrowContainer: {
    position: 'absolute',
    right: spacingUnit,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
  },
  openContainer: {
    alignItems: 'center',
  },
  openButton: {
    height: 3 * spacingUnit,
    paddingHorizontal: 3 * spacingUnit,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: spacingUnit / 2,
    marginHorizontal: marginDefault * 2,
    marginBottom: marginDefault,
    borderRadius: spacingUnit * 1.5,
  },
  openText: {
    ...(fonts.BodyStrong as object),
    textTransform: 'uppercase',
    color: colors.secondaryText,
  },
})

export default class MatchItem extends React.PureComponent<Props> {
  handleSelect = () => {
    const { onSelect, match } = this.props
    onSelect(match)
  }

  renderSeasonLogo = () => {
    const { match } = this.props
    const seasonLogoUrl = getImageUrl(match.season.id, ItemImageType.SeasonLogo)
    return (
      <View style={styles.seasonLogoWrapper}>
        <RemoteImage
          style={styles.tournamentLogo}
          source={{ uri: seasonLogoUrl }}
          defaultSource={fallbackImages.seasonLogo}
        />
      </View>
    )
  }

  render = () => {
    const { match, localizedTeams, availableTeams } = this.props
    const home = localizeTeam(match.teams.home, localizedTeams)
    const away = localizeTeam(match.teams.away, localizedTeams)
    const vs = translate('match.vs')

    const now = new Date().toISOString()
    const isOpenForVoting =
      match.reactionEndTime &&
      match.reactionStartTime &&
      now > match.reactionStartTime &&
      now < match.reactionEndTime

    const isVotable =
      availableTeams &&
      !!Object.values(availableTeams).find(
        team =>
          team.isFavorite &&
          (team.id === match.teams.home.id || team.id === match.teams.away.id)
      )
    return (
      <TouchableOpacity onPress={this.handleSelect} style={styles.matchButton}>
        <LinearGradient
          colors={[
            // colors.navigationBackground,
            colors.fixmeSecondaryBackground,
            colors.navigationBackground,
          ]}
        >
          <GenericItem
            accessoryText={timeFormatter.format(new Date(match.startTime))}
            accessoryView={this.renderSeasonLogo()}
            title={match.tournament.name}
            subject={`${home} ${vs} ${away}`}
            location={match.venue && match.venue.name}
          />
          {isOpenForVoting && isVotable && (
            <View style={styles.openContainer}>
              <LinearGradient
                colors={[colors.primary, colors.primaryAlt]}
                style={styles.openButton}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
              >
                <Text style={styles.openText}>
                  {translate('match.openForVoting')}
                </Text>
              </LinearGradient>
            </View>
          )}
          <View style={styles.arrowContainer}>
            <Image
              style={styles.arrowIndicator}
              source={require('../../../../assets/icons/buttons/front-arrow.png')}
            />
          </View>
        </LinearGradient>
      </TouchableOpacity>
    )
  }
}

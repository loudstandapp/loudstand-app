import React from 'react'
import {
  Question,
  QuestionResult,
  QuestionResponseType,
  ISO639a2Language,
  LocalizedTeams,
  UserVoteHistory,
  generateVoteKey,
  VoteType,
} from '@loudstand/shared'
import { View, StyleSheet, Text } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { currentLocale, fallback } from '../../../translation'
import colors from '../../../theme/colors'
import { marginDefault } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import QuestionOptionsBar from '../../../components/QuestionOptionsBar'
import {
  fallbackImages,
  getImageUrl,
  ItemImageType,
  cheats,
} from '../../../utils'
import RemoteImage from '../../../components/RemoteImage'
import { ChosenTeamList } from '../../../store/model'

interface Props {
  question: Question
  hasBeenAnswered?: boolean
  hasBeenSubmitted?: boolean
  result?: QuestionResult
  localizedTeams?: LocalizedTeams
  chosen?: QuestionResponseType
  voteHistory?: UserVoteHistory
  availableTeams: ChosenTeamList
  onResponse?: (question: Question, response: QuestionResponseType) => void
}

const { languageCode } = currentLocale

function localizeQuestion(question: { [key in ISO639a2Language]: string }) {
  return (
    question[languageCode] ||
    question[fallback.languageTag] ||
    Object.values(question)[0]
  )
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'pink',
  },
  image: {
    height: '100%',
    width: '60%',
    position: 'absolute',
    right: 0,
  },
  imageInnerContainer: {},
  questionText: {
    ...(fonts.CustomQuestionBody as object),
    width: '60%',
    color: colors.secondaryText,
    margin: marginDefault,
  },
  questionHighlightedText: {
    ...(fonts.CustomQuestionTopic as object),
    color: colors.primary,
  },
})

export default class QuestionItem extends React.PureComponent<Props> {
  handleYes = () => {
    const { onResponse, question } = this.props
    onResponse && onResponse(question, QuestionResponseType.Yes)
  }

  handleNo = () => {
    const { onResponse, question } = this.props
    onResponse && onResponse(question, QuestionResponseType.No)
  }

  render = () => {
    const {
      question: { imageUrl, question, id, teamId, playerId, reactionEndTime },
      hasBeenAnswered,
      result,
      voteHistory,
      chosen,
      hasBeenSubmitted,
      availableTeams,
    } = this.props

    const questionText = localizeQuestion(question)

    const voteKey = generateVoteKey(VoteType.Question, [
      {
        question: id,
      },
    ])

    const chosenOption =
      chosen ||
      (voteKey &&
        voteHistory &&
        voteHistory[voteKey] &&
        voteHistory[voteKey].response)

    const matches = questionText.match(/^(.*)\{(.*)\}(.*)$/)
    const textWithHighlights = matches
      ? [
          matches[1],
          <Text key="highlighted" style={styles.questionHighlightedText}>
              {matches[2]}
            </Text>,
          matches[3],
        ]
      : questionText

    const isFavoriteTeam =
      availableTeams[teamId] && availableTeams[teamId].isFavorite

    const hasExpired = reactionEndTime < new Date().toISOString()

    const allowsVoting =
      isFavoriteTeam && (!hasExpired || cheats.alwaysAllowVoting)

    const imageSourceUrl =
      imageUrl ||
      (playerId && getImageUrl(playerId, ItemImageType.Actionshot)) ||
      getImageUrl(teamId, ItemImageType.TeamPhoto)

    const fallbackImage = playerId
      ? fallbackImages.playerActionShot
      : fallbackImages.teamPhoto

    return (
      <View style={styles.container}>
        <RemoteImage
          style={styles.image}
          source={{ uri: imageSourceUrl }}
          defaultSource={fallbackImage}
        />
        <LinearGradient
          colors={[colors.fixmeSecondaryBackground, 'transparent']}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0.5 }}
          locations={[0.5, 0.9]}
        >
          <Text style={styles.questionText}>{textWithHighlights}</Text>
          <QuestionOptionsBar
            allowsVoting={!!allowsVoting}
            result={result}
            onYes={this.handleYes}
            onNo={this.handleNo}
            chosen={chosenOption}
            hasBeenSubmitted={hasBeenSubmitted}
            hasBeenAnswered={hasBeenAnswered}
          />
        </LinearGradient>
      </View>
    )
  }
}

import React, { ReactNode } from 'react'
import { View, Text, StyleSheet, Image, ImageRequireSource } from 'react-native'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'

export const DAILY_ITEM_ICON_SIZE = 25
export const ACCESSORIES_WIDTH = 104

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginHorizontal: marginDefault,
    marginVertical: marginDefault,
  },
  accessories: {
    flexDirection: 'row',
    width: ACCESSORIES_WIDTH,
  },
  icon: {
    marginRight: spacingUnit,
    width: DAILY_ITEM_ICON_SIZE,
    height: DAILY_ITEM_ICON_SIZE,
  },
  details: {
    flex: 1,
    paddingVertical: spacingUnit / 2,
  },
  accessoryText: {
    ...(fonts.BodyBold as object),
    color: colors.secondaryText,
    textTransform: 'uppercase',
    marginRight: spacingUnit,
    textAlign: 'right',
    flex: 1,
  },
  titleText: {
    ...(fonts.Body as object),
    color: colors.secondaryText,
    textTransform: 'uppercase',
    paddingBottom: spacingUnit / 4,
  },
  subjectText: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
    paddingBottom: spacingUnit / 4,
  },
  locationText: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
  },
})

interface Props {
  accessoryText?: string
  accessoryIcon?: ImageRequireSource
  accessoryView?: ReactNode
  title?: string
  subject?: string | null
  location?: string | null
}

export default class GenericItem extends React.PureComponent<Props> {
  render = () => {
    const {
      accessoryIcon,
      accessoryText,
      accessoryView,
      subject,
      title,
      location,
    } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.accessories}>
          <Text style={styles.accessoryText}>{accessoryText}</Text>
          {accessoryIcon ? (
            <Image
              source={accessoryIcon}
              style={styles.icon}
              resizeMode="contain"
            />
          ) : (
            accessoryView
          )}
        </View>

        <View style={styles.details}>
          <Text numberOfLines={1} style={styles.titleText}>
            {title}
          </Text>
          <Text style={styles.subjectText}>{subject}</Text>
          <Text style={styles.locationText}>{location}</Text>
        </View>
      </View>
    )
  }
}

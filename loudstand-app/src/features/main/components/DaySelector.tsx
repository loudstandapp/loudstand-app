import React, { RefObject } from 'react'
import {
  View,
  StyleSheet,
  ScrollView,
  Dimensions,
  ListRenderItemInfo,
  FlatList,
  NativeScrollEvent,
  NativeSyntheticEvent,
} from 'react-native'
import { ISO8601Date } from '@loudstand/shared'
import DaySelectorItem from './DaySelectorItem'
import { spacingUnit } from '../../../theme/layout'

const { width } = Dimensions.get('screen')

const ITEM_WIDTH = width / 7

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: spacingUnit,
  },
  scrollViewContentContainer: {
    paddingHorizontal: width / 2 - ITEM_WIDTH / 2,
  },
  scrollView: {},
})

interface Props {
  allDates: ISO8601Date[]
  highlightedDates: ISO8601Date[]
  selectedIndex: number
  onChange(selectedIndex: number): void
}

interface State {
  shownSelectedIndex?: number
  defaultIndex?: number
}

export default class DaySelector extends React.Component<Props, State> {
  handleNextScrollEnd = false

  scrollViewRef: RefObject<ScrollView> = React.createRef()

  carouselRef: RefObject<FlatList<ISO8601Date>> = React.createRef()

  constructor(props: Props) {
    super(props)
    this.state = {
      shownSelectedIndex: undefined,
      defaultIndex: props.selectedIndex,
    }
  }

  componentDidMount = () => {
    this.handleValueChange()
  }

  componentDidUpdate = (prevProps: Props) => {
    const { selectedIndex, allDates } = this.props
    const { shownSelectedIndex, defaultIndex } = this.state
    if (
      (prevProps.selectedIndex !== selectedIndex &&
        selectedIndex !== shownSelectedIndex &&
        defaultIndex !== undefined) ||
      (prevProps.selectedIndex !== selectedIndex &&
        defaultIndex === undefined &&
        selectedIndex < allDates.length) ||
      prevProps.allDates !== allDates
    ) {
      this.handleValueChange()
    }
  }

  handleValueChange = () => {
    const { selectedIndex, allDates } = this.props
    const isOutOfBounds = selectedIndex >= allDates.length

    this.setState(prevState => {
      const defaultIndex =
        prevState.defaultIndex !== undefined
          ? prevState.defaultIndex
          : selectedIndex

      const shownSelectedIndex = isOutOfBounds ? undefined : selectedIndex
      if (shownSelectedIndex !== undefined) {
        this.carouselRef.current &&
          this.carouselRef.current.scrollToIndex({
            index: shownSelectedIndex,
            animated: true,
          })
      }
      return {
        shownSelectedIndex,
        defaultIndex: isOutOfBounds ? undefined : defaultIndex,
      }
    })
  }

  handleSelectDay = (index: number) => {
    const { onChange } = this.props
    this.setState({ shownSelectedIndex: index })
    this.carouselRef.current &&
      this.carouselRef.current.scrollToIndex({ index, animated: true })
    onChange(index)
  }

  handleMomentumScrollEnd = (
    event: NativeSyntheticEvent<NativeScrollEvent>
  ) => {
    if (!this.handleNextScrollEnd) {
      return
    }
    this.handleNextScrollEnd = false
    const index = Math.round(event.nativeEvent.contentOffset.x / ITEM_WIDTH)
    this.handleSelectDay(index)
  }

  handleDragBegin = () => {
    this.handleNextScrollEnd = true
  }

  renderDateItem = ({ item, index }: ListRenderItemInfo<ISO8601Date>) => {
    const { shownSelectedIndex } = this.state
    const { highlightedDates } = this.props
    const isSelected = shownSelectedIndex === index
    const isHighligthed = !!highlightedDates.includes(item)
    return (
      <DaySelectorItem
        isHighlighted={isHighligthed}
        isSelected={isSelected}
        date={item}
        index={index}
        onSelect={this.handleSelectDay}
      />
    )
  }

  render = () => {
    const { allDates } = this.props
    const { defaultIndex, shownSelectedIndex } = this.state
    if (defaultIndex === undefined) {
      return null
    }
    return (
      <View style={styles.container}>
        <FlatList
          renderItem={this.renderDateItem}
          data={allDates}
          style={styles.scrollView}
          contentContainerStyle={styles.scrollViewContentContainer}
          onMomentumScrollEnd={this.handleMomentumScrollEnd}
          ref={this.carouselRef}
          extraData={[shownSelectedIndex, allDates]}
          initialScrollIndex={defaultIndex}
          snapToInterval={ITEM_WIDTH}
          onScrollBeginDrag={this.handleDragBegin}
          getItemLayout={(data: any, index) => ({
            length: ITEM_WIDTH,
            index,
            offset: index * ITEM_WIDTH,
          })}
          keyExtractor={item => item}
          horizontal
          showsHorizontalScrollIndicator={false}
          snapToAlignment="center"
          decelerationRate="fast"
        />
      </View>
    )
  }
}

import React from 'react'
import { ISO8601Date } from '@loudstand/shared'
import {
  TouchableOpacity,
  Text,
  View,
  Dimensions,
  StyleSheet,
} from 'react-native'
import Color from 'color'
import colors from '../../../theme/colors'
import fonts from '../../../theme/fonts'
import { spacingUnit } from '../../../theme/layout'
import { currentLocale } from '../../../translation'

const weekdayFormatter = new Intl.DateTimeFormat(currentLocale.languageTag, {
  weekday: 'narrow',
})

const { width } = Dimensions.get('screen')

const ITEM_WIDTH = width / 7
const CIRCLE_1_SIZE = 24
const CIRCLE_2_SIZE = 28

const styles = StyleSheet.create({
  dayItem: {
    width: ITEM_WIDTH,
    alignItems: 'center',
  },
  dayText: {
    textAlign: 'center',
    ...(fonts.CustomDaySelectorDay as object),
    color: colors.tertiaryAlt,
  },
  weekDayText: {
    textAlign: 'center',
    marginBottom: spacingUnit / 2,
    ...(fonts.CustomDaySelectorWeekday as object),
    color: colors.tertiaryAlt,
  },
  selectedText: {
    color: colors.primaryText,
  },
  highlighedText: {
    color: colors.secondaryText,
  },
  selectedDayWrapper: {
    borderRadius: CIRCLE_1_SIZE / 2,
    backgroundColor: colors.background,
    borderColor: colors.tertiaryAlt,
    borderWidth: 2,
  },
  dayWrapper: {
    width: CIRCLE_1_SIZE,
    height: CIRCLE_1_SIZE,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  selectedCircleWrapper: {
    borderRadius: CIRCLE_1_SIZE / 2,
    backgroundColor: Color(colors.background)
      .alpha(0.2)
      .toString(),
  },
  circleWrapper: {
    width: CIRCLE_2_SIZE,
    height: CIRCLE_2_SIZE,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

interface Props {
  isSelected: boolean
  isHighlighted?: boolean
  date: ISO8601Date
  index: number
  onSelect: (index: number) => void
}

export default class DaySelectorItem extends React.PureComponent<Props> {
  handleSelect = () => {
    const { onSelect, index } = this.props
    onSelect(index)
  }

  render = () => {
    const { date, isSelected, isHighlighted } = this.props

    const dateObj = new Date(date)
    const day = dateObj.getDate()
    const weekDay = weekdayFormatter.format(dateObj)

    return (
      <TouchableOpacity style={styles.dayItem} onPress={this.handleSelect}>
        <Text
          style={[styles.weekDayText, isHighlighted && styles.highlighedText]}
        >
          {weekDay}
        </Text>
        <View
          style={[
            styles.circleWrapper,
            isSelected && styles.selectedCircleWrapper,
          ]}
        >
          <View
            style={[
              styles.dayWrapper,
              isSelected && styles.selectedDayWrapper,
              // isHighlighted && styles.highlighedDayWrapper,
            ]}
          >
            <Text
              style={[
                styles.dayText,
                isHighlighted && styles.highlighedText,
                isSelected && styles.selectedText,
              ]}
            >
              {day}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

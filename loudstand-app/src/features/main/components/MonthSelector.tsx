import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { ISO8601Month, ISO8601Date } from '@loudstand/shared'
import { cleanDate } from '../../../utils'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { spacingUnit, touchableSize } from '../../../theme/layout'
import { currentLocale } from '../../../translation'

interface Props {
  firstMonth: ISO8601Month
  lastMonth: ISO8601Month
  value: ISO8601Date
  onChange(selectedDate: ISO8601Date): void
}

const dateFormatter = new Intl.DateTimeFormat(currentLocale.languageTag, {
  year: 'numeric',
  month: 'long',
})

const IMAGE_SIZE = 25 // TODO: resolveImageAssetSource
const hitSlop = (touchableSize - IMAGE_SIZE) / 2

export function monthFromDate(date: ISO8601Date): ISO8601Month {
  const regex = /^(\d+)-(\d+).*$/
  const replacement = '$1-$2'
  return date.replace(regex, replacement)
}

export function monthsFromDates(dates: ISO8601Date[]): ISO8601Month[] {
  const months = dates.map(monthFromDate)
  const uniqueMonths = [...new Set(months)]
  const sortedMonths = uniqueMonths.sort()
  return sortedMonths
}

const styles = StyleSheet.create({
  container: {
    marginTop: spacingUnit,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  button: {},
  buttonImage: {
    tintColor: colors.secondaryText,
  },
  valueText: {
    ...(fonts.Header as object),
    color: colors.secondaryText,
    marginHorizontal: spacingUnit,
  },
})

export default function MonthSelector({
  firstMonth,
  lastMonth,
  value,
  onChange,
}: Props) {
  const monthValue = monthFromDate(value)
  function handleChange(change: number) {
    const date = new Date(value)
    date.setMonth(date.getMonth() + change)
    const newMonth = cleanDate(date.toISOString())
    onChange(newMonth)
  }

  function handlePrev() {
    handleChange(-1)
  }
  function handleNext() {
    handleChange(1)
  }
  const isFirst = monthValue === firstMonth
  const isLast = monthValue === lastMonth
  const formattedMonth = dateFormatter.format(new Date(value))
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.button}
        onPress={handlePrev}
        disabled={isFirst}
        hitSlop={{
          top: hitSlop,
          bottom: hitSlop,
          left: hitSlop,
          right: hitSlop,
        }}
      >
        <Image
          style={styles.buttonImage}
          source={require('../../../../assets/icons/buttons/calendar-prev-arrow.png')}
        />
      </TouchableOpacity>
      <Text style={styles.valueText}>{formattedMonth}</Text>
      <TouchableOpacity
        style={styles.button}
        onPress={handleNext}
        disabled={isLast}
      >
        <Image
          style={styles.buttonImage}
          source={require('../../../../assets/icons/buttons/calendar-next-arrow.png')}
        />
      </TouchableOpacity>
    </View>
  )
}

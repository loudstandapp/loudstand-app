import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { CalendarItemType } from '@loudstand/shared'
import { View } from 'react-native'
import GenericItem from './GenericItem'

const TEST_TIMESTAMP = '2012-12-14T12:32:11Z'

storiesOf('GenericItem', module)
  .addDecorator(getStory => (
    <View style={{ backgroundColor: 'black', padding: 20 }}>{getStory()}</View>
  ))
  .add('Injuries', () => (
    <GenericItem
      item={{
        timestamp: TEST_TIMESTAMP,
        subject: 'Player1, player2, player 3',
        type: CalendarItemType.Injuries,
        location: null,
      }}
    />
  ))
  .add('Press', () => (
    <GenericItem
      item={{
        timestamp: TEST_TIMESTAMP,
        subject: "About tonight's match",
        type: CalendarItemType.Press,
        location: 'Some great place',
      }}
    />
  ))
  .add('Training', () => (
    <GenericItem
      item={{
        timestamp: TEST_TIMESTAMP,
        subject: 'Getting better and better',
        type: CalendarItemType.Training,
        location: 'Another place in the unknown',
      }}
    />
  ))

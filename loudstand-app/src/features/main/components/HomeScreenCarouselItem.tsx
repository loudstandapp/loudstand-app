import React from 'react'
import {
  MatchListItem,
  ID,
  LocalizedTeams,
  ISO8601Date,
  Question,
  SubmittedVote,
  QuestionResponseType,
  UserVoteHistory,
  generateVoteKey,
  VoteType,
  QuestionResultsSummaryDocument,
  PlayerList,
  TeamPlayer,
  HomeScreenMemoryCalendar,
  CalendarItem,
  CalendarItemType,
  ISO639a2Language,
  ISO8601Timestamp,
} from '@loudstand/shared'
import {
  View,
  StyleSheet,
  Dimensions,
  FlatList,
  ListRenderItemInfo,
  NativeSyntheticEvent,
  NativeScrollEvent,
  ImageRequireSource,
} from 'react-native'
import moment from 'moment'
import MatchItem from './MatchItem'
import QuestionItem from './QuestionItem'
import { questionVote } from '../../match/voting'
import { getComparatorForKey } from '../../../utils'
import GenericItem from './GenericItem'
import { SubmittedVotes, ChosenTeamList } from '../../../store/model'
import { mainImageMaxHeight, spacingUnit } from '../../../theme/layout'
import { translate, fallback, currentLocale } from '../../../translation'
import colors from '../../../theme/colors'

export type Row =
  | MatchRow
  | QuestionRow
  | GenericItemRow
  | PlayerBirthdayRow
  | MemoryRow

interface Props {
  date: ISO8601Date
  matchesForDate?: { [key in ID]: MatchListItem }
  itemsForDate?: { [key in ID]: CalendarItem }
  question?: Question
  teamPlayers?: PlayerList
  memoryCalendar?: HomeScreenMemoryCalendar
  questionResults?: QuestionResultsSummaryDocument
  onVote: (vote: SubmittedVote) => void
  onSelectMatch: (match: MatchListItem) => void
  localizedTeams?: LocalizedTeams
  voteHistory?: UserVoteHistory
  submittedVotes?: SubmittedVotes
  onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void
  availableTeams: ChosenTeamList
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
  carouselItem: {
    width,
  },
  flatlistContentContainer: {
    paddingTop: mainImageMaxHeight,
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: colors.tertiary30pct,
  },
})

enum RowType {
  MatchRow,
  QuestionRow,
  GenericItemRow,
  BirthdayRow,
  MemoryRow,
}
interface MatchRow {
  type: RowType.MatchRow
  match: MatchListItem
}
interface QuestionRow {
  type: RowType.QuestionRow
  question: Question
}
interface GenericItemRow {
  type: RowType.GenericItemRow
  item: CalendarItem
}
interface PlayerBirthdayRow {
  type: RowType.BirthdayRow
  player: TeamPlayer
}
interface MemoryRow {
  type: RowType.MemoryRow
  memory: CalendarItem
}

export const timeFormatter = new Intl.DateTimeFormat('default', {
  hour: 'numeric',
  minute: 'numeric',
})

function safeFormatTime(timestamp: ISO8601Timestamp | null | undefined) {
  try {
    if (!timestamp) {
      return ''
    }
    return timeFormatter.format(new Date(timestamp))
  } catch (e) {
    console.log('GenericItem safeFormatTime', e)
    return ''
  }
}

export const yearFormatter = new Intl.DateTimeFormat('default', {
  year: 'numeric',
})

function safeFormatYear(date: ISO8601Date | null | undefined) {
  try {
    if (!date) {
      return ''
    }
    return yearFormatter.format(new Date(date))
  } catch (e) {
    console.warn('MemoryItem safeFormatYear', e)
    return ''
  }
}

const { languageCode } = currentLocale

export function localizeSubject(
  subject: { [key in ISO639a2Language]: string }
) {
  return (
    subject[languageCode] ||
    subject[fallback.languageTag] ||
    Object.values(subject)[0]
  )
}
function getType(
  type: CalendarItemType | undefined | null
): { title: string | undefined; icon: ImageRequireSource | undefined } {
  switch (type) {
    case CalendarItemType.Injuries:
      return {
        title: translate('homeScreenItem.injuries'),
        icon: require('../../../../assets/icons/indicators/injury.png'),
      }
    case CalendarItemType.Memory:
      return {
        title: translate('homeScreenItem.memory'),
        icon: require('../../../../assets/icons/indicators/memory.png'),
      }
    case CalendarItemType.Press:
      return {
        title: translate('homeScreenItem.press'),
        icon: require('../../../../assets/icons/indicators/press.png'),
      }
    case CalendarItemType.Training:
      return {
        title: translate('homeScreenItem.training'),
        icon: require('../../../../assets/icons/indicators/training.png'),
      }
    default:
      return {
        title: undefined,
        icon: undefined,
      }
  }
}

function getBirthdays(teamPlayers: PlayerList, date: ISO8601Date) {
  const dateOnly = date.substring(5)
  const birthdayPlayers = Object.values(teamPlayers).filter(
    player => player.birthDate && player.birthDate.substring(5) === dateOnly
  )
  return birthdayPlayers
}

// TODO: Optimize these if number of items becomes too mich to filter on each render
function getMemories(memories: HomeScreenMemoryCalendar, date: ISO8601Date) {
  const dateOnly = date.substring(5)
  const todaysMemories = Object.values(memories).filter(
    memory => memory.date && memory.date.substring(5) === dateOnly
  )
  return todaysMemories
}

function Separator() {
  return <View style={styles.separator} />
}

export default class HomeScreenCarouselItem extends React.PureComponent<Props> {
  ref = React.createRef<FlatList<Row>>()

  shouldReportScrolling = true

  isScrolledToTop = true

  handleSelectMatch = (match: MatchListItem) => {
    const { onSelectMatch } = this.props
    onSelectMatch(match)
  }

  handleQuestionResponse = (
    question: Question,
    response: QuestionResponseType
  ) => {
    const { onVote } = this.props
    const { id, teamId, playerId } = question
    const vote = questionVote({
      questionId: id,
      teamId,
      playerId,
      answer: response,
    })
    onVote(vote)
  }

  handleScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const { onScroll } = this.props
    if (this.shouldReportScrolling && onScroll) {
      onScroll(event)
    }
    const { y } = event.nativeEvent.contentOffset
    if (y === 0) {
      this.isScrolledToTop = true
      this.shouldReportScrolling = true
    } else {
      this.isScrolledToTop = false
    }
  }

  scrollToTopIfNeeded = () => {
    if (this.isScrolledToTop) {
      return
    }
    this.shouldReportScrolling = false
    this.ref.current &&
      this.ref.current.scrollToOffset({ offset: 0, animated: true })
  }

  renderItem = ({ item }: ListRenderItemInfo<Row>) => {
    const { availableTeams } = this.props
    switch (item.type) {
      case RowType.MatchRow: {
        const { match } = item
        const { localizedTeams } = this.props
        return (
          <MatchItem
            key={match.id}
            match={match}
            onSelect={this.handleSelectMatch}
            localizedTeams={localizedTeams}
            availableTeams={availableTeams}
          />
        )
      }
      case RowType.GenericItemRow: {
        const { item: genericItem } = item
        const { icon, title } = getType(genericItem.type)
        return (
          <GenericItem
            accessoryIcon={icon}
            title={title}
            subject={
              genericItem.subject && localizeSubject(genericItem.subject)
            }
            location={genericItem.location}
            accessoryText={safeFormatTime(genericItem.timestamp)}
          />
        )
      }
      case RowType.BirthdayRow: {
        const { player } = item
        const { date } = this.props
        const years = player.birthDate
          ? moment(date).diff(player.birthDate, 'year')
          : 0
        return (
          <GenericItem
            accessoryIcon={require('../../../../assets/icons/indicators/birthday.png')}
            title={translate('homescreenPlayerBirthday.greeting')}
            subject={player.name}
            accessoryText={safeFormatYear(player.birthDate)}
            location={translate('homescreenPlayerBirthday.years', { years })}
          />
        )
      }
      case RowType.MemoryRow: {
        const { memory } = item
        const { icon, title } = getType(memory.type)
        return (
          <GenericItem
            accessoryIcon={icon}
            title={title}
            subject={memory.subject && localizeSubject(memory.subject)}
            location={memory.location}
            accessoryText={safeFormatYear(memory.date)}
          />
        )
      }
      case RowType.QuestionRow: {
        const {
          questionResults,
          voteHistory,
          submittedVotes,
          availableTeams,
        } = this.props
        const { question } = item
        const questionVoteKey =
          question &&
          generateVoteKey(VoteType.Question, [{ question: question.id }])
        const hasBeenAnswered =
          !!voteHistory && !!questionVoteKey && !!voteHistory[questionVoteKey]

        const chosenOption =
          voteHistory &&
          questionVoteKey &&
          voteHistory[questionVoteKey] &&
          voteHistory[questionVoteKey].response

        return (
          <QuestionItem
            question={question}
            hasBeenAnswered={hasBeenAnswered}
            result={questionResults && questionResults[question.id]}
            onResponse={this.handleQuestionResponse}
            chosen={chosenOption}
            availableTeams={availableTeams}
            hasBeenSubmitted={
              !!submittedVotes &&
              !!questionVoteKey &&
              submittedVotes[questionVoteKey]
            }
          />
        )
      }
      default:
        return null
    }
  }

  render = () => {
    const {
      matchesForDate,
      itemsForDate,
      question,
      teamPlayers,
      date,
      memoryCalendar,
    } = this.props
    const matches: MatchRow[] = matchesForDate
      ? Object.values(matchesForDate)
          .sort(getComparatorForKey('startTime'))
          .map(match => ({ type: RowType.MatchRow, match }))
      : []

    const questionRows: QuestionRow[] = question
      ? [{ type: RowType.QuestionRow, question }]
      : []

    const genericItems: GenericItemRow[] = itemsForDate
      ? Object.values(itemsForDate)
          .sort(getComparatorForKey('timestamp'))
          .map(item => ({ type: RowType.GenericItemRow, item }))
      : []

    const playerBirthdays: PlayerBirthdayRow[] = teamPlayers
      ? getBirthdays(teamPlayers, date).map(player => ({
          type: RowType.BirthdayRow,
          player,
        }))
      : []

    const teamMemories: MemoryRow[] = memoryCalendar
      ? getMemories(memoryCalendar, date).map(memory => ({
          type: RowType.MemoryRow,
          memory,
        }))
      : []

    const rows: Row[] = [
      ...matches,
      ...questionRows,
      ...teamMemories,
      ...playerBirthdays,
      ...genericItems,
    ]

    return (
      <View style={styles.carouselItem}>
        <FlatList
          contentContainerStyle={styles.flatlistContentContainer}
          onScroll={this.handleScroll}
          scrollEventThrottle={16}
          data={rows}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
          ref={this.ref}
          ItemSeparatorComponent={Separator}
        />
      </View>
    )
  }
}

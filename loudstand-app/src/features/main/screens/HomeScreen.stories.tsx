/* eslint-disable @typescript-eslint/camelcase */

import React from 'react'
// eslint-disable-next-line import/no-extraneous-dependencies
import { storiesOf } from '@storybook/react-native'
// eslint-disable-next-line import/no-extraneous-dependencies
import { action } from '@storybook/addon-actions'
import {
  HomeScreenItemCalendar,
  CalendarItemType,
  ISO8601Date,
  Question,
} from '@loudstand/shared'
import { HomeScreen } from './HomeScreen'

export const matchCalendar = {
  '2018-11-03': {
    'sr:match:14894233': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:816',
        name: 'Volkswagen Arena',
      },
      id: 'sr:match:14894233',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2524',
          name: 'VfL Wolfsburg',
        },
      },
      date: '2018-11-03',
      startTime: '2018-11-03T14:30:00+00:00',
    },
  },
  '2019-03-16': {
    'sr:match:14894517': {
      season: {
        name: 'Primeira Liga 19/20',
        id: 'sr:season:67257',
      },
      venue: {
        id: 'sr:venue:966',
        name: 'Olympiastadion',
      },
      id: 'sr:match:14894517',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2528',
          name: 'Hertha BSC',
        },
      },
      date: '2019-03-16',
      startTime: '2019-03-16T17:30:00+00:00',
    },
  },
  '2019-02-02': {
    'sr:match:14894405': {
      season: {
        name: 'UEFA Europa League 19/20',
        id: 'sr:season:68134',
      },
      venue: {
        id: 'sr:venue:609',
        name: 'Commerzbank Arena',
      },
      id: 'sr:match:14894405',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2674',
          name: 'Eintracht Frankfurt',
        },
      },
      date: '2019-02-02',
      startTime: '2019-02-02T14:30:00+00:00',
    },
  },
  '2019-04-21': {
    'sr:match:14894589': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:592',
        name: 'Schwarzwald-Stadion',
      },
      id: 'sr:match:14894589',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2538',
          name: 'SC Freiburg',
        },
      },
      date: '2019-04-21',
      startTime: '2019-04-21T13:30:00+00:00',
    },
  },
  '2018-12-15': {
    'sr:match:14894309': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894309',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2534',
          name: 'Werder Bremen',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2018-12-15',
      startTime: '2018-12-15T17:30:00+00:00',
    },
  },
  '2018-10-20': {
    'sr:match:14894185': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:648',
        name: 'Mercedes Benz Arena',
      },
      id: 'sr:match:14894185',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2677',
          name: 'VfB Stuttgart',
        },
      },
      date: '2018-10-20',
      startTime: '2018-10-20T13:30:00+00:00',
    },
  },
  '2019-02-09': {
    'sr:match:14894417': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894417',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2569',
          name: '1899 Hoffenheim',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2019-02-09',
      startTime: '2019-02-09T14:30:00+00:00',
    },
  },
  '2018-09-14': {
    'sr:match:14894093': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894093',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2674',
          name: 'Eintracht Frankfurt',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2018-09-14',
      startTime: '2018-09-14T18:30:00+00:00',
    },
  },
  '2019-03-09': {
    'sr:match:14894491': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894491',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2677',
          name: 'VfB Stuttgart',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2019-03-09',
      startTime: '2019-03-09T14:30:00+00:00',
    },
  },
  '2018-10-27': {
    'sr:match:14894201': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894201',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2528',
          name: 'Hertha BSC',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2018-10-27',
      startTime: '2018-10-27T13:30:00+00:00',
    },
  },
  '2018-12-01': {
    'sr:match:14894273': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894273',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2538',
          name: 'SC Freiburg',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2018-12-01',
      startTime: '2018-12-01T14:30:00+00:00',
    },
  },
  '2018-12-08': {
    'sr:match:14894291': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:812',
        name: 'Veltins-Arena',
      },
      id: 'sr:match:14894291',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2530',
          name: 'FC Schalke 04',
        },
      },
      date: '2018-12-08',
      startTime: '2018-12-08T14:30:00+00:00',
    },
  },
  '2018-08-26': {
    'sr:match:14893639': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14893639',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:36360',
          name: 'RB Leipzig',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2018-08-26',
      startTime: '2018-08-26T16:00:00+00:00',
    },
  },
  '2018-09-26': {
    'sr:match:14894129': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894129',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2523',
          name: '1 FC Nuremberg',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2018-09-26',
      startTime: '2018-09-26T18:30:00+00:00',
    },
  },
  '2019-01-26': {
    'sr:match:14894381': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894381',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2541',
          name: 'Hannover 96',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2019-01-26',
      startTime: '2019-01-26T14:30:00+00:00',
    },
  },
  '2018-08-31': {
    'sr:match:14894087': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:591',
        name: 'HDI Arena',
      },
      id: 'sr:match:14894087',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2541',
          name: 'Hannover 96',
        },
      },
      date: '2018-08-31',
      startTime: '2018-08-31T18:30:00+00:00',
    },
  },
  '2018-10-06': {
    'sr:match:14894167': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894167',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2600',
          name: 'FC Augsburg',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2018-10-06',
      startTime: '2018-10-06T13:30:00+00:00',
    },
  },
  '2019-01-19': {
    'sr:match:14894367': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:850',
        name: 'Red Bull Arena',
      },
      id: 'sr:match:14894367',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:36360',
          name: 'RB Leipzig',
        },
      },
      date: '2019-01-19',
      startTime: '2019-01-19T17:30:00+00:00',
    },
  },
  '2019-02-24': {
    'sr:match:14894453': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894453',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2681',
          name: 'Bayer Leverkusen',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2019-02-24',
      startTime: '2019-02-24T17:00:00+00:00',
    },
  },
  '2018-12-18': {
    'sr:match:14894341': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:680',
        name: 'Merkur Spiel-Arena',
      },
      id: 'sr:match:14894341',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2588',
          name: 'Fortuna Dusseldorf',
        },
      },
      date: '2018-12-18',
      startTime: '2018-12-18T19:30:00+00:00',
    },
  },
  '2018-09-22': {
    'sr:match:14894111': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:1089',
        name: 'PreZero Arena',
      },
      id: 'sr:match:14894111',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2569',
          name: '1899 Hoffenheim',
        },
      },
      date: '2018-09-22',
      startTime: '2018-09-22T13:30:00+00:00',
    },
  },
  '2018-09-29': {
    'sr:match:14894149': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:593',
        name: 'BayArena',
      },
      id: 'sr:match:14894149',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2681',
          name: 'Bayer Leverkusen',
        },
      },
      date: '2018-09-29',
      startTime: '2018-09-29T16:30:00+00:00',
    },
  },
  '2018-12-21': {
    'sr:match:14894345': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894345',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2527',
          name: 'Monchengladbach',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2018-12-21',
      startTime: '2018-12-21T19:30:00+00:00',
    },
  },
  '2019-03-01': {
    'sr:match:14894481': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:1655',
        name: 'Wwk Arena',
      },
      id: 'sr:match:14894481',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2600',
          name: 'FC Augsburg',
        },
      },
      date: '2019-03-01',
      startTime: '2019-03-01T19:30:00+00:00',
    },
  },
  '2019-04-27': {
    'sr:match:14894607': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894607',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2530',
          name: 'FC Schalke 04',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2019-04-27',
      startTime: '2019-04-27T13:30:00+00:00',
    },
  },
  '2019-04-13': {
    'sr:match:14894561': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894561',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2556',
          name: '1. FSV Mainz 05',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2019-04-13',
      startTime: '2019-04-13T16:30:00+00:00',
    },
  },
  '2019-04-06': {
    'sr:match:14894541': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:574',
        name: 'Allianz Arena',
      },
      id: 'sr:match:14894541',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2672',
          name: 'Bayern Munich',
        },
      },
      date: '2019-04-06',
      startTime: '2019-04-06T16:30:00+00:00',
    },
  },
  '2018-11-24': {
    'sr:match:14894267': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:2406',
        name: 'Opel Arena',
      },
      id: 'sr:match:14894267',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2556',
          name: '1. FSV Mainz 05',
        },
      },
      date: '2018-11-24',
      startTime: '2018-11-24T14:30:00+00:00',
    },
  },
  '2019-03-30': {
    'sr:match:14894525': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894525',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2524',
          name: 'VfL Wolfsburg',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2019-03-30',
      startTime: '2019-03-30T14:30:00+00:00',
    },
  },
  '2019-05-11': {
    'sr:match:14894633': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894633',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2588',
          name: 'Fortuna Dusseldorf',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2019-05-11',
      startTime: '2019-05-11T13:30:00+00:00',
    },
  },
  '2019-02-18': {
    'sr:match:14894449': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:617',
        name: 'Max-Morlock-Stadion',
      },
      id: 'sr:match:14894449',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2523',
          name: '1 FC Nuremberg',
        },
      },
      date: '2019-02-18',
      startTime: '2019-02-18T19:30:00+00:00',
    },
  },
  '2019-05-17': {
    'sr:match:14894237': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:759',
        name: 'Signal Iduna Park',
      },
      id: 'sr:match:14894237',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2672',
          name: 'Bayern Munich',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
      },
      date: '2018-11-10',
      startTime: '2018-11-10T17:30:00+00:00',
    },
  },
  '2019-05-18': {
    'sr:match:14894653': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:788',
        name: 'Borussia-Park',
      },
      id: 'sr:match:14894653',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2527',
          name: 'Monchengladbach',
        },
      },
      date: '2019-05-18',
      startTime: '2019-05-18T13:30:00+00:00',
    },
  },
  '2019-05-16': {
    'sr:match:14894623': {
      season: {
        id: 'sr:season:55017',
        name: 'Bundesliga 18/19',
      },
      venue: {
        id: 'sr:venue:821',
        name: 'Weserstadion',
      },
      id: 'sr:match:14894623',
      tournament: {
        id: 'sr:tournament:35',
        name: 'Bundesliga',
      },
      teams: {
        home: {
          country: 'DE',
          id: 'sr:competitor:2673',
          name: 'Borussia Dortmund',
        },
        away: {
          country: 'DE',
          id: 'sr:competitor:2534',
          name: 'Werder Bremen',
        },
      },
      date: '2019-05-04',
      startTime: '2019-05-04T16:30:00+00:00',
    },
  },
}

const TEST_ITEM_CALENDAR: HomeScreenItemCalendar = {
  '2019-05-18': [
    {
      location: 'Your mama',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T12:12:12Z',
    },
    {
      location: 'Your mama XXX',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T12:12:12Z',
    },
    {
      location: 'Your mama2',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T22:12:12Z',
    },
    {
      location: 'Your mama3',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T14:12:12Z',
    },
    {
      location: 'Your mama2',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T16:12:12Z',
    },
    {
      location: 'Your mama3',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T14:12:12Z',
    },
    {
      location: 'Your mama2',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T16:12:12Z',
    },
    {
      location: 'Your mama3',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T14:12:12Z',
    },
    {
      location: 'Your mama2',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T16:12:12Z',
    },
    {
      location: 'Your mama3',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T14:12:12Z',
    },
    {
      location: 'Your mama2',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T16:12:12Z',
    },
    {
      location: 'Your mama3',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T14:12:12Z',
    },
    {
      location: 'Your mama2',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T16:12:12Z',
    },
    {
      location: 'Your mama3',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T14:12:12Z',
    },
    {
      location: 'Your mama2',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T16:12:12Z',
    },
  ],
  '2019-05-16': [
    {
      location: 'Your mama',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T12:12:12Z',
    },
    {
      location: 'Your mama XXX',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T12:12:12Z',
    },
    {
      location: 'Your mama2',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T22:12:12Z',
    },
    {
      location: 'Your mama3',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T14:12:12Z',
    },
    {
      location: 'Your mama2',
      subject: { en: 'Crazy action' },
      type: CalendarItemType.Press,
      timestamp: '2100-01-01T16:12:12Z',
    },
  ],
}

const TEST_QUESTIONS: { [key in ISO8601Date]: Question } = {
  '2019-05-16': {
    id: '12345',
    date: '2019-05-16',
    question: { en: 'Are you hot?' },
    teamId: 'sr:competitor:1281',
    playerId: null,
    imageUrl:
      'http://blog.readytomanage.com/wp-content/uploads/2013/02/teambuilding-questions.jpg',
  },
  '2019-05-17': {
    id: '12345',
    date: '2019-05-16',
    question: { en: 'Are you hot?' },
    teamId: 'sr:competitor:1281',
    playerId: null,
    imageUrl:
      'http://blog.readytomanage.com/wp-content/uploads/2013/02/teambuilding-questions.jpg',
  },
  '2019-05-18': {
    id: '12345',
    date: '2019-05-16',
    question: { en: 'Are you hot?' },
    teamId: 'sr:competitor:1281',
    playerId: null,
    imageUrl:
      'http://blog.readytomanage.com/wp-content/uploads/2013/02/teambuilding-questions.jpg',
  },
}

storiesOf('HomeScreen', module)
  // .addDecorator(getStory => <CenterView>{getStory()}</CenterView>)
  .add('default', () => (
    <HomeScreen
      activeTeam={{
        id: 'sr:competitor:3006',
        name: 'My favorite team',
        country: 'DNK',
      }}
      onSelectMatch={action('onSelectMatch')}
      onSelectSearch={action('onSelectSearch')}
      matchCalendar={matchCalendar}
      onSelectTeam={action('onSelectTeam')}
      onVote={action('onVote')}
      availableTeams={{}} // / FIXME
      itemCalendar={TEST_ITEM_CALENDAR}
      questions={TEST_QUESTIONS}
    />
  ))

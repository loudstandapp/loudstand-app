import React, { Component } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  ListRenderItemInfo,
  NativeSyntheticEvent,
  NativeScrollEvent,
  Image,
  Animated,
  Easing,
  ActivityIndicator,
} from 'react-native'
import { connect } from 'react-redux'
import { firestoreConnect } from 'react-redux-firebase'
import moment from 'moment'
import { compose, Store, bindActionCreators, Dispatch } from 'redux'
import {
  MatchListItem,
  CalendarItemByDay,
  ISO8601Month,
  ISO8601Date,
  TeamListItem,
  LocalizedTeams,
  Question,
  SubmittedVote,
  QuestionResultsSummaryDocument,
  UserVoteHistory,
  HomeScreenItemCalendar,
  ISO8601Timestamp,
  PlayerList,
  HomeScreenMemoryCalendar,
} from '@loudstand/shared'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import LinearGradient from 'react-native-linear-gradient'
import MonthSelector, { monthFromDate } from '../components/MonthSelector'
import DaySelector from '../components/DaySelector'
import { styles as ScreenWrapperStyles } from '../../../components/ScreenWrapper'
import {
  ReduxState,
  ChosenTeamListItem,
  ChosenTeamList,
  SubmittedVotes,
  DismissedGuides,
} from '../../../store/model'
import {
  localizeTeam,
  cleanDate,
  ItemImageType,
  getImageUrl,
  fallbackImages,
  cheats,
} from '../../../utils'
import colors from '../../../theme/colors'
import {
  spacingUnit,
  marginDefault,
  mainImageMaxHeight,
  mainImageMinHeight,
  touchableSize,
} from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import HomeScreenCarouselItem from '../components/HomeScreenCarouselItem'
import { showPickerOverlay } from '../../../components/PickerOverlay'
import { currentLocale } from '../../../translation'
import * as fq from '../../../store/firestoreQueries'
import { votingFlowThunk } from '../../user/votingFlow'
import { selectTeamThunk } from '../navigation'
import TeamPickerItem from '../../../components/TeamPickerItem'
import RemoteImage from '../../../components/RemoteImage'
import ActivityIndicatorLine from '../../../components/ActivityIndicatorLine'
import * as selectors from '../../../store/selectors'
import Guide, { GuideType } from '../../../components/Guide'
import { dismissGuide } from '../../../store/actions'
import RemoteImageBackground from '../../../components/RemoteImageBackground'

const currentLanguageCode = currentLocale.languageCode

const FIXME_BORDER_WIDTH = 0 // StyleSheet.hairlineWidth

const TOP_SCROLL_INSET = 6 * spacingUnit

const TEAM_LOGO_SIZE = 64

const IMAGE_OVERLAY_COLORS = [
  colors.fixmeSecondaryBackground,
  'transparent',
  colors.fixmeSecondaryBackground,
]
const HEADER_COLORS = [colors.primaryAlt, colors.secondary, 'transparent']

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.fixmeSecondaryBackground,
  },
  safeArea: {
    flex: 1,
  },
  topBar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    paddingBottom: TOP_SCROLL_INSET,
    borderWidth: FIXME_BORDER_WIDTH,
    borderColor: 'yellow',
  },
  topBarSafeArea: {
    flex: 1,
  },
  carousel: {
    borderWidth: FIXME_BORDER_WIDTH,
    borderColor: 'purple',
  },
  carouselContent: {
    borderWidth: FIXME_BORDER_WIDTH,
    borderColor: 'blue',
  },
  topWrapper: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  backgroundImage: { width, height: '100%' },
  backgroundImageContent: {
    borderColor: 'green',
    borderWidth: FIXME_BORDER_WIDTH,
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: marginDefault,
    ...StyleSheet.absoluteFillObject,
  },
  teamContainer: {
    alignItems: 'center',
  },
  teamNameRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  teamNameText: {
    ...(fonts.CustomHomeScreenTeam as object),
    color: colors.secondaryText,
  },
  teamLogo: {
    width: TEAM_LOGO_SIZE,
    height: TEAM_LOGO_SIZE,
    marginBottom: marginDefault,
  },
  teamFollowersText: {
    ...(fonts.Body as object),
    color: colors.secondaryText,
  },
  teamFollowersValue: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
  },
  teamSelectorIcon: {
    tintColor: colors.secondaryText,
    marginLeft: spacingUnit,
  },
  topLogo: {
    tintColor: colors.secondaryText,
    margin: spacingUnit,
  },
  settingsButton: {
    width: touchableSize,
    height: touchableSize,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: spacingUnit,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  buttonImage: {
    tintColor: colors.secondaryText,
  },
  logoRow: {
    alignItems: 'center',
  },
  monthSelectorRow: {
    alignItems: 'center',
  },
})

export interface NavigationProps {
  onSelectMatch: (match: MatchListItem) => void
  onSelectSettings: () => void
  onSelectSearch: () => void
}

const SEARCH_BUTTON_ID = 'search'

interface ReduxProps {
  matchCalendar?: CalendarItemByDay<MatchListItem>
  activeTeam?: TeamListItem
  localizedTeams?: LocalizedTeams
  availableTeams: ChosenTeamList
  questions?: { [key in ISO8601Date]: Question }
  questionResults?: QuestionResultsSummaryDocument
  voteHistory?: UserVoteHistory
  submittedVotes?: SubmittedVotes
  itemCalendar?: HomeScreenItemCalendar
  memoryCalendar?: HomeScreenMemoryCalendar
  teamPlayers?: PlayerList
  isLoading: boolean
  isGenerating: boolean
  dismissedGuides: DismissedGuides
  launchCount: number
}

interface ReduxActionProps {
  onVote: (vote: SubmittedVote) => void
  onSelectTeam: (team: TeamListItem) => void
  onDismissGuide: (type: GuideType) => void
}

interface Props extends NavigationProps, ReduxProps, ReduxActionProps {}
interface State {
  firstMonth?: ISO8601Month
  lastMonth?: ISO8601Month
  selectedDateIndex?: number
  allDates: ISO8601Date[]
}

function getValidDateIndex(date: ISO8601Date, allDates: ISO8601Date[]) {
  const firstDate = allDates[0]
  const [lastDate] = allDates.slice(-1)

  if (date > lastDate) {
    return allDates.length - 1
  }
  if (date < firstDate) {
    return 0
  }
  return allDates.indexOf(date)
}

export function generateDateList(startDate: ISO8601Date, endDate: ISO8601Date) {
  const now = moment(startDate)
  const end = moment(endDate)
  const dates = []

  while (now.isSameOrBefore(end)) {
    dates.push(now.format('YYYY-MM-DD'))
    now.add(1, 'days')
  }
  return dates
}

const AmimatedRemoteImageBackground = Animated.createAnimatedComponent(
  RemoteImageBackground
)

const AmimatedTouchableOpacity = Animated.createAnimatedComponent(
  TouchableOpacity
)

export class HomeScreen extends Component<Props, State> {
  carouselRef: React.RefObject<FlatList<ISO8601Month>> = React.createRef()

  carouselItemRefs: { [index: number]: HomeScreenCarouselItem } = {}

  didPickDate = false

  mainScrollOffset = new Animated.Value(0)

  handleVerticalScroll = Animated.event([
    {
      nativeEvent: {
        contentOffset: { y: this.mainScrollOffset },
      },
    },
  ])

  constructor(props: Props) {
    super(props)
    this.state = {
      firstMonth: undefined,
      lastMonth: undefined,
      selectedDateIndex: undefined,
      allDates: [],
    }
  }

  componentDidMount = () => {
    this.generateAllDates()
  }

  componentDidUpdate = (prevProps: Props) => {
    const { matchCalendar } = this.props
    if (prevProps.matchCalendar !== matchCalendar) {
      this.generateAllDates()
    }
  }

  generateAllDates = () => {
    const { matchCalendar } = this.props
    if (!matchCalendar || Object.keys(matchCalendar).length === 0) {
      return
    }
    const datesWithContent = Object.keys(matchCalendar).sort()
    const firstDate = datesWithContent[0]
    const firstMonth = monthFromDate(firstDate)
    const [lastDate] = datesWithContent.slice(-1)
    const lastMonth = monthFromDate(lastDate)
    const allDates = generateDateList(firstDate, lastDate)

    this.setState(prevState => {
      const today = cleanDate(new Date().toISOString())
      const prevSelectedDate =
        prevState.allDates &&
        prevState.selectedDateIndex &&
        prevState.allDates[prevState.selectedDateIndex]

      const selectedDateIndex = prevSelectedDate
        ? getValidDateIndex(prevSelectedDate, allDates)
        : getValidDateIndex(today, allDates)
      // const selectedDateIndex =
      //   prevSelectedDate && allDates.includes(prevSelectedDate)
      //     ? prevState.selectedDateIndex
      //     : newDateIndex
      return {
        allDates,
        lastMonth,
        firstMonth,
        selectedDateIndex,
      }
    })
  }

  handleSelectDate = (newDate: ISO8601Date) => {
    const { allDates } = this.state
    const selectedDateIndex = getValidDateIndex(newDate, allDates)
    this.handleSelectDateIndex(selectedDateIndex)
  }

  handleSelectDateIndex = (selectedDateIndex: number) => {
    this.didPickDate = true
    this.carouselRef.current &&
      this.carouselRef.current.scrollToIndex({
        index: selectedDateIndex,
        animated: true,
      })
    this.setDateIndex(selectedDateIndex)
  }

  setDateFromScrollEventIfNeeded = (
    event: NativeSyntheticEvent<NativeScrollEvent>
  ) => {
    const { selectedDateIndex } = this.state
    const newIndex = Math.round(event.nativeEvent.contentOffset.x / width)
    if (newIndex !== selectedDateIndex) {
      this.setDateIndex(newIndex)
    }
  }

  handleScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    if (this.didPickDate) {
      // Don't handle scroll events when dates are picked directly.
      return
    }
    this.setDateFromScrollEventIfNeeded(event)
  }

  handleMomentumScrollEnd = (
    event: NativeSyntheticEvent<NativeScrollEvent>
  ) => {
    this.didPickDate = false
    this.setDateFromScrollEventIfNeeded(event)
  }

  setDateIndex = (selectedDateIndex: number) => {
    this.setState({ selectedDateIndex })
    ReactNativeHapticFeedback.trigger('selection')
    Animated.timing(this.mainScrollOffset, {
      toValue: 0,
      duration: 250,
      easing: Easing.linear,
    }).start()
    if (this.carouselItemRefs[selectedDateIndex]) {
      this.carouselItemRefs[selectedDateIndex].scrollToTopIfNeeded()
    }
  }

  renderItem = ({ item, index }: ListRenderItemInfo<ISO8601Date>) => {
    const {
      matchCalendar,
      itemCalendar,
      onSelectMatch,
      localizedTeams,
      questions,
      onVote,
      questionResults,
      voteHistory,
      submittedVotes,
      availableTeams,
      teamPlayers,
      memoryCalendar,
    } = this.props
    if (!matchCalendar) return <></>

    const now = new Date()
    const visibleQuestionDates: { [key in ISO8601Date]: Question } | undefined =
      questions &&
      Object.keys(questions).reduce((acc, date: ISO8601Timestamp) => {
        const question = questions[date]
        if (!cheats.alwaysAllowVoting && new Date(question.publishTime) > now) {
          return acc
        }
        return {
          ...acc,
          [date]: question,
        }
      }, {})

    return (
      <HomeScreenCarouselItem
        matchesForDate={matchCalendar[item]}
        itemsForDate={itemCalendar && itemCalendar[item]}
        question={visibleQuestionDates && visibleQuestionDates[item]}
        questionResults={questionResults}
        onVote={onVote}
        date={item}
        localizedTeams={localizedTeams}
        onSelectMatch={onSelectMatch}
        voteHistory={voteHistory}
        submittedVotes={submittedVotes}
        onScroll={this.handleVerticalScroll}
        availableTeams={availableTeams}
        teamPlayers={teamPlayers}
        memoryCalendar={memoryCalendar}
        ref={carouselItem => {
          if (carouselItem) {
            this.carouselItemRefs[index] = carouselItem
          }
        }}
      />
    )
  }

  showTeamSelector = () => {
    const { localizedTeams, availableTeams } = this.props
    const teamsArray = Object.values(availableTeams).sort((a, b) =>
      a.isFavorite && !b.isFavorite ? -1 : 1
    )
    showPickerOverlay({
      data: teamsArray,
      backgroundColor: colors.navigationBackground,
      renderItem: ({ item }: ListRenderItemInfo<ChosenTeamListItem>) => (
        <TeamPickerItem item={item} localizedTeams={localizedTeams} />
      ),
      onSelect: this.handleSelectTeam,
    })
  }

  handleSelectTeam = (team: ChosenTeamListItem) => {
    const { onSelectTeam } = this.props
    onSelectTeam(team)
    return true
  }

  render() {
    const {
      matchCalendar,
      activeTeam,
      localizedTeams,
      isLoading,
      isGenerating,
      dismissedGuides,
      onDismissGuide,
      launchCount,
      onSelectSettings,
    } = this.props
    const { firstMonth, lastMonth, selectedDateIndex, allDates } = this.state

    const selectedDate =
      selectedDateIndex !== undefined && allDates[selectedDateIndex]

    const mainImageHeight = this.mainScrollOffset.interpolate({
      inputRange: [0, mainImageMaxHeight - mainImageMinHeight],
      outputRange: [mainImageMaxHeight, mainImageMinHeight],
      extrapolateLeft: 'extend',
      extrapolateRight: 'clamp',
    })

    const teamButtonOpacity = this.mainScrollOffset.interpolate({
      inputRange: [0, mainImageMaxHeight - mainImageMinHeight],
      outputRange: [1, 0],
      extrapolateLeft: 'clamp',
      extrapolateRight: 'clamp',
    })

    return (
      <View style={styles.container}>
        {selectedDateIndex !== undefined && (
          <SafeAreaView>
            <FlatList
              data={allDates}
              renderItem={this.renderItem}
              style={styles.carousel}
              contentContainerStyle={styles.carouselContent}
              ref={this.carouselRef}
              initialScrollIndex={selectedDateIndex}
              getItemLayout={(data: string[] | null, index: number) => ({
                length: width,
                offset: width * index,
                index,
              })}
              horizontal
              showsHorizontalScrollIndicator={false}
              snapToInterval={width}
              keyExtractor={item => item}
              onMomentumScrollEnd={this.handleMomentumScrollEnd}
              onScroll={this.handleScroll}
              scrollEventThrottle={33} // 30 FPS
              decelerationRate="fast"
            />
          </SafeAreaView>
        )}
        {matchCalendar && (
          <SafeAreaView style={styles.topWrapper}>
            <AmimatedRemoteImageBackground
              source={
                activeTeam
                  ? {
                      uri: getImageUrl(activeTeam.id, ItemImageType.TeamPhoto),
                    }
                  : fallbackImages.homescreen
              }
              defaultSource={fallbackImages.homescreen}
              style={[styles.backgroundImage, { height: mainImageHeight }]}
            >
              <LinearGradient
                locations={[0.1, 0.5, 0.95]}
                colors={IMAGE_OVERLAY_COLORS}
                style={styles.backgroundImageContent}
              >
                {activeTeam && (
                  <AmimatedTouchableOpacity
                    style={[
                      styles.teamContainer,
                      { opacity: teamButtonOpacity },
                    ]}
                    onPress={this.showTeamSelector}
                  >
                    <RemoteImage
                      style={styles.teamLogo}
                      source={{
                        uri: getImageUrl(activeTeam.id, ItemImageType.TeamLogo),
                      }}
                      defaultSource={fallbackImages.teamLogo}
                    />
                    <View style={styles.teamNameRow}>
                      <Text style={styles.teamNameText}>
                        {localizeTeam(activeTeam, localizedTeams)}
                      </Text>
                      <Image
                        source={require('../../../../assets/icons/buttons/down-arrow2.png')}
                        style={styles.teamSelectorIcon}
                      />
                    </View>
                    {/* <Text style={styles.teamFollowersText}>
                    Followed by
                    <Text style={styles.teamFollowersValue}> FIXME </Text>
                    fans
                  </Text> */}
                  </AmimatedTouchableOpacity>
                )}
              </LinearGradient>
            </AmimatedRemoteImageBackground>
          </SafeAreaView>
        )}
        <LinearGradient colors={HEADER_COLORS} style={styles.topBar}>
          <SafeAreaView style={styles.topBarSafeArea}>
            <View style={styles.logoRow}>
              <TouchableOpacity
                style={styles.settingsButton}
                onPress={onSelectSettings}
              >
                <Image
                  style={styles.buttonImage}
                  source={require('../../../../assets/icons/navigation/settings.png')}
                />
              </TouchableOpacity>
              <Image
                style={styles.topLogo}
                source={require('../../../../assets/icons/misc/logo-white-homescreen.png')}
              />
            </View>
            <View style={styles.monthSelectorRow}>
              {firstMonth && lastMonth && selectedDate && (
                <MonthSelector
                  firstMonth={firstMonth}
                  lastMonth={lastMonth}
                  onChange={this.handleSelectDate}
                  value={selectedDate}
                />
              )}
            </View>
            {selectedDateIndex !== undefined && (
              <DaySelector
                allDates={allDates}
                highlightedDates={
                  matchCalendar ? Object.keys(matchCalendar) : []
                }
                onChange={this.handleSelectDateIndex}
                selectedIndex={selectedDateIndex}
              />
            )}
          </SafeAreaView>
        </LinearGradient>
        {isLoading && (
          <View style={ScreenWrapperStyles.loading} pointerEvents="none">
            <ActivityIndicator
              size="large"
              style={ScreenWrapperStyles.activityIndicator}
              color={colors.primaryAlt}
            />
          </View>
        )}
        {isGenerating && (
          <View style={ScreenWrapperStyles.generating} pointerEvents="none">
            <ActivityIndicatorLine
              indicatorColor={colors.tertiary30pct}
              backgroundColor={colors.fixmeSecondaryBackground}
            />
          </View>
        )}
        <Guide
          type={GuideType.Home}
          dismissedGuides={dismissedGuides}
          onDismiss={onDismissGuide}
          launchCount={launchCount}
        />
      </View>
    )
  }
}

const mapStateToProps = (state: ReduxState): ReduxProps => {
  const {
    activeTeam,
    dismissedGuides,
    launchCount,
    availableTeams,
  } = state.general
  const { uid } = state.firebase.auth
  const basic = {
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
    dismissedGuides,
    launchCount,
    availableTeams,
  }
  if (!activeTeam) {
    return basic
  }
  const {
    [fq.teamMatchCalendar.key(activeTeam.id)]: matchCalendar,
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
    [fq.teamPlayers.key(activeTeam.id)]: teamPlayers,
    [fq.teamQuestions.key(activeTeam.id)]: questions,
    [fq.teamQuestionResults.key(activeTeam.id)]: questionResults,
    [fq.userVoteHistory.key(uid)]: voteHistory,
    [fq.teamHomeScreenItemCalendar.key(activeTeam.id)]: itemCalendar,
    [fq.teamHomeScreenMemoryCalendar.key(activeTeam.id)]: memoryCalendar,
  } = state.firestore.data
  return {
    ...basic,
    activeTeam: state.general.activeTeam,
    matchCalendar: matchCalendar && matchCalendar.body,
    localizedTeams: localizedTeams && localizedTeams.body,
    teamPlayers: teamPlayers && teamPlayers.body,
    questions,
    questionResults,
    voteHistory,
    itemCalendar,
    memoryCalendar,
    submittedVotes: state.general.submittedVotes,
  }
}

const mapDispatchToProps = (dispatch: Dispatch): ReduxActionProps =>
  bindActionCreators(
    {
      onVote: votingFlowThunk,
      onSelectTeam: selectTeamThunk,
      onDismissGuide: dismissGuide,
    },
    dispatch
  )

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const { activeTeam } = state.general
    const { uid } = state.firebase.auth
    return activeTeam && uid
      ? [
          fq.teamMatchCalendar.query(activeTeam.id),
          fq.localizedTeams.query(currentLanguageCode),
          fq.teamQuestions.query(activeTeam.id),
          fq.teamPlayers.query(activeTeam.id),
          fq.teamQuestionResults.query(activeTeam.id),
          fq.userVoteHistory.query(uid),
          fq.teamHomeScreenItemCalendar.query(activeTeam.id),
          fq.teamHomeScreenMemoryCalendar.query(activeTeam.id),
        ]
      : []
  })
  // @ts-ignore https://github.com/prescottprue/react-redux-firebase/issues/682
)(HomeScreen)

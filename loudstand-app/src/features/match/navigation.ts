import { Navigation } from 'react-native-navigation'
import { MatchListItem, ID } from '@loudstand/shared'
import { ScreenNames } from '../../registerScreens'
import { NavProps as MatchScreenProps } from './screens/MatchScreen'
import {
  playerScreenNavigationProps,
  teamScreenNavigationProps,
  coachScreenNavigationProps,
} from '../stats/navigation'
import { randomComponentId } from '../../utils'

export function matchScreenNavigationProps(
  mainComponentId: string,
  match: MatchListItem
): MatchScreenProps {
  return {
    match,
    onBack: () => Navigation.pop(mainComponentId),
    onSelectPlayer: (id: ID) => {
      const componentId = randomComponentId(ScreenNames.PlayerScreen)
      return Navigation.push(mainComponentId, {
        component: {
          name: ScreenNames.PlayerScreen,
          id: componentId,
          passProps: playerScreenNavigationProps(componentId, id),
        },
      })
    },
    onSelectTeam: (id: ID) => {
      const componentId = randomComponentId(ScreenNames.TeamScreen)
      return Navigation.push(mainComponentId, {
        component: {
          name: ScreenNames.TeamScreen,
          id: componentId,
          passProps: teamScreenNavigationProps(componentId, id),
        },
      })
    },
    onSelectManager: (id: ID) => {
      const componentId = randomComponentId(ScreenNames.CoachScreen)
      return Navigation.push(mainComponentId, {
        component: {
          name: ScreenNames.CoachScreen,
          id: componentId,
          passProps: coachScreenNavigationProps(componentId, id),
        },
      })
    },
  }
}

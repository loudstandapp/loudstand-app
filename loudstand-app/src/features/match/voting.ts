import {
  QuestionResponseType,
  ISO8601Date,
  SubmittedVote,
  VoteType,
  MatchEventType,
  ID,
  MatchEvent,
} from '@loudstand/shared'

export function matchEventVote({
  event,
  response,
  seasonId,
  matchId,
  teamId,
  matchDate,
  refereeId,
  managerId,
}: {
  event: MatchEvent
  response: QuestionResponseType
  seasonId: ID
  matchId: ID
  teamId: ID
  matchDate: ISO8601Date
  refereeId: ID | null
  managerId: ID | null
}): SubmittedVote | null {
  switch (event.data.type) {
    case MatchEventType.Penalty: {
      return {
        type: VoteType.Penalty,
        answer: response,
        eventId: event.id,
        matchId,
        seasonId,
        teamId,
        refereeId,
        matchDate,
      }
    }
    case MatchEventType.RedCard: {
      return {
        type: VoteType.RedCard,
        answer: response,
        eventId: event.id,
        matchId,
        seasonId,
        teamId,
        refereeId,
        matchDate,
      }
    }
    case MatchEventType.YellowCard: {
      return {
        type: VoteType.YellowCard,
        answer: response,
        eventId: event.id,
        matchId,
        seasonId,
        teamId,
        refereeId,
        matchDate,
      }
    }
    case MatchEventType.SecondYellowCard: {
      return {
        type: VoteType.YellowRedCard,
        answer: response,
        eventId: event.id,
        matchId,
        seasonId,
        teamId,
        refereeId,
        matchDate,
      }
    }
    case MatchEventType.Substitution: {
      return {
        type: VoteType.Substitution,
        answer: response,
        eventId: event.id,
        matchId,
        seasonId,
        teamId,
        managerId,
        matchDate,
      }
    }
    default:
      return null
  }
}

export function lineupVote({
  seasonId,
  matchId,
  teamId,
  matchDate,
  lineup,
}: {
  seasonId: ID
  matchId: ID
  teamId: ID
  matchDate: ISO8601Date
  lineup: ID[]
}): SubmittedVote {
  return {
    type: VoteType.Lineup,
    teamId,
    matchId,
    matchDate,
    seasonId,
    lineup,
  }
}

export function managerPerformanceVote({
  seasonId,
  matchId,
  teamId,
  matchDate,
  managerId,
  rating,
}: {
  seasonId: ID
  matchId: ID
  teamId: ID
  matchDate: ISO8601Date
  managerId: ID
  rating: number
}): SubmittedVote {
  return {
    type: VoteType.ManagerPerformance,
    teamId,
    matchId,
    matchDate,
    seasonId,
    rating,
    managerId,
  }
}

export function teamPerformanceVote({
  seasonId,
  matchId,
  teamId,
  matchDate,
  rating,
}: {
  seasonId: ID
  matchId: ID
  teamId: ID
  matchDate: ISO8601Date
  rating: number
}): SubmittedVote {
  return {
    type: VoteType.TeamPerformance,
    teamId,
    matchId,
    matchDate,
    seasonId,
    rating,
  }
}
export function mostValuablePlayerVote({
  seasonId,
  matchId,
  teamId,
  matchDate,
  playerId,
}: {
  seasonId: ID
  matchId: ID
  teamId: ID
  matchDate: ISO8601Date
  playerId: ID
}): SubmittedVote {
  return {
    type: VoteType.MostValuablePlayer,
    teamId,
    matchId,
    matchDate,
    seasonId,
    playerId,
  }
}

export function questionVote({
  questionId,
  answer,
  teamId,
  playerId,
}: {
  questionId: ID
  teamId: ID
  answer: QuestionResponseType
  playerId: ID | null
}): SubmittedVote {
  return {
    type: VoteType.Question,
    teamId,
    playerId,
    questionId,
    answer,
  }
}

import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import React from 'react'
import {
  MatchState,
  MatchLineup,
  PlayerPosition,
  MatchTimeline,
  MatchEventResultsSummaryDocument,
  TeamPerformancelResultDocument,
  ManagerPerformanceResultDocument,
  MatchActivePlayers,
  MatchRoot,
  MatchListItem,
  UserVoteHistory,
  MatchTeamStatistics,
  TeamRoot,
  MatchStatusType,
} from '@loudstand/shared'
import { MatchScreen } from './MatchScreen'
import { TEST_TEAM_PLAYERS } from '../../stats/screens/TeamScreen.stories'
import {
  TEST_SEASON_STANDINGS,
  TEST_TEAM_POPULARITY,
} from '../../stats/components/Standings.stories'
import { TEST_TOP_PLAYERS } from '../../../components/Top3Players.stories'
import { TEST_TIMELINE } from '../components/Timeline.stories'
import { ChosenTeamList } from '../../../store/model'

const TEST_TEAM_STATISTICS: MatchTeamStatistics = {
  offsides: {
    home: 4,
    away: 0,
  },
  corner_kicks: {
    home: 17,
    away: 1,
  },
  fouls: {
    home: 17,
    away: 11,
  },
  yellow_cards: {
    home: 4,
    away: 4,
  },
  free_kicks: {
    home: 11,
    away: 21,
  },
  goal_kicks: {
    home: 6,
    away: 13,
  },
  red_cards: {
    home: 1,
    away: 1,
  },
  throw_ins: {
    home: 18,
    away: 18,
  },
  injuries: {
    home: 0,
    away: 4,
  },
  shots_on_target: {
    home: 6,
    away: 1,
  },
  shots_off_target: {
    home: 8,
    away: 0,
  },
  ball_possession: {
    home: 58,
    away: 42,
  },
  shots_saved: {
    home: 0,
    away: 4,
  },
  shots_blocked: {
    home: 7,
    away: 0,
  },
}

const TEST_LINEUPS: MatchLineup = {
  home: {
    manager: {
      id: 'sr:player:6327',
      name: 'Sergio Conceicao',
    },
    playing: [
      {
        number: 26,
        country: null,
        id: 'sr:player:248245',
        name: 'Vana Alves',
        position: PlayerPosition.Goalkeeper,
      },
      {
        number: 3,
        country: null,
        id: 'sr:player:918726',
        name: 'Eder Militao',
        position: PlayerPosition.Defender,
      },
      {
        number: 33,
        country: null,
        id: 'sr:player:5884',
        name: 'Pepe',
        position: PlayerPosition.Defender,
      },
      {
        number: 28,
        country: null,
        id: 'sr:player:242211',
        name: 'Felipe',
        position: PlayerPosition.Defender,
      },
      {
        number: 13,
        country: null,
        id: 'sr:player:312110',
        name: 'Alex Telles',
        position: PlayerPosition.Defender,
      },
      {
        number: 17,
        country: null,
        id: 'sr:player:123906',
        name: 'Jesus Corona',
        position: PlayerPosition.Forward,
      },
      {
        number: 22,
        country: null,
        id: 'sr:player:99545',
        name: 'Danilo Pereira',
        position: PlayerPosition.Midfielder,
      },
      {
        number: 16,
        country: null,
        id: 'sr:player:204790',
        name: 'Hector Herrera',
        position: PlayerPosition.Midfielder,
      },
      {
        number: 25,
        country: null,
        id: 'sr:player:244801',
        name: 'Otavio',
        position: PlayerPosition.Midfielder,
      },
      {
        number: 11,
        country: null,
        id: 'sr:player:298300',
        name: 'Moussa Marega',
        position: PlayerPosition.Forward,
      },
      {
        number: 29,
        country: null,
        id: 'sr:player:800432',
        name: 'Soares',
        position: PlayerPosition.Forward,
      },
    ],
    starting: {
      defender: [
        {
          number: 3,
          country: null,
          id: 'sr:player:918726',
          name: 'Eder Militao',
          position: PlayerPosition.Defender,
        },
        {
          number: 33,
          country: null,
          id: 'sr:player:5884',
          name: 'Pepe',
          position: PlayerPosition.Defender,
        },
        {
          number: 28,
          country: null,
          id: 'sr:player:242211',
          name: 'Felipe',
          position: PlayerPosition.Defender,
        },
        {
          number: 13,
          country: null,
          id: 'sr:player:312110',
          name: 'Alex Telles',
          position: PlayerPosition.Defender,
        },
      ],
      forward: [
        {
          number: 17,
          country: null,
          id: 'sr:player:123906',
          name: 'Jesus Corona',
          position: PlayerPosition.Forward,
        },
        {
          number: 11,
          country: null,
          id: 'sr:player:298300',
          name: 'Moussa Marega',
          position: PlayerPosition.Forward,
        },
        {
          number: 29,
          country: null,
          id: 'sr:player:800432',
          name: 'Soares',
          position: PlayerPosition.Forward,
        },
      ],
      goalkeeper: [
        {
          number: 26,
          country: null,
          id: 'sr:player:248245',
          name: 'Vana Alves',
          position: PlayerPosition.Goalkeeper,
        },
      ],
      midfielder: [
        {
          number: 22,
          country: null,
          id: 'sr:player:99545',
          name: 'Danilo Pereira',
          position: PlayerPosition.Midfielder,
        },
        {
          number: 16,
          country: null,
          id: 'sr:player:204790',
          name: 'Hector Herrera',
          position: PlayerPosition.Midfielder,
        },
        {
          number: 25,
          country: null,
          id: 'sr:player:244801',
          name: 'Otavio',
          position: PlayerPosition.Midfielder,
        },
      ],
    },
    substitutes: [
      {
        number: 31,
        country: null,
        id: 'sr:player:936350',
        name: 'Diogo Costa',
        position: PlayerPosition.Goalkeeper,
      },
      {
        number: 2,
        country: null,
        id: 'sr:player:26179',
        name: 'Maxi Pereira',
        position: PlayerPosition.Defender,
      },
      {
        number: 12,
        country: null,
        id: 'sr:player:344205',
        name: 'Wilson Manafa',
        position: PlayerPosition.Defender,
      },
      {
        number: 10,
        country: null,
        id: 'sr:player:231776',
        name: 'Oliver Torres',
        position: PlayerPosition.Midfielder,
      },
      {
        number: 8,
        country: null,
        id: 'sr:player:39066',
        name: 'Yacine Brahimi',
        position: PlayerPosition.Forward,
      },
      {
        number: 9,
        country: null,
        id: 'sr:player:116869',
        name: 'Vincent Aboubakar',
        position: PlayerPosition.Forward,
      },
      {
        number: 37,
        country: null,
        id: 'sr:player:268821',
        name: 'Fernando Andrade',
        position: PlayerPosition.Forward,
      },
    ],
  },
  away: {
    manager: {
      id: 'sr:player:232754',
      name: 'Marcel Keizer',
    },
    playing: [
      {
        number: 40,
        country: null,
        id: 'sr:player:82598',
        name: 'Renan Ribeiro',
        position: PlayerPosition.Goalkeeper,
      },
      {
        number: 76,
        country: null,
        id: 'sr:player:252819',
        name: 'Bruno Gaspar',
        position: PlayerPosition.Defender,
      },
      {
        number: 6,
        country: null,
        id: 'sr:player:59616',
        name: 'Andre Pinto',
        position: PlayerPosition.Defender,
      },
      {
        number: 22,
        country: null,
        id: 'sr:player:3179',
        name: 'Jeremy Mathieu',
        position: PlayerPosition.Defender,
      },
      {
        number: 26,
        country: null,
        id: 'sr:player:957322',
        name: 'Cristian Borja',
        position: PlayerPosition.Defender,
      },
      {
        number: 23,
        country: null,
        id: 'sr:player:115718',
        name: 'Abdoulay Diaby',
        position: PlayerPosition.Forward,
      },
      {
        number: 25,
        country: null,
        id: 'sr:player:39586',
        name: 'Radosav Petrovic',
        position: PlayerPosition.Midfielder,
      },
      {
        number: 86,
        country: null,
        id: 'sr:player:68332',
        name: 'Nemanja Gudelj',
        position: PlayerPosition.Midfielder,
      },
      {
        number: 8,
        country: null,
        id: 'sr:player:288205',
        name: 'Bruno Fernandes',
        position: PlayerPosition.Midfielder,
      },
      {
        number: 9,
        country: null,
        id: 'sr:player:578560',
        name: 'Marcos Acuna',
        position: PlayerPosition.Forward,
      },
      {
        number: 29,
        country: null,
        id: 'sr:player:291707',
        name: 'Luiz Phellype',
        position: PlayerPosition.Forward,
      },
    ],
    starting: {
      defender: [
        {
          number: 76,
          country: null,
          id: 'sr:player:252819',
          name: 'Bruno Gaspar',
          position: PlayerPosition.Defender,
        },
        {
          number: 6,
          country: null,
          id: 'sr:player:59616',
          name: 'Andre Pinto',
          position: PlayerPosition.Defender,
        },
        {
          number: 22,
          country: null,
          id: 'sr:player:3179',
          name: 'Jeremy Mathieu',
          position: PlayerPosition.Defender,
        },
        {
          number: 26,
          country: null,
          id: 'sr:player:957322',
          name: 'Cristian Borja',
          position: PlayerPosition.Defender,
        },
      ],
      forward: [
        {
          number: 23,
          country: null,
          id: 'sr:player:115718',
          name: 'Abdoulay Diaby',
          position: PlayerPosition.Forward,
        },
        {
          number: 9,
          country: null,
          id: 'sr:player:578560',
          name: 'Marcos Acuna',
          position: PlayerPosition.Forward,
        },
        {
          number: 29,
          country: null,
          id: 'sr:player:291707',
          name: 'Luiz Phellype',
          position: PlayerPosition.Forward,
        },
      ],
      goalkeeper: [
        {
          number: 40,
          country: null,
          id: 'sr:player:82598',
          name: 'Renan Ribeiro',
          position: PlayerPosition.Goalkeeper,
        },
      ],
      midfielder: [
        {
          number: 25,
          country: null,
          id: 'sr:player:39586',
          name: 'Radosav Petrovic',
          position: PlayerPosition.Midfielder,
        },
        {
          number: 86,
          country: null,
          id: 'sr:player:68332',
          name: 'Nemanja Gudelj',
          position: PlayerPosition.Midfielder,
        },
        {
          number: 8,
          country: null,
          id: 'sr:player:288205',
          name: 'Bruno Fernandes',
          position: PlayerPosition.Midfielder,
        },
      ],
    },
    substitutes: [
      {
        number: 19,
        country: null,
        id: 'sr:player:27069',
        name: 'Romain Salin',
        position: PlayerPosition.Goalkeeper,
      },
      {
        number: 3,
        country: null,
        id: 'sr:player:167655',
        name: 'Tiago Ilori',
        position: PlayerPosition.Defender,
      },
      {
        number: 5,
        country: null,
        id: 'sr:player:39693',
        name: 'Jefferson',
        position: PlayerPosition.Defender,
      },
      {
        number: 37,
        country: null,
        id: 'sr:player:1121457',
        name: 'Wendel',
        position: PlayerPosition.Midfielder,
      },
      {
        number: 21,
        country: null,
        id: 'sr:player:956430',
        name: 'Raphinha',
        position: PlayerPosition.Forward,
      },
      {
        number: 28,
        country: null,
        id: 'sr:player:32260',
        name: 'Bas Dost',
        position: PlayerPosition.Forward,
      },
      {
        number: 77,
        country: null,
        id: 'sr:player:1013609',
        name: 'Jovane Cabral',
        position: PlayerPosition.Forward,
      },
    ],
  },
}

export const TEST_MATCH_ROOT: MatchRoot & MatchListItem = {
  matchStatus: null,
  id: 'sr:match:12321321',
  score: {
    home: 2,
    away: 1,
  },
  venue: {
    id: 'sr:venue:907',
    name: 'Estadio do Dragao',
  },
  reactionEndTime: '2019-05-25T17:30:00.000Z',
  teams: {
    home: {
      country: 'PT',
      id: 'sr:competitor:3002',
      name: 'FC Porto',
    },
    away: {
      country: 'PT',
      id: 'sr:competitor:3001',
      name: 'Sporting CP',
    },
  },
  reactionStartTime: '2019-05-17T17:30:00.000Z',
  startTime: '2019-05-18T17:30:00+00:00',
  state: MatchState.PreMatch,
  tournament: {
    id: 'sr:tournament:35',
    name: 'Bundesliga',
  },
  season: {
    id: 'sr:season:55017',
    name: 'Bundesliga 18/19',
  },
  date: '2019-05-04',
  referee: { name: 'Reffie', id: '666', country: 'NK' },
}

// const TEST_MATCH: MatchListItem = {
//   season: {
//     id: 'sr:season:55017',
//     name: 'Bundesliga 18/19',
//   },
//   venue: {
//     id: 'sr:venue:821',
//     name: 'Weserstadion',
//   },
//   id: 'sr:match:14894623',
//   tournament: {
//     id: 'sr:tournament:35',
//     name: 'Bundesliga',
//   },
//   teams: {
//     home: {
//       country: 'DE',
//       id: 'sr:competitor:2673',
//       name: 'Borussia Dortmund',
//     },
//     away: {
//       country: 'DE',
//       id: 'sr:competitor:2534',
//       name: 'Werder Bremen',
//     },
//   },
//   date: '2019-05-04',
//   startTime: '2019-05-04T16:30:00+00:00',
// }

const TEST_VOTE_HISTORY: UserVoteHistory = {
  'matchEvent---event-559084705': {
    response: 'no',
    timestamp: '2017-12-31T23:12:12Z',
  },
  'matchEvent---event-559084807': {
    response: 'no',
    timestamp: '2017-12-31T23:12:12Z',
  },
  'matchEvent---event-559086645': {
    response: 'no',
    timestamp: '2017-12-31T23:12:12Z',
  },
  'matchEvent---event-559100035': {
    response: 'no',
    timestamp: '2017-12-31T23:12:12Z',
  },
  'matchEvent---event-559103199': {
    response: 'no',
    timestamp: '2017-12-31T23:12:12Z',
  },
  'matchEvent---event-559103833': {
    response: 'no',
    timestamp: '2017-12-31T23:12:12Z',
  },
  'matchEvent---event-559104453': {
    response: 'no',
    timestamp: '2017-12-31T23:12:12Z',
  },
  'matchEvent---event-559104485': {
    response: 'no',
    timestamp: '2017-12-31T23:12:12Z',
  },
  'managerPerformance---match-sr:match:12321321---team-sr:competitor:3002': {
    response: 2.5,
    timestamp: '2017-12-31T23:12:12Z',
  },
  'teamPerformance---match-sr:match:12321321---team-sr:competitor:3002': {
    response: 4,
    timestamp: '2017-12-31T23:12:12Z',
  },
}

const TEST_MATCH_EVENT_HOME_VOTE_RESULTS: MatchEventResultsSummaryDocument = {
  '534229204': { result: { yes: 0.3, no: 0.7 }, voteCount: 12 },
  '534236680': { result: { yes: 0.36, no: 0.64 }, voteCount: 12 },
  '534240734': { result: { yes: 0.1, no: 0.9 }, voteCount: 12 },
  '534245108': { result: { yes: 0.43, no: 0.57 }, voteCount: 12 },
  '534246632': { result: { yes: 0.54, no: 0.46 }, voteCount: 12 },
  '534276694': { result: { yes: 0.61, no: 0.39 }, voteCount: 12 },
  '534282898': { result: { yes: 0.41, no: 0.59 }, voteCount: 12 },
  '534283880': { result: { yes: 0.123, no: 0.877 }, voteCount: 12 },
}

const TEST_MATCH_EVENT_AWAY_VOTE_RESULTS: MatchEventResultsSummaryDocument = {
  '534187950': { result: { yes: 0.3, no: 0.7 }, voteCount: 12 },
  '534226764': { result: { yes: 0.36, no: 0.64 }, voteCount: 12 },
  '534227906': { result: { yes: 0.1, no: 0.9 }, voteCount: 12 },
  '534244076': { result: { yes: 0.43, no: 0.57 }, voteCount: 12 },
  '534247438': { result: { yes: 0.54, no: 0.46 }, voteCount: 12 },
  '534282510': { result: { yes: 0.61, no: 0.39 }, voteCount: 12 },
  '534525526': { result: { yes: 0.41, no: 0.59 }, voteCount: 12 },
  '534525528': { result: { yes: 0.123, no: 0.877 }, voteCount: 12 },
}

const TEST_MATCH_EVENT_VOTE_RESULTS = {
  home: TEST_MATCH_EVENT_HOME_VOTE_RESULTS,
  away: TEST_MATCH_EVENT_AWAY_VOTE_RESULTS,
}

const TEST_HOME_TEAM_PERFORMANCE: TeamPerformancelResultDocument = {
  rating: 2.7,
  voteCount: 324,
}
const TEST_AWAY_TEAM_PERFORMANCE: TeamPerformancelResultDocument = {
  rating: 0.7,
  voteCount: 324,
}

const TEST_TEAM_PERFORMANCE = {
  home: TEST_HOME_TEAM_PERFORMANCE,
  away: TEST_AWAY_TEAM_PERFORMANCE,
}

const TEST_HOME_MANAGER_PERFORMANCE: ManagerPerformanceResultDocument = {
  rating: 4.8,
  voteCount: 324,
}
const TEST_AWAY_MANAGER_PERFORMANCE: ManagerPerformanceResultDocument = {
  rating: 1.8,
  voteCount: 324,
}

const TEST_MANAGER_PERFORMANCE = {
  home: TEST_HOME_MANAGER_PERFORMANCE,
  away: TEST_AWAY_MANAGER_PERFORMANCE,
}

const TEST_LINEUP_RESULT = {
  players: {
    'sr:player:1047385': 0.2, // G
    'sr:player:37813': 0.3, // G
    'sr:player:253515': 0.6, // G
    'sr:player:37378': 0.1, // G
    'sr:player:26760': 0.29, // D
    'sr:player:903858': 0.47, // D
    'sr:player:280435': 0.28, // D
    'sr:player:246999': 0.24, // D
    'sr:player:39015': 0.22, // D
    'sr:player:907834': 0.01, // D
    'sr:player:383560': 0.44, // D
    'sr:player:341589': 0.22, // M
    'sr:player:35612': 0.25, // M
    'sr:player:809758': 0.21, // M
    'sr:player:66477': 0.27, // M
    'sr:player:1051087': 0.23, // M
    'sr:player:24829': 0.32, // M
    'sr:player:756428': 0.223, // M
    'sr:player:66518': 0.262, // M
    'sr:player:254119': 0.24, // M
    'sr:player:237068': 0.27, // M
    'sr:player:1419559': 0.21, // M
    'sr:player:171927': 0.9, // F
    'sr:player:37092': 0.94, // F
    'sr:player:96366': 0.85, // F
    'sr:player:1047147': 0.9, // F
  },
  voteCount: 123,
}

const TEST_ACTIVE_PLAYERS: MatchActivePlayers = {
  home: [
    {
      country: 'CH',
      number: 31,
      id: 'sr:player:318941',
      name: 'Eric Oelschlagel',
      position: PlayerPosition.Defender,
    },
    {
      country: 'CH',
      number: 213,
      id: 'sr:player:47549',
      name: 'Marcel Schmelzer',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:178407',
      country: 'CH',
      number: 16,
      name: 'Manuel Akanji',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:47906',
      country: 'FR',
      number: 2,
      name: 'Dan Axel Zagadou',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:557008',
      country: 'TR',
      number: 36,
      name: 'Omer Toprak',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:188133',
      country: 'AR',
      number: 18,
      name: 'Leonardo Balerdi',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:37378',
      country: 'CH',
      number: 1,
      name: 'Roman Burki',
      position: PlayerPosition.Goalkeeper,
    },
    {
      id: 'sr:player:237068',
      country: 'DE',
      number: 33,
      name: 'Julian Weigl',
      position: PlayerPosition.Midfielder,
    },
    {
      id: 'sr:player:96366',
      country: 'ES',
      number: 9,
      name: 'Paco Alcacer',
      position: PlayerPosition.Forward,
    },
    {
      id: 'sr:player:254119',
      country: 'DE',
      number: 27,
      name: 'Marius Wolf',
      position: PlayerPosition.Midfielder,
    },
    {
      id: 'sr:player:171927',
      country: 'DE',
      number: 20,
      name: 'Maximilian Philipp',
      position: PlayerPosition.Forward,
    },
    {
      id: 'sr:player:66518',
      country: 'DE',
      number: 10,
      name: 'Mario Gotze',
      position: PlayerPosition.Midfielder,
    },
    {
      id: 'sr:player:37813',
      country: 'CH',
      number: 35,
      name: 'Marwin Hitz',
      position: PlayerPosition.Goalkeeper,
    },
    {
      id: 'sr:player:1047147',
      country: null,
      number: 7,
      name: 'Jadon Sancho',
      position: PlayerPosition.Forward,
    },
    {
      id: 'sr:player:37092',
      country: 'DE',
      number: 11,
      name: 'Marco Reus',
      position: PlayerPosition.Forward,
    },
  ],
  away: [
    {
      country: 'CH',
      number: 31,
      id: 'sr:player:318941',
      name: 'Eric Oelschlagel',
      position: PlayerPosition.Defender,
    },
    {
      country: 'CH',
      number: 213,
      id: 'sr:player:47549',
      name: 'Marcel Schmelzer',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:178407',
      country: 'CH',
      number: 16,
      name: 'Manuel Akanji',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:47906',
      country: 'FR',
      number: 2,
      name: 'Dan Axel Zagadou',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:557008',
      country: 'TR',
      number: 36,
      name: 'Omer Toprak',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:188133',
      country: 'AR',
      number: 18,
      name: 'Leonardo Balerdi',
      position: PlayerPosition.Defender,
    },
    {
      id: 'sr:player:37378',
      country: 'CH',
      number: 1,
      name: 'Roman Burki',
      position: PlayerPosition.Goalkeeper,
    },
    {
      id: 'sr:player:237068',
      country: 'DE',
      number: 33,
      name: 'Julian Weigl',
      position: PlayerPosition.Midfielder,
    },
    {
      id: 'sr:player:96366',
      country: 'ES',
      number: 9,
      name: 'Paco Alcacer',
      position: PlayerPosition.Forward,
    },
    {
      id: 'sr:player:254119',
      country: 'DE',
      number: 27,
      name: 'Marius Wolf',
      position: PlayerPosition.Midfielder,
    },
    {
      id: 'sr:player:171927',
      country: 'DE',
      number: 20,
      name: 'Maximilian Philipp',
      position: PlayerPosition.Forward,
    },
    {
      id: 'sr:player:66518',
      country: 'DE',
      number: 10,
      name: 'Mario Gotze',
      position: PlayerPosition.Midfielder,
    },
    {
      id: 'sr:player:37813',
      country: 'CH',
      number: 35,
      name: 'Marwin Hitz',
      position: PlayerPosition.Goalkeeper,
    },
    {
      id: 'sr:player:1047147',
      country: null,
      number: 7,
      name: 'Jadon Sancho',
      position: PlayerPosition.Forward,
    },
    {
      id: 'sr:player:37092',
      country: 'DE',
      number: 11,
      name: 'Marco Reus',
      position: PlayerPosition.Forward,
    },
  ],
}

const TEST_AVAILABLE_TEAMS: ChosenTeamList = {
  'sr:competitor:2673': {
    id: 'sr:competitor:2673',
    name: 'Borussia Dortmund',
    country: 'DE',
    isFavorite: true,
  },
}

const TEST_HOME_TEAM_ROOT: TeamRoot = {
  country: 'DE',
  id: 'sr:competitor:2673',
  name: 'Borussia Dortmund',
  jerseyColors: { base: 'red', number: 'brown', sleeve: 'blue' },
  defaultTournamentId: 'sr:tournament:35',
  defaultCurrentSeasonId: 'sr:season:55017',
  manager: {
    name: 'blaaa manager',
    id: 'sr:player:666',
  },
}
const TEST_AWAY_TEAM_ROOT: TeamRoot = {
  country: 'DE',
  id: 'sr:competitor:2534',
  name: 'Werder Bremen',
  jerseyColors: { base: 'purple', number: 'yellow', sleeve: 'pink' },
  defaultTournamentId: 'sr:tournament:35',
  defaultCurrentSeasonId: 'sr:season:55017',
  manager: {
    name: 'lalala manager',
    id: 'sr:player:777',
  },
}

const timeDifference = 7 * 24 * 60 * 60 * 1000

const DATE_IN_PAST = new Date(Date.now() - timeDifference).toISOString()

const DATE_IN_FUTURE = new Date(Date.now() + timeDifference).toISOString()

storiesOf('MatchScreen', module)
  .add('favorite, no votes, no results', () => (
    <MatchScreen
      timeline={TEST_TIMELINE}
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      teamStatistics={TEST_TEAM_STATISTICS}
      lineups={TEST_LINEUPS}
      matchRoot={TEST_MATCH_ROOT}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: true,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      lineupSuggestionResult={{
        home: undefined,
        away: undefined,
      }}
      mostValuablePlayersResult={{ home: undefined, away: undefined }}
      matchEventVoteResults={{ home: undefined, away: undefined }}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      match={TEST_MATCH_ROOT}
      voteHistory={{}}
      availableTeams={TEST_AVAILABLE_TEAMS}
    />
  ))
  .add('favorite, no votes, results', () => (
    <MatchScreen
      timeline={TEST_TIMELINE}
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      teamStatistics={TEST_TEAM_STATISTICS}
      lineups={TEST_LINEUPS}
      matchRoot={TEST_MATCH_ROOT}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: true,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      match={TEST_MATCH_ROOT}
      voteHistory={{}}
      matchEventVoteResults={TEST_MATCH_EVENT_VOTE_RESULTS}
      teamPerformanceResult={TEST_TEAM_PERFORMANCE}
      managerPerformanceResult={TEST_MANAGER_PERFORMANCE}
      lineupSuggestionResult={{
        home: TEST_LINEUP_RESULT,
        away: TEST_LINEUP_RESULT,
      }}
      mostValuablePlayersResult={{ home: undefined, away: undefined }}
      activePlayers={TEST_ACTIVE_PLAYERS}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      availableTeams={TEST_AVAILABLE_TEAMS}
      // most valuable
    />
  ))
  .add('favorite, votes, results', () => (
    <MatchScreen
      timeline={TEST_TIMELINE}
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      teamStatistics={TEST_TEAM_STATISTICS}
      lineups={TEST_LINEUPS}
      matchRoot={TEST_MATCH_ROOT}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: true,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      match={TEST_MATCH_ROOT}
      voteHistory={TEST_VOTE_HISTORY}
      matchEventVoteResults={TEST_MATCH_EVENT_VOTE_RESULTS}
      teamPerformanceResult={TEST_TEAM_PERFORMANCE}
      managerPerformanceResult={TEST_MANAGER_PERFORMANCE}
      lineupSuggestionResult={{
        home: TEST_LINEUP_RESULT,
        away: TEST_LINEUP_RESULT,
      }}
      mostValuablePlayersResult={{ home: undefined, away: undefined }}
      activePlayers={TEST_ACTIVE_PLAYERS}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      availableTeams={TEST_AVAILABLE_TEAMS}
      //  most valuable
    />
  ))
  .add('not favorite, votes, results', () => (
    <MatchScreen
      timeline={TEST_TIMELINE}
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      teamStatistics={TEST_TEAM_STATISTICS}
      lineups={TEST_LINEUPS}
      matchRoot={TEST_MATCH_ROOT}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: false,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      match={TEST_MATCH_ROOT}
      voteHistory={TEST_VOTE_HISTORY}
      matchEventVoteResults={TEST_MATCH_EVENT_VOTE_RESULTS}
      teamPerformanceResult={TEST_TEAM_PERFORMANCE}
      managerPerformanceResult={TEST_MANAGER_PERFORMANCE}
      lineupSuggestionResult={{
        home: TEST_LINEUP_RESULT,
        away: TEST_LINEUP_RESULT,
      }}
      mostValuablePlayersResult={{ home: undefined, away: undefined }}
      activePlayers={TEST_ACTIVE_PLAYERS}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      availableTeams={TEST_AVAILABLE_TEAMS}
      // most valuable
    />
  ))
  // PRE MATCH
  .add('pre match, before allowing votes', () => (
    <MatchScreen
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      matchRoot={{
        ...TEST_MATCH_ROOT,
        state: MatchState.PreMatch,
        reactionStartTime: DATE_IN_FUTURE,
        reactionEndTime: DATE_IN_FUTURE,
      }}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: true,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      match={TEST_MATCH_ROOT}
      voteHistory={{}}
      matchEventVoteResults={{ home: undefined, away: undefined }}
      lineupSuggestionResult={{
        home: TEST_LINEUP_RESULT,
        away: TEST_LINEUP_RESULT,
      }}
      mostValuablePlayersResult={{ home: undefined, away: undefined }}
      teamPerformanceResult={{ home: undefined, away: undefined }}
      managerPerformanceResult={{ home: undefined, away: undefined }}
      activePlayers={TEST_ACTIVE_PLAYERS}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      availableTeams={TEST_AVAILABLE_TEAMS}
    />
  ))
  .add('pre match, after allowing votes', () => (
    <MatchScreen
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      matchRoot={{
        ...TEST_MATCH_ROOT,
        state: MatchState.PreMatch,
        reactionStartTime: DATE_IN_PAST,
        reactionEndTime: DATE_IN_FUTURE,
      }}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: true,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      match={TEST_MATCH_ROOT}
      voteHistory={{}}
      matchEventVoteResults={{ home: undefined, away: undefined }}
      lineupSuggestionResult={{
        home: TEST_LINEUP_RESULT,
        away: TEST_LINEUP_RESULT,
      }}
      mostValuablePlayersResult={{ home: undefined, away: undefined }}
      teamPerformanceResult={{ home: undefined, away: undefined }}
      managerPerformanceResult={{ home: undefined, away: undefined }}
      activePlayers={TEST_ACTIVE_PLAYERS}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      availableTeams={TEST_AVAILABLE_TEAMS}
    />
  ))

  // LIVE MATCH
  .add('live match', () => (
    <MatchScreen
      timeline={TEST_TIMELINE}
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      teamStatistics={TEST_TEAM_STATISTICS}
      lineups={TEST_LINEUPS}
      matchRoot={{
        ...TEST_MATCH_ROOT,
        state: MatchState.Live,
        reactionStartTime: DATE_IN_PAST,
        reactionEndTime: DATE_IN_FUTURE,
        matchStatus: {
          current: MatchStatusType.SecondHalf,
          time: new Date(Date.now() - 30 * 60 * 1000).toISOString(),
        },
      }}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: true,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      match={TEST_MATCH_ROOT}
      voteHistory={{}}
      matchEventVoteResults={TEST_MATCH_EVENT_VOTE_RESULTS}
      lineupSuggestionResult={{
        home: TEST_LINEUP_RESULT,
        away: TEST_LINEUP_RESULT,
      }}
      mostValuablePlayersResult={{ home: undefined, away: undefined }}
      teamPerformanceResult={{ home: undefined, away: undefined }}
      managerPerformanceResult={{ home: undefined, away: undefined }}
      activePlayers={TEST_ACTIVE_PLAYERS}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      availableTeams={TEST_AVAILABLE_TEAMS}
    />
  ))
  .add('live match overtime', () => (
    <MatchScreen
      timeline={TEST_TIMELINE as MatchTimeline}
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      teamStatistics={TEST_TEAM_STATISTICS}
      lineups={TEST_LINEUPS}
      matchRoot={{
        ...TEST_MATCH_ROOT,
        state: MatchState.Live,
        reactionStartTime: DATE_IN_PAST,
        reactionEndTime: DATE_IN_FUTURE,
        matchStatus: {
          current: MatchStatusType.SecondHalf,
          time: new Date(Date.now() - 50 * 60 * 1000).toISOString(),
        },
      }}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: true,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      match={TEST_MATCH_ROOT}
      voteHistory={{}}
      matchEventVoteResults={TEST_MATCH_EVENT_VOTE_RESULTS}
      lineupSuggestionResult={{
        home: TEST_LINEUP_RESULT,
        away: TEST_LINEUP_RESULT,
      }}
      mostValuablePlayersResult={{ home: undefined, away: undefined }}
      teamPerformanceResult={{ home: undefined, away: undefined }}
      managerPerformanceResult={{ home: undefined, away: undefined }}
      activePlayers={TEST_ACTIVE_PLAYERS}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      availableTeams={TEST_AVAILABLE_TEAMS}
    />
  ))
  // POST MATCH
  .add('post match, before expiration', () => (
    <MatchScreen
      timeline={TEST_TIMELINE}
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      teamStatistics={TEST_TEAM_STATISTICS}
      lineups={TEST_LINEUPS}
      matchRoot={{
        ...TEST_MATCH_ROOT,
        state: MatchState.PostMatch,
        reactionStartTime: DATE_IN_PAST,
        reactionEndTime: DATE_IN_FUTURE,
        matchStatus: {
          current: MatchStatusType.Ended,
        },
      }}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: true,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      match={TEST_MATCH_ROOT}
      voteHistory={{}}
      matchEventVoteResults={TEST_MATCH_EVENT_VOTE_RESULTS}
      teamPerformanceResult={TEST_TEAM_PERFORMANCE}
      managerPerformanceResult={TEST_MANAGER_PERFORMANCE}
      lineupSuggestionResult={{
        home: TEST_LINEUP_RESULT,
        away: TEST_LINEUP_RESULT,
      }}
      activePlayers={TEST_ACTIVE_PLAYERS}
      mostValuablePlayersResult={{
        home: TEST_TOP_PLAYERS,
        away: TEST_TOP_PLAYERS,
      }}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      availableTeams={TEST_AVAILABLE_TEAMS}
    />
  ))
  .add('post match, after expiration', () => (
    <MatchScreen
      timeline={TEST_TIMELINE}
      seasonStandings={TEST_SEASON_STANDINGS}
      teamPopularity={TEST_TEAM_POPULARITY}
      teamStatistics={TEST_TEAM_STATISTICS}
      lineups={TEST_LINEUPS}
      matchRoot={{
        ...TEST_MATCH_ROOT,
        state: MatchState.PostMatch,
        reactionStartTime: DATE_IN_PAST,
        reactionEndTime: DATE_IN_PAST,
        matchStatus: {
          current: MatchStatusType.Ended,
        },
      }}
      activeTeam={{
        country: 'PT',
        id: 'sr:competitor:3002',
        name: 'FC Porto',
        isFavorite: true,
      }}
      onBack={action('onBack')}
      onSelectPlayer={action('onSelectPlayer')}
      onSelectTeam={action('onSelectTeam')}
      match={TEST_MATCH_ROOT}
      voteHistory={{}}
      matchEventVoteResults={TEST_MATCH_EVENT_VOTE_RESULTS}
      teamPerformanceResult={TEST_TEAM_PERFORMANCE}
      managerPerformanceResult={TEST_MANAGER_PERFORMANCE}
      lineupSuggestionResult={{
        home: TEST_LINEUP_RESULT,
        away: TEST_LINEUP_RESULT,
      }}
      activePlayers={TEST_ACTIVE_PLAYERS}
      mostValuablePlayersResult={{
        home: TEST_TOP_PLAYERS,
        away: TEST_TOP_PLAYERS,
      }}
      onSelectManager={action('onSelectManager')}
      onVote={action('onVote')}
      teamPlayers={{ home: TEST_TEAM_PLAYERS, away: TEST_TEAM_PLAYERS }}
      teamRoot={{ home: TEST_HOME_TEAM_ROOT, away: TEST_AWAY_TEAM_ROOT }}
      availableTeams={TEST_AVAILABLE_TEAMS}
    />
  ))

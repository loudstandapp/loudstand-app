import React from 'react'
import { compose, Store, Dispatch, bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import {
  LocalizedTeams,
  SeasonStandings,
  MatchTimeline,
  MatchListItem,
  MatchTeamStatistics,
  MatchRoot,
  MatchLineup,
  ID,
  PlayerList,
  UserVoteHistory,
  MatchEventResultsSummaryDocument,
  TeamPerformancelResultDocument,
  ManagerPerformanceResultDocument,
  LineupSuggestionResultDocument,
  MatchActivePlayers,
  MostValuablePlayerResultDocument,
  SubmittedVote,
  TeamRoot,
  SeasonTeamPopularityDocument,
  MatchState,
} from '@loudstand/shared'
import { firestoreConnect } from 'react-redux-firebase'
import LinearGradient from 'react-native-linear-gradient'
import {
  StyleSheet,
  View,
  Dimensions,
  Image,
  ImageRequireSource,
  ListRenderItemInfo,
  Text,
  FlatList,
  NativeSyntheticEvent,
  NativeScrollEvent,
  Animated,
  TouchableOpacity,
} from 'react-native'
import { Options } from 'react-native-navigation'
import moment from 'moment'
import {
  ReduxState,
  ChosenTeamListItem,
  SubmittedVotes,
  ChosenTeamList,
  DismissedGuides,
} from '../../../store/model'
import colors from '../../../theme/colors'
import { translate, currentLocale } from '../../../translation'
import { spacingUnit, marginDefault } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import Standings from '../../stats/components/Standings'
import Timeline from '../components/Timeline'
import Lineups from '../components/Lineups'
import TeamStatistics from '../components/TeamStatistics'
import MatchTopBar from '../components/MatchTopBar'
import Loudstand from '../components/Loudstand'
import * as fq from '../../../store/firestoreQueries'
import { votingFlowThunk } from '../../user/votingFlow'
import { cheats } from '../../../utils'
import ScreenWrapper from '../../../components/ScreenWrapper'
import * as selectors from '../../../store/selectors'
import Guide, { GuideType } from '../../../components/Guide'
import { dismissGuide } from '../../../store/actions'

const currentLanguageCode = currentLocale.languageCode

interface Slide {
  key: string
  type: SlideType
  icon: ImageRequireSource
}

enum SlideType {
  Lineups = 'LINEUPS',
  SeasonStandings = 'SEASON_STANDINGS',
  Loudstand = 'LOUDSTAND',
  TeamStats = 'TEAM_STATS',
  Timeline = 'TIMELINE',
}

const slides: Slide[] = [
  {
    key: 'matchNavigation.statistics',
    type: SlideType.TeamStats,
    icon: require('../../../../assets/icons/navigation/statistics.png'),
  },
  {
    key: 'matchNavigation.timeline',
    type: SlideType.Timeline,
    icon: require('../../../../assets/icons/navigation/summary.png'),
  },
  {
    key: 'matchNavigation.loudstand',
    type: SlideType.Loudstand,
    icon: require('../../../../assets/icons/navigation/loudstand.png'),
  },
  {
    key: 'matchNavigation.lineups',
    type: SlideType.Lineups,
    icon: require('../../../../assets/icons/navigation/formations.png'),
  },
  {
    key: 'matchNavigation.standings',
    type: SlideType.SeasonStandings,
    icon: require('../../../../assets/icons/navigation/standings.png'),
  },
]

const INITIAL_SLIDE = 2

export interface NavProps {
  match: MatchListItem
  onSelectPlayer: (id: ID) => void
  onSelectManager: (id: ID) => void
  onSelectTeam: (id: ID) => void
  onBack: () => void
}

interface ReduxProps {
  teamStatistics?: MatchTeamStatistics
  matchRoot?: MatchRoot
  lineups?: MatchLineup
  seasonStandings?: SeasonStandings
  localizedTeams?: LocalizedTeams
  timeline?: MatchTimeline
  activeTeam?: ChosenTeamListItem
  voteHistory?: UserVoteHistory
  activePlayers?: MatchActivePlayers
  submittedVotes?: SubmittedVotes
  teamPopularity?: SeasonTeamPopularityDocument
  availableTeams: ChosenTeamList
  isLoading: boolean
  isGenerating: boolean
  teamRoot: {
    home: TeamRoot | undefined
    away: TeamRoot | undefined
  }
  teamPlayers: { home: PlayerList | undefined; away: PlayerList | undefined }
  matchEventVoteResults: {
    home: MatchEventResultsSummaryDocument | undefined
    away: MatchEventResultsSummaryDocument | undefined
  }
  teamPerformanceResult: {
    home: TeamPerformancelResultDocument | undefined
    away: TeamPerformancelResultDocument | undefined
  }
  managerPerformanceResult: {
    home: ManagerPerformanceResultDocument | undefined
    away: ManagerPerformanceResultDocument | undefined
  }
  lineupSuggestionResult: {
    home: LineupSuggestionResultDocument | undefined
    away: LineupSuggestionResultDocument | undefined
  }
  mostValuablePlayersResult: {
    home: MostValuablePlayerResultDocument | undefined
    away: MostValuablePlayerResultDocument | undefined
  }
  dismissedGuides: DismissedGuides
  launchCount: number
}
interface ReduxActionProps {
  onVote: (vote: SubmittedVote) => void
  onDismissGuide: (type: GuideType) => void
}

interface Props extends NavProps, ReduxProps, ReduxActionProps {}

interface State {
  activeIndex: number
}

const { width } = Dimensions.get('screen')

const styles = StyleSheet.create({
  topBarContentWrapper: {
    height: 100,
    flex: 1,
    backgroundColor: 'purple',
  },
  slideIndicatorBar: {
    backgroundColor: colors.navigationBackground,
    flex: 0,
    flexDirection: 'row',
  },
  activeSliderItem: {
    width: width / slides.length,
    ...StyleSheet.absoluteFillObject,
  },
  swiper: {
    flex: 1,
  },
  slide: {
    width,
    flex: 1,
  },
  sliderItem: {
    width: width / slides.length,
    padding: spacingUnit / 2,
    alignItems: 'center',
  },
  sliderItemIcon: {
    tintColor: colors.secondaryText,
    marginBottom: spacingUnit / 2,
  },
  activeSliderItemGradient: { flex: 1 },
  sliderItemName: {
    ...(fonts.Navigation as object),
    color: colors.secondaryText,
  },
  loudstandWarningContainer: {
    margin: marginDefault,
    alignItems: 'center',
  },
  loudstandWarningText: {
    ...(fonts.BodyLarge as object),
    margin: marginDefault,
  },
})

export class MatchScreen extends React.Component<Props, State> {
  static options = (): Options => ({
    bottomTabs: {
      visible: false,
      drawBehind: true,
    },
  })

  indicatorPosition = new Animated.Value(0)

  carouselRef = React.createRef<FlatList<Slide>>()

  handleScroll = Animated.event([
    { nativeEvent: { contentOffset: { x: this.indicatorPosition } } },
  ])

  renderSlide = (type: SlideType) => {
    const {
      teamStatistics,
      matchRoot,
      lineups,
      seasonStandings,
      localizedTeams,
      timeline,
      onSelectPlayer,
      onSelectTeam,
      activeTeam,
      voteHistory,
      matchEventVoteResults,
      teamPerformanceResult,
      managerPerformanceResult,
      lineupSuggestionResult,
      mostValuablePlayersResult,
      activePlayers,
      onVote,
      teamRoot,
      teamPlayers,
      onSelectManager,
      submittedVotes,
      teamPopularity,
      availableTeams,
    } = this.props

    switch (type) {
      case SlideType.SeasonStandings: {
        const playingTeams =
          matchRoot && matchRoot.teams
            ? [matchRoot.teams.home, matchRoot.teams.away]
            : []
        return seasonStandings ? (
          <Standings
            standingGroups={seasonStandings}
            playingTeams={playingTeams}
            localizedTeams={localizedTeams}
            onSelectTeam={onSelectTeam}
            teamPopularity={teamPopularity}
          />
        ) : (
          this.renderNoData()
        )
      }
      case SlideType.Timeline: {
        return timeline ? (
          <Timeline timeline={timeline} onSelectPlayer={onSelectPlayer} />
        ) : (
          this.renderNoData()
        )
      }
      case SlideType.Lineups: {
        return lineups && matchRoot ? (
          <Lineups
            onSelectPlayer={onSelectPlayer}
            onSelectManager={onSelectManager}
            lineups={lineups}
            matchRoot={matchRoot}
            localizedTeams={localizedTeams}
          />
        ) : (
          this.renderNoData()
        )
      }
      case SlideType.TeamStats: {
        return teamStatistics ? (
          <TeamStatistics teamStatistics={teamStatistics} lineups={lineups} />
        ) : (
          this.renderNoData()
        )
      }
      case SlideType.Loudstand: {
        const now = new Date().toISOString()
        if (
          !cheats.alwaysAllowVoting &&
          matchRoot &&
          (!matchRoot.reactionStartTime || matchRoot.reactionStartTime > now)
        ) {
          return this.renderNotOpenForVotingYet()
        }
        const teamRoots = teamRoot.away &&
          teamRoot.home && { home: teamRoot.home, away: teamRoot.away }
        if (
          !teamRoots ||
          (!teamRoots.home.defaultTournamentId &&
            !teamRoots.away.defaultTournamentId)
        ) {
          return this.renderNoVotableTeams()
        }
        return (
          activeTeam &&
          teamRoot &&
          teamPlayers &&
          matchRoot &&
          teamRoots && (
            <Loudstand
              availableTeams={availableTeams}
              activeTeam={activeTeam}
              teamPlayers={teamPlayers}
              matchRoot={matchRoot}
              timeline={timeline}
              voteHistory={voteHistory}
              matchEventVoteResults={matchEventVoteResults}
              localizedTeams={localizedTeams}
              teamPerformanceResult={teamPerformanceResult}
              managerPerformanceResult={managerPerformanceResult}
              lineupSuggestionResult={lineupSuggestionResult}
              mostValuablePlayersResult={mostValuablePlayersResult}
              lineups={lineups}
              activePlayers={activePlayers}
              onVote={onVote}
              submittedVotes={submittedVotes}
              teamRoot={teamRoots}
              onSelectManager={onSelectManager}
              onSelectTeam={onSelectTeam}
              onSelectPlayer={onSelectPlayer}
            />
          )
        )
      }
      default:
        return false
    }
  }

  renderNotOpenForVotingYet = () => {
    const { matchRoot } = this.props
    return (
      <View style={styles.loudstandWarningContainer}>
        <Text style={styles.loudstandWarningText}>
          {translate('loudstand.nothingYet', {
            time:
              matchRoot && matchRoot.reactionStartTime
                ? moment(matchRoot.reactionStartTime).fromNow()
                : 'N/A',
          })}
        </Text>
      </View>
    )
  }

  renderNoVotableTeams = () => {
    return (
      <View style={styles.loudstandWarningContainer}>
        <Text style={styles.loudstandWarningText}>
          {translate('loudstand.noVotableTeams')}
        </Text>
      </View>
    )
  }

  renderNoData = () => {
    const { matchRoot } = this.props
    if (!matchRoot || matchRoot.state === MatchState.PreMatch) {
      return (
        <View style={styles.loudstandWarningContainer}>
          <Text style={styles.loudstandWarningText}>
            {translate('match.noDataYet')}
          </Text>
        </View>
      )
    }
    return (
      <View style={styles.loudstandWarningContainer}>
        <Text style={styles.loudstandWarningText}>
          {translate('match.noDataAvailable')}
        </Text>
      </View>
    )
  }

  renderItem = ({ item }: ListRenderItemInfo<Slide>) => {
    return <View style={styles.slide}>{this.renderSlide(item.type)}</View>
  }

  handleScrollDragEnd = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    const activeIndex = Math.round(event.nativeEvent.contentOffset.x / width)
    this.setState({ activeIndex })
  }

  handleMomentumScrollEnd = (
    event: NativeSyntheticEvent<NativeScrollEvent>
  ) => {
    ReactNativeHapticFeedback.trigger('selection')
    const activeIndex = Math.round(event.nativeEvent.contentOffset.x / width)
    this.setState({ activeIndex })
  }

  gotoSlide = (index: number) => {
    ReactNativeHapticFeedback.trigger('selection')

    this.carouselRef.current &&
      this.carouselRef.current.scrollToIndex({ index, animated: true })
  }

  render = () => {
    const {
      matchRoot,
      onBack,
      isLoading,
      isGenerating,
      onSelectTeam,
      dismissedGuides,
      onDismissGuide,
      launchCount,
    } = this.props
    return (
      <ScreenWrapper isLoading={isLoading} isGenerating={isGenerating}>
        <MatchTopBar
          onBack={onBack}
          matchRoot={matchRoot}
          onSelectTeam={onSelectTeam}
        />
        <View style={styles.slideIndicatorBar}>
          <Animated.View
            style={[
              styles.activeSliderItem,
              {
                left: this.indicatorPosition.interpolate({
                  inputRange: [0, width * slides.length],
                  outputRange: [0, width],
                }),
              },
            ]}
          >
            <LinearGradient
              colors={[colors.primary, colors.primaryAlt]}
              end={{ x: 1, y: 0 }}
              style={styles.activeSliderItemGradient}
            />
          </Animated.View>
          {slides.map((slide: Slide, index: number) => (
            <TouchableOpacity
              style={styles.sliderItem}
              key={index.toString()}
              onPress={() => this.gotoSlide(index)}
            >
              <Image style={styles.sliderItemIcon} source={slide.icon} />
              <Text numberOfLines={1} style={styles.sliderItemName}>
                {translate(slide.key)}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.swiper}>
          <FlatList
            onMomentumScrollEnd={this.handleMomentumScrollEnd}
            decelerationRate="fast"
            onScrollEndDrag={this.handleScrollDragEnd}
            onScroll={this.handleScroll}
            snapToInterval={width}
            ref={this.carouselRef}
            data={slides}
            showsHorizontalScrollIndicator={false}
            horizontal
            scrollEventThrottle={33} // 30 FPS
            initialScrollIndex={INITIAL_SLIDE}
            renderItem={this.renderItem}
            keyExtractor={(item: Slide, index: number) => index.toString()}
            getItemLayout={(data, index) => ({
              length: width,
              offset: width * index,
              index,
            })}
          />
        </View>
        <Guide
          type={GuideType.Match}
          dismissedGuides={dismissedGuides}
          onDismiss={onDismissGuide}
          launchCount={launchCount}
        />
      </ScreenWrapper>
    )
  }
}

const mapStateToProps = (state: ReduxState, props: Props): ReduxProps => {
  const {
    activeTeam,
    availableTeams,
    dismissedGuides,
    launchCount,
  } = state.general
  const { uid } = state.firebase.auth
  const matchId = props.match.id
  const homeId = props.match.teams.home.id
  const awayId = props.match.teams.away.id
  const {
    [fq.matchLineups.key(matchId)]: lineups,
    [fq.matchRoot.key(matchId)]: matchRoot,
    [fq.localizedTeams.key(currentLanguageCode)]: localizedTeams,
    [fq.matchTeamStatistics.key(matchId)]: teamStatistics,
    [fq.matchTimeline.key(matchId)]: timeline,
    [fq.seasonStandings.key(props.match.season.id)]: seasonStandings,
    [fq.teamPlayers.key(homeId)]: homeTeamPlayers,
    [fq.teamPlayers.key(awayId)]: awayTeamPlayers,
    [fq.userVoteHistory.key(uid)]: voteHistory,
    [fq.matchEventVoteResults.key({
      matchId,
      teamId: homeId,
    })]: homeMatchEventVoteResults,
    [fq.matchEventVoteResults.key({
      matchId,
      teamId: awayId,
    })]: awayMatchEventVoteResults,
    [fq.teamPerformanceResult.key({
      matchId,
      teamId: homeId,
    })]: homeTeamPerformanceResult,
    [fq.teamPerformanceResult.key({
      matchId,
      teamId: awayId,
    })]: awayTeamPerformanceResult,
    [fq.managerPerformanceResult.key({
      matchId,
      teamId: homeId,
    })]: homeManagerPerformanceResult,
    [fq.managerPerformanceResult.key({
      matchId,
      teamId: awayId,
    })]: awayManagerPerformanceResult,
    [fq.matchLineupSuggestionResult.key({
      matchId,
      teamId: homeId,
    })]: homeLineupSuggestionResult,
    [fq.matchLineupSuggestionResult.key({
      matchId,
      teamId: awayId,
    })]: awayLineupSuggestionResult,
    [fq.mostValuablePlayersResult.key({
      matchId,
      teamId: homeId,
    })]: homeMostValuablePlayersResult,
    [fq.mostValuablePlayersResult.key({
      matchId,
      teamId: awayId,
    })]: awayMostValuablePlayersResult,
    [fq.matchActivePlayers.key(matchId)]: activePlayers,
    [fq.teamRoot.key(props.match.teams.away.id)]: awayTeamRoot,
    [fq.teamRoot.key(props.match.teams.home.id)]: homeTeamRoot,
    [fq.seasonTeamPopularity.key(props.match.season.id)]: teamPopularity,
  } = state.firestore.data
  return {
    dismissedGuides,
    launchCount,
    isLoading: selectors.isLoading(state),
    isGenerating: selectors.isGenerating(state),
    matchRoot: matchRoot && matchRoot.body,
    localizedTeams: localizedTeams && localizedTeams.body,
    lineups: lineups && lineups.body,
    seasonStandings: seasonStandings && seasonStandings.body,
    teamStatistics: teamStatistics && teamStatistics.body,
    timeline: timeline && timeline.body,
    teamPlayers: {
      home: homeTeamPlayers && homeTeamPlayers.body,
      away: awayTeamPlayers && awayTeamPlayers.body,
    },
    activePlayers: activePlayers && activePlayers.body,
    teamRoot: {
      away: awayTeamRoot && awayTeamRoot.body,
      home: homeTeamRoot && homeTeamRoot.body,
    },
    availableTeams,
    activeTeam,
    voteHistory,
    matchEventVoteResults: {
      home: homeMatchEventVoteResults,
      away: awayMatchEventVoteResults,
    },
    teamPerformanceResult: {
      home: homeTeamPerformanceResult,
      away: awayTeamPerformanceResult,
    },
    managerPerformanceResult: {
      home: homeManagerPerformanceResult,
      away: awayManagerPerformanceResult,
    },
    lineupSuggestionResult: {
      home: homeLineupSuggestionResult,
      away: awayLineupSuggestionResult,
    },
    mostValuablePlayersResult: {
      home: homeMostValuablePlayersResult,
      away: awayMostValuablePlayersResult,
    },
    submittedVotes: state.general.submittedVotes,
    teamPopularity,
  }
}

const mapDispatchToProps = (dispatch: Dispatch): ReduxActionProps =>
  bindActionCreators(
    {
      onVote: votingFlowThunk,
      onDismissGuide: dismissGuide,
    },
    dispatch
  )

export default compose(
  firestoreConnect((props: Props, store: Store<ReduxState>) => {
    const state = store.getState()
    const { uid } = state.firebase.auth
    const matchId = props.match.id
    const homeId = props.match.teams.home.id
    const awayId = props.match.teams.away.id

    const queries = [
      fq.matchLineups.query(matchId),
      fq.matchRoot.query(matchId),
      // TODO: optimize  - only request when team is votable
      fq.teamRoot.query(props.match.teams.home.id),
      fq.teamRoot.query(awayId),
      // TODO: optimize  - only request when it's possible to pick start lineup
      fq.teamPlayers.query(props.match.teams.home.id),
      fq.teamPlayers.query(awayId),
      fq.matchRoot.query(matchId),
      fq.matchTeamStatistics.query(matchId),
      fq.matchTimeline.query(matchId),
      fq.seasonStandings.query(props.match.season.id),
      fq.localizedTeams.query(currentLanguageCode),
      fq.seasonTeamPopularity.query(props.match.season.id),
      // TODO: optimize  - only request when team is votable
      fq.matchEventVoteResults.query({
        matchId,
        teamId: homeId,
      }),
      fq.matchEventVoteResults.query({
        matchId,
        teamId: awayId,
      }),
      fq.teamPerformanceResult.query({
        matchId,
        teamId: homeId,
      }),
      fq.teamPerformanceResult.query({
        matchId,
        teamId: awayId,
      }),
      fq.managerPerformanceResult.query({
        matchId,
        teamId: homeId,
      }),
      fq.managerPerformanceResult.query({
        matchId,
        teamId: awayId,
      }),
      fq.matchLineupSuggestionResult.query({
        matchId,
        teamId: homeId,
      }),
      fq.matchLineupSuggestionResult.query({
        matchId,
        teamId: awayId,
      }),
      fq.mostValuablePlayersResult.query({
        matchId,
        teamId: homeId,
      }),
      fq.mostValuablePlayersResult.query({
        matchId,
        teamId: awayId,
      }),
      // TODO: optimize  - only request when it's possible to vote
      fq.userVoteHistory.query(uid),
      // TODO: optimize  - only request when it's possible vote most valuable player
      fq.matchActivePlayers.query(matchId),
    ]
    return queries
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(MatchScreen)

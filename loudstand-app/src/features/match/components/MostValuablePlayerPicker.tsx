import React from 'react'
import { TeamPlayer, JerseyColors } from '@loudstand/shared'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { translate } from '../../../translation'
import colors from '../../../theme/colors'
import {
  marginDefault,
  spacingUnit,
  isSmallDevice,
} from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import PlayerHeadshot from '../../../components/PlayerHeadshot'
import VoteStatusMessage from './VoteStatusMessage'

interface Props {
  players: TeamPlayer[]
  onSelect?: (player: TeamPlayer) => void
  jerseyColors?: JerseyColors
  hasBeenSubmitted?: boolean
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.tertiaryLight,
    marginBottom: marginDefault,
    marginLeft: marginDefault,
    marginRight: marginDefault,
    borderRadius: spacingUnit / 2,
  },
  contentWrapper: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    margin: marginDefault,
    // justifyContent:'center',
    // alignItems:'center',
  },
  inactiveContentWrapper: {
    opacity: 0,
  },
  headerWrapper: {
    backgroundColor: colors.secondary,
    marginTop: marginDefault,
    marginHorizontal: -marginDefault,
    paddingHorizontal: 2 * marginDefault,
    paddingVertical: spacingUnit,
  },
  headerText: {
    ...(fonts.Body as object),
    color: colors.secondaryText,
  },
  headerMainText: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
  },
  playerContainer: {
    padding: spacingUnit,
    width: isSmallDevice ? '33%' : '25%',
    alignItems: 'center',
    // justifyContent: 'flex-start',
    // justifyContent:'center',
  },
  playerImage: {
    marginBottom: spacingUnit,
  },
  playerNameText: {
    ...(fonts.Body as object),
    textAlign: 'center',
  },
  submitReceipt: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default class MostValuablePlayerPicker extends React.PureComponent<
  Props
> {
  handleSelect = (player: TeamPlayer) => {
    const { onSelect } = this.props
    onSelect && onSelect(player)
  }

  render = () => {
    const { players, jerseyColors, onSelect, hasBeenSubmitted } = this.props
    return (
      <View style={styles.container}>
        <View style={styles.headerWrapper}>
          <Text style={styles.headerText}>
            {`${translate('loudstand.mostValuablePlayerPrefix')} `}
            <Text style={styles.headerMainText}>
              {translate('loudstand.mostValuablePlayer')}
            </Text>
          </Text>
        </View>
        <View>
          <View
            style={[
              styles.contentWrapper,
              hasBeenSubmitted && styles.inactiveContentWrapper,
            ]}
          >
            {players.map(player => (
              <PlayerItem
                disabled={hasBeenSubmitted || !onSelect}
                player={player}
                onSelect={this.handleSelect}
                key={player.id}
                jerseyColors={jerseyColors}
              />
            ))}
          </View>
          {hasBeenSubmitted && (
            <View style={styles.submitReceipt}>
              <VoteStatusMessage
                message={translate('loudstand.voteSubmitted')}
              />
            </View>
          )}
        </View>
      </View>
    )
  }
}

interface PlayerItemProps {
  player: TeamPlayer
  jerseyColors?: JerseyColors
  onSelect: (player: TeamPlayer) => void
  disabled: boolean
}

// eslint-disable-next-line react/no-multi-comp
class PlayerItem extends React.PureComponent<PlayerItemProps> {
  handleSelect = () => {
    const { onSelect, player } = this.props
    onSelect(player)
  }

  render = () => {
    const { player, jerseyColors, disabled } = this.props
    return (
      <TouchableOpacity
        style={styles.playerContainer}
        onPress={this.handleSelect}
        disabled={disabled}
      >
        <PlayerHeadshot
          player={player}
          size="large"
          jerseyColors={jerseyColors}
          style={styles.playerImage}
        />
        <Text
          numberOfLines={2}
          ellipsizeMode="tail"
          style={styles.playerNameText}
        >
          {player.name}
        </Text>
      </TouchableOpacity>
    )
  }
}

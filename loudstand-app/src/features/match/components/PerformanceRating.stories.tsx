import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { Text } from 'react-native'
import PerformanceRating from './PerformanceRating'
import { TEST_MATCH_ROOT } from '../screens/MatchScreen.stories'

storiesOf('PerformanceRating', module)
  .add('manager before vote', () => (
    <PerformanceRating
      allowsVoting
      onRateManager={action('onRateManager')}
      manager={{
        id: 'asd:12312',
        name: 'Crazy Coach',
      }}
    />
  ))
  .add('manager after vote', () => (
    <PerformanceRating
      manager={{
        id: 'asd:12312',
        name: 'Crazy Coach',
      }}
      result={{ rating: 1.3, voteCount: 12 }}
    />
  ))
  .add('team before vote', () => (
    <PerformanceRating
      allowsVoting
      onRateTeam={action('onRateManager')}
      team={{
        id: 'asd:12312',
        name: 'BLABLA TEAM',
        country: 'IT',
      }}
    />
  ))
  .add('team after vote', () => (
    <PerformanceRating
      team={{
        id: 'asd:12312',
        name: 'BLABLA TEAM',
        country: 'IT',
      }}
      result={{ rating: 4.3, voteCount: 12 }}
    />
  ))
  .add('4.3', () => (
    <PerformanceRating
      team={{
        id: 'asd:12312',
        name: 'BLABLA TEAM',
        country: 'IT',
      }}
      result={{ rating: 4.3, voteCount: 12 }}
    />
  ))
  .add('0', () => (
    <PerformanceRating
      team={{
        id: 'asd:12312',
        name: 'BLABLA TEAM',
        country: 'IT',
      }}
      result={{ rating: 0, voteCount: 12 }}
    />
  ))
  .add('4.2', () => (
    <PerformanceRating
      team={{
        id: 'asd:12312',
        name: 'BLABLA TEAM',
        country: 'IT',
      }}
      result={{ rating: 4.2, voteCount: 12 }}
    />
  ))
  .add('5', () => (
    <PerformanceRating
      team={{
        id: 'asd:12312',
        name: 'BLABLA TEAM',
        country: 'IT',
      }}
      result={{ rating: 5, voteCount: 12 }}
    />
  ))
  .add('2', () => (
    <PerformanceRating
      team={{
        id: 'asd:12312',
        name: 'BLABLA TEAM',
        country: 'IT',
      }}
      result={{ rating: 2, voteCount: 12 }}
    />
  ))

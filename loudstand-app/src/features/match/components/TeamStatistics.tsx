import React from 'react'
import { MatchTeamStatistics, MatchLineup } from '@loudstand/shared'
import { FlatList, ListRenderItem, View, Text, StyleSheet } from 'react-native'
import fonts from '../../../theme/fonts'
import { translate } from '../../../translation'
import colors from '../../../theme/colors'
import { marginDefault } from '../../../theme/layout'

const INDICATOR_HEIGHT = 20
const TITLE_WIDTH = 120

/* eslint-disable @typescript-eslint/camelcase */
const STATISTICS_WEIGHT: { [key: string]: number } = {
  ball_possession: 0,
  shots_total: 1,
  shots_on_target: 2,
  corner_kicks: 3,
  offsides: 4,
  fouls: 5,
  yellow_cards: 6,
  red_cards: 7,
  shots_blocked: 8,
  shots_saved: 9,
  shots_off_target: 10,
  injuries: 11,
  throw_ins: 12,
  free_kicks: 13,
  goal_kicks: 14,
  cards_given: 15,
  yellow_red_cards: 16,
}

function compareByWeight(a: string, b: string) {
  if (!(a in STATISTICS_WEIGHT)) {
    return 1
  }
  if (!(b in STATISTICS_WEIGHT)) {
    return -1
  }
  if (STATISTICS_WEIGHT[a] > STATISTICS_WEIGHT[b]) {
    return 1
  }
  if (STATISTICS_WEIGHT[a] < STATISTICS_WEIGHT[b]) {
    return -1
  }
  return 0
}

const styles = StyleSheet.create({
  statsRow: {
    flex: 1,
    flexDirection: 'row',
    minHeight: INDICATOR_HEIGHT * 2.5,
    alignItems: 'center',
  },
  statTitle: { width: TITLE_WIDTH },
  statTitleText: {
    ...(fonts.HeaderSmallLight as object),
    textAlign: 'center',
  },
  valueText: {
    ...(fonts.HeaderSmall as object),
    width: 25,
    flex: 0,
  },
  homeText: {
    textAlign: 'left',
  },
  awayText: {
    textAlign: 'right',
  },
  valueContainer: {
    flex: 1,
    backgroundColor: colors.tertiaryLight,
    paddingStart: marginDefault,
    height: INDICATOR_HEIGHT,
  },
  homeContainer: {
    flexDirection: 'row',
  },
  awayContainer: {
    flexDirection: 'row-reverse',
  },
  indicator: {
    height: INDICATOR_HEIGHT,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.4,
    shadowRadius: 1,
    elevation: 2,
  },
  homeIndicator: {
    backgroundColor: colors.primary,
    borderTopStartRadius: INDICATOR_HEIGHT / 2,
    borderBottomStartRadius: INDICATOR_HEIGHT / 2,
  },
  awayIndicator: {
    backgroundColor: colors.secondary,
    borderTopEndRadius: INDICATOR_HEIGHT / 2,
    borderBottomEndRadius: INDICATOR_HEIGHT / 2,
  },
  indicatorContainer: {
    justifyContent: 'flex-end',
    flexDirection: 'row',
    flex: 1,
  },
  homeIndicatorContainer: {
    flexDirection: 'row',
  },
  awayIndicatorContainer: {
    flexDirection: 'row-reverse',
  },
})

interface Props {
  teamStatistics: MatchTeamStatistics
  lineups?: MatchLineup
}

interface StatListValue {
  key: string
  away?: number
  home?: number
}

export default class TeamStatistics extends React.PureComponent<Props> {
  renderItem: ListRenderItem<StatListValue> = ({ item }) => {
    const home = item.home || 0
    const away = item.away || 0
    const homePercentage = `${(home / (home + away)) * 100}%`
    const awayPecentage = `${(away / (home + away)) * 100}%`
    const { lineups } = this.props
    const homeColor =
      (lineups &&
        lineups.home.jerseyColors &&
        lineups.home.jerseyColors.base) ||
      colors.primary
    const awayColor =
      (lineups &&
        lineups.away.jerseyColors &&
        lineups.away.jerseyColors.base) ||
      colors.secondary
    return (
      <View style={styles.statsRow}>
        <View style={[styles.valueContainer, styles.homeContainer]}>
          <Text style={[styles.valueText, styles.homeText]}>{home}</Text>
          <View
            style={[styles.indicatorContainer, styles.homeIndicatorContainer]}
          >
            <View
              style={[
                styles.indicator,
                styles.homeIndicator,
                { width: homePercentage, backgroundColor: homeColor },
              ]}
            />
          </View>
        </View>
        <View style={styles.statTitle}>
          <Text style={styles.statTitleText}>
            {translate(`matchTeamStats.${item.key}`)}
          </Text>
        </View>
        <View style={[styles.valueContainer, styles.awayContainer]}>
          <Text style={[styles.valueText, styles.awayText]}>{away}</Text>
          <View
            style={[styles.indicatorContainer, styles.awayIndicatorContainer]}
          >
            <View
              style={[
                styles.indicator,
                styles.awayIndicator,
                { width: awayPecentage, backgroundColor: awayColor },
              ]}
            />
          </View>
        </View>
      </View>
    )
  }

  render = () => {
    const { teamStatistics } = this.props

    const listValues: StatListValue[] = Object.keys(teamStatistics)
      .sort(compareByWeight)
      .map((key: string) => ({
        key,
        away: teamStatistics[key].away,
        home: teamStatistics[key].home,
      }))

    return (
      <FlatList
        keyExtractor={item => item.key}
        data={listValues}
        renderItem={this.renderItem}
      />
    )
  }
}

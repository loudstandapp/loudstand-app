import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { PlayerPosition } from '@loudstand/shared'
import LineupPickerRow from './LineupPickerRow'

storiesOf('LineupPickerRow', module)
  .add('defender empty', () => (
    <LineupPickerRow
      type={PlayerPosition.Defender}
      onRemove={action('onRemove')}
      onAdd={action('onAdd')}
      onPick={action('onPick')}
      players={[null, null, null]}
    />
  ))
  .add('defender too few', () => (
    <LineupPickerRow
      type={PlayerPosition.Defender}
      onRemove={action('onRemove')}
      onAdd={action('onAdd')}
      onPick={action('onPick')}
      players={[
        null,
        null,
        {
          number: 33,
          country: null,
          id: 'sr:player:5884',
          name: 'Pepe',
          position: PlayerPosition.Defender,
        },
      ]}
    />
  ))
  .add('defender valid', () => (
    <LineupPickerRow
      type={PlayerPosition.Defender}
      onRemove={action('onRemove')}
      onAdd={action('onAdd')}
      onPick={action('onPick')}
      players={[
        {
          number: 3,
          country: null,
          id: 'sr:player:918726',
          name: 'Eder Militao',
          position: PlayerPosition.Defender,
        },
        {
          number: 33,
          country: null,
          id: 'sr:player:355352',
          name: 'Pepe',
          position: PlayerPosition.Defender,
        },
        {
          number: 33,
          country: null,
          id: 'sr:player:355352',
          name: 'Pepe',
          position: PlayerPosition.Defender,
        },
      ]}
    />
  ))
  .add('defender full', () => (
    <LineupPickerRow
      type={PlayerPosition.Defender}
      onRemove={action('onRemove')}
      onAdd={action('onAdd')}
      onPick={action('onPick')}
      players={[
        {
          number: 3,
          country: null,
          id: 'sr:player:355352',
          name: 'Eder Militao',
          position: PlayerPosition.Defender,
        },

        {
          number: 33,
          country: null,
          id: 'sr:player:5884',
          name: 'Pepe',
          position: PlayerPosition.Defender,
        },
        {
          number: 28,
          country: null,
          id: 'sr:player:242211',
          name: 'Felipe',
          position: PlayerPosition.Defender,
        },
        {
          number: 13,
          country: null,
          id: 'sr:player:312110',
          name: 'Alex Telles',
          position: PlayerPosition.Defender,
        },
      ]}
    />
  ))
  .add('goalkeeper empty', () => (
    <LineupPickerRow
      type={PlayerPosition.Goalkeeper}
      onRemove={action('onRemove')}
      onAdd={action('onAdd')}
      onPick={action('onPick')}
      players={[null]}
    />
  ))
  .add('goalkeeper full', () => (
    <LineupPickerRow
      type={PlayerPosition.Goalkeeper}
      onRemove={action('onRemove')}
      onAdd={action('onAdd')}
      onPick={action('onPick')}
      players={[
        {
          number: 13,
          country: null,
          id: 'sr:player:355352',
          name: 'Alex Telles',
          position: PlayerPosition.Defender,
        },
      ]}
    />
  ))

import React from 'react'
import {
  MatchLineup,
  LocalizedTeams,
  MatchRoot,
  ID,
  TeamPlayer,
  LineupPosition,
  LineupPlayer,
  PlayerPosition,
} from '@loudstand/shared'
import {
  ScrollView,
  StyleSheet,
  ImageBackground,
  View,
  Text,
  SectionList,
  SectionListRenderItemInfo,
} from 'react-native'
import colors from '../../../theme/colors'
import { spacingUnit, marginDefault } from '../../../theme/layout'
import LineupRow from './LineupRow'
import { localizeTeam, getComparatorForKey } from '../../../utils'
import { ListSeparator } from '../../../components/ListSeparator'
import TabSelector from '../../../components/TabSelector'
import PlayerListItem from '../../../components/PlayerListItem'
import { translate } from '../../../translation'
import fonts from '../../../theme/fonts'

interface Props {
  lineups: MatchLineup
  localizedTeams?: LocalizedTeams
  matchRoot: MatchRoot
  onSelectPlayer: (id: ID) => void
  onSelectManager: (id: ID) => void
}

interface State {
  activeTeam: 'home' | 'away'
}

const styles = StyleSheet.create({
  fieldContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    margin: marginDefault,
  },
  fieldContainerImage: {
    tintColor: colors.tertiaryLight,
    borderWidth: 3,
    borderColor: colors.tertiaryLight,
  },
  teamsContainer: {
    flex: 1,
    justifyContent: 'space-between',
    marginHorizontal: marginDefault,
    marginTop: spacingUnit,
  },
  tabBar: {
    flexDirection: 'row',
    flex: 0,
  },
  sliderDots: { alignItems: 'center' },
  listSectionHeader: {
    ...(fonts.HeaderSmall as object),
    margin: marginDefault,
    textAlign: 'center',
  },
})

const positionMap = {
  goalKeeper: [LineupPosition.GoalKeeper],
  defender: [
    LineupPosition.RightBack,
    LineupPosition.LeftBack,
    LineupPosition.CentralDefender,
  ],
  midfielder: [
    LineupPosition.RightWinger,
    LineupPosition.LeftWinger,
    LineupPosition.CentralMidfielder,
  ],
  forward: [LineupPosition.Striker],
}

function getPlayerRow(
  players: LineupPlayer[],
  positions: LineupPosition[],
  fallbackType: PlayerPosition,
  reverse: boolean = false
) {
  return players
    .filter(player =>
      player.playingPosition
        ? positions.includes(player.playingPosition)
        : player.position === fallbackType
    )
    .sort(getComparatorForKey('order', reverse))
}

export default class Lineups extends React.PureComponent<Props, State> {
  state = {
    activeTeam: 'home' as 'home' | 'away',
  }

  handleTabSelection = (index: number) => {
    const qualifiers = ['home', 'away']
    this.setState({ activeTeam: qualifiers[index] as 'home' | 'away' })
  }

  render = () => {
    const {
      lineups,
      matchRoot,
      localizedTeams,
      onSelectPlayer,
      onSelectManager,
    } = this.props
    const { activeTeam } = this.state
    const { home, away } = lineups

    const playerSections = [
      {
        title: translate('lineup.starting'),
        data: lineups[activeTeam].starting
          .slice()
          .sort(getComparatorForKey('order')),
      },
      {
        title: translate('lineup.substitutes'),
        data: lineups[activeTeam].substitutes
          .slice()
          .sort(getComparatorForKey('number')),
      },
    ]
    const managerSection = lineups[activeTeam].manager && {
      title: translate('lineup.manager'),
      data: [lineups[activeTeam].manager],
    }
    const squadSections = managerSection
      ? [...playerSections, managerSection]
      : playerSections
    return (
      <ScrollView>
        <ImageBackground
          imageStyle={styles.fieldContainerImage}
          style={styles.fieldContainer}
          source={require('../../../../assets/images/borderless-pitch.png')}
        >
          <LineupRow
            players={getPlayerRow(
              home.starting,
              positionMap.goalKeeper,
              PlayerPosition.Goalkeeper
            )}
            teamQualifier="home"
            jerseyColors={home.jerseyColors}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupRow
            players={getPlayerRow(
              home.starting,
              positionMap.defender,
              PlayerPosition.Defender
            )}
            teamQualifier="home"
            jerseyColors={home.jerseyColors}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupRow
            players={getPlayerRow(
              home.starting,
              positionMap.midfielder,
              PlayerPosition.Midfielder
            )}
            teamQualifier="home"
            jerseyColors={home.jerseyColors}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupRow
            players={getPlayerRow(
              home.starting,
              positionMap.forward,
              PlayerPosition.Forward
            )}
            teamQualifier="home"
            jerseyColors={home.jerseyColors}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupRow
            players={getPlayerRow(
              away.starting,
              positionMap.forward,
              PlayerPosition.Forward,
              true
            )}
            teamQualifier="away"
            jerseyColors={away.jerseyColors}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupRow
            players={getPlayerRow(
              away.starting,
              positionMap.midfielder,
              PlayerPosition.Midfielder,
              true
            )}
            teamQualifier="away"
            jerseyColors={away.jerseyColors}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupRow
            players={getPlayerRow(
              away.starting,
              positionMap.defender,
              PlayerPosition.Midfielder,
              true
            )}
            teamQualifier="away"
            jerseyColors={away.jerseyColors}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupRow
            players={getPlayerRow(
              away.starting,
              positionMap.goalKeeper,
              PlayerPosition.Goalkeeper,
              true
            )}
            teamQualifier="away"
            jerseyColors={away.jerseyColors}
            onSelectPlayer={onSelectPlayer}
          />
        </ImageBackground>

        <View style={styles.teamsContainer}>
          {matchRoot.teams && (
            <TabSelector
              options={[
                localizeTeam(matchRoot.teams.home, localizedTeams),
                localizeTeam(matchRoot.teams.away, localizedTeams),
              ]}
              onSelect={this.handleTabSelection}
              selectedIndex={0}
            />
          )}
          <SectionList
            stickySectionHeadersEnabled={false}
            sections={squadSections}
            renderItem={({ item }: SectionListRenderItemInfo<TeamPlayer>) => (
              <PlayerListItem
                key={item.id}
                player={item}
                onSelect={
                  lineups[activeTeam].manager &&
                  // @ts-ignore
                  item.id === lineups[activeTeam].manager.id
                    ? onSelectManager
                    : onSelectPlayer
                }
                jerseyColors={
                  activeTeam === 'home' ? home.jerseyColors : away.jerseyColors
                }
              />
            )}
            renderSectionHeader={({ section }) => (
              <Text style={styles.listSectionHeader}>{section.title}</Text>
            )}
            ItemSeparatorComponent={ListSeparator}
            keyExtractor={item => item.id}
          />
        </View>
      </ScrollView>
    )
  }
}

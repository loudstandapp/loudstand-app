import React from 'react'
import {
  TeamPlayer,
  FormationLineup,
  MatchRoot,
  PlayerList,
  MatchTimeline,
  MatchState,
  MatchEvent,
  QuestionResponseType,
  UserVoteHistory,
  MatchEventResultsSummaryDocument,
  generateVoteKey,
  PlayerBasic,
  LocalizedTeams,
  TeamPerformancelResultDocument,
  ManagerPerformanceResultDocument,
  LineupSuggestionResultDocument,
  MatchLineup,
  MatchActivePlayers,
  MostValuablePlayerResultDocument,
  ID,
  SubmittedVote,
  TeamRoot,
  VoteType,
} from '@loudstand/shared'
import {
  SectionList,
  SectionListRenderItem,
  SectionListData,
  Text,
  View,
  StyleSheet,
} from 'react-native'
import moment from 'moment'
import Sentry from 'react-native-sentry'
import LineupPicker from './LineupPicker'
import { getComparatorForKey, cheats, localizeTeam } from '../../../utils'
import LiveEventItem from './LiveEventItem'
import LoudstandSectionHeader from './LoudstandSectionHeader'
import { translate } from '../../../translation'
import {
  ChosenTeamListItem,
  SubmittedVotes,
  ChosenTeamList,
} from '../../../store/model'
import PerformanceRating from './PerformanceRating'
import MostValuablePlayerPicker from './MostValuablePlayerPicker'
import Top3Players from '../../../components/Top3Players'
import {
  lineupVote,
  matchEventVote,
  managerPerformanceVote,
  teamPerformanceVote,
  mostValuablePlayerVote,
} from '../voting'
import { showErrorOverlay } from '../../../components/ErrorOverlay'
import TabSelector from '../../../components/TabSelector'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import SimpleLineupResult from '../../../components/SimpleLineupResult'

interface Props {
  matchRoot: MatchRoot
  activeTeam: ChosenTeamListItem
  availableTeams: ChosenTeamList
  lineups?: MatchLineup
  timeline?: MatchTimeline
  voteHistory?: UserVoteHistory
  localizedTeams?: LocalizedTeams
  teamRoot: {
    home: TeamRoot
    away: TeamRoot
  }
  teamPlayers: { home: PlayerList | undefined; away: PlayerList | undefined }
  matchEventVoteResults: {
    home: MatchEventResultsSummaryDocument | undefined
    away: MatchEventResultsSummaryDocument | undefined
  }
  teamPerformanceResult: {
    home: TeamPerformancelResultDocument | undefined
    away: TeamPerformancelResultDocument | undefined
  }
  managerPerformanceResult: {
    home: ManagerPerformanceResultDocument | undefined
    away: ManagerPerformanceResultDocument | undefined
  }
  lineupSuggestionResult: {
    home: LineupSuggestionResultDocument | undefined
    away: LineupSuggestionResultDocument | undefined
  }
  mostValuablePlayersResult: {
    home: MostValuablePlayerResultDocument | undefined
    away: MostValuablePlayerResultDocument | undefined
  }
  activePlayers?: MatchActivePlayers
  submittedVotes?: SubmittedVotes
  onVote: (vote: SubmittedVote) => void
  onSelectPlayer: (id: ID) => void
  onSelectTeam: (id: ID) => void
  onSelectManager: (id: ID) => void
}

interface Section {
  title?: string
  data: SectionListItem[]
}

interface SelectableTeam {
  team: TeamRoot
  qualifier: 'home' | 'away'
}

interface State {
  visibleTeamQualifier: 'home' | 'away'
  selectableTeams: SelectableTeam[]
  sections: Section[]
  votesAllowed: VotesAllowed
}

enum ItemType {
  LineupInput,
  LineupResult,
  MatchEvent,
  TeamSelector,
  ManagerPerformanceInput,
  TeamPerformanceInput,
  MostValuablePlayerInput,
  ManagerPerformanceResult,
  TeamPerformanceResult,
  MostValuablePlayerResult,
}

type SectionListItem =
  | { type: ItemType.TeamSelector }
  | { type: ItemType.LineupInput }
  | { type: ItemType.LineupResult }
  | { type: ItemType.ManagerPerformanceInput }
  | { type: ItemType.ManagerPerformanceResult }
  | { type: ItemType.TeamPerformanceInput }
  | { type: ItemType.TeamPerformanceResult }
  | { type: ItemType.MostValuablePlayerInput }
  | { type: ItemType.MostValuablePlayerResult }
  | { type: ItemType.MatchEvent; event: MatchEvent }

interface VotesAllowed {
  managerPerformance: boolean
  teamPerformance: boolean
  mostValuablePlayer: boolean
  lineup: boolean
  matchEvents: boolean
}

const styles = StyleSheet.create({
  selectorContainer: {
    marginHorizontal: marginDefault,
    marginTop: spacingUnit,
  },
})

function getVotesAllowed(
  matchRoot: MatchRoot,
  availableTeams: ChosenTeamList,
  visibleTeamQualifier: 'home' | 'away',
  votehistory?: UserVoteHistory
): VotesAllowed {
  const denyAll = {
    managerPerformance: false,
    teamPerformance: false,
    mostValuablePlayer: false,
    lineup: false,
    matchEvents: false,
  }
  const allowAll = {
    managerPerformance: true,
    teamPerformance: true,
    mostValuablePlayer: true,
    lineup: true,
    matchEvents: true,
  }
  if (
    !matchRoot.reactionEndTime ||
    !matchRoot.reactionStartTime ||
    !matchRoot.teams
  ) {
    console.log('insufficient matchroot data')
    return denyAll
  }
  const visibleTeam = matchRoot.teams && matchRoot.teams[visibleTeamQualifier]
  if (!visibleTeam) {
    console.log('cannot determine visible team')
    return denyAll
  }
  if (
    !availableTeams[visibleTeam.id] ||
    !availableTeams[visibleTeam.id].isFavorite
  ) {
    console.log({ availableTeams, visibleTeam }, 'not a favorite')
    return denyAll
  }

  const startTimeObj = new Date(matchRoot.reactionStartTime)
  const endTimeObj = new Date(matchRoot.reactionEndTime)
  const now = new Date()
  if (!cheats.alwaysAllowVoting && (now < startTimeObj || now > endTimeObj)) {
    console.log('outside date range')

    return denyAll
  }

  const allowByMatchState = cheats.alwaysAllowVoting
    ? allowAll
    : {
        managerPerformance: matchRoot.state === MatchState.PostMatch,
        teamPerformance: matchRoot.state === MatchState.PostMatch,
        mostValuablePlayer: matchRoot.state === MatchState.PostMatch,
        lineup: matchRoot.state === MatchState.PreMatch,
        matchEvents: matchRoot.state !== MatchState.PreMatch,
      }

  if (!votehistory) {
    console.log('no vote history')
    return allowByMatchState
  }
  const managerPerformanceVoteKey = generateVoteKey(
    VoteType.ManagerPerformance,
    [{ match: matchRoot.id }, { team: visibleTeam.id }]
  )
  const teamPerformanceVoteKey = generateVoteKey(VoteType.TeamPerformance, [
    { match: matchRoot.id },
    { team: visibleTeam.id },
  ])
  const mostValuablePlayerVoteKey = generateVoteKey(
    VoteType.MostValuablePlayer,
    [{ match: matchRoot.id }, { team: visibleTeam.id }]
  )
  const lineupVoteKey = generateVoteKey(VoteType.Lineup, [
    { match: matchRoot.id },
    { team: visibleTeam.id },
  ])
  return {
    managerPerformance:
      allowByMatchState.managerPerformance &&
      !votehistory[managerPerformanceVoteKey],
    teamPerformance:
      allowByMatchState.teamPerformance && !votehistory[teamPerformanceVoteKey],
    mostValuablePlayer:
      allowByMatchState.mostValuablePlayer &&
      !votehistory[mostValuablePlayerVoteKey],
    lineup: allowByMatchState.lineup && !votehistory[lineupVoteKey],
    matchEvents: allowByMatchState.matchEvents,
  }
}

function getSections(
  matchRoot: MatchRoot,
  votesAllowed: VotesAllowed,
  visibleTeamQualifier: 'home' | 'away',
  timeline?: MatchTimeline
): Section[] {
  if (!matchRoot.reactionEndTime || !matchRoot.reactionStartTime) {
    return []
  }
  const startTimeObj = new Date(matchRoot.reactionStartTime)
  const now = new Date()

  // Bail out if no votes are allowed yet
  if (!cheats.alwaysAllowVoting && startTimeObj > now) {
    return []
  }

  const teamSelectorSection: Section = {
    data: [{ type: ItemType.TeamSelector }],
  }

  const lineupResults: Section = {
    title: translate('loudstand.startingLineuphHeaderResults'),
    data: [{ type: ItemType.LineupResult }],
  }

  const lineupInput: Section = {
    title: translate('loudstand.startingLineuphHeaderInput'),
    data: [{ type: ItemType.LineupInput }],
  }

  const sections = [teamSelectorSection]

  votesAllowed.lineup
    ? sections.push(lineupInput)
    : sections.push(lineupResults)
  if (matchRoot.state === MatchState.PreMatch) {
    return sections
  }

  if (timeline && matchRoot.teams) {
    const reactableEvents = Object.values(timeline.events)
      .filter(event => event.isReactable && event.team === visibleTeamQualifier)
      .sort(getComparatorForKey('seconds'))

    const displayEvents: SectionListItem[] = reactableEvents.map(event => ({
      type: ItemType.MatchEvent,
      event,
      key: event.id,
      // response get user response and event responses
    }))

    const liveEvents: Section = {
      title: translate('loudstand.liveEventsHeader'),
      data: displayEvents,
    }

    if (displayEvents.length) {
      sections.push(liveEvents)
    }
  }
  if (matchRoot.state === MatchState.Live) {
    return sections
  }

  const evaluation: Section = {
    title: translate('loudstand.postMatchHeader'),
    data: [
      {
        type: votesAllowed.mostValuablePlayer
          ? ItemType.MostValuablePlayerInput
          : ItemType.MostValuablePlayerResult,
      },
      {
        type: votesAllowed.managerPerformance
          ? ItemType.ManagerPerformanceInput
          : ItemType.ManagerPerformanceResult,
      },
      {
        type: votesAllowed.teamPerformance
          ? ItemType.TeamPerformanceInput
          : ItemType.TeamPerformanceResult,
      },
    ],
  }

  sections.push(evaluation)
  return sections
}

function getInitialVisibleQualifier(
  matchRoot: MatchRoot,
  activeTeam: ChosenTeamListItem
) {
  if (matchRoot.teams && matchRoot.teams.away.id === activeTeam.id) {
    return 'away'
  }
  return 'home'
}

function getSelectableTeams(home: TeamRoot, away: TeamRoot): SelectableTeam[] {
  const teams: SelectableTeam[] = []
  if (home.defaultTournamentId) {
    teams.push({ qualifier: 'home', team: home })
  }
  if (away.defaultTournamentId) {
    teams.push({ qualifier: 'away', team: away })
  }
  return teams
}

export default class Loudstand extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)
    const {
      matchRoot,
      availableTeams,
      timeline,
      voteHistory,
      activeTeam,
      teamRoot: { home, away },
    } = this.props
    const visibleTeamQualifier = getInitialVisibleQualifier(
      matchRoot,
      activeTeam
    )
    const selectableTeams = getSelectableTeams(home, away)
    const votesAllowed = getVotesAllowed(
      matchRoot,
      availableTeams,
      visibleTeamQualifier,
      voteHistory
    )
    const sections = getSections(
      matchRoot,
      votesAllowed,
      visibleTeamQualifier,
      timeline
    )
    this.state = {
      sections,
      votesAllowed,
      visibleTeamQualifier,
      selectableTeams,
    }
  }

  componentDidUpdate(prevProps: Props, prevState: State) {
    const { matchRoot, timeline, voteHistory } = this.props
    const { visibleTeamQualifier, selectableTeams } = this.state
    if (
      visibleTeamQualifier !== prevState.visibleTeamQualifier ||
      selectableTeams !== prevState.selectableTeams
    ) {
      this.updateSections()
      return
    }
    if (
      prevProps.matchRoot !== matchRoot ||
      prevProps.timeline !== timeline ||
      prevProps.voteHistory !== voteHistory
    ) {
      this.updateAll()
    }
  }

  updateSections = () => {
    const { matchRoot, timeline, voteHistory, availableTeams } = this.props
    const { visibleTeamQualifier } = this.state
    const votesAllowed = getVotesAllowed(
      matchRoot,
      availableTeams,
      visibleTeamQualifier,
      voteHistory
    )
    const sections = getSections(
      matchRoot,
      votesAllowed,
      visibleTeamQualifier,
      timeline
    )
    this.setState({ sections, votesAllowed })
  }

  updateAll = () => {
    const {
      matchRoot,
      timeline,
      voteHistory,
      availableTeams,
      teamRoot: { home, away },
    } = this.props
    const selectableTeams = getSelectableTeams(home, away)
    const { visibleTeamQualifier } = this.state
    const votesAllowed = getVotesAllowed(
      matchRoot,
      availableTeams,
      visibleTeamQualifier,
      voteHistory
    )
    const sections = getSections(
      matchRoot,
      votesAllowed,
      visibleTeamQualifier,
      timeline
    )
    this.setState({ sections, votesAllowed, selectableTeams })
  }

  handlePlayersPicked = (players: FormationLineup<TeamPlayer>) => {
    try {
      const { matchRoot, onVote } = this.props
      const { season, id: matchId, startTime } = matchRoot
      const { visibleTeamQualifier } = this.state
      const visibleTeam =
        matchRoot.teams && matchRoot.teams[visibleTeamQualifier]
      if (!season || !startTime || !visibleTeam) {
        throw new Error('Season or startTime or visibleTeam missing')
      }
      const lineup = Object.values(players).reduce(
        (acc: ID[], items: TeamPlayer[]) => {
          const ids = items.map(item => item.id)
          return [...acc, ...ids]
        },
        []
      )
      const vote = lineupVote({
        lineup,
        matchId,
        seasonId: season.id,
        teamId: visibleTeam.id,
        matchDate: startTime,
      })
      onVote(vote)
    } catch (error) {
      showErrorOverlay(error.message)
      Sentry.captureException(error)
    }
  }

  handleEventResponse = (event: MatchEvent, response: QuestionResponseType) => {
    try {
      const { matchRoot, lineups, onVote, teamRoot } = this.props
      const { season, referee, id: matchId } = matchRoot
      const { visibleTeamQualifier } = this.state
      const visibleTeam = teamRoot[visibleTeamQualifier]
      if (!season || !matchRoot.startTime || !visibleTeam) {
        throw new Error('Season or startTime or lineups or visibleTeam missing')
      }
      const lineup = lineups && lineups[visibleTeamQualifier]
      const manager = (lineup && lineup.manager) || visibleTeam.manager
      const vote = matchEventVote({
        response,
        event,
        matchId,
        seasonId: season.id,
        teamId: visibleTeam.id,
        matchDate: matchRoot.startTime,
        refereeId: referee ? referee.id : null,
        managerId: manager ? manager.id : null,
      })
      vote && onVote(vote)
    } catch (error) {
      showErrorOverlay(error.message)
      Sentry.captureException(error)
    }
  }

  handleManagerRating = (rating: number, manager: PlayerBasic) => {
    try {
      const { matchRoot, onVote } = this.props
      const { visibleTeamQualifier } = this.state
      const visibleTeam =
        matchRoot.teams && matchRoot.teams[visibleTeamQualifier]
      const { season, id: matchId } = matchRoot
      if (!season || !matchRoot.startTime || !visibleTeam) {
        throw new Error('Season or startTime or visibleTeam missing')
      }
      const vote = managerPerformanceVote({
        rating,
        matchId,
        seasonId: season.id,
        teamId: visibleTeam.id,
        matchDate: matchRoot.startTime,
        managerId: manager.id,
      })
      onVote(vote)
    } catch (error) {
      showErrorOverlay(error.message)
      Sentry.captureException(error)
    }
  }

  handleTeamRating = (rating: number, team: ChosenTeamListItem) => {
    try {
      const { matchRoot, onVote } = this.props
      const { season, id: matchId } = matchRoot
      if (!season || !matchRoot.startTime) {
        throw new Error('Season or startTime missing')
      }
      const vote = teamPerformanceVote({
        rating,
        matchId,
        seasonId: season.id,
        teamId: team.id,
        matchDate: matchRoot.startTime,
      })
      onVote(vote)
    } catch (error) {
      showErrorOverlay(error.message)
      Sentry.captureException(error)
    }
  }

  handleMostValuablePlayerPicked = (player: TeamPlayer) => {
    try {
      const { matchRoot, onVote } = this.props
      const { visibleTeamQualifier } = this.state
      const visibleTeam =
        matchRoot.teams && matchRoot.teams[visibleTeamQualifier]
      const { season, id: matchId } = matchRoot
      if (!season || !matchRoot.startTime || !visibleTeam) {
        throw new Error('Season  or startTime or visibleTeam missing')
      }
      const vote = mostValuablePlayerVote({
        playerId: player.id,
        matchId,
        seasonId: season.id,
        teamId: visibleTeam.id,
        matchDate: matchRoot.startTime,
      })
      onVote(vote)
    } catch (error) {
      showErrorOverlay(error.message)
      Sentry.captureException(error)
    }
  }

  renderItem: SectionListRenderItem<SectionListItem> = ({ item }) => {
    const {
      teamPlayers,
      matchRoot,
      voteHistory,
      submittedVotes,
      teamRoot,
      onSelectManager,
      onSelectTeam,
      onSelectPlayer,
    } = this.props
    const { visibleTeamQualifier } = this.state
    const visibleTeamId =
      matchRoot.teams && matchRoot.teams[visibleTeamQualifier].id
    if (!visibleTeamId) {
      return null
    }

    const visibleTeam = teamRoot[visibleTeamQualifier]
    const visibleTeamPlayers = teamPlayers[visibleTeamQualifier]

    switch (item.type) {
      case ItemType.TeamSelector: {
        return this.renderTeamSelector()
      }
      case ItemType.LineupInput: {
        if (!visibleTeamPlayers) {
          return null
        }
        const voteKey = generateVoteKey(VoteType.Lineup, [
          { match: matchRoot.id },
          { team: visibleTeam.id },
        ])
        const hasBeenSubmitted = !!submittedVotes && submittedVotes[voteKey]
        return (
          <LineupPicker
            players={visibleTeamPlayers}
            onDone={this.handlePlayersPicked}
            jerseyColors={visibleTeam.jerseyColors || undefined}
            hasBeenSubmitted={hasBeenSubmitted}
          />
        )
      }
      case ItemType.LineupResult: {
        if (!visibleTeamPlayers) {
          return null
        }
        const { lineupSuggestionResult } = this.props
        const visibleLineupSuggestionResult =
          lineupSuggestionResult[visibleTeamQualifier]
        // fixme handle empty content
        if (!visibleLineupSuggestionResult) {
          return null
        }
        const voteKey = generateVoteKey(VoteType.Lineup, [
          { match: matchRoot.id },
          { team: visibleTeam.id },
        ])
        const chosen =
          voteHistory && voteHistory[voteKey] && voteHistory[voteKey].response
        return (
          <SimpleLineupResult
            teamPlayers={visibleTeamPlayers}
            lineupSuggestionResult={visibleLineupSuggestionResult}
            jerseyColors={visibleTeam.jerseyColors || undefined}
            chosen={chosen}
            onSelectPlayer={onSelectPlayer}
            title={translate('loudstand.startingLineupResultTitle')}
          />
        )
      }
      case ItemType.MatchEvent: {
        const { event } = item
        const { matchEventVoteResults } = this.props
        const { votesAllowed } = this.state
        const visiblematchEventVoteResults =
          matchEventVoteResults[visibleTeamQualifier]
        return (
          <LiveEventItem
            event={event}
            voteHistory={voteHistory}
            allowsVoting={votesAllowed.matchEvents}
            onResponse={this.handleEventResponse}
            submittedVotes={submittedVotes}
            result={
              visiblematchEventVoteResults &&
              visiblematchEventVoteResults[event.id] &&
              visiblematchEventVoteResults[event.id].result
            }
          />
        )
      }
      case ItemType.ManagerPerformanceInput: {
        const { lineups } = this.props
        if (!matchRoot.teams) {
          return null
        }
        const matchSpecificManager =
          lineups && lineups[visibleTeamQualifier].manager
        const manager = matchSpecificManager || visibleTeam.manager
        if (!manager) {
          return null
        }

        const { votesAllowed } = this.state
        const voteKey = generateVoteKey(VoteType.ManagerPerformance, [
          { match: matchRoot.id },
          { team: visibleTeam.id },
        ])
        const hasBeenSubmitted = !!submittedVotes && submittedVotes[voteKey]

        return (
          <PerformanceRating
            manager={manager}
            allowsVoting={votesAllowed.managerPerformance}
            onRateManager={this.handleManagerRating}
            hasBeenSubmitted={hasBeenSubmitted}
            onSelectManager={onSelectManager}
          />
        )
      }
      case ItemType.ManagerPerformanceResult: {
        const { lineups, managerPerformanceResult } = this.props
        if (!matchRoot.teams) {
          return null
        }
        const matchSpecificManager =
          lineups && lineups[visibleTeamQualifier].manager
        const manager = matchSpecificManager || visibleTeam.manager
        if (!manager) {
          return null
        }
        const visibleManagerPerformanceResult =
          managerPerformanceResult[visibleTeamQualifier]
        if (!visibleManagerPerformanceResult) {
          return null
        }
        const voteKey = generateVoteKey(VoteType.ManagerPerformance, [
          { match: matchRoot.id },
          { team: visibleTeamId },
        ])
        const chosen =
          voteHistory && voteHistory[voteKey] && voteHistory[voteKey].response
        return (
          <PerformanceRating
            manager={manager}
            result={visibleManagerPerformanceResult}
            chosen={chosen}
            onSelectManager={onSelectManager}
          />
        )
      }
      case ItemType.TeamPerformanceInput: {
        const { localizedTeams } = this.props
        const { votesAllowed } = this.state
        const voteKey = generateVoteKey(VoteType.TeamPerformance, [
          { match: matchRoot.id },
          { team: visibleTeam.id },
        ])
        const hasBeenSubmitted = !!submittedVotes && submittedVotes[voteKey]

        return (
          <PerformanceRating
            team={visibleTeam}
            allowsVoting={votesAllowed.teamPerformance}
            onRateTeam={this.handleTeamRating}
            localizedTeams={localizedTeams}
            hasBeenSubmitted={hasBeenSubmitted}
            onSelectTeam={onSelectTeam}
          />
        )
      }

      case ItemType.TeamPerformanceResult: {
        const { localizedTeams, teamPerformanceResult } = this.props
        const visibleTeamPerformanceResult =
          teamPerformanceResult[visibleTeamQualifier]
        if (!visibleTeamPerformanceResult) {
          return null
        }
        const visibleTeamListItem =
          matchRoot.teams && matchRoot.teams[visibleTeamQualifier]

        if (!visibleTeamListItem) {
          return null
        }
        const voteKey = generateVoteKey(VoteType.TeamPerformance, [
          { match: matchRoot.id },
          { team: visibleTeamId },
        ])
        const chosen =
          voteHistory && voteHistory[voteKey] && voteHistory[voteKey].response

        return (
          <PerformanceRating
            team={visibleTeamListItem}
            localizedTeams={localizedTeams}
            result={visibleTeamPerformanceResult}
            chosen={chosen}
            onSelectTeam={onSelectTeam}
          />
        )
      }

      case ItemType.MostValuablePlayerInput: {
        const { activePlayers } = this.props
        if (!matchRoot.teams || !activePlayers) {
          return null
        }
        const voteKey = generateVoteKey(VoteType.MostValuablePlayer, [
          { match: matchRoot.id },
          { team: visibleTeam.id },
        ])
        const hasBeenSubmitted = !!submittedVotes && submittedVotes[voteKey]

        const players = activePlayers[visibleTeamQualifier]
        return (
          <MostValuablePlayerPicker
            jerseyColors={visibleTeam.jerseyColors || undefined}
            players={players}
            onSelect={this.handleMostValuablePlayerPicked}
            hasBeenSubmitted={hasBeenSubmitted}
          />
        )
      }

      case ItemType.MostValuablePlayerResult: {
        if (!visibleTeamPlayers) {
          return null
        }
        const { mostValuablePlayersResult } = this.props
        const visibleMostValuablePlayersResult =
          mostValuablePlayersResult[visibleTeamQualifier]

        if (!visibleMostValuablePlayersResult) {
          return null
        }

        const voteKey = generateVoteKey(VoteType.MostValuablePlayer, [
          { match: matchRoot.id },
          { team: visibleTeamId },
        ])
        const chosen =
          voteHistory && voteHistory[voteKey] && voteHistory[voteKey].response

        return (
          <Top3Players
            onSelectPlayer={onSelectPlayer}
            results={visibleMostValuablePlayersResult}
            teamPlayers={visibleTeamPlayers}
            jerseyColors={visibleTeam.jerseyColors || undefined}
            chosen={chosen}
          />
        )
      }

      default:
        return null
    }
  }

  renderHeader = (info: { section: SectionListData<SectionListItem> }) => {
    return (
      info.section.title && (
        <LoudstandSectionHeader title={info.section.title} />
      )
    )
  }

  renderTeamSelector = () => {
    const { localizedTeams } = this.props
    const { selectableTeams, visibleTeamQualifier } = this.state
    return (
      <View style={styles.selectorContainer}>
        <TabSelector
          options={selectableTeams.map(({ team }) =>
            localizeTeam(team, localizedTeams)
          )}
          selectedIndex={selectableTeams.findIndex(
            ({ qualifier }) => qualifier === visibleTeamQualifier
          )}
          onSelect={this.handleSelectTeam}
        />
      </View>
    )
  }

  handleSelectTeam = (index: number) => {
    this.setState(prevState => {
      const { selectableTeams } = prevState
      const visibleTeamQualifier = selectableTeams[index].qualifier
      return { visibleTeamQualifier }
    })
  }

  render = () => {
    const { sections, visibleTeamQualifier } = this.state
    const { matchRoot } = this.props
    return sections.length > 0 ? (
      <SectionList
        stickySectionHeadersEnabled={false}
        sections={sections}
        renderItem={this.renderItem}
        renderSectionHeader={this.renderHeader}
        keyExtractor={(item: SectionListItem) => {
          if (item.type === ItemType.MatchEvent) {
            return `${item.event.id}-${visibleTeamQualifier}`
          }
          return `${item.type.toString()}-${visibleTeamQualifier}`
        }}
      />
    ) : (
      <Text>
        {translate('loudstand.nothingYet', {
          time:
            matchRoot && matchRoot.reactionStartTime
              ? moment(matchRoot.reactionStartTime).fromNow()
              : 'N/A',
        })}
      </Text>
    )
  }
}

import React from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  ListRenderItem,
} from 'react-native'
import {
  MatchTimeline,
  MatchEvent,
  MatchEventType,
  MatchPeriodType,
  ID,
} from '@loudstand/shared'
import SummaryRow from './SummaryRow'
import { getComparatorForKey } from '../../../utils'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import { translate } from '../../../translation'
import colors from '../../../theme/colors'
import fonts from '../../../theme/fonts'

const styles = StyleSheet.create({
  listContentContainer: {
    marginTop: marginDefault,
  },
  periodText: {
    ...(fonts.Body as object),
    padding: spacingUnit,
    backgroundColor: colors.fixmeSecondaryBackground,
    color: colors.secondaryText,
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  gameIndicator: {
    alignItems: 'center',
    margin: spacingUnit,
  },
  gameIndicatorText: {
    ...(fonts.HeaderSmall as object),
    marginTop: spacingUnit,
    textTransform: 'uppercase',
  },
})

interface Props {
  timeline: MatchTimeline
  onSelectPlayer: (id: ID) => void
}

const secondsComparator = getComparatorForKey<MatchEvent>('seconds')
const timeComparator = getComparatorForKey<MatchEvent>('time')
const roundComparator = getComparatorForKey<MatchEvent>('periodRound')

function periodTypeComparator(a: MatchEvent, b: MatchEvent) {
  if (a.periodType === b.periodType) {
    return 0
  }
  if (a.periodType === MatchPeriodType.Regular) {
    return -1
  }
  if (b.periodType === MatchPeriodType.Regular) {
    return 1
  }
  // MatchPeriodType.Overtime in middle
  if (a.periodType === MatchPeriodType.Penalties) {
    return 1
  }
  if (b.periodType === MatchPeriodType.Penalties) {
    return -1
  }
  return 0
}

function periodStartComparator(a: MatchEvent, b: MatchEvent) {
  if (a.data.type === b.data.type) {
    return 0
  }
  if (a.data.type === MatchEventType.PeriodStart) {
    return -1
  }
  if (b.data.type === MatchEventType.PeriodStart) {
    return 1
  }
  return 0
}

function stateComparator(a: MatchEvent, b: MatchEvent) {
  if (a.data.type === b.data.type) {
    return 0
  }
  if (a.data.type === MatchEventType.MatchEnded) {
    return 1
  }
  if (b.data.type === MatchEventType.MatchEnded) {
    return -1
  }
  if (a.data.type === MatchEventType.MatchStarted) {
    return -1
  }
  if (b.data.type === MatchEventType.MatchStarted) {
    return 1
  }
  return 0
}

function matchEventComparator(a: MatchEvent, b: MatchEvent) {
  return (
    stateComparator(a, b) ||
    periodTypeComparator(a, b) ||
    roundComparator(a, b) ||
    periodStartComparator(a, b) ||
    secondsComparator(a, b) ||
    timeComparator(a, b)
  )
}

export default class Timeline extends React.PureComponent<Props> {
  renderItem: ListRenderItem<MatchEvent> = ({ item }) => {
    const { onSelectPlayer } = this.props
    switch (item.data.type) {
      case MatchEventType.MatchStarted:
        return this.renderGameStart()
      case MatchEventType.MatchEnded:
        return this.renderGameEnd()
      case MatchEventType.PeriodStart: {
        switch (item.periodType) {
          case MatchPeriodType.Regular:
            switch (item.periodRound) {
              case 2:
                return this.renderPeriod(translate('matchTimeline.secondHalf'))
              default:
                return null
            }
          case MatchPeriodType.Overtime:
            switch (item.periodRound) {
              case 1:
                return this.renderPeriod(translate('matchTimeline.extraTime'))
              default:
                return null
            }
          case MatchPeriodType.Penalties:
            return this.renderPeriod(translate('matchTimeline.penalties'))
          default:
            return null
        }
      }
      default:
        return (
          <SummaryRow
            key={item.id}
            event={item}
            onSelectPlayer={onSelectPlayer}
          />
        )
    }
  }

  renderPeriod = (title: string) => (
    <Text style={styles.periodText}>{title}</Text>
  )

  renderGameStart = () => (
    <View style={styles.gameIndicator}>
      <Image
        source={require('../../../../assets/icons/indicators/whistle.png')}
      />
      <Text style={styles.gameIndicatorText}>
        {translate('matchTimeline.gameStart')}
      </Text>
    </View>
  )

  renderGameEnd = () => (
    <View style={styles.gameIndicator}>
      <Image
        source={require('../../../../assets/icons/indicators/whistle.png')}
      />
      <Text style={styles.gameIndicatorText}>
        {translate('matchTimeline.gameEnd')}
      </Text>
    </View>
  )

  render() {
    const { timeline } = this.props
    const sorted = Object.values(timeline.events).sort(matchEventComparator)
    return (
      <FlatList
        data={sorted}
        renderItem={this.renderItem}
        keyExtractor={event => event.id}
      />
    )
  }
}

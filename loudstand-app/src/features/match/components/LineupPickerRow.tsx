import React from 'react'
import { TeamPlayer, PlayerPosition, JerseyColors } from '@loudstand/shared'
import { StyleSheet, TouchableOpacity, Text, View, Image } from 'react-native'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { constraints } from './LineupPicker'
import PlayerHeadshot from '../../../components/PlayerHeadshot'

const BORDER_MARGIN = 3
const BORDER_WIDTH = 1
const PLUS_BUTTON_SIZE = 25

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  players: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    // flex: 1,
  },
  playerContainer: {
    alignItems: 'center',
    flex: 1,
  },
  borderContainer: {
    width: PlayerHeadshot.mediumSize + 2 * BORDER_MARGIN + 2 * BORDER_WIDTH,
    height: PlayerHeadshot.mediumSize + 2 * BORDER_MARGIN + 2 * BORDER_WIDTH,
    borderRadius: PlayerHeadshot.mediumSize / 2 + BORDER_MARGIN + BORDER_WIDTH,
    backgroundColor: colors.secondary25pct,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: PlayerHeadshot.mediumSize + 2 * BORDER_WIDTH,
    height: PlayerHeadshot.mediumSize + 2 * BORDER_WIDTH,
    borderRadius: PlayerHeadshot.mediumSize / 2 + BORDER_WIDTH,
    borderWidth: BORDER_WIDTH,
    borderColor: colors.tertiary,
  },
  empty: {
    backgroundColor: undefined,
    width: PlayerHeadshot.mediumSize,
    height: PlayerHeadshot.mediumSize,
    borderRadius: PlayerHeadshot.mediumSize / 2,
  },
  positionButton: {
    flex: 1,
    marginVertical: marginDefault,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addButton: {
    width: PlayerHeadshot.mediumSize + 2 * BORDER_MARGIN,
    height: PlayerHeadshot.mediumSize + 2 * BORDER_MARGIN,
    marginTop: 2 * spacingUnit,
    marginRight: spacingUnit,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: -(marginDefault + PLUS_BUTTON_SIZE / 2),
  },
  addButtonImage: {
    tintColor: colors.valid,
  },

  playerNameText: {
    marginTop: spacingUnit,
    ...(fonts.BodySmallBold as object),
    textAlign: 'center',
  },
})

interface Props {
  type: PlayerPosition
  disabled?: boolean
  onRemove: (index: number, type: PlayerPosition) => void
  onPick: (index: number, type: PlayerPosition) => void
  onAdd: (type: PlayerPosition) => void
  players: (TeamPlayer | null)[]
  jerseyColors?: JerseyColors
}

export default class LineupPickerRow extends React.PureComponent<Props> {
  handleAdd = () => {
    const { onAdd, type } = this.props
    onAdd(type)
    ReactNativeHapticFeedback.trigger('selection')
  }

  handleRemove = (index: number) => {
    const { onRemove, type } = this.props
    onRemove(index, type)
    ReactNativeHapticFeedback.trigger('selection')
  }

  handlePick = (index: number) => {
    const { onPick, type } = this.props
    onPick(index, type)
    ReactNativeHapticFeedback.trigger('selection')
  }

  render = () => {
    const { players, type, jerseyColors, disabled } = this.props
    const { min, max } = constraints[type]
    const validPlayers = players.filter(player => !!player)

    return (
      <View style={styles.container}>
        <View style={styles.players}>
          {players.map((player, index) => {
            const action = player ? this.handleRemove : this.handlePick
            return (
              <TouchableOpacity
                key={index.toString()}
                onPress={() => action(index)}
                style={[styles.positionButton]}
                disabled={disabled}
              >
                {player ? (
                  <View style={styles.playerContainer}>
                    <View style={styles.borderContainer}>
                      <PlayerHeadshot
                        style={styles.image}
                        player={player}
                        size="medium"
                        jerseyColors={jerseyColors}
                      />
                    </View>

                    <Text numberOfLines={2} style={styles.playerNameText}>
                      {player.name}
                    </Text>
                  </View>
                ) : (
                  <View style={styles.borderContainer}>
                    <View style={[styles.image, styles.empty]} />
                  </View>
                )}
              </TouchableOpacity>
            )
          })}
        </View>

        {players.length < max && validPlayers.length >= min && (
          <TouchableOpacity
            onPress={this.handleAdd}
            style={styles.addButton}
            disabled={disabled}
          >
            <Image
              source={require('../../../../assets/icons/buttons/plus.png')}
              style={[styles.addButtonImage]}
            />
          </TouchableOpacity>
        )}
      </View>
    )
  }
}

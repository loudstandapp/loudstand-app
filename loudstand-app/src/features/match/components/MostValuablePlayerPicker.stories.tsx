import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { PlayerPosition } from '@loudstand/shared'
import MostValuablePlayerPicker from './MostValuablePlayerPicker'

const ACTIVE_PLAYERS = [
  {
    id: 'sr:player:318941',
    country: 'DE',
    number: 40,
    name: 'Eric Oelschlagel',
    position: PlayerPosition.Goalkeeper,
  },
  {
    id: 'sr:player:47549',
    country: 'DE',
    number: 29,
    name: 'Marcel Schmelzer',
    position: PlayerPosition.Defender,
  },
  {
    id: 'sr:player:178407',
    country: 'CH',
    number: 16,
    name: 'Manuel Akanji',
    position: PlayerPosition.Defender,
  },
  {
    id: 'sr:player:47906',
    country: 'FR',
    number: 2,
    name: 'Dan Axel Zagadou',
    position: PlayerPosition.Defender,
  },
  {
    id: 'sr:player:557008',
    country: 'TR',
    number: 36,
    name: 'Omer Toprak',
    position: PlayerPosition.Defender,
  },
  {
    id: 'sr:player:188133',
    country: 'AR',
    number: 18,
    name: 'Leonardo Balerdi',
    position: PlayerPosition.Defender,
  },
  {
    id: 'sr:player:37378',
    country: 'CH',
    number: 1,
    name: 'Roman Burki',
    position: PlayerPosition.Goalkeeper,
  },
  {
    id: 'sr:player:237068',
    country: 'DE',
    number: 33,
    name: 'Julian Weigl',
    position: PlayerPosition.Midfielder,
  },
  {
    id: 'sr:player:96366',
    country: 'ES',
    number: 9,
    name: 'Paco Alcacer',
    position: PlayerPosition.Forward,
  },
  {
    id: 'sr:player:254119',
    country: 'DE',
    number: 27,
    name: 'Marius Wolf',
    position: PlayerPosition.Midfielder,
  },
  {
    id: 'sr:player:171927',
    country: 'DE',
    number: 20,
    name: 'Maximilian Philipp',
    position: PlayerPosition.Forward,
  },
  {
    id: 'sr:player:66518',
    country: 'DE',
    number: 10,
    name: 'Mario Gotze',
    position: PlayerPosition.Midfielder,
  },
  {
    id: 'sr:player:37813',
    country: 'CH',
    number: 35,
    name: 'Marwin Hitz',
    position: PlayerPosition.Goalkeeper,
  },
  {
    id: 'sr:player:1047147',
    country: null,
    number: 7,
    name: 'Jadon Sancho',
    position: PlayerPosition.Forward,
  },
  {
    id: 'sr:player:37092',
    country: 'DE',
    number: 11,
    name: 'Marco Reus',
    position: PlayerPosition.Forward,
  },
]

storiesOf('MostValuablePlayerPicker', module).add('default', () => (
  <MostValuablePlayerPicker
    players={ACTIVE_PLAYERS}
    onSelect={action('onSelect')}
  />
))

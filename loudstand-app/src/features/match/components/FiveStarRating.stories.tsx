import React from 'react'
import { storiesOf } from '@storybook/react-native'
import FiveStarRating from './FiveStarRating'

storiesOf('FiveStarRating', module)
  .add('0', () => <FiveStarRating value={0} />)
  .add('1', () => <FiveStarRating value={1} />)
  .add('1.5', () => <FiveStarRating value={1.5} />)
  .add('3.2', () => <FiveStarRating value={3.2} />)
  .add('3.4', () => <FiveStarRating value={3.4} />)
  .add('3.6', () => <FiveStarRating value={3.6} />)
  .add('3.8', () => <FiveStarRating value={3.8} />)
  .add('3.9', () => <FiveStarRating value={3.9} />)
  .add('4', () => <FiveStarRating value={4} />)
  .add('5', () => <FiveStarRating value={5} />)

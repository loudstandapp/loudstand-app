import React from 'react'
import { Image, View, StyleSheet } from 'react-native'

interface Props {
  value: number
}

const FullStar = () => (
  <Image
    source={require('../../../../assets/icons/indicators/full-star.png')}
  />
)

const HalfStar = () => (
  <Image
    source={require('../../../../assets/icons/indicators/half-star.png')}
  />
)

const EmptyStar = () => (
  <Image
    source={require('../../../../assets/icons/indicators/empty-star.png')}
  />
)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
})

const STEP = 0.5
function getStars(value: number) {
  const rounded = Math.round(value / STEP) * STEP
  const full = Math.floor(rounded)
  const half = full < rounded ? 1 : 0
  const empty = 5 - full - half
  return { full, half, empty }
}

export default class FiveStarRating extends React.PureComponent<Props> {
  render = () => {
    const { value } = this.props
    const stars = getStars(value)
    return (
      <View style={styles.container}>
        {[...Array(stars.full)].map((e, index) => (
          <FullStar key={index.toString()} />
        ))}
        {[...Array(stars.half)].map((e, index) => (
          <HalfStar key={index.toString()} />
        ))}
        {[...Array(stars.empty)].map((e, index) => (
          <EmptyStar key={index.toString()} />
        ))}
      </View>
    )
  }
}

import React from 'react'
import {
  MatchRoot,
  MatchStatusType,
  ISO8601Timestamp,
  MatchState,
} from '@loudstand/shared'
import { Text, StyleSheet } from 'react-native'
import { currentLocale, translate } from '../../../translation'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { spacingUnit } from '../../../theme/layout'

interface Props {
  matchRoot: MatchRoot
}

interface State {
  now: Date
}

const TIMER_INTERVAL = 10

const styles = StyleSheet.create({
  matchTime: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
    marginTop: spacingUnit / 2,
  },
  matchTimeAfterStart: {
    ...(fonts.BodyBold as object),
    color: colors.secondaryText,
  },
})

const timeOffset: { [key in string]: number | undefined } = {
  [MatchStatusType.FirstHalf]: 0,
  [MatchStatusType.SecondHalf]: 45 * 60,
  [MatchStatusType.FirstExtra]: 90 * 60,
  [MatchStatusType.ExtraTime]: 90 * 60,
  [MatchStatusType.SecondExtra]: 105 * 60,
}

const timeLimit: { [key in string]: number | undefined } = {
  [MatchStatusType.FirstHalf]: 45 * 60,
  [MatchStatusType.SecondHalf]: 90 * 60,
  [MatchStatusType.FirstExtra]: 105 * 60,
  [MatchStatusType.ExtraTime]: 105 * 60,
  [MatchStatusType.SecondExtra]: 120 * 60,
}

export const dateFormatter = new Intl.DateTimeFormat(
  currentLocale.languageTag,
  {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
  }
)
export const timeFormatter = new Intl.DateTimeFormat(
  currentLocale.languageTag,
  {
    hour: 'numeric',
    minute: '2-digit',
  }
)

const formatStartTime = (startTime: ISO8601Timestamp) =>
  `${dateFormatter.format(new Date(startTime))} | ${timeFormatter.format(
    new Date(startTime)
  )}`

function formatMatchTimer(
  status: MatchStatusType,
  time: ISO8601Timestamp,
  now: Date
): string | null {
  const diffSeconds = (now.getTime() - new Date(time).getTime()) / 1000
  const limit = timeLimit[status]
  const offset = timeOffset[status]
  if (limit === undefined || offset === undefined) {
    return null
  }
  const timeSinceStart = diffSeconds + offset
  const mainTime = Math.min(timeSinceStart, limit)
  const plusTime = Math.max(0, timeSinceStart - limit)
  if (!plusTime) {
    return `${Math.ceil(mainTime / 60)}'`
  }

  return `${Math.ceil(mainTime / 60)}' + ${Math.ceil(plusTime / 60)}'`
}

function generateMatchStatus(
  { startTime, state, matchStatus }: MatchRoot,
  now: Date
): string | null {
  if (state === MatchState.PreMatch) {
    return formatStartTime(startTime)
  }
  if (state === MatchState.PostMatch) {
    return translate('matchStatus.fullTime')
  }
  if (matchStatus) {
    switch (matchStatus.current) {
      case MatchStatusType.FirstHalf:
      case MatchStatusType.SecondHalf:
      case MatchStatusType.FirstExtra:
      case MatchStatusType.SecondExtra:
      case MatchStatusType.ExtraTime: {
        if (!matchStatus.time) {
          return startTime && formatStartTime(startTime)
        }
        return formatMatchTimer(matchStatus.current, matchStatus.time, now)
      }
      case MatchStatusType.Pause:
        return translate('matchStatus.paused')
      case MatchStatusType.Penalties:
        return translate('matchStatus.penalties')
      case MatchStatusType.Abandoned:
        return translate('matchStatus.abandoned')
      case MatchStatusType.AwaitingPenalties:
        return translate('matchStatus.awaitingPenalties')
      case MatchStatusType.AwaitingExtraTime:
        return translate('matchStatus.awaitingExtraTime')
      case MatchStatusType.Cancelled:
        return translate('matchStatus.cancelled')
      case MatchStatusType.Delayed:
        return translate('matchStatus.delayed')
      case MatchStatusType.Postponed:
        return translate('matchStatus.postponed')
      case MatchStatusType.Live:
        return translate('matchStatus.live')
      case MatchStatusType.Overtime:
        return translate('matchStatus.overtime')
      case MatchStatusType.Interrupted:
        return translate('matchStatus.interrupted')
      case MatchStatusType.HalfTime:
        return translate('matchStatus.halfTime')
      case MatchStatusType.Ended:
      case MatchStatusType.Closed:
      case MatchStatusType.FullTime:
      case MatchStatusType.EndedAfterExtraTime:
      case MatchStatusType.EndedAfterPenalties:
        return translate('matchStatus.fullTime')
      default:
        return formatStartTime(startTime)
    }
  }
  return formatStartTime(startTime)
}

export default class MatchTime extends React.PureComponent<Props, State> {
  state = {
    now: new Date(),
  }

  timerId: number | null = null

  componentDidMount = () => {
    this.startTimer()
  }

  componentWillUnmount = () => {
    this.stopTimer()
  }

  startTimer = () => {
    // @ts-ignore wrong type of timeout id
    this.timerId = setInterval(
      () => this.setState({ now: new Date() }),
      TIMER_INTERVAL
    )
  }

  stopTimer = () => {
    if (this.timerId === null) {
      return
    }
    clearInterval(this.timerId)
    this.timerId = null
  }

  render = () => {
    const { matchRoot } = this.props
    const { now } = this.state
    const matchStatus = generateMatchStatus(matchRoot, now)
    return (
      <Text
        style={[
          styles.matchTime,
          matchRoot.state !== MatchState.PreMatch && styles.matchTimeAfterStart,
        ]}
        numberOfLines={1}
        ellipsizeMode="head"
      >
        {matchStatus}
      </Text>
    )
  }
}

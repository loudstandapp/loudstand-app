import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import RatingPicker from './RatingPicker'

storiesOf('RatingPicker', module).add('default', () => (
  <RatingPicker onSelect={action('onSelect')} />
))

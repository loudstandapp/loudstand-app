import React from 'react'

import { storiesOf, getStorybookUI } from '@storybook/react-native'

import { MatchStatus, MatchStatusType, MatchState } from '@loudstand/shared'
import { View } from 'react-native'
import MatchTime from './MatchTime'
import { TEST_MATCH_ROOT } from '../screens/MatchScreen.stories'

const nowMinus0minutes = new Date(Date.now() - 10 * 1000).toISOString()
const nowMinus10minutes = new Date(Date.now() - 10 * 60 * 1000).toISOString()
const nowMinus30minutes = new Date(Date.now() - 30 * 60 * 1000).toISOString()
const nowMinus50minutes = new Date(Date.now() - 50 * 60 * 1000).toISOString()

const variants: {
  [key: string]: { matchStatus: MatchStatus | null; state: MatchState }
} = {
  'not started': {
    state: MatchState.PreMatch,
    matchStatus: null,
  },
  '1h 0 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.FirstHalf,
      time: nowMinus0minutes,
    },
  },
  '1h 10 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.FirstHalf,
      time: nowMinus10minutes,
    },
  },
  '1h 30 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.FirstHalf,
      time: nowMinus30minutes,
    },
  },
  '1h 50 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.FirstHalf,
      time: nowMinus50minutes,
    },
  },
  '2h 0 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.SecondHalf,
      time: nowMinus0minutes,
    },
  },
  '2h 10 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.SecondHalf,
      time: nowMinus10minutes,
    },
  },
  '2h 30 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.SecondHalf,
      time: nowMinus30minutes,
    },
  },
  '2h 50 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.SecondHalf,
      time: nowMinus50minutes,
    },
  },
  '1extra 0 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.FirstExtra,
      time: nowMinus0minutes,
    },
  },
  '1extra 10 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.FirstExtra,
      time: nowMinus10minutes,
    },
  },
  '1extra 30 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.FirstExtra,
      time: nowMinus30minutes,
    },
  },

  '2extra 0 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.SecondExtra,
      time: nowMinus0minutes,
    },
  },
  '2extra 10 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.SecondExtra,
      time: nowMinus10minutes,
    },
  },
  '2extra 30 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.SecondExtra,
      time: nowMinus30minutes,
    },
  },
  'extratime 0 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.ExtraTime,
      time: nowMinus0minutes,
    },
  },
  'extratime 10 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.ExtraTime,
      time: nowMinus10minutes,
    },
  },
  'extratime 30 mins': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.ExtraTime,
      time: nowMinus30minutes,
    },
  },
  'awaiting penalties': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.AwaitingPenalties,
      time: nowMinus30minutes,
    },
  },
  'awaiting extra time': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.AwaitingExtraTime,
      time: nowMinus30minutes,
    },
  },
  'half time': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.HalfTime,
      time: nowMinus30minutes,
    },
  },
  'full time': {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.FullTime,
      time: nowMinus30minutes,
    },
  },
  Delayed: {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.Delayed,
      time: nowMinus30minutes,
    },
  },
  Cancelled: {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.Cancelled,
      time: nowMinus30minutes,
    },
  },
  Abandoned: {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.Abandoned,
      time: nowMinus30minutes,
    },
  },
  Pause: {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.Pause,
      time: nowMinus30minutes,
    },
  },
  Live: {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.Live,
      time: nowMinus30minutes,
    },
  },
  Postponed: {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.Postponed,
      time: nowMinus30minutes,
    },
  },
  Interrupted: {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.Interrupted,
      time: nowMinus30minutes,
    },
  },
  EndedAfterPenalties: {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.EndedAfterPenalties,
      time: nowMinus30minutes,
    },
  },
  Penalties: {
    state: MatchState.Live,
    matchStatus: {
      current: MatchStatusType.Penalties,
      time: nowMinus30minutes,
    },
  },
  EndedAfterExtraTime: {
    state: MatchState.PostMatch,
    matchStatus: {
      current: MatchStatusType.EndedAfterExtraTime,
      time: nowMinus30minutes,
    },
  },
  Closed: {
    state: MatchState.PostMatch,
    matchStatus: {
      current: MatchStatusType.Closed,
      time: nowMinus30minutes,
    },
  },
  Ended: {
    state: MatchState.PostMatch,
    matchStatus: {
      current: MatchStatusType.Ended,
      time: nowMinus30minutes,
    },
  },
}

const stories = storiesOf('MatchTime', module).addDecorator(getStory => (
  <View style={{ backgroundColor: 'grey' }}>{getStory()}</View>
))
// eslint-disable-next-line no-restricted-syntax, guard-for-in
for (const name in variants) {
  const { matchStatus, state } = variants[name]
  stories.add(name, () => (
    <MatchTime matchRoot={{ ...TEST_MATCH_ROOT, matchStatus, state }} />
  ))
}

import React from 'react'
import {
  View,
  StyleSheet,
  LayoutChangeEvent,
  GestureResponderEvent,
} from 'react-native'
import FiveStarRating from './FiveStarRating'

interface Props {
  disabled?: boolean
  onSelect: (rating: number) => void
}

interface State {
  currentRating: number
}

const MIN = 0
const MAX = 5
const STEP = 0.5

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  disabled: {
    opacity: 0.4,
  },
  touchableArea: {
    ...StyleSheet.absoluteFillObject,
  },
})
export default class RatingPicker extends React.PureComponent<Props, State> {
  state = { currentRating: 0 }

  touchableWidth = 1

  handleTouchableLayout = (event: LayoutChangeEvent) => {
    const { width } = event.nativeEvent.layout
    this.touchableWidth = width
  }

  handleLocation = (event: GestureResponderEvent) => {
    const { onSelect } = this.props
    const { locationX } = event.nativeEvent
    if (this.touchableWidth === 0) {
      return
    }
    const relative = locationX / this.touchableWidth
    const range = MAX - MIN
    const value = relative * range
    const roundedValue = Math.round(value / STEP) * STEP
    console.log({ relative, value, roundedValue })
    this.setState(prevState => {
      if (prevState.currentRating === roundedValue) {
        return null
      }
      onSelect(roundedValue)
      return { currentRating: roundedValue }
    })
  }

  render = () => {
    const { disabled } = this.props
    const { currentRating } = this.state
    return (
      <View style={[styles.container, disabled && styles.disabled]}>
        <FiveStarRating value={currentRating} />
        {!disabled && (
          <View
            onLayout={this.handleTouchableLayout}
            style={styles.touchableArea}
            onStartShouldSetResponder={() => true}
            onResponderMove={this.handleLocation}
            onResponderRelease={this.handleLocation}
          />
        )}
      </View>
    )
  }
}

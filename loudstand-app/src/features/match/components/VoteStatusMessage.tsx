import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import colors from '../../../theme/colors'
import { headerHeight, spacingUnit } from '../../../theme/layout'
import fonts from '../../../theme/fonts'

interface Props {
  message: string
}
const styles = StyleSheet.create({
  messageContainer: {
    backgroundColor: colors.navigationBackground,
    height: headerHeight,
    borderRadius: headerHeight / 2,
    justifyContent: 'center',
    paddingHorizontal: 3 * spacingUnit,
  },
  messageText: {
    ...(fonts.HeaderSmall as object),
    color: colors.secondaryText,
  },
})
// eslint-disable-next-line react/prefer-stateless-function
export default class VoteStatusMessage extends React.Component<Props> {
  render = () => {
    const { message } = this.props
    return (
      <View style={styles.messageContainer}>
        <Text style={styles.messageText}>{message}</Text>
      </View>
    )
  }
}

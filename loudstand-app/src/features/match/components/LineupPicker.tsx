import React from 'react'
import {
  TeamPlayer,
  FormationLineup,
  PlayerPosition,
  PlayerList,
  JerseyColors,
} from '@loudstand/shared'
import {
  StyleSheet,
  ImageBackground,
  ListRenderItemInfo,
  View,
} from 'react-native'
import colors from '../../../theme/colors'
import LineupPickerRow from './LineupPickerRow'
import { showPickerOverlay } from '../../../components/PickerOverlay'
import PlayerPickerItem from '../../../components/PlayerPickerItem'
import { spacingUnit, marginDefault } from '../../../theme/layout'
import { translate } from '../../../translation'
import SubmitButton from '../../../components/SubmitButton'
import VoteStatusMessage from './VoteStatusMessage'
import InfoButton from '../../../components/InfoButton'
import { InfoType } from '../../../components/Info'

enum Status {
  EmptyPositions,
  TooFew,
  TooMany,
  OK,
}

interface Props {
  players: PlayerList
  jerseyColors?: JerseyColors
  hasBeenSubmitted: boolean
  onDone: (lineup: FormationLineup<TeamPlayer>) => void
}

interface State {
  lineup: FormationLineup<TeamPlayer | null>
  status: Status
}

function getMessage(status: Status, hasBeenSubmitted?: boolean) {
  if (hasBeenSubmitted) {
    return translate('loudstand.voteSubmitted')
  }
  switch (status) {
    case Status.EmptyPositions:
      return translate('loudstand.startingLineupEmptyPositions')
    case Status.TooFew:
      return translate('loudstand.startingLineupTooFew')
    case Status.TooMany:
      return translate('loudstand.startingLineupTooMany')
    default:
      return undefined
  }
}
const styles = StyleSheet.create({
  fieldContainer: {
    alignItems: 'stretch',
    justifyContent: 'space-between',
    marginHorizontal: marginDefault,
    marginVertical: spacingUnit,
    flex: 1,
  },
  fieldContainerImage: {
    tintColor: colors.tertiaryLight,
    borderWidth: 3,
    borderColor: colors.tertiaryLight,
  },
  submitButtonWrapper: {
    marginBottom: marginDefault,
    alignItems: 'center',
  },
})

export const constraints = {
  [PlayerPosition.Defender]: {
    min: 3,
    max: 5,
  },
  [PlayerPosition.Forward]: {
    min: 1,
    max: 3,
  },
  [PlayerPosition.Midfielder]: {
    min: 3,
    max: 5,
  },
  [PlayerPosition.Goalkeeper]: {
    min: 1,
    max: 1,
  },
}

export const REQUIRED_NUMBER_OF_PLAYERS = 11

export default class LineupPicker extends React.PureComponent<Props, State> {
  state = {
    status: Status.EmptyPositions,
    lineup: {
      [PlayerPosition.Goalkeeper]: Array(
        constraints[PlayerPosition.Goalkeeper].min
      ).fill(null),
      [PlayerPosition.Forward]: Array(
        constraints[PlayerPosition.Forward].min
      ).fill(null),
      [PlayerPosition.Midfielder]: Array(
        constraints[PlayerPosition.Midfielder].min
      ).fill(null),
      [PlayerPosition.Defender]: Array(
        constraints[PlayerPosition.Defender].min
      ).fill(null),
    },
  }

  validate = () => {
    this.setState(prevState => {
      const { lineup } = prevState
      let totalSelectedPlayers = 0
      // eslint-disable-next-line no-restricted-syntax
      for (const position of [
        PlayerPosition.Defender,
        PlayerPosition.Forward,
        PlayerPosition.Goalkeeper,
        PlayerPosition.Midfielder,
      ]) {
        const { min } = constraints[position]
        const selectedPlayers = lineup[position].filter(player => !!player)
        if (selectedPlayers.length < min) {
          return { status: Status.EmptyPositions }
        }
        totalSelectedPlayers += selectedPlayers.length
      }
      if (totalSelectedPlayers < REQUIRED_NUMBER_OF_PLAYERS) {
        return { status: Status.TooFew }
      }
      if (totalSelectedPlayers > REQUIRED_NUMBER_OF_PLAYERS) {
        return { status: Status.TooMany }
      }
      return {
        status: Status.OK,
      }
    })
  }

  handleAdd = (type: PlayerPosition) => this.showPicker(type)

  handleRemove = (index: number, position: PlayerPosition) =>
    this.setState(prevState => {
      const { lineup } = prevState
      const players = lineup[position].slice()
      const { min } = constraints[position]

      if (players.length > min) {
        players.splice(index, 1)
      } else {
        players[index] = null
      }

      return {
        lineup: {
          ...lineup,
          [position]: players,
        },
      }
    }, this.validate)

  handlePick = (index: number, position: PlayerPosition) =>
    this.showPicker(position, index)

  handlePlayerPicked = (
    player: TeamPlayer,
    position: PlayerPosition,
    index?: number
  ) =>
    this.setState(prevState => {
      const { lineup } = prevState
      function getPlayers() {
        if (index === undefined) {
          return [...lineup[position], player]
        }
        const players = lineup[position].slice()
        players[index] = player
        return players
      }
      return {
        lineup: {
          ...lineup,
          [position]: getPlayers(),
        },
      }
    }, this.validate)

  showPicker = (position: PlayerPosition, index?: number) => {
    const { players, jerseyColors } = this.props
    if (!players) {
      return
    }
    const { lineup } = this.state
    const selectedPlayers = lineup[position]
    showPickerOverlay({
      data: Object.values(players).filter(player => {
        if (player.position !== position) {
          return false
        }
        if (
          selectedPlayers.find(
            selectedPlayer => selectedPlayer && selectedPlayer.id === player.id
          )
        ) {
          return false
        }
        return true
      }),
      backgroundColor: colors.navigationBackground,
      onSelect: (item: TeamPlayer) => {
        this.handlePlayerPicked(item, position, index)
        return true
      },
      renderItem: ({ item }: ListRenderItemInfo<TeamPlayer>) => (
        <PlayerPickerItem item={item} jerseyColors={jerseyColors} />
      ),
    })
  }

  handleSubmit = () => {
    const { onDone, hasBeenSubmitted } = this.props
    if (hasBeenSubmitted) {
      return
    }
    const { lineup } = this.state
    onDone(lineup)
  }

  render = () => {
    const { jerseyColors, hasBeenSubmitted } = this.props
    const { lineup, status } = this.state
    const message = getMessage(status, hasBeenSubmitted)
    const canSubmit = status === Status.OK && !hasBeenSubmitted
    return (
      <ImageBackground
        imageStyle={styles.fieldContainerImage}
        style={[styles.fieldContainer]}
        source={require('../../../../assets/images/borderless-pitch.png')}
      >
        <LineupPickerRow
          disabled={hasBeenSubmitted}
          players={lineup[PlayerPosition.Goalkeeper]}
          type={PlayerPosition.Goalkeeper}
          onAdd={this.handleAdd}
          onRemove={this.handleRemove}
          onPick={this.handlePick}
          jerseyColors={jerseyColors}
        />
        <LineupPickerRow
          disabled={hasBeenSubmitted}
          players={lineup[PlayerPosition.Defender]}
          type={PlayerPosition.Defender}
          onAdd={this.handleAdd}
          onRemove={this.handleRemove}
          onPick={this.handlePick}
          jerseyColors={jerseyColors}
        />
        <LineupPickerRow
          disabled={hasBeenSubmitted}
          players={lineup[PlayerPosition.Midfielder]}
          type={PlayerPosition.Midfielder}
          onAdd={this.handleAdd}
          onRemove={this.handleRemove}
          onPick={this.handlePick}
          jerseyColors={jerseyColors}
        />
        <LineupPickerRow
          disabled={hasBeenSubmitted}
          players={lineup[PlayerPosition.Forward]}
          type={PlayerPosition.Forward}
          onAdd={this.handleAdd}
          onRemove={this.handleRemove}
          onPick={this.handlePick}
          jerseyColors={jerseyColors}
        />
        <View style={styles.submitButtonWrapper}>
          {canSubmit && <SubmitButton onSubmit={this.handleSubmit} />}
          {message && <VoteStatusMessage message={message} />}
        </View>
        <InfoButton type={InfoType.Lineup} />
      </ImageBackground>
    )
  }
}

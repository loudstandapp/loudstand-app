import React from 'react'
import { Text, StyleSheet, View } from 'react-native'
import fonts from '../../../theme/fonts'
import colors from '../../../theme/colors'
import { headerHeight, spacingUnit, marginDefault } from '../../../theme/layout'

interface Props {
  title: string
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.tertiaryLight,
    height: headerHeight,
    borderRadius: headerHeight / 2,
    padding: spacingUnit,
    paddingHorizontal: 3 * spacingUnit,
    justifyContent: 'center',
    marginHorizontal: marginDefault,
    marginVertical: spacingUnit,
  },
  titleText: {
    flex: 1,
    ...(fonts.HeaderSmall as object),
  },
})
export default class LoudstandSectionHeader extends React.PureComponent<Props> {
  render = () => {
    const { title } = this.props
    return (
      <View style={styles.container}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
    )
  }
}

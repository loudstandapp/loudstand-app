import React from 'react'
import {
  PlayerBasic,
  LocalizedTeams,
  TeamPerformancelResultDocument,
  ManagerPerformanceResultDocument,
  ID,
} from '@loudstand/shared'
import Slider from '@react-native-community/slider'
import {
  View,
  Text,
  StyleSheet,
  ImageBackground,
  Platform,
  TouchableOpacity,
} from 'react-native'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import { ChosenTeamListItem } from '../../../store/model'
import colors from '../../../theme/colors'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import {
  localizeTeam,
  getImageUrl,
  ItemImageType,
  fallbackImages,
} from '../../../utils'
import { translate, currentLocale } from '../../../translation'
import SubmitButton from '../../../components/SubmitButton'
import VoteStatusMessage from './VoteStatusMessage'
import ChosenIndicator from './ChosenIndicator'
import FiveStarRating from './FiveStarRating'
import RatingPicker from './RatingPicker'
import RemoteImage from '../../../components/RemoteImage'

interface Props {
  team?: ChosenTeamListItem
  manager?: PlayerBasic
  onRateManager?: (rating: number, manager: PlayerBasic) => void
  onRateTeam?: (rating: number, team: ChosenTeamListItem) => void
  onSelectTeam?: (id: ID) => void
  onSelectManager?: (id: ID) => void
  allowsVoting?: boolean
  result?: TeamPerformancelResultDocument | ManagerPerformanceResultDocument
  localizedTeams?: LocalizedTeams
  chosen?: number
  hasBeenSubmitted?: boolean
}

interface State {
  value: number
}

const numberFormatter = new Intl.NumberFormat(currentLocale.languageTag, {
  minimumIntegerDigits: 1,
  maximumFractionDigits: 1,
  minimumFractionDigits: 1,
})

const IMAGE_HEIGHT = 80
const VALUE_CONTAINER_SIZE = 64
const RESULT_CONTAINER_SIZE = 50
const BAR_HEIGHT = 36
const RESULT_CONTAINER_BORDER = 4

const styles = StyleSheet.create({
  container: {},
  imageRow: {
    height: IMAGE_HEIGHT,
  },
  image: {
    height: IMAGE_HEIGHT,
    ...StyleSheet.absoluteFillObject,
  },
  contentWrapper: {
    backgroundColor: colors.tertiaryLight,
    marginBottom: marginDefault,
    marginLeft: marginDefault,
    marginRight: marginDefault,
    borderBottomLeftRadius: spacingUnit / 2,
    borderBottomRightRadius: spacingUnit / 2,
    paddingVertical: marginDefault,
    paddingHorizontal: marginDefault,
    justifyContent: 'space-between',
  },
  descriptionRow: {
    justifyContent: 'space-between',
  },
  detailsHeaderText: {
    ...(fonts.Header as object),
    marginBottom: spacingUnit / 2,
  },
  detailsSubheaderText: {
    ...(fonts.Body as object),
  },
  detailsTimeText: {
    ...(fonts.BodyBold as object),
  },
  voteContainer: {
    justifyContent: 'center',
    marginVertical: spacingUnit,
    height: VALUE_CONTAINER_SIZE,
  },
  voteBar: {
    height: BAR_HEIGHT,
    flexDirection: 'row',
    backgroundColor: colors.tertiaryAlt,
    borderTopRightRadius: BAR_HEIGHT / 2,
    borderBottomRightRadius: BAR_HEIGHT / 2,
    marginLeft: VALUE_CONTAINER_SIZE / 2,
    paddingLeft: VALUE_CONTAINER_SIZE / 2,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  resultContainer: {
    justifyContent: 'center',
    marginVertical: spacingUnit,
    height: RESULT_CONTAINER_SIZE,
  },
  resultBar: {
    height: BAR_HEIGHT,
    backgroundColor: colors.tertiaryAlt,
    borderTopRightRadius: BAR_HEIGHT / 2,
    borderBottomRightRadius: BAR_HEIGHT / 2,
    marginLeft: RESULT_CONTAINER_SIZE / 2,
    paddingLeft: RESULT_CONTAINER_SIZE / 2,
  },

  slider: {
    flex: 1,
    marginHorizontal: spacingUnit,
  },
  voteValueImage: {
    height: VALUE_CONTAINER_SIZE,
    width: VALUE_CONTAINER_SIZE,
    alignItems: 'center',
    justifyContent: 'center',
    left: 0,
    position: 'absolute',
  },
  valueText: {
    ...(fonts.CustomRating as object),
    color: colors.secondaryText,
    textAlign: 'center',
  },
  voteResultBorder: {
    borderColor: colors.secondary25pct,
    borderWidth: RESULT_CONTAINER_BORDER,
    height: RESULT_CONTAINER_SIZE + 2 * RESULT_CONTAINER_BORDER,
    width: RESULT_CONTAINER_SIZE + 2 * RESULT_CONTAINER_BORDER,
    borderRadius: RESULT_CONTAINER_SIZE / 2 + RESULT_CONTAINER_BORDER,

    left: -RESULT_CONTAINER_BORDER,
    position: 'absolute',
  },
  voteResult: {
    backgroundColor: colors.secondary,
    borderRadius: RESULT_CONTAINER_SIZE / 2,
    width: RESULT_CONTAINER_SIZE,
    height: RESULT_CONTAINER_SIZE,
    alignItems: 'center',
    justifyContent: 'center',
  },
  resultText: {
    ...(fonts.CustomRating as object),
    color: colors.secondaryText,
  },
  submitContainer: {
    alignItems: 'center',
  },
})

function getDetailsForTeam(
  team: ChosenTeamListItem,
  localizedTeams?: LocalizedTeams
) {
  const teamName = localizeTeam(team, localizedTeams)
  return {
    title: teamName,
    subtitle: translate('loudstand.teamRatingSubtitle', {
      teamName,
    }),
    imageUrl: getImageUrl(team.id, ItemImageType.TeamPhoto),
  }
}

function getDetailsForManager(manager: PlayerBasic) {
  return {
    title: manager.name,
    subtitle: translate('loudstand.managerRatingSubtitle', {
      managerName: manager.name,
    }),
    imageUrl: getImageUrl(manager.id, ItemImageType.Actionshot),
  }
}

const INITIAL_VALUE = 2.5
const MAX_VALUE = 5
const MIN_VALUE = 0
const STEP = 0.5

export default class PerformanceRating extends React.Component<Props, State> {
  state = { value: INITIAL_VALUE }

  handleSliderChange = (value: number) => {
    ReactNativeHapticFeedback.trigger('selection')

    this.setState({ value })
  }

  handleSubmit = () => {
    const { onRateManager, onRateTeam, team, manager } = this.props
    const { value } = this.state
    if (onRateManager && manager) {
      onRateManager(value, manager)
      return
    }
    if (onRateTeam && team) {
      onRateTeam(value, team)
      return
    }
    throw new Error(
      'PerformanceRating needs either onRateManager or onRateTeam  prop'
    )
  }

  renderInput = (title: string, subtitle: string) => {
    const { value } = this.state
    const { hasBeenSubmitted } = this.props
    return (
      <View style={styles.contentWrapper}>
        <View style={styles.descriptionRow}>
          <Text
            style={styles.detailsHeaderText}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {title}
          </Text>
          <Text style={styles.detailsSubheaderText}>{subtitle}</Text>
        </View>
        <View style={styles.voteContainer}>
          <View style={styles.voteBar}>
            {Platform.select({
              ios: (
                <Slider
                  disabled={hasBeenSubmitted}
                  style={styles.slider}
                  step={STEP}
                  minimumValue={MIN_VALUE}
                  maximumValue={MAX_VALUE}
                  value={value}
                  onValueChange={this.handleSliderChange}
                  maximumTrackTintColor={colors.secondary}
                  minimumTrackTintColor={colors.primary}
                />
              ),
              android: (
                <RatingPicker
                  onSelect={this.handleSliderChange}
                  disabled={hasBeenSubmitted}
                />
              ),
            })}
          </View>
          <ImageBackground
            style={styles.voteValueImage}
            source={require('../../../../assets/icons/indicators/big-star.png')}
          >
            <Text style={styles.valueText}>
              {numberFormatter.format(value)}
            </Text>
          </ImageBackground>
        </View>
        <View style={styles.submitContainer}>
          {hasBeenSubmitted ? (
            <VoteStatusMessage message={translate('loudstand.voteSubmitted')} />
          ) : (
            <SubmitButton onSubmit={this.handleSubmit} />
          )}
        </View>
      </View>
    )
  }

  renderResult = (title: string) => {
    const { result, chosen } = this.props
    if (result === undefined) {
      return false
    }
    return (
      <View style={styles.contentWrapper}>
        <View style={styles.descriptionRow}>
          <Text
            style={styles.detailsHeaderText}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {title}
          </Text>
        </View>
        <View style={styles.resultContainer}>
          <View style={styles.resultBar}>
            <FiveStarRating value={result.rating} />
          </View>
          <View style={styles.voteResultBorder}>
            <View style={styles.voteResult}>
              <Text style={styles.valueText}>
                {numberFormatter.format(result.rating)}
              </Text>
            </View>
          </View>
        </View>
        {chosen !== undefined && (
          <ChosenIndicator value={numberFormatter.format(chosen)} />
        )}
      </View>
    )
  }

  handleSelect = () => {
    const { manager, team, onSelectTeam, onSelectManager } = this.props
    if (team && onSelectTeam) {
      onSelectTeam(team.id)
      return
    }
    if (manager && onSelectManager) {
      onSelectManager(manager.id)
    }
  }

  render = () => {
    const { allowsVoting, manager, team, localizedTeams } = this.props
    if (!manager && !team) {
      throw new Error('PerformanceRating needs either manager or team prop')
    }

    const details =
      (manager && getDetailsForManager(manager)) ||
      (team && getDetailsForTeam(team, localizedTeams))
    if (!details) {
      return false
    }
    const { imageUrl, title, subtitle } = details

    return (
      <View style={styles.container}>
        <View style={styles.imageRow}>
          <TouchableOpacity onPress={this.handleSelect}>
            {imageUrl && (
              <RemoteImage
                source={{ uri: imageUrl }}
                style={styles.image}
                defaultSource={
                  manager
                    ? fallbackImages.managerActionShot
                    : fallbackImages.teamPhoto
                }
              />
            )}
          </TouchableOpacity>
        </View>
        {allowsVoting
          ? this.renderInput(title, subtitle)
          : this.renderResult(title)}
      </View>
    )
  }
}

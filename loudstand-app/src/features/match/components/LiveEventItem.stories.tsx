import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { MatchEventType } from '@loudstand/shared'
import LiveEventItem from './LiveEventItem'

const TEST_RESULT = {
  yes: 0.4,
  no: 0.6,
}

storiesOf('LiveEventItem', module)
  .add('goal', () => (
    <LiveEventItem
      allowsVoting
      hasBeenAnswered={false}
      onResponse={action('onResponse')}
      result={TEST_RESULT}
      event={{
        id: '559100035',
        data: {
          homeScore: 1,
          awayScore: 1,
          player: {
            id: 'sr:player:99545',
            name: 'Danilo Pereira',
          },
          type: MatchEventType.Goal,
        },
        seconds: 4671,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('substitution', () => (
    <LiveEventItem
      allowsVoting
      hasBeenAnswered={false}
      onResponse={action('onResponse')}
      result={TEST_RESULT}
      event={{
        id: '559098415',
        data: {
          type: MatchEventType.Substution,
          playerOut: {
            id: 'sr:player:312110',
            name: 'Alex Telles',
          },
          playerIn: {
            id: 'sr:player:116869',
            name: 'Vincent Aboubakar',
          },
        },
        seconds: 4520,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('yellow card', () => (
    <LiveEventItem
      allowsVoting
      hasBeenAnswered={false}
      onResponse={action('onResponse')}
      result={TEST_RESULT}
      event={{
        id: '559103199',
        data: {
          player: {
            id: 'sr:player:99545',
            name: 'Danilo Pereira',
          },
          type: MatchEventType.YellowCard,
        },
        seconds: 5136,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('red card', () => (
    <LiveEventItem
      allowsVoting
      hasBeenAnswered={false}
      onResponse={action('onResponse')}
      result={TEST_RESULT}
      event={{
        id: '559103199',
        data: {
          player: {
            id: 'sr:player:99545',
            name: 'Danilo Pereira',
          },
          type: MatchEventType.RedCard,
        },
        seconds: 5136,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('yellow red card', () => (
    <LiveEventItem
      allowsVoting
      hasBeenAnswered={false}
      onResponse={action('onResponse')}
      result={TEST_RESULT}
      event={{
        id: '559103199',
        data: {
          player: {
            id: 'sr:player:99545',
            name: 'Danilo Pereira',
          },
          type: MatchEventType.SecondYellowCard,
        },
        seconds: 5136,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('penalty', () => (
    <LiveEventItem
      allowsVoting
      hasBeenAnswered={false}
      onResponse={action('onResponse')}
      result={TEST_RESULT}
      event={{
        id: '559103199',
        data: {
          player: {
            id: 'sr:player:99545',
            name: 'Danilo Pereira',
          },
          type: MatchEventType.Penalty,
        },
        seconds: 5136,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('answered', () => (
    <LiveEventItem
      allowsVoting
      hasBeenAnswered
      onResponse={action('onResponse')}
      result={TEST_RESULT}
      event={{
        id: '559103199',
        data: {
          player: {
            id: 'sr:player:99545',
            name: 'Danilo Pereira',
          },
          type: MatchEventType.RedCard,
        },
        seconds: 5136,
        isReactable: true,
        team: 'home',
      }}
    />
  ))

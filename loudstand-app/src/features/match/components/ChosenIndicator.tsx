import React from 'react'
import { View, StyleSheet, Text } from 'react-native'
import fonts from '../../../theme/fonts'
import { translate } from '../../../translation'
import { spacingUnit } from '../../../theme/layout'

const styles = StyleSheet.create({
  chosenContainer: {
    marginTop: spacingUnit,
  },
  chosenText: {
    ...(fonts.BodySmall as object),
  },
  chosenValue: {
    ...(fonts.BodySmallBold as object),
  },
})

interface Props {
  value: string
}

// eslint-disable-next-line react/prefer-stateless-function
export default class ChosenIndicator extends React.PureComponent<Props> {
  render = () => {
    const { value } = this.props
    return (
      <View style={styles.chosenContainer}>
        <Text style={styles.chosenText}>
          {`${translate('loudstand.youVoted')} `}
          <Text style={styles.chosenValue}>{value}</Text>
        </Text>
      </View>
    )
  }
}

import React from 'react'
import {
  MatchEvent,
  QuestionResult,
  QuestionResponseType,
  MatchEventType,
  UserVoteHistory,
  generateVoteKey,
  VoteType,
} from '@loudstand/shared'
import { StyleSheet, View, Image, Text } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import QuestionOptionsBar from '../../../components/QuestionOptionsBar'
import { translate } from '../../../translation'
import {
  formatEventTime,
  getImageUrl,
  ItemImageType,
  fallbackImages,
} from '../../../utils'
import colors from '../../../theme/colors'
import { spacingUnit, marginDefault } from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import { SubmittedVotes } from '../../../store/model'

const IMAGE_HEIGHT = 80
const ICON_WRAPPER_HEIGHT = 40

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  imageRow: {
    // flexDirection: 'row',
    height: IMAGE_HEIGHT,
    // alignItems: 'flex-end',
  },
  onlyImage: {
    ...StyleSheet.absoluteFillObject,
    height: IMAGE_HEIGHT,
    width: '100%',
  },
  firstImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: '50%',
    bottom: 0,
    flex: 1,
  },
  secondImage: {
    position: 'absolute',
    top: 0,
    right: 0,
    width: '50%',
    bottom: 0,
  },
  optionsBarWrapper: {
    flex: 1,
    // height: '100%',
    justifyContent: 'flex-end',
  },
  detailsRow: {
    flexDirection: 'row',
    backgroundColor: colors.tertiaryLight,
    marginBottom: marginDefault,
    marginLeft: marginDefault,
    marginRight: marginDefault,
    borderBottomLeftRadius: spacingUnit / 2,
    borderBottomRightRadius: spacingUnit / 2,
    paddingVertical: spacingUnit,
    paddingHorizontal: marginDefault,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  detailsWrapper: {
    flex: 1,
  },
  detailsHeaderText: {
    ...(fonts.Header as object),
    marginBottom: spacingUnit / 2,
  },
  detailsSubheaderText: {
    ...(fonts.Body as object),
  },
  detailsTimeText: {
    ...(fonts.BodyBold as object),
  },
  detailsIconWrapper: {
    backgroundColor: colors.background,
    borderRadius: ICON_WRAPPER_HEIGHT / 2,
    height: ICON_WRAPPER_HEIGHT,
    paddingVertical: spacingUnit,
    paddingHorizontal: 3 * spacingUnit,
  },
  detailsIconImage: {
    // height: ICON_HEIGHT,
  },
})

interface Props {
  event: MatchEvent
  voteHistory?: UserVoteHistory
  allowsVoting: boolean
  result?: QuestionResult
  chosen?: QuestionResponseType
  submittedVotes?: SubmittedVotes
  onResponse?: (event: MatchEvent, response: QuestionResponseType) => void
}

interface State {
  useSingleFallback: boolean
}

function getDataFromEvent(event: MatchEvent) {
  switch (event.data.type) {
    case MatchEventType.YellowCard:
      return {
        onlyImageSource: event.data.player
          ? {
              uri: getImageUrl(event.data.player.id, ItemImageType.Actionshot),
            }
          : fallbackImages.yellowCard,
        singleFallbackImageSource: fallbackImages.yellowCard,
        title: event.data.player ? event.data.player.name : '',
        subtitle: translate('eventType.yellowCard'),
        time: formatEventTime(event.seconds),
        icon: require('../../../../assets/icons/indicators/yellow-card.png'),
      }
    case MatchEventType.RedCard:
      return {
        onlyImageSource: event.data.player
          ? {
              uri: getImageUrl(event.data.player.id, ItemImageType.Actionshot),
            }
          : fallbackImages.redCard,
        singleFallbackImageSource: fallbackImages.redCard,
        title: event.data.player ? event.data.player.name : '',
        subtitle: translate('eventType.redCard'),
        time: formatEventTime(event.seconds),
        icon: require('../../../../assets/icons/indicators/red-card.png'),
      }
    case MatchEventType.SecondYellowCard:
      return {
        onlyImageSource: event.data.player
          ? {
              uri: getImageUrl(event.data.player.id, ItemImageType.Actionshot),
            }
          : fallbackImages.redCard,
        singleFallbackImageSource: fallbackImages.redCard,
        title: event.data.player ? event.data.player.name : '',
        subtitle: translate('eventType.yellowRedCard'),
        time: formatEventTime(event.seconds),
        icon: require('../../../../assets/icons/indicators/red-card.png'),
      }
    case MatchEventType.Substitution:
      return {
        onlyImageSource:
          !event.data.playerIn &&
          !event.data.playerOut &&
          fallbackImages.substitution,
        secondImageSource: event.data.playerIn &&
          event.data.playerOut && {
            uri: getImageUrl(event.data.playerIn.id, ItemImageType.Actionshot),
          },
        firstImageSource: event.data.playerIn &&
          event.data.playerOut && {
            uri: getImageUrl(event.data.playerOut.id, ItemImageType.Actionshot),
          },
        singleFallbackImageSource: fallbackImages.substitution,
        title:
          event.data.playerIn && event.data.playerOut
            ? `${event.data.playerOut.name} - ${event.data.playerIn.name}`
            : '',
        subtitle: translate('eventType.substitution'),
        time: formatEventTime(event.seconds),
        icon: require('../../../../assets/icons/indicators/substitution.png'),
      }
    case MatchEventType.Penalty:
      return {
        onlyImageSource: event.data.player
          ? {
              uri: getImageUrl(event.data.player.id, ItemImageType.Actionshot),
            }
          : fallbackImages.penalty,
        singleFallbackImageSource: fallbackImages.penalty,
        title: event.data.player ? event.data.player.name : '',
        subtitle: translate('eventType.penalty'),
        time: formatEventTime(event.seconds),
        icon: require('../../../../assets/icons/indicators/penalty.png'),
      }
    default: {
      return null
    }
  }
}

const voteTypeFromMatchEventType: {
  [key in MatchEventType]: VoteType | null
} = {
  [MatchEventType.Goal]: null,
  [MatchEventType.PeriodStart]: null,
  [MatchEventType.MatchStarted]: null,
  [MatchEventType.MatchEnded]: null,
  [MatchEventType.RedCard]: VoteType.RedCard,
  [MatchEventType.YellowCard]: VoteType.YellowCard,
  [MatchEventType.SecondYellowCard]: VoteType.YellowRedCard,
  [MatchEventType.Penalty]: VoteType.Penalty,
  [MatchEventType.Substitution]: VoteType.Substitution,
}

export default class LiveEventItem extends React.PureComponent<Props, State> {
  constructor(props: Props) {
    super(props)
    const voteKey = voteTypeFromMatchEventType[props.event.data.type]
    if (voteKey) {
      this.voteKey = generateVoteKey(voteKey, [
        {
          event: props.event.id,
        },
      ])
    } else {
      this.voteKey = null
    }
    this.state = { useSingleFallback: false }
  }

  handleYes = () => {
    const { onResponse, event } = this.props
    onResponse && onResponse(event, QuestionResponseType.Yes)
  }

  handleNo = () => {
    const { onResponse, event } = this.props
    onResponse && onResponse(event, QuestionResponseType.No)
  }

  handleImageError = () => this.setState({ useSingleFallback: true })

  voteKey: string | null

  renderImages = () => {
    const { event } = this.props
    const { useSingleFallback } = this.state
    const details = getDataFromEvent(event)
    if (!details) {
      return false
    }
    const {
      onlyImageSource,
      firstImageSource,
      secondImageSource,
      singleFallbackImageSource,
    } = details

    if (useSingleFallback && singleFallbackImageSource) {
      return (
        <Image
          source={singleFallbackImageSource}
          style={styles.onlyImage}
          onError={this.handleImageError}
        />
      )
    }
    if (onlyImageSource) {
      return (
        <Image
          source={onlyImageSource}
          style={styles.onlyImage}
          onError={this.handleImageError}
        />
      )
    }
    return (
      <>
        {firstImageSource && (
          <>
            <Image
              source={firstImageSource}
              style={styles.firstImage}
              onError={this.handleImageError}
            />
            <LinearGradient
              colors={['transparent', 'black']}
              locations={[0.98, 1]}
              start={{ x: 0, y: 0 }}
              end={{ x: 1, y: 0 }}
              style={styles.firstImage}
            />
          </>
        )}
        {secondImageSource && (
          <>
            <Image
              source={secondImageSource}
              style={styles.secondImage}
              onError={this.handleImageError}
            />
            <LinearGradient
              colors={['black', 'transparent']}
              locations={[0, 0.02]}
              start={{ x: 0, y: 0.0 }}
              end={{ x: 1, y: 0 }}
              style={styles.secondImage}
            />
          </>
        )}
      </>
    )
  }

  render = () => {
    const {
      event,
      result,
      voteHistory,
      allowsVoting,
      chosen,
      submittedVotes,
    } = this.props
    const hasBeenAnswered =
      voteHistory && this.voteKey && !!voteHistory[this.voteKey]
    const details = getDataFromEvent(event)
    if (!details) {
      return false
    }
    const { title, subtitle, time, icon } = details
    const chosenOption =
      chosen ||
      (this.voteKey &&
        voteHistory &&
        voteHistory[this.voteKey] &&
        voteHistory[this.voteKey].response)
    const hasBeenSubmitted =
      this.voteKey && submittedVotes && submittedVotes[this.voteKey]
    return (
      <View style={styles.container}>
        <View style={styles.imageRow}>
          {this.renderImages()}
          <LinearGradient
            colors={['transparent', 'rgba(0,0,0,0.8)']}
            style={styles.optionsBarWrapper}
          >
            <QuestionOptionsBar
              result={result}
              allowsVoting={allowsVoting}
              onNo={this.handleNo}
              onYes={this.handleYes}
              chosen={chosenOption}
              hasBeenSubmitted={!!hasBeenSubmitted}
              hasBeenAnswered={!!hasBeenAnswered}
            />
          </LinearGradient>
        </View>
        <View style={styles.detailsRow}>
          <View style={styles.detailsWrapper}>
            <Text
              style={styles.detailsHeaderText}
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              {title}
            </Text>
            <Text style={styles.detailsSubheaderText}>
              {`${subtitle} `}
              <Text style={styles.detailsTimeText}>{time}</Text>
            </Text>
          </View>
          <View style={styles.detailsIconWrapper}>
            <Image style={styles.detailsIconImage} source={icon} />
          </View>
        </View>
      </View>
    )
  }
}

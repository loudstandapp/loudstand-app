import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { MatchEventType } from '@loudstand/shared'
import SummaryRow from './SummaryRow'

storiesOf('SummaryRow', module)
  .add('yellow card home very long name', () => (
    <SummaryRow
      event={{
        id: '559061057',
        data: {
          player: {
            id: 'sr:player:957322',
            name: "Cristian Borja Long long name you know what i'm sayin'?",
          },
          type: MatchEventType.YellowCard,
        },
        seconds: 1147,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('yellow card away very long name', () => (
    <SummaryRow
      event={{
        id: '559061057',
        data: {
          player: {
            id: 'sr:player:957322',
            name: "Cristian Borja Long long name you know what i'm sayin'?",
          },
          type: MatchEventType.YellowCard,
        },
        seconds: 1147,
        isReactable: true,
        team: 'away',
      }}
    />
  ))
  .add('red card home', () => (
    <SummaryRow
      event={{
        id: '559061057',
        data: {
          player: {
            id: 'sr:player:957322',
            name: 'Cristian Borja',
          },
          type: MatchEventType.RedCard,
        },
        seconds: 1147,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('red card away', () => (
    <SummaryRow
      event={{
        id: '559061057',
        data: {
          player: {
            id: 'sr:player:957322',
            name: 'Cristian Borja',
          },
          type: MatchEventType.RedCard,
        },
        seconds: 1147,
        isReactable: true,
        team: 'away',
      }}
    />
  ))
  .add('penalty', () => (
    <SummaryRow
      event={{
        id: '559061057',
        data: {
          player: {
            id: 'sr:player:957322',
            name: 'Cristian Borja',
          },
          type: MatchEventType.Penalty,
        },
        seconds: 1147,
        isReactable: true,
        team: 'away',
      }}
    />
  ))
  .add('substituion home', () => (
    <SummaryRow
      event={{
        id: '559098415',
        data: {
          type: MatchEventType.Substution,
          playerOut: {
            id: 'sr:player:312110',
            name: 'Alex Telles',
          },
          playerIn: {
            id: 'sr:player:116869',
            name: 'Vincent Aboubakar',
          },
        },
        seconds: 4520,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('substituion away', () => (
    <SummaryRow
      event={{
        id: '559093027',
        data: {
          type: MatchEventType.Substution,
          playerOut: {
            id: 'sr:player:252819',
            name: 'Bruno Gaspar',
          },
          playerIn: {
            id: 'sr:player:167655',
            name: 'Tiago Ilori',
          },
        },
        seconds: 3870,
        isReactable: true,
        team: 'away',
      }}
    />
  ))
  .add('goal home', () => (
    <SummaryRow
      event={{
        id: '559091893',
        data: {
          homeScore: 0,
          awayScore: 1,
          player: {
            id: 'sr:player:291707',
            name: 'Luiz Phellype',
          },
          type: MatchEventType.Goal,
        },
        seconds: 3654,
        isReactable: true,
        team: 'home',
      }}
    />
  ))
  .add('goal away', () => (
    <SummaryRow
      event={{
        id: '559091893',
        data: {
          homeScore: 0,
          awayScore: 1,
          player: {
            id: 'sr:player:291707',
            name: 'Luiz Phellype',
          },
          type: MatchEventType.Goal,
        },
        seconds: 3654,
        isReactable: true,
        team: 'away',
      }}
    />
  ))
  .add('yellow red card', () => (
    <SummaryRow
      event={{
        id: '559091893',
        data: {
          player: {
            id: 'sr:player:291707',
            name: 'Luiz Phellype',
          },
          type: MatchEventType.SecondYellowCard,
        },
        seconds: 3654,
        isReactable: true,
        team: 'away',
      }}
    />
  ))

import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { MatchEvent, MatchEventType, ID } from '@loudstand/shared'
import LinearGradient from 'react-native-linear-gradient'
import colors from '../../../theme/colors'
import fonts from '../../../theme/fonts'
import { marginDefault, spacingUnit } from '../../../theme/layout'
import { formatEventTime } from '../../../utils'

const SMALL_CIRCLE_SIZE = 8
const LARGE_CIRCLE_SIZE = 40
const LARGE_CIRCLE_BORDER = 4
const VERTICAL_PADDING = spacingUnit
const SMALL_TIME_WIDTH = 36

const styles = StyleSheet.create({
  substitutionPlayersWrapper: {
    flex: 1,
  },
  playerOutText: {
    color: colors.tertiary,
  },
  genericEvent: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flex: 1,
    marginEnd: -SMALL_TIME_WIDTH * 0.6,
  },
  touchablePlayerName: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flex: 1,
  },
  touchablePlayerNameAway: {
    justifyContent: 'flex-start',
  },
  goalEvent: {
    marginEnd: -SMALL_TIME_WIDTH * 0.3,
  },
  goalAccessoryText: {
    ...(fonts.CustomMatchEventTimeLight as object),
    color: colors.tertiary,
    marginHorizontal: 2 * spacingUnit,
  },

  awayPlayerNameText: {
    textAlign: 'left',
  },
  playerNameText: {
    ...(fonts.BodyBold as object),
    textAlign: 'right',
  },
  eventImage: {
    margin: spacingUnit,
  },
  multipleImageWrapper: {
    margin: spacingUnit,
    flexDirection: 'row',
  },

  summaryRow: {
    alignItems: 'center',
    flex: 1,
  },
  eventDetails: {
    flex: 1,
    paddingVertical: marginDefault,
    flexDirection: 'column',
  },
  placeholder: { flex: 1 },
  homeEvent: {
    flexDirection: 'row',
  },
  awayEvent: {
    flexDirection: 'row-reverse',
  },
  homeEventDetails: {
    alignItems: 'flex-end',
  },
  awayEventDetails: {
    alignItems: 'flex-start',
  },
  time: {
    alignItems: 'center',
    marginHorizontal: spacingUnit,
  },
  centerLine: {
    position: 'absolute',
    top: -VERTICAL_PADDING,
    bottom: -VERTICAL_PADDING,
    width: 1,
    backgroundColor: colors.secondary10pct,
    height: '100%',
  },
  smallTimeWrapper: {
    width: 2 * SMALL_TIME_WIDTH,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: spacingUnit,
  },
  smallTimeText: {
    ...(fonts.CustomMatchEventTimeLight as object),
    color: colors.tertiaryLight,
    flex: 1,
  },
  smallTimeDot: {
    width: SMALL_CIRCLE_SIZE,
    height: SMALL_CIRCLE_SIZE,
    borderRadius: SMALL_CIRCLE_SIZE / 2,
    flex: 0,
  },
  smallTimePlaceholder: {
    flex: 1,
  },
  secondaryTimeOuterWrapper: {
    width: LARGE_CIRCLE_SIZE + LARGE_CIRCLE_BORDER,
    height: LARGE_CIRCLE_SIZE + LARGE_CIRCLE_BORDER,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: colors.primary,
    borderRadius: (LARGE_CIRCLE_SIZE + LARGE_CIRCLE_BORDER) / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  secondaryTimeInnerWrapper: {
    width: LARGE_CIRCLE_SIZE,
    height: LARGE_CIRCLE_SIZE,
    borderRadius: LARGE_CIRCLE_SIZE / 2,
    backgroundColor: colors.tertiaryLight,
    alignItems: 'center',
    justifyContent: 'center',
  },
  secondaryTimeText: {
    ...(fonts.CustomMatchEventTime as object),
    color: colors.primaryText,
  },
  primaryTimeOuterWrapper: {
    width: LARGE_CIRCLE_SIZE + LARGE_CIRCLE_BORDER,
    height: LARGE_CIRCLE_SIZE + LARGE_CIRCLE_BORDER,
    borderWidth: LARGE_CIRCLE_BORDER,
    borderColor: colors.tertiaryLight,
    borderRadius: (LARGE_CIRCLE_SIZE + LARGE_CIRCLE_BORDER) / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  primaryTimeInnerWrapper: {
    width: LARGE_CIRCLE_SIZE,
    height: LARGE_CIRCLE_SIZE,
    borderRadius: LARGE_CIRCLE_SIZE / 2,
    backgroundColor: colors.tertiaryLight,
    alignItems: 'center',
    justifyContent: 'center',
  },
  primaryTimeText: {
    ...(fonts.CustomMatchEventTime as object),
    color: colors.secondaryText,
  },
})

interface Props {
  event: MatchEvent
  onSelectPlayer: (id: ID) => void
}

function getIndicatorType(eventType: MatchEventType) {
  switch (eventType) {
    case MatchEventType.Goal:
      return { isPrimary: true }
    case MatchEventType.Substitution:
      return { isSecondary: true }
    default:
      return { isSmall: true }
  }
}

export default class SummaryRow extends React.PureComponent<Props> {
  renderEvent = (event: MatchEvent) => {
    const { team } = event
    const { onSelectPlayer } = this.props
    const isAway = team === 'away'
    switch (event.data.type) {
      case MatchEventType.Goal: {
        const {
          data: { player },
        } = event
        return (
          <View
            style={[
              styles.genericEvent,
              styles.goalEvent,
              isAway && styles.awayEvent,
            ]}
          >
            {player && (
              <TouchableOpacity
                style={[
                  styles.touchablePlayerName,
                  isAway && styles.touchablePlayerNameAway,
                ]}
                onPress={() => onSelectPlayer(player.id)}
              >
                <Text
                  style={[
                    styles.playerNameText,
                    isAway && styles.awayPlayerNameText,
                  ]}
                >
                  {player.name}
                </Text>
              </TouchableOpacity>
            )}
            <Image
              style={styles.eventImage}
              source={require('../../../../assets/icons/indicators/football.png')}
            />
          </View>
        )
      }
      case MatchEventType.Penalty: {
        const {
          data: { player },
        } = event
        return (
          <View style={[styles.genericEvent, isAway && styles.awayEvent]}>
            {player && (
              <TouchableOpacity
                style={[
                  styles.touchablePlayerName,
                  isAway && styles.touchablePlayerNameAway,
                ]}
                onPress={() => onSelectPlayer(player.id)}
              >
                <Text
                  style={[
                    styles.playerNameText,
                    isAway && styles.awayPlayerNameText,
                  ]}
                >
                  {player.name}
                </Text>
              </TouchableOpacity>
            )}
            <Image
              style={styles.eventImage}
              source={require('../../../../assets/icons/indicators/penalty.png')}
            />
          </View>
        )
      }
      case MatchEventType.RedCard: {
        const {
          data: { player },
        } = event
        return (
          <View style={[styles.genericEvent, isAway && styles.awayEvent]}>
            {player && (
              <TouchableOpacity
                style={[
                  styles.touchablePlayerName,
                  isAway && styles.touchablePlayerNameAway,
                ]}
                onPress={() => onSelectPlayer(player.id)}
              >
                <Text
                  style={[
                    styles.playerNameText,
                    isAway && styles.awayPlayerNameText,
                  ]}
                >
                  {player.name}
                </Text>
              </TouchableOpacity>
            )}
            <Image
              style={styles.eventImage}
              source={require('../../../../assets/icons/indicators/red-card.png')}
            />
          </View>
        )
      }
      case MatchEventType.SecondYellowCard: {
        const {
          data: { player },
        } = event
        return (
          <View style={[styles.genericEvent, isAway && styles.awayEvent]}>
            {player && (
              <TouchableOpacity
                style={[
                  styles.touchablePlayerName,
                  isAway && styles.touchablePlayerNameAway,
                ]}
                onPress={() => onSelectPlayer(player.id)}
              >
                <Text
                  style={[
                    styles.playerNameText,
                    isAway && styles.awayPlayerNameText,
                  ]}
                >
                  {player.name}
                </Text>
              </TouchableOpacity>
            )}
            <View style={styles.multipleImageWrapper}>
              <Image
                source={require('../../../../assets/icons/indicators/red-card.png')}
              />
              <Image
                source={require('../../../../assets/icons/indicators/yellow-card.png')}
              />
            </View>
          </View>
        )
      }
      case MatchEventType.Substitution: {
        const {
          data: { playerIn, playerOut },
        } = event
        return (
          <View
            style={[
              styles.genericEvent,
              styles.goalEvent,
              isAway && styles.awayEvent,
            ]}
          >
            <View style={styles.substitutionPlayersWrapper}>
              {playerOut && (
                <TouchableOpacity
                  style={[
                    styles.touchablePlayerName,
                    isAway && styles.touchablePlayerNameAway,
                  ]}
                  onPress={() => onSelectPlayer(playerOut.id)}
                >
                  <Text
                    style={[
                      styles.playerNameText,
                      styles.playerOutText,
                      isAway && styles.awayPlayerNameText,
                    ]}
                  >
                    {playerOut.name}
                  </Text>
                </TouchableOpacity>
              )}
              {playerIn && (
                <TouchableOpacity
                  style={[
                    styles.touchablePlayerName,
                    isAway && styles.touchablePlayerNameAway,
                  ]}
                  onPress={() => onSelectPlayer(playerIn.id)}
                >
                  <Text
                    style={[
                      styles.playerNameText,
                      isAway && styles.awayPlayerNameText,
                    ]}
                  >
                    {playerIn.name}
                  </Text>
                </TouchableOpacity>
              )}
            </View>
            <Image
              style={styles.eventImage}
              source={require('../../../../assets/icons/indicators/substitution.png')}
            />
          </View>
        )
      }
      case MatchEventType.YellowCard: {
        const {
          data: { player },
        } = event
        return (
          <View style={[styles.genericEvent, isAway && styles.awayEvent]}>
            {player && (
              <TouchableOpacity
                style={[
                  styles.touchablePlayerName,
                  isAway && styles.touchablePlayerNameAway,
                ]}
                onPress={() => onSelectPlayer(player.id)}
              >
                <Text
                  style={[
                    styles.playerNameText,
                    isAway && styles.awayPlayerNameText,
                  ]}
                >
                  {player.name}
                </Text>
              </TouchableOpacity>
            )}
            <Image
              style={styles.eventImage}
              source={require('../../../../assets/icons/indicators/yellow-card.png')}
            />
          </View>
        )
      }
      default:
        return null
    }
  }

  renderEventAccessory = (event: MatchEvent) => {
    const isHome = event.team === 'home'

    switch (event.data.type) {
      case MatchEventType.Goal: {
        const score =
          event.data.homePenaltyScore !== null &&
          event.data.awayPenaltyScore !== null
            ? `${event.data.homePenaltyScore} - ${event.data.awayPenaltyScore}`
            : `${event.data.homeScore} - ${event.data.awayScore}`
        return (
          <View
            style={[
              // styles.goalAccessory,
              styles.genericEvent,
              // Yes we reverse things on the other side :)
              isHome && styles.awayEvent,
            ]}
          >
            <Text style={styles.goalAccessoryText}>{score}</Text>
          </View>
        )
      }
      default:
        return null
    }
  }

  render = () => {
    const { event } = this.props
    const { isPrimary, isSecondary, isSmall } = getIndicatorType(
      event.data.type
    )
    return (
      <View
        style={[
          styles.summaryRow,
          event.team === 'home' ? styles.homeEvent : styles.awayEvent,
        ]}
      >
        <View
          style={[
            styles.eventDetails,
            event.team === 'home'
              ? styles.homeEventDetails
              : styles.awayEventDetails,
          ]}
        >
          {this.renderEvent(event)}
        </View>

        {isPrimary && (
          <View style={styles.time}>
            <View style={styles.centerLine} />
            <View style={styles.primaryTimeOuterWrapper}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[colors.primary, colors.primaryAlt]}
                style={styles.primaryTimeInnerWrapper}
              >
                <Text style={styles.primaryTimeText}>
                  {formatEventTime(event.seconds)}
                </Text>
              </LinearGradient>
            </View>
          </View>
        )}

        {isSecondary && (
          <View style={styles.time}>
            <View style={styles.centerLine} />
            <View style={styles.secondaryTimeOuterWrapper}>
              <View style={styles.secondaryTimeInnerWrapper}>
                <Text style={styles.secondaryTimeText}>
                  {formatEventTime(event.seconds)}
                </Text>
              </View>
            </View>
          </View>
        )}
        {isSmall && (
          <View style={styles.time}>
            <View style={styles.centerLine} />
            <View style={styles.smallTimeWrapper}>
              <Text style={styles.smallTimeText}>
                {formatEventTime(event.seconds)}
              </Text>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[colors.primary, colors.primaryAlt]}
                style={styles.smallTimeDot}
              />
              <View style={styles.smallTimePlaceholder} />
            </View>
          </View>
        )}
        <View style={styles.placeholder}>
          {this.renderEventAccessory(event)}
        </View>
      </View>
    )
  }
}

import React from 'react'
import { MatchRoot } from '@loudstand/shared'
import { View, StyleSheet } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import MatchHeader from './MatchHeader'
import colors from '../../../theme/colors'

interface Props {
  onClose: () => void
  matchRoot?: MatchRoot
  children: React.ReactNode
  statusBarHeight: number
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  main: {
    flex: 1,
  },
})

export default function MatchScreenWrapper({
  onClose,
  matchRoot,
  children,
  statusBarHeight,
}: Props) {
  return (
    <LinearGradient
      style={styles.container}
      colors={[colors.background, colors.backgroundAlt]}
    >
      {matchRoot && (
        <MatchHeader
          statusBarHeight={statusBarHeight}
          onClose={onClose}
          matchRoot={matchRoot}
        />
      )}
      <View style={styles.main}>{children}</View>
    </LinearGradient>
  )
}

MatchScreenWrapper.defaultProps = {
  statusBarHeight: 20,
}

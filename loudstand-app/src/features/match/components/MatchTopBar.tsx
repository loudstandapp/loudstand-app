import React from 'react'
import { MatchRoot, MatchState, ID } from '@loudstand/shared'
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import colors from '../../../theme/colors'
import {
  marginDefault,
  spacingUnit,
  touchableSize,
} from '../../../theme/layout'
import fonts from '../../../theme/fonts'
import { getImageUrl, ItemImageType, fallbackImages } from '../../../utils'
import RemoteImage from '../../../components/RemoteImage'
import RemoteImageBackground from '../../../components/RemoteImageBackground'
import MatchTime from './MatchTime'
import BackButton from '../../../components/BackButton'

const LOGO_SIZE = 60
const HEIGHT = 100

const styles = StyleSheet.create({
  container: {},
  contentWrapper: {
    paddingBottom: spacingUnit,
    paddingTop: spacingUnit,
    paddingHorizontal: 3 * marginDefault,
    height: HEIGHT,
  },
  mainContent: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  safeArea: {},
  teamLogo: {
    width: LOGO_SIZE,
    height: LOGO_SIZE,
  },
  standingsText: {
    ...(fonts.CustomMatchScore as object),
    color: colors.secondaryText,
  },
  penaltyScoreText: {
    ...(fonts.BodySmall as object),
    color: colors.secondaryText,
  },
  vsText: {
    ...(fonts.TopBarTitle as object),
    color: colors.secondaryText,
  },
  stadiumNameText: {
    ...(fonts.BodyStrong as object),
    marginTop: spacingUnit / 2,
    color: colors.secondaryText,
    textAlign: 'center',
  },
  textContainer: {
    alignItems: 'center',
    flex: 1,
  },
  backButtonWrapper: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
    width: touchableSize,
  },
  backButtonImage: {
    tintColor: colors.secondaryText,
  },
})

interface Props {
  matchRoot?: MatchRoot
  onBack: () => void
  onSelectTeam: (id: ID) => void
}

export default class MatchTopBar extends React.PureComponent<Props> {
  render = () => {
    const { onBack, matchRoot, onSelectTeam } = this.props
    return (
      <RemoteImageBackground
        source={
          matchRoot && matchRoot.venue
            ? {
                uri: getImageUrl(matchRoot.venue.id, ItemImageType.Venue),
              }
            : fallbackImages.venue
        }
        defaultSource={fallbackImages.venue}
        style={styles.container}
      >
        <LinearGradient colors={['rgba(0,0,0,0.4)', 'rgba(0,0,0,0.6)']}>
          <SafeAreaView style={styles.safeArea}>
            <View style={styles.contentWrapper}>
              {matchRoot && (
                <View>
                  <View style={styles.mainContent}>
                    {matchRoot.teams && (
                      <TouchableOpacity
                        onPress={() =>
                          matchRoot.teams && onSelectTeam(matchRoot.teams.home.id)
                        }
                      >
                        <RemoteImage
                          style={styles.teamLogo}
                          defaultSource={fallbackImages.teamLogo}
                          source={{
                            uri: getImageUrl(
                              matchRoot.teams.home.id,
                              ItemImageType.TeamLogo
                            ),
                          }}
                        />
                      </TouchableOpacity>
                    )}
                    <View style={styles.textContainer}>
                      {matchRoot.state !== MatchState.PreMatch && (
                        <>
                          <Text style={styles.standingsText}>
                            {`${matchRoot.score.home} - ${matchRoot.score.away}`}
                          </Text>
                          {matchRoot.penaltyScore && (
                            <Text style={styles.penaltyScoreText}>
                              {`(${matchRoot.penaltyScore.home} - ${
                                matchRoot.penaltyScore.away
                              }p)`}
                            </Text>
                          )}
                        </>
                      )}
                      <MatchTime matchRoot={matchRoot} />
                    </View>
                    {matchRoot.teams && (
                      <TouchableOpacity
                        onPress={() =>
                          matchRoot.teams && onSelectTeam(matchRoot.teams.away.id)
                        }
                      >
                        <RemoteImage
                          style={styles.teamLogo}
                          defaultSource={fallbackImages.teamLogo}
                          source={{
                            uri: getImageUrl(
                              matchRoot.teams.away.id,
                              ItemImageType.TeamLogo
                            ),
                          }}
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                  {matchRoot.venue && (
                    <Text
                      style={styles.stadiumNameText}
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {matchRoot.venue.name}
                    </Text>
                  )}
                </View>
              )}
            </View>
            <View style={styles.backButtonWrapper}>
              <BackButton tintColor={colors.secondaryText} onPress={onBack} />
            </View>
          </SafeAreaView>
        </LinearGradient>
      </RemoteImageBackground>
    )
  }
}

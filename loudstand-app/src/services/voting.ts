import {
  SubmittedVote,
  FinalSubmittedVote,
  generateVoteKeyFromVote,
  UserProfileDetails,
  ISO3166v1a2Country,
  VoteType,
} from '@loudstand/shared'
import { ThunkAction } from 'redux-thunk'
// @ts-ignore missing types
import RNCarrierInfo from 'react-native-carrier-info'
import firebase from 'react-native-firebase'
import Sentry from 'react-native-sentry'
import { ReduxState } from '../store/model'
import {
  Action,
  submittedVote,
  submitVoteFailed,
  submittingVote,
} from '../store/actions'
import { ExtraAgument } from '../store/getStore'

function getBirthDate(facebookProfileDetails: UserProfileDetails | undefined) {
  if (!facebookProfileDetails) {
    return null
  }
  const { birthdate, birthyear } = facebookProfileDetails
  if (birthdate) {
    return facebookProfileDetails.birthdate
  }
  if (birthyear) {
    return `${birthyear}-07-03`
  }
  return null
}

async function getCountry(
  facebookProfileDetails: UserProfileDetails | undefined
): Promise<ISO3166v1a2Country | null> {
  if (facebookProfileDetails && facebookProfileDetails.country) {
    return facebookProfileDetails.country
  }

  try {
    const carrierCountryCode = await RNCarrierInfo.isoCountryCode()
    const countryCode = carrierCountryCode.toUpperCase()
    return countryCode
  } catch (e) {
    console.log(
      'Error loading carrier country code. Probably on a simulator or no cellular connection.'
    )
    console.log(e)
    return null
  }
}

export function submitVoteThunk(
  vote: SubmittedVote
): ThunkAction<void, ReduxState, ExtraAgument, Action> {
  return async (dispatch, getState, { getFirestore }) => {
    const state = getState()
    const voteKey = generateVoteKeyFromVote(vote as FinalSubmittedVote)
    if (
      (state.general.submittedVotes && state.general.submittedVotes[voteKey]) ||
      (state.general.submittingVotes && state.general.submittingVotes[voteKey])
    ) {
      return
    }
    try {
      dispatch(submittingVote(voteKey))
      const firestore = getFirestore()
      const { geolocation } = state.general

      const { uid: userId } = state.firebase.auth
      const { facebookProfileDetails } = state.firebase.profile

      const birthDate = getBirthDate(facebookProfileDetails)
      const country = await getCountry(facebookProfileDetails)

      const finalVote: FinalSubmittedVote = {
        ...vote,
        metadata: {
          gender:
            (facebookProfileDetails && facebookProfileDetails.gender) || null,
          birthDate,
          country,
          geoLocation: geolocation
            ? new firestore.GeoPoint(
                geolocation.latitude,
                geolocation.longitude
              )
            : null,
        },
        userId,
        created: new Date().toISOString(),
      }

      await firestore.collection('votes').add(finalVote)
      const itemId =
        vote.type === VoteType.Question ? vote.questionId : vote.matchId
      firebase.analytics().logEvent('select_content', {
        content_type: vote.type,
        item_id: itemId,
      })
      dispatch(submittedVote(voteKey))
    } catch (e) {
      Sentry.captureException(e)
      dispatch(submitVoteFailed(voteKey))
    }
  }
}

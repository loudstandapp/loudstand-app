import { GraphRequest, GraphRequestManager } from 'react-native-fbsdk'
import {
  UpdateFriendsListRequest,
  UserFriendList,
  getChangedFacebookFriends,
} from '@loudstand/shared'
import { Dispatch } from 'redux'
import { functions } from '../utils'
import { ReduxState } from '../store/model'
import * as fq from '../store/firestoreQueries'

export async function updateFriendList(
  fullRefresh: boolean,
  friendList?: UserFriendList
) {
  const updateFriendsListAction = functions.httpsCallable('updateFriendsList')
  const friendsRequest = new GraphRequest(
    '/me/friends',
    null,
    async (error?: object, result?: object) => {
      if (error) {
        console.error(error)
        return
      }
      if (!result) {
        return
      }
      // @ts-ignore
      const friends = result.data

      const ids: string[] = friends.map((friend: any) => friend.id)
      if (fullRefresh) {
        const updateRequest: UpdateFriendsListRequest = {
          fullRefresh,
          friendIds: { facebook: ids },
        }
        console.log({ updateRequest })
        const updateResult = await updateFriendsListAction(updateRequest)
        console.log({ updateResult })
        return
      }
      const { addFacebookUids, removeUids } = getChangedFacebookFriends(
        ids,
        friendList || { friends: {} }
      )
      if (!addFacebookUids.length && !removeUids.length) {
        console.log('No change in friends, not requesting update')
        return
      }

      const updateRequest: UpdateFriendsListRequest = {
        fullRefresh,
        friendIds: { facebook: ids },
      }
      console.log({ updateRequest })

      const updateResult = await updateFriendsListAction(updateRequest)
      console.log({ updateResult })
    }
  )
  new GraphRequestManager().addRequest(friendsRequest).start()
}
type GetState = () => ReduxState

export function updateFriendListThunk(fullRefresh: boolean) {
  return async (dispatch: Dispatch, getState: GetState) => {
    const state = getState()
    const { uid } = state.firebase.auth
    const {
      [fq.userFriendList.key(uid)]: userFriendList,
    } = state.firestore.data

    await updateFriendList(fullRefresh, userFriendList)
  }
}

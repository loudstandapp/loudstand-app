import { ID, UserDevice } from '@loudstand/shared'
import firebase from 'react-native-firebase'
import { Navigation } from 'react-native-navigation'
import { Alert } from 'react-native'
import Sentry from 'react-native-sentry'
import { showErrorOverlay } from '../components/ErrorOverlay'
import { ReduxState } from '../store/model'
import { ScreenNames } from '../registerScreens'
import { requestNotificationScreenNavigationProps } from '../features/user/navigation'
import { translate, currentLocale } from '../translation'
import { randomComponentId, openAppSettings } from '../utils'

const db = firebase.firestore()

export async function updateUserDevice(userId: ID, messageToken?: string) {
  try {
    const realMessageToken =
      messageToken || (await firebase.messaging().getToken())

    const device: UserDevice = {
      updated: new Date().toISOString(),
      messageToken: realMessageToken,
      isActive: true,
      activeLanguage: currentLocale.languageCode,
    }
    const deviceId = await firebase.iid().get()

    await db
      .collection('users')
      .doc(userId)
      .set({ devices: { [deviceId]: device } }, { merge: true })
  } catch (e) {
    showErrorOverlay(e.message)
    Sentry.captureException(e)
  }
}

export async function disableUserDevice(userId: ID) {
  try {
    const deviceId = await firebase.iid().get()
    await db
      .collection('users')
      .doc(userId)
      .set({ devices: { [deviceId]: { isActive: false } } }, { merge: true })
  } catch (e) {
    showErrorOverlay(e.message)
    Sentry.captureException(e)
  }
}

const REQUEST_PERMISSION_LAUNCH_COUNT_DELAYS: {
  [LaunchCount: number]: number
} = {
  1: 120,
  2: 20,
  6: 20,
  12: 20,
  24: 20,
  48: 20,
  96: 20,
  192: 20,
}

export function notificationPermissionThunk() {
  const componentId = randomComponentId(
    ScreenNames.RequestNotificationPermissionScreen
  )

  const onSkip = () => Navigation.dismissModal(componentId)

  const showSettingsAlert = () =>
    Alert.alert(
      translate('requestNotificationPermission.permissionDeniedDialogTitle'),
      translate('requestNotificationPermission.permissionDeniedDialogMessage'),
      [
        {
          text: translate('requestNotificationPermission.openSettings'),
          onPress: () => {
            openAppSettings()
            onSkip()
          },
          style: 'default',
        },
        {
          text: translate('requestNotificationPermission.skipButtonText'),
          onPress: onSkip,
          style: 'cancel',
        },
      ]
    )

  async function onAccept() {
    try {
      await firebase.messaging().requestPermission()
      const hasPermission = await firebase.messaging().hasPermission()
      if (!hasPermission) {
        console.warn('permission not granted')
        showSettingsAlert()
      }
      await Navigation.dismissModal(componentId)
    } catch (e) {
      console.warn('permission not granted', e)
      showSettingsAlert()
    }
  }

  const showPermissionRequestScreen = () => {
    return Navigation.showModal({
      component: {
        id: componentId,
        name: ScreenNames.RequestNotificationPermissionScreen,
        passProps: requestNotificationScreenNavigationProps(onAccept, onSkip),
      },
    })
  }

  return async (dispatch: any, getState: () => ReduxState) => {
    const hasPermission = await firebase.messaging().hasPermission()
    if (hasPermission) {
      return
    }
    const state = getState()
    const { launchCount } = state.general
    // This should not happen...
    if (!launchCount) {
      return
    }
    const delay = REQUEST_PERMISSION_LAUNCH_COUNT_DELAYS[launchCount]
    if (delay === undefined) {
      return
    }
    setTimeout(showPermissionRequestScreen, delay * 1000)
  }
}

/* global __DEV__ */

import {
  ImportTriggerReason,
  ISO8601Timestamp,
  QuestionResultsSummaryDocument,
  ISO8601Date,
  Question,
  MatchEventResultsSummaryDocument,
  MatchTimeline,
  MatchRoot,
  ImportTriggerRequest,
  ImportTriggerResponse,
  ID,
  PlayerList,
} from '@loudstand/shared'
import { AnyAction, MiddlewareAPI } from 'redux'
import Sentry from 'react-native-sentry'
import { functions } from '../utils'
import * as selectors from '../store/selectors'
import {
  setAvailableTeams,
  setTournament,
  beginGenerating,
  endGenerating,
} from '../store/actions'
import * as fq from '../store/firestoreQueries'
import { ReduxState } from '../store/model'
import { showErrorOverlay } from '../components/ErrorOverlay'

const importTrigger = functions.httpsCallable('triggerImport')

async function generateDocument(
  request: ImportTriggerRequest,
  store: MiddlewareAPI
) {
  try {
    console.log('GENERATING', request)
    store.dispatch(beginGenerating(request.path))
    const response = await importTrigger(request)
    const result: ImportTriggerResponse = response.data
    store.dispatch(endGenerating(request.path))
    if (!result) {
      console.log('GENERATE-RESULT', result)
      const message = `Error generating ${
        request.path
      } at ${new Date().toISOString()}`
      throw new Error(message)
    }
    if (result.errors && result.errors.length > 0) {
      console.log('GENERATE-RESULT', result)
      const message = `Error generating ${
        request.path
      } at ${new Date().toISOString()}:
      ${result.errors.join('\n')}`
      throw new Error(message)
    }
    console.log('GENERATED', request, result)
  } catch (error) {
    store.dispatch(endGenerating(request.path))
    if (__DEV__) {
      showErrorOverlay(error.message)
    }
    Sentry.captureException(error, {
      tags: {
        function: 'importTrigger',
        path: request.path,
      },
    })
  }
}

async function handleAllTournamentsReceived(
  action: AnyAction,
  store: MiddlewareAPI
) {
  const { storeAs } = action.meta
  const { ordered } = action.payload

  if (storeAs === fq.allTournaments.key()) {
    const state: ReduxState = store.getState()
    // Set the active tournament if none is currently defined
    const { activeTournament } = state.general
    if (activeTournament) {
      return
    }
    if (!Array.isArray(ordered) || ordered.length === 0) {
      return
    }
    const tournaments = selectors.tournamentListBodies(ordered)
    const defaultTournaments = tournaments.filter(
      tournament => tournament.isDefaultForTeams
    )
    const [newTournament] = defaultTournaments
    store.dispatch(setTournament(newTournament))
  }
}

async function handleUpdateChosenTeams(
  action: AnyAction,
  store: MiddlewareAPI
) {
  const state = store.getState()
  const { uid } = state.firebase.auth

  if (
    ![
      fq.allTournaments.key(),
      fq.favoriteTeams.key(uid),
      fq.followingTeams.key(uid),
    ].includes(action.meta.storeAs)
  ) {
    return
  }
  const defaultTournaments = selectors.defaultTournamentsList(state)
  if (defaultTournaments.length === 0) {
    console.log('no default tournaments yet', action)
    return
  }
  const favoriteTeams = selectors.ownFavoriteTeams(state)
  if (!favoriteTeams) {
    console.log('no favorites yet', action)
    return
  }
  const followingTeams = selectors.followingTeams(state)
  if (!followingTeams) {
    console.log('no followingTeams yet', action)
    return
  }
  const chosenTeams = selectors.getChosenTeams(
    defaultTournaments,
    favoriteTeams,
    followingTeams
  )
  store.dispatch(setAvailableTeams(chosenTeams))
}

function isExpired(expiration: ISO8601Timestamp) {
  const expirationDate = new Date(expiration)
  return expirationDate < new Date()
}

const blacklist = [
  /^users\/.*/,
  /^players\/[^\/]+\/managerPopularity\/[^\/]+$/,
  /^players\/[^\/]+\/mostValuableShare\/[^\/]+$/,
  /^players\/[^\/]+\/lineupShare\/[^\/]+$/,
  /^players\/[^\/]+\/popularity\/[^\/]+$/,
  /^players\/[^\/]+\/substitutionApproval\/[^\/]+$/,
  /^seasons\/[^\/]+\/statistics\/teamPopularity/,
  /^teams\/[^\/]+\/attributes\/questions$/,
  /^teams\/[^\/]+\/statistics\/questionResults+$/,
  /^players\/[^\/]+\/attributes\/questions$/,
  /^players\/[^\/]+\/statistics\/questionResults+$/,
  /^teams\/[^\/]+\/attributes\/homeScreenItemCalendar$/,
  /^teams\/[^\/]+\/attributes\/homeScreenMemoryCalendar$/,
  /^matches\/([^\/]+)\/matchEventResults\/([^\/]+)$/,
  /^teams\/([^\/]+)\/statistics\/suggestedLineup$/,
  /^teams\/([^\/]+)\/statistics\/mostValuablePlayers$/,
  /^matches\/([^\/]+)\/mostValuablePlayers\/([^\/]+)$/,
  /^matches\/([^\/]+)\/suggestedLineup\/([^\/]+)$/,
  /^followingTeams$/,
]

async function handleExpiredOrEmpty(action: AnyAction, store: MiddlewareAPI) {
  const { ordered } = action.payload
  const { storeAs } = action.meta
  // eslint-disable-next-line no-restricted-syntax
  for (const regex of blacklist) {
    if (storeAs.match(regex)) {
      return
    }
  }
  if (storeAs === fq.allTournaments.key()) {
    await (ordered as any[]).forEach(async item => {
      if (item.expiration && isExpired(item.expiration)) {
        console.log('tournament EXPIRED PAYLOAD', action)
        await generateDocument(
          {
            path: `${fq.allTournaments.key()}/${item.id}`,
            reason: ImportTriggerReason.Expired,
          },
          store
        )
      }
    })
    return
  }
  if (
    !ordered ||
    ordered.length === 0 ||
    Object.keys(ordered[0]).length === 1 // only ID
  ) {
    console.log('EMPTY PAYLOAD', action)

    await generateDocument(
      {
        path: action.meta.storeAs,
        reason: ImportTriggerReason.NotFound,
      },
      store
    )
  } else if (ordered[0].expiration && isExpired(ordered[0].expiration)) {
    // FIXME GO THROUGH ALL. AND TRIGGER PROPERLY. Convention about `$storeAs/$key`...
    console.log('EXPIRED PAYLOAD', action)
    await generateDocument(
      {
        path: action.meta.storeAs,
        reason: ImportTriggerReason.Expired,
      },
      store
    )
  }
}

const isQuestionsList = (action: AnyAction) =>
  action.meta &&
  action.meta.subcollections &&
  action.meta.subcollections[0] &&
  action.meta.subcollections[0].collection === 'attributes' &&
  action.meta.subcollections[0].doc === 'questions'

const isQuestionsResult = (action: AnyAction) =>
  action.meta &&
  action.meta.subcollections &&
  action.meta.subcollections[0] &&
  action.meta.subcollections[0].collection === 'statistics' &&
  action.meta.subcollections[0].doc === 'questionResults'

const questionsResultPath = (action: AnyAction) =>
  `${action.meta.collection}/${action.meta.doc}/statistics/questionResults`

const questionsListPath = (action: AnyAction) =>
  `${action.meta.collection}/${action.meta.doc}/attributes/questions`

const isMatchEventResults = (action: AnyAction) =>
  action.meta &&
  action.meta.collection === 'matches' &&
  action.meta.subcollections &&
  action.meta.subcollections[0] &&
  action.meta.subcollections[0].collection === 'matchEventResults'

const isMatchMostValuablePlayersForTeam = (action: AnyAction) =>
  action.meta &&
  action.meta.collection === 'matches' &&
  action.meta.subcollections &&
  action.meta.subcollections[0] &&
  action.meta.subcollections[0].collection === 'mostValuablePlayers'

const isMatchSuggestedLineupForTeam = (action: AnyAction) =>
  action.meta &&
  action.meta.collection === 'matches' &&
  action.meta.subcollections &&
  action.meta.subcollections[0] &&
  action.meta.subcollections[0].collection === 'suggestedLineup'

const isMatchRoot = (action: AnyAction) =>
  action.meta &&
  action.meta.collection === 'matches' &&
  !action.meta.subcollections

const isMatchActivePlayers = (action: AnyAction) =>
  action.meta &&
  action.meta.collection === 'matches' &&
  action.meta.subcollections &&
  action.meta.subcollections[0].collection === 'attributes' &&
  action.meta.subcollections[0].doc === 'activePlayers'

const isTeamMostValuablePlayers = (action: AnyAction) =>
  action.meta &&
  action.meta.collection === 'teams' &&
  action.meta.subcollections &&
  action.meta.subcollections[0] &&
  action.meta.subcollections[0].collection === 'statistics' &&
  action.meta.subcollections[0].doc === 'mostValuablePlayers'

const isTeamSuggestedLineup = (action: AnyAction) =>
  action.meta &&
  action.meta.collection === 'teams' &&
  action.meta.subcollections &&
  action.meta.subcollections[0] &&
  action.meta.subcollections[0].collection === 'statistics' &&
  action.meta.subcollections[0].doc === 'suggestedLineup'

const isTeamPlayers = (action: AnyAction) =>
  action.meta &&
  action.meta.collection === 'teams' &&
  action.meta.subcollections &&
  action.meta.subcollections[0] &&
  action.meta.subcollections[0].collection === 'attributes' &&
  action.meta.subcollections[0].doc === 'players'

const isMatchTimeline = (action: AnyAction) =>
  action.meta &&
  action.meta.subcollections &&
  action.meta.collection === 'matches' &&
  action.meta.subcollections[0] &&
  action.meta.subcollections[0].collection === 'attributes' &&
  action.meta.subcollections[0].doc === 'timeline'

const getMatchId = (action: AnyAction): ID =>
  action.meta && action.meta.collection === 'matches' && action.meta.doc

const getTeamId = (action: AnyAction): ID =>
  action.meta &&
  action.meta.subcollections && // ?
  action.meta.collection === 'teams' &&
  action.meta.doc

// Maybe use  state.firestore.status.requested {} instad
const hasDocumentBeenLoaded = (state: ReduxState, path: string): boolean =>
  !!state.firestore.ordered[path]

const isDocumentBodyEmpty = (state: ReduxState, path: string): boolean =>
  !state.firestore.ordered[path] ||
  state.firestore.ordered[path].length === 0 ||
  !state.firestore.ordered[path][0].body

const isDocumentEmpty = (state: ReduxState, path: string): boolean =>
  !state.firestore.ordered[path] || state.firestore.ordered[path].length === 0

const getDocumentBody = <T>(state: ReduxState, path: string): T | null =>
  (state.firestore.data[path] && state.firestore.data[path].body) || null

const getDocument = <T>(state: ReduxState, path: string): T | null =>
  state.firestore.data[path] || null

async function generateDocumentIfLoadedAndNotFound(
  path: string,
  store: MiddlewareAPI
) {
  const state = store.getState()
  if (hasDocumentBeenLoaded(state, path) && isDocumentEmpty(state, path)) {
    await generateDocument(
      {
        reason: ImportTriggerReason.NotFound,
        path,
      },
      store
    )
  }
}

async function handleMissingQuestionResults(
  action: AnyAction,
  store: MiddlewareAPI
) {
  if (!isQuestionsList(action) && !isQuestionsResult(action)) {
    return
  }
  const state = store.getState()

  const questionsPath = questionsListPath(action)
  const questions = getDocument<{ [key in ISO8601Date]: Question }>(
    state,
    questionsPath
  )
  if (!questions) {
    return
  }

  const resultPath = questionsResultPath(action)
  if (!hasDocumentBeenLoaded(state, resultPath)) {
    return
  }

  const questionIds = Object.keys(questions)
    // Remove invalid elements
    .filter(date => questions[date].id)
    .map(date => questions[date].id)

  const results = getDocument<QuestionResultsSummaryDocument>(state, resultPath)
  await questionIds
    .filter(questionId => !results || !(questionId in results))
    .forEach(async questionId => {
      console.log('missing question results', questionId, results)
      await generateDocument(
        {
          path: `questions/${questionId}/statistics/result`,
          reason: ImportTriggerReason.NotFound,
        },
        store
      )
    })
}

async function handleMissingMatchEventResults(
  action: AnyAction,
  store: MiddlewareAPI
) {
  if (
    !isMatchEventResults(action) &&
    !isMatchTimeline(action) &&
    !isMatchRoot(action)
  ) {
    return
  }
  const state: ReduxState = store.getState()
  const matchId = getMatchId(action)

  const matchRootPath = fq.matchRoot.key(matchId)
  const matchRoot = getDocumentBody<MatchRoot>(state, matchRootPath)
  if (!matchRoot || !matchRoot.teams) {
    console.log('missing matchRoot with teams', matchId)
    return
  }

  const { teams } = matchRoot

  const timelinePath = fq.matchTimeline.key(matchId)
  const timeline = getDocumentBody<MatchTimeline>(state, timelinePath)
  if (!timeline) {
    console.log('missing timeline', matchId)
    return
  }

  Object.keys(teams).forEach(async qualifier => {
    if (qualifier !== 'home' && qualifier !== 'away') {
      console.log('invalid qualifier', qualifier)
      return
    }
    const teamId = teams[qualifier].id
    const matchEventResultsPath = fq.matchEventVoteResults.key({
      matchId,
      teamId,
    })
    if (!hasDocumentBeenLoaded(state, matchEventResultsPath)) {
      console.log('matchEventResults not loaded yet', matchId, teamId)
      return
    }
    const matchEventResults = getDocument<MatchEventResultsSummaryDocument>(
      state,
      matchEventResultsPath
    )

    const currentTeamEvents = Object.values(timeline.events).filter(
      event => event.team === qualifier
    )
    const eventsWithoutResponse = currentTeamEvents.find(
      event => !matchEventResults || !(event.id in matchEventResults)
    )
    if (eventsWithoutResponse) {
      console.log('missing match event results', timeline, matchEventResults)
      await generateDocument(
        {
          path: fq.matchEventVoteResults.key({
            matchId,
            teamId,
          }),
          reason: ImportTriggerReason.Incomplete,
        },
        store
      )
    }
  })
}

async function handleMissingTeamMostValuablePlayers(
  action: AnyAction,
  store: MiddlewareAPI
) {
  if (!isTeamMostValuablePlayers(action) && !isTeamPlayers(action)) {
    return
  }
  const state: ReduxState = store.getState()
  const teamId = getTeamId(action)
  if (!teamId) {
    return
  }
  const teamPlayersPath = fq.teamPlayers.key(teamId)
  const teamPlayers = getDocumentBody<PlayerList>(state, teamPlayersPath)
  if (!teamPlayers) {
    return
  }
  const path = fq.teamMostValuablePlayers.key(teamId)
  await generateDocumentIfLoadedAndNotFound(path, store)
}

async function handleMissingTeamSuggestedLineup(
  action: AnyAction,
  store: MiddlewareAPI
) {
  if (!isTeamSuggestedLineup(action) && !isTeamPlayers(action)) {
    return
  }
  const state: ReduxState = store.getState()
  const teamId = getTeamId(action)
  if (!teamId) {
    return
  }
  const teamPlayersPath = fq.teamPlayers.key(teamId)
  const teamPlayers = getDocumentBody<PlayerList>(state, teamPlayersPath)
  if (!teamPlayers) {
    return
  }
  const path = fq.teamSuggestedLineup.key(teamId)
  await generateDocumentIfLoadedAndNotFound(path, store)
}

async function handleMissingMatchMostValuablePlayersForTeam(
  action: AnyAction,
  store: MiddlewareAPI
) {
  if (
    !isMatchMostValuablePlayersForTeam(action) &&
    !isMatchActivePlayers(action) &&
    !isMatchRoot(action)
  ) {
    return
  }
  const matchId = getMatchId(action)
  const matchRootPath = fq.matchRoot.key(matchId)
  const matchActivePlayersPath = fq.matchActivePlayers.key(matchId)

  const state = store.getState()

  if (isDocumentBodyEmpty(state, matchActivePlayersPath)) {
    console.log('No match active players', matchId)
    return
  }
  console.log(
    'matchActivePlayersPath',
    state.firestore.data[matchActivePlayersPath]
  )

  const matchRoot = getDocumentBody<MatchRoot>(state, matchRootPath)
  if (!matchRoot || !matchRoot.teams) {
    console.log('No matchRoot with teams', matchId)
    return
  }

  Object.values(matchRoot.teams).forEach(async team => {
    const path = fq.mostValuablePlayersResult.key({
      matchId,
      teamId: team.id,
    })
    await generateDocumentIfLoadedAndNotFound(path, store)
  })
}

async function handleMissingMatchSuggestedLineupForTeam(
  action: AnyAction,
  store: MiddlewareAPI
) {
  if (
    !isMatchSuggestedLineupForTeam(action) &&
    !isTeamPlayers(action) &&
    !isMatchRoot(action)
  ) {
    return
  }
  const matchId = getMatchId(action)
  const matchRootPath = fq.matchRoot.key(matchId)

  const state = store.getState()

  const matchRoot = getDocumentBody<MatchRoot>(state, matchRootPath)
  if (!matchRoot || !matchRoot.teams) {
    console.log('No matchRoot with teams', matchId)
    return
  }

  Object.values(matchRoot.teams).forEach(async team => {
    const teamPlayersPath = fq.teamPlayers.key(team.id)
    if (isDocumentBodyEmpty(state, teamPlayersPath)) {
      console.log('No team players', team.id)
      return
    }
    const path = fq.matchLineupSuggestionResult.key({
      matchId,
      teamId: team.id,
    })
    await generateDocumentIfLoadedAndNotFound(path, store)
  })
}

export async function handleListenerResponse(
  action: AnyAction,
  store: MiddlewareAPI
) {
  handleMissingQuestionResults(action, store)
  handleMissingMatchEventResults(action, store)
  handleMissingMatchMostValuablePlayersForTeam(action, store)
  handleMissingMatchSuggestedLineupForTeam(action, store)
  handleExpiredOrEmpty(action, store)
  handleUpdateChosenTeams(action, store)
  handleAllTournamentsReceived(action, store)
  handleMissingTeamMostValuablePlayers(action, store)
  handleMissingTeamSuggestedLineup(action, store)
}

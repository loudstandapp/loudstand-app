import { MiddlewareAPI, Action, Dispatch, Store } from 'redux'
import { actionTypes } from 'redux-firestore'
import { ReduxState } from '../store/model'
import { handleListenerResponse } from './handleListenerResponse'

function handleAction(action: Action, store: MiddlewareAPI) {
  switch (action.type) {
    case actionTypes.LISTENER_RESPONSE: {
      handleListenerResponse(action, store)
      break
    }
    default:
      break
  }
}

export const observeActions = (store: MiddlewareAPI) => (next: Dispatch) => (
  action: Action
) => {
  const result = next(action)
  handleAction(action, store)
  return result
}

import { Dispatch } from 'redux'
import { Navigation } from 'react-native-navigation'
import { ThunkAction } from 'redux-thunk'
import { ReduxState } from '../store/model'
import { ScreenNames } from '../registerScreens'

type GetState = () => ReduxState
interface FlowChallengeResponse {
  promise: Promise<any>
  componentId: string
}
export type FlowChallenge = (
  latestComponentId: string | undefined,
  dispatch: Dispatch,
  getState: GetState,
  extra: { getFirebase: any; getFirestore: any }
) => FlowChallengeResponse
export type FlowValidator = (
  dispatch: Dispatch,
  getState: GetState,
  extra: { getFirebase: any; getFirestore: any }
) => Promise<boolean>

export interface FlowPrerequisite {
  challenge: FlowChallenge
  validator: FlowValidator
}

export function conditionalFlowThunk(
  prerequisites: FlowPrerequisite[],
  execute: () => ThunkAction<any, any, any, any>,
  originComponentId?: string
) {
  return async (
    dispatch: Dispatch,
    getState: GetState,
    { getFirebase, getFirestore }: any
  ) => {
    let latestComponentId = originComponentId
    try {
      for (const prerequisite of prerequisites) {
        const isValid = await prerequisite.validator(dispatch, getState, {
          getFirebase,
          getFirestore,
        })
        if (!isValid) {
          const { promise, componentId } = prerequisite.challenge(
            latestComponentId,
            dispatch,
            getState,
            {
              getFirebase,
              getFirestore,
            }
          )
          latestComponentId = componentId
          await promise
        }
      }

      // @ts-ignore fix thunk typings
      dispatch(execute())
    } catch (e) {
      // eslint-disable-next-line no-console
      console.warn('Conditional flow was aborted by rejected challenge', e)
    }
  }
}

export async function showChallengeScreen({
  screenName,
  passProps,
  canGoBack,
  latestComponentId,
}: {
  screenName: ScreenNames
  passProps: object
  canGoBack: boolean
  latestComponentId?: string
}) {
  const component = {
    id: screenName,
    name: screenName,
    passProps,
  }
  if (latestComponentId) {
    await Navigation.push(latestComponentId, {
      component: {
        ...component,
        options: {
          popGesture: canGoBack,
        },
      },
    })
  } else {
    await Navigation.setRoot({
      root: {
        stack: {
          children: [
            {
              component,
            },
          ],
        },
      },
    })
  }
}

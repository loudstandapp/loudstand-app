import { Dimensions } from 'react-native'

const { width } = Dimensions.get('screen')
const smallDeviceThreshold = 360

export const isSmallDevice = width < smallDeviceThreshold

export const customNavBarHeight = 48
export const touchableSize = 48
export const spacingUnit = 8
export const marginDefault = spacingUnit * 2
export const headerHeight = 4 * spacingUnit
export const buttonHeight = 4 * spacingUnit

export const mainImageMaxHeight = width * 0.8
export const mainImageMinHeight = 200

import Color from 'color'

export default {
  primary: '#12a538',
  primaryAlt: '#007434',
  secondary: '#242b36',
  // secondaryLighter: '#9AA2A0',
  tertiary: '#999b9d',
  tertiaryAlt: Color('#ffffff')
    .mix(Color('#242b36'), 0.4)
    .toString(),
  tertiaryLight: '#c5cbd0',

  primaryLight: Color('#ffffff')
    .mix(Color('#12a538'), 0.6)
    .toString(),
  secondaryLight: Color('#ffffff')
    .mix(Color('#242b36'), 0.4)
    .toString(),

  secondary10pct: Color('#242b36')
    .alpha(0.1)
    .toString(),

  secondary25pct: Color('#242b36')
    .alpha(0.25)
    .toString(),

  tertiary30pct: Color('#ffffff')
    .mix(Color('#242b36'), 0.2)
    .alpha(0.3)
    .toString(),

  primaryText: '#242b36',
  secondaryText: '#FFFFFF',
  secondaryTextInactive: '#CCCCCC',
  background: '#ffffff',
  backgroundAlt: '#c6d9e3',
  white: '#ffffff',
  navigationBackground: '#232b37',
  fixmeSecondaryBackground: '#141920',
  valid: '#00a23a',
  invalid: '#e02857',
  live: '#ff0000',

  valid40pct: Color('#00a23a')
    .alpha(0.4)
    .toString(),

  invalid40pct: Color('#e02857')
    .alpha(0.4)
    .toString(),

  // This one is made to ensure smooth gradients
  navigationBackground0pct: Color('#232b37')
    .alpha(0)
    .toString(),
  playerHeadshotBorder: Color('white')
    .alpha(0.4)
    .toString(),
}

import { StyleProp, TextStyle } from 'react-native'
import colors from './colors'

type FontType =
  | 'Navigation'
  | 'BodySmall'
  | 'BodySmallBold'
  | 'Body'
  | 'BodyStrong'
  | 'BodyBold'
  | 'HeaderSmall'
  | 'HeaderSmallBold'
  | 'HeaderSmallLight'
  | 'BodyLarge'
  | 'Header'
  | 'TopBarTitle'
  | 'SuperLarge'
  | 'CustomFullDate'
  | 'CustomPercentageCircleSmall'
  | 'CustomPercentageCircleLarge'
  | 'CustomRating'
  | 'CustomDaySelectorDay'
  | 'CustomDaySelectorWeekday'
  | 'CustomMatchEventTimeLight'
  | 'CustomMatchEventTime'
  | 'CustomQuestionBody'
  | 'CustomQuestionTopic'
  | 'CustomPlayerRanking'
  | 'CustomMatchScore'
  | 'CustomHomeScreenTeam'
  | 'CustomPhoneNumberInput'

const NAVIGATION_SIZE = 11 // 6
const BODY_SMALL_SIZE = 12 // 8
const BODY_SIZE = 14 // 9
const HEADER_SMALL_SIZE = 15 // 10
const BODY_LARGE_SIZE = 16 // 12
const HEADER_SIZE = 16 // 12
const TITLE_SIZE = 20 // 16
const SUPER_LARGE_SIZE = 33 // 22

export const CAPTION_SIZE = 7
export const GIANT_SIZE = 27

const fonts: { [key in FontType]: StyleProp<TextStyle> } = {
  Navigation: {
    fontSize: NAVIGATION_SIZE,
    fontFamily: 'Raleway-Medium',
    color: colors.primaryText,
  },
  BodySmall: {
    fontSize: BODY_SMALL_SIZE,
    fontFamily: 'Raleway-Medium',
    color: colors.primaryText,
  },
  BodySmallBold: {
    fontSize: BODY_SMALL_SIZE,
    fontFamily: 'Raleway-Bold',
    color: colors.primaryText,
  },
  Body: {
    fontSize: BODY_SIZE,
    fontFamily: 'Raleway-Light',
    color: colors.primaryText,
  },
  BodyStrong: {
    fontSize: BODY_SIZE,
    fontFamily: 'Raleway-Medium',
    color: colors.primaryText,
  },
  BodyBold: {
    fontSize: BODY_SIZE,
    fontFamily: 'Raleway-Bold',
    color: colors.primaryText,
  },
  HeaderSmall: {
    fontSize: HEADER_SMALL_SIZE,
    fontFamily: 'Raleway-Medium',
    color: colors.primaryText,
  },
  HeaderSmallBold: {
    fontSize: HEADER_SMALL_SIZE,
    fontFamily: 'Raleway-Bold',
    color: colors.primaryText,
  },
  HeaderSmallLight: {
    fontSize: HEADER_SMALL_SIZE,
    fontFamily: 'Raleway-Light',
    color: colors.primaryText,
  },
  BodyLarge: {
    fontSize: BODY_LARGE_SIZE,
    fontFamily: 'Raleway-Medium',
    color: colors.primaryText,
  },
  Header: {
    fontSize: HEADER_SIZE,
    fontFamily: 'Raleway-Bold',
    color: colors.primaryText,
  },
  TopBarTitle: {
    fontSize: TITLE_SIZE,
    fontFamily: 'Raleway-Bold',
    color: colors.primaryText,
  },
  SuperLarge: {
    fontSize: SUPER_LARGE_SIZE,
    fontFamily: 'Raleway-Medium',
    color: colors.primaryText,
  },
  CustomFullDate: {
    fontSize: 12, // 7,
    fontFamily: 'Raleway-Light',
    color: colors.secondaryText,
  },
  CustomPercentageCircleSmall: {
    fontSize: 10, // 7 ,
    fontFamily: 'Raleway-Light',
    color: colors.secondaryText,
  },
  CustomPercentageCircleLarge: {
    fontSize: 13, // 13,
    fontFamily: 'Raleway-Light',
    color: colors.secondaryText,
  },
  CustomRating: {
    fontSize: 20, // 13,
    fontFamily: 'Raleway-Medium',
    color: colors.secondaryText,
  },
  CustomDaySelectorDay: {
    fontSize: 15, // 10,
    fontFamily: 'Raleway-Medium',
    color: colors.secondaryText,
  },
  CustomDaySelectorWeekday: {
    fontSize: 15, // 10,
    fontFamily: 'Raleway-Bold',
    color: colors.secondaryText,
  },
  CustomMatchEventTimeLight: {
    fontSize: 16, // 11,
    fontFamily: 'Raleway-Bold',
    color: colors.primaryText,
  },
  CustomMatchEventTime: {
    fontSize: 16, // 15,
    fontFamily: 'Raleway-Bold',
    color: colors.primaryText,
  },
  CustomQuestionBody: {
    fontSize: 18, // 15,
    fontFamily: 'Raleway-Light',
    color: colors.secondaryText,
  },
  CustomQuestionTopic: {
    fontSize: 24, // 20,
    fontFamily: 'Raleway-ExtraBold',
    color: colors.secondaryText,
  },
  CustomPlayerRanking: {
    fontSize: 18, // 17,
    fontFamily: 'Raleway-Bold',
    color: colors.secondaryText,
  },
  CustomMatchScore: {
    fontSize: 40, // 27,
    fontFamily: 'Raleway-Bold',
    color: colors.secondaryText,
  },
  CustomHomeScreenTeam: {
    fontSize: 24, // 18,
    fontFamily: 'Raleway-Bold',
    color: colors.secondaryText,
  },
  CustomPhoneNumberInput: {
    fontSize: 24, // 18,
    fontFamily: 'Raleway-Medium',
  },
}
export default fonts

import React from 'react'
import { translate } from '../translation'
import GuideOverlay from './GuideOverlay'
import { DismissedGuides } from '../store/model'

export enum GuideType {
  Home = 'HOME',
  Statistics = 'STATISTICS',
  Community = 'COMMUNITY',
  Match = 'MATCH',
}

interface Props {
  type: GuideType
  dismissedGuides: DismissedGuides
  launchCount: number
  onDismiss: (type: GuideType) => void
}

function getContent() {
  const guideContent: {
    [key in GuideType]: { body: string; title: string }
  } = {
    [GuideType.Community]: {
      body: translate('guides.community.body'),
      title: translate('guides.community.title'),
    },
    [GuideType.Statistics]: {
      body: translate('guides.statistics.body'),
      title: translate('guides.statistics.title'),
    },
    [GuideType.Home]: {
      body: translate('guides.home.body'),
      title: translate('guides.home.title'),
    },
    [GuideType.Match]: {
      body: translate('guides.match.body'),
      title: translate('guides.match.title'),
    },
  }
  return guideContent
}

const minimumLaunchCountForGuide: { [key in GuideType]: number } = {
  [GuideType.Home]: 2,
  [GuideType.Community]: 2,
  [GuideType.Match]: 2,
  [GuideType.Statistics]: 2,
}

export default class Guide extends React.PureComponent<Props> {
  handleDismiss = () => {
    const { onDismiss, type } = this.props
    onDismiss(type)
  }

  render = () => {
    const { type, dismissedGuides, launchCount } = this.props
    if (dismissedGuides[type]) {
      return null
    }
    if (launchCount < minimumLaunchCountForGuide[type]) {
      return null
    }
    const guideContent = getContent()
    const { body, title } = guideContent[type]
    return (
      <GuideOverlay
        mode="tip"
        body={body}
        title={title}
        onDismiss={this.handleDismiss}
      />
    )
  }
}

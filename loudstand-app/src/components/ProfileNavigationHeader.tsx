import React from 'react'
import {
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  ImageRequireSource,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { TeamListItem, LocalizedTeams } from '@loudstand/shared'
import { FastImageSource } from 'react-native-fast-image'
import { spacingUnit, touchableSize } from '../theme/layout'
import colors from '../theme/colors'
import fonts from '../theme/fonts'
import {
  getImageUrl,
  ItemImageType,
  localizeTeam,
  fallbackImages,
} from '../utils'
import RemoteImage from './RemoteImage'
import RemoteImageBackground from './RemoteImageBackground'
import BackButton from './BackButton'

const TEAM_LOGO_SIZE = 40

interface Props {
  onBack: () => void
  title: string
  subTitle?: string
  backgroundSource: FastImageSource
  defaultBackgroundSource: ImageRequireSource
  onSelectSettings?: () => void
  team?: TeamListItem
  localizedTeams?: LocalizedTeams
  onSelectTeam?: () => void
  backgroundHeight: number | string
}

const styles = StyleSheet.create({
  backgroundImage: {},
  safeArea: {
    flex: 1,
  },
  container: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    flex: 1,
  },
  gradientOverlay: {
    flex: 1,
  },
  leftButtonContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    flex: 1,
    justifyContent: 'center',
  },
  rightButtonContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    flex: 1,
    justifyContent: 'center',
  },
  backButton: {},
  settingsButton: {
    width: touchableSize,
    height: touchableSize,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: spacingUnit,
  },
  teamSelectorButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 2 * spacingUnit,
  },
  teamName: {
    ...(fonts.Header as object),
    color: colors.secondaryText,
    marginHorizontal: spacingUnit,
  },
  teamLogo: {
    width: TEAM_LOGO_SIZE,
    height: TEAM_LOGO_SIZE,
  },
  selectTeamImage: {
    tintColor: colors.secondaryText,
  },
  buttonImage: {
    tintColor: colors.secondaryText,
  },
  title: {
    ...(fonts.TopBarTitle as object),
    color: colors.secondaryText,
  },
  subTitle: {
    ...(fonts.BodyStrong as object),
    marginTop: spacingUnit / 2,
    color: colors.secondaryText,
  },
  textContainer: {
    alignItems: 'center',
    width: '100%',
    paddingBottom: 2 * spacingUnit,
  },
})

export default class ProfileNavigationHeader extends React.PureComponent<
  Props
> {
  render = () => {
    const {
      onBack,
      title,
      subTitle,
      backgroundSource,
      defaultBackgroundSource,
      onSelectSettings,
      onSelectTeam,
      team,
      localizedTeams,
      backgroundHeight,
    } = this.props
    return (
      <RemoteImageBackground
        source={backgroundSource}
        defaultSource={defaultBackgroundSource}
        style={[styles.backgroundImage, { height: backgroundHeight }]}
      >
        <LinearGradient
          colors={['rgba(0,0,0,0.5)', 'rgba(0,0,0,0.7)']}
          style={styles.gradientOverlay}
        >
          <SafeAreaView style={styles.safeArea}>
            <View style={styles.container}>
              {team && (
                <TouchableOpacity
                  onPress={onSelectTeam}
                  style={styles.teamSelectorButton}
                  disabled={!onSelectTeam}
                >
                  <RemoteImage
                    source={{
                      uri: getImageUrl(team.id, ItemImageType.TeamLogo),
                    }}
                    defaultSource={fallbackImages.teamLogo}
                    style={styles.teamLogo}
                  />
                  <Text style={styles.teamName}>
                    {localizeTeam(team, localizedTeams)}
                  </Text>
                  {onSelectTeam && (
                    <Image
                      source={require('../../assets/icons/buttons/down-arrow2.png')}
                      style={styles.selectTeamImage}
                    />
                  )}
                </TouchableOpacity>
              )}
              <View style={styles.textContainer}>
                <Text style={styles.title}>{title}</Text>
                {subTitle && <Text style={styles.subTitle}>{subTitle}</Text>}
              </View>
              <View style={styles.leftButtonContainer}>
                <BackButton tintColor={colors.secondaryText} onPress={onBack} />
              </View>
              <View style={styles.rightButtonContainer}>
                {onSelectSettings && (
                  <TouchableOpacity
                    style={styles.settingsButton}
                    onPress={onSelectSettings}
                  >
                    <Image
                      style={styles.buttonImage}
                      source={require('../../assets/icons/navigation/settings.png')}
                    />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </SafeAreaView>
        </LinearGradient>
      </RemoteImageBackground>
    )
  }
}

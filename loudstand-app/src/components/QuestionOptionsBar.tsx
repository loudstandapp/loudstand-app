import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  Dimensions,
} from 'react-native'
import { QuestionResult, QuestionResponseType } from '@loudstand/shared'
import { formatPercentage } from '../utils'
import { spacingUnit } from '../theme/layout'
import fonts from '../theme/fonts'
import colors from '../theme/colors'
import { translate } from '../translation'

interface Props {
  allowsVoting: boolean
  result?: QuestionResult
  chosen?: QuestionResponseType
  onYes: () => void
  onNo: () => void
  hasBeenSubmitted?: boolean
  hasBeenAnswered?: boolean
}

const { width: deviceWidth } = Dimensions.get('screen')

const RESULT_MARGIN = spacingUnit / 2
const MIN_WIDTH = 50

// Adds some fake votes on the option that the user chose, if the current result is 0.0
function fakeAdjustResultIfneeded(
  result: QuestionResult,
  chosen?: QuestionResponseType
): QuestionResult {
  if (!chosen) {
    return result
  }
  if (result[chosen] > 0) {
    return result
  }
  return chosen === QuestionResponseType.No
    ? {
        [QuestionResponseType.Yes]: 0.89,
        [QuestionResponseType.No]: 0.11,
      }
    : {
        [QuestionResponseType.No]: 0.89,
        [QuestionResponseType.Yes]: 0.11,
      }
}

function getWidth(decimal: number) {
  const totalAvailableWidth = deviceWidth - RESULT_MARGIN
  const maxAllowedWidth = totalAvailableWidth - MIN_WIDTH
  const calculatedWidth = totalAvailableWidth * decimal
  const fittedWidth = Math.min(
    Math.max(calculatedWidth, MIN_WIDTH),
    maxAllowedWidth
  )
  return fittedWidth
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  optionsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  optionButton: {
    flex: 1,
    backgroundColor: colors.tertiary30pct,
    marginRight: spacingUnit / 2,
    padding: spacingUnit,
  },
  optionButtonLast: {
    marginRight: 0,
  },
  optionText: {
    ...(fonts.BodyStrong as object),
    textAlign: 'center',
    color: colors.secondaryText,
    textTransform: 'uppercase',
  },
  resultsContainer: {
    flexDirection: 'row',
  },
  resultText: {
    ...(fonts.BodyStrong as object),
    padding: 8,
    color: colors.secondaryText,
    textAlign: 'center',
    flex: 1,
  },
  result: {
    flexGrow: 1,
    flexShrink: 1,
    minWidth: 50,
    flexDirection: 'row',
  },
  resultYes: {
    backgroundColor: colors.valid40pct,
    marginRight: RESULT_MARGIN,
  },
  resultNo: {
    backgroundColor: colors.invalid40pct,
  },
  inactive: {
    opacity: 0.2,
  },
  chosenOption: {
    borderWidth: 1,
    borderColor: colors.secondaryText,
  },
  chosenText: {
    ...(fonts.BodyBold as object),
    color: colors.secondaryText,
  },
  chosenOptionYes: {
    backgroundColor: colors.valid40pct,
  },
  chosenOptionNo: {
    backgroundColor: colors.invalid40pct,
  },
})

export default class QuestionOptionsBar extends React.Component<Props> {
  renderOptions = () => {
    const {
      chosen,
      hasBeenSubmitted,
      onNo,
      onYes,
      allowsVoting,
      hasBeenAnswered,
    } = this.props
    const isDisabled =
      hasBeenSubmitted ||
      hasBeenAnswered ||
      !allowsVoting ||
      (chosen !== null && chosen !== undefined)
    return (
      <View
        style={[styles.optionsContainer, hasBeenSubmitted && styles.inactive]}
      >
        <TouchableHighlight
          style={[
            styles.optionButton,
            chosen === QuestionResponseType.Yes && styles.chosenOption,
            chosen === QuestionResponseType.Yes && styles.chosenOptionYes,
          ]}
          onPress={onYes}
          disabled={isDisabled}
        >
          <Text
            style={[
              styles.optionText,
              chosen === QuestionResponseType.Yes && styles.chosenText,
            ]}
          >
            {translate('question.yes')}
          </Text>
        </TouchableHighlight>
        <TouchableHighlight
          style={[
            styles.optionButton,
            styles.optionButtonLast,
            chosen === QuestionResponseType.No && styles.chosenOption,
            chosen === QuestionResponseType.No && styles.chosenOptionNo,
          ]}
          onPress={onNo}
          disabled={isDisabled}
        >
          <Text
            style={[
              styles.optionText,
              chosen === QuestionResponseType.No && styles.chosenText,
            ]}
          >
            {translate('question.no')}
          </Text>
        </TouchableHighlight>
      </View>
    )
  }

  renderResults = () => {
    const { result: rawResult, chosen } = this.props
    if (!rawResult) {
      return false // FIXME make indicator showing "no results"
    }
    const result = fakeAdjustResultIfneeded(rawResult, chosen)
    const noFormatted = formatPercentage(result[QuestionResponseType.No])
    const yesFormatted = formatPercentage(result[QuestionResponseType.Yes])
    return (
      <View style={styles.resultsContainer}>
        <View
          style={[
            styles.resultYes,
            styles.result,
            chosen === QuestionResponseType.Yes && styles.chosenOption,
            { width: getWidth(result[QuestionResponseType.Yes]) },
          ]}
        >
          <Text numberOfLines={1} style={styles.resultText}>
            {yesFormatted}
          </Text>
        </View>
        <View
          style={[
            styles.resultNo,
            styles.result,
            chosen === QuestionResponseType.No && styles.chosenOption,
            { width: getWidth(result[QuestionResponseType.No]) },
          ]}
        >
          <Text numberOfLines={1} style={styles.resultText}>
            {noFormatted}
          </Text>
        </View>
      </View>
    )
  }

  render = () => {
    const { allowsVoting, result, hasBeenAnswered, chosen } = this.props
    if (!allowsVoting && (chosen === undefined || chosen === null)) {
      return this.renderResults()
    }
    if (result && hasBeenAnswered) {
      return this.renderResults()
    }
    return this.renderOptions()
  }
}

/**
 * allowsVoting results     hasBeenSubmitted    hasBeenAnswered   chosen        render
 * false          *             *                 *                  empty      renderResults
 * false          *             *                 *                  not empty  renderOptions
 * true         true          false               true               *          renderResults
 * true         true          true                true               *          renderResults
 * true         true          true                false              *          renderOptions
 * true         true          false               false              *          renderOptions
 * true         false         true                true               *          renderOptions
 * true         false         true                false              *          renderOptions
 * true         false         false               true               *          renderOptions
 * true         false         false               false              *          renderOptions
 */

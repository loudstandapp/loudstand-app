import React from 'react'
import { View, StyleSheet } from 'react-native'
import { spacingUnit, marginDefault } from '../theme/layout'
import colors from '../theme/colors'

const styles = StyleSheet.create({
  separator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: colors.tertiaryLight,
    marginVertical: spacingUnit / 2,
    marginHorizontal: marginDefault,
  },
})

export class ListSeparator extends React.PureComponent {
  render = () => <View style={styles.separator} />
}

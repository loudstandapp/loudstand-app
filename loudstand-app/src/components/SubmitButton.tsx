import React from 'react'
import { StyleSheet, TouchableOpacity, Text } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { translate } from '../translation'
import colors from '../theme/colors'
import fonts from '../theme/fonts'
import { buttonHeight, spacingUnit } from '../theme/layout'

const styles = StyleSheet.create({
  submitButton: {
    height: buttonHeight,
  },
  submitButtonWrapper: {
    borderRadius: buttonHeight / 2,
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 3 * spacingUnit,
  },
  submitButtonText: {
    ...(fonts.HeaderSmall as object),
    color: colors.secondaryText,
  },
  disabled: { opacity: 0.4 },
})

interface Props {
  onSubmit: () => void
  title?: string
  disabled?: boolean
}

export default class SubmitButton extends React.PureComponent<Props> {
  render = () => {
    const { onSubmit, title, disabled } = this.props
    return (
      <TouchableOpacity
        onPress={onSubmit}
        style={[styles.submitButton, disabled && styles.disabled]}
        disabled={disabled}
      >
        <LinearGradient
          colors={[colors.primary, colors.primaryAlt]}
          style={styles.submitButtonWrapper}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
        >
          <Text style={styles.submitButtonText}>
            {title || translate('loudstand.submit')}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    )
  }
}

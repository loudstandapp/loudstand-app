import React from 'react'
import { storiesOf } from '@storybook/react-native'
import Flag from './Flag'

storiesOf('Flag', module)
  .add('Denmark', () => <Flag countryCode="DNK" />)
  .add('Portugal', () => <Flag countryCode="PRT" />)
  .add('Turkey', () => <Flag countryCode="TUR" />)
  .add('USA', () => <Flag countryCode="USA" />)
  .add('Venezuela', () => <Flag countryCode="VEN" />)

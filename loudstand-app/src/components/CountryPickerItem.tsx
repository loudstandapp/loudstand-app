import React from 'react'
import { ISO3166v1a2Country } from '@loudstand/shared'
import { Text, StyleSheet, View } from 'react-native'
import countries from 'i18n-iso-countries'
import Flag from './Flag'
import { currentLocale } from '../translation'
import fonts from '../theme/fonts'
import colors from '../theme/colors'
import { marginDefault } from '../theme/layout'

const styles = StyleSheet.create({
  pickerRow: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  pickerText: {
    ...(fonts.Header as object),
    marginLeft: marginDefault,
    color: colors.secondaryText,
    flex: 1,
  },
})

interface Props {
  countryCode: ISO3166v1a2Country
}

const { languageCode } = currentLocale

export default class CountryPickerItem extends React.PureComponent<Props> {
  render = () => {
    const { countryCode } = this.props
    const countryName = countries.getName(countryCode, languageCode)
    return (
      <View style={styles.pickerRow}>
        <Flag countryCode={countryCode} />
        <Text style={styles.pickerText}>{countryName}</Text>
      </View>
    )
  }
}

import React, { ReactNode } from 'react'

import { View, StyleProp, ViewStyle } from 'react-native'

interface Props {
  style?: StyleProp<ViewStyle>
  children: ReactNode
}

export default function MockImageBackground({ style, children }: Props) {
  return (
    <View
      style={{
        // @ts-ignore
        ...style,
        backgroundColor: 'brown',
      }}
    >
      {children}
    </View>
  )
}

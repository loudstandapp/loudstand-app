import React, { Component } from 'react'
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  TouchableWithoutFeedback,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native'

const styles = StyleSheet.create({
  outerContainer: {
    backgroundColor: 'rgba(0,0,0,0.7)',
    width: '100%',
    height: '100%',
  },
  innerContainer: {
    height: '100%',
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    display: 'flex',
    marginTop: 20,
    marginHorizontal: 20,
    padding: 10,
  },
  results: {
    width: '100%',
    flex: 1,
  },
  resultRow: {
    fontSize: 25,
  },
})

export interface NavigationProps {
  dismiss: () => void
}

export interface OwnProps {}

export type Props = NavigationProps & OwnProps

// eslint-disable-next-line react/prefer-stateless-function
export default class SearchOverlay extends Component<Props> {
  render() {
    const { dismiss } = this.props
    return (
      <KeyboardAvoidingView enabled behavior="padding">
        <TouchableWithoutFeedback onPress={dismiss}>
          <View style={styles.outerContainer}>
            <SafeAreaView>
              <View style={styles.innerContainer}>
                <TextInput />
                <ScrollView style={styles.results}>
                  <View onStartShouldSetResponder={() => true}>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>Result a</Text>
                    <Text style={styles.resultRow}>3a</Text>
                    <Text style={styles.resultRow}>2</Text>
                    <Text style={styles.resultRow}>1</Text>
                    <Text style={styles.resultRow}>LAST</Text>
                  </View>
                </ScrollView>
              </View>
            </SafeAreaView>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    )
  }
}

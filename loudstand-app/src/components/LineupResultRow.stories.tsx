import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { PlayerPosition } from '@loudstand/shared'
import LineupResultRow from './LineupResultRow'

const TEST_PLAYER_SHARES = [
  {
    player: {
      id: 'sr:player:254119',
      country: 'DE',
      number: 27,
      name: 'Marius Wolf',
      position: PlayerPosition.Midfielder,
    },
    share: 0.12,
  },

  {
    player: {
      id: 'sr:player:171927',
      country: 'DE',
      number: 20,
      name: 'Maximilian Philipp',
      position: PlayerPosition.Forward,
    },
    share: 0.77,
  },
  {
    player: {
      id: 'sr:player:66518',
      country: 'DE',
      number: 10,
      name: 'Mario Gotze',
      position: PlayerPosition.Midfielder,
    },
    share: 0.009,
  },
  {
    player: {
      id: 'sr:player:37813',
      country: 'CH',
      number: 35,
      name: 'Marwin Hitz',
      position: PlayerPosition.Goalkeeper,
    },
    share: 0.65,
  },
  {
    player: {
      id: 'sr:player:1047147',
      country: null,
      number: 7,
      name: 'Jadon Sancho',
      position: PlayerPosition.Forward,
    },
    share: 0.72,
  },
]

storiesOf('LineupResultRow', module).add('default', () => (
  <LineupResultRow
    playerShares={TEST_PLAYER_SHARES}
    jerseyColors={{ number: 'red', sleeve: 'blue', base: 'black' }}
  />
))

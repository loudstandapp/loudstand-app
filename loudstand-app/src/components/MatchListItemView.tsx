import React from 'react'
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native'
import {
  MatchListItem,
  LocalizedTeams,
  ISO8601Date,
  MatchResultItem,
  MatchState,
} from '@loudstand/shared'
import Color from 'color'
import {
  getImageUrl,
  ItemImageType,
  localizeTeam,
  fallbackImages,
} from '../utils'
import { translate } from '../translation'
import { marginDefault, spacingUnit } from '../theme/layout'
import colors from '../theme/colors'
import fonts from '../theme/fonts'
import RemoteImage from './RemoteImage'
import RemoteImageBackground from './RemoteImageBackground'

interface Props {
  match: MatchListItem
  result?: MatchResultItem
  onSelectMatch: (match: MatchListItem) => void
  localizedTeams?: LocalizedTeams
  dateFormatter: { format: (date: Date) => string }
  today?: ISO8601Date
}

const IMAGE_WIDTH = 112
const TEAM_LOGO_SIZE = 24
const BORDER_RADIUS = 4
const LIVE_INDICATOR_SIZE = 8
const teamLogoCircleSize = TEAM_LOGO_SIZE * Math.SQRT2

const styles = StyleSheet.create({
  matchContainer: {
    flexDirection: 'row',
    marginHorizontal: marginDefault,
    marginVertical: spacingUnit,
    backgroundColor: Color(colors.tertiaryLight)
      .alpha(0.4)
      .toString(),
    borderRadius: BORDER_RADIUS,
  },
  imageContainer: {
    justifyContent: 'center',
    borderRadius: BORDER_RADIUS,
  },
  imageContainerBackground: {
    width: IMAGE_WIDTH,
    paddingHorizontal: spacingUnit,
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: BORDER_RADIUS,
  },
  teamLogo: {
    width: TEAM_LOGO_SIZE,
    height: TEAM_LOGO_SIZE,
  },
  teamLogoCircle: {
    width: teamLogoCircleSize,
    height: teamLogoCircleSize,
    backgroundColor: 'rgba(255,255,255,0.6)',
    borderRadius: teamLogoCircleSize / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  teamsSeparator: {
    ...(fonts.BodyBold as object),
    color: colors.secondaryText,
  },
  liveIndicator: {
    width: LIVE_INDICATOR_SIZE,
    height: LIVE_INDICATOR_SIZE,
    borderRadius: LIVE_INDICATOR_SIZE / 2,
    backgroundColor: colors.live,
    position: 'absolute',
    left: spacingUnit / 2,
    top: spacingUnit / 2,
  },
  score: {
    ...(fonts.BodyBold as object),
    color: colors.secondaryText,
  },
  teamsRow: {},
  infoRow: {},

  detailsBox: {
    flex: 1,
    padding: 8,
  },
  topRowText: {
    ...(fonts.Body as object),
  },
  bottomRowText: {
    ...(fonts.BodyStrong as object),
  },
})
export default class MatchListItemView extends React.PureComponent<Props> {
  handleSelectMatch = () => {
    const { onSelectMatch, match } = this.props
    onSelectMatch(match)
  }

  render = () => {
    const { match, localizedTeams, dateFormatter, result } = this.props
    return (
      <TouchableOpacity onPress={this.handleSelectMatch}>
        <View style={[styles.matchContainer]}>
          <RemoteImageBackground
            source={
              match.venue
                ? { uri: getImageUrl(match.venue.id, ItemImageType.Venue) }
                : fallbackImages.venue
            }
            defaultSource={fallbackImages.venue}
            style={styles.imageContainer}
          >
            <View style={styles.imageContainerBackground}>
              <View style={styles.teamLogoCircle}>
                <RemoteImage
                  style={styles.teamLogo}
                  defaultSource={fallbackImages.teamLogo}
                  source={{
                    uri: getImageUrl(
                      match.teams.home.id,
                      ItemImageType.TeamLogo
                    ),
                  }}
                />
              </View>
              {result && result.score ? (
                <Text style={styles.score}>
                  {`${result.score.home}-${result.score.away}`}
                </Text>
              ) : (
                <Text style={styles.teamsSeparator}>
                  {translate('match.vs')}
                </Text>
              )}
              <View style={styles.teamLogoCircle}>
                <RemoteImage
                  style={styles.teamLogo}
                  defaultSource={fallbackImages.teamLogo}
                  source={{
                    uri: getImageUrl(
                      match.teams.away.id,
                      ItemImageType.TeamLogo
                    ),
                  }}
                />
              </View>
            </View>
            {result && result.state === MatchState.Live && (
              <View style={styles.liveIndicator} />
            )}
          </RemoteImageBackground>
          <View style={styles.detailsBox}>
            <View style={styles.teamsRow}>
              <Text numberOfLines={1} style={styles.topRowText}>
                {`${localizeTeam(match.teams.home, localizedTeams)} ${translate(
                  'match.vs'
                )} ${localizeTeam(match.teams.away, localizedTeams)}`}
              </Text>
            </View>
            <View style={styles.infoRow}>
              <Text style={styles.bottomRowText} numberOfLines={1}>
                {match.venue
                  ? `${dateFormatter.format(new Date(match.startTime))} | ${
                      match.venue.name
                    }`
                  : dateFormatter.format(new Date(match.startTime))}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

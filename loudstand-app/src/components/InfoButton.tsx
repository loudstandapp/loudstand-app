import React from 'react'
import {
  TouchableOpacity,
  Image,
  StyleSheet,
  StyleProp,
  ViewStyle,
  ImageStyle,
} from 'react-native'
import colors from '../theme/colors'
import { showInfo, InfoType } from './Info'
import { touchableSize } from '../theme/layout'

interface Props {
  style?: StyleProp<ViewStyle>
  iconStyle?: StyleProp<ImageStyle>
  type: InfoType
}

const styles = StyleSheet.create({
  container: {
    width: touchableSize,
    height: touchableSize,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    right: 0,
  },
  image: { tintColor: colors.secondary },
})

export default class InfoButton extends React.PureComponent<Props> {
  handlePress = () => {
    const { type } = this.props

    showInfo(type)
  }

  render = () => {
    const { style, iconStyle } = this.props
    return (
      <TouchableOpacity
        onPress={this.handlePress}
        style={[styles.container, style]}
        hitSlop={{
          top: 10,
          left: 10,
          right: 10,
          bottom: 10,
        }}
      >
        <Image
          style={[styles.image, iconStyle]}
          source={require('../../assets/icons/buttons/information.png')}
        />
      </TouchableOpacity>
    )
  }
}

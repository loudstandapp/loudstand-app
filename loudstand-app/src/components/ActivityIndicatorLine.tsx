import React from 'react'
import { StyleSheet, Animated, View, Dimensions, Easing } from 'react-native'
import { spacingUnit } from '../theme/layout'

const { width } = Dimensions.get('screen')

const INDICATOR_SIZE = width / 2

const styles = StyleSheet.create({
  container: {
    height: spacingUnit / 2,
  },
  indicator: {
    width: INDICATOR_SIZE,
    flex: 1,
  },
})

interface Props {
  indicatorColor: string
  backgroundColor: string
}

export default class ActivityIndicatorLine extends React.PureComponent<Props> {
  position = new Animated.Value(0)

  animation = Animated.loop(
    Animated.sequence([
      Animated.timing(this.position, {
        toValue: 1,
        duration: 1000,
        easing: Easing.ease,
      }),
    ])
  )

  componentDidMount = () => this.animation.start()

  render = () => {
    const { indicatorColor, backgroundColor } = this.props
    const indicatorOffset = this.position.interpolate({
      inputRange: [0, 1],
      outputRange: [-INDICATOR_SIZE, width],
    })
    return (
      <View style={[styles.container, { backgroundColor }]}>
        <Animated.View
          style={[
            styles.indicator,
            { left: indicatorOffset, backgroundColor: indicatorColor },
          ]}
        />
      </View>
    )
  }
}

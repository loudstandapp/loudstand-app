import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { Text } from 'react-native'
import RemoteImageBackground from './RemoteImageBackground'

const FALLBACK_IMAGE = require('../../assets/images/fallback/player.jpg')

const Content = () => (
  <Text style={{ fontSize: 18, color: 'purple' }}>
    blaaaaaa some content is here
  </Text>
)

storiesOf('RemoteImageBackground', module)
  .add('20 KB', () => (
    <RemoteImageBackground
      source={{
        uri:
          'http://www.uandblog.com/uandblog-adminpanel-superadmin/images/Reduce_Image_Size_Less_than_10KB_7_uandblog.jpg',
      }}
      defaultSource={FALLBACK_IMAGE}
      style={{ width: 120, height: 200 }}
    >
      <Content />
    </RemoteImageBackground>
  ))
  .add('400 KB', () => (
    <RemoteImageBackground
      source={{ uri: 'https://msme.gov.in/sites/default/files/ntpc-7.jpg' }}
      defaultSource={FALLBACK_IMAGE}
      style={{ width: 300, height: 80 }}
    >
      <Content />
    </RemoteImageBackground>
  ))
  .add('2 MB', () => (
    <RemoteImageBackground
      source={{
        uri:
          'https://upload.wikimedia.org/wikipedia/commons/4/4e/Pleiades_large.jpg',
      }}
      defaultSource={FALLBACK_IMAGE}
      style={{ width: 300, height: 300 }}
    >
      <Content />
    </RemoteImageBackground>
  ))
  .add('error 404', () => (
    <RemoteImageBackground
      source={{ uri: 'https://msme.gov.in/sites/default/files/ndsfsfs7.jpg' }}
      defaultSource={FALLBACK_IMAGE}
      style={{ width: 300, height: 120 }}
    >
      <Content />
    </RemoteImageBackground>
  ))
  .add('error DNS', () => (
    <RemoteImageBackground
      source={{ uri: 'https:/dasd.coma/sites/default/files/ndsfsfs7.jpg' }}
      defaultSource={FALLBACK_IMAGE}
      style={{ width: 300, height: 120 }}
    >
      <Content />
    </RemoteImageBackground>
  ))

import React from 'react'
import { StyleSheet, TouchableOpacity, Text, View } from 'react-native'
import colors from '../theme/colors'
import fonts from '../theme/fonts'
import { buttonHeight, spacingUnit } from '../theme/layout'

const styles = StyleSheet.create({
  button: {
    height: buttonHeight,
  },
  buttonWrapper: {
    borderRadius: buttonHeight / 2,
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 3 * spacingUnit,
    backgroundColor: colors.tertiaryLight,
  },
  buttonText: {
    ...(fonts.HeaderSmall as object),
  },
  disabled: {
    opacity: 0.4,
  },
})

interface Props {
  onPress: () => void
  title: string
  disabled?: boolean
}

export default class GenericButton extends React.PureComponent<Props> {
  render = () => {
    const { onPress, title, disabled } = this.props
    return (
      <TouchableOpacity
        onPress={onPress}
        style={[styles.button, disabled && styles.disabled]}
        disabled={disabled}
      >
        <View style={styles.buttonWrapper}>
          <Text style={styles.buttonText}>{title}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

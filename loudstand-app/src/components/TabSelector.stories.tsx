import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import TabSelector from './TabSelector'

storiesOf('TabSelector', module).add('default', () => (
  <TabSelector
    options={['Tab 1', 'Tab 2', 'Tab with long name']}
    onSelect={action('onSelect')}
  />
))

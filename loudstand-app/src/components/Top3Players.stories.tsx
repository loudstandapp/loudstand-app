import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { MostValuablePlayerResultDocument } from '@loudstand/shared'
import Top3Players from './Top3Players'
import { TEST_TEAM_PLAYERS } from '../features/stats/screens/TeamScreen.stories'

export const TEST_TOP_PLAYERS: MostValuablePlayerResultDocument = {
  players: {
    'sr:player:178407': 0.2, // G
    'sr:player:47906': 0.3, // G
    'sr:player:557008': 0.6, // G
    'sr:player:37378': 0.1, // G
  },
  voteCount: 123,
}
storiesOf('Top3Players', module)
  .add('default', () => (
    <Top3Players results={TEST_TOP_PLAYERS} teamPlayers={TEST_TEAM_PLAYERS} />
  ))
  .add('one 100% player', () => (
    <Top3Players
      results={{ voteCount: 1, players: { 'sr:player:37378': 1 } }}
      teamPlayers={TEST_TEAM_PLAYERS}
    />
  ))

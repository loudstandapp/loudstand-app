import React from 'react'

import { View, StyleProp, ViewStyle } from 'react-native'

interface Props {
  style?: StyleProp<ViewStyle>
}

export default function MockImage({ style }: Props) {
  return <View style={[style, { backgroundColor: 'pink' }]} />
}

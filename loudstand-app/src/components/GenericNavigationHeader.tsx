import React from 'react'
import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { spacingUnit } from '../theme/layout'
import colors from '../theme/colors'
import fonts from '../theme/fonts'

import BackButton from './BackButton'

const CONTENT_HEIGHT = 48

interface Props {
  onBack?: () => void
  title?: string
}

const styles = StyleSheet.create({
  backgroundImage: {},
  safeArea: {},
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: CONTENT_HEIGHT,
  },
  gradientOverlay: {},
  leftButtonContainer: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    flex: 1,
    justifyContent: 'center',
  },

  backButton: {},
  title: {
    ...(fonts.TopBarTitle as object),
    color: colors.secondaryText,
  },

  textContainer: {
    alignItems: 'center',
    marginHorizontal: 4 * spacingUnit,
  },
  childrenContainer: {
    marginHorizontal: 6 * spacingUnit,
  },
})

export default class GenericNavigationHeader extends React.PureComponent<
  Props
> {
  render = () => {
    const { onBack, title, children } = this.props
    return (
      <LinearGradient
        colors={[colors.fixmeSecondaryBackground, colors.secondary]}
        style={styles.gradientOverlay}
      >
        <SafeAreaView style={styles.safeArea}>
          <View style={styles.container}>
            {title && (
              <View style={styles.textContainer}>
                <Text style={styles.title} numberOfLines={1}>
                  {title}
                </Text>
              </View>
            )}
            {children && (
              <View style={styles.childrenContainer}>{children}</View>
            )}
            {onBack && (
              <View style={styles.leftButtonContainer}>
                <BackButton tintColor={colors.secondaryText} onPress={onBack} />
              </View>
            )}
          </View>
        </SafeAreaView>
      </LinearGradient>
    )
  }
}

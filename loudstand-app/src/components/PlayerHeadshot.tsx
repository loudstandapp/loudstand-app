import React from 'react'
import { TeamPlayer, JerseyColors } from '@loudstand/shared'
import { StyleProp, ViewStyle, Text, View, StyleSheet } from 'react-native'
import FastImage from 'react-native-fast-image'
import colors from '../theme/colors'
import { getImageUrl, ItemImageType } from '../utils'
import fonts from '../theme/fonts'

const defaultColors: JerseyColors = {
  base: colors.primary,
  number: colors.secondaryText,
  sleeve: colors.secondary,
}

interface Props {
  player: TeamPlayer
  jerseyColors: JerseyColors
  size: 'large' | 'medium' | 'small'
  style?: StyleProp<ViewStyle>
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let styles: StyleSheet.NamedStyles<any>

export default class PlayerHeadshot extends React.PureComponent<Props> {
  static defaultProps = { jerseyColors: defaultColors }

  static largeSize = 48

  static mediumSize = 36

  static smallSize = 24

  render = () => {
    const { player, jerseyColors, size, style } = this.props
    return (
      <View
        style={[
          styles.container,
          size === 'large' && styles.large,
          size === 'small' && styles.small,
          size === 'medium' && styles.medium,
          { backgroundColor: jerseyColors.base },
          style && (style as object),
        ]}
      >
        <Text
          style={[
            size === 'medium' && styles.numberText,
            size === 'large' && styles.largeNumberText,
            size === 'small' && styles.smallNumberText,
            { color: jerseyColors.number as string },
          ]}
        >
          {player.number || '?'}
        </Text>
        <FastImage
          style={[
            styles.image,
            size === 'large' && styles.large,
            size === 'small' && styles.small,
            size === 'medium' && styles.medium,
          ]}
          source={{ uri: getImageUrl(player.id, ItemImageType.Headshot) }}
        />
      </View>
    )
  }
}

styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  numberText: {
    ...(fonts.Body as object),
  },
  image: {
    ...StyleSheet.absoluteFillObject,
  },
  large: {
    width: PlayerHeadshot.largeSize,
    height: PlayerHeadshot.largeSize,
    borderRadius: PlayerHeadshot.largeSize / 2,
  },
  small: {
    width: PlayerHeadshot.smallSize,
    height: PlayerHeadshot.smallSize,
    borderRadius: PlayerHeadshot.smallSize / 2,
  },
  medium: {
    width: PlayerHeadshot.mediumSize,
    height: PlayerHeadshot.mediumSize,
    borderRadius: PlayerHeadshot.mediumSize / 2,
  },
  smallNumberText: {
    ...(fonts.BodySmall as object),
  },
  largeNumberText: {
    ...(fonts.BodyLarge as object),
  },
})

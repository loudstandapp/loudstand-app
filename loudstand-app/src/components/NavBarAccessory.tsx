import React from 'react'
import { View, Text } from 'react-native'
import { ID } from '@loudstand/shared'

interface Props {
  id: ID
}

export default function NavBarAccessory({ id }: Props) {
  return (
    <View
      style={{
        flexDirection: 'row',
        // width: '100%',
        height: 90,
        backgroundColor: 'red',
        borderWidth: 1,
        borderColor: 'blue',
      }}
    >
      <Text>{id}</Text>
      {/* <Text>sadfafdsafdsafasdf asdfasd fasdf s</Text> */}
      <View style={{ width: 40, height: 40, backgroundColor: 'yellow' }}>
        <Text>aa</Text>
      </View>
    </View>
  )
}

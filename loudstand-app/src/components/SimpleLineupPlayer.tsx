import React from 'react'
import { TeamPlayer, JerseyColors, ID } from '@loudstand/shared'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import Color from 'color'
import { formatPercentage } from '../utils'
import { marginDefault, spacingUnit, isSmallDevice } from '../theme/layout'
import fonts from '../theme/fonts'
import colors from '../theme/colors'
import PlayerHeadshot from './PlayerHeadshot'

const PLAYER_BORDER_MARGIN = 3
const PERCENT_CIRCLE_SIZE = 28
const PERCENT_BORDER_MARGIN = 2

const styles = StyleSheet.create({
  imageContainer: {},
  playerContainer: {
    paddingVertical: marginDefault,
    alignItems: 'center',
    width: isSmallDevice ? '33%' : '25%',
  },
  borderContainer: {
    width: PlayerHeadshot.mediumSize + 2 * PLAYER_BORDER_MARGIN,
    height: PlayerHeadshot.mediumSize + 2 * PLAYER_BORDER_MARGIN,
    borderRadius: PlayerHeadshot.mediumSize / 2 + PLAYER_BORDER_MARGIN,
    backgroundColor: colors.secondary25pct,
    alignItems: 'center',
    justifyContent: 'center',
  },
  playerNameText: {
    marginTop: spacingUnit,
    ...(fonts.BodySmallBold as object),
    textAlign: 'center',
  },

  percentageText: {
    ...(fonts.CustomPercentageCircleSmall as object),
    color: colors.secondaryText,
  },
  percentageContainer: {
    height: PERCENT_CIRCLE_SIZE - 2 * PERCENT_BORDER_MARGIN,
    width: PERCENT_CIRCLE_SIZE - 2 * PERCENT_BORDER_MARGIN,
    borderRadius: PERCENT_CIRCLE_SIZE / 2 - PERCENT_BORDER_MARGIN,

    alignItems: 'center',
    justifyContent: 'center',
  },
  percentageBorder: {
    height: PERCENT_CIRCLE_SIZE,
    width: PERCENT_CIRCLE_SIZE,
    borderRadius: PERCENT_CIRCLE_SIZE / 2,
    borderWidth: PERCENT_BORDER_MARGIN,
    borderColor: colors.primaryLight,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: -PERCENT_CIRCLE_SIZE / 2,
    right: -PERCENT_CIRCLE_SIZE / 2,
  },
})

interface Props {
  share?: number
  player: TeamPlayer
  jerseyColors?: JerseyColors
  onSelectPlayer: (id: ID) => void
}

export default class SimpleLineupResultGroup extends React.PureComponent<
  Props
> {
  render = () => {
    const { player, share, jerseyColors, onSelectPlayer } = this.props

    return (
      <TouchableOpacity
        style={styles.playerContainer}
        onPress={() => onSelectPlayer(player.id)}
      >
        <View style={styles.imageContainer}>
          <View
            style={[
              styles.borderContainer,
              jerseyColors && {
                backgroundColor: colors.playerHeadshotBorder,
              },
            ]}
          >
            <PlayerHeadshot
              size="medium"
              player={player}
              jerseyColors={jerseyColors}
            />
          </View>
          {share !== undefined && (
            <View style={styles.percentageBorder}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[colors.primary, colors.primaryAlt]}
                style={styles.percentageContainer}
              >
                <Text style={styles.percentageText}>
                  {formatPercentage(share)}
                </Text>
              </LinearGradient>
            </View>
          )}
        </View>

        <Text numberOfLines={2} style={styles.playerNameText}>
          {player.name}
        </Text>
      </TouchableOpacity>
    )
  }
}

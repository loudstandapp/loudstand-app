import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'
import {
  TeamPlayer,
  MostValuablePlayerResultDocument,
  PlayerList,
  JerseyColors,
  ID,
} from '@loudstand/shared'
import LinearGradient from 'react-native-linear-gradient'
import Color from 'color'
import colors from '../theme/colors'
import { getComparatorForKey, formatPercentage } from '../utils'
import fonts from '../theme/fonts'
import { spacingUnit } from '../theme/layout'
import { translate } from '../translation'
import PlayerHeadshot from './PlayerHeadshot'
import ChosenIndicator from '../features/match/components/ChosenIndicator'

interface TeamPlayerShare {
  share: number
  player: TeamPlayer
}

interface Props {
  results: MostValuablePlayerResultDocument
  teamPlayers: PlayerList
  jerseyColors?: JerseyColors
  chosen?: ID
  onSelectPlayer: (id: ID) => void
}

const PLAYER_BORDER_MARGIN = 4
const PLAYER_BORDER_COLOR = Color(colors.secondary)
  .alpha(0.4)
  .toString()
const PLAYER_EXTRA_BORDER_MARGIN = 4
const PLAYER_EXTRA_BORDER_COLOR = Color(colors.secondary)
  .alpha(0.2)
  .toString()
const PERCENT_CIRCLE_SIZE = 36
const PERCENT_BORDER_MARGIN = 2
const BIG_THING_HEIGHT = 80

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginBottom: spacingUnit,
  },
  wrapper: {
    paddingTop: PlayerHeadshot.largeSize,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginHorizontal: 6 * spacingUnit,
  },
  playerColumn: {
    alignItems: 'center',
    flex: 1,
  },
  playerName: {
    marginTop: spacingUnit,
    height: 30,
    justifyContent: 'flex-start',
  },
  playerNameText: {
    ...(fonts.BodySmallBold as object),
    textAlign: 'center',
    height: 30,
  },
  playerImageBorder: {
    width: PlayerHeadshot.largeSize + 2 * PLAYER_BORDER_MARGIN,
    height: PlayerHeadshot.largeSize + 2 * PLAYER_BORDER_MARGIN,
    borderRadius: PlayerHeadshot.largeSize / 2 + PLAYER_BORDER_MARGIN,
    borderWidth: PLAYER_BORDER_MARGIN,
    borderColor: PLAYER_BORDER_COLOR,
  },
  playerImageExtraBorder: {
    width:
      PlayerHeadshot.largeSize +
      2 * PLAYER_BORDER_MARGIN +
      2 * PLAYER_EXTRA_BORDER_MARGIN,
    height:
      PlayerHeadshot.largeSize +
      2 * PLAYER_BORDER_MARGIN +
      2 * PLAYER_EXTRA_BORDER_MARGIN,
    borderRadius:
      PlayerHeadshot.largeSize / 2 +
      PLAYER_BORDER_MARGIN +
      PLAYER_EXTRA_BORDER_MARGIN,
    borderWidth: PLAYER_EXTRA_BORDER_MARGIN,
    borderColor: PLAYER_EXTRA_BORDER_COLOR,
    position: 'absolute',
    top:
      -PlayerHeadshot.largeSize / 2 -
      PLAYER_BORDER_MARGIN -
      PLAYER_EXTRA_BORDER_MARGIN,
    left: -PLAYER_BORDER_MARGIN - PLAYER_EXTRA_BORDER_MARGIN,
  },
  percentageContainer: {
    height: PERCENT_CIRCLE_SIZE - 2 * PERCENT_BORDER_MARGIN,
    width: PERCENT_CIRCLE_SIZE - 2 * PERCENT_BORDER_MARGIN,
    borderRadius: PERCENT_CIRCLE_SIZE / 2 - PERCENT_BORDER_MARGIN,
    alignItems: 'center',
    justifyContent: 'center',
  },
  percentageBorder: {
    height: PERCENT_CIRCLE_SIZE,
    width: PERCENT_CIRCLE_SIZE,
    borderRadius: PERCENT_CIRCLE_SIZE / 2,
    borderWidth: PERCENT_BORDER_MARGIN,
    borderColor: colors.primaryLight,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: -PERCENT_CIRCLE_SIZE / 3,
    right: -PERCENT_CIRCLE_SIZE / 3,
  },
  bigThing: {
    height: BIG_THING_HEIGHT,
    width: PlayerHeadshot.largeSize,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.secondary,
    shadowColor: colors.secondary,
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
  rankingText: {
    ...(fonts.CustomPlayerRanking as object),
    color: colors.secondaryText,
    position: 'absolute',
    bottom: '20%',
  },
  percentageText: {
    ...(fonts.CustomPercentageCircleLarge as object),
    color: colors.secondaryText,
  },
})

export default class Top3Players extends React.PureComponent<Props> {
  renderPlayer = (player: TeamPlayer, share: number, rank: number) => {
    const scale = 1 - 0.1 * (rank - 1)
    const alpha = 1 - 0.3 * (rank - 1)
    const { jerseyColors, onSelectPlayer } = this.props
    return (
      <TouchableOpacity
        style={styles.playerColumn}
        onPress={() => onSelectPlayer(player.id)}
      >
        <View
          style={[
            styles.bigThing,
            {
              transform: [
                { scaleX: scale },
                { scaleY: scale },
                { translateY: ((1 - scale) * BIG_THING_HEIGHT) / 2 / scale },
              ],
            },
            {
              backgroundColor: Color(styles.bigThing.backgroundColor)
                .alpha(alpha)
                .toString(),
            },
          ]}
        >
          <View
            style={[
              styles.playerImageExtraBorder,
              rank > 1 && { borderColor: 'transparent' },
            ]}
          >
            <View
              style={[
                styles.playerImageBorder, //  FIXME
                rank > 1 && { borderColor: PLAYER_EXTRA_BORDER_COLOR },
              ]}
            >
              <PlayerHeadshot
                player={player}
                size="large"
                jerseyColors={jerseyColors}
              />
            </View>
            <View style={styles.percentageBorder}>
              <LinearGradient
                colors={[colors.primary, colors.primaryAlt]}
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 1 }}
                style={styles.percentageContainer}
              >
                <Text style={styles.percentageText}>
                  {formatPercentage(share)}
                </Text>
              </LinearGradient>
            </View>
          </View>
          <Text style={styles.rankingText}>
            {translate(`ordinalNumber.${rank}`)}
          </Text>
        </View>
        <View style={styles.playerName}>
          <Text
            numberOfLines={2}
            ellipsizeMode="tail"
            style={styles.playerNameText}
          >
            {player.name}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  render = () => {
    const { teamPlayers, results, chosen } = this.props
    const players = Object.keys(results.players)
      .reduce((acc: TeamPlayerShare[], playerId) => {
        if (!teamPlayers[playerId]) {
          return acc
        }
        return [
          ...acc,
          { player: teamPlayers[playerId], share: results.players[playerId] },
        ]
      }, [])
      .sort(getComparatorForKey('share', true))
      .slice(0, 3)

    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          {players[1] &&
            this.renderPlayer(players[1].player, players[1].share, 2)}
          {players[0] &&
            this.renderPlayer(players[0].player, players[0].share, 1)}
          {players[2] &&
            this.renderPlayer(players[2].player, players[2].share, 3)}
        </View>
        {chosen && teamPlayers[chosen] && (
          <ChosenIndicator value={teamPlayers[chosen].name} />
        )}
      </View>
    )
  }
}

import { Navigation } from 'react-native-navigation'
import {
  OwnProps as SearchOverlayProps,
  NavigationProps as SearchOverlayNavigationProps,
} from './SearchOverlay'
import { ScreenNames } from '../registerScreens'

export function showSearchOverlay(props: SearchOverlayProps) {
  const navigationProps: SearchOverlayNavigationProps = {
    dismiss: () => Navigation.dismissOverlay(ScreenNames.SearchOverlay),
  }
  Navigation.showOverlay({
    component: {
      name: ScreenNames.SearchOverlay,
      id: ScreenNames.SearchOverlay, // FIXME!!!
      passProps: {
        ...navigationProps,
        ...props,
      },
    },
  })
}

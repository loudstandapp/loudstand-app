import React from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  LayoutChangeEvent,
  LayoutRectangle,
  LayoutAnimation,
} from 'react-native'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import { marginDefault } from '../theme/layout'
import colors from '../theme/colors'
import fonts from '../theme/fonts'

interface Props {
  onSelect: (index: number) => void
  selectedIndex: number
  options: string[]
}

interface State {
  layouts: LayoutRectangle[]
  selectedIndex: number
}

const BUTTON_HEIGHT = 32

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderRadius: BUTTON_HEIGHT / 2,
    height: BUTTON_HEIGHT,
    backgroundColor: colors.tertiaryLight,
    flex: 0,
  },
  indicator: {
    backgroundColor: colors.secondary,
    position: 'absolute',
    top: 0,
    height: BUTTON_HEIGHT,
    borderRadius: BUTTON_HEIGHT / 2,
  },
  button: {
    height: BUTTON_HEIGHT,
    borderRadius: BUTTON_HEIGHT / 2,
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
  },
  buttonText: {
    ...(fonts.HeaderSmall as object),
    color: colors.primaryText,
    paddingHorizontal: marginDefault,
  },
  activeButton: {},
  activeButtonText: {
    color: colors.secondaryText,
  },
})

export default class TabSelector extends React.PureComponent<Props, State> {
  static defaultProps = {
    selectedIndex: 0,
  }

  constructor(props: Props) {
    super(props)
    this.state = { layouts: [], selectedIndex: props.selectedIndex }
  }

  componentDidUpdate = (prevProps: Props) => {
    const { selectedIndex } = this.props
    if (selectedIndex !== prevProps.selectedIndex) {
      this.setState({ selectedIndex })
    }
  }

  handleLayoutOption = (event: LayoutChangeEvent, index: number) => {
    const { layout } = event.nativeEvent
    this.setState(prevState => {
      const layouts = prevState.layouts.slice()
      layouts[index] = layout
      return {
        layouts,
      }
    })
  }

  handleChangeTab = (selectedIndex: number) => {
    const { onSelect } = this.props
    onSelect(selectedIndex)
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut)
    this.setState({ selectedIndex })
    ReactNativeHapticFeedback.trigger('selection')
  }

  render = () => {
    const { options } = this.props
    const { layouts, selectedIndex } = this.state
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.indicator,
            layouts[selectedIndex] && {
              left: layouts[selectedIndex].x,
              width: layouts[selectedIndex].width,
            },
          ]}
        />
        {options.map((option, index) => (
          <TouchableOpacity
            onPress={() => this.handleChangeTab(index)}
            key={index.toString()}
            onLayout={event => this.handleLayoutOption(event, index)}
            style={[
              styles.button,
              selectedIndex === index && styles.activeButton,
            ]}
          >
            <Text
              ellipsizeMode="tail"
              numberOfLines={1}
              style={[
                styles.buttonText,
                selectedIndex === index && styles.activeButtonText,
              ]}
            >
              {option}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    )
  }
}

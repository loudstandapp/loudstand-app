import React from 'react'
import { storiesOf } from '@storybook/react-native'
import RemoteImage from './RemoteImage'

const FALLBACK_IMAGE = require('../../assets/images/fallback/player.jpg')

storiesOf('RemoteImage', module)
  .add('20 KB', () => (
    <RemoteImage
      source={{
        uri:
          'http://www.uandblog.com/uandblog-adminpanel-superadmin/images/Reduce_Image_Size_Less_than_10KB_7_uandblog.jpg',
      }}
      defaultSource={FALLBACK_IMAGE}
      style={{ width: 120, height: 200 }}
    />
  ))
  .add('400 KB', () => (
    <RemoteImage
      source={{ uri: 'https://msme.gov.in/sites/default/files/ntpc-7.jpg' }}
      defaultSource={FALLBACK_IMAGE}
      style={{ width: 300, height: 80 }}
    />
  ))
  .add('2 MB', () => (
    <>
      <RemoteImage
        source={{
          uri:
            'https://upload.wikimedia.org/wikipedia/commons/4/4e/Pleiades_large.jpg',
        }}
        defaultSource={FALLBACK_IMAGE}
        style={{ width: 300, height: 300 }}
      />
      <RemoteImage
        source={{
          uri:
            'https://upload.wikimedia.org/wikipedia/commons/4/4e/Pleiades_large.jpg',
        }}
        defaultSource={FALLBACK_IMAGE}
        style={{ width: 300, height: 300 }}
      />
      <RemoteImage
        source={{
          uri:
            'https://upload.wikimedia.org/wikipedia/commons/4/4e/Pleiades_large.jpg',
        }}
        defaultSource={FALLBACK_IMAGE}
        style={{ width: 300, height: 300 }}
      />
      <RemoteImage
        source={{
          uri:
            'https://upload.wikimedia.org/wikipedia/commons/4/4e/Pleiades_large.jpg',
        }}
        defaultSource={FALLBACK_IMAGE}
        style={{ width: 300, height: 300 }}
      />
      <RemoteImage
        source={{
          uri:
            'https://upload.wikimedia.org/wikipedia/commons/4/4e/Pleiades_large.jpg',
        }}
        defaultSource={FALLBACK_IMAGE}
        style={{ width: 300, height: 300 }}
      />
      <RemoteImage
        source={{
          uri:
            'https://upload.wikimedia.org/wikipedia/commons/4/4e/Pleiades_large.jpg',
        }}
        defaultSource={FALLBACK_IMAGE}
        style={{ width: 300, height: 300 }}
      />
      <RemoteImage
        source={{
          uri:
            'https://upload.wikimedia.org/wikipedia/commons/4/4e/Pleiades_large.jpg',
        }}
        defaultSource={FALLBACK_IMAGE}
        style={{ width: 300, height: 300 }}
      />
    </>
  ))
  .add('error 404', () => (
    <RemoteImage
      source={{ uri: 'https://msme.gov.in/sites/default/files/ndsfsfs7.jpg' }}
      defaultSource={FALLBACK_IMAGE}
      style={{ width: 300, height: 120 }}
    />
  ))
  .add('error DNS', () => (
    <RemoteImage
      source={{ uri: 'https:/dasd.coma/sites/default/files/ndsfsfs7.jpg' }}
      defaultSource={FALLBACK_IMAGE}
      style={{ width: 300, height: 120 }}
    />
  ))

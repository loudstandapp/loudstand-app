import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { TeamPlayer, JerseyColors } from '@loudstand/shared'
import colors from '../theme/colors'
import fonts from '../theme/fonts'
import PlayerHeadshot from './PlayerHeadshot'

const styles = StyleSheet.create({
  pickerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  pickerText: {
    ...(fonts.Header as object),
    color: colors.secondaryText,
  },
})
interface Props {
  item: TeamPlayer
  jerseyColors?: JerseyColors
}

export default class PlayerPickerItem extends React.PureComponent<Props> {
  static backgroundColor = colors.navigationBackground

  render = () => {
    const { item, jerseyColors } = this.props
    return (
      <View style={styles.pickerRow}>
        <PlayerHeadshot
          player={item}
          size="medium"
          jerseyColors={jerseyColors}
        />
        <Text style={styles.pickerText}>{item.name}</Text>
        <Text style={styles.pickerText}>{item.number}</Text>
      </View>
    )
  }
}

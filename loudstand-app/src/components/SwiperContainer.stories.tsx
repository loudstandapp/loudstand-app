import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { Text } from 'react-native'
import SwiperContainer from './SwiperContainer'

storiesOf('SwiperContainer', module)
  .add('default', () => (
    <SwiperContainer>
      <Text>Screen 1</Text>
      <Text>Screen 2</Text>
      <Text>Screen 3</Text>
      <Text>Screen 4</Text>
    </SwiperContainer>
  ))
  .add('with titles', () => (
    <SwiperContainer titles={['title 1', 'title 2', 'longer title', 'st']}>
      <Text>Screen 1</Text>
      <Text>Screen 2</Text>
      <Text>Screen 3</Text>
      <Text>Screen 4</Text>
    </SwiperContainer>
  ))
  .add('with missing title', () => (
    <SwiperContainer titles={['title 1', 'title 2', 'longer title', '']}>
      <Text>Screen 1</Text>
      <Text>Screen 2</Text>
      <Text>Screen 3</Text>
      <Text>Screen 4</Text>
    </SwiperContainer>
  ))

import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { View } from 'react-native'
import { withKnobs, number } from '@storybook/addon-knobs'
import PercentageCircle from './PercentageCircle'

storiesOf('PercentageCircle', module)
  .addDecorator(getStory => (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      {getStory()}
    </View>
  ))
  .addDecorator(withKnobs)
  .add('0%', () => <PercentageCircle percentage={0} />)
  .add('5%', () => (
    <PercentageCircle percentage={5} title="Here is some title" />
  ))
  .add('10%', () => <PercentageCircle percentage={10} />)
  .add('20%', () => (
    <PercentageCircle percentage={20} title="Here is some title" />
  ))
  .add('50%', () => <PercentageCircle percentage={50} />)
  .add('77%', () => <PercentageCircle percentage={77} />)
  .add('100%', () => <PercentageCircle percentage={100} />)
  .add('custom', () => (
    <PercentageCircle
      title="Here is some title"
      percentage={number('Percentage', 70, {
        range: true,
        min: 0,
        max: 100,
        step: 1,
      })}
    />
  ))

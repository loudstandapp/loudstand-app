import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { View, TouchableHighlight, Text } from 'react-native'
import { withKnobs, number } from '@storybook/addon-knobs'
import PercentageCircleWithLines from './PercentageCircleWithLines'

storiesOf('PercentageCircleWithLines', module)
  .addDecorator(getStory => (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      {getStory()}
    </View>
  ))
  .addDecorator(withKnobs)
  .add('0%', () => (
    <PercentageCircleWithLines percentage={0} title="Some title" />
  ))
  .add('5%', () => (
    <PercentageCircleWithLines percentage={5} title="Some title" />
  ))
  .add('10%', () => <PercentageCircleWithLines percentage={10} />)
  .add('20%', () => (
    <PercentageCircleWithLines percentage={20} title="Some title" />
  ))
  .add('50%', () => <PercentageCircleWithLines percentage={50} />)
  .add('77%', () => <PercentageCircleWithLines percentage={77} />)
  .add('100%', () => <PercentageCircleWithLines percentage={100} />)
  .add('custom', () => (
    <PercentageCircleWithLines
      percentage={number('Percentage', 70, {
        range: true,
        min: 0,
        max: 100,
        step: 1,
      })}
    />
  ))
  .add('animated 77%', () => <Container percentage={77} />)

interface ContainerProps {
  percentage: number
  title?: string
}

class Container extends React.PureComponent<ContainerProps> {
  ref = React.createRef<PercentageCircleWithLines>()

  handleAnimate = () => {
    this.ref.current && this.ref.current.animateToValue()
  }

  render = () => {
    const { percentage, title } = this.props
    return (
      <View>
        <TouchableHighlight onPress={this.handleAnimate}>
          <Text>Animate</Text>
        </TouchableHighlight>
        <PercentageCircleWithLines
          percentage={percentage}
          title={title}
          shouldAnimate
          ref={this.ref}
        />
      </View>
    )
  }
}

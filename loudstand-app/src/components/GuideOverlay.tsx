import React from 'react'
import {
  View,
  Image,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { translate } from '../translation'
import colors from '../theme/colors'
import fonts from '../theme/fonts'
import { spacingUnit } from '../theme/layout'

type Mode = 'tip' | 'info'

interface Props {
  title: string
  body: string
  mode: Mode
  onDismiss: () => void
}

const HEADER_HEIGHT = 64

const styles = StyleSheet.create({
  outerTouchable: { flex: 1 },
  tipOuterWrapper: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: colors.secondary25pct,
    justifyContent: 'flex-end',
  },
  infoOuterWrapper: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: colors.secondary25pct,
    justifyContent: 'center',
  },
  container: {
    backgroundColor: colors.navigationBackground,
    borderRadius: spacingUnit,
    overflow: 'hidden',
    margin: 2 * spacingUnit,
  },
  header: {
    alignItems: 'center',
    padding: 2 * spacingUnit,
    paddingBottom: 3 * spacingUnit,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: HEADER_HEIGHT,
  },
  body: {
    // flex: 1,
  },
  bodyContainer: {
    paddingHorizontal: 3 * spacingUnit,
    paddingTop: HEADER_HEIGHT,
    paddingBottom: 2 * spacingUnit,
  },
  bodyText: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
    marginVertical: 2 * spacingUnit,
  },
  headerText: {
    ...(fonts.Body as object),
    color: colors.secondaryText,
    textTransform: 'uppercase',
  },
  headerImage: {},
  titleText: {
    ...(fonts.Header as object),
    color: colors.secondaryText,
    textTransform: 'uppercase',
    textAlign: 'center',
  },
  buttonContainer: {
    alignItems: 'center',
  },
  closeButtonText: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
    opacity: 0.5,
  },
})

export default class GuideOverlay extends React.PureComponent<Props> {
  render = () => {
    const { title, body, onDismiss, mode } = this.props
    return (
      <SafeAreaView
        style={[
          mode === 'tip' && styles.tipOuterWrapper,
          mode === 'info' && styles.infoOuterWrapper,
        ]}
      >
        <View style={styles.container}>
          <ScrollView
            style={styles.body}
            contentContainerStyle={styles.bodyContainer}
            // contentInset={{ top: HEADER_HEIGHT }}
          >
            <Text style={styles.titleText}>{title}</Text>
            <Text style={styles.bodyText}>{body}</Text>
            <TouchableOpacity
              onPress={onDismiss}
              style={styles.buttonContainer}
            >
              <Text style={styles.closeButtonText}>
                {mode === 'tip' && translate('guides.skipButton')}
                {mode === 'info' && translate('info.closeButton')}
              </Text>
            </TouchableOpacity>
          </ScrollView>
          <LinearGradient
            colors={[
              colors.primaryAlt,
              colors.navigationBackground,
              colors.navigationBackground0pct,
            ]}
            locations={[0.0, 0.8, 1]}
            style={styles.header}
          >
            <Image
              style={styles.headerImage}
              source={require('../../assets/icons/misc/logo-white-homescreen.png')}
            />
            <Text style={styles.headerText}>
              {translate('guides.logoTitle')}
            </Text>
          </LinearGradient>
        </View>
      </SafeAreaView>
    )
  }
}

import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { View } from 'react-native'
import CountryPickerItem from './CountryPickerItem'

storiesOf('CountryPickerItem', module)
  .addDecorator(getStory => (
    <View style={{ backgroundColor: 'black' }}>{getStory()}</View>
  ))
  .add('DK', () => <CountryPickerItem countryCode="DK" />)
  .add('US', () => <CountryPickerItem countryCode="US" />)

import React from 'react'
import { storiesOf } from '@storybook/react-native'
import {
  LineupSuggestionResultDocument,
  PlayerList,
  PlayerPosition,
} from '@loudstand/shared'
import LineupResult from './LineupResult'
import { TEST_TEAM_PLAYERS } from '../features/stats/screens/TeamScreen.stories'

const TEST_MANY_DEFENDERS: LineupSuggestionResultDocument = {
  players: {
    'sr:player:1047385': 0.2, // G
    'sr:player:37813': 0.3, // G
    'sr:player:253515': 0.6, // G
    'sr:player:37378': 0.1, // G
    'sr:player:26760': 0.9, // D
    'sr:player:903858': 0.7, // D
    'sr:player:280435': 0.8, // D
    'sr:player:246999': 0.94, // D
    'sr:player:39015': 0.92, // D
    'sr:player:907834': 0.01, // D
    'sr:player:383560': 0.94, // D
    'sr:player:341589': 0.22, // M
    'sr:player:35612': 0.25, // M
    'sr:player:809758': 0.21, // M
    'sr:player:66477': 0.27, // M
    'sr:player:1051087': 0.23, // M
    'sr:player:24829': 0.32, // M
    'sr:player:756428': 0.223, // M
    'sr:player:66518': 0.262, // M
    'sr:player:254119': 0.24, // M
    'sr:player:237068': 0.27, // M
    'sr:player:1419559': 0.21, // M
    'sr:player:171927': 0.5, // F
    'sr:player:37092': 0.04, // F
    'sr:player:96366': 0.06, // F
    'sr:player:1047147': 0.6, // F
  },
  voteCount: 123,
}

const TEST_MANY_FORWARDS: LineupSuggestionResultDocument = {
  players: {
    'sr:player:1047385': 0.2, // G
    'sr:player:37813': 0.3, // G
    'sr:player:253515': 0.6, // G
    'sr:player:37378': 0.1, // G
    'sr:player:26760': 0.29, // D
    'sr:player:903858': 0.47, // D
    'sr:player:280435': 0.28, // D
    'sr:player:246999': 0.24, // D
    'sr:player:39015': 0.22, // D
    'sr:player:907834': 0.01, // D
    'sr:player:383560': 0.44, // D
    'sr:player:341589': 0.22, // M
    'sr:player:35612': 0.25, // M
    'sr:player:809758': 0.21, // M
    'sr:player:66477': 0.27, // M
    'sr:player:1051087': 0.23, // M
    'sr:player:24829': 0.32, // M
    'sr:player:756428': 0.223, // M
    'sr:player:66518': 0.262, // M
    'sr:player:254119': 0.24, // M
    'sr:player:237068': 0.27, // M
    'sr:player:1419559': 0.21, // M
    'sr:player:171927': 0.9, // F
    'sr:player:37092': 0.94, // F
    'sr:player:96366': 0.85, // F
    'sr:player:1047147': 0.9, // F
  },
  voteCount: 123,
}

const TEST_MANY_MIDFIELDERS: LineupSuggestionResultDocument = {
  players: {
    'sr:player:1047385': 0.2, // G
    'sr:player:37813': 0.3, // G
    'sr:player:253515': 0.6, // G
    'sr:player:37378': 0.1, // G
    'sr:player:26760': 0.29, // D
    'sr:player:903858': 0.47, // D
    'sr:player:280435': 0.28, // D
    'sr:player:246999': 0.24, // D
    'sr:player:39015': 0.22, // D
    'sr:player:907834': 0.01, // D
    'sr:player:383560': 0.44, // D
    'sr:player:341589': 0.92, // M
    'sr:player:35612': 0.85, // M
    'sr:player:809758': 0.91, // M
    'sr:player:66477': 0.97, // M
    'sr:player:1051087': 0.83, // M
    'sr:player:24829': 0.72, // M
    'sr:player:756428': 0.923, // M
    'sr:player:66518': 0.962, // M
    'sr:player:254119': 0.84, // M
    'sr:player:237068': 0.97, // M
    'sr:player:1419559': 0.21, // M
    'sr:player:171927': 0.5, // F
    'sr:player:37092': 0.04, // F
    'sr:player:96366': 0.06, // F
    'sr:player:1047147': 0.6, // F
  },
  voteCount: 123,
}

const TEST_COLORS = { number: 'red', sleeve: 'blue', base: 'black' }

const TEST_REPLACED_PLAYERS_RESULT: LineupSuggestionResultDocument = {
  players: {
    'sr:player:1246940': 1,
    'sr:player:44640': 1,
    'sr:player:1246942': 1,
    'sr:player:85118': 1,
    'sr:player:41650': 1,
    'sr:player:44745': 1,
    'sr:player:796044': 1,
    'sr:player:96849': 1,
    'sr:player:1511195': 1,
    'sr:player:1512705': 1,
    'sr:player:47614': 1,
  },
  voteCount: 1,
}

const TEST_REPLACED_PLAYERS: PlayerList = {
  'sr:player:59843': {
    id: 'sr:player:59843',
    country: 'PT',
    number: 87,
    name: 'Zequinha',
    position: PlayerPosition.Forward,
  },
  'sr:player:1297890': {
    id: 'sr:player:1297890',
    country: 'PT',
    number: null,
    name: 'Joao Serrao',
    position: PlayerPosition.Defender,
  },
  'sr:player:1658717': {
    id: 'sr:player:1658717',
    country: 'MZ',
    number: null,
    name: 'Bruno Langa',
    position: PlayerPosition.Defender,
  },
  'sr:player:1852818': {
    id: 'sr:player:1852818',
    country: 'BR',
    number: null,
    name: 'Rodrigo Mathiola',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:44718': {
    id: 'sr:player:44718',
    country: 'PT',
    number: 7,
    name: 'Tiago',
    position: PlayerPosition.Forward,
  },
  'sr:player:31325': {
    id: 'sr:player:31325',
    country: 'GE',
    number: 90,
    name: 'Giorgi Makaridze',
    position: PlayerPosition.Goalkeeper,
  },
  'sr:player:1004363': {
    id: 'sr:player:1004363',
    country: 'PT',
    number: 27,
    name: 'Andre Pedrosa',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:925772': {
    id: 'sr:player:925772',
    country: 'PT',
    number: 47,
    name: 'Tiago Castro',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:310800': {
    id: 'sr:player:310800',
    country: 'PT',
    number: 4,
    name: 'Artur Jorge',
    position: PlayerPosition.Defender,
  },
  'sr:player:803786': {
    id: 'sr:player:803786',
    country: 'AR',
    number: 22,
    name: 'Brian Mansilla',
    position: PlayerPosition.Forward,
  },
  'sr:player:1672421': {
    id: 'sr:player:1672421',
    country: 'PT',
    number: 1,
    name: 'Joao Valido',
    position: PlayerPosition.Goalkeeper,
  },
  'sr:player:1848614': {
    id: 'sr:player:1848614',
    country: 'MZ',
    number: null,
    name: 'Neymar',
    position: PlayerPosition.Forward,
  },
  'sr:player:352880': {
    id: 'sr:player:352880',
    country: 'PT',
    number: 11,
    name: 'Hildeberto Pereira',
    position: PlayerPosition.Forward,
  },
  'sr:player:138889': {
    id: 'sr:player:138889',
    country: 'PT',
    number: 44,
    name: 'Luis Cortez',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:796044': {
    id: 'sr:player:796044',
    country: 'BR',
    number: 10,
    name: 'Eber Bessa',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:1430701': {
    id: 'sr:player:1430701',
    country: 'BR',
    number: 34,
    name: 'Leandro Sales',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:41650': {
    id: 'sr:player:41650',
    country: 'PT',
    number: 13,
    name: 'Vasco Fernandes',
    position: PlayerPosition.Defender,
  },
  'sr:player:44745': {
    id: 'sr:player:44745',
    country: 'PT',
    number: 17,
    name: 'Silvio',
    position: PlayerPosition.Defender,
  },
  'sr:player:309358': {
    id: 'sr:player:309358',
    country: 'GW',
    number: 23,
    name: 'Frederic Mendy',
    position: PlayerPosition.Forward,
  },
  'sr:player:47614': {
    id: 'sr:player:47614',
    country: 'PT',
    number: 8,
    name: 'Nuno Valente',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:85118': {
    id: 'sr:player:85118',
    country: 'BR',
    number: 12,
    name: 'Milton',
    position: PlayerPosition.Goalkeeper,
  },
  'sr:player:44696': {
    id: 'sr:player:44696',
    country: 'PT',
    number: 21,
    name: 'Nuno Pinto',
    position: PlayerPosition.Defender,
  },
  'sr:player:143570': {
    id: 'sr:player:143570',
    country: 'PT',
    number: 66,
    name: 'Joao Meira',
    position: PlayerPosition.Defender,
  },
  'sr:player:1511195': {
    id: 'sr:player:1511195',
    country: 'PT',
    number: 40,
    name: 'Baba Fernandes',
    position: PlayerPosition.Defender,
  },
  'sr:player:244787': {
    id: 'sr:player:244787',
    country: 'BR',
    number: 76,
    name: 'Carlinhos',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:44640': {
    id: 'sr:player:44640',
    country: 'PT',
    number: 5,
    name: 'Mano',
    position: PlayerPosition.Defender,
  },
  'sr:player:18159': {
    id: 'sr:player:18159',
    country: 'PT',
    number: 29,
    name: 'Jose Semedo',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:1134509': {
    id: 'sr:player:1134509',
    country: 'ZA',
    number: 26,
    name: 'Kgaogelo Rathete Sekgota',
    position: PlayerPosition.Midfielder,
  },
  'sr:player:1099100': {
    id: 'sr:player:1099100',
    country: 'PT',
    number: 16,
    name: 'Andre Sousa',
    position: PlayerPosition.Defender,
  },
  'sr:player:166499': {
    id: 'sr:player:166499',
    country: 'BR',
    number: 24,
    name: 'Bruno Pires',
    position: PlayerPosition.Defender,
  },
  'sr:player:1766439': {
    id: 'sr:player:1766439',
    country: 'MA',
    number: 9,
    name: 'Khalid Hachadi',
    position: PlayerPosition.Forward,
  },
}

storiesOf('LineupResult', module)
  .add('many defenders', () => (
    <LineupResult
      lineupSuggestionResult={TEST_MANY_DEFENDERS}
      teamPlayers={TEST_TEAM_PLAYERS}
      jerseyColors={TEST_COLORS}
    />
  ))
  .add('many midfielders', () => (
    <LineupResult
      lineupSuggestionResult={TEST_MANY_MIDFIELDERS}
      teamPlayers={TEST_TEAM_PLAYERS}
      jerseyColors={TEST_COLORS}
    />
  ))
  .add('many forwards', () => (
    <LineupResult
      lineupSuggestionResult={TEST_MANY_FORWARDS}
      teamPlayers={TEST_TEAM_PLAYERS}
      jerseyColors={TEST_COLORS}
    />
  ))
  .add('many replaced players', () => (
    <LineupResult
      lineupSuggestionResult={TEST_REPLACED_PLAYERS_RESULT}
      teamPlayers={TEST_REPLACED_PLAYERS}
      jerseyColors={TEST_COLORS}
    />
  ))

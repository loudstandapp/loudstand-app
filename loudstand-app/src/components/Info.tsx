import React from 'react'
import { Navigation } from 'react-native-navigation'
import GuideOverlay from './GuideOverlay'
import { translate } from '../translation'
import { ScreenNames } from '../registerScreens'
import { randomComponentId } from '../utils'

export enum InfoType {
  Lineup = 'LINEUP',
}

interface Props {
  type: InfoType
  onDismiss: () => void
}

function getContent() {
  const guideContent: { [key in InfoType]: { body: string; title: string } } = {
    [InfoType.Lineup]: {
      body: translate('info.lineup.body'),
      title: translate('info.lineup.title'),
    },
  }
  return guideContent
}

export default class Info extends React.PureComponent<Props> {
  render = () => {
    const { type, onDismiss } = this.props
    const infoContent = getContent()

    const { body, title } = infoContent[type]
    return (
      <GuideOverlay
        onDismiss={onDismiss}
        mode="info"
        body={body}
        title={title}
      />
    )
  }
}

export function showInfo(type: InfoType) {
  const componentId = randomComponentId(ScreenNames.Info)

  const passProps: Props = {
    type,
    onDismiss: () => Navigation.dismissOverlay(componentId),
  }

  Navigation.showOverlay({
    component: {
      name: ScreenNames.Info,
      id: componentId,
      passProps,
    },
  })
}

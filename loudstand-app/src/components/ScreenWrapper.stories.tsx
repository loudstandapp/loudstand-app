import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { Text, Alert, Button } from 'react-native'
import ScreenWrapper from './ScreenWrapper'

storiesOf('ScreenWrapper', module)
  .add('default', () => (
    <ScreenWrapper>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Button onPress={() => Alert.alert('hej')} title="click me" />
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
    </ScreenWrapper>
  ))
  .add('loading', () => (
    <ScreenWrapper isLoading>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Button onPress={() => Alert.alert('hej')} title="click me" />
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
    </ScreenWrapper>
  ))
  .add('generating', () => (
    <ScreenWrapper isGenerating>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Button onPress={() => Alert.alert('hej')} title="click me" />
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
    </ScreenWrapper>
  ))
  .add('generating and loading ', () => (
    <ScreenWrapper isGenerating isLoading>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Button onPress={() => Alert.alert('hej')} title="click me" />
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
      <Text>Here is some content</Text>
    </ScreenWrapper>
  ))

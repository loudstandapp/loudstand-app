import React from 'react'
import {
  LineupSuggestionResultDocument,
  PlayerList,
  PlayerPosition,
  JerseyColors,
  ID,
} from '@loudstand/shared'
import { ImageBackground, StyleSheet, View } from 'react-native'
import { spacingUnit, marginDefault } from '../theme/layout'
import colors from '../theme/colors'
import LineupResultRow, { TeamPlayerShare } from './LineupResultRow'
import {
  constraints,
  REQUIRED_NUMBER_OF_PLAYERS,
} from '../features/match/components/LineupPicker'
import { getComparatorForKey } from '../utils'
import ChosenIndicator from '../features/match/components/ChosenIndicator'
import InfoButton from './InfoButton'
import { InfoType } from './Info'

interface Props {
  lineupSuggestionResult: LineupSuggestionResultDocument
  teamPlayers: PlayerList
  jerseyColors?: JerseyColors
  chosen?: ID[]
  onSelectPlayer: (id: ID) => void
}
const styles = StyleSheet.create({
  container: {
    marginHorizontal: marginDefault,
    marginVertical: spacingUnit,
    flex: 1,
  },
  fieldContainer: {
    justifyContent: 'space-between',
    flex: 1,
  },
  fieldContainerImage: {
    tintColor: colors.tertiaryLight,
    borderWidth: 3,
    borderColor: colors.tertiaryLight,
  },
})

function generatePlayerShares(
  lineupSuggestionResult: LineupSuggestionResultDocument,
  teamPlayers: PlayerList
) {
  const playersWithShares = Object.keys(teamPlayers)
    .map(playerId => ({
      player: teamPlayers[playerId],
      share: lineupSuggestionResult.players[playerId] || 0,
    }))
    .sort(getComparatorForKey('share', true))

  const mostPopularPlayers = playersWithShares.slice(
    0,
    REQUIRED_NUMBER_OF_PLAYERS
  )
  const remainingPlayers = playersWithShares.slice(REQUIRED_NUMBER_OF_PLAYERS)
  const playerShares: { [key in PlayerPosition]: TeamPlayerShare[] } = {
    [PlayerPosition.Goalkeeper]: [],
    [PlayerPosition.Defender]: [],
    [PlayerPosition.Midfielder]: [],
    [PlayerPosition.Forward]: [],
  }

  const positions = [
    PlayerPosition.Goalkeeper,
    PlayerPosition.Forward,
    PlayerPosition.Midfielder,
    PlayerPosition.Defender,
  ]

  let playerCount = 0
  let remainingMax = positions.reduce(
    (total, position) => total + constraints[position].max,
    0
  )

  positions.forEach((position: PlayerPosition, index: number) => {
    const { min, max } = constraints[position]
    const remainingPlayerCount = REQUIRED_NUMBER_OF_PLAYERS - playerCount

    // Fill up remaining spots with the last position in list
    if (index === positions.length - 1) {
      const list = playersWithShares
        .filter(({ player }) => player.position === position)
        .slice(0, REQUIRED_NUMBER_OF_PLAYERS - playerCount)
      playerCount += list.length
      playerShares[position] = list
      return
    }
    const mostPopularOptions = mostPopularPlayers
      .filter(({ player }) => player.position === position)
      .slice(0, max)

    const current = mostPopularOptions.length
    const nextRemainingMax = remainingMax - max
    const totalMissing = Math.max(
      0,
      remainingPlayerCount - current - nextRemainingMax
    )
    const thisTypeMissing = Math.max(0, min - current)
    const playersToAdd = Math.max(totalMissing, thisTypeMissing)

    const remainingOptions = remainingPlayers
      .filter(({ player }) => player.position === position)
      .slice(0, playersToAdd)

    const list = [...mostPopularOptions, ...remainingOptions]
    playerCount += list.length
    remainingMax -= max
    playerShares[position] = list
  })

  return playerShares
}

export default class LineupResult extends React.PureComponent<Props> {
  render = () => {
    const {
      lineupSuggestionResult,
      teamPlayers,
      jerseyColors,
      chosen,
      onSelectPlayer,
    } = this.props
    const playerShares = generatePlayerShares(
      lineupSuggestionResult,
      teamPlayers
    )
    const chosenPlayers =
      chosen && chosen.map((id: ID) => teamPlayers[id].name).join(', ')
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.fieldContainer}
          imageStyle={styles.fieldContainerImage}
          source={require('../../assets/images/borderless-pitch.png')}
        >
          <LineupResultRow
            jerseyColors={jerseyColors}
            playerShares={playerShares[PlayerPosition.Goalkeeper]}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupResultRow
            jerseyColors={jerseyColors}
            playerShares={playerShares[PlayerPosition.Defender]}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupResultRow
            jerseyColors={jerseyColors}
            playerShares={playerShares[PlayerPosition.Midfielder]}
            onSelectPlayer={onSelectPlayer}
          />
          <LineupResultRow
            jerseyColors={jerseyColors}
            playerShares={playerShares[PlayerPosition.Forward]}
            onSelectPlayer={onSelectPlayer}
          />
          <InfoButton type={InfoType.Lineup} />
        </ImageBackground>
        {chosenPlayers && <ChosenIndicator value={chosenPlayers} />}
      </View>
    )
  }
}

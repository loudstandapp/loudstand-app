import React from 'react'
import { View, Text, StyleSheet, SafeAreaView } from 'react-native'
import { Dispatch, bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import LinearGradient from 'react-native-linear-gradient'
import { ReduxState } from '../store/model'
import { logoutThunk } from '../features/user/userFlow'
import { spacingUnit } from '../theme/layout'
import colors from '../theme/colors'
import GenericButton from './GenericButton'
import fonts from '../theme/fonts'
import RemoteImage from './RemoteImage'
import { translate } from '../translation'

const PROFILE_IMAGE_SIZE = 40

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    marginTop: spacingUnit,
  },
  safeArea: {},
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 2 * spacingUnit,
    paddingVertical: spacingUnit,
  },
  loggedInText: {
    ...(fonts.BodySmall as object),
    color: colors.secondaryText,
  },
  displayNameText: {
    ...(fonts.HeaderSmallLight as object),
    color: colors.secondaryText,
  },
  logoutButton: {
    backgroundColor: 'blue',
  },
  logoutButtonText: {
    color: 'red',
  },
  nameWrapper: {
    flex: 1,
    marginLeft: spacingUnit,
  },
  userImage: {
    width: PROFILE_IMAGE_SIZE,
    height: PROFILE_IMAGE_SIZE,
    borderRadius: PROFILE_IMAGE_SIZE / 2,
  },
})

interface Props {
  auth: {
    displayName: string
    photoURL?: string
  }
  onLogout: () => void
}

export class CurrentUserIndicator extends React.PureComponent<Props> {
  render = () => {
    const { auth, onLogout } = this.props
    return (
      <LinearGradient
        colors={[colors.secondary, colors.primaryAlt]}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
        style={styles.container}
      >
        <SafeAreaView style={styles.safeArea}>
          <View style={styles.wrapper}>
            {auth.photoURL && (
              <RemoteImage
                style={styles.userImage}
                source={{ uri: auth.photoURL }}
              />
            )}
            <View style={styles.nameWrapper}>
              <Text style={styles.loggedInText}>
                {translate('currentUserIndicator.loggedInAs')}
              </Text>
              <Text style={styles.displayNameText}>{auth.displayName}</Text>
            </View>
            <GenericButton
              title={translate('currentUserIndicator.logOut')}
              onPress={onLogout}
            />
          </View>
        </SafeAreaView>
      </LinearGradient>
    )
  }
}

const mapStateToProps = (state: ReduxState) => ({
  auth: state.firebase.auth,
})

const mapDispatchToProps = (dispatch: Dispatch) =>
  bindActionCreators(
    {
      onLogout: logoutThunk,
    },
    dispatch
  )
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CurrentUserIndicator)

import React from 'react'
import {
  SectionListData,
  SectionList,
  SectionListRenderItemInfo,
  View,
  Text,
  StyleSheet,
} from 'react-native'
import { LabelValuePair } from '@loudstand/shared'
import fonts from '../theme/fonts'
import { spacingUnit, marginDefault } from '../theme/layout'

interface Props {
  sections: SectionListData<LabelValuePair>[]
  topInset: number
}

const styles = StyleSheet.create({
  listRow: {
    flexDirection: 'row',
    marginVertical: spacingUnit,
    marginHorizontal: marginDefault,
  },
  listLabel: {
    flex: 1,
  },
  listValue: {
    flex: 0,
  },
  sectionHeader: {
    marginVertical: spacingUnit,
    ...(fonts.HeaderSmall as object),
    textAlign: 'center',
  },
  sectionList: {},
  sectionHeaderText: {},
})

export default function LabelValueList({ sections, topInset }: Props) {
  return (
    <SectionList
      contentContainerStyle={{ paddingTop: topInset }}
      style={styles.sectionList}
      stickySectionHeadersEnabled={false}
      sections={sections}
      renderSectionHeader={({
        section: { title, data },
      }: {
        section: SectionListData<LabelValuePair>
      }) =>
        title && data.length > 0 ? (
          <Text style={styles.sectionHeader}>{title}</Text>
        ) : (
          <></>
        )
      }
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item }: SectionListRenderItemInfo<LabelValuePair>) => (
        <View style={styles.listRow}>
          <Text style={[styles.listLabel, fonts.Body]}>{item.label}</Text>
          <Text style={[styles.listValue, fonts.BodyStrong]}>{item.value}</Text>
        </View>
      )}
    />
  )
}
LabelValueList.defaultProps = {
  topInset: 0,
}

import React from 'react'
import { LocalizedTeams } from '@loudstand/shared'
import { View, StyleSheet, Image, Text } from 'react-native'
import { ChosenTeamListItem } from '../store/model'
import fonts from '../theme/fonts'
import colors from '../theme/colors'
import {
  getImageUrl,
  ItemImageType,
  localizeTeam,
  fallbackImages,
} from '../utils'
import Flag from './Flag'
import RemoteImage from './RemoteImage'

const PICKER_IMAGE_SIZE = 32

const styles = StyleSheet.create({
  pickerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  pickerText: {
    ...(fonts.Header as object),
    color: colors.secondaryText,
  },
  pickerImage: {
    width: PICKER_IMAGE_SIZE,
    height: PICKER_IMAGE_SIZE,
  },
})

interface Props {
  item: ChosenTeamListItem
  localizedTeams?: LocalizedTeams
}

export default class TeamPickerItem extends React.PureComponent<Props> {
  render = () => {
    const { item, localizedTeams } = this.props
    return (
      <View style={styles.pickerRow}>
        <RemoteImage
          style={styles.pickerImage}
          source={{
            uri: getImageUrl(item.id, ItemImageType.TeamLogo),
          }}
          defaultSource={fallbackImages.teamLogo}
        />
        <Text style={styles.pickerText}>
          {localizeTeam(item, localizedTeams)}
        </Text>
        {item.isFavorite && (
          <Image
            source={require('../../assets/icons/indicators/full-star.png')}
          />
        )}
        {item.country && !item.isFavorite && (
          <Flag countryCode={item.country} />
        )}
      </View>
    )
  }
}

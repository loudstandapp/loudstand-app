import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { View, Text } from 'react-native'
import { action } from '@storybook/addon-actions'
import GenericNavigationHeader from './GenericNavigationHeader'

storiesOf('GenericNavigationHeader', module)
  .addDecorator(getStory => (
    <View style={{ flex: 1, justifyContent: 'center' }}>
      <View style={{ borderColor: 'red', borderWidth: 1 }}>{getStory()}</View>
    </View>
  ))
  .add('title', () => (
    <GenericNavigationHeader
      title="Some normal title"
      onBack={action('onBack')}
    />
  ))
  .add('no back button', () => (
    <GenericNavigationHeader title="Some normal title" />
  ))
  .add('long title', () => (
    <GenericNavigationHeader
      title="Some normal title very very long title"
      onBack={action('onBack')}
    />
  ))
  .add('children', () => (
    <GenericNavigationHeader onBack={action('onBack')}>
      <Text>asdas</Text>
    </GenericNavigationHeader>
  ))

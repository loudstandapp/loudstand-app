import React from 'react'
import { StyleSheet, View, Text, ScrollView, SafeAreaView } from 'react-native'
import { Navigation } from 'react-native-navigation'
import Color from 'color'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import colors from '../theme/colors'
import { isLoading } from '../store/selectors'
import { ScreenNames } from '../registerScreens'
import fonts from '../theme/fonts'
import { spacingUnit } from '../theme/layout'
import GenericButton from './GenericButton'

interface OwnProps {
  message: string
  onDismiss: (componentId: string) => void
}

interface NavigationProps {
  componentId: string
}

type Props = OwnProps & NavigationProps

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color(colors.fixmeSecondaryBackground)
      .alpha(0.95)
      .toString(),
  },
  safeAreaView: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  errorText: {
    ...(fonts.Header as object),
    color: colors.secondaryText,
    margin: 2 * spacingUnit,
  },
  scrollContent: {
    alignItems: 'center',
  },
})

export default class ErrorOverlay extends React.PureComponent<Props> {
  handleDismiss = () => {
    const { onDismiss, componentId } = this.props
    ReactNativeHapticFeedback.trigger('selection')

    onDismiss(componentId)
  }

  componentDidMount = () => {
    ReactNativeHapticFeedback.trigger('notificationError', {
      enableVibrateFallback: true,
      ignoreAndroidSystemSettings: true,
    })
  }

  render = () => {
    const { message } = this.props
    if (!isLoading) {
      return false
    }
    return (
      <View style={styles.container}>
        <SafeAreaView>
          <ScrollView contentContainerStyle={styles.scrollContent}>
            <Text style={styles.errorText}>{message}</Text>
            <GenericButton title="OK" onPress={this.handleDismiss} />
          </ScrollView>
        </SafeAreaView>
      </View>
    )
  }
}

function errorOverlayProps(message: string): OwnProps {
  return {
    message,
    onDismiss: (componentId: string) => Navigation.dismissOverlay(componentId),
  }
}

export function showErrorOverlay(message: string) {
  Navigation.showOverlay({
    component: {
      name: ScreenNames.ErrorOverlay,
      passProps: errorOverlayProps(message),
    },
  })
}

import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { TournamentSelectorButton } from './TournamentSelectorButton'

storiesOf('TourmamentSelectorButton', module).add('defalt', () => (
  <TournamentSelectorButton
    onPress={action('onPress')}
    activeTournament={{
      id: '1234',
      name: 'Superior league',
      currentSeason: { id: '12342536', name: 'bla' },
      isDefaultForTeams: true,
      languageCode: 'pl',
      country: 'DK',
    }}
  />
))

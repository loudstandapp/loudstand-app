import React from 'react'
import {
  ListRenderItem,
  FlatList,
  TouchableOpacity,
  ListRenderItemInfo,
  View,
  StyleSheet,
  TouchableHighlight,
  Animated,
  SafeAreaView,
} from 'react-native'
import { Navigation } from 'react-native-navigation'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import { marginDefault, spacingUnit } from '../theme/layout'
import colors from '../theme/colors'
import { ScreenNames } from '../registerScreens'
import { randomComponentId } from '../utils'

interface NavigationProps {
  onClose: () => void
}

interface OwnProps<T> {
  data: T[]
  renderItem: ListRenderItem<T>
  onSelect: (item: T) => boolean
  backgroundColor: string
}

interface Props<T> extends NavigationProps, OwnProps<T> {}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(0, 0, 0, 0.8)',
  },
  wrapper: {
    margin: 2 * marginDefault,
    borderRadius: spacingUnit,
    // padding,
  },
  list: {},
  touchableArea: {
    flex: 1,
  },
  touchablerow: {
    padding: marginDefault,
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: colors.tertiaryLight,
    marginHorizontal: marginDefault,
  },
  safeArea: {
    flex: 1,
  },
})

const ANIMATION_DURATION = 100

export default class PickerOverlay<T> extends React.Component<Props<T>> {
  opacity = new Animated.Value(0)

  componentDidMount = () => {
    Animated.timing(this.opacity, {
      toValue: 1,
      duration: ANIMATION_DURATION,
    }).start()
  }

  handleSelect = (item: T) => {
    const { onSelect } = this.props
    ReactNativeHapticFeedback.trigger('selection')

    if (onSelect(item)) {
      this.handleClose()
    }
  }

  handleClose = () => {
    const { onClose } = this.props
    Animated.timing(this.opacity, {
      toValue: 0,
      duration: ANIMATION_DURATION,
    }).start(onClose)
  }

  render = () => {
    const { data, renderItem, backgroundColor } = this.props
    return (
      <Animated.View style={[styles.container, { opacity: this.opacity }]}>
        <SafeAreaView style={styles.safeArea}>
          <TouchableOpacity
            onPress={this.handleClose}
            style={styles.touchableArea}
          >
            <View style={[styles.wrapper, { backgroundColor }]}>
              <FlatList
                contentContainerStyle={styles.list}
                renderItem={(info: ListRenderItemInfo<T>) => (
                  <TouchableHighlight
                    underlayColor={colors.primary}
                    onPress={() => this.handleSelect(info.item)}
                    style={styles.touchablerow}
                  >
                    {renderItem(info)}
                  </TouchableHighlight>
                )}
                ItemSeparatorComponent={() => <View style={styles.separator} />}
                data={data}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </TouchableOpacity>
        </SafeAreaView>
      </Animated.View>
    )
  }
}

export function showPickerOverlay<T>(props: OwnProps<T>) {
  const componentId = randomComponentId(ScreenNames.PickerOverlay)
  const navigationProps: NavigationProps = {
    onClose: () => Navigation.dismissOverlay(componentId),
  }
  Navigation.showOverlay({
    component: {
      name: ScreenNames.PickerOverlay,
      id: componentId,
      passProps: {
        ...navigationProps,
        ...props,
      },
    },
  })
}

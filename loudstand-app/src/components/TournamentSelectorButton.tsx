import React from 'react'
import { TournamentRoot } from '@loudstand/shared'
import { TouchableOpacity, Text, StyleSheet, Image, View } from 'react-native'
import { connect } from 'react-redux'
import { spacingUnit } from '../theme/layout'
import fonts from '../theme/fonts'
import { ReduxState } from '../store/model'
import { getImageUrl, ItemImageType, fallbackImages } from '../utils'
import RemoteImage from './RemoteImage'
import colors from '../theme/colors'

const LOGO_SIZE = 28
const logoCircleSize = LOGO_SIZE * 1.1

interface ReduxProps {
  activeTournament?: TournamentRoot
}

interface Props extends ReduxProps {
  onPress: () => void
  disabled: boolean
  tintColor?: string
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    // paddingHorizontal: 2 * spacingUnit,
  },
  nameText: {
    marginHorizontal: spacingUnit,
    ...(fonts.Header as object),
    flexShrink: 1,
    textAlign: 'center',
  },
  logoImage: {
    width: LOGO_SIZE,
    height: LOGO_SIZE,
    borderRadius: LOGO_SIZE / 2,
  },
  seasonLogoWrapper: {
    width: logoCircleSize,
    height: logoCircleSize,
    backgroundColor: colors.background,
    borderRadius: logoCircleSize / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export function TournamentSelectorButton({
  activeTournament,
  onPress,
  disabled,
  tintColor,
}: Props) {
  if (!activeTournament) {
    return <></>
  }
  const seasonLogoUrl = getImageUrl(
    activeTournament.currentSeason.id,
    ItemImageType.SeasonLogo
  )
  return (
    <TouchableOpacity
      onPress={onPress}
      style={styles.container}
      disabled={disabled}
    >
      <View style={styles.seasonLogoWrapper}>
        <RemoteImage
          source={{ uri: seasonLogoUrl }}
          style={styles.logoImage}
          defaultSource={fallbackImages.seasonLogo}
        />
      </View>
      <Text style={[styles.nameText, { color: tintColor }]}>
        {activeTournament.currentSeason.name}
      </Text>
      {!disabled && (
        <Image
          style={{ tintColor }}
          source={require('../../assets/icons/buttons/down-arrow2.png')}
        />
      )}
    </TouchableOpacity>
  )
}

TournamentSelectorButton.defaultProps = {
  disabled: false,
}

function mapStateToProps(state: ReduxState): ReduxProps {
  const { activeTournament } = state.general
  return {
    activeTournament,
  }
}

export default connect(mapStateToProps)(TournamentSelectorButton)

import React, { ReactNode } from 'react'
import Sentry from 'react-native-sentry'
import { showErrorOverlay } from './ErrorOverlay'

interface Props {
  children?: ReactNode
}

export default class ErrorBoundary extends React.Component<Props> {
  componentDidCatch(error: Error) {
    showErrorOverlay(error.message)
    Sentry.captureException(error)
  }

  render() {
    const { children } = this.props
    return children
  }
}

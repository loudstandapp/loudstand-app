import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { TournamentRoot, ID } from '@loudstand/shared'
import colors from '../theme/colors'
import fonts from '../theme/fonts'
import Flag from './Flag'
import RemoteImage from './RemoteImage'
import {
  getImageUrl,
  ItemImageType,
  fallbackImages,
  getComparatorForKey,
} from '../utils'

const LOGO_SIZE = 28
const logoCircleSize = LOGO_SIZE * 1.1

const styles = StyleSheet.create({
  seasonLogoWrapper: {
    width: logoCircleSize,
    height: logoCircleSize,
    backgroundColor: colors.background,
    borderRadius: logoCircleSize / 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pickerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  pickerText: {
    ...(fonts.Header as object),
    color: colors.secondaryText,
  },
  logoImage: {
    width: LOGO_SIZE,
    height: LOGO_SIZE,
    borderRadius: LOGO_SIZE / 2,
  },
})
interface Props {
  item: TournamentRoot
}

export default class TournamentPickerItem extends React.PureComponent<Props> {
  static backgroundColor = colors.navigationBackground

  render = () => {
    const { item } = this.props
    const imageUrl = getImageUrl(
      item.currentSeason.id,
      ItemImageType.SeasonLogo
    )

    return (
      <View style={styles.pickerRow}>
        <View style={styles.seasonLogoWrapper}>
          <RemoteImage
            style={styles.logoImage}
            source={{ uri: imageUrl }}
            defaultSource={fallbackImages.seasonLogo}
          />
        </View>

        <Text style={styles.pickerText}>{item.name}</Text>
        {item.country && <Flag countryCode={item.country} />}
      </View>
    )
  }
}

const tournamentIsDefaultComparator = getComparatorForKey<TournamentRoot>(
  'isDefaultForTeams',
  true
)
const tournamentCountryComparator = getComparatorForKey<TournamentRoot>(
  'country'
)
const tournamentIdComparator = getComparatorForKey<TournamentRoot>('id')

function tournamentComparator(a: TournamentRoot, b: TournamentRoot) {
  return (
    tournamentIsDefaultComparator(a, b) ||
    tournamentCountryComparator(a, b) ||
    tournamentIdComparator(a, b)
  )
}

export function tournamentListToArray(
  tournaments?: { [key in ID]: TournamentRoot }
) {
  if (!tournaments) {
    return []
  }

  const list = Object.keys(tournaments).reduce(
    (result: TournamentRoot[], id) => [...result, tournaments[id]],
    []
  )

  return list.sort(tournamentComparator)
}

import React from 'react'
import { Svg, Defs, Stop, LinearGradient, Circle } from 'react-native-svg'
import { StyleSheet, View, Text, Animated, Easing } from 'react-native'
import colors from '../theme/colors'
import fonts from '../theme/fonts'
import { spacingUnit } from '../theme/layout'

interface Props {
  percentage: number
  title?: string
  shouldAnimate?: boolean
  isVisible?: boolean
}

const RADIUS = 64
const INDICATOR_STROKE_WIDTH = 32
const INNER_CIRCLE_STROKE_WIDTH = 8
const OUTER_CIRCLE_STROKE_WIDTH = 16

// We only calculate this once in the lifetime of the code, not in every render.
const circleRadius = RADIUS - INDICATOR_STROKE_WIDTH / 2
const circumference = 2 * Math.PI * circleRadius
const buttonSize = 2 * RADIUS

const innerCircleRadius =
  circleRadius - INDICATOR_STROKE_WIDTH / 2 - INNER_CIRCLE_STROKE_WIDTH / 2

const outerCircleRadius =
  innerCircleRadius +
  INNER_CIRCLE_STROKE_WIDTH / 2 +
  OUTER_CIRCLE_STROKE_WIDTH / 2

const styles = StyleSheet.create({
  container: {
    margin: 2 * spacingUnit,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    ...(fonts.HeaderSmall as object),
    marginBottom: 2 * spacingUnit,
    textAlign: 'center',
  },
  circle: {
    ...StyleSheet.absoluteFillObject,
  },
  circleContainer: {
    width: RADIUS * 2,
    height: RADIUS * 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    ...(fonts.Header as object),
    color: colors.primaryText,
  },
})

const AnimatedCircle = Animated.createAnimatedComponent(Circle)

export default class PercentageCircle extends React.PureComponent<Props> {
  animatedValue = new Animated.Value(0)

  componentDidMount = () => {
    this.animateIfNeeded(false)
  }

  componentDidUpdate(prevProps: Props) {
    this.animateIfNeeded(!!prevProps.isVisible, prevProps.percentage)
  }

  animateToValue = () => {
    const { percentage } = this.props

    Animated.timing(this.animatedValue, {
      toValue: percentage,
      easing: Easing.elastic(1),
      duration: 1000,
    }).start()
  }

  animateIfNeeded = (wasVisible: boolean, prevPercentage?: number) => {
    const { isVisible, shouldAnimate, percentage } = this.props
    if (
      isVisible &&
      prevPercentage !== undefined &&
      prevPercentage !== percentage
    ) {
      this.animateToValue()
      return
    }
    if (!shouldAnimate) {
      return
    }
    if (isVisible && !wasVisible) {
      this.animateToValue()
    }
    if (!isVisible && wasVisible) {
      this.animatedValue.setValue(0)
    }
  }

  render = () => {
    const { percentage, title, shouldAnimate } = this.props
    return (
      <View style={styles.container}>
        {title && <Text style={styles.title}>{title}</Text>}
        <View style={styles.circleContainer}>
          <Text numberOfLines={0} style={styles.text}>{`${percentage}%`}</Text>
          <Svg width={buttonSize} height={buttonSize} style={styles.circle}>
            <Defs>
              <LinearGradient
                id="indicator"
                x1="0%"
                y1="0%"
                x2="100%"
                y2="100%"
              >
                <Stop
                  offset="0%"
                  stopColor={colors.primary}
                  stopOpacity="0.8"
                />
                <Stop
                  offset="100%"
                  stopColor={colors.primaryAlt}
                  stopOpacity="0.8"
                />
              </LinearGradient>
              <LinearGradient id="circle" x1="0%" y1="0%" x2="100%" y2="100%">
                <Stop
                  offset="0%"
                  stopColor={colors.tertiary}
                  stopOpacity="0.4"
                />
                <Stop
                  offset="100%"
                  stopColor={colors.tertiaryAlt}
                  stopOpacity="0.4"
                />
              </LinearGradient>
            </Defs>
            <Circle
              r={innerCircleRadius}
              cx={RADIUS}
              cy={RADIUS}
              fill="transparent"
              stroke={colors.tertiaryLight}
              strokeWidth={INNER_CIRCLE_STROKE_WIDTH}
            />
            <Circle
              r={outerCircleRadius}
              cx={RADIUS}
              cy={RADIUS}
              fill="transparent"
              stroke="url(#circle)"
              strokeWidth={OUTER_CIRCLE_STROKE_WIDTH}
            />
            {shouldAnimate ? (
              <AnimatedCircle
                r={circleRadius}
                cx={RADIUS}
                cy={RADIUS}
                fill="transparent"
                strokeDasharray={circumference.toString()}
                strokeDashoffset={this.animatedValue.interpolate({
                  inputRange: [0, 100],
                  outputRange: [circumference, 0],
                })}
                stroke="url(#indicator)"
                strokeWidth={INDICATOR_STROKE_WIDTH}
                transform={`rotate(-90, ${RADIUS}, ${RADIUS})`}
              />
            ) : (
              <Circle
                r={circleRadius}
                cx={RADIUS}
                cy={RADIUS}
                fill="transparent"
                strokeDasharray={circumference.toString()}
                strokeDashoffset={(1 - percentage / 100) * circumference}
                stroke="url(#indicator)"
                strokeWidth={INDICATOR_STROKE_WIDTH}
                transform={`rotate(-90, ${RADIUS}, ${RADIUS})`}
              />
            )}
          </Svg>
        </View>
      </View>
    )
  }
}

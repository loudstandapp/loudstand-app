import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { PlayerPosition } from '@loudstand/shared'
import { action } from '@storybook/addon-actions'
import PlayerListItem from './PlayerListItem'

storiesOf('PlayerListItem', module)
  .add('default', () => (
    <PlayerListItem
      player={{
        id: 'sr:player:253515',
        country: 'DE',
        number: 40,
        name: 'Eric Oelschlagel',
        position: PlayerPosition.Goalkeeper,
      }}
      onSelect={action('onSelect')}
    />
  ))
  .add('no country', () => (
    <PlayerListItem
      player={{
        id: 'sr:player:253515',
        country: null,
        number: 40,
        name: 'Eric Oelschlagel',
        position: PlayerPosition.Goalkeeper,
      }}
      onSelect={action('onSelect')}
    />
  ))

import React, { ReactElement, Key } from 'react'
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Dimensions,
  FlatList,
  NativeSyntheticEvent,
  NativeScrollEvent,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import ReactNativeHapticFeedback from 'react-native-haptic-feedback'
import colors from '../theme/colors'
import { spacingUnit, buttonHeight } from '../theme/layout'
import fonts from '../theme/fonts'

interface Props {
  titles?: string[]
  onSnapToSlide?: (key: Key) => void
}

interface State {
  activeIndex: number
}

const DOT_SIZE = spacingUnit
const { width } = Dimensions.get('screen')

export const largeInset = buttonHeight + 2 * spacingUnit
export const smallInset = DOT_SIZE + 2 * spacingUnit

const styles = StyleSheet.create({
  indicatorDot: {
    height: DOT_SIZE,
    width: DOT_SIZE,
    borderRadius: DOT_SIZE / 2,
    marginHorizontal: spacingUnit,
    backgroundColor: colors.tertiaryLight,
  },
  activeIndicatorDot: {},
  indicatorButton: {
    height: buttonHeight,
    minWidth: buttonHeight * 2,
  },
  indicatorButtonWrapper: {
    borderRadius: buttonHeight / 2,
    justifyContent: 'center',
    flex: 1,
    backgroundColor: colors.tertiaryLight,
  },
  indicatorButtonText: {
    textAlign: 'center',
    color: colors.primaryText,
    paddingHorizontal: 2 * spacingUnit,
  },
  activeIndicatorButtonText: {
    color: colors.secondaryText,
  },
  buttonIndicatorsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: colors.navigationBackground,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  dotIndicatorsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  swiper: {
    flex: 1,
  },
  slide: {
    width,
    flex: 1,
  },
  indicatorsContainer: {
    paddingVertical: spacingUnit,
    paddingHorizontal: spacingUnit,
    flex: 0,
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
  },
  container: {
    flex: 1,
  },
})

const gradientColorsActive = [colors.primary, colors.primaryAlt]

export default class SwiperContainer extends React.Component<Props, State> {
  state = { activeIndex: 0 }

  carouselRef = React.createRef<FlatList<React.ReactNode>>()

  isDragging = false

  handleSelect = (activeIndex: number) => {
    ReactNativeHapticFeedback.trigger('selection')
    this.setState({ activeIndex })
    this.carouselRef.current &&
      this.carouselRef.current.scrollToIndex({
        index: activeIndex,
        animated: true,
      })
  }

  handleMomentumScrollEnd = (
    event: NativeSyntheticEvent<NativeScrollEvent>
  ) => {
    const { onSnapToSlide, children } = this.props
    const activeIndex = Math.round(event.nativeEvent.contentOffset.x / width)
    this.setState({ activeIndex })
    if (Array.isArray(children)) {
      const slide = children[activeIndex] as ReactElement
      onSnapToSlide && slide && slide.key && onSnapToSlide(slide.key)
    }
    ReactNativeHapticFeedback.trigger('selection')
  }

  reset = () => {
    this.carouselRef.current &&
      this.carouselRef.current.scrollToIndex({ index: 0, animated: true })
  }

  handleScrollDragEnd = () => {
    this.isDragging = false
  }

  handleScrollDragBegin = () => {
    this.isDragging = true
  }

  handleScroll = (event: NativeSyntheticEvent<NativeScrollEvent>) => {
    if (!this.isDragging) {
      return
    }
    const { activeIndex } = this.state
    const newIndex = Math.round(event.nativeEvent.contentOffset.x / width)
    if (newIndex !== activeIndex) {
      this.setState({ activeIndex: newIndex })
    }
  }

  renderIndicatorItem = (index: number) => {
    const { titles } = this.props
    const { activeIndex } = this.state
    const isActive = activeIndex === index
    if (!titles || titles.length === 0) {
      return <IndicatorDot isActive={isActive} key={index} />
    }
    return (
      <IndicatorButton
        title={titles[index] || ''}
        index={index}
        isActive={isActive}
        onPress={this.handleSelect}
        key={index}
      />
    )
  }

  render = () => {
    const { children, titles } = this.props
    if (!Array.isArray(children)) {
      return false
    }
    const filteredChildren = children.filter(child => !!child)
    if (filteredChildren.length === 0) {
      return false
    }
    if (filteredChildren.length === 1) {
      return filteredChildren[0]
    }
    return (
      <View style={styles.container}>
        <FlatList
          style={styles.swiper}
          data={filteredChildren}
          renderItem={({ item, index }) => (
            <View style={styles.slide} key={index}>
              {item}
            </View>
          )}
          onMomentumScrollEnd={this.handleMomentumScrollEnd}
          decelerationRate="fast"
          onScroll={this.handleScroll}
          scrollEventThrottle={33} // 30 FPS
          onScrollEndDrag={this.handleScrollDragEnd}
          onScrollBeginDrag={this.handleScrollDragBegin}
          snapToInterval={width}
          ref={this.carouselRef}
          getItemLayout={(data: any, index: number) => ({
            length: width,
            offset: width * index,
            index,
          })}
          keyExtractor={(item, index) => index.toString()}
          horizontal
          showsHorizontalScrollIndicator={false}
        />
        <View
          style={[
            styles.indicatorsContainer,
            titles && titles.length > 0
              ? styles.buttonIndicatorsContainer
              : styles.dotIndicatorsContainer,
          ]}
        >
          {filteredChildren.map((item, index) =>
            this.renderIndicatorItem(index)
          )}
        </View>
      </View>
    )
  }
}

interface IndicatorDotProps {
  isActive: boolean
}
class IndicatorDot extends React.PureComponent<IndicatorDotProps> {
  render = () => {
    const { isActive } = this.props

    return isActive ? (
      <LinearGradient
        style={[styles.indicatorDot, styles.activeIndicatorDot]}
        colors={gradientColorsActive}
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 0 }}
      />
    ) : (
      <View style={styles.indicatorDot} />
    )
  }
}

interface IndicatorButtonProps {
  isActive: boolean
  index: number
  title: string
  onPress: (index: number) => void
}
class IndicatorButton extends React.PureComponent<IndicatorButtonProps> {
  handleSelect = () => {
    const { index, onPress } = this.props
    onPress(index)
  }

  render = () => {
    const { isActive, title } = this.props

    return isActive ? (
      <TouchableOpacity
        onPress={this.handleSelect}
        style={[styles.indicatorButton]}
      >
        <LinearGradient
          colors={gradientColorsActive}
          style={[styles.indicatorButtonWrapper]}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
        >
          <Text
            style={[
              fonts.HeaderSmall,
              styles.indicatorButtonText,
              isActive && styles.activeIndicatorButtonText,
            ]}
          >
            {title}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    ) : (
      <TouchableOpacity
        onPress={this.handleSelect}
        style={[styles.indicatorButton]}
      >
        <View style={[styles.indicatorButtonWrapper]}>
          <Text style={[fonts.HeaderSmall, styles.indicatorButtonText]}>
            {title}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
}

import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import { CurrentUserIndicator } from './CurrentUserIndicator'

storiesOf('CurrentUserIndicator', module)
  .add('default', () => (
    <CurrentUserIndicator
      auth={{
        displayName: 'Esben von Buchwald',
        photoURL:
          'https://mymodernmet.com/wp/wp-content/uploads/2018/10/Mou-Aysha-portrait-photography-3.jpg',
      }}
      onLogout={action('onLogout')}
    />
  ))
  .add('no photo', () => (
    <CurrentUserIndicator
      auth={{
        displayName: 'Esben von Buchwald',
      }}
      onLogout={action('onLogout')}
    />
  ))
  .add('long name', () => (
    <CurrentUserIndicator
      auth={{
        displayName: 'Esben von Buchwald long long long long name',
        photoURL:
          'https://mymodernmet.com/wp/wp-content/uploads/2018/10/Mou-Aysha-portrait-photography-3.jpg',
      }}
      onLogout={action('onLogout')}
    />
  ))

import React from 'react'
import {
  ImageRequireSource,
  View,
  StyleProp,
  ImageStyle,
  ActivityIndicator,
  StyleSheet,
  LayoutChangeEvent,
} from 'react-native'
import FastImage, { FastImageSource } from 'react-native-fast-image'

enum Status {
  Initial,
  Loading,
  Done,
  Failed,
}

interface State {
  status: Status
  imageWidth: number
  imageHeight: number
}

interface Props {
  source: FastImageSource
  defaultSource?: ImageRequireSource
  style: StyleProp<ImageStyle>
}

const styles = StyleSheet.create({
  activityIndicator: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {},
  image: {},
})

export default class RemoteImage extends React.PureComponent<Props, State> {
  state = { status: Status.Initial, imageWidth: 0, imageHeight: 0 }

  handleStart = () =>
    this.setState(prevState =>
      prevState.status !== Status.Failed ? { status: Status.Loading } : null
    )

  handleFinish = () =>
    this.setState(prevState =>
      prevState.status !== Status.Failed ? { status: Status.Done } : null
    )

  handleError = () => this.setState({ status: Status.Failed })

  handleLayoutChange = (event: LayoutChangeEvent) => {
    const { width, height } = event.nativeEvent.layout
    this.setState({ imageWidth: width, imageHeight: height })
  }

  render = () => {
    const { source, defaultSource, style } = this.props
    const { status, imageWidth, imageHeight } = this.state
    const imageSource =
      status !== Status.Failed || !defaultSource ? source : defaultSource
    const {
      width,
      height,
      position,
      top,
      left,
      bottom,
      right,
      ...rest
    } = StyleSheet.flatten(style)
    return (
      <View
        onLayout={this.handleLayoutChange}
        style={[
          styles.container,
          {
            width,
            height,
            position,
            top,
            left,
            bottom,
            right,
          },
        ]}
      >
        {status === Status.Loading && (
          <ActivityIndicator size="small" style={styles.activityIndicator} />
        )}
        <FastImage
          source={imageSource}
          onLoad={this.handleFinish}
          onError={this.handleError}
          onLoadStart={this.handleStart}
          style={[
            styles.image,
            { width: imageWidth, height: imageHeight },
            rest,
          ]}
        />
      </View>
    )
  }
}

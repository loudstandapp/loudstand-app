import React from 'react'
import LinearGradient from 'react-native-linear-gradient'
import { ActivityIndicator, View, StyleSheet } from 'react-native'
import colors from '../theme/colors'
import ActivityIndicatorLine from './ActivityIndicatorLine'

interface Props {
  isLoading?: boolean
  isGenerating?: boolean
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loading: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  generating: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  activityIndicator: {
    transform: [{ scale: 3 }],
  },
})

export default class ScreenWrapper extends React.PureComponent<Props> {
  render = () => {
    const { isLoading, children, isGenerating } = this.props
    return (
      <LinearGradient
        colors={[colors.background, colors.backgroundAlt]}
        style={styles.container}
      >
        {children}
        {isLoading && (
          <View style={styles.loading} pointerEvents="none">
            <ActivityIndicator
              size="large"
              style={styles.activityIndicator}
              color={colors.secondary25pct}
            />
          </View>
        )}
        {isGenerating && (
          <View style={styles.generating} pointerEvents="none">
            <ActivityIndicatorLine
              indicatorColor={colors.primaryLight}
              backgroundColor={colors.secondary25pct}
            />
          </View>
        )}
      </LinearGradient>
    )
  }
}

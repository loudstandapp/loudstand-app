import React from 'react'
import { TeamPlayer, ID, JerseyColors } from '@loudstand/shared'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'
import Flag from './Flag'
import fonts from '../theme/fonts'
import { spacingUnit, marginDefault } from '../theme/layout'
import PlayerHeadshot from './PlayerHeadshot'

interface Props {
  player: TeamPlayer
  onSelect: (id: ID) => void
  jerseyColors?: JerseyColors | null
}

const styles = StyleSheet.create({
  playerListImage: {
    marginRight: spacingUnit,
  },
  playerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: spacingUnit,
    marginHorizontal: marginDefault,
  },
  playerName: {
    flex: 3,
  },
  playerNumber: {
    flexShrink: 1,
    flexGrow: 0,
    textAlign: 'right',
  },
  flag: {
    margin: spacingUnit,
  },
})

export default class PlayerListItem extends React.PureComponent<Props> {
  handleSelect = () => {
    const { player, onSelect } = this.props
    onSelect(player.id)
  }

  render = () => {
    const { player, jerseyColors } = this.props
    return (
      <TouchableOpacity style={styles.playerRow} onPress={this.handleSelect}>
        <PlayerHeadshot
          player={player}
          jerseyColors={jerseyColors}
          style={styles.playerListImage}
          size="large"
        />
        {player.country && (
          <Flag countryCode={player.country} style={styles.flag} />
        )}
        <Text style={[styles.playerName, fonts.Body]}>{player.name}</Text>
        <Text style={[styles.playerNumber, fonts.BodyStrong]}>
          {player.number}
        </Text>
      </TouchableOpacity>
    )
  }
}

import React from 'react'
import { Svg, Defs, Stop, LinearGradient, Circle, Line } from 'react-native-svg'
import { StyleSheet, View, Text, Animated, Easing } from 'react-native'
import colors from '../theme/colors'
import fonts from '../theme/fonts'
import { spacingUnit } from '../theme/layout'

interface Props {
  percentage: number
  title?: string
  shouldAnimate?: boolean
  isVisible?: boolean
}

const RADIUS = 64
const INDICATOR_STROKE_WIDTH = 40
const INNER_CIRCLE_STROKE_WIDTH = 8
const LINE_STROKE_WIDTH = 2
const LINE_OVERFLOW = 4
const center = RADIUS + LINE_OVERFLOW

// We only calculate this once in the lifetime of the code, not in every render.
const circleRadius = RADIUS - INDICATOR_STROKE_WIDTH / 2
const circumference = 2 * Math.PI * circleRadius
const buttonSize = 2 * center

const innerCircleRadius =
  circleRadius - INDICATOR_STROKE_WIDTH / 2 + INNER_CIRCLE_STROKE_WIDTH / 2

const styles = StyleSheet.create({
  container: {
    margin: 2 * spacingUnit,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    ...(fonts.HeaderSmall as object),
    marginBottom: spacingUnit,
    textAlign: 'center',
  },
  circle: {
    ...StyleSheet.absoluteFillObject,
  },
  circleContainer: {
    width: center * 2,
    height: center * 2,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    ...(fonts.Header as object),
    color: colors.primaryText,
  },
})

const AnimatedCircle = Animated.createAnimatedComponent(Circle)
const AnimateLine = Animated.createAnimatedComponent(Line)

export default class PercentageCircleWithLines extends React.PureComponent<
  Props
> {
  animatedValue = new Animated.Value(0)

  animatedLineValue = new Animated.Value(0)

  componentDidMount = () => {
    this.animateIfNeeded(false)
  }

  componentDidUpdate(prevProps: Props) {
    this.animateIfNeeded(!!prevProps.isVisible, prevProps.percentage)
  }

  animateToValue = () => {
    this.animatedLineValue.setValue(0)
    const { percentage } = this.props

    Animated.parallel([
      Animated.timing(this.animatedValue, {
        toValue: percentage,
        easing: Easing.elastic(1),
        duration: 1000,
      }),
      Animated.timing(this.animatedLineValue, {
        toValue: 1,
        delay: 750,
        duration: 250,
      }),
    ]).start()
  }

  animateIfNeeded = (wasVisible: boolean, prevPercentage?: number) => {
    const { isVisible, shouldAnimate, percentage } = this.props
    if (
      isVisible &&
      prevPercentage !== undefined &&
      prevPercentage !== percentage
    ) {
      this.animateToValue()
      return
    }
    if (!shouldAnimate) {
      return
    }
    if (isVisible && !wasVisible) {
      this.animateToValue()
    }
    if (!isVisible && wasVisible) {
      this.animatedValue.setValue(0)
      this.animatedLineValue.setValue(0)
    }
  }

  render = () => {
    const { percentage, title, shouldAnimate } = this.props
    return (
      <View style={styles.container}>
        {title && (
          <Text numberOfLines={2} style={styles.title}>
            {title}
          </Text>
        )}
        <View style={styles.circleContainer}>
          <Text style={styles.text}>{`${percentage}%`}</Text>
          <Svg width={buttonSize} height={buttonSize} style={styles.circle}>
            <Defs>
              <LinearGradient
                id="indicator"
                x1="0%"
                y1="0%"
                x2="100%"
                y2="100%"
              >
                <Stop
                  offset="0%"
                  stopColor={colors.primary}
                  stopOpacity="0.8"
                />
                <Stop
                  offset="33%"
                  stopColor={colors.primaryAlt}
                  stopOpacity="0.8"
                />
                <Stop
                  offset="66%"
                  stopColor={colors.primary}
                  stopOpacity="0.8"
                />
                <Stop
                  offset="100%"
                  stopColor={colors.primaryAlt}
                  stopOpacity="0.8"
                />
              </LinearGradient>
              <LinearGradient id="circle" x1="0%" y1="0%" x2="100%" y2="100%">
                <Stop
                  offset="0%"
                  stopColor={colors.secondary}
                  stopOpacity="0.1"
                />
                <Stop
                  offset="100%"
                  stopColor={colors.secondary}
                  stopOpacity="0.3"
                />
              </LinearGradient>
              <LinearGradient
                id="innerCircle"
                x1="0%"
                y1="0%"
                x2="100%"
                y2="100%"
              >
                <Stop
                  offset="0%"
                  stopColor={colors.primary}
                  stopOpacity="0.6"
                />
                <Stop
                  offset="50%"
                  stopColor={colors.primaryAlt}
                  stopOpacity="0.6"
                />
                <Stop
                  offset="100%"
                  stopColor={colors.primary}
                  stopOpacity="0.6"
                />
              </LinearGradient>
            </Defs>
            <Circle
              r={innerCircleRadius}
              cx={center}
              cy={center}
              fill="transparent"
              stroke="url(#innerCircle)"
              strokeWidth={INNER_CIRCLE_STROKE_WIDTH}
            />
            <Circle
              r={circleRadius}
              cx={center}
              cy={center}
              fill="transparent"
              stroke="url(#circle)"
              strokeWidth={INDICATOR_STROKE_WIDTH}
            />
            {shouldAnimate ? (
              <AnimatedCircle
                r={circleRadius}
                cx={center}
                cy={center}
                fill="transparent"
                strokeDasharray={circumference.toString()}
                strokeDashoffset={this.animatedValue.interpolate({
                  inputRange: [0, 100],
                  outputRange: [circumference, 0],
                })}
                stroke="url(#indicator)"
                strokeWidth={INDICATOR_STROKE_WIDTH}
                transform={`rotate(-90, ${center}, ${center})`}
              />
            ) : (
              <Circle
                r={circleRadius}
                cx={center}
                cy={center}
                fill="transparent"
                strokeDasharray={circumference.toString()}
                strokeDashoffset={(1 - percentage / 100) * circumference}
                stroke="url(#indicator)"
                strokeWidth={INDICATOR_STROKE_WIDTH}
                transform={`rotate(-90, ${center}, ${center})`}
              />
            )}

            <Line
              strokeWidth={LINE_STROKE_WIDTH}
              stroke="url(#indicator)"
              x1={center}
              x2={center}
              y1={0}
              y2={INDICATOR_STROKE_WIDTH + 2 * LINE_OVERFLOW}
            />
            {shouldAnimate ? (
              <AnimateLine
                strokeWidth={LINE_STROKE_WIDTH}
                stroke="url(#indicator)"
                x1={center}
                x2={center}
                y1={0}
                y2={INDICATOR_STROKE_WIDTH + 2 * LINE_OVERFLOW}
                transform={`rotate(${percentage * 3.6}, ${center}, ${center})`}
                opacity={this.animatedLineValue}
              />
            ) : (
              <Line
                strokeWidth={LINE_STROKE_WIDTH}
                stroke="url(#indicator)"
                x1={center}
                x2={center}
                y1={0}
                y2={INDICATOR_STROKE_WIDTH + 2 * LINE_OVERFLOW}
                transform={`rotate(${percentage * 3.6}, ${center}, ${center})`}
              />
            )}
          </Svg>
        </View>
      </View>
    )
  }
}

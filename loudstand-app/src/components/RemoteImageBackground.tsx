import React from 'react'
import {
  ImageRequireSource,
  StyleProp,
  ActivityIndicator,
  StyleSheet,
  ViewStyle,
} from 'react-native'
import FastImage, { FastImageSource } from 'react-native-fast-image'

enum Status {
  Initial,
  Loading,
  Done,
  Failed,
}

interface State {
  status: Status
}

interface Props {
  source: FastImageSource
  defaultSource?: ImageRequireSource
  style?: StyleProp<ViewStyle>
}

const styles = StyleSheet.create({
  activityIndicator: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
    justifyContent: 'center',
  },
})

export default class RemoteImageBackground extends React.PureComponent<
  Props,
  State
> {
  state = { status: Status.Initial }

  handleStart = () =>
    this.setState(prevState =>
      prevState.status !== Status.Failed ? { status: Status.Loading } : null
    )

  handleFinish = () =>
    this.setState(prevState =>
      prevState.status !== Status.Failed ? { status: Status.Done } : null
    )

  handleError = () => this.setState({ status: Status.Failed })

  render = () => {
    const { source, defaultSource, style, children } = this.props
    const { status } = this.state
    const imageSource =
      status !== Status.Failed || !defaultSource ? source : defaultSource

    return (
      <FastImage
        source={imageSource}
        onLoad={this.handleFinish}
        onError={this.handleError}
        onLoadStart={this.handleStart}
        style={style}
      >
        {status === Status.Loading && (
          <ActivityIndicator size="large" style={styles.activityIndicator} />
        )}
        {children}
      </FastImage>
    )
  }
}

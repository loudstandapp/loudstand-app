import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { PlayerPosition } from '@loudstand/shared'
import PlayerPickerItem from './PlayerPickerItem'

storiesOf('PlayerPickerItem', module).add('default', () => (
  <PlayerPickerItem
    item={{
      number: 13,
      country: null,
      id: 'sr:player:355352',
      name: 'Alex Telles',
      position: PlayerPosition.Defender,
    }}
  />
))

import React from 'react'
import { TouchableOpacity, Image, StyleSheet } from 'react-native'
import { touchableSize, spacingUnit } from '../theme/layout'

interface Props {
  onPress: () => void
  tintColor: string
}

const styles = StyleSheet.create({
  button: {
    width: touchableSize,
    height: touchableSize,
    justifyContent: 'center',
    alignItems: 'flex-start',
    paddingLeft: spacingUnit,
  },
})

export default class BackButton extends React.PureComponent<Props> {
  render = () => {
    const { onPress, tintColor } = this.props
    return (
      <TouchableOpacity onPress={onPress} style={styles.button}>
        <Image
          style={{ tintColor }}
          source={require('../../assets/icons/buttons/back-arrow.png')}
        />
      </TouchableOpacity>
    )
  }
}

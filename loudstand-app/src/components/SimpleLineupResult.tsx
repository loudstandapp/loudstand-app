import React from 'react'
import {
  LineupSuggestionResultDocument,
  PlayerList,
  PlayerPosition,
  JerseyColors,
  ID,
  TeamPlayer,
} from '@loudstand/shared'
import { StyleSheet, View, Text } from 'react-native'
import { spacingUnit, marginDefault } from '../theme/layout'
import colors from '../theme/colors'
import SimpleLineupPlayer from './SimpleLineupPlayer'
import {
  constraints,
  REQUIRED_NUMBER_OF_PLAYERS,
} from '../features/match/components/LineupPicker'
import { getComparatorForKey } from '../utils'
import ChosenIndicator from '../features/match/components/ChosenIndicator'
import InfoButton from './InfoButton'
import { InfoType } from './Info'
import fonts from '../theme/fonts'

export interface TeamPlayerShare {
  share?: number
  player: TeamPlayer
}

interface Props {
  lineupSuggestionResult?: LineupSuggestionResultDocument
  teamPlayers: PlayerList
  jerseyColors?: JerseyColors
  chosen?: ID[]
  title?: string
  onSelectPlayer: (id: ID) => void
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.tertiaryLight,
    marginLeft: marginDefault,
    marginRight: marginDefault,
    marginVertical: spacingUnit,
    borderRadius: spacingUnit / 2,
  },
  contentWrapper: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    margin: marginDefault,
  },
  fieldContainer: {
    justifyContent: 'space-between',
    flex: 1,
  },
  fieldContainerImage: {
    tintColor: colors.tertiaryLight,
    borderWidth: 3,
    borderColor: colors.tertiaryLight,
  },
  headerWrapper: {
    backgroundColor: colors.secondary,
    marginTop: marginDefault,
    marginHorizontal: -marginDefault,
    paddingHorizontal: 2 * marginDefault,
    paddingVertical: spacingUnit,
  },
  headerText: {
    ...(fonts.Body as object),
    color: colors.secondaryText,
  },
  headerMainText: {
    ...(fonts.BodyStrong as object),
    color: colors.secondaryText,
  },
  infoButtonIcon: {
    tintColor: colors.secondaryText,
  },
  infoButton: {
    bottom: 0,
    height: undefined,
    width: undefined,
    justifyContent: 'center',
    marginRight: 2 * marginDefault,
  },
})

function generatePlayerShares(playersWithShares: TeamPlayerShare[]) {
  const mostPopularPlayers = playersWithShares.slice(
    0,
    REQUIRED_NUMBER_OF_PLAYERS
  )
  const remainingPlayers = playersWithShares.slice(REQUIRED_NUMBER_OF_PLAYERS)
  const playerShares: { [key in PlayerPosition]: TeamPlayerShare[] } = {
    [PlayerPosition.Goalkeeper]: [],
    [PlayerPosition.Defender]: [],
    [PlayerPosition.Midfielder]: [],
    [PlayerPosition.Forward]: [],
  }

  const positions = [
    PlayerPosition.Goalkeeper,
    PlayerPosition.Forward,
    PlayerPosition.Midfielder,
    PlayerPosition.Defender,
  ]

  let playerCount = 0
  let remainingMax = positions.reduce(
    (total, position) => total + constraints[position].max,
    0
  )

  positions.forEach((position: PlayerPosition, index: number) => {
    const { min, max } = constraints[position]
    const remainingPlayerCount = REQUIRED_NUMBER_OF_PLAYERS - playerCount

    // Fill up remaining spots with the last position in list
    if (index === positions.length - 1) {
      const list = playersWithShares
        .filter(({ player }) => player.position === position)
        .slice(0, REQUIRED_NUMBER_OF_PLAYERS - playerCount)
      playerCount += list.length
      playerShares[position] = list
      return
    }
    const mostPopularOptions = mostPopularPlayers
      .filter(({ player }) => player.position === position)
      .slice(0, max)

    const current = mostPopularOptions.length
    const nextRemainingMax = remainingMax - max
    const totalMissing = Math.max(
      0,
      remainingPlayerCount - current - nextRemainingMax
    )
    const thisTypeMissing = Math.max(0, min - current)
    const playersToAdd = Math.max(totalMissing, thisTypeMissing)

    const remainingOptions = remainingPlayers
      .filter(({ player }) => player.position === position)
      .slice(0, playersToAdd)

    const list = [...mostPopularOptions, ...remainingOptions]
    playerCount += list.length
    remainingMax -= max
    playerShares[position] = list
  })

  return playerShares
}

export default class SimpleLineupResult extends React.PureComponent<Props> {
  render = () => {
    const {
      lineupSuggestionResult,
      teamPlayers,
      jerseyColors,
      chosen,
      onSelectPlayer,
      title,
    } = this.props

    function getPlayerShares() {
      if (!lineupSuggestionResult && chosen) {
        const playersWithoutShares = chosen
          .map(playerId => ({
            player: teamPlayers[playerId],
            share: undefined,
          }))
          .sort(getComparatorForKey('share', true))
        return generatePlayerShares(playersWithoutShares, teamPlayers)
      }
      if (!lineupSuggestionResult) {
        throw new Error(
          'You must Specify lineupSuggestionResult, chosen or both'
        )
      }
      const playersWithShares = Object.keys(teamPlayers)
        .map(playerId => ({
          player: teamPlayers[playerId],
          share: lineupSuggestionResult.players[playerId] || 0,
        }))
        .sort(getComparatorForKey('share', true))
      return generatePlayerShares(playersWithShares, teamPlayers)
    }

    const playerShares = getPlayerShares()

    const allPlayersAndShares = [
      ...playerShares.GOALKEEPER,
      ...playerShares.DEFENDER,
      ...playerShares.MIDFIELDER,
      ...playerShares.FORWARD,
    ]

    const chosenPlayers =
      chosen &&
      chosen
        .map((id: ID) => (teamPlayers[id] ? teamPlayers[id].name : ''))
        .join(', ')

    return (
      <View style={styles.container}>
        <View style={styles.headerWrapper}>
          <Text style={styles.headerText}>
            <Text style={styles.headerMainText}>{title}</Text>
          </Text>
          <InfoButton
            type={InfoType.Lineup}
            iconStyle={styles.infoButtonIcon}
            style={styles.infoButton}
          />
        </View>
        <View style={styles.contentWrapper}>
          {allPlayersAndShares.map(({ player, share }) => (
            <SimpleLineupPlayer
              key={player.id}
              jerseyColors={jerseyColors}
              player={player}
              share={share}
              onSelectPlayer={onSelectPlayer}
            />
          ))}
          {chosenPlayers && lineupSuggestionResult && (
            <ChosenIndicator value={chosenPlayers} />
          )}
        </View>
      </View>
    )
  }
}

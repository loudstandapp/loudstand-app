import React from 'react'
import { storiesOf } from '@storybook/react-native'
import { action } from '@storybook/addon-actions'
import ProfileNavigationHeader from './ProfileNavigationHeader'

storiesOf('ProfileNavigationHeader', module)
  .add('default', () => (
    <ProfileNavigationHeader
      backgroundSource={{
        uri:
          'https://thumbs.dreamstime.com/z/soccer-football-action-shot-8825994.jpg',
      }}
      defaultBackgroundSource={require('../../assets/images/fallback/stadium.jpg')}
      onBack={action('onBack')}
      title="Some title bla bla"
      subTitle="Some subtitle"
    />
  ))
  .add('no subtitle', () => (
    <ProfileNavigationHeader
      backgroundSource={{
        uri:
          'https://thumbs.dreamstime.com/z/soccer-football-action-shot-8825994.jpg',
      }}
      defaultBackgroundSource={require('../../assets/images/fallback/stadium.jpg')}
      onBack={action('onBack')}
      title="Some title bla bla"
    />
  ))
  .add('long title', () => (
    <ProfileNavigationHeader
      backgroundSource={{
        uri:
          'https://thumbs.dreamstime.com/z/soccer-football-action-shot-8825994.jpg',
      }}
      defaultBackgroundSource={require('../../assets/images/fallback/stadium.jpg')}
      onBack={action('onBack')}
      title="Some title bla bla it is very long yes it is indeed! blablabla"
      subTitle="Some subtitle"
    />
  ))
  .add('with settings', () => (
    <ProfileNavigationHeader
      backgroundSource={{
        uri:
          'https://thumbs.dreamstime.com/z/soccer-football-action-shot-8825994.jpg',
      }}
      defaultBackgroundSource={require('../../assets/images/fallback/stadium.jpg')}
      onBack={action('onBack')}
      title="Some title bla bla"
      onSelectSettings={action('onSelectSettings')}
    />
  ))
  .add('with team', () => (
    <ProfileNavigationHeader
      backgroundSource={{
        uri:
          'https://thumbs.dreamstime.com/z/soccer-football-action-shot-8825994.jpg',
      }}
      defaultBackgroundSource={require('../../assets/images/fallback/stadium.jpg')}
      onBack={action('onBack')}
      title="Some title bla bla"
      onSelectSettings={action('onSelectSettings')}
      team={{
        country: 'PT',
        id: 'sr:competitor:3006',
        name: 'Benfica Lisbon',
      }}
    />
  ))
  .add('with selectable team', () => (
    <ProfileNavigationHeader
      backgroundSource={{
        uri:
          'https://thumbs.dreamstime.com/z/soccer-football-action-shot-8825994.jpg',
      }}
      defaultBackgroundSource={require('../../assets/images/fallback/stadium.jpg')}
      onBack={action('onBack')}
      title="Some title bla bla"
      onSelectSettings={action('onSelectSettings')}
      onSelectTeam={action('onSelectTeam')}
      team={{
        country: 'PT',
        id: 'sr:competitor:3006',
        name: 'Benfica Lisbon',
      }}
    />
  ))

/* eslint-disable no-shadow,react/display-name */
import React from 'react'

import { Navigation } from 'react-native-navigation'
import { Store } from 'redux'
import { Provider } from 'react-redux'
import HomeScreen from './features/main/screens/HomeScreen'
import FansListScreen from './features/user/screens/FansListScreen'
import StatsScreen from './features/stats/screens/StatsScreen'
import CoachScreen from './features/stats/screens/CoachScreen'
import TeamScreen from './features/stats/screens/TeamScreen'
import PlayerScreen from './features/stats/screens/PlayerScreen'
import LeaguesScreen from './features/stats/screens/LeaguesScreen'
import UserProfileScreen from './features/user/screens/UserProfileScreen'
import SettingsScreen from './features/user/screens/SettingsScreen'
import NotificationSettingsScreen from './features/user/screens/NotificationSettingsScreen'
import SignupFollowScreen from './features/user/screens/SignupFollowScreen'
import SignupFavoriteScreen from './features/user/screens/SignupFavoriteScreen'
import LoginScreen from './features/user/screens/LoginScreen'
import PickerOverlay from './components/PickerOverlay'
import MatchScreen from './features/match/screens/MatchScreen'
import AwaitAuthScreen from './features/user/screens/AwaitAuthScreen'
import ErrorOverlay from './components/ErrorOverlay'
import { VerifyPhoneNumberScreen } from './features/user/screens/VerifyPhoneNumberScreen'
import RequestLocationScreen from './features/user/screens/RequestLocationScreen'
import RequestNotificationPermissionScreen from './features/user/screens/RequestNotificationPermissionScreen'
import UserActivityHistoryScreen from './features/user/screens/UserActivityHistoryScreen'
import ErrorBoundary from './components/ErrorBoundary'
import Info from './components/Info'

export enum ScreenNames {
  HomeScreen = 'HomeScreen',
  FansListScreen = 'FansListScreen',
  StatsScreen = 'StatsScreen',
  CoachScreen = 'CoachScreen',
  TeamScreen = 'TeamScreen',
  PlayerScreen = 'PlayerScreen',
  LeaguesScreen = 'LeaguesScreen',
  UserProfileScreen = 'UserProfileScreen',
  SettingsScreen = 'SettingsScreen',
  NotificationSettingsScreen = 'NotificationSettingsScreen',
  SignupFavoriteScreen = 'SignupFavoriteScreen',
  SignupFollowScreen = 'SignupFollowScreen',
  LoginScreen = 'LoginScreen',
  SearchOverlay = 'SearchOverlay',
  PickerOverlay = 'PickerOverlay',
  MatchScreen = 'MatchScreen',
  AwaitAuthScreen = 'AwaitAuthScreen',
  ErrorOverlay = 'ErrorOverlay',
  Info = 'Info',
  VerifyPhoneNumberScreen = 'VerifyPhoneNumberScreen',
  RequestLocationScreen = 'RequestLocationScreen',
  RequestNotificationPermissionScreen = 'RequestNotificationPermissionScreen',
  UserActivityHistoryScreen = 'UserActivityHistoryScreen',
}

const screenComponents: { [key: string]: React.ComponentType<any> } = {
  [ScreenNames.HomeScreen]: HomeScreen,
  [ScreenNames.FansListScreen]: FansListScreen,
  [ScreenNames.StatsScreen]: StatsScreen,
  [ScreenNames.CoachScreen]: CoachScreen,
  [ScreenNames.TeamScreen]: TeamScreen,
  [ScreenNames.PlayerScreen]: PlayerScreen,
  [ScreenNames.LeaguesScreen]: LeaguesScreen,
  [ScreenNames.UserProfileScreen]: UserProfileScreen,
  [ScreenNames.SettingsScreen]: SettingsScreen,
  [ScreenNames.NotificationSettingsScreen]: NotificationSettingsScreen,
  [ScreenNames.SignupFavoriteScreen]: SignupFavoriteScreen,
  [ScreenNames.SignupFollowScreen]: SignupFollowScreen,
  [ScreenNames.LoginScreen]: LoginScreen,
  [ScreenNames.SettingsScreen]: SettingsScreen,
  [ScreenNames.MatchScreen]: MatchScreen,
  [ScreenNames.AwaitAuthScreen]: AwaitAuthScreen,
  [ScreenNames.ErrorOverlay]: ErrorOverlay,
  [ScreenNames.Info]: Info,
  [ScreenNames.VerifyPhoneNumberScreen]: VerifyPhoneNumberScreen,
  [ScreenNames.RequestLocationScreen]: RequestLocationScreen,
  [ScreenNames.UserActivityHistoryScreen]: UserActivityHistoryScreen,
  [ScreenNames.RequestNotificationPermissionScreen]: RequestNotificationPermissionScreen,
  [ScreenNames.PickerOverlay]: PickerOverlay,
}

export default function registerScreens(store: Store) {
  const providerWrapper = (Component: React.ComponentType<any>) => [
    () => (props: any) => (
      <Provider store={store}>
        <ErrorBoundary>
          <Component {...props} />
        </ErrorBoundary>
      </Provider>
    ),
    () => Component,
  ]

  Object.keys(screenComponents).forEach(name => {
    const component = screenComponents[name]
    return Navigation.registerComponent(name, ...providerWrapper(component))
  })
}

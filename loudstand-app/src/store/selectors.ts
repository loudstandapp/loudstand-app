import { TournamentRoot, ID, FavoriteTeams, TeamList } from '@loudstand/shared'
import { ReduxState, ChosenTeamList } from './model'
import * as fq from './firestoreQueries'

interface TournamentRootDoc {
  body?: TournamentRoot
}
export function tournamentBodies(tournamentDocs?: TournamentRootDoc[]) {
  if (!tournamentDocs) {
    return {}
  }
  return Object.values(tournamentDocs).reduce(
    (acc: { [key in ID]: TournamentRoot }, doc: TournamentRootDoc) => {
      if (!doc.body) {
        return acc
      }
      return {
        ...acc,
        [doc.body.id]: doc.body,
      }
    },
    {}
  )
}

export function defaultTournamentBodies(state: ReduxState) {
  const defaultTournaments =
    state.firestore.ordered.tournaments &&
    state.firestore.ordered.tournaments.filter(
      ({ body: tournament }: TournamentRootDoc) =>
        tournament && tournament.isDefaultForTeams
    )
  return tournamentBodies(defaultTournaments || {})
}

export function tournamentListBodies(
  rawTournamentList?: TournamentRootDoc[]
): TournamentRoot[] {
  if (!rawTournamentList) {
    return []
  }
  return rawTournamentList.reduce(
    (acc: TournamentRoot[], doc: TournamentRootDoc) => {
      if (!doc.body) {
        return acc
      }
      return [...acc, doc.body]
    },
    []
  )
}

export function isLoading(state: ReduxState): boolean {
  return !!Object.values(state.firestore.status.requesting).find(
    value => !!value
  )
}

export function isGenerating(state: ReduxState): boolean {
  return !!Object.values(state.general.generatingDocuments).find(
    value => !!value
  )
}

export function ownFavoriteTeams(state: ReduxState): FavoriteTeams | null {
  const { uid } = state.firebase.auth
  if (!uid) {
    return null
  }
  return state.firestore.data[fq.favoriteTeams.key(uid)] || null
}

export function followingTeams(state: ReduxState): TeamList | null {
  return state.firestore.data.followingTeams || null
}

export function defaultTournamentsList(state: ReduxState): TournamentRoot[] {
  const defaultTournaments =
    state.firestore.ordered.tournaments &&
    state.firestore.ordered.tournaments.filter(
      ({ body: tournament }: TournamentRootDoc) =>
        tournament && tournament.isDefaultForTeams
    )
  return tournamentListBodies(defaultTournaments || [])
}

// FIXME move to shared cvode
export function getFavoriteTeams(
  defaultTournaments: TournamentRoot[],
  favoriteTeams: FavoriteTeams
): ChosenTeamList {
  return defaultTournaments.reduce((acc, tournament) => {
    const {
      id,
      currentSeason: { id: currentSeasonId },
    } = tournament
    // TODO: Add following teams here
    const team =
      favoriteTeams.tournaments &&
      favoriteTeams.tournaments[id] &&
      favoriteTeams.tournaments[id].seasons &&
      favoriteTeams.tournaments[id].seasons[currentSeasonId] &&
      favoriteTeams.tournaments[id].seasons[currentSeasonId].team
    return {
      ...acc,
      ...(team && { [team.id]: { ...team, isFavorite: true } }),
    }
  }, {})
}

export function getChosenTeams(
  defaultTournaments: TournamentRoot[],
  favoriteTeams: FavoriteTeams,
  followingTeams: TeamList
): ChosenTeamList {
  const favoriteTeamsList = getFavoriteTeams(defaultTournaments, favoriteTeams)
  return { ...followingTeams, ...favoriteTeamsList }
}

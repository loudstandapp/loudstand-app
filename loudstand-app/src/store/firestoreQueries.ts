import { ISO639a2Language, ID } from '@loudstand/shared'

interface FirestoreQueryItem<T> {
  query: (arg: T) => object
  key: (arg: T) => string
}

export const allTournaments: FirestoreQueryItem<void> = {
  key() {
    return 'tournaments'
  },
  query() {
    return {
      collection: 'tournaments',
      storeAs: this.key(),
    }
  },
}

export const localizedTeams: FirestoreQueryItem<ISO639a2Language> = {
  key(languageCode) {
    return `localizations/teams/languages/${languageCode}`
  },
  query(languageCode) {
    return {
      collection: 'localizations',
      doc: 'teams',
      subcollections: [
        {
          collection: 'languages',
          doc: languageCode,
        },
      ],
      storeAs: this.key(languageCode),
    }
  },
}

export const seasonTeams: FirestoreQueryItem<ID> = {
  key(id) {
    return `seasons/${id}/attributes/teams`
  },
  query(id: ID) {
    return {
      collection: 'seasons',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'teams' }],
      storeAs: this.key(id),
    }
  },
}

export const favoriteTeams: FirestoreQueryItem<ID> = {
  key(id: ID) {
    return `users/${id}/attributes/favoriteTeams`
  },
  query(id: ID) {
    return {
      collection: 'users',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'favoriteTeams' }],
      storeAs: this.key(id),
    }
  },
}

export const followingTeams: FirestoreQueryItem<ID> = {
  key() {
    return `followingTeams`
  },
  query(id: ID) {
    return {
      collection: 'users',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'followingTeams' }],
      storeAs: this.key(id),
    }
  },
}

export const playerQuestions: FirestoreQueryItem<ID> = {
  key(id) {
    return `players/${id}/attributes/questions`
  },
  query(id: ID) {
    return {
      collection: 'players',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'questions' }],
      storeAs: this.key(id),
    }
  },
}

export const teamQuestions: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}/attributes/questions`
  },
  query(id: ID) {
    return {
      collection: 'teams',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'questions' }],
      storeAs: this.key(id),
    }
  },
}

export const teamPlayers: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}/attributes/players`
  },
  query(id) {
    return {
      collection: 'teams',
      doc: id,
      subcollections: [
        {
          collection: 'attributes',
          doc: 'players',
        },
      ],
      storeAs: this.key(id),
    }
  },
}

export const teamSeasonStatistics: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}/attributes/seasonStatistics`
  },
  query(id) {
    return {
      collection: 'teams',
      doc: id,
      subcollections: [
        {
          collection: 'attributes',
          doc: 'seasonStatistics',
        },
      ],
      storeAs: this.key(id),
    }
  },
}

export const teamRoot: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}`
  },
  query(id) {
    return {
      collection: 'teams',
      doc: id,
      storeAs: this.key(id),
    }
  },
}

export const teamMatchCalendar: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}/attributes/matchCalendar`
  },
  query(id) {
    return {
      collection: 'teams',
      doc: id,
      subcollections: [
        {
          collection: 'attributes',
          doc: 'matchCalendar',
        },
      ],
      storeAs: this.key(id),
    }
  },
}

export const seasonMatchCalendar: FirestoreQueryItem<ID> = {
  key(id) {
    return `seasons/${id}/attributes/matchCalendar`
  },
  query(id) {
    return {
      collection: 'seasons',
      doc: id,
      subcollections: [
        {
          collection: 'attributes',
          doc: 'matchCalendar',
        },
      ],
      storeAs: this.key(id),
    }
  },
}

export const seasonResults: FirestoreQueryItem<ID> = {
  key(id) {
    return `seasons/${id}/attributes/results`
  },
  query(id) {
    return {
      collection: 'seasons',
      doc: id,
      subcollections: [
        {
          collection: 'attributes',
          doc: 'results',
        },
      ],
      storeAs: this.key(id),
    }
  },
}

export const seasonTeamPopularity: FirestoreQueryItem<ID> = {
  key(id) {
    return `seasons/${id}/statistics/teamPopularity`
  },
  query(id) {
    return {
      collection: 'seasons',
      doc: id,
      subcollections: [
        {
          collection: 'statistics',
          doc: 'teamPopularity',
        },
      ],
      storeAs: this.key(id),
    }
  },
}

export const teamHomeScreenItemCalendar: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}/attributes/homeScreenItemCalendar`
  },
  query(id) {
    return {
      collection: 'teams',
      doc: id,
      subcollections: [
        {
          collection: 'attributes',
          doc: 'homeScreenItemCalendar',
        },
      ],
      storeAs: this.key(id),
    }
  },
}
export const teamHomeScreenMemoryCalendar: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}/attributes/homeScreenMemoryCalendar`
  },
  query(id) {
    return {
      collection: 'teams',
      doc: id,
      subcollections: [
        {
          collection: 'attributes',
          doc: 'homeScreenMemoryCalendar',
        },
      ],
      storeAs: this.key(id),
    }
  },
}

export const matchLineups: FirestoreQueryItem<ID> = {
  key(id) {
    return `matches/${id}/attributes/lineups`
  },
  query(id) {
    return {
      collection: 'matches',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'lineups' }],
      storeAs: this.key(id),
    }
  },
}

export const matchRoot: FirestoreQueryItem<ID> = {
  key(id) {
    return `matches/${id}`
  },
  query(id) {
    return {
      collection: 'matches',
      doc: id,
      storeAs: this.key(id),
    }
  },
}

export const matchTeamStatistics: FirestoreQueryItem<ID> = {
  key(id) {
    return `matches/${id}/attributes/teamStatistics`
  },
  query(id) {
    return {
      collection: 'matches',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'teamStatistics' }],
      storeAs: this.key(id),
    }
  },
}

export const matchTimeline: FirestoreQueryItem<ID> = {
  key(id) {
    return `matches/${id}/attributes/timeline`
  },
  query(id) {
    return {
      collection: 'matches',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'timeline' }],
      storeAs: this.key(id),
    }
  },
}

export const seasonStandings: FirestoreQueryItem<ID> = {
  key(id) {
    return `seasons/${id}/attributes/standings`
  },
  query(id) {
    return {
      collection: 'seasons',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'standings' }],
      storeAs: this.key(id),
    }
  },
}
export const seasonPlayerStandings: FirestoreQueryItem<ID> = {
  key(id) {
    return `seasons/${id}/attributes/playerStandings`
  },
  query(id) {
    return {
      collection: 'seasons',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'playerStandings' }],
      storeAs: this.key(id),
    }
  },
}

export const userVoteHistory: FirestoreQueryItem<ID> = {
  key(id) {
    return `users/${id}/votes/history`
  },
  query(id) {
    return {
      collection: 'users',
      doc: id,
      subcollections: [{ collection: 'votes', doc: 'history' }],
      storeAs: this.key(id),
    }
  },
}

export const matchEventVoteResults: FirestoreQueryItem<{
  matchId: ID
  teamId: ID
}> = {
  key({ matchId, teamId }) {
    return `matches/${matchId}/matchEventResults/${teamId}`
  },
  query({ matchId, teamId }) {
    return {
      collection: 'matches',
      doc: matchId,
      subcollections: [{ collection: 'matchEventResults', doc: teamId }],
      storeAs: this.key({ matchId, teamId }),
    }
  },
}
export const teamQuestionResults: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}/statistics/questionResults`
  },
  query(id) {
    return {
      collection: 'teams',
      doc: id,
      subcollections: [{ collection: 'statistics', doc: 'questionResults' }],
      storeAs: this.key(id),
    }
  },
}

export const playerQuestionResults: FirestoreQueryItem<ID> = {
  key(id) {
    return `players/${id}/statistics/questionResults`
  },
  query(id) {
    return {
      collection: 'players',
      doc: id,
      subcollections: [{ collection: 'statistics', doc: 'questionResults' }],
      storeAs: this.key(id),
    }
  },
}

export const playerPopularity: FirestoreQueryItem<{
  playerId: ID
  seasonId: ID
}> = {
  key({ playerId, seasonId }) {
    return `players/${playerId}/popularity/${seasonId}`
  },
  query({ playerId, seasonId }) {
    return {
      collection: 'players',
      doc: playerId,
      subcollections: [{ collection: 'popularity', doc: seasonId }],
      storeAs: this.key({ playerId, seasonId }),
    }
  },
}

export const playerLineupShare: FirestoreQueryItem<{
  playerId: ID
  seasonId: ID
}> = {
  key({ playerId, seasonId }) {
    return `players/${playerId}/lineupShare/${seasonId}`
  },
  query({ playerId, seasonId }) {
    return {
      collection: 'players',
      doc: playerId,
      subcollections: [{ collection: 'lineupShare', doc: seasonId }],
      storeAs: this.key({ playerId, seasonId }),
    }
  },
}

export const playerMostValuableShare: FirestoreQueryItem<{
  playerId: ID
  seasonId: ID
}> = {
  key({ playerId, seasonId }) {
    return `players/${playerId}/mostValuableShare/${seasonId}`
  },
  query({ playerId, seasonId }) {
    return {
      collection: 'players',
      doc: playerId,
      subcollections: [{ collection: 'mostValuableShare', doc: seasonId }],
      storeAs: this.key({ playerId, seasonId }),
    }
  },
}

export const managerPopularity: FirestoreQueryItem<{
  playerId: ID
  seasonId: ID
}> = {
  key({ playerId, seasonId }) {
    return `players/${playerId}/managerPopularity/${seasonId}`
  },
  query({ playerId, seasonId }) {
    return {
      collection: 'players',
      doc: playerId,
      subcollections: [{ collection: 'managerPopularity', doc: seasonId }],
      storeAs: this.key({ playerId, seasonId }),
    }
  },
}

export const playerRoot: FirestoreQueryItem<ID> = {
  key(id) {
    return `players/${id}`
  },
  query(id) {
    return {
      collection: 'players',
      doc: id,
      storeAs: this.key(id),
    }
  },
}

export const teamPerformanceResult: FirestoreQueryItem<{
  matchId: ID
  teamId: ID
}> = {
  key({ matchId, teamId }) {
    return `matches/${matchId}/teamPerformance/${teamId}`
  },
  query({ matchId, teamId }) {
    return {
      collection: 'matches',
      doc: matchId,
      subcollections: [{ collection: 'teamPerformance', doc: teamId }],
      storeAs: this.key({ matchId, teamId }),
    }
  },
}

export const managerPerformanceResult: FirestoreQueryItem<{
  matchId: ID
  teamId: ID
}> = {
  key({ matchId, teamId }) {
    return `matches/${matchId}/managerPerformance/${teamId}`
  },
  query({ matchId, teamId }) {
    return {
      collection: 'matches',
      doc: matchId,
      subcollections: [{ collection: 'managerPerformance', doc: teamId }],
      storeAs: this.key({ matchId, teamId }),
    }
  },
}

export const matchLineupSuggestionResult: FirestoreQueryItem<{
  matchId: ID
  teamId: ID
}> = {
  key({ matchId, teamId }) {
    return `matches/${matchId}/suggestedLineup/${teamId}`
  },
  query({ matchId, teamId }) {
    return {
      collection: 'matches',
      doc: matchId,
      subcollections: [{ collection: 'suggestedLineup', doc: teamId }],
      storeAs: this.key({ matchId, teamId }),
    }
  },
}

export const mostValuablePlayersResult: FirestoreQueryItem<{
  matchId: ID
  teamId: ID
}> = {
  key({ matchId, teamId }) {
    return `matches/${matchId}/mostValuablePlayers/${teamId}`
  },
  query({ matchId, teamId }) {
    return {
      collection: 'matches',
      doc: matchId,
      subcollections: [{ collection: 'mostValuablePlayers', doc: teamId }],
      storeAs: this.key({ matchId, teamId }),
    }
  },
}

export const matchActivePlayers: FirestoreQueryItem<ID> = {
  key(id) {
    return `matches/${id}/attributes/activePlayers`
  },
  query(id) {
    return {
      collection: 'matches',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'activePlayers' }],
      storeAs: this.key(id),
    }
  },
}

export const teamMostValuablePlayers: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}/statistics/mostValuablePlayers`
  },
  query(id) {
    return {
      collection: 'teams',
      doc: id,
      subcollections: [
        { collection: 'statistics', doc: 'mostValuablePlayers' },
      ],
      storeAs: this.key(id),
    }
  },
}

export const teamSuggestedLineup: FirestoreQueryItem<ID> = {
  key(id) {
    return `teams/${id}/statistics/suggestedLineup`
  },
  query(id) {
    return {
      collection: 'teams',
      doc: id,
      subcollections: [{ collection: 'statistics', doc: 'suggestedLineup' }],
      storeAs: this.key(id),
    }
  },
}

export const managerSubstitutionApproval: FirestoreQueryItem<{
  playerId: ID
  seasonId: ID
}> = {
  key({ playerId, seasonId }) {
    return `players/${playerId}/substitutionApproval/${seasonId}`
  },
  query({ playerId, seasonId }) {
    return {
      collection: 'players',
      doc: playerId,
      subcollections: [{ collection: 'substitutionApproval', doc: seasonId }],
      storeAs: this.key({
        playerId,
        seasonId,
      }),
    }
  },
}

export const teamMostValuablePlayersByUser: FirestoreQueryItem<{
  teamId: ID
  userId: ID
}> = {
  key({ userId, teamId }) {
    return `users/${userId}/mostValuablePlayers/${teamId}`
  },
  query({ userId, teamId }) {
    return {
      collection: 'users',
      doc: userId,
      subcollections: [{ collection: 'mostValuablePlayers', doc: teamId }],
      storeAs: this.key({ userId, teamId }),
    }
  },
}

export const teamSuggestedLineupByUser: FirestoreQueryItem<{
  teamId: ID
  userId: ID
}> = {
  key({ userId, teamId }) {
    return `users/${userId}/suggestedLineup/${teamId}`
  },
  query({ userId, teamId }) {
    return {
      collection: 'users',
      doc: userId,
      subcollections: [{ collection: 'suggestedLineup', doc: teamId }],
      storeAs: this.key({ userId, teamId }),
    }
  },
}

export const teamPerformanceByUser: FirestoreQueryItem<{
  teamId: ID
  userId: ID
}> = {
  key({ userId, teamId }) {
    return `users/${userId}/teamPerformance/${teamId}`
  },
  query({ userId, teamId }) {
    return {
      collection: 'users',
      doc: userId,
      subcollections: [{ collection: 'teamPerformance', doc: teamId }],
      storeAs: this.key({ userId, teamId }),
    }
  },
}

export const managerPerformanceByUser: FirestoreQueryItem<{
  teamId: ID
  userId: ID
}> = {
  key({ userId, teamId }) {
    return `users/${userId}/managerPerformance/${teamId}`
  },
  query({ userId, teamId }) {
    return {
      collection: 'users',
      doc: userId,
      subcollections: [{ collection: 'managerPerformance', doc: teamId }],
      storeAs: this.key({ userId, teamId }),
    }
  },
}

export const reactionsByUser: FirestoreQueryItem<ID> = {
  key(id) {
    return `users/${id}/statistics/reactions`
  },
  query(id) {
    return {
      collection: 'users',
      doc: id,
      subcollections: [{ collection: 'statistics', doc: 'reactions' }],
      storeAs: this.key(id),
    }
  },
}

export const userFriendList: FirestoreQueryItem<ID> = {
  key(id) {
    return `users/${id}/attributes/friends`
  },
  query(id) {
    return {
      collection: 'users',
      doc: id,
      subcollections: [{ collection: 'attributes', doc: 'friends' }],
      storeAs: this.key(id),
    }
  },
}

export const userNotificationSettings: FirestoreQueryItem<ID> = {
  key(id) {
    return `users/${id}/attributes/notificationSettings`
  },
  query(id) {
    return {
      collection: 'users',
      doc: id,
      subcollections: [
        { collection: 'attributes', doc: 'notificationSettings' },
      ],
      storeAs: this.key(id),
    }
  },
}

export const userProfile: FirestoreQueryItem<ID> = {
  key(id) {
    return `users/${id}`
  },
  query(id) {
    return {
      collection: 'users',
      doc: id,
      storeAs: this.key(id),
    }
  },
}

export const userActivities: FirestoreQueryItem<ID> = {
  key(id) {
    return `users/${id}/votes/activityFeed`
  },
  query(id) {
    return {
      collection: 'users',
      doc: id,
      subcollections: [{ collection: 'votes', doc: 'activityFeed' }],
      storeAs: this.key(id),
    }
  },
}

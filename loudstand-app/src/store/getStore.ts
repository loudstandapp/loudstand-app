import { createStore, applyMiddleware } from 'redux'

import { reduxFirestore, getFirestore } from 'redux-firestore'
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import firebase from 'react-native-firebase'
import AsyncStorage from '@react-native-community/async-storage'
import { persistReducer } from 'redux-persist'
import reducer, { generalInitialValue } from './reducer'
import { ReduxState } from './model'
import { observeActions } from '../services/observeActions'

export interface ExtraAgument {
  getFirebase: Function
  getFirestore: Function
}

const initialValue: ReduxState = {
  general: generalInitialValue,
  firebase: {},
  firestore: {},
}

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['firebase', 'firestore', 'general.submittedVotes'], // test this
}

const persistedReducer = persistReducer(persistConfig, reducer)

export const getStore = () => {
  const store = createStore(
    persistedReducer,
    initialValue,
    composeWithDevTools(
      reactReduxFirebase(firebase, {
        userProfile: 'users',
        useFirestoreForProfile: true,
        // updateProfileOnLogin: true,
        // presence: 'presence',
        enableRedirectHandling: false,
      }),
      reduxFirestore(firebase),
      applyMiddleware(
        thunk.withExtraArgument({ getFirebase, getFirestore }),
        observeActions
      )
    )
  )
  return store
}

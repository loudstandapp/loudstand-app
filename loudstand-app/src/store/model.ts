import { TeamListItem, TournamentRoot, ID } from '@loudstand/shared'
import { GuideType } from '../components/Guide'

export interface ChosenTeamListItem extends TeamListItem {
  isFavorite?: boolean
}

export interface GeolocationAccess {
  hasBeenAsked: boolean
  isAllowed: boolean
  numberOfRejects: number
  numberOfContinueWithoutPermission: number
}

export interface Geolocation {
  latitude: number
  longitude: number
  timestamp: number
}

interface GeneratingDocuments {
  [path: string]: boolean
}

interface ActiveTeamForUser {
  [UserID: string]: TeamListItem
}

export interface SubmittedVotes {
  [VoteKey: string]: boolean
}

export type DismissedGuides = { [type in GuideType]?: boolean }

export type ChosenTeamList = { [key in ID]: ChosenTeamListItem }

export interface GeneralState {
  activeTeam?: ChosenTeamListItem
  activeTournament?: TournamentRoot
  geolocationAccess: GeolocationAccess
  activeTeamForUser: ActiveTeamForUser
  // These below are cleaned on app launch
  availableTeams: ChosenTeamList
  geolocation?: Geolocation
  submittedVotes?: SubmittedVotes
  submittingVotes?: SubmittedVotes
  generatingDocuments: GeneratingDocuments
  launchCount: number
  isFacebookLoggingIn: boolean
  dismissedGuides: DismissedGuides
}

export interface ReduxState {
  general: GeneralState
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  firebase: any
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  firestore: any
}

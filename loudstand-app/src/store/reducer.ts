import { combineReducers } from 'redux'
import { firebaseReducer } from 'react-redux-firebase'
import { firestoreReducer } from 'redux-firestore'
import { GeneralState, ChosenTeamListItem } from './model'
import { ActionTypes, Action } from './actions'

export const generalInitialValue: GeneralState = {
  availableTeams: {},
  geolocationAccess: {
    isAllowed: false,
    hasBeenAsked: false,
    numberOfRejects: 0,
    numberOfContinueWithoutPermission: 0,
  },
  activeTeamForUser: {},
  generatingDocuments: {},
  dismissedGuides: {},
  launchCount: 0,
  isFacebookLoggingIn: false,
}

export function general(
  state: GeneralState | undefined = generalInitialValue,
  action: Action
): GeneralState {
  switch (action.type) {
    case ActionTypes.SubmittingVote: {
      return {
        ...state,
        submittingVotes: {
          ...state.submittingVotes,
          [action.voteKey]: true,
        },
      }
    }
    case ActionTypes.SubmittedVote: {
      return {
        ...state,
        submittedVotes: {
          ...state.submittedVotes,
          [action.voteKey]: true,
        },
        submittingVotes: {
          ...state.submittingVotes,
          [action.voteKey]: false,
        },
      }
    }
    case ActionTypes.SubmitVoteFailed: {
      return {
        ...state,
        submittingVotes: {
          ...state.submittingVotes,
          [action.voteKey]: false,
        },
      }
    }
    case ActionTypes.SetTournament: {
      return {
        ...state,
        activeTournament: action.tournament,
      }
    }
    case ActionTypes.BeginGenerating: {
      return {
        ...state,
        generatingDocuments: {
          ...state.generatingDocuments,
          [action.path]: true,
        },
      }
    }
    case ActionTypes.EndGenerating: {
      return {
        ...state,
        generatingDocuments: {
          ...state.generatingDocuments,
          [action.path]: false,
        },
      }
    }
    case ActionTypes.LaunchCleanup: {
      const {
        activeTournament,
        geolocationAccess,
        activeTeamForUser,
        launchCount,
        dismissedGuides,
      } = state
      return {
        ...generalInitialValue,
        activeTournament,
        geolocationAccess,
        activeTeamForUser,
        launchCount,
        // Upgrade path
        dismissedGuides: dismissedGuides || {},
      }
    }
    case ActionTypes.LogoutCleanup: {
      return generalInitialValue
    }
    case ActionTypes.SetTeam: {
      return {
        ...state,
        activeTeam: action.team,
      }
    }
    case ActionTypes.SetActiveTeamForUser: {
      const { userId, team } = action
      return {
        ...state,
        activeTeamForUser: {
          ...state.activeTeamForUser,
          [userId]: team,
        },
      }
    }
    case ActionTypes.SetAvailableTeams: {
      const { activeTeam } = state
      const teamsArray = Object.values(action.teams) as ChosenTeamListItem[]
      // Set active team to a valid team if needed.
      if (
        (!activeTeam || !action.teams[activeTeam.id]) &&
        teamsArray.length > 0
      ) {
        const [newActiveTeam] = teamsArray.filter(team => team.isFavorite)
        return {
          ...state,
          availableTeams: action.teams,
          activeTeam: newActiveTeam,
        }
      }
      return {
        ...state,
        availableTeams: action.teams,
      }
    }
    case ActionTypes.SetGeolocation: {
      const { latitude, longitude, timestamp } = action
      return {
        ...state,
        geolocation: {
          latitude,
          longitude,
          timestamp,
        },
      }
    }
    case ActionTypes.ClearGeolocation: {
      return {
        ...state,
        geolocation: undefined,
      }
    }
    case ActionTypes.SetGeolocationAccess: {
      const { isAllowed } = action
      return {
        ...state,
        geolocationAccess: {
          isAllowed,
          hasBeenAsked: true,
          numberOfRejects: isAllowed
            ? 0
            : state.geolocationAccess.numberOfRejects + 1,
          numberOfContinueWithoutPermission: isAllowed
            ? 0
            : state.geolocationAccess.numberOfContinueWithoutPermission,
        },
      }
    }
    case ActionTypes.ContinueWithoutGeolocationPermission: {
      return {
        ...state,
        geolocationAccess: {
          ...state.geolocationAccess,
          numberOfContinueWithoutPermission:
            state.geolocationAccess.numberOfContinueWithoutPermission + 1,
        },
      }
    }
    case ActionTypes.CountLaunch: {
      return {
        ...state,
        launchCount:
          state.launchCount !== undefined ? state.launchCount + 1 : 1,
      }
    }
    case ActionTypes.BeginFacebookLogin: {
      return {
        ...state,
        isFacebookLoggingIn: true,
      }
    }
    case ActionTypes.EndFacebookLogin: {
      return {
        ...state,
        isFacebookLoggingIn: false,
      }
    }
    case ActionTypes.DismissGuide: {
      const { guideType } = action
      return {
        ...state,
        dismissedGuides: {
          ...state.dismissedGuides,
          [guideType]: true,
        },
      }
    }

    default: {
      return state
    }
  }
}

export default combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer,
  general,
})

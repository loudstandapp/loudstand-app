import { TournamentRoot, TeamListItem, ID } from '@loudstand/shared'
import { ChosenTeamListItem, ChosenTeamList } from './model'
import { GuideType } from '../components/Guide'

export enum ActionTypes {
  SetTournament = 'SET_TOURNAMENT',
  SetTeam = 'SET_TEAM',
  SetAvailableTeams = 'SET_AVAILABLE_TEAMS',
  SetGeolocationAccess = 'SET_GEOLOCATION_ACCESS',
  ContinueWithoutGeolocationPermission = 'CONTIUE_WITHOUT_GEOLOCATION_PERMISSION',
  SetGeolocation = 'SET_GEOLOCATION',
  ClearGeolocation = 'CLEAR_GEOLOCATION',
  SetActiveTeamForUser = 'SET_ACTIVE_TEAM_FOR_USER',
  SubmittedVote = 'SUBMITTED_VOTE',
  SubmittingVote = 'SUBMITTING_VOTE',
  SubmitVoteFailed = 'SUBMIT_VOTE_FAILED',
  BeginGenerating = 'BEGIN_GENERATING',
  EndGenerating = 'END_GENERATING',
  LaunchCleanup = 'LAUNCH_CLEANUP',
  LogoutCleanup = 'LOGOUT_CLEANUP',
  CountLaunch = 'COUNT_LAUNCH',
  BeginFacebookLogin = 'BEGIN_FACEBOOK_LOGIN',
  EndFacebookLogin = 'END_FACEBOOK_LOGIN',
  DismissGuide = 'DISMISS_GUIDE',
}

interface SubmittedVoteAction {
  type: typeof ActionTypes.SubmittedVote
  voteKey: ID
}

export function submittedVote(voteKey: ID): SubmittedVoteAction {
  return {
    type: ActionTypes.SubmittedVote,
    voteKey,
  }
}

interface SubmittingVoteAction {
  type: typeof ActionTypes.SubmittingVote
  voteKey: ID
}

export function submittingVote(voteKey: ID): SubmittingVoteAction {
  return {
    type: ActionTypes.SubmittingVote,
    voteKey,
  }
}

interface SubmitedVoteFailedAction {
  type: typeof ActionTypes.SubmitVoteFailed
  voteKey: ID
}

export function submitVoteFailed(voteKey: ID): SubmitedVoteFailedAction {
  return {
    type: ActionTypes.SubmitVoteFailed,
    voteKey,
  }
}

interface SetTournamentAction {
  type: typeof ActionTypes.SetTournament
  tournament: TournamentRoot
}

export function setTournament(tournament: TournamentRoot): SetTournamentAction {
  return {
    type: ActionTypes.SetTournament,
    tournament,
  }
}

interface SetActiveTeamForUser {
  type: typeof ActionTypes.SetActiveTeamForUser
  team: ChosenTeamListItem
  userId: ID
}

export function setActiveTeamForUser(
  team: TeamListItem,
  userId: ID
): SetActiveTeamForUser {
  return {
    type: ActionTypes.SetActiveTeamForUser,
    team,
    userId,
  }
}

interface SetTeam {
  type: typeof ActionTypes.SetTeam
  team: ChosenTeamListItem
}

export function setTeam(team: ChosenTeamListItem): SetTeam {
  // FIXME TeamRoot
  return {
    type: ActionTypes.SetTeam,
    team,
  }
}

interface BeginGenerating {
  type: typeof ActionTypes.BeginGenerating
  path: string
}

export function beginGenerating(path: string): BeginGenerating {
  return {
    type: ActionTypes.BeginGenerating,
    path,
  }
}

interface EndGenerating {
  type: typeof ActionTypes.EndGenerating
  path: string
}

export function endGenerating(path: string): EndGenerating {
  return {
    type: ActionTypes.EndGenerating,
    path,
  }
}

interface BeginFacebookLogin {
  type: typeof ActionTypes.BeginFacebookLogin
}

export function beginFacebookLogin(): BeginFacebookLogin {
  return {
    type: ActionTypes.BeginFacebookLogin,
  }
}

interface EndFacebookLogin {
  type: typeof ActionTypes.EndFacebookLogin
}

export function endFacebookLogin(): EndFacebookLogin {
  return {
    type: ActionTypes.EndFacebookLogin,
  }
}

interface SetAvailableTeams {
  type: typeof ActionTypes.SetAvailableTeams
  teams: ChosenTeamList
}

export function setAvailableTeams(teams: ChosenTeamList): SetAvailableTeams {
  // FIXME TeamRoot
  return {
    type: ActionTypes.SetAvailableTeams,
    teams,
  }
}

interface SetGeolocationAccess {
  type: typeof ActionTypes.SetGeolocationAccess
  isAllowed: boolean
}

export function setGeolocationAccess(isAllowed: boolean): SetGeolocationAccess {
  return {
    type: ActionTypes.SetGeolocationAccess,
    isAllowed,
  }
}

interface ContinueWithoutGeolocationPermission {
  type: typeof ActionTypes.ContinueWithoutGeolocationPermission
}

export function continueWithoutGeolocationPermission(): ContinueWithoutGeolocationPermission {
  return {
    type: ActionTypes.ContinueWithoutGeolocationPermission,
  }
}

interface SetGeolocation {
  type: typeof ActionTypes.SetGeolocation
  latitude: number
  longitude: number
  timestamp: number
}

export function setGeolocation(
  latitude: number,
  longitude: number,
  timestamp: number
): SetGeolocation {
  return {
    type: ActionTypes.SetGeolocation,
    latitude,
    longitude,
    timestamp,
  }
}

interface ClearGeolocation {
  type: typeof ActionTypes.ClearGeolocation
}

export function clearGeolocation(): ClearGeolocation {
  return {
    type: ActionTypes.ClearGeolocation,
  }
}

interface LaunchCleanup {
  type: typeof ActionTypes.LaunchCleanup
}

export function launchCleanup(): LaunchCleanup {
  return {
    type: ActionTypes.LaunchCleanup,
  }
}

interface LogoutCleanup {
  type: typeof ActionTypes.LogoutCleanup
}

export function logoutCleanup(): LogoutCleanup {
  return {
    type: ActionTypes.LogoutCleanup,
  }
}
interface CountLaunch {
  type: typeof ActionTypes.CountLaunch
}

export function countLaunch(): CountLaunch {
  return {
    type: ActionTypes.CountLaunch,
  }
}

interface DismissGuideAction {
  type: typeof ActionTypes.DismissGuide
  guideType: GuideType
}

export function dismissGuide(guideType: GuideType): DismissGuideAction {
  return {
    type: ActionTypes.DismissGuide,
    guideType,
  }
}

export type Action =
  | SetTournamentAction
  | SetTeam
  | SetAvailableTeams
  | SetGeolocationAccess
  | ClearGeolocation
  | SetGeolocation
  | SetActiveTeamForUser
  | SubmitVoteAction
  | ContinueWithoutGeolocationPermission
  | BeginGenerating
  | EndGenerating
  | LogoutCleanup
  | LaunchCleanup
  | CountLaunch
  | BeginFacebookLogin
  | EndFacebookLogin
  | DismissGuideAction

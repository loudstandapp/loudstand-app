package com.loudstand.football.app;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;

import com.BV.LinearGradient.LinearGradientPackage;

import com.horcrux.svg.SvgPackage;

import com.loudstand.football.app.BuildConfig;
import com.mkuczera.RNReactNativeHapticFeedbackPackage;

import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;

import com.reactnativenavigation.NavigationApplication;
import com.reactnativenavigation.react.NavigationReactNativeHost;
import com.reactnativenavigation.react.ReactGateway;
import com.reactcommunity.rnlocalize.RNLocalizePackage;

import com.facebook.CallbackManager;
import com.facebook.reactnative.androidsdk.FBSDKPackage;

import java.util.Arrays;
import java.util.List;

import com.ianlin.RNCarrierInfo.RNCarrierInfoPackage;

import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;
import io.invertase.firebase.auth.RNFirebaseAuthPackage;
import io.invertase.firebase.firestore.RNFirebaseFirestorePackage;
import io.invertase.firebase.functions.RNFirebaseFunctionsPackage;
import io.invertase.firebase.fabric.crashlytics.RNFirebaseCrashlyticsPackage;
import io.invertase.firebase.instanceid.RNFirebaseInstanceIdPackage;
import io.invertase.firebase.messaging.RNFirebaseMessagingPackage;
import io.invertase.firebase.notifications.RNFirebaseNotificationsPackage;
import io.sentry.RNSentryPackage;

import com.reactnativecommunity.slider.ReactSliderPackage;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;

import com.microsoft.codepush.react.CodePush;

import com.agontuk.RNFusedLocation.RNFusedLocationPackage;

import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.dylanvann.fastimage.FastImageViewPackage;

import com.levelasquez.androidopensettings.AndroidOpenSettingsPackage;

public class MainApplication extends NavigationApplication {
    private static CallbackManager mCallbackManager = CallbackManager.Factory.create();

    protected static CallbackManager getCallbackManager() {
        return mCallbackManager;
    }

    // @Override
    // public void onCreate() {
    // super.onCreate();
    //
    // registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
    // @Override
    // public void onActivityCreated(Activity activity, Bundle bundle) {
    // // Here we specify to force portrait at each start of any Activity
    // activity.setRequestedOrientation(
    // ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    // }
    // });
    // }

    @Override
    protected ReactGateway createReactGateway() {
        ReactNativeHost host = new NavigationReactNativeHost(this, isDebug(), createAdditionalReactPackages()) {
            @Override
            protected String getJSMainModuleName() {
                return "index";
            }

            @javax.annotation.Nullable
            @Override
            protected String getJSBundleFile() {
                return CodePush.getJSBundleFile();
            }
        };
        return new ReactGateway(this, isDebug(), host);
    }

    @Override
    public boolean isDebug() {
        return BuildConfig.DEBUG;
    }

    protected List<ReactPackage> getPackages() {
        // Add additional packages you require here
        // No need to add RnnPackage and MainReactPackage

        String codePushDeploymentKey = getResources().getString(R.string.reactNativeCodePush_androidDeploymentKey);
        return Arrays.<ReactPackage>asList(new LinearGradientPackage(), new SvgPackage(),
                new RNReactNativeHapticFeedbackPackage(), new RNLocalizePackage(), new FBSDKPackage(mCallbackManager),
                new RNCarrierInfoPackage(), new RNFirebasePackage(), new RNFirebaseAuthPackage(),
                new RNFirebaseFirestorePackage(), new RNFirebaseFunctionsPackage(), new ReactSliderPackage(),
                new RNFirebaseCrashlyticsPackage(), new AsyncStoragePackage(),
                new CodePush(codePushDeploymentKey, MainApplication.this, BuildConfig.DEBUG),
                new RNFusedLocationPackage(), new RNDeviceInfo(), new RNFirebaseAnalyticsPackage(),
                new RNFirebaseMessagingPackage(), new RNFirebaseNotificationsPackage(),
                new RNFirebaseInstanceIdPackage(), new RNSentryPackage(), new FastImageViewPackage(),
                new AndroidOpenSettingsPackage()
        // eg. new VectorIconsPackage()
        );
    }

    @Override
    public List<ReactPackage> createAdditionalReactPackages() {
        return getPackages();
    }
}

import { ISO8601Date } from "./models";

export function splitIntoChunks<T>(list: T[], maxSize: number): Array<T[]> {
  let pointer = 0;
  let chunks: Array<T[]> = [];
  do {
    const chunk = list.slice(pointer, pointer + maxSize);
    if (chunk.length > 0) {
      chunks.push(chunk);
    }
    pointer += maxSize;
  } while (pointer < list.length);
  return chunks;
}

export function cleanDate(dateWithZ: string): ISO8601Date {
  const match = dateWithZ.match(/^(\d{4}-\d{2}-\d{2}).*$/);
  if (!match || match.length < 2 || match[1] === null) {
    throw new Error(`cleanDate() could not parse timestamp "${dateWithZ}"`);
  }
  return match[1];
}

type ID = string;
export type ISO8601Timestamp = string;
export type ISO8601Date = string;
type CountryCode = string;

interface LoudstandUserStatistics {}

interface LoudstandTeamCalendarStatistics {
  followers: number;
}

interface User {
  birthday: Date;
  slogan: string;
  name: string;
  nickname: string;
  favoriteTeam: ID;
  followingTeams: ID[];
  country: CountryCode;
  loudstandStatistics: LoudstandUserStatistics;
}

interface LoudstandTeamCalendarGameStatistics {}

export interface TeamCalendarGame {
  id: ID;
  date: ISO8601Date;
  leagueId: ID;
  leagueName: string;
  seasonId: ID;
  seasonName: string;
  homeTeamName: string;
  homeTeamId: ID;
  awayTeamName: string;
  awayTeamId: ID;
  loudstandStatistics: LoudstandTeamCalendarGameStatistics;
  stadiumName: string;
}

interface DailyQuestion {
  date: ISO8601Date;
  image: string;
  playerId: ID;
  question: {}; // TODO: Translatable text
}

// Used for team calendar home screen
export interface TeamCalendar {
  id: ID;
  name: string;
  logo?: string;
  games: TeamCalendarGame[];
  news?: [];
  press?: [];
  training?: [];
  questions?: DailyQuestion[];
  loudstandStatistics: LoudstandTeamCalendarStatistics;
}

interface TeamEvent {}

interface LoudstandCalendarEventStatistics {}

const team: TeamCalendar = {
  name: "fck",
  id: "12134",
  loudstandStatistics: {
    followers: 123
  },
  games: [
    {
      id: "1234",
      stadiumName: "PARKEN",
      date: "2014-03-31",
      leagueId: "12313123",
      leagueName: "12321312",
      seasonId: "12345",
      seasonName: "5231",
      homeTeamName: "Benfica",
      homeTeamId: "12345",
      awayTeamName: "Guamares",
      awayTeamId: "1235",
      loudstandStatistics: {}
    }
  ],
  news: [],
  training: [],
  questions: []
};

type QuestionCalendar = {
  [key: string]: DailyQuestion[];
};

interface LoudstandTeamStatistics {
  popularity: number;
}

interface Team {
  id: ID;
  country: CountryCode;
  logo: string;
  loudstandStatistics: LoudstandTeamStatistics;
  calendar: TeamCalendar;
  questions: QuestionCalendar;
}

// Used for league stats screen
interface LeageStandingItem {
  position: number;
  team: Team;
  playedMatches: number;
  points: number;
  goalDifference: {
    for: number;
    against: number;
  };
  leagueId: string;
}

enum MatchState {
  PreMatch = "PRE_MATCH",
  Live = "LIVE",
  PostMach = "POST_MATCH"
}

interface LoudstandMatchStatistics {
  mostPopularPlayer: Player;
  coachPopularity: number;
}

// Use for header in the match screen
interface Match {
  state: MatchState;
  home: {
    team: Team;
    score: number;
    loudstandStatistics: LoudstandMatchStatistics;
  };
  away: {
    team: Team;
    score: number;
  };
  startTime: ISO8601Timestamp;
  stadiumName: string;
  reactionEndTime: ISO8601Timestamp;
  reactionStartTime: ISO8601Timestamp;
}

interface Player {}
interface Coach {}

interface PlayerPercentage {
  player: Player;
  percentage: number;
}

interface FormationLineup<T> {
  // use Player or PlayerPercentage depending on needs
  goalkeeper: T[];
  defender: T[];
  midfield: T[];
  forward: T[];
}

// Used for formations screen
interface MatchFormations {
  home: {
    team: Team;
    formation: TeamFormation;
  };
  away: {
    team: Team;
    formation: TeamFormation;
  };
}

interface TeamFormation {
  lineup: FormationLineup<Player>;
  coach: Coach;
  playing: Player[];
  bench: Player[]; // FIXME proper terms
  allActivePlayers: Player[];
}

enum MatchEventType {
  Goal = "GOAL",
  Substution = "SUBSTITUTION",
  YellowCard = "YELLOW_CARD",
  RedCard = "RED_CARD",
  Penalty = "PENALTY"
}

interface MatchEventGoal extends MatchEvent {
  type: typeof MatchEventType.Goal;
  player: Player;
}

interface MatchEventSubstitution {
  type: typeof MatchEventType.Substution;
  playerIn: Player;
  playerOut: Player;
}

interface MatchEventYellowCard {
  type: typeof MatchEventType.YellowCard;
  player: Player;
}

interface MatchEventRedCard {
  type: typeof MatchEventType.RedCard;
  player: Player;
}
interface MatchEventPenalty {
  type: typeof MatchEventType.Penalty;
  player: Player;
}

type EventData =
  | MatchEventGoal
  | MatchEventSubstitution
  | MatchEventYellowCard
  | MatchEventRedCard
  | MatchEventPenalty;

// Used for the events summary screen
interface MatchEvent {
  id: string;
  seconds: number;
  team: Team;
  data: EventData;
  isReactable: boolean;
}

interface MatchStatisticsItem {
  key: number;
  value: number;
}

interface MatchStatistics {
  home: {
    team: Team;
    statistics: MatchStatisticsItem[];
  };
  away: {
    team: Team;
    statistics: MatchStatisticsItem[];
  };
}

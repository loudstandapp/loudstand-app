import { splitIntoChunks } from "../utils";

const empty: number[] = [];

const listOf5: number[] = [1, 2, 3, 4, 5];

const listOf15: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

console.log("chunksize 3");
console.log(splitIntoChunks(empty, 3));
console.log(splitIntoChunks(listOf5, 3));
console.log(splitIntoChunks(listOf15, 3));
console.log("chunksize 5");
console.log(splitIntoChunks(empty, 5));
console.log(splitIntoChunks(listOf5, 5));
console.log(splitIntoChunks(listOf15, 5));
console.log("chunksize 15");
console.log(splitIntoChunks(empty, 15));
console.log(splitIntoChunks(listOf5, 15));
console.log(splitIntoChunks(listOf15, 15));

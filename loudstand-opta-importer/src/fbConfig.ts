import * as firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";

// Replace this with your own config details
var config = {
  apiKey: "AIzaSyB7kMO-25To2TuNURBQEZSlYKyVH6v_lR0",
  authDomain: "esben-test.firebaseapp.com",
  databaseURL: "https://esben-test.firebaseio.com",
  projectId: "esben-test",
  storageBucket: "esben-test.appspot.com",
  messagingSenderId: "752469057951"
};
firebase.initializeApp(config);
import "firebase/functions";

export default firebase;

import axios from "axios";
import * as fs from "fs";
import firebase from "./fbConfig";

const API_KEY = "1vmmaetzoxkgg1qf6pkpfmku0k";

const activeLeagues = [
  "8yi6ejjd1zudcqtbn07haahg6", // PT primeira
  "29actv1ohj8r10kd9hu0jnb0n", // DK superliga
  "2kwbbcootiqqgmrzs6o5inle5" // EN premier league
];

async function loadData() {
  const db = firebase.firestore();

  let leaguesAndTeams = [];
  try {
    for (const leagueId of activeLeagues) {
      const leagueUrl = `http://api.performfeeds.com/soccerdata/tournamentcalendar/${API_KEY}/active?_fmt=json&_rt=b&comp=${leagueId}`;
      const leagueResponse = await axios.get(leagueUrl);
      const league = leagueResponse.data.competition[0];
      const seasonsData: Array<any> = league.tournamentCalendar;
      const season = seasonsData.find((e: any) => e.active === "yes");
      const teamsUrl = `http://api.performfeeds.com/soccerdata/team/${API_KEY}?_rt=b&_fmt=json&tmcl=${
        season.id
      }`;
      const teamsResponse = await axios.get(teamsUrl);
      const teams: Array<any> = teamsResponse.data.contestant.map(
        ({ shortName, id }: any) => ({
          id,
          name: shortName
        })
      );

      const item = {
        leagueId: league.id,
        leagueName: league.name,
        leagueCountry: league.countryCode,
        seasonId: season.id,
        seasonName: season.name,
        teams
      };

      db.collection("app-data")
        .doc("team-selection")
        .collection("leagues")
        .doc(leagueId)
        .set(item, { merge: true });
      leaguesAndTeams.push(item);
    }

    fs.writeFileSync(
      "output/leaguesAndTeams.json",
      JSON.stringify(leaguesAndTeams)
    );
  } catch (error) {
    console.log(error);
  }
}

loadData();

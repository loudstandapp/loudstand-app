import axios from "axios";
import firebase from "./fbConfig";
import { cleanDate } from "./utils";
import { TeamCalendarGame, TeamCalendar } from "./models";
const API_KEY = "xroygy79l4kyzzvz53fi20w7";

const activeLeagues = [
  "8yi6ejjd1zudcqtbn07haahg6", // PT primeira
  "29actv1ohj8r10kd9hu0jnb0n", // DK superliga
  "2kwbbcootiqqgmrzs6o5inle5" // EN premier league
];

function sortGamesByDate(games: TeamCalendarGame[]): TeamCalendarGame[] {
  return games.sort((a, b) =>
    a.date === b.date ? 0 : a.date > b.date ? 1 : -1
  );
}

async function loadData() {
  const db = firebase.firestore();

  let leaguesAndTeams = [];
  try {
    for (const leagueId of activeLeagues) {
      const leagueUrl = `http://api.performfeeds.com/soccerdata/tournamentcalendar/${API_KEY}/active?_fmt=json&_rt=b&comp=${leagueId}`;
      const leagueResponse = await axios.get(leagueUrl);
      const league = leagueResponse.data.competition[0];
      const seasonsData: Array<any> = league.tournamentCalendar;
      const season = seasonsData.find((e: any) => e.active === "yes");

      const matchesUrl = `http://api.performfeeds.com/soccerdata/match/${API_KEY}?_rt=b&_fmt=json&tmcl=${
        season.id
      }&_pgSz=1000`;

      console.log({ matchesUrl });

      const matchesResponse = await axios.get(matchesUrl);
      const games: TeamCalendarGame[] = matchesResponse.data.match.map(
        ({ matchInfo }: any) => {
          const homeTeam = matchInfo.contestant.find(
            (team: any) => team.position === "home"
          );
          const awayTeam = matchInfo.contestant.find(
            (team: any) => team.position === "away"
          );
          const isoDate = cleanDate(matchInfo.date);
          return {
            id: matchInfo.id,
            date: isoDate,
            startTime: `${isoDate}T${matchInfo.time}`,
            leagueId: matchInfo.competition.id,
            leagueName: matchInfo.competition.name,
            seasonId: matchInfo.tournamentCalendar.id,
            seasonName: matchInfo.tournamentCalendar.name,
            awayTeamId: awayTeam.id,
            awayTeamName: awayTeam.name,
            homeTeamId: homeTeam.id,
            homeTeamName: homeTeam.name,
            stadiumName: matchInfo.venue.shortName
          };
        }
      );

      const teams: {
        [key in string]: {
          id: string;
          name: string;
        }
      } = games.reduce(
        (result, game) => ({
          ...result,
          [game.awayTeamId]: { id: game.awayTeamId, name: game.awayTeamName },
          [game.homeTeamId]: { id: game.homeTeamId, name: game.homeTeamId }
        }),
        {}
      );

      for (const teamId of Object.keys(teams)) {
        const teamDocRef = db
          .collection("app-data")
          .doc("team-calendar")
          .collection("teams")
          .doc(teamId);

        const gamesForTeam = games.filter(
          game => game.awayTeamId === teamId || game.homeTeamId === teamId
        );

        await db.runTransaction(async transaction => {
          const teamDoc = await transaction.get(teamDocRef);
          // Create a new team calendar
          if (!teamDoc.exists) {
            const team = teams[teamId];
            const teamCalendar = {
              id: team.id,
              name: team.name,
              games: sortGamesByDate(gamesForTeam)
            };
            return transaction.set(teamDocRef, teamCalendar);
          }
          // Update existing, and preserve games from other leagues
          const teamData = (await teamDoc.data()) as TeamCalendar;
          const otherLeagueGames = teamData.games.filter(
            ({ seasonId }) => seasonId !== season.id
          );
          const allUpdatedGames = otherLeagueGames.concat(gamesForTeam);
          const games = sortGamesByDate(allUpdatedGames);
          return transaction.update(teamDocRef, { games });
        });
      }
    }
  } catch (error) {
    console.log(error);
  }
}

loadData();

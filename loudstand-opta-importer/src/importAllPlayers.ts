import axios from "axios";
// import * as fs from "fs";
import firebase from "./fbConfig";
import { splitIntoChunks } from "./utils";

const MAX_BATCH_SIZE = 500;
const API_KEY = "1vmmaetzoxkgg1qf6pkpfmku0k";
loadData();
console.log("laaa");
async function loadData() {
  const db = firebase.firestore();
  try {
    const leagesUrl = `http://api.performfeeds.com/soccerdata/tournamentcalendar/${API_KEY}/active?_fmt=json&_rt=b`;
    const response = await axios.get(leagesUrl);
    const competitions = response.data.competition;

    const seasonIds: string[] = competitions
      .filter((c: any) => c.type === "men")
      .map(
        (c: any) =>
          c.tournamentCalendar.filter((s: any) => s.active === "yes")[0].id
      );

    // let players: Array<any> = [];

    // const [s1, s2, s3] = seasonIds;
    // await seasonIds.forEach(async id => {
    for (let id of seasonIds /*.slice(0, 150)*/) {
      const seasonSquadUrl = `http://api.performfeeds.com/soccerdata/squads/${API_KEY}?_rt=b&_fmt=json&tmcl=${id}&detailed=yes`;
      console.log(seasonSquadUrl);
      const response = await axios.get(seasonSquadUrl);
      const teams: Array<any> = response.data.squad;
      const seasonPlayers: Array<any> = teams.reduce(
        (acc, team) => [
          ...acc,
          ...team.person.map((person: any) => ({
            ...person,
            teamId: team.contestantId,
            teamName: team.contestantClubName
          }))
        ],
        []
      );

      console.log({ seasonPlayers });
      if (seasonPlayers.length > 0) {
        const chunks = splitIntoChunks(seasonPlayers, MAX_BATCH_SIZE);
        for (const chunk of chunks) {
          const batch = db.batch();
          for (const player of chunk) {
            const docRef = db
              .collection("app-data")
              .doc("transfer-suggestions")
              .collection("players")
              .doc(player.id);

            batch.set(docRef, player, { merge: true });
          }
          await batch.commit();
        }
      }
    }
    // console.log({ seasonIds });
  } catch (error) {
    console.log(error);
  }
}
